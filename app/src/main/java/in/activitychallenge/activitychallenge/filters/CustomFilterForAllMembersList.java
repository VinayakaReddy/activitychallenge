package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.adapter.AllMembersAdapter;
import in.activitychallenge.activitychallenge.models.AllMembersModel;


public class CustomFilterForAllMembersList extends Filter {

    AllMembersAdapter adapter;
    ArrayList<AllMembersModel> filterList;

    public CustomFilterForAllMembersList(ArrayList<AllMembersModel> filterList, AllMembersAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AllMembersModel> filteredPlayers=new ArrayList<AllMembersModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.allmemberlistModels = (ArrayList<AllMembersModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
