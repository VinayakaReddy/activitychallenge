package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.adapter.GetMemUpdateStatusGroupAdapter;
import in.activitychallenge.activitychallenge.models.GetMemUpdtStatusModel;


public class CustomFilterforMembersStatus extends Filter {

    GetMemUpdateStatusGroupAdapter adapter;
    ArrayList<GetMemUpdtStatusModel> filterList;

    public CustomFilterforMembersStatus(ArrayList<GetMemUpdtStatusModel> filterList, GetMemUpdateStatusGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<GetMemUpdtStatusModel> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.getMemStatusList = (ArrayList<GetMemUpdtStatusModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
