package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.adapter.GroupActivityAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;


public class GroupActivityFilterList extends Filter {

    GroupActivityAdapter adapter;
    ArrayList<ActivityModel> filterList;

    public GroupActivityFilterList(ArrayList<ActivityModel> filterList, GroupActivityAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<ActivityModel> filteredPlayers=new ArrayList<ActivityModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getActivity_name().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.listModels = (ArrayList<ActivityModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
