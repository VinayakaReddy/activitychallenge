package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.fragments.AllGroupsFragment;
import in.activitychallenge.activitychallenge.models.AllGroupsModel;


public class MyAllGroupFilterList extends Filter {

    AllGroupsFragment.MyGroupAdapter adapter;
    ArrayList<AllGroupsModel> filterList;

    public MyAllGroupFilterList(ArrayList<AllGroupsModel> filterList, AllGroupsFragment.MyGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AllGroupsModel> filteredPlayers=new ArrayList<AllGroupsModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.myList = (ArrayList<AllGroupsModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
