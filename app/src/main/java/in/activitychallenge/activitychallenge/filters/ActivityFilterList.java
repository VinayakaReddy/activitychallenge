package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.adapter.ActivityAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;



public class ActivityFilterList extends Filter {

    ActivityAdapter adapter;
    ArrayList<ActivityModel> filterList;

    public ActivityFilterList(ArrayList<ActivityModel> filterList, ActivityAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<ActivityModel> filteredPlayers=new ArrayList<ActivityModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getActivity_name().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.listModels = (ArrayList<ActivityModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
