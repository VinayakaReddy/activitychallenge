package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.adapter.ChallengeToFriendAdapter;
import in.activitychallenge.activitychallenge.adapter.GroupListAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeToFriendModel;
import in.activitychallenge.activitychallenge.models.GroupListModel;


public class CustomFilterForGroupChat extends Filter {

    GroupListAdapter adapter;
    ArrayList<GroupListModel> filterList;

    public CustomFilterForGroupChat(ArrayList<GroupListModel> filterList, GroupListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<GroupListModel> filteredPlayers=new ArrayList<GroupListModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getFrom_name().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.groupListModels = (ArrayList<GroupListModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
