package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.CardAdapter;
import in.activitychallenge.activitychallenge.adapter.AllMembersAdapter;
import in.activitychallenge.activitychallenge.models.ActivityDataModel;
import in.activitychallenge.activitychallenge.models.AllMembersModel;


public class CustomFilterForAllActivityWinLossList extends Filter {

    CardAdapter adapter;
    ArrayList<ActivityDataModel> filterList;

    public CustomFilterForAllActivityWinLossList(ArrayList<ActivityDataModel> filterList, CardAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<ActivityDataModel> filteredPlayers=new ArrayList<ActivityDataModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getActivity_name().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.activityDataList = (ArrayList<ActivityDataModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
