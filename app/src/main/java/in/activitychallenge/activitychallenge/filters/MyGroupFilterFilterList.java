package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.fragments.MyGroupsFragment;
import in.activitychallenge.activitychallenge.models.MyGroupModel;


public class MyGroupFilterFilterList extends Filter {

    MyGroupsFragment.MyGroupAdapter adapter;
    ArrayList<MyGroupModel> filterList;

    public MyGroupFilterFilterList(ArrayList<MyGroupModel> filterList, MyGroupsFragment.MyGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<MyGroupModel> filteredPlayers=new ArrayList<MyGroupModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.myList = (ArrayList<MyGroupModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
