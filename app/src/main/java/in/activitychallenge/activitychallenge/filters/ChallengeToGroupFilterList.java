package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.activities.ChallengeToGroupActivity;
import in.activitychallenge.activitychallenge.models.ChallengeToGroupModel;


public class ChallengeToGroupFilterList extends Filter {

    ChallengeToGroupActivity.MyGroupAdapter adapter;
    ArrayList<ChallengeToGroupModel> filterList;

    public ChallengeToGroupFilterList(ArrayList<ChallengeToGroupModel> filterList, ChallengeToGroupActivity.MyGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<ChallengeToGroupModel> filteredPlayers=new ArrayList<ChallengeToGroupModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.myList = (ArrayList<ChallengeToGroupModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
