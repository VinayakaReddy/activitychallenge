package in.activitychallenge.activitychallenge.filters;

import android.widget.Filter;

import java.util.ArrayList;


import in.activitychallenge.activitychallenge.fragments.SponsorChatFragment;
import in.activitychallenge.activitychallenge.models.MessagesModel;


public class CustomFilterForSponsorChat extends Filter {

    SponsorChatFragment.MessagesSponsorAdapter adapter;
    ArrayList<MessagesModel> filterList;

    public CustomFilterForSponsorChat(ArrayList<MessagesModel> filterList, SponsorChatFragment.MessagesSponsorAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<MessagesModel> filteredPlayers=new ArrayList<MessagesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getFrom_name().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.msgModels = (ArrayList<MessagesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
