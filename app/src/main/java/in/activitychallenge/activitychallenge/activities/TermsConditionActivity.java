package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;

public class TermsConditionActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    TextView about_text_title;
    Typeface typeface, typeface_bold;
    WebView browser;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        about_text_title = (TextView) findViewById(R.id.about_text_title);
        about_text_title.setTypeface(typeface_bold);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        browser = (WebView) findViewById(R.id.webVw);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setWebViewClient(new MyBrowser());
        browser.loadUrl(AppUrls.BASE_IMAGE_URL + AppUrls.TERMS_CONDITION_LINK);
    }

    private class MyBrowser extends WebViewClient
    {


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
