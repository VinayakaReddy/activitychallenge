package in.activitychallenge.activitychallenge.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.models.DeactivateReasonModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class DeactivateAccountActivity extends AppCompatActivity implements View.OnClickListener {

    TextView deactivate_button;
    ImageView close;
    RecyclerView recyclerview_radio_group;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    RadioButton radio_other;
    RecyclerView.LayoutManager layoutManager;
    DeactivateAccAdapter deactivateAccAdapter;
    ArrayList<DeactivateReasonModel> reasonmodelList = new ArrayList<DeactivateReasonModel>();
    LinearLayout ll_description;
    EditText reason_other_desc_edt;
    String running_chalnge, upcoming_chalnge, pending_chalnge, paused_chalnge, wallet_amt, my_groups, send_check_Value;
    Dialog dialog,dialog2;
    private RadioButton lastCheckedRB = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivate_account);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(DeactivateAccountActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        ll_description = (LinearLayout) findViewById(R.id.ll_description);
        reason_other_desc_edt = (EditText) findViewById(R.id.reason_other_desc_edt);
        radio_other = (RadioButton) findViewById(R.id.radio_other);
        deactivate_button = (TextView) findViewById(R.id.deactivate_button);
        deactivate_button.setOnClickListener(this);
        recyclerview_radio_group = (RecyclerView) findViewById(R.id.recyclerview_radio_group);
        deactivateAccAdapter = new DeactivateAccAdapter(reasonmodelList, DeactivateAccountActivity.this, R.layout.row_deactivate_reason_list);
        layoutManager = new LinearLayoutManager(this);
        recyclerview_radio_group.setNestedScrollingEnabled(false);
        recyclerview_radio_group.setLayoutManager(layoutManager);

        getReasons();

        radio_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (radio_other.isChecked()) {
                    ll_description.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    private void getReasons() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_deactivate = AppUrls.BASE_URL + AppUrls.USER_DEACTIVATE_REASON;
            Log.d("DEACTURL", url_deactivate);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_deactivate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("DECTRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                DeactivateReasonModel dact = new DeactivateReasonModel();
                                dact.setId(jdataobj.getString("id"));
                                dact.setReason(jdataobj.getString("reason"));
                                reasonmodelList.add(dact);
                            }
                            recyclerview_radio_group.setAdapter(deactivateAccAdapter);
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateAccountActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(DeactivateAccountActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == deactivate_button) {
            getDeactivateDetail();

        }


    }

    private void getDeactivateDetail() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_all_members = AppUrls.BASE_URL + AppUrls.DEACTIVATE_USER_DETAIL + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("DDUSEDETAIURL", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("DDUSERDETRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject dataObj = jobcode.getJSONObject("data");
                            running_chalnge = dataObj.getString("running_challenges");
                            upcoming_chalnge = dataObj.getString("upcoming_challenges");
                            pending_chalnge = dataObj.getString("pending_challenges");
                            paused_chalnge = dataObj.getString("paused_challenges");
                            wallet_amt = dataObj.getString("wallet_amt");
                            //my_groups = dataObj.getString("my_groups");
                            // Log.d("DDDDDDD",running_chalnge+"//"+upcoming_chalnge+"//"+pending_chalnge+"//"+paused_chalnge+"//"+wallet_amt+"//"+my_groups);
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        getDialogDetail();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateAccountActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(DeactivateAccountActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }

    }

    private void getDialogDetail()
    {
        dialog = new Dialog(DeactivateAccountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.deactivate_detail_custom_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

     //title
        TextView pending_title_text = (TextView) dialog.findViewById(R.id.pending_title_text);
        TextView pausedtitle_text = (TextView) dialog.findViewById(R.id.pausedtitle_text);
        TextView upcoming_title_text = (TextView) dialog.findViewById(R.id.upcoming_title_text);
        TextView running_title_text = (TextView) dialog.findViewById(R.id.running_title_text);

     //value text
        TextView okdialogbtn = (TextView) dialog.findViewById(R.id.okdialogbtn);
        final TextView sendmoneydialogbtn = (TextView) dialog.findViewById(R.id.sendmoneydialogbtn);
        TextView pending_text = (TextView) dialog.findViewById(R.id.pending_text);
        TextView paused_text = (TextView) dialog.findViewById(R.id.paused_text);
        TextView upcoming_text = (TextView) dialog.findViewById(R.id.upcoming_text);
        TextView running_text = (TextView) dialog.findViewById(R.id.running_text);
        TextView wallet_amount_text = (TextView) dialog.findViewById(R.id.wallet_amount_text);
        TextView mygroups_text = (TextView) dialog.findViewById(R.id.mygroups_text);
        if(user_type.equals("SPONSOR"))
        {
            //texttitle
            pending_title_text.setVisibility(View.GONE);
            pausedtitle_text.setVisibility(View.GONE);
            upcoming_title_text.setVisibility(View.GONE);
            running_title_text.setVisibility(View.GONE);
           //value
            pending_text.setVisibility(View.GONE);
            paused_text.setVisibility(View.GONE);
            upcoming_text.setVisibility(View.GONE);
            running_text.setVisibility(View.GONE);
        }
        pending_text.setText(pending_chalnge);
        paused_text.setText(paused_chalnge);
        upcoming_text.setText(upcoming_chalnge);
        running_text.setText(running_chalnge);
        wallet_amount_text.setText(Html.fromHtml("&#36;" + "<b>" + wallet_amt + "</b>"));
        mygroups_text.setText(my_groups);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
        okdialogbtn.setOnClickListener(new View.OnClickListener()
        {

            @Override

            public void onClick(View v) {
                // dialog.cancel();
               double parse_wallet_amt=Double.parseDouble(wallet_amt);
                if (parse_wallet_amt!=0)
                {
                    Toast.makeText(DeactivateAccountActivity.this, "You Can't Deactivate Account...!  Please Transfer Amount to your PayPal Account.", Toast.LENGTH_LONG).show();
                    sendmoneydialogbtn.setVisibility(View.VISIBLE);
                }
                else if(!running_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateAccountActivity.this, "You Can't Deactivate Account...!  Please Complete Running Challnge.", Toast.LENGTH_LONG).show();
                }
                else if(!upcoming_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateAccountActivity.this, "You Can't Deactivate Account...!  Please Complete Upcoming Challegne.", Toast.LENGTH_LONG).show();
                }
                else if(!pending_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateAccountActivity.this, "You Can't Deactivate Account...!  Please Complete Pending Challenge.", Toast.LENGTH_LONG).show();
                }
                else if(!paused_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateAccountActivity.this, "You Can't Deactivate Account...!  Please Complete Paused Challenge.", Toast.LENGTH_LONG).show();
                }
                else
                    {
                        finalDeactivationAcc();
                }
            }

        });
        sendmoneydialogbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {// Toast.makeText(DeactivateAccountActivity.this, "SEND MONEY", Toast.LENGTH_LONG).show();
                dialog2 = new Dialog(DeactivateAccountActivity.this);
                dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog2.setContentView(R.layout.custom_dialog_send_money);
                final EditText edt_send_amount = (EditText) dialog2.findViewById(R.id.edt_send_amount);
                final Button sendMoneyButton = (Button) dialog2.findViewById(R.id.sendMoneyButton);

                sendMoneyButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        String send_amount=edt_send_amount.getText().toString();

                        if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                        {
                            edt_send_amount.setError("Please Enter Amount..!");
                        }
                        else
                        {
                            sendWalletAmount(send_amount);

                        }
                    }
                });

                dialog2.show();

            }
        });
        dialog.show();
    }

    private void finalDeactivationAcc() {

        checkInternet = NetworkChecking.isConnected(DeactivateAccountActivity.this);
        if (checkInternet) {
            String finaldeact_user_url = AppUrls.BASE_URL + AppUrls.DEACTIVATE_USER;

            final String send_reason_other = reason_other_desc_edt.getText().toString();
            Log.d("DEACTFIALURL", finaldeact_user_url + "//" + send_reason_other);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, finaldeact_user_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("FinalDeactivatRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            // JSONObject dataObj = jobcode.getJSONObject("message");
                            Toast.makeText(DeactivateAccountActivity.this, "Deactivated Successfully..", Toast.LENGTH_LONG).show();
                            dialog.cancel();
                            Intent redirect = new Intent(DeactivateAccountActivity.this, VerifyAccountActivity.class);
                            startActivity(redirect);
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    if (radio_other.isChecked()) {
                        param.put("reason", send_reason_other);
                    } else {
                        param.put("reason", send_check_Value);
                    }

                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateAccountActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            Toast.makeText(DeactivateAccountActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    ///////////////////////////////////////////////ADAPTER///////////////////////////////////////////

    public class DeactivateAccAdapter extends RecyclerView.Adapter<DeactivateReasonHolder> {
        public ArrayList<DeactivateReasonModel> reasonlistModels;
        DeactivateAccountActivity context;
        LayoutInflater li;
        int resource;
        Typeface typeface, typeface2;
        ProgressDialog progressDialog;
        UserSessionManager session;
        String user_name;

        public DeactivateAccAdapter(ArrayList<DeactivateReasonModel> reasonlistModels, DeactivateAccountActivity context, int resource) {
            this.reasonlistModels = reasonlistModels;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
            typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
            session = new UserSessionManager(context);
            HashMap<String, String> userDetails = session.getUserDetails();
            user_name = userDetails.get(UserSessionManager.USER_NAME);

        }

        @Override
        public DeactivateReasonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            DeactivateReasonHolder slh = new DeactivateReasonHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final DeactivateReasonHolder holder, final int position) {
            String str_reason = reasonlistModels.get(position).getReason();
            String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
            holder.reasone_radio_dynamic_button.setText(converted_string);

            View.OnClickListener rbClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RadioButton checked_rb = (RadioButton) v;
                    if (lastCheckedRB != null) {
                        lastCheckedRB.setChecked(false);
                        radio_other.setChecked(false);
                        ll_description.setVisibility(View.GONE);
                        send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;
                    radio_other.setChecked(false);
                    ll_description.setVisibility(View.GONE);
                    send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
                }
            };
            holder.reasone_radio_dynamic_button.setOnClickListener(rbClick);
            radio_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (radio_other.isChecked()) {
                        ll_description.setVisibility(View.VISIBLE);
                        reasonmodelList.clear();
                        getReasons();
                    }
                }
            });
            holder.setItemClickListener(new DeactivateReasonItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {


                }
            });
        }

        @Override
        public int getItemCount() {
            return this.reasonlistModels.size();
        }
    }
//////////////////////////////////Holder////////////////////////////////////////////////////////////////////////

    public class DeactivateReasonHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RadioButton reasone_radio_dynamic_button;
        DeactivateReasonItemClickListener deactivateReasonItemClickListener;

        public DeactivateReasonHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            reasone_radio_dynamic_button = (RadioButton) itemView.findViewById(R.id.reasone_radio_dynamic_button);
        }

        @Override
        public void onClick(View view) {

            this.deactivateReasonItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(DeactivateReasonItemClickListener ic) {
            this.deactivateReasonItemClickListener = ic;
        }
    }

    /////////////////// ITEM CLICK LISTERNER////////////////////
    public interface DeactivateReasonItemClickListener {
        void onItemClick(View v, int pos);
    }

    private void sendWalletAmount(final String amount)
    {
        checkInternet = NetworkChecking.isConnected(DeactivateAccountActivity.this);

        if (checkInternet)
        {
            dialog.cancel();
            String send_money_url = AppUrls.BASE_URL + AppUrls.TRANSFER_AMOUNT_TO_PAYPAL_ACC;


            Log.d("SENDMONEYLURL", send_money_url);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, send_money_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("RESPSENDMONEY", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(DeactivateAccountActivity.this, "Payment transfered successfully..", Toast.LENGTH_LONG).show();

                            Intent redirect = new Intent(DeactivateAccountActivity.this, DeactivateAccountActivity.class);
                            startActivity(redirect);
                            finish();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "User not exist.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Paypal Registered Email not Found..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10500")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Insufficient amount.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10600")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(DeactivateAccountActivity.this, "Payment transfer failed.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    param.put("amount", amount);

                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateAccountActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            Toast.makeText(DeactivateAccountActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
}
