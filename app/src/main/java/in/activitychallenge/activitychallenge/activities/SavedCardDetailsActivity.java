package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.SavedCardDetailsAdapter;
import in.activitychallenge.activitychallenge.models.SavedCardDetailsModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SavedCardDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,no_data_image;
    LinearLayout add_card_img;
    TextView toolbar_title;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String device_id, token, user_id;
    UserSessionManager session;
    RecyclerView saved_cards_recyclerview;
    LinearLayoutManager layoutManager;
    SavedCardDetailsAdapter adapter;
    ArrayList<SavedCardDetailsModel> savedCardDetailsModels = new ArrayList<SavedCardDetailsModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_card_details);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(SavedCardDetailsActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        no_data_image = (ImageView) findViewById(R.id.no_data_image);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        add_card_img = (LinearLayout) findViewById(R.id.add_card_img);
        add_card_img.setOnClickListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        saved_cards_recyclerview = (RecyclerView) findViewById(R.id.saved_cards_recyclerview);
        adapter = new SavedCardDetailsAdapter(savedCardDetailsModels, SavedCardDetailsActivity.this, R.layout.row_saved_card_details);
        layoutManager = new LinearLayoutManager(this);
        saved_cards_recyclerview.setNestedScrollingEnabled(false);
        saved_cards_recyclerview.setLayoutManager(layoutManager);

        getSavedCards();
    }

    private void getSavedCards() {
        checkInternet = NetworkChecking.isConnected(SavedCardDetailsActivity.this);
        Log.d("SAVEDURL", AppUrls.BASE_URL + AppUrls.SAVED_CARD + user_id);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.SAVED_CARD + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("SAVEDCARDS", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SavedCardDetailsModel scd = new SavedCardDetailsModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        scd.setId(jsonObject1.getString("id"));
                                        scd.setAcc_type(jsonObject1.getString("acc_type"));
                                        scd.setAcc_no(jsonObject1.getString("acc_no"));
                                        scd.setCc_year(jsonObject1.getString("cc_year"));
                                        scd.setCc_month(jsonObject1.getString("cc_month"));
                                        scd.setCc_name_on_card(jsonObject1.getString("cc_name_on_card"));
                                        scd.setCc_cvv(jsonObject1.getString("cc_cvv"));
                                        scd.setCreated_on(jsonObject1.getString("created_on"));
                                        savedCardDetailsModels.add(scd);
                                    }
                                    saved_cards_recyclerview.setAdapter(adapter);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(SavedCardDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    no_data_image.setVisibility(View.VISIBLE);
                                   // Toast.makeText(SavedCardDetailsActivity.this, "No Data Found..", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEDCARDHEAD", headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SavedCardDetailsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(SavedCardDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            Intent intent = new Intent(SavedCardDetailsActivity.this, MainActivity.class);
            startActivity(intent);
        }
        if (v == add_card_img)
        {


            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {

                Intent intent = new Intent(SavedCardDetailsActivity.this, SaveCardDetailsActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }







        }
    }
}
