package in.activitychallenge.activitychallenge.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.MyChallengesGroupFragment;
import in.activitychallenge.activitychallenge.fragments.MyChallengesIndividualFragment;
import in.activitychallenge.activitychallenge.fragments.SpecialEventFragment;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyChallengesActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    ViewPager view_pager_my_chalng;
    TabLayout tab;
    private boolean checkInternet;
    MyChllengesViewPagerAdapter myChallengeadapter;

    UserSessionManager userSessionManager;
    String token, user_id, user_type, device_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_challenges);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        tab = (TabLayout) findViewById(R.id.tabLayout_mychalleng);
        view_pager_my_chalng = (ViewPager) findViewById(R.id.view_pager_my_chalng);
        setupViewPager(view_pager_my_chalng);
        tab.setupWithViewPager(view_pager_my_chalng);
    }

    private void setupViewPager(ViewPager view_pager_suggest) {
        myChallengeadapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());
        myChallengeadapter.addFrag(new MyChallengesIndividualFragment(), "Individual");
        myChallengeadapter.addFrag(new MyChallengesGroupFragment(), "Group");
       // myChallengeadapter.addFrag(new SpecialEventFragment(), "SpecialEvent");
        view_pager_suggest.setAdapter(myChallengeadapter);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}

