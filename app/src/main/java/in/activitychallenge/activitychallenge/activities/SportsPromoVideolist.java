package in.activitychallenge.activitychallenge.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.SportsPromoVideoHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.PromoVideoClickListner;
import in.activitychallenge.activitychallenge.models.SportsPromoVideoModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SportsPromoVideolist extends AppCompatActivity implements View.OnClickListener {
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    SportsPromoVideoAdapter promoAdap;
    RecyclerView promo_recycler;
    LinearLayoutManager linLay;
    ArrayList<SportsPromoVideoModel> promooList = new ArrayList<SportsPromoVideoModel>();
    VideoView vid_view;
    ImageView close;
    TextView txtil;
    String device_id, access_token, vid_uri, txt_title,userid,usertype;
    DisplayMetrics dm;
    MediaController media_Controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_promo_videolist);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        userid = userDetails.get(UserSessionManager.USER_ID);
        usertype = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SESSIgATA", access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        txtil = (TextView) findViewById(R.id.txtil);
        vid_view = (VideoView) findViewById(R.id.vid_view);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        promo_recycler = (RecyclerView) findViewById(R.id.promo_recycler);
        promo_recycler.setNestedScrollingEnabled(false);
        promoAdap = new SportsPromoVideoAdapter(SportsPromoVideolist.this, promooList, R.layout.row_promovideo);
        linLay = new LinearLayoutManager(this);
        promo_recycler.setLayoutManager(new LinearLayoutManager(SportsPromoVideolist.this, LinearLayoutManager.HORIZONTAL, false));

        getVideoList();
    }

    private void getVideoList() {
        checkInternet = NetworkChecking.isConnected(SportsPromoVideolist.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("PROMOVIDE", AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO);
                            Log.d("PROMOVIDE_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                  //  Toast.makeText(SportsPromoVideolist.this, "Success", Toast.LENGTH_SHORT).show();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        SportsPromoVideoModel rhm = new SportsPromoVideoModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setAdv_id(itemArray.getString("adv_id"));
                                        rhm.setCompany_name(itemArray.getString("company_name"));
                                        rhm.setTimespan(itemArray.getString("timespan"));
                                        rhm.setVideo_path(itemArray.getString("video_path"));
                                        promooList.add(rhm);
                                    }
                                    promo_recycler.setAdapter(promoAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(SportsPromoVideolist.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("PROMOMMOVID_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SportsPromoVideolist.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(SportsPromoVideolist.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    public class SportsPromoVideoAdapter extends RecyclerView.Adapter<SportsPromoVideoHolder> {
        SportsPromoVideolist context;
        ArrayList<SportsPromoVideoModel> promoList;
        int resource;
        LayoutInflater lInfla;

        public SportsPromoVideoAdapter(SportsPromoVideolist context, ArrayList<SportsPromoVideoModel> promoList, int resource) {
            this.context = context;
            this.resource = resource;
            this.promoList = promoList;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public SportsPromoVideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            SportsPromoVideoHolder rHol = new SportsPromoVideoHolder(inf);
            return rHol;

        }

        @Override
        public void onBindViewHolder(SportsPromoVideoHolder holder, final int position)
        {
            holder.title.setText(promoList.get(position).getCompany_name());

            final String aaa = promoList.get(position).getVideo_path();
            final String video_id = promoList.get(position).getId();
            holder.setItemClickListener(new PromoVideoClickListner() {
                @Override
                public void onItemClick(View v, int pos) {
                  //  vid_uri = AppUrls.BASE_IMAGE_URL + promoList.get(pos).getVideo_path();
                    txt_title = promoList.get(pos).getCompany_name();
                    Log.d("dsfdfdf", txt_title);
                    try {
                        // Start the MediaController

                        String uriPath = AppUrls.BASE_IMAGE_URL + promoList.get(pos).getVideo_path();
                        Log.d("VIDEOPATH", uriPath);
                        final Dialog dialog = new Dialog(SportsPromoVideolist.this);// add here your class name
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.scratch_custom_videoview_dialog);
                        final   VideoView adv_vid_view = (VideoView) dialog.findViewById(R.id.scratch_vid_view);

                        dialog.show();

                        media_Controller = new MediaController(SportsPromoVideolist.this);
                        dm = new DisplayMetrics();
                        SportsPromoVideolist.this.getWindowManager().getDefaultDisplay().getMetrics(dm);
                        int height = dm.heightPixels;
                        int width = dm.widthPixels;
                        adv_vid_view.setMinimumWidth(width);
                        adv_vid_view.setMinimumHeight(height);

                        try {
                            Uri video = Uri.parse(uriPath);
                            adv_vid_view.setVideoURI(video);
                            adv_vid_view.setMediaController(media_Controller);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                        adv_vid_view.requestFocus();
                        adv_vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            // Close the progress bar and play the video
                            public void onPrepared(MediaPlayer mp) {

                                adv_vid_view.start();

                            }
                        });

                        dialog.setCanceledOnTouchOutside(false);

                        adv_vid_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                txtil.setVisibility(View.GONE);
                                dialog.dismiss();
                                getScratchData(video_id);


                                //call scratch Card after completion video

                            }
                        });
                       /* try
                        {
                            Uri video = Uri.parse(uriPath);
                            vid_view.setVideoURI(video);
                            vid_view.setMediaController(mediacontroller);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                        vid_view.requestFocus();
                        vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            // Close the progress bar and play the video
                            public void onPrepared(MediaPlayer mp) {

                                vid_view.start();

                            }
                        });*/
                        txtil.setText(txt_title);
                        Log.d("SSCCC", txt_title);
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return promoList.size();
        }
    }

    private void getScratchData(String video_id)
    {
        checkInternet = NetworkChecking.isConnected(SportsPromoVideolist.this);
        if (checkInternet)
        {
            Log.d("vvVIDEURL", AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO_COMPLETION+"?user_type="+usertype+"&user_id="+userid+"&adv_id="+video_id);

            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO_COMPLETION+"?user_type="+usertype+"&user_id="+userid+"&adv_id="+video_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("COMPLETRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    pprogressDialog.dismiss();
                                 /*   "data": {
                                    "scratch_card": {
                                        "amount": 0.03,
                                                "id": "5aa767d1839b7e26b41b0008",
                                                "is_scratched": 0,
                                                "scratching_date": "2018-03-12 18:30:00",
                                                "view": 1
                                    }*/
                                    JSONObject jsobjj_data = jsonObject.getJSONObject("data");
                                    JSONObject jsob_scratch_data = jsobjj_data.getJSONObject("scratch_card");
                                    String amount=jsob_scratch_data.getString("amount");
                                    String id=jsob_scratch_data.getString("id");
                                    String is_scratched=jsob_scratch_data.getString("is_scratched");
                                    String view=jsob_scratch_data.getString("view");
                                    if(is_scratched.equals("0"))
                                    {
                                        Intent sc_history = new Intent(SportsPromoVideolist.this, AdvertiseScratchCardActivity.class);

                                        sc_history.putExtra("AMOUNT", amount);
                                        sc_history.putExtra("SCRATCH_ID", id);
                                        startActivity(sc_history);
                                    }
                                    else
                                    {
                                       //nothing
                                    }



                                }
                               /* if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(SportsPromoVideolist.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("PROMOMMOVID_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SportsPromoVideolist.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(SportsPromoVideolist.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }
}
