package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.DayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.adapter.OpponentDayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.models.DayDateWiseStatusModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ConnectivityReceiver;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.StatusMinuiteChallengeDB;
import in.activitychallenge.activitychallenge.utilities.StatusPictureChallengeDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class PictureChallengeStatusActivity extends AppCompatActivity implements View.OnClickListener, Animation.AnimationListener, ConnectivityReceiver.ConnectivityReceiverListener {

    ImageView close, challenger_one_image, challenger_two_image, goal, upload_txt, upload_holo_img, upload_holo_img_oponent;
    TextView toolbar_title, vsTxt, participent_oponent_type, participent_user_type, participent_user_name, participent_oponent_name, memb_count_top, mem_cccount_down;
    Typeface typeface3;
    private boolean checkInternet;
    UserSessionManager session;
    String token = "", device_id = "", usd_usertype, challenge_id, activity_id, challenge_goal, opponent_type, group_id, user_type_for_challenge, user_id, user_type, user_completed_goal, opponent_completed_goal;
    ProgressDialog progressDialog;
    String opponentType;
    int id = 1;
    TextView activity_name, cash_price, challenger_one_rank, challenger_two_rank;
    LinearLayout ll_top, ll_down;
    RelativeLayout reltive_hollow_top, reltive_hollow_down;
    String opponent_id, opponent_grp_id, usd_user_id, usd_group_id, challenge_type, evaluation_factor;
    StatusPictureChallengeDB statusPictureChallengeDB;
    ContentValues values;
    String user_hollow_picImg,opponent_hollow_picImg;

    RecyclerView hollow_recyclerview_days_status_top, hollow_recyclerview_days_status_down;
    DayDateWiseChallengeStatusAdapter dayDateWiseChallengeStatusAdapter;
    ArrayList<DayDateWiseStatusModel> dayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    ArrayList<DayDateWiseStatusModel> opponentdayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    OpponentDayDateWiseChallengeStatusAdapter opponentDayDateWiseChallengeStatusAdapter;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_picture_challenge_status);
        setContentView(R.layout.dummy_picture_challenge_status);
        statusPictureChallengeDB = new StatusPictureChallengeDB(PictureChallengeStatusActivity.this);
        values = new ContentValues();
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        activity_id = bundle.getString("activity_id");
        opponentType = bundle.getString("opponent_type");
        Log.v("Type",opponentType);
        close = (ImageView) findViewById(R.id.close);
        challenger_one_image = (ImageView) findViewById(R.id.challenger_one_image);
        challenger_two_image = (ImageView) findViewById(R.id.challenger_two_image);
        upload_holo_img = (ImageView) findViewById(R.id.upload_holo_img);
        upload_holo_img.setOnClickListener(this);
        upload_holo_img_oponent = (ImageView) findViewById(R.id.upload_holo_img_oponent);
        upload_holo_img_oponent.setOnClickListener(this);

        hollow_recyclerview_days_status_top = (RecyclerView) findViewById(R.id.hollow_recyclerview_days_status_top);
        hollow_recyclerview_days_status_down = (RecyclerView) findViewById(R.id.hollow_recyclerview_days_status_down);

        close.setOnClickListener(this);
        ll_top = (LinearLayout) findViewById(R.id.ll_top);
        ll_down = (LinearLayout) findViewById(R.id.ll_down);

        reltive_hollow_top = (RelativeLayout) findViewById(R.id.reltive_hollow_top);
        reltive_hollow_down = (RelativeLayout) findViewById(R.id.reltive_hollow_down);


        typeface3 = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.hermes));
        vsTxt = (TextView) findViewById(R.id.vsTxt);
        vsTxt.setTypeface(typeface3);
        participent_oponent_type = (TextView) findViewById(R.id.participent_oponent_type);
        participent_user_type = (TextView) findViewById(R.id.participent_user_type);
        participent_user_name = (TextView) findViewById(R.id.participent_user_name);
        participent_oponent_name = (TextView) findViewById(R.id.participent_oponent_name);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        activity_name = (TextView) findViewById(R.id.activity_name);
        goal = (ImageView) findViewById(R.id.goal);
        cash_price = (TextView) findViewById(R.id.cash_price);
        challenger_one_rank = (TextView) findViewById(R.id.challenger_one_rank);
        challenger_two_rank = (TextView) findViewById(R.id.challenger_two_rank);
        upload_txt = (ImageView) findViewById(R.id.upload_txt);
        upload_txt.setOnClickListener(this);

        memb_count_top = (TextView) findViewById(R.id.memb_count_top);
        memb_count_top.setOnClickListener(this);
        mem_cccount_down = (TextView) findViewById(R.id.mem_cccount_down);
        mem_cccount_down.setOnClickListener(this);


        getPictureChallengeStatus();
        //  checkConnection();
    }

    public void getPictureChallengeStatus() {
        checkInternet = NetworkChecking.isConnected(PictureChallengeStatusActivity.this);
        if (checkInternet) {
            //statusPictureChallengeDB.emptyDBBucket();
            Log.d("PICTURECHALLSTATUS", AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("PICTURESTATUSRESP", response);
                            try {
                                values.put(StatusMinuiteChallengeDB.CHALLENGE_ID, challenge_id);
                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("activity");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String challenge_id = jsonObject2.getString("challenge_id");
                                        String user_id_ = jsonObject2.getString("user_id");
                                        user_type_for_challenge = jsonObject2.getString("user_type");
                                        String opponent_id = jsonObject2.getString("opponent_id");
                                        opponent_type = jsonObject2.getString("opponent_type");
                                        if (opponent_type.equals("ADMIN")) {
                                            ll_down.setVisibility(View.GONE);
                                            vsTxt.setVisibility(View.GONE);
                                        } else {
                                            ll_down.setVisibility(View.VISIBLE);
                                            vsTxt.setVisibility(View.VISIBLE);
                                        }
                                        participent_user_type.setText(user_type_for_challenge);

                                        if (user_type_for_challenge.equals("USER")) {
                                            user_id = user_id;
                                            user_type = "USER";
                                        } else {
                                            group_id = user_id_;
                                            user_type = "GROUP";

                                        }

                                     /*   if (opponent_type.equals("ADMIN")) {
                                            ll_down.setVisibility(View.GONE);
                                            vsTxt.setVisibility(View.GONE);
                                        }
*/
                                        challenge_type = jsonObject2.getString("challenge_type");
                                        if (challenge_type.equals("DAILY")) {

                                            hollow_recyclerview_days_status_top.setVisibility(View.GONE);
                                            hollow_recyclerview_days_status_down.setVisibility(View.GONE);
                                        } else {
                                            hollow_recyclerview_days_status_down.setVisibility(View.VISIBLE);
                                            hollow_recyclerview_days_status_top.setVisibility(View.VISIBLE);
                                        }
                                        participent_oponent_type.setText(opponent_type);

                                        challenge_goal = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("challenge_goal");
                                        values.put(StatusPictureChallengeDB.CHALLENGE_GOAL, challenge_goal);
                                        Picasso.with(PictureChallengeStatusActivity.this)
                                                .load(challenge_goal)
                                                .placeholder(R.drawable.dummy_user32)
                                                .into(goal);
                                        String amount = jsonObject2.getString("amount");
                                        cash_price.setText(" $" + amount);
                                        values.put(StatusPictureChallengeDB.AMOUNT, " $" + amount);
                                        evaluation_factor = jsonObject2.getString("evaluation_factor");
                                        String activity_name_string = jsonObject2.getString("activity_name");
                                        values.put(StatusPictureChallengeDB.ACTIVITY_NAME, activity_name_string);
                                        activity_name.setText(activity_name_string);
                                        String activity_image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image");
                                        String evaluation_factor_unit = jsonObject2.getString("evaluation_factor_unit");
                                        String start_on = jsonObject2.getString("start_on");
                                        String completed_on = jsonObject2.getString("completed_on");
                                        String completed_on_txt = jsonObject2.getString("completed_on_txt");

                                    }
                                    String user_count = jsonObject1.getString("user_count");
                                    values.put(StatusPictureChallengeDB.USER_COUNT,user_count);

                                    if (!user_count.equals("1")) {
                                        memb_count_top.setVisibility(View.VISIBLE);
                                        memb_count_top.setText("members " + user_count + "\n" + "see more..");

                                    } else {
                                        memb_count_top.setVisibility(View.GONE);
                                    }

                                    String opponent_count = jsonObject1.getString("opponent_count");
                                    values.put(StatusPictureChallengeDB.OPPONENT_COUNT,opponent_count);
                                    if (!opponent_count.equals("1")) {
                                        mem_cccount_down.setVisibility(View.VISIBLE);
                                        mem_cccount_down.setText("members " + opponent_count + "\n" + "see more..");

                                    } else {
                                        mem_cccount_down.setVisibility(View.GONE);
                                    }


                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("user_details");

                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    usd_usertype = jsonObject3.getString("user_type");
                                    user_completed_goal = jsonObject3.getString("user_completed_goal");

                                    usd_user_id = jsonObject3.getString("user_id");
                                    usd_group_id = jsonObject3.getString("group_id");
                                    values.put(StatusPictureChallengeDB.USER_GROUP_ID,usd_group_id);

                                    String user_name = jsonObject3.getString("user_name");
                                    participent_user_name.setText(user_name);
                                    String user_rank = jsonObject3.getString("user_rank");
                                    challenger_one_rank.setText(user_rank);
                                    String image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("image");
                                    Log.d("UUUUUU", image);
                                    String group_name = jsonObject3.getString("group_name");
                                    String group_rank = jsonObject3.getString("group_rank");
                                    String group_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("group_image");
                                    String super_admin_name = jsonObject3.getString("super_admin_name");
                                    String super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("super_admin_image");
                                    String super_admin_rank = jsonObject3.getString("super_admin_rank");

                                    ///////////////////////////////////////////////Day WISE STATUS TOP/////////////////////////
                                    //  dayDateWiseStatusModel.clear();


                                    JSONArray jsonchallenger = jsonObject1.getJSONArray("challenger_daywise_status");
                                    Log.d("KDKDKDKDK", jsonchallenger.toString());
                                    for (int i = 0; i < jsonchallenger.length(); i++) {
                                        JSONObject jdataobj = jsonchallenger.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        dayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", dayDateWiseStatusModel.toString());

                                    }
                                    hollow_recyclerview_days_status_top.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(PictureChallengeStatusActivity.this);
                                    hollow_recyclerview_days_status_top.setLayoutManager(new LinearLayoutManager(PictureChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    dayDateWiseChallengeStatusAdapter = new DayDateWiseChallengeStatusAdapter(dayDateWiseStatusModel, PictureChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    hollow_recyclerview_days_status_top.setNestedScrollingEnabled(false);
                                    hollow_recyclerview_days_status_top.setAdapter(dayDateWiseChallengeStatusAdapter);


////////////////////////////////////////END/////////////////////////////////////////////////////


                                    if (usd_usertype.equals("USER")) {
                                        participent_user_name.setText(user_name);
                                        participent_user_type.setText(usd_usertype);
                                        challenger_one_rank.setText(user_rank);

                                        Picasso.with(PictureChallengeStatusActivity.this)
                                                .load(image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusPictureChallengeDB.USER_NAME, user_name);
                                        values.put(StatusPictureChallengeDB.USER_TYPE, usd_usertype);
                                        values.put(StatusPictureChallengeDB.USER_RANK, user_rank);
                                        values.put(StatusPictureChallengeDB.IMAGE, image);
                                        if (user_completed_goal.equals("0")) {
                                            // upload_holo_img.setVisibility(View.GONE);
                                            reltive_hollow_top.setVisibility(View.GONE);
                                            values.put(StatusPictureChallengeDB.USER_COMPLETED_GOAL, "");
                                        } else {
                                            user_hollow_picImg=AppUrls.BASE_IMAGE_URL + user_completed_goal;
                                            Log.d("PPPPSDPF", AppUrls.BASE_IMAGE_URL + user_completed_goal);
                                            reltive_hollow_top.setVisibility(View.VISIBLE);
                                            Picasso.with(PictureChallengeStatusActivity.this)
                                                    .load(user_hollow_picImg)
                                                    .placeholder(R.drawable.no_image_found)
                                                    .into(upload_holo_img);
                                            values.put(StatusPictureChallengeDB.USER_COMPLETED_GOAL, user_hollow_picImg);
                                        }
                                    } else {
                                        participent_user_name.setText(group_name);
                                        challenger_one_rank.setText(group_rank);
                                        participent_user_type.setText(usd_usertype);
                                        Picasso.with(PictureChallengeStatusActivity.this)
                                                .load(group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusPictureChallengeDB.USER_NAME, group_name);
                                        values.put(StatusPictureChallengeDB.USER_TYPE, usd_usertype);
                                        values.put(StatusPictureChallengeDB.USER_RANK, group_rank);
                                        values.put(StatusPictureChallengeDB.IMAGE, group_image);
                                        if (user_completed_goal.equals("0")) {
                                            // upload_holo_img.setVisibility(View.GONE);
                                            reltive_hollow_top.setVisibility(View.GONE);
                                            values.put(StatusPictureChallengeDB.USER_COMPLETED_GOAL, "");
                                        } else {
                                            user_hollow_picImg=AppUrls.BASE_IMAGE_URL + user_completed_goal;
                                            reltive_hollow_top.setVisibility(View.VISIBLE);
                                            Picasso.with(PictureChallengeStatusActivity.this)
                                                    .load(user_hollow_picImg)
                                                    .placeholder(R.drawable.no_image_found)
                                                    .into(upload_holo_img);

                                            values.put(StatusPictureChallengeDB.USER_COMPLETED_GOAL, user_hollow_picImg);
                                        }


                                    }

                                 /*   Picasso.with(PictureChallengeStatusActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(challenger_one_image);*/
                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("opponent_details");
                                    JSONObject jsonObject4 = jsonArray2.getJSONObject(0);
                                    opponent_type = jsonObject4.getString("opponent_type");
                                    opponent_completed_goal = jsonObject4.getString("opponent_completed_goal");

                                    opponent_id = jsonObject4.getString("opponent_id");
                                    opponent_grp_id = jsonObject4.getString("opponent_group_id");
                                     values.put(StatusPictureChallengeDB.OPPONENT_GROUP_ID,opponent_grp_id);
                                    String opponent_name = jsonObject4.getString("opponent_name");
                                    Log.d("LGOGOGOGO", opponent_name);
                                    participent_oponent_name.setText(opponent_name);
                                    String opponent_rank = jsonObject4.getString("opponent_rank");
                                    //challenger_two_rank.setText(opponent_rank);
                                    String opponent_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_image");
                                    String opponent_group_name = jsonObject4.getString("opponent_group_name");
                                    String opponent_group_rank = jsonObject4.getString("opponent_group_rank");
                                    String opponent_group_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_group_image");
                                    String opponent_super_admin_name = jsonObject4.getString("opponent_super_admin_name");
                                    String opponent_super_admin_rank = jsonObject4.getString("opponent_super_admin_rank");
                                    String opponent_super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_super_admin_image");

                                    ///////////////////////////////////////////////Day WISE STATUS DOWN/////////////////////////

                                    JSONArray jsonopponent = jsonObject1.getJSONArray("opponent_daywise_status");
                                    Log.d("KDKDKDKDK", jsonopponent.toString());
                                    for (int i = 0; i < jsonopponent.length(); i++) {
                                        JSONObject jdataobj = jsonopponent.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        opponentdayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", opponentdayDateWiseStatusModel.toString());

                                    }

                                    hollow_recyclerview_days_status_down.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(PictureChallengeStatusActivity.this);
                                    hollow_recyclerview_days_status_down.setLayoutManager(new LinearLayoutManager(PictureChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    opponentDayDateWiseChallengeStatusAdapter = new OpponentDayDateWiseChallengeStatusAdapter(opponentdayDateWiseStatusModel, PictureChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    hollow_recyclerview_days_status_down.setNestedScrollingEnabled(false);
                                    hollow_recyclerview_days_status_down.setAdapter(opponentDayDateWiseChallengeStatusAdapter);

////////////////////////////////////////END/////////////////////////////////////////////////////

                                    if (opponent_type.equals("USER")) {
                                        participent_oponent_name.setText(opponent_name);
                                        participent_oponent_type.setText(opponent_type);
                                        challenger_two_rank.setText(opponent_rank);

                                        Picasso.with(PictureChallengeStatusActivity.this)
                                                .load(opponent_image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusPictureChallengeDB.OPPONENT_NAME, opponent_name);
                                        values.put(StatusPictureChallengeDB.OPPONENT_TYPE, opponent_type);
                                        values.put(StatusPictureChallengeDB.OPPONENT_RANK, opponent_rank);
                                        values.put(StatusPictureChallengeDB.OPPONENT_IMAGE, opponent_image);

                                        if (opponent_completed_goal.equals("0")) {
                                            //  upload_holo_img_oponent.setVisibility(View.GONE);
                                            reltive_hollow_down.setVisibility(View.GONE);
                                            values.put(StatusPictureChallengeDB.OPPONENT_COMPLETED_GOAL, "");
                                        } else {
                                            opponent_hollow_picImg=AppUrls.BASE_IMAGE_URL + opponent_completed_goal;
                                            reltive_hollow_down.setVisibility(View.VISIBLE);
                                            Picasso.with(PictureChallengeStatusActivity.this)
                                                    .load(opponent_hollow_picImg)
                                                    .placeholder(R.drawable.no_image_found)
                                                    .into(upload_holo_img_oponent);
                                            values.put(StatusPictureChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_hollow_picImg);
                                        }
                                    } else {
                                        participent_oponent_name.setText(opponent_group_name);
                                        participent_oponent_type.setText(opponent_type);
                                        challenger_two_rank.setText(opponent_group_rank);
                                        Log.d("afdasdf", opponent_group_image);
                                        Picasso.with(PictureChallengeStatusActivity.this)
                                                .load(opponent_group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_two_image);

                                        if (opponent_completed_goal.equals("0")) {
                                            //upload_holo_img_oponent.setVisibility(View.GONE);
                                            reltive_hollow_down.setVisibility(View.GONE);
                                            values.put(StatusPictureChallengeDB.OPPONENT_COMPLETED_GOAL, "");
                                        } else {
                                            opponent_hollow_picImg=AppUrls.BASE_IMAGE_URL + opponent_completed_goal;
                                            reltive_hollow_down.setVisibility(View.VISIBLE);
                                            Picasso.with(PictureChallengeStatusActivity.this)
                                                    .load(opponent_hollow_picImg)
                                                    .placeholder(R.drawable.no_image_found)
                                                    .into(upload_holo_img_oponent);
                                            values.put(StatusPictureChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_hollow_picImg);
                                        }
                                        values.put(StatusPictureChallengeDB.OPPONENT_NAME, opponent_group_name);
                                        values.put(StatusPictureChallengeDB.OPPONENT_TYPE, opponent_type);
                                        values.put(StatusPictureChallengeDB.OPPONENT_RANK, opponent_group_rank);
                                        values.put(StatusPictureChallengeDB.OPPONENT_IMAGE, opponent_group_image);


                                    }
                                    statusPictureChallengeDB.addPictures(values);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(PictureChallengeStatusActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(PictureChallengeStatusActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PictureChallengeStatusActivity.this);
            requestQueue.add(strRe);
        } else {
            progressDialog.cancel();
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            statusPicturesData();
            //Toast.makeText(PictureChallengeStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == upload_txt) {
            Intent intent = new Intent(PictureChallengeStatusActivity.this, UploadHoloPictureActivity.class);
            intent.putExtra("challenge_id", challenge_id);
            intent.putExtra("activity_id", activity_id);
            intent.putExtra("group_id", usd_group_id);

          /*  intent.putExtra("group_id", group_id);
            intent.putExtra("USER_TYPE", user_type);*/
            //opponent_type //usd_usertype ///////opponent_id , opponent_grp_id, usd_user_id ,usd_group_id;

            Log.d("MediumData", activity_id + "\n" + challenge_id + "////group_id");
            startActivity(intent);
        }

        if (view == upload_holo_img) {
            Intent top = new Intent(PictureChallengeStatusActivity.this, HoloImageZoomActivity.class);
            top.putExtra("HOLOPATH", user_hollow_picImg);
            Log.d("HOLOPATH", user_hollow_picImg);
            startActivity(top);
        }
        if (view == upload_holo_img_oponent) {
            Intent top = new Intent(PictureChallengeStatusActivity.this, HoloImageZoomActivity.class);
            top.putExtra("HOLOPATH", opponent_hollow_picImg);
            Log.d("HOLOPATH", opponent_hollow_picImg);
            startActivity(top);
        }
        if (view == memb_count_top) { //opponent_type    //usd_usertype
            Intent top = new Intent(PictureChallengeStatusActivity.this, PictureMemberCountChallengeActivity.class);
            top.putExtra("challenge_id", challenge_id);

            if (usd_usertype.equals("GROUP")) {
                top.putExtra("group_id", usd_group_id);
            } else {
                top.putExtra("group_id", "");
            }

            startActivity(top);
        }
        if (view == mem_cccount_down) {
            Intent bottom = new Intent(PictureChallengeStatusActivity.this, PictureMemberCountChallengeActivity.class);
            bottom.putExtra("challenge_id", challenge_id);
            if (opponent_type.equals("GROUP")) {
                bottom.putExtra("group_id", opponent_grp_id);
            } else {
                bottom.putExtra("group_id", "");
            }

            startActivity(bottom);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {

            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //show a No Internet Alert or Dialog
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {

            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);

            // Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            // dismiss the dialog or refresh the activity
        }
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {

            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //show a No Internet Alert or Dialog
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {

            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);

            // Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            // dismiss the dialog or refresh the activity
        }
    }

    private void statusPicturesData() {

        List<String> activityName = statusPictureChallengeDB.getActivityName();

        String challengeId = statusPictureChallengeDB.ChallengeIDstr(challenge_id);
        Log.v("ChallengeId", challengeId);


        List<String> challeneg_Ids = statusPictureChallengeDB.getChallengeID();


        List<String> user_type = statusPictureChallengeDB.getUserType();
        List<String> challenge_goal = statusPictureChallengeDB.getChallengeGoal();
        List<String> amount = statusPictureChallengeDB.getAmount();
        List<String> activity_name_string = statusPictureChallengeDB.getActivityName();

        List<String> user_name = statusPictureChallengeDB.getUserName();
        List<String> user_rank = statusPictureChallengeDB.getUserRank();
        List<String> image = statusPictureChallengeDB.getImage();
        List<String> oppentName = statusPictureChallengeDB.getopponentName();
        List<String> oppentrank = statusPictureChallengeDB.getopponentrank();

        List<String> oppentImage = statusPictureChallengeDB.getopponentImge();
        List<String> oppenttype = statusPictureChallengeDB.getopponentType();

        List<String> user_complet_goal = statusPictureChallengeDB.getUserCompletedGoal();
        List<String> usercounts = statusPictureChallengeDB.getUserCount();
        List<String> opponentcounts = statusPictureChallengeDB.getOpponentCount();
        List<String> usergroupids = statusPictureChallengeDB.getUserGroupId();
        List<String> opponentgroupids = statusPictureChallengeDB.getOpponentGroupId();

        List<String> opponentgoal = statusPictureChallengeDB.getopponentgoal();
        for (int i = 0; i < challeneg_Ids.size(); i++) {
            if (challeneg_Ids.get(i).equalsIgnoreCase(challengeId)) {
                cash_price.setText(amount.get(i));
                activity_name.setText(activity_name_string.get(i));
                Picasso.with(PictureChallengeStatusActivity.this)
                        .load(challenge_goal.get(i))
                        .placeholder(R.drawable.no_image_found)
                        .into(goal);
                usd_usertype=user_name.get(i);
                participent_user_name.setText(user_name.get(i));
                participent_user_type.setText(user_type.get(i));
                challenger_one_rank.setText(user_rank.get(i));
                Picasso.with(PictureChallengeStatusActivity.this)
                        .load(image.get(i))
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(challenger_one_image);

                if (!usercounts.get(i).equals("1")) {
                    memb_count_top.setVisibility(View.VISIBLE);
                    memb_count_top.setText("members " + usercounts.get(i) + "\n" + "see more..");

                } else {
                    memb_count_top.setVisibility(View.GONE);
                }
                if (!opponentcounts.get(i).equals("1")) {
                    mem_cccount_down.setVisibility(View.VISIBLE);
                    mem_cccount_down.setText("members " + opponentcounts.get(i) + "\n" + "see more..");

                } else {
                    mem_cccount_down.setVisibility(View.GONE);
                }
                opponent_grp_id=opponentgroupids.get(i);
                usd_group_id=opponentgroupids.get(i);
                if (user_complet_goal.get(i).equals("0")) {
                    reltive_hollow_top.setVisibility(View.GONE);

                } else {

                    Log.d("PPPPSDPF", AppUrls.BASE_IMAGE_URL + user_complet_goal.get(i));
                    if (user_complet_goal.get(i).toString().length() > 0) {
                        user_hollow_picImg=user_complet_goal.get(i);
                        reltive_hollow_top.setVisibility(View.VISIBLE);
                        Picasso.with(PictureChallengeStatusActivity.this)
                                .load(user_complet_goal.get(i))
                                .placeholder(R.drawable.no_image_found)
                                .into(upload_holo_img);
                    }

                    String opponenttype = oppenttype.get(i);
                     if(opponenttype!=null && opponenttype.length()>0){
                         opponent_type=opponenttype;
                         if (opponenttype.equals("ADMIN")) {
                             ll_down.setVisibility(View.GONE);
                             vsTxt.setVisibility(View.GONE);
                         } else {
                             ll_down.setVisibility(View.VISIBLE);
                             vsTxt.setVisibility(View.VISIBLE);
                             participent_oponent_name.setText(oppentName.get(i));
                             if(opponentType.contains("Group") || opponentType.contains("GROUP"))
                                 participent_oponent_type.setText("GROUP");
                             else
                                 participent_oponent_type.setText("USER");
                             challenger_two_rank.setText(oppentrank.get(i));
                             if (oppentImage.get(i).length() > 0) {

                                 Picasso.with(PictureChallengeStatusActivity.this)
                                         .load(oppentImage.get(i))
                                         .placeholder(R.drawable.dummy_user_profile)
                                         .into(challenger_two_image);
                             }
                     }

                        Log.v("Opponent",opponentgoal.get(i));
                        if (opponentgoal.get(i).equals("0")) {
                            //  upload_holo_img_oponent.setVisibility(View.GONE);
                            reltive_hollow_down.setVisibility(View.GONE);

                        } else {
                            if (opponentgoal.get(i).length() > 0) {
                               opponent_hollow_picImg=opponentgoal.get(i);
                                reltive_hollow_down.setVisibility(View.VISIBLE);
                                Picasso.with(PictureChallengeStatusActivity.this)
                                        .load(opponentgoal.get(i))
                                        .placeholder(R.drawable.no_image_found)
                                        .into(upload_holo_img_oponent);
                            }

                        }

                    }

                }
            }


        }

    }
}
