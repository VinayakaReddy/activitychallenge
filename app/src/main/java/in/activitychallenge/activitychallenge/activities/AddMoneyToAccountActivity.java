package in.activitychallenge.activitychallenge.activities;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import in.activitychallenge.activitychallenge.R;

public class AddMoneyToAccountActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView add_money_btn;
    RadioGroup pay_radio_group;
    RadioButton paypal_radio, debitcard_radio;
    TextInputLayout addmoney_user_til, addmoney_password_til, card_number_til, cvv_number_til, customer_name_til;
    EditText addmoney_user_edt, addmoney_pasword_edt, card_number_edt, cvv_number_edt, customer_name_edt;
    Typeface typeface, typeface2;
    TextView monet_header_title, card_exp_date_txt;
    RelativeLayout relative_paypal, relative_credit_detail;
    public int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_money_to_account);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        addmoney_user_edt = (EditText) findViewById(R.id.addmoney_user_edt);
        addmoney_user_edt.setTypeface(typeface);
        addmoney_pasword_edt = (EditText) findViewById(R.id.addmoney_pasword_edt);
        addmoney_pasword_edt.setTypeface(typeface);
        card_number_edt = (EditText) findViewById(R.id.card_number_edt);
        card_number_edt.setTypeface(typeface);
        cvv_number_edt = (EditText) findViewById(R.id.cvv_number_edt);
        cvv_number_edt.setTypeface(typeface);
        customer_name_edt = (EditText) findViewById(R.id.customer_name_edt);
        customer_name_edt.setTypeface(typeface);
        monet_header_title = (TextView) findViewById(R.id.monet_header_title);
        monet_header_title.setTypeface(typeface2);
        card_exp_date_txt = (TextView) findViewById(R.id.card_exp_date_txt);
        card_exp_date_txt.setTypeface(typeface);
        addmoney_user_til = (TextInputLayout) findViewById(R.id.addmoney_user_til);
        addmoney_password_til = (TextInputLayout) findViewById(R.id.addmoney_password_til);
        card_number_til = (TextInputLayout) findViewById(R.id.card_number_til);
        cvv_number_til = (TextInputLayout) findViewById(R.id.cvv_number_til);
        customer_name_til = (TextInputLayout) findViewById(R.id.customer_name_til);
        add_money_btn = (ImageView) findViewById(R.id.add_money_btn);
        add_money_btn.setOnClickListener(this);
        relative_paypal = (RelativeLayout) findViewById(R.id.relative_paypal);
        relative_credit_detail = (RelativeLayout) findViewById(R.id.relative_credit_detail);
        pay_radio_group = (RadioGroup) findViewById(R.id.pay_radio_group);
        paypal_radio = (RadioButton) findViewById(R.id.paypal_radio);
        paypal_radio.setOnClickListener(this);
        paypal_radio.setTypeface(typeface);
        debitcard_radio = (RadioButton) findViewById(R.id.debitcard_radio);
        debitcard_radio.setOnClickListener(this);
        debitcard_radio.setTypeface(typeface);
    }


    @Override
    public void onClick(View view) {
        if (debitcard_radio.isChecked()) {
            relative_paypal.setVisibility(View.GONE);
            relative_credit_detail.setVisibility(View.VISIBLE);
        }

        if (paypal_radio.isChecked()) {
            relative_paypal.setVisibility(View.VISIBLE);
            relative_credit_detail.setVisibility(View.GONE);
        }

        if (view == card_exp_date_txt) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    card_exp_date_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));
                    card_exp_date_txt.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }


    public boolean validate_paypal() {
        boolean result = true;
        String name = addmoney_user_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            addmoney_user_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                addmoney_user_til.setError("Special characters not allowed");
                result = false;

            } else {
                addmoney_user_til.setErrorEnabled(false);
            }
        }

        String psw = addmoney_pasword_edt.getText().toString().trim();
        if (psw == null || psw.equals("") || psw.length() < 6) {
            addmoney_password_til.setError("Invalid Pssword/ Min 6 Character");
            result = false;
        } else
            addmoney_password_til.setErrorEnabled(false);

        return result;
    }

    public boolean validate_debit_card() {
        boolean result = true;

        String customer_name = customer_name_edt.getText().toString().trim();
        if ((customer_name == null || customer_name.equals("") || customer_name.length() < 3)) {
            customer_name_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!customer_name.matches("^[\\p{L} .'-]+$")) {
                customer_name_til.setError("Special characters not allowed");
                result = false;

            } else {
                customer_name_til.setErrorEnabled(false);
            }
        }


        String cvv = cvv_number_edt.getText().toString().trim();

        if (cvv == null || cvv.equals("") || cvv.length() < 3) {
            cvv_number_til.setError("Invalid CVV ");
            result = false;
        } else
            cvv_number_til.setErrorEnabled(false);
        String card_number = card_number_edt.getText().toString().trim();

        if (card_number == null || card_number.equals("") || card_number.length() < 16) {
            card_number_til.setError("Invalid Card Number");
            result = false;
        } else
            card_number_til.setErrorEnabled(false);

        return result;
    }


}
