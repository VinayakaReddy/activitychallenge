package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clock.scratch.ScratchView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AdvertiseScratchCardActivity extends AppCompatActivity implements View.OnClickListener{

    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    String user_id, user_type, token, device_id, amount, scratch_type, scratch_ID, ref_txn_id = "", wallet_amount;
    ImageView close;
    TextView prize_text, tell_to_button;
    ScratchView ad_video_scratch_view;
    Typeface typeface, typeface2;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_scratch_card);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        Bundle bundle = getIntent().getExtras();
        amount = bundle.getString("AMOUNT");
        scratch_ID = bundle.getString("SCRATCH_ID");
        Log.d("SCRATCHTYPE", scratch_type + "" + scratch_ID);
        userSessionManager = new UserSessionManager(AdvertiseScratchCardActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);

        progressDialog = new ProgressDialog(AdvertiseScratchCardActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        prize_text = (TextView) findViewById(R.id.prize_text);
        tell_to_button = (TextView) findViewById(R.id.tell_to_button);
        tell_to_button.setOnClickListener(this);
        ad_video_scratch_view = (ScratchView) findViewById(R.id.ad_video_scratch_view);


        ad_video_scratch_view.setEraseStatusListener(new ScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (amount.equals("") || amount.equals("0")) {
                    prize_text.setText("Better luck  \n next time..!");
                } else {
                    prize_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + amount + "</b>"));
                }
            }

            @Override
            public void onCompleted(View view) {
                ad_video_scratch_view.clear();
                sendScratchId(scratch_ID);
            }
        });
    }

    private void sendScratchId(String sratchcard_id) {

        checkInternet = NetworkChecking.isConnected(AdvertiseScratchCardActivity.this);
        final String _sratchcard = sratchcard_id;

        if (checkInternet) {
            //   progressDialog.show();
            Log.d("GETOTPURL:", AppUrls.BASE_URL + AppUrls.SCRATCH_CARD_DO);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SCRATCH_CARD_DO,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("GETOTPRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    //  Toast.makeText(CardPageActivity.this, "Scratch card updated successfully..!", Toast.LENGTH_LONG).show();
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    ref_txn_id = data.getString("ref_txn_id");


                                }
                                if (successResponceCode.equals("10200")) {
                                    //   Toast.makeText(CardPageActivity.this, "Invalid input..!", Toast.LENGTH_LONG).show();


                                }
                                if (successResponceCode.equals("10300")) {
                                    //   Toast.makeText(CardPageActivity.this, "Scratch Card not found..!", Toast.LENGTH_LONG).show();

                                }
                                if (successResponceCode.equals("10400")) {
                                    //    Toast.makeText(CardPageActivity.this, "Scratch card is already scratched..!", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("scratch_card_id", _sratchcard);
                    Log.d("GETOTPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AdvertiseScratchCardActivity.this);
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(AdvertiseScratchCardActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view)
    {

        if (view == close)
        {
            if (ref_txn_id.equals("")) {
                ad_video_scratch_view.clear();
            } else {
                AdvertiseScratchCardActivity.this.finish();
            }
        }
        if (view == tell_to_button)
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I have won the amount  "+"$"+amount+" in ActivityChallenge App Why don't you join?. "+AppUrls.SHARE_APP_URL);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
    }

    @Override
    public void onBackPressed()
    {

        if (ref_txn_id.equals("")) {
            ad_video_scratch_view.clear();
        } else {
            AdvertiseScratchCardActivity.this.finish();
        }
        //  super.onBackPressed();
    }
}
