package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.AllMembersAdapter;
import in.activitychallenge.activitychallenge.models.AllMembersModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllMembersActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_data_image;
    TextView allmembers_toolbar_title;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    SearchView members_search;
    RecyclerView recycler_all_members;
    AllMembersAdapter allMembersAdapter;
    ArrayList<AllMembersModel> allmemMoldelList = new ArrayList<AllMembersModel>();
    RecyclerView.LayoutManager layoutManager;
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_members);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        pprogressDialog = new ProgressDialog(AllMembersActivity.this);

        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        no_data_image = (ImageView) findViewById(R.id.no_data_image);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        members_search = (SearchView) findViewById(R.id.members_search);

        members_search.setOnClickListener(this);
        View view = findViewById(R.id.custom_tab);
        members_search.setIconified(false);
        members_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                members_search.setIconified(false);
            }
        });
        members_search.clearFocus();
        members_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
               if(allMembersAdapter!=null)
                allMembersAdapter.getFilter().filter(query);
                return false;
            }
        });
        recycler_all_members = (RecyclerView) findViewById(R.id.recycler_all_members);
        allMembersAdapter = new AllMembersAdapter(allmemMoldelList, AllMembersActivity.this, R.layout.row_all_members);
        layoutManager = new LinearLayoutManager(this);
        recycler_all_members.setNestedScrollingEnabled(false);
        recycler_all_members.setLayoutManager(layoutManager);
        getCount();
        getAllmembers();
        if (user_type.equals("USER")) {
            view.setVisibility(View.VISIBLE);
        }else {
            view.setVisibility(View.GONE);
        }
    }

    private void getAllmembers() {
        allmemMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            //                       http://192.168.1.61:8090/api/v1/user?user_id=5a2e086eba81eb10188eed6c&user_type=USER
            String url_all_members = AppUrls.BASE_URL + AppUrls.ALL_MEMBERS + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("MEMBERSURL", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("MEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMembersModel allmember = new AllMembersModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                JSONArray jarrActivityimg = jdataobj.getJSONArray("recent_five_activity");
                                allmember.setImage1(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(0).getString("img1"));
                                allmember.setImage2(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(1).getString("img2"));
                                allmember.setImage3(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(2).getString("img3"));
                                allmember.setImage4(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(3).getString("img4"));
                                allmember.setImage5(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(4).getString("img5"));
                                allmemMoldelList.add(allmember);
                            }
                            recycler_all_members.setAdapter(allMembersAdapter);

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(AllMembersActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            no_data_image.setVisibility(View.VISIBLE);
                            //Toast.makeText(AllMembersActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllMembersActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(AllMembersActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(AllMembersActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(AllMembersActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(AllMembersActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllMembersActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
  
}