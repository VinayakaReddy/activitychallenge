package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.SponsorRequestAdapter;
import in.activitychallenge.activitychallenge.models.SponsorRequestModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SponsorRequestActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_requests_img;
    TextView sponsor_req_toolbar_title;
    Typeface typeface, typeface_bold;
    RecyclerView sponsor_request_recyclerview;
    LinearLayoutManager layoutManager;
    SponsorRequestAdapter adapter;
    ArrayList<SponsorRequestModel> sponsorRequestModels = new ArrayList<SponsorRequestModel>();
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String user_id = "", device_id = "", token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_request);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        no_requests_img = (ImageView) findViewById(R.id.no_requests_img);
        sponsor_req_toolbar_title = (TextView) findViewById(R.id.sponsor_req_toolbar_title);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        sponsor_request_recyclerview = (RecyclerView) findViewById(R.id.sponsor_request_recyclerview);
        adapter = new SponsorRequestAdapter(sponsorRequestModels, SponsorRequestActivity.this, R.layout.row_sponsor_list);
        layoutManager = new LinearLayoutManager(this);
        sponsor_request_recyclerview.setNestedScrollingEnabled(false);
        sponsor_request_recyclerview.setLayoutManager(layoutManager);

        getSponsorRequest();

    }

    private void getSponsorRequest() {
        checkInternet = NetworkChecking.isConnected(SponsorRequestActivity.this);
        if (checkInternet) {
            Log.d("URL",AppUrls.BASE_URL + AppUrls.SPONSOR_REQUEST + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.SPONSOR_REQUEST + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("RERERERE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);
                                        SponsorRequestModel crm = new SponsorRequestModel();
                                        crm.setId(jsonObject1.getString("id"));
                                        crm.setAmount_paid(jsonObject1.getString("amount_paid"));
                                        crm.setTransaction_id(jsonObject1.getString("transaction_id"));
                                        crm.setSponsor_type(jsonObject1.getString("sponsor_type"));
                                        crm.setStatus(jsonObject1.getString("status"));
                                        crm.setSponsor_id(jsonObject1.getString("sponsor_id"));
                                        crm.setSponsor_name(jsonObject1.getString("sponsor_name"));
                                        crm.setSponsor_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("sponsor_image"));
                                        sponsorRequestModels.add(crm);
                                    }
                                    sponsor_request_recyclerview.setAdapter(adapter);

                                    if (jarray.length() == 0) {
                                        no_requests_img.setVisibility(View.VISIBLE);
                                    }
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_requests_img.setVisibility(View.VISIBLE);
                                    Toast.makeText(SponsorRequestActivity.this, "No Sponsor Requests Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    no_requests_img.setVisibility(View.VISIBLE);
                                   // Toast.makeText(SponsorRequestActivity.this, "No Sponsor Requests Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SponsorRequestActivity.this);
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(SponsorRequestActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            Intent intent = new Intent(SponsorRequestActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}