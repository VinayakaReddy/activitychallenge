package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyFollowersAdapter;
import in.activitychallenge.activitychallenge.models.AllMembersModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyFollowersActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close,default_img;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    RecyclerView myfollowers_recyclerview;
    MyFollowersAdapter myFollowersAdapter;
    ArrayList<AllMembersModel> myFollowersMoldelList = new ArrayList<AllMembersModel>();
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_followers);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        pprogressDialog = new ProgressDialog(MyFollowersActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        default_img = (ImageView) findViewById(R.id.default_img);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        myfollowers_recyclerview = (RecyclerView) findViewById(R.id.myfollowers_recyclerview);
        myFollowersAdapter = new MyFollowersAdapter(myFollowersMoldelList, MyFollowersActivity.this, R.layout.row_all_members);
        layoutManager = new LinearLayoutManager(this);
        myfollowers_recyclerview.setNestedScrollingEnabled(false);
        myfollowers_recyclerview.setLayoutManager(layoutManager);

        getMyFollosers();
    }

    //MY_FOLLOWERS
    private void getMyFollosers() {
        checkInternet = NetworkChecking.isConnected(MyFollowersActivity.this);
        if (checkInternet) {
            String following_url = AppUrls.BASE_URL + AppUrls.MY_FOLLOWERS + "?user_id=" + user_id;
            Log.d("FOLLOWERSURL", following_url);
            StringRequest strFollowers = new StringRequest(Request.Method.GET, following_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("FOLLOWersMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMembersModel allmember = new AllMembersModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                JSONArray jarrActivityimg = jdataobj.getJSONArray("recent_five_activity");
                                allmember.setImage1(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(0).getString("img1"));
                                allmember.setImage2(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(1).getString("img2"));
                                allmember.setImage3(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(2).getString("img3"));
                                allmember.setImage4(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(3).getString("img4"));
                                allmember.setImage5(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(4).getString("img5"));
                                myFollowersMoldelList.add(allmember);
                            }
                            myfollowers_recyclerview.setAdapter(myFollowersAdapter);
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "Users does not exist..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {
                            pprogressDialog.dismiss();
                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "No Followers found.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyFollowersActivity.this);
            requestQueue.add(strFollowers);
        } else {
            Toast.makeText(MyFollowersActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
