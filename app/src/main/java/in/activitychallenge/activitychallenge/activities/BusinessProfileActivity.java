package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vatsal.imagezoomer.ImageZoomButton;
import com.vatsal.imagezoomer.ZoomAnimation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.dbhelper.BusinessProfileDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class BusinessProfileActivity extends AppCompatActivity implements View.OnClickListener{
    ImageView close;
    ImageZoomButton qr_code_imagview;
    private boolean checkInternet;
    TextInputLayout ti_name, ti_business, ti_address, ti_pincode,ti_business_type;
    EditText edt_name, edt_business_name, edt_address, edt_pincode,edt_business_type;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String user_id, user_type,  details,  access_token, device_id,name,business_name,address,pincode,qr_code,business_type;
    TextView send_detail_button,text_title,edit_profile;
    BusinessProfileDB businessProfileDB;
    ContentValues values;
    long duration = 500;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        businessProfileDB=new BusinessProfileDB(BusinessProfileActivity.this);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_business_name = (EditText) findViewById(R.id.edt_business_name);
        edt_address = (EditText) findViewById(R.id.edt_address);
        edt_pincode = (EditText) findViewById(R.id.edt_pincode);
        edt_business_type = (EditText) findViewById(R.id.edt_business_type);

        ti_name = (TextInputLayout) findViewById(R.id.ti_name);
        ti_business = (TextInputLayout) findViewById(R.id.ti_business);
        ti_address = (TextInputLayout) findViewById(R.id.ti_address);
        ti_pincode = (TextInputLayout) findViewById(R.id.ti_pincode);
        ti_business_type = (TextInputLayout) findViewById(R.id.ti_business_type);


        text_title = (TextView) findViewById(R.id.text_title);
        edit_profile = (TextView) findViewById(R.id.edit_profile);
        send_detail_button = (TextView) findViewById(R.id.send_detail_button);
        send_detail_button.setOnClickListener(this);
        edit_profile.setOnClickListener(this);


        close = (ImageView) findViewById(R.id.close);
        qr_code_imagview = (ImageZoomButton) findViewById(R.id.qr_code_imagview);
        close.setOnClickListener(this);
        qr_code_imagview.setOnClickListener(this);

        getBusinessData();

    }

    private void getBusinessData()
    {
        checkInternet = NetworkChecking.isConnected(BusinessProfileActivity.this);
         if(checkInternet)
         {
             Log.d("getbusinessurl", AppUrls.BASE_URL + AppUrls.BUSINESS_PROFILE_GET+"?user_id="+user_id);
             StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.BUSINESS_PROFILE_GET+"?user_id="+user_id,
                     new Response.Listener<String>() {
                         @Override
                         public void onResponse(String response) {
                             pprogressDialog.dismiss();
                             Log.d("getBusinessRESP", response);

                             try {
                                 JSONObject jsonObject = new JSONObject(response);
                                 String successResponceCode = jsonObject.getString("response_code");
                                 if (successResponceCode.equals("10100"))
                                 {
                                     pprogressDialog.dismiss();
                                     JSONObject jobData=jsonObject.getJSONObject("data");
                                     edit_profile.setVisibility(View.VISIBLE);
                                      values=new ContentValues();
                                     name = jobData.getString("owner_name");
                                     values.put(BusinessProfileDB.OWNER_NAME,name);
                                     edt_name.setText(name);
                                     edt_name.setFocusable(false);
                                     edt_name.setFocusableInTouchMode(false);
                                     business_name = jobData.getString("business_name");
                                     values.put(BusinessProfileDB.BUSINESS_NAME,business_name);
                                     edt_business_name.setText(business_name);
                                     edt_business_name.setFocusable(false);
                                     edt_business_name.setFocusableInTouchMode(false);
                                     business_type = jobData.getString("business_type");
                                     values.put(BusinessProfileDB.BUSINESS_TYPE,business_type);
                                     edt_business_type.setText(business_type);
                                     edt_business_type.setFocusable(false);
                                     edt_business_type.setFocusableInTouchMode(false);
                                     address = jobData.getString("address");
                                     values.put(BusinessProfileDB.ADDRESS,address);
                                     edt_address.setText(address);
                                     edt_address.setFocusable(false);
                                     edt_address.setFocusableInTouchMode(false);
                                     pincode = jobData.getString("pincode");
                                     values.put(BusinessProfileDB.PINCODE,pincode);
                                     edt_pincode.setText(pincode);
                                     edt_pincode.setFocusable(false);
                                     edt_pincode.setFocusableInTouchMode(false);
                                     qr_code = AppUrls.BASE_IMAGE_URL+jobData.getString("qrcode");
                                     values.put(BusinessProfileDB.QRCODE,qr_code);
                                     Log.d("PICQR",qr_code);
                                     Picasso.with(BusinessProfileActivity.this)
                                             .load(qr_code)
                                             .placeholder(R.drawable.no_qrcode)
                                             .into(qr_code_imagview);

                                     businessProfileDB.businessProfileSponsor(values);


                                 }
                                 if (successResponceCode.equals("10200"))
                                 {
                                     pprogressDialog.dismiss();
                                     Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                 }if (successResponceCode.equals("10300"))
                                 {
                                     pprogressDialog.dismiss();
                                     text_title.setVisibility(View.VISIBLE);
                                     send_detail_button.setVisibility(View.VISIBLE);
                                    // Toast.makeText(getApplicationContext(), "No Data Found..!", Toast.LENGTH_SHORT).show();

                                 }
                             } catch (JSONException e) {
                                 e.printStackTrace();
                                 pprogressDialog.cancel();
                             }
                         }
                     },
                     new Response.ErrorListener() {
                         @Override
                         public void onErrorResponse(VolleyError error) {
                             pprogressDialog.cancel();

                             if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                             } else if (error instanceof AuthFailureError) {

                             } else if (error instanceof ServerError) {

                             } else if (error instanceof NetworkError) {

                             } else if (error instanceof ParseError) {

                             }
                         }
                     }) {

                 @Override
                 public Map<String, String> getHeaders() throws AuthFailureError {
                     Map<String, String> headers = new HashMap<>();
                     headers.put("x-access-token", access_token);
                     headers.put("x-device-id", device_id);
                     headers.put("x-device-platform", "ANDROID");
                     Log.d("getBusiness_HEADER", "getBusiness_HEADER " + headers.toString());
                     return headers;
                 }


             };
             stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
             RequestQueue requestQueue = Volley.newRequestQueue(BusinessProfileActivity.this);
             requestQueue.add(stringRequest);
         }
         else
         {
             pprogressDialog.cancel();
             Toast.makeText(BusinessProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
             sponsorBusinessProfileDBData();
         }
    }

    private void sponsorBusinessProfileDBData()
    {
       // Toast.makeText(BusinessProfileActivity.this,"BDB",Toast.LENGTH_LONG).show();
        Log.d("BusinesDBDATA","BusinesDBDATA");
        List<String> f_Name = businessProfileDB.getOwnerName();
        Log.d("DATAddAAAA",""+f_Name);
        if(f_Name.size() > 0)
        {

            List<String> owner_name = businessProfileDB.getOwnerName();
            List<String> business_name = businessProfileDB.getBusniessName();
            List<String> business_type = businessProfileDB.getBusinessType();
            List<String> address = businessProfileDB.getAddress();
            List<String> pincode = businessProfileDB.getPincode();
            List<String> qr_code = businessProfileDB.getQRCodeImage();

            for (int i = 0; i < f_Name.size(); i++)
            {
                edt_name.setText(owner_name.get(0));
                edt_name.setFocusable(false);
                edt_name.setFocusableInTouchMode(false);

                edt_business_name.setText(business_name.get(0));
                edt_business_name.setFocusable(false);
                edt_business_name.setFocusableInTouchMode(false);

                edt_business_type.setText(business_type.get(0));
                edt_business_type.setFocusable(false);
                edt_business_type.setFocusableInTouchMode(false);

                edt_address.setText(address.get(0));
                edt_address.setFocusable(false);
                edt_address.setFocusableInTouchMode(false);

                edt_pincode.setText(pincode.get(0));
                edt_pincode.setFocusable(false);
                edt_pincode.setFocusableInTouchMode(false);


                Picasso.with(BusinessProfileActivity.this)
                        .load(qr_code.get(0))
                        .placeholder(R.drawable.no_qrcode)
                        // .resize(60,60)
                        .into(qr_code_imagview);

            }

        }
        else
        {

        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == close)
        {
            finish();
        }
        if (view == qr_code_imagview)
        {

            ZoomAnimation zoomAnimation = new ZoomAnimation(BusinessProfileActivity.this);
            zoomAnimation.zoom(view, duration);


         /*   Intent fullintent=new Intent(BusinessProfileActivity.this,FullImage.class);
            fullintent.putExtra("HOLOPATH",qr_code);
            startActivity(fullintent);*/
        }
        if (view == edit_profile)
        {
            send_detail_button.setVisibility(View.VISIBLE);
            edt_name.setFocusable(true);
            edt_name.setFocusableInTouchMode(true);
            edt_business_name.setFocusable(true);
            edt_business_name.setFocusableInTouchMode(true);
            edt_business_type.setFocusable(true);
            edt_business_type.setFocusableInTouchMode(true);
            edt_address.setFocusable(true);
            edt_address.setFocusableInTouchMode(true);
            edt_pincode.setFocusable(true);
            edt_pincode.setFocusableInTouchMode(true);
        }

        if (view == send_detail_button)
        {
            checkInternet = NetworkChecking.isConnected(BusinessProfileActivity.this);
            if (checkInternet) {
                if (validate())
                {

                    name = edt_name.getText().toString().trim();
                    business_name = edt_business_name.getText().toString().trim();
                    business_type = edt_business_type.getText().toString().trim();
                    address = edt_address.getText().toString().trim();
                    pincode = edt_pincode.getText().toString().trim();

                    Log.d("sendbusinessurl", AppUrls.BASE_URL + AppUrls.BUSINESS_PROFILE_GET+"?user_id="+user_id);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.BUSINESS_PROFILE_GET+"?user_id="+user_id,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    pprogressDialog.dismiss();
                                    Log.d("sendBusinessRESP", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("response_code");
                                        if (successResponceCode.equals("10100"))
                                        {
                                            pprogressDialog.dismiss();
                                            edit_profile.setVisibility(View.GONE);
                                            Toast.makeText(getApplicationContext(), "Business Detail Saved Succefully..", Toast.LENGTH_SHORT).show();
                                            getBusinessData();
                                           /* edt_name.setText("");
                                            edt_business_name.setText("");
                                            edt_business_type.setText("");
                                            edt_address.setText("");
                                            edt_pincode.setText("");*/

                                        }
                                        if (successResponceCode.equals("10200")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pprogressDialog.cancel();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    pprogressDialog.cancel();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", access_token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("sendBusiness_HEADER", "sendBusiness_HEADER " + headers.toString());
                            return headers;
                        }


                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);

                            params.put("owner_name", name);
                            params.put("business_name", business_name);
                            params.put("business_type", business_type);
                            params.put("address", address);
                            params.put("pincode", pincode);
                            Log.d("business_PARAM:", "business_PARAM" + params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(BusinessProfileActivity.this);
                    requestQueue.add(stringRequest);
                }

            } else {
                pprogressDialog.cancel();
                Toast.makeText(BusinessProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }



    private boolean validate() {

        boolean result = true;

       /* TextInputLayout ti_name, ti_business, ti_address, ti_pincode,ti_business_type;
        EditText edt_name, edt_business_name, edt_address, edt_pincode,edt_business_type;*/



        String username = edt_name.getText().toString().trim();
        if (username.isEmpty()) {
            ti_name.setError("Invalid Username");
            result = false;
        }else
        {
            ti_name.setErrorEnabled(false);
        }

        String business_name = edt_business_name.getText().toString().trim();
        if (business_name.isEmpty()) {
            ti_business.setError("Invalid Business Name");
            result = false;
        }else
        {
            ti_business.setErrorEnabled(false);
        }
        String address = edt_address.getText().toString().trim();
        if (address.isEmpty()) {
            ti_address.setError("Invalid Address");
            result = false;
        }else
        {
            ti_address.setErrorEnabled(false);
        }
        String business_type = edt_business_type.getText().toString().trim();
        if (business_type.isEmpty()) {
            ti_business_type.setError("Invalid Business type");
            result = false;
        }else
        {
            ti_business_type.setErrorEnabled(false);
        }

        String pincode = edt_pincode.getText().toString().trim();
        if (pincode.isEmpty()|| pincode.length() < 6) {
            ti_pincode.setError("Invalid Pincode");
            result = false;
        }else {
            ti_pincode.setErrorEnabled(false);
        }

        return result;
    }



}
