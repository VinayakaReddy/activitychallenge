package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.HistoryLotteryCodesItemClickListener;
import in.activitychallenge.activitychallenge.models.LotteryCodesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class HistoryLotteryCodesActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView history_lotterycodes_recyclerview;
    ImageView close,no_data_image;
    LinearLayoutManager layoutManager;
    HistoryLotteryCodesAdapter historyLotteryCodesAdapter;
    ArrayList<LotteryCodesModel> historylotteryCodesModels = new ArrayList<LotteryCodesModel>();
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String user_id,token,device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_lottery_codes);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        no_data_image=(ImageView)findViewById(R.id.no_data_image);
        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);

        history_lotterycodes_recyclerview = (RecyclerView) findViewById(R.id.history_lotterycodes_recyclerview);
        historyLotteryCodesAdapter = new HistoryLotteryCodesAdapter(historylotteryCodesModels, HistoryLotteryCodesActivity.this, R.layout.row_history_lottery_codes);
        layoutManager = new LinearLayoutManager(this);
        history_lotterycodes_recyclerview.setNestedScrollingEnabled(false);
        history_lotterycodes_recyclerview.setLayoutManager(layoutManager);

        getLotteryCodesHistory();

    }

    private void getLotteryCodesHistory()
    {//http://192.168.1.61:8090/api/v1/user/lottery/my_applied_lottery?user_id=5a695f6f54b2ad18d09495c0
        checkInternet = NetworkChecking.isConnected(HistoryLotteryCodesActivity.this);
        if (checkInternet) {

            Log.d("LOTTHISCODEURL", AppUrls.BASE_URL + AppUrls.HISTORY_LOTTERY_CODES+"?user_id="+user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.HISTORY_LOTTERY_CODES+"?user_id="+user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("TTTTTT", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100"))
                                {
                                    progressDialog.dismiss();

                                    JSONArray jarray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jdataobj = jarray.getJSONObject(i);
                                        LotteryCodesModel rhm = new LotteryCodesModel();

                                         rhm.setLotid(jdataobj.getString("id"));
                                         rhm.setDates(jdataobj.getString("applied_on"));
                                         rhm.setLottery_codes(jdataobj.getString("codes"));
                                        historylotteryCodesModels.add(rhm);
                                    }
                                    history_lotterycodes_recyclerview.setAdapter(historyLotteryCodesAdapter);
                                }
                                if (response_code.equals("10200"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(HistoryLotteryCodesActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300"))
                                {
                                    progressDialog.dismiss();
                                    no_data_image.setVisibility(View.VISIBLE);
                                  Toast.makeText(HistoryLotteryCodesActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryLotteryCodesActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(HistoryLotteryCodesActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        if(view==close)
        {
            finish();
        }
    }

    public class HistoryLotteryCodesAdapter extends RecyclerView.Adapter<LotteryCodesHolder> {

        public ArrayList<LotteryCodesModel> hislotteryCodesModels;
        HistoryLotteryCodesActivity context;
        LayoutInflater li;
        int resource;
        private boolean checkInternet;
        ProgressDialog progressDialog;

        public HistoryLotteryCodesAdapter(ArrayList<LotteryCodesModel> hislotteryCodesModels, HistoryLotteryCodesActivity context, int resource) {
            this.hislotteryCodesModels = hislotteryCodesModels;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public LotteryCodesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            LotteryCodesHolder slh = new LotteryCodesHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final LotteryCodesHolder holder, final int position) {

            holder.history_lottery_code_txt.setText(hislotteryCodesModels.get(position).getLottery_codes());

          String timedate=parseDateToddMMyyyy(hislotteryCodesModels.get(position).getDates());
           holder.date_lottery_code_txt.setText(Html.fromHtml(timedate));



            holder.setItemClickListener(new HistoryLotteryCodesItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.hislotteryCodesModels.size();
        }
    }

    public class LotteryCodesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView history_lottery_code_txt,date_lottery_code_txt;
        HistoryLotteryCodesItemClickListener historylotteryCodesItemClickListener;

        public LotteryCodesHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            history_lottery_code_txt = (TextView) itemView.findViewById(R.id.history_lottery_code_txt);
            date_lottery_code_txt = (TextView) itemView.findViewById(R.id.date_lottery_code_txt);

        }

        @Override
        public void onClick(View view) {

            this.historylotteryCodesItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(HistoryLotteryCodesItemClickListener ic)
        {
            this.historylotteryCodesItemClickListener = ic;
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
