package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.DefaultDayViewAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.models.DailyStepsChallengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;

public class SpecialEventCalenderActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    ImageView close;
    Button next;

    String startdate,enddate,goal,activityname,activityStr;
    String locationName,lattitude,longitude,challenge_id,challengeAmount;
    int noofdays;
    JSONArray locationArr;
    CalendarPickerView calendarView;
    String specialevent_id;
    TextView noofdays_text,activity_name_text;
    AppCompatSpinner goal_spinner;
    String device_id, access_token, user_id,user_type;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    ArrayList<Object> challengesArr=new ArrayList<>();

    String challengeid,challenge_goal,evaluation_factor,evaluation_factor_unit,activityid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_event_calender);

        close = (ImageView) findViewById(R.id.close);
        next=(Button)findViewById(R.id.next);
        noofdays_text=(TextView)findViewById(R.id.special_no_of_days);
        goal_spinner=(AppCompatSpinner) findViewById(R.id.spinner_goal);
        activity_name_text=(TextView)findViewById(R.id.special_activity_name);


        close.setOnClickListener(this);
        next.setOnClickListener(this);
        goal_spinner.setOnItemSelectedListener(this);
        calendarView=findViewById(R.id.simpleCalendarView);

        startdate=getIntent().getStringExtra("startdate");

        activityStr=getIntent().getStringExtra("activity");
        enddate=getIntent().getStringExtra("enddate");
        noofdays=getIntent().getIntExtra("noofdays",0);
        specialevent_id=getIntent().getStringExtra("specialactivityid");

        locationName=getIntent().getStringExtra("locationname");
        lattitude=getIntent().getStringExtra("lattitude");
        longitude=getIntent().getStringExtra("longitude");
        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        goal=getIntent().getStringExtra("goal");
        activityname=getIntent().getStringExtra("activityname");
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        if(activityStr.equalsIgnoreCase("ChallengeRequestAdapter")){
            challengeAmount=getIntent().getStringExtra("challengeAmount");
            challenge_id=getIntent().getStringExtra("challenge_id");

        }

        noofdays_text.setText(""+noofdays);
        activity_name_text.setText(activityname);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
       /* long start=parseStartDate(startdate);
        long end=parseStartDate(enddate);
        calendarView.setMinDate(start);
        calendarView.setMaxDate(end);
        calendarView.dispatchSetSelected(false);
        calendarView.setSelectedDateVerticalBar(R.color.com_facebook_button_background_color);
*/
        Calendar c = Calendar.getInstance();

        c.setTime(parsestartDate(startdate));

        calendarView.setCustomDayView(new DefaultDayViewAdapter());
        calendarView.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        // 20 years, enough to show performance failure.
        calendarView.init(parsestartDate(startdate),parseendDate( parseDate(enddate)))
                .inMode(CalendarPickerView.SelectionMode.SINGLE).withSelectedDate(c.getTime());
        Calendar today = Calendar.getInstance();
        ArrayList<Date> datesar = new ArrayList<Date>();
        List<Date> dates = getDates(parseDate(startdate), parseDate(enddate));
        for(Date date:dates)
            datesar.add(date);
        calendarView.highlightDates(datesar);


        getspecialChallenges();

    }



    private void getspecialChallenges()
    {
        //AppUrls.BASE_URL + AppUrls.WEEKLYCHALLENGES + id + AppUrls.TYPE + "longrun";

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_CHALLENGES + specialevent_id + AppUrls.TYPE + "daily";
            Log.d("SPECIALCHALLENGESURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SPECIALCHALLENGES", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonobj = jsonArray.getJSONObject(0);
                                    String id = jsonobj.getString("id");
                                     activityid = jsonobj.getString("activity_id");
                                    String challenge_sub_logo = jsonobj.getString("challenge_sub_logo");
                                    JSONArray challenges_array = jsonobj.getJSONArray("challenges");
                                    Type stringStringMap = new TypeToken<ArrayList<Object>>() {
                                    }.getType();
                                    ArrayList<Object> tmpObj = new Gson().fromJson(String.valueOf(challenges_array), stringStringMap);
                                    challengesArr.clear();
                                    challengesArr.addAll(tmpObj);
                                    ArrayList<String> goalvalues=new ArrayList<>();
                                    if(challengesArr.size()>0){
                                        for(int i=0;i<challengesArr.size();i++){
                                            Map<String,Object> tmp= (Map<String, Object>) challengesArr.get(i);
                                            String factor_goalval= (String) tmp.get("factor_goal_val");
                                            String factor_eval_unit= (String) tmp.get("factor_eval_unit");
                                            String val=factor_goalval+" "+factor_eval_unit+"S";
                                            goalvalues.add(val);

                                        }
                                    }else{
                                        goalvalues.add(goal);
                                    }
                                    goal_spinner.setAdapter(new ArrayAdapter<String>(SpecialEventCalenderActivity.this, android.R.layout.simple_spinner_dropdown_item, goalvalues));

                                    if(challengesArr.size()>0){
                                        Map<String,Object> tmp= (Map<String, Object>) challengesArr.get(0);
                                        challengeid= (String) tmp.get("id");
                                        challenge_goal=(String) tmp.get("factor_goal_val");
                                        evaluation_factor=(String) tmp.get("factor_eval");
                                        evaluation_factor_unit=(String) tmp.get("factor_eval_unit");
                                    }
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(SpecialEventCalenderActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
    @Override
    public void onClick(View view)
    {
        if(view==close) {
            finish();
        }
        if(view==next) {
            String[] datearr=startdate.split(" ");
            String datestr="";
            if(datearr[0]!=null && datearr[0].length()>0)
             datestr=parseDateToddMMyyyy(datearr[0]);
            if(activityStr.equalsIgnoreCase("ChallengeRequestAdapter")){
                Intent intent = new Intent(SpecialEventCalenderActivity.this, ChallengePaymentActivity.class);
                intent.putExtra("activity","ChallengeRequestAdapter");
                intent.putExtra("challengeAmount",challengeAmount);
                intent.putExtra("challenge_id",challenge_id);
                intent.putExtra("location_name",locationName);
                intent.putExtra("location_lat",lattitude);
                intent.putExtra("location_long",longitude);
                Log.d("Details",challengeAmount+"\n"+challenge_id+"\n"+locationName+"\n"+lattitude+"\n"+lattitude);
                startActivity(intent);
            }else {
                Intent intent = new Intent(SpecialEventCalenderActivity.this, ActivityPrice.class);
                intent.putExtra("activity_id", activityid);
                intent.putExtra("user_id", user_id);
                intent.putExtra("user_type", user_type);
                intent.putExtra("challenge_id", challengeid);
                intent.putExtra("challenge_goal", challenge_goal);
                intent.putExtra("evaluation_factor", evaluation_factor);
                intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                intent.putExtra("type", "DAILY");
                intent.putExtra("span", String.valueOf(noofdays));
                intent.putExtra("eventflag",1);
                intent.putExtra("date", datestr);
                intent.putExtra("location_name", locationName);
                intent.putExtra("location_lat", lattitude);
                intent.putExtra("location_lng", longitude);
                startActivity(intent);
            }

        }

    }

    public String parseDate(String str) {
           String[] datearr=str.split(" ");
        return datearr[0];
    }

    public Date parsestartDate(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    //get extra one date from given date
     public Date parseendDate(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date extradate=addDays(date,1);
        return extradate;
    }


    public static Date addDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    //change date format
    public String parseDateToddMMyyyy(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    //get dates between start and end dates
    private static List<Date> getDates(String dateString1, String dateString2)
    {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1 .parse(dateString1);
            date2 = df1 .parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    //get milliseconds from date
    public long getMills(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        Date date = null;
        long str = 0;

        try {
            date = inputFormat.parse(time);
            str=date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public long parseStartDate(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        Date date = null;
        long str = 0;

        try {
            date = inputFormat.parse(time);
            str=date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(challengesArr.size()>0){
            Map<String,Object> tmpObj= (Map<String, Object>) challengesArr.get(i);
            challengeid= (String) tmpObj.get("id");
            challenge_goal=(String) tmpObj.get("factor_goal_val");
            evaluation_factor=(String) tmpObj.get("factor_eval");
            evaluation_factor_unit=(String) tmpObj.get("factor_eval_unit");
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
