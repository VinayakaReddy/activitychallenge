package in.activitychallenge.activitychallenge.activities;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gjiazhe.panoramaimageview.GyroscopeObserver;
import com.gjiazhe.panoramaimageview.PanoramaImageView;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.GroupActivityAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.ActivityDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class GroupMainActivity extends AppCompatActivity implements View.OnClickListener {

    GroupActivityAdapter activityAdapter;
    ArrayList<ActivityModel> activitylist = new ArrayList<ActivityModel>();
    private RecyclerView activities_recyclerview;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    private FlowingDrawer mDrawer;
    private Boolean exit = false;
    private boolean checkInternet;
    SearchView activity_search;
    ActivityDB activityDB;
    CircleImageView user_profile_image;
    ImageView sponsor_iv, challenge_iv, mesage_iv, banner_image, voice_search, image_view,close;
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text, player_user_name, ranking_text, more;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    LinearLayout linear_home, linear_cards, linear_profile, linear_recent, linear_win_lost, linear_mychallenges, linear_my_group, linear_allmembers, linear_all_groups, Linear_following, linear_mypromotions, linear_me_as_sponsor,
            linear_earn_money, linear_refer_friend, linear_advertise, linear_report, linear_help, linear_terms_and_cond, linear_contact_us, linear_deactivate, linear_mysponsorings;
    private GyroscopeObserver gyroscopeObserver;
    UserSessionManager session;
    String user_id, token, device_id, user_type = "", user_id_user;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    EditText searchEditText;
    ////////////////////////////////////////
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    EditText current_location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_group);
        gps = new GPSTracker(GroupMainActivity.this);
        geocoder = new Geocoder(this, Locale.getDefault());
        session = new UserSessionManager(GroupMainActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_id_user = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        user_id = getIntent().getExtras().getString("user_id");
        user_type = getIntent().getExtras().getString("user_type");
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        gyroscopeObserver = new GyroscopeObserver();
        PanoramaImageView panoramaImageView = (PanoramaImageView) findViewById(R.id.panorama_image_view);
        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);
        activity_search = (SearchView) findViewById(R.id.activity_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        activity_search.clearFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText = (EditText) activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon = (ImageView) activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        searchEditText.setBackgroundColor(getResources().getColor(R.color.white));
        activities_recyclerview = (RecyclerView) findViewById(R.id.activities_recyclerview);
        activities_recyclerview.setHasFixedSize(true);
        activityAdapter = new GroupActivityAdapter(activitylist, GroupMainActivity.this, R.layout.row_activity, user_id, user_type);
        gridLayoutManager = new GridLayoutManager(this, 3);
        layoutManager = new LinearLayoutManager(this);
        activities_recyclerview.setLayoutManager(gridLayoutManager);
        activityDB = new ActivityDB(GroupMainActivity.this);
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        sponsor_iv = (ImageView) findViewById(R.id.sponsor_iv);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        challenge_iv = (ImageView) findViewById(R.id.challenge_iv);
        mesage_iv = (ImageView) findViewById(R.id.mesage_iv);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        voice_search = (ImageView) findViewById(R.id.voice_search);
        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        more = (TextView) findViewById(R.id.more);
        more.setOnClickListener(this);
        getAccessControl();
        getActivities();
        getAds();
        getCount();
        getValidateUser();
        getUserDetails();

        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                activityAdapter.getFilter().filter(query);
                return false;
            }
        });
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getApplicationContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
// check if GPS enabled
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //  Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        sendUserData(user_id, device_id, lat, lng, city, country, state, place, token);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);
            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
            //   lat_lng_change_text.setText("Your Location is - \nLat: " + ""+latitude + "\nLong: " + longitude);
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }

    }





    @Override
    public void onBackPressed() {

            finish();

    }

    private void getActivities() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            activitylist.clear();
            activityDB.emptyDBBucket();
            String url = AppUrls.BASE_URL + AppUrls.ACTIVITY;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    ContentValues values = new ContentValues();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        values.put(ActivityDB.ID, jsonObject1.getString("id"));
                                        values.put(ActivityDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                        values.put(ActivityDB.ACTIVITY_NO, jsonObject1.getString("activity_no"));
                                        values.put(ActivityDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                        values.put(ActivityDB.TOOLS_REQUIRED, jsonObject1.getString("tools_required"));
                                        values.put(ActivityDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        values.put(ActivityDB.MIN_VALUE, jsonObject1.getString("min_value"));
                                        values.put(ActivityDB.MIN_VALUE_UNIT, jsonObject1.getString("min_value_unit"));
                                        values.put(ActivityDB.DAILY_CHALLENGES, jsonObject1.getString("daily_challenges"));
                                        values.put(ActivityDB.WEEKLY_CHALLENGES, jsonObject1.getString("weekly_challenges"));
                                        values.put(ActivityDB.LONGRUN_CHALLENGES, jsonObject1.getString("longrun_challenges"));
                                        activityDB.addFriendsList(values);

                                       /* ActivityModel am = new ActivityModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setActivity_name(jsonObject1.getString("activity_name"));
                                        am.setActivity_no(jsonObject1.getString("activity_no"));
                                        am.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                        am.setTools_required(jsonObject1.getString("tools_required"));
                                        am.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        activitylist.add(am);
                                        Log.d("fjbvsdfkvsdf",activitylist.toString());*/

                                    }
                                    activityList();
                                    // activities_recyclerview.setAdapter(activityAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            activityList();
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void activityList() {

        activitylist.clear();
        List<String> activityName = activityDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> id = activityDB.getId();
            List<String> activity_name = activityDB.getActivityName();
            List<String> activity_no = activityDB.getActivityNo();
            List<String> evalution_factor = activityDB.getEvaluationFactor();
            List<String> tools_required = activityDB.getToolsRequired();
            List<String> activity_image = activityDB.getActivity_image();
            List<String> min_value = activityDB.getMin_value();
            List<String> min_value_unit = activityDB.getMinimum_valu_unit();
            List<String> daily_challenges = activityDB.getDaily_challenges();
            List<String> weekly_challenges = activityDB.getWeekly_challenges();
            List<String> longrun_challenges = activityDB.getLongRun_challenges();
            for (int i = 0; i < activityName.size(); i++) {

                ActivityModel sm = new ActivityModel();
                sm.setId(id.get(i));
                sm.setActivity_name(activity_name.get(i));
                sm.setActivity_no(activity_no.get(i));
                sm.setEvaluation_factor(evalution_factor.get(i));
                sm.setTools_required(tools_required.get(i));
                sm.setActivity_image(activity_image.get(i));
                sm.setMin_value(min_value.get(i));
                sm.setMin_value_unit(min_value_unit.get(i));
                sm.setDaily_challenges(daily_challenges.get(i));
                sm.setWeekly_challenges(weekly_challenges.get(i));
                sm.setLongrun_challenges(longrun_challenges.get(i));
                activitylist.add(sm);
            }
            activities_recyclerview.setAdapter(activityAdapter);
        } else {
        }
    }

    private void getAds() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                int count = jsonObject.getInt("count");
                                if (count > 1) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.GONE);
                                }
                                String status = jsonObject.getString("response_code");

                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String image = AppUrls.BASE_IMAGE_URL + jobj.getString("banner_path");
                                    Picasso.with(GroupMainActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(banner_image);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);
                                }
                                if (status.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getAccessControl() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.GET_ACCESS_CONTROL + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    JSONObject ignore_entities = jsonArray.getJSONObject("ignore_entities");
                                    String scratch_card = ignore_entities.getString("scratch_card");
                                    String lottery = ignore_entities.getString("lottery");
                                    String promotion = ignore_entities.getString("promotion");
                                    String sponsoring = ignore_entities.getString("sponsoring");
                                    String challenge_user_group_to_admin = ignore_entities.getString("challenge_user_group_to_admin");
                                    String challenge_group_to_group = ignore_entities.getString("challenge_group_to_group");
                                    String challenge_user_to_user = ignore_entities.getString("challenge_user_to_user");
                                    if (scratch_card.equals("1") || lottery.equals("1")) {
                                        linear_earn_money.setVisibility(View.GONE);
                                    }
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getValidateUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CHECK_USER + user_id + AppUrls.USER_TYPE + user_type;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String is_mobile_verified = jobj.getString("is_mobile_verified");
                                    String is_email_verified = jobj.getString("is_email_verified");
                                    String is_active = jobj.getString("is_active");
                                    String has_payment_done = jobj.getString("has_payment_done");
                                    String has_completed_trial_period = jobj.getString("has_completed_trial_period");
                                    String trial_period_remaining_days = jobj.getString("trial_period_remaining_days");
                                    if (is_active.equals("1")) {
                                        Toast.makeText(GroupMainActivity.this, "User is Active", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(GroupMainActivity.this, "User is not Active", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUserDetails() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.USER_BASIC_DETAILS + user_id_user + AppUrls.USER_TYPE + "USER";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("OOOACTIVITYRESPPPP", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String name = jobj.getString("first_name") + " " + jobj.getString("last_name");
                                    String profile_pic = AppUrls.BASE_IMAGE_URL + jobj.getString("profile_pic");
                                    String overall_rank = jobj.getString("overall_rank");
                                    String email = jobj.getString("email");

                                    Log.d("xfkldfbng", profile_pic);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

        if (view == linear_home) {
            Intent profile = new Intent(GroupMainActivity.this, GroupMainActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
        }
        if (view == linear_cards) {
            Intent profile = new Intent(GroupMainActivity.this, SavedCardDetailsActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
        }
        if (view == linear_profile) {
            Intent profile = new Intent(GroupMainActivity.this, MyProfileActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
        }
        if (view == linear_recent) {
            Intent profile = new Intent(GroupMainActivity.this, RecentHistoryActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
        }
        if (view == linear_win_lost) {
            Intent winlost = new Intent(GroupMainActivity.this, WinLossActivity.class);
            startActivity(winlost);
            mDrawer.closeMenu();
        }
        if (view == linear_mychallenges) {
            Intent challeng = new Intent(GroupMainActivity.this, MyChallengesActivity.class);
            startActivity(challeng);
            mDrawer.closeMenu();
        }
        if (view == linear_my_group) {
            Intent group = new Intent(GroupMainActivity.this, MyGroupActivity.class);
            startActivity(group);
            mDrawer.closeMenu();
        }
        if (view == linear_allmembers) {
            Intent allmembers = new Intent(GroupMainActivity.this, AllMembersActivity.class);
            startActivity(allmembers);
            mDrawer.closeMenu();
        }
        if (view == linear_all_groups) {
            Intent all_groups = new Intent(GroupMainActivity.this, AllGroupsActivity.class);
            startActivity(all_groups);
            mDrawer.closeMenu();
        }
        if (view == Linear_following) {
            Intent following = new Intent(GroupMainActivity.this, MyFollowing.class);
            startActivity(following);
            mDrawer.closeMenu();
        }
        if (view == linear_mypromotions) {
            Intent mypromotions = new Intent(GroupMainActivity.this, MyPromotionActivity.class);
            startActivity(mypromotions);
            mDrawer.closeMenu();
        }
        if (view == linear_me_as_sponsor) {
            Intent me_as_sponsor = new Intent(GroupMainActivity.this, MeAsSponsorActivity.class);
            startActivity(me_as_sponsor);
            mDrawer.closeMenu();
        }
        if (view == linear_earn_money) {
            Intent earn_money = new Intent(GroupMainActivity.this, EarnActivity.class);
            startActivity(earn_money);
            mDrawer.closeMenu();
        }
        if (view == linear_refer_friend) {
            Intent refer_friend = new Intent(GroupMainActivity.this, ReferFriendActivity.class);
            startActivity(refer_friend);
            mDrawer.closeMenu();
        }

        if (view == linear_advertise) {

            Intent advertise = new Intent(GroupMainActivity.this, SportsPromoVideolist.class);
            startActivity(advertise);
            mDrawer.closeMenu();
        }
        if (view == linear_report) {
            Intent report = new Intent(GroupMainActivity.this, ReportUsActivity.class);
            startActivity(report);
            mDrawer.closeMenu();
        }
        if (view == linear_help) {
            Intent help = new Intent(GroupMainActivity.this, HelpActivity.class);
            startActivity(help);
            mDrawer.closeMenu();
        }
        if (view == linear_terms_and_cond) {
            Intent terms_and_cond = new Intent(GroupMainActivity.this, TermsConditionActivity.class);
            startActivity(terms_and_cond);
            mDrawer.closeMenu();
        }
        if (view == linear_contact_us) {
            Intent contact_us = new Intent(GroupMainActivity.this, ContactUsActivity.class);
            startActivity(contact_us);
            mDrawer.closeMenu();
        }
        if (view == linear_mysponsorings) {
            Intent deactivate = new Intent(GroupMainActivity.this, MySponsorings.class);
            startActivity(deactivate);
            mDrawer.closeMenu();
        }
        if (view == linear_deactivate) {
            Intent deactivate = new Intent(GroupMainActivity.this, DeactivateAccountActivity.class);
            startActivity(deactivate);
            mDrawer.closeMenu();
        }
//////////////////////////////////////////////
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(GroupMainActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
            mDrawer.closeMenu();
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(GroupMainActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
            mDrawer.closeMenu();
        }
        if (view == linear_messges) {
            Intent msg = new Intent(GroupMainActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
            //mDrawer.closeMenu();
        }
        if (view == more) {
            Intent more = new Intent(GroupMainActivity.this, AllPromotionsActivity.class);
            startActivity(more);
            //mDrawer.closeMenu();
        }
        if (view == close) {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gyroscopeObserver.register(this);
        getUserDetails();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gyroscopeObserver.unregister();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                Intent intent = new Intent(GroupMainActivity.this, RunningChallengesActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    activityAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }

    public void sendUserData(final String user_id, final String device_id, final String lat, final String lng, final String city, final String country, final String state, final String place, final String token) {
        checkInternet = NetworkChecking.isConnected(GroupMainActivity.this);
        if (checkInternet) {


            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FEEDBACK);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CURRENT_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("lat", lat);
                    params.put("long", lng);
                    params.put("city", city);
                    params.put("country", country);
                    params.put("state", state);
                    params.put("place", place);
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(GroupMainActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(GroupMainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
}
