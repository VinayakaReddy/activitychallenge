package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ReportUsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    TextView report_h_butt;
    EditText report_message, report_title;
    TextView send_report_button;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    RadioGroup rb_group;
    RadioButton rb_feedback, rb_suggest, rb_complain, radioButton;
    String user_id, user_type, title, details, device_name, access_token, device_id, device_platform, radioValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_us);

        device_name = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
        Log.d("DEVICESSSSS", device_name);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("TOKENNNN", user_id);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        report_h_butt = (TextView) findViewById(R.id.report_h_butt);
        report_h_butt.setOnClickListener(this);
        send_report_button = (TextView) findViewById(R.id.send_report_button);
        send_report_button.setOnClickListener(this);
        report_message = (EditText) findViewById(R.id.report_message);
        report_title = (EditText) findViewById(R.id.report_title);
        rb_group = (RadioGroup) findViewById(R.id.rb_group);
        rb_feedback = (RadioButton) findViewById(R.id.rb_feedback);
        rb_suggest = (RadioButton) findViewById(R.id.rb_suggest);
        rb_complain = (RadioButton) findViewById(R.id.rb_complain);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }

        if (view == report_h_butt) {


            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent i = new Intent(ReportUsActivity.this, ReportHistory.class);
                startActivity(i);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }




        }
        if (view == send_report_button)
        {
            checkInternet = NetworkChecking.isConnected(ReportUsActivity.this);
            if (checkInternet) {
                if (validate()) {
                    int selectedId = rb_group.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(selectedId);
                    radioValue = radioButton.getText().toString();
                    Log.d("xdfsd", radioValue);
                    title = report_title.getText().toString().trim();
                    details = report_message.getText().toString().trim();
                    Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FEEDBACK);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FEEDBACK,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    pprogressDialog.dismiss();
                                    Log.d("REPORTRESP", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("response_code");
                                        if (successResponceCode.equals("10100")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(ReportUsActivity.this, "Report has Sent Successfully", Toast.LENGTH_SHORT).show();
                                            finish();
                                        }
                                        if (successResponceCode.equals("10200")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pprogressDialog.cancel();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    pprogressDialog.cancel();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", access_token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                            return headers;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("user_type", user_type);
                            params.put("type", radioValue);
                            params.put("title", title);
                            params.put("details", details);
                            params.put("device_name", device_name);
                            Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(ReportUsActivity.this);
                    requestQueue.add(stringRequest);
                }

            } else {
                pprogressDialog.cancel();
                Toast.makeText(ReportUsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean validate() {
        boolean result = true;
        String msg = report_message.getText().toString().trim();
        if (msg.isEmpty() || msg.equals("") || msg.equals(null)) {
            report_message.setError("Please Enter  Report Message");
            result = false;
        } else {
            report_message.setError(null);
        }
        String titl = report_title.getText().toString().trim();
        if (titl.isEmpty() || titl.equals("") || titl.equals(null)) {
            report_title.setError("Please Enter  Report Subject");
            result = false;
        } else {
            report_title.setError(null);
        }
        return result;
    }
}
