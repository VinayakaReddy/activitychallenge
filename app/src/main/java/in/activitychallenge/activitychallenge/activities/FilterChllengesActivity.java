package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class FilterChllengesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView apply_filter;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    Typeface typeface;
    String token, user_id, user_type, radiotype;
    RadioGroup filter_group;
    RadioButton radio_all, radio_win, radio_loss, radio_draw;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_chllenges);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        pprogressDialog = new ProgressDialog(FilterChllengesActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        filter_group = (RadioGroup) findViewById(R.id.filter_group);
        radio_all = (RadioButton) findViewById(R.id.radio_all);
        radio_all.setOnClickListener(this);
        radio_all.setTypeface(typeface);
        radio_win = (RadioButton) findViewById(R.id.radio_win);
        radio_win.setOnClickListener(this);
        radio_win.setTypeface(typeface);
        radio_loss = (RadioButton) findViewById(R.id.radio_loss);
        radio_loss.setOnClickListener(this);
        radio_loss.setTypeface(typeface);
        radio_draw = (RadioButton) findViewById(R.id.radio_draw);
        radio_draw.setOnClickListener(this);
        radio_draw.setTypeface(typeface);
        apply_filter = (TextView) findViewById(R.id.apply_filter);
        apply_filter.setOnClickListener(this);
        int checkedRadioButtonID = filter_group.getCheckedRadioButtonId();
        filter_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case -1:
                        break;
                    case R.id.radio_all:
                        radiotype = "ALL";
                        break;
                    case R.id.radio_win:
                        radiotype = "WIN";
                        break;
                    case R.id.radio_loss:
                        radiotype = "LOSS";
                        break;
                    case R.id.radio_draw:
                        radiotype = "TIE";
                        break;
                    default:

                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            radiotype = "ALL";
            intent.putExtra("win_type", radiotype);
            setResult(2, intent);
            finish();
        }
        if (view == apply_filter) {
            Log.d("RADIOTYPE", radiotype);
            intent.putExtra("win_type", radiotype);
            setResult(2, intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        radiotype = "ALL";
        Log.d("RADIOTYPE", radiotype);
        intent.putExtra("win_type", radiotype);
        setResult(2, intent);
        finish();
        super.onBackPressed();
    }
}
