package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.RecentHistoryActivityAdapter;
import in.activitychallenge.activitychallenge.models.RecentHistoryModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class RecentHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,unav;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id, user_type;
    UserSessionManager session;
    RecyclerView recent_history_recycler;
    LinearLayoutManager lLayMan;
    RecentHistoryActivityAdapter adapter;
    ArrayList<RecentHistoryModel> recentHistoryModels = new ArrayList<RecentHistoryModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_history);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        recent_history_recycler = (RecyclerView) findViewById(R.id.recent_history_recycler);
        adapter = new RecentHistoryActivityAdapter(recentHistoryModels, RecentHistoryActivity.this, R.layout.row_recent_history);
        lLayMan = new LinearLayoutManager(this);
        recent_history_recycler.setNestedScrollingEnabled(false);
        recent_history_recycler.setLayoutManager(lLayMan);
        close = (ImageView) findViewById(R.id.close);
        unav = (ImageView) findViewById(R.id.unav);
        close.setOnClickListener(this);

        getRecentHistory();
    }

    private void getRecentHistory() {
        checkInternet = NetworkChecking.isConnected(RecentHistoryActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.RECENT_HISTORY + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("URL", AppUrls.BASE_URL + AppUrls.RECENT_HISTORY + user_id + "&user_type=" + user_type);
                            Log.d("RECENTHISTORY", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        RecentHistoryModel rhm = new RecentHistoryModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setUser_id(itemArray.getString("user_id"));
                                        rhm.setUser_type(itemArray.getString("user_type"));
                                        rhm.setTitle(itemArray.getString("title"));
                                        rhm.setTxn_type(itemArray.getString("txn_type"));
                                        rhm.setTxn_flag(itemArray.getString("txn_flag"));
                                        rhm.setTxn_status(itemArray.getString("txn_status"));
                                        rhm.setAmount(itemArray.getString("amount"));
                                        rhm.setCreated_on_txt(itemArray.getString("created_on_txt"));
                                        rhm.setCreated_on(itemArray.getString("created_on"));
                                        if (itemArray.has("for_span")) {
                                            rhm.setFor_span(itemArray.getString("for_span"));
                                        }
                                        rhm.setUpdated_on_txt(itemArray.getString("updated_on_txt"));
                                        rhm.setUpdated_on(itemArray.getString("updated_on"));
                                        recentHistoryModels.add(rhm);
                                    }
                                    recent_history_recycler.setAdapter(adapter);
                                }
                                if (response_code.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(RecentHistoryActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    unav.setVisibility(View.VISIBLE);
                                    Toast.makeText(RecentHistoryActivity.this, "No data Found", Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(RecentHistoryActivity.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(RecentHistoryActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            finish();
        }
    }
}
