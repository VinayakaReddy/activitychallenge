package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;


import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ImagePermissions;
import in.activitychallenge.activitychallenge.utilities.MyMultipartEntity;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AddToPromoteActivity extends AppCompatActivity implements View.OnClickListener {

    TextView photoErrorText;
    EditText nameEdt, amountEdt,descEdt;
    RadioGroup radioGrp;
    RadioButton weekly_radio, monthly_radio, radioButton;
    TextInputLayout nameTil, amountTil,descTil;
    ImageView imgEdt, imgUploadButt, close;
    TextView sendReqButt,fromDateEdt, toDateEdt;
    public int mYear, mMonth, mDay;
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "";
    Bitmap bitmap;
    String amount, time_span, radioValue, from_on, to_on, file;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id, user_type, nameS, amountS,descS, fromDateS, toDateS, action_type;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_promote);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
       // user_id = userDetails.get(UserSessionManager.USER_ID);
     //   user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("PROMOTION_SESS_DATA", user_id + "\n" + access_token + "\n" + device_id + "\n" + user_type);

        action_type = getIntent().getExtras().getString("action_type");

        if(action_type.equals("GROUPPROMOTE"))
        {
            user_id=getIntent().getExtras().getString("group_id");
            user_type=getIntent().getExtras().getString("user_type");
        }
        else
        {
            user_id=getIntent().getExtras().getString("user_id");
            user_type=getIntent().getExtras().getString("user_type");
        }


    //
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        radioGrp = (RadioGroup) findViewById(R.id.radioGrp);
        weekly_radio = (RadioButton) findViewById(R.id.weekly_radio);
        monthly_radio = (RadioButton) findViewById(R.id.monthly_radio);

        nameEdt = (EditText) findViewById(R.id.nameEdt);
        descEdt = (EditText) findViewById(R.id.descEdt);
        amountEdt = (EditText) findViewById(R.id.amountEdt);
        fromDateEdt = (TextView) findViewById(R.id.fromDateEdt);
        fromDateEdt.setOnClickListener(this);
        toDateEdt = (TextView) findViewById(R.id.toDateEdt);
        toDateEdt.setOnClickListener(this);

        photoErrorText = (TextView) findViewById(R.id.photoErrorText);
        nameTil = (TextInputLayout) findViewById(R.id.nameTil);
        amountTil = (TextInputLayout) findViewById(R.id.amountTil);
        descTil = (TextInputLayout) findViewById(R.id.descTil);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        imgEdt = (ImageView) findViewById(R.id.imgEdt);
        imgUploadButt = (ImageView) findViewById(R.id.imgUploadButt);
        imgUploadButt.setOnClickListener(this);

        sendReqButt = (TextView) findViewById(R.id.sendReqButt);
        sendReqButt.setOnClickListener(this);

    }

    private void addPromote() {
        if (getCreateGroupValidation()) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                String url = AppUrls.BASE_URL + AppUrls.ADD_PROMOTE;
                Log.d("ADDPROMOTE", url);
                Log.d("PATHHHHH", selectedImagePath);
                AddToPromoteActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                    }
                });
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);

                new AddToPromoteActivity.HttpUpload(this, selectedImagePath).execute();


            } else {
                Toast.makeText(AddToPromoteActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onClick(View v) {

        if (v == close) {
            finish();
        }

        if (v == imgUploadButt) {
            selectOption();
        }

        if (v == sendReqButt) {
            nameS = nameEdt.getText().toString().trim();
            amountS = amountEdt.getText().toString().trim();
            descS = descEdt.getText().toString().trim();
            fromDateS = fromDateEdt.getText().toString().trim();
            toDateS = toDateEdt.getText().toString().trim();

            int selectedId = radioGrp.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedId);
            radioValue = radioButton.getText().toString();

            addPromote();
            sendReqButt.setClickable(false);
        }

        if (v == fromDateEdt)
        {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {                              //strDay + "-" + strMonth + "-" + year
                        strDay = dayOfMonth + "";
                    }
                    fromDateEdt.setText(String.valueOf(year + "-" + strMonth + "-" + strDay));

                    fromDateEdt.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == toDateEdt) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {                              //strDay + "-" + strMonth + "-" + year
                        strDay = dayOfMonth + "";
                    }
                    toDateEdt.setText(String.valueOf(year + "-" + strMonth + "-" + strDay));

                    toDateEdt.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }

    private Boolean getCreateGroupValidation() {
        Boolean result = true;

        if (nameS.equals("") || nameS.length() < 3)
        {
            nameTil.setError("Enter Valid Name");
            result = false;
        }
        if (descS.equals("") )
        {
            descTil.setError("Enter Valid Description");
            result = false;
        }
        else if (amountS.equals("")) {
            amountTil.setError("Enter Valid Amount");
            result = false;
        } else if (!nameS.matches("^[\\p{L} .'-]+$")) {
            nameTil.setError("Special characters not allowed");
            result = false;
        }
        else if (toDateS.equals(""))
        {
            toDateEdt.setError("Choose To Date");
            result = false;
        }
        else if (fromDateS.equals("")) {
            fromDateEdt.setError("Choose From Date");
            result = false;
        } else {
            nameTil.setErrorEnabled(false);
            amountTil.setErrorEnabled(false);

        }

        return result;
    }

    private void selectOption() {
        final Dialog dialog = new Dialog(AddToPromoteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.group_camera_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button mCamerabtn = (Button) dialog.findViewById(R.id.cameradialogbtn);
        Button mGallerybtn = (Button) dialog.findViewById(R.id.gallerydialogbtn);
        Button btnCancel = (Button) dialog.findViewById(R.id.canceldialogbtn);
        final boolean result = ImagePermissions.checkPermission(AddToPromoteActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        mCamerabtn.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                if (result)

                    if (ContextCompat.checkSelfPermission(AddToPromoteActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(AddToPromoteActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(AddToPromoteActivity.this,
                                android.Manifest.permission.CAMERA)

                                != PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.shouldShowRequestPermissionRationale(AddToPromoteActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                ActivityCompat.requestPermissions(AddToPromoteActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);

                    }


                dialog.cancel();

            }

        });

        mGallerybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();

            }

        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }

        });

        dialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 0:

                if (resultCode == RESULT_OK) {

                    Log.d("PPP", outputFileUri.toString());

                    if (outputFileUri != null)
                    {

                        bitmap = decodeSampledBitmapFromUri(outputFileUri, imgEdt.getWidth(), imgEdt.getHeight());
                        if (bitmap == null)
                        {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();

                        } else
                            {
                            selectedImagePath = getRealPathFromURI(AddToPromoteActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            imgEdt.setImageBitmap(bitmap);
//                            uploadImages();
                        }

                    }

                }

                break;

            case 1:

                if (resultCode == RESULT_OK) {

                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, imgEdt.getWidth(), imgEdt.getHeight());


                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();

                    } else {

                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);

                        imgEdt.setImageBitmap(bitmap);
                        //uploadImages();
                    }
                }
                break;

            default:

                break;

        }

    }

    private void uploadImages() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            //String urllllll = AppUrls.BASE_URL + AppUrls.CREATE_GROUP;
            //Log.d("GROUPCREATIONURL:", urllllll);
            Log.d("PATHHHHH", selectedImagePath);

            AddToPromoteActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    //  if (progressDialog != null)
                    //   progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            new AddToPromoteActivity.HttpUpload(this, selectedImagePath).execute();

        } else {
            pprogressDialog.cancel();
            Toast.makeText(AddToPromoteActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = managedQuery(uri, projection, null, null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }

        }

    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        String filePath =getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // return filename;
       /* try {
// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }*/
        return bm;

       /* Bitmap bm = null;
        try {
// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return bm;*/

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(AddToPromoteActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(AddToPromoteActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public static void saveToPreferences(Context context, String key, Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(AddToPromoteActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(AddToPromoteActivity.this);
                    }
                });

        alertDialog.show();
    }


    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private String convertToBase64(String imagePath) {

        Bitmap bm = BitmapFactory.decodeFile(imagePath);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        return encodedImage;
    }

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;

        private HttpClient client;

        private ProgressDialog pd;
        private long totalSize;

        public String url = AppUrls.BASE_URL + AppUrls.ADD_PROMOTE;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);

            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                File file = new File(imgPath);

                Log.d("USERDETAIL", user_id + "//" + imgPath + "//" + file);
                //Create the POST object
                HttpPost post = new HttpPost(url);

                //Create the multipart entity object and add a progress listener
                //this is a our extended class so we can know the bytes that have been transfered
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                //Add the file to the content's body


                ContentBody cbFile = new FileBody(file, "image/*");
                entity.addPart("file", cbFile);
                entity.addPart("user_id", new StringBody(user_id));
                entity.addPart("user_type", new StringBody(user_type));
                entity.addPart("amount", new StringBody(amountS));
                entity.addPart("description", new StringBody(descS));
                entity.addPart("time_span", new StringBody(radioValue.toUpperCase()));
                entity.addPart("from_on", new StringBody(fromDateS));
                entity.addPart("to_on", new StringBody(toDateS));

                post.addHeader("x-access-token", access_token);
                post.addHeader("x-device-platform", device_id);
                post.addHeader("x-device-id", "ANDROID");

                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();

                //We add the entity to the post request
                post.setEntity(entity);

                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();

                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }

            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog

        }

        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(AddToPromoteActivity.this, "Promotion sent Successfully", Toast.LENGTH_SHORT).show();
/*
            Log.d("ADDPROMOTE", result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result.toString());
                String success = jsonObject.getString("success");
                String message = jsonObject.getString("message");
                String response_code = jsonObject.getString("response_code");

                if (response_code.equals("10100"))
                {
                    Toast.makeText(AddToPromoteActivity.this, "Promotion Success", Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    String ref_id = jsonObject1.getString("ref_id");
                    String promotion_id = jsonObject1.getString("promotion_id");
                    Intent intent = new Intent(AddToPromoteActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                if (response_code.equals("10200")) {
                    pprogressDialog.dismiss();
                    Toast.makeText(AddToPromoteActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                }
                //Dismiss progress dialog
                Toast.makeText(AddToPromoteActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                //  Intent intent = new Intent(AddToPromoteActivity.this, MyProfileActivity.class);
                //   startActivity(intent);

            }
            catch (JSONException e)
            {
              e.printStackTrace();
            }*/
        }
    }
}
