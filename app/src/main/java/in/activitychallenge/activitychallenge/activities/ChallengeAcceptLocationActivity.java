package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ChallengeAcceptLocationActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener,AdapterView.OnItemSelectedListener {
    private static final int PLACE_PICKER_REQUEST = 1;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    EditText current_location;
    Button next;
    TextView location_text;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id = "", email = "", mobile = "", payment_info = "", device_id = "", token = "";
    private ImageView image;
    private float currentDegree = 0f;
    SensorManager mSensorManager;
    String startdate, enddate,goal,activityname="",specialeventid="",selectedlocationName="";
    int noofdays;
    String event;
    JSONArray locationArr;
    AppCompatSpinner location_spinner;
    ArrayList<String> location_drop_list = new ArrayList<>();
    ArrayList<String> location_lattitude_list = new ArrayList<>();
    ArrayList<String> location_longitude_list = new ArrayList<>();
    TextView tvHeading, loc;
    LinearLayout current_location_layout, other_location_layout,pick_layout, spinner_layout;
    String send_lat = "", send_lng = "", send_city = "", send_area = "", send_pincode = "";
    String location_name = "", location_lat = "", location_lng = "",activity = "",challenge_id = "",challengeAmount = "";
    ImageView close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_accept_location);

        Bundle bundle = getIntent().getExtras();
        activity = bundle.getString("activity");
        challenge_id = bundle.getString("challenge_id");
        challengeAmount = bundle.getString("challengeAmount");
         event = getIntent().getStringExtra("event");
        if (event.equalsIgnoreCase("special")) {
            startdate = getIntent().getStringExtra("startdate");
            enddate = getIntent().getStringExtra("enddate");
            noofdays = getIntent().getIntExtra("noofdays", 0);
            goal=getIntent().getStringExtra("minvalue");
            activityname=getIntent().getStringExtra("activity_name");
            specialeventid=getIntent().getStringExtra("specialactivityid");
            selectedlocationName=getIntent().getStringExtra("locationName");
            String tmparr = getIntent().getStringExtra("location");
            try {
                locationArr = new JSONArray(tmparr);
                location_drop_list.clear();
                if (locationArr.length() > 0) {
                    for (int i = 0; i < locationArr.length(); i++) {

                        JSONObject jsonObject = (JSONObject) locationArr.get(i);
                        String address = (String) jsonObject.get("address");
                        String lattitude= String.valueOf( jsonObject.get("latitude"));
                        String longtitude=  String.valueOf( jsonObject.get("latitude"));

                        location_drop_list.add(address);
                        location_lattitude_list.add(lattitude);
                        location_longitude_list.add(longtitude);

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        location_text = (TextView) findViewById(R.id.location_text);
        pick_layout = (LinearLayout) findViewById(R.id.pick_layout);
        spinner_layout = (LinearLayout) findViewById(R.id.spinner_layout);
        location_spinner = (AppCompatSpinner) findViewById(R.id.spinner_location);
        current_location_layout = (LinearLayout) findViewById(R.id.current_location_layout);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        image = (ImageView) findViewById(R.id.imageViewCompass);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        loc = (TextView) findViewById(R.id.loc);
        current_location_layout = (LinearLayout) findViewById(R.id.current_location_layout);
        other_location_layout = (LinearLayout) findViewById(R.id.other_location_layout);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        session = new UserSessionManager(ChallengeAcceptLocationActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        email = userDetails.get(UserSessionManager.USER_EMAIL);
        mobile = userDetails.get(UserSessionManager.MOBILE_NO);
        payment_info = userDetails.get(UserSessionManager.PAYMENT_STATUS);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("dgafd", user_id + "////" + mobile + "////" + payment_info + "////" + email);
        current_location = (EditText) findViewById(R.id.current_location);
        next = (Button) findViewById(R.id.next);
        gps = new GPSTracker(ChallengeAcceptLocationActivity.this);
        geocoder = new Geocoder(this, Locale.getDefault());
        if (event.equalsIgnoreCase("special")) {
            pick_layout.setVisibility(View.GONE);
            spinner_layout.setVisibility(View.VISIBLE);
            if (location_drop_list.size() > 1)
                location_text.setVisibility(View.VISIBLE);
            else
                location_text.setVisibility(View.GONE);
            location_name = location_drop_list.get(0);
            location_lat=location_longitude_list.get(0);
            location_lng=location_longitude_list.get(0);
            ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, location_drop_list);
            location_spinner.setAdapter(adapter);
           int selectedPos = adapter.getPosition(selectedlocationName);
            location_spinner.setSelection(selectedPos);
            location_spinner.setOnItemSelectedListener(this);


        } else {
            pick_layout.setVisibility(View.VISIBLE);
            spinner_layout.setVisibility(View.GONE);
        }
        current_location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    Intent intent = intentBuilder.build(ChallengeAcceptLocationActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        other_location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ChallengeAcceptLocationActivity.this);
                dialog.setContentView(R.layout.custom_dialog_layout);
                dialog.setTitle("Custom Dialog");
                final EditText text = (EditText) dialog.findViewById(R.id.textDialog);
                dialog.show();
                Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
                declineButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (text.getText().toString().length() > 6) {
                            dialog.dismiss();
                            loc.setVisibility(View.VISIBLE);
                            loc.setText("Current Location");
                            current_location.setVisibility(View.VISIBLE);
                            current_location.setText(text.getText().toString());
                            location_name = current_location.getText().toString().trim();
                        } else {
                            Toast.makeText(ChallengeAcceptLocationActivity.this, "Please provide correct Location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        // check if GPS enabled
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //   Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);

            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                location_name = current_location.getText().toString();
                location_lat = lat;
                location_lng = lng;
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!location_name.equals("")) {
                    if(event.equalsIgnoreCase("special")){
                        Intent it=new Intent(ChallengeAcceptLocationActivity.this,SpecialEventCalenderActivity.class);
                        it.putExtra("activity",activity);
                        it.putExtra("startdate",startdate);
                        it.putExtra("enddate",enddate);
                        it.putExtra("noofdays",noofdays);
                        it.putExtra("locationname",location_name);
                        it.putExtra("lattitude",location_lat);
                        it.putExtra("longitude",location_lng);
                        it.putExtra("goal",goal);
                        it.putExtra("challengeAmount",challengeAmount);
                        it.putExtra("challenge_id",challenge_id);
                        it.putExtra("activityname",activityname);
                        it.putExtra("specialactivityid",specialeventid);
                        startActivity(it);
                    }else{
                        Intent intent = new Intent(ChallengeAcceptLocationActivity.this, ChallengePaymentActivity.class);
                        intent.putExtra("activity","ChallengeRequestAdapter");
                        intent.putExtra("challengeAmount",challengeAmount);
                        intent.putExtra("challenge_id",challenge_id);
                        intent.putExtra("location_name",location_name);
                        intent.putExtra("location_lat",location_lat);
                        intent.putExtra("location_long",location_lng);
                        Log.d("CCCCCCCC",challengeAmount+"\n"+challenge_id+"\n"+location_name+"\n"+location_lat+"\n"+location_lng);
                        startActivity(intent);
                    }



                } else {
                    Toast.makeText(ChallengeAcceptLocationActivity.this, "please provide the location", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(ChallengeAcceptLocationActivity.this, data);
            String stringlat = place.getLatLng().toString();
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            final LatLng latlng = place.getLatLng();
            double lat_value = latlng.latitude;
            String lat = String.valueOf(lat_value);
            double lng_value = latlng.longitude;
            String lng = String.valueOf(lng_value);
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
          //  Toast.makeText(ChallengeAcceptLocationActivity.this, "" + name + " - " + address, Toast.LENGTH_SHORT).show();
            Log.d("map_selected_adderss", "" + name + " - " + address);
            loc.setVisibility(View.VISIBLE);
            loc.setText("Current Location");
            current_location.setVisibility(View.VISIBLE);
            current_location.setText("" + name + " - " + address);
            location_name = current_location.getText().toString();
            location_lat = lat;
            location_lng = lng;

        } else {
            // super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        float degree = Math.round(sensorEvent.values[0]);
        tvHeading.setText("Heading: " + Float.toString(degree) + " degrees");
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        ra.setDuration(210);
        ra.setFillAfter(true);
        image.startAnimation(ra);
        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        location_name = location_drop_list.get(i);
        location_lat=location_longitude_list.get(i);
        location_lng=location_longitude_list.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
