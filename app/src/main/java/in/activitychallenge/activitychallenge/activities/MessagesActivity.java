package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MessagesAdapter;
import in.activitychallenge.activitychallenge.models.MessagesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MessagesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    Typeface typeface, typeface_bold;
    RecyclerView recycler_messages;
    MessagesAdapter mesageAdapter;
    ArrayList<MessagesModel> msgModalList = new ArrayList<MessagesModel>();
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    String user_id, user_type, token, device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(MessagesActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);
        pprogressDialog = new ProgressDialog(MessagesActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        recycler_messages = (RecyclerView) findViewById(R.id.recycler_messages);
        layoutManager = new LinearLayoutManager(this);
        recycler_messages.setNestedScrollingEnabled(false);
        recycler_messages.setLayoutManager(layoutManager);

        getMessages();
    }

    public void getMessages() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("MSGURL", AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_MESSAGE + "?entity_id=" + user_id + "&entity_type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_MESSAGE + "?entity_id=" + user_id + "&entity_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("MESSAGRESP", response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    pprogressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        MessagesModel msg = new MessagesModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        msg.setFrom_id(jsonObject1.getString("from_id"));
                                        msg.setFrom_name(jsonObject1.getString("from_name"));
                                        msg.setFrom_user_type(jsonObject1.getString("from_user_type"));
                                        msg.setMsg(jsonObject1.getString("msg"));
                                        msg.setIs_read(jsonObject1.getString("is_read"));
                                        msg.setSent_on_txt(jsonObject1.getString("sent_on_txt"));
                                        msg.setSent_on(jsonObject1.getString("sent_on"));
                                        msg.setProfile_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("profile_pic"));
                                        msgModalList.add(msg);
                                    }
                                    recycler_messages.setAdapter(mesageAdapter);
                                }
                                if (responceCode.equals("10200")) {
                                    pprogressDialog.cancel();
                                    Toast.makeText(MessagesActivity.this, "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {
                                    Toast.makeText(MessagesActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                pprogressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MessagesActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
