package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.Config;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    ImageView login_btn, facebook_img, google_img;
    RadioGroup login_radio_group;
    RadioButton radio_buttn_user_login, radio_buttn_sponser_login, usertypeStatus;
    TextInputLayout email_til, password_til;
    EditText email_edt, pasword_edt;
    TextView forget_txt, login_txt, sign_in_txt;
    String user_id, user_type, device_id, country_code, mobile_no, send_email, send_pasword;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    Typeface typeface, typeface2;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 420;
    private static final String TAG = LoginActivity.class.getSimpleName();
    CallbackManager callbackManager;
    String sendFBAccessToken, sendFBUserID, token = "", verified_data, fb_user_id_data, email_data, name_data, gender_data, ref_txn_id;
    String nm = "", lnm = "";
    private Boolean exit = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        Bundle bundle = getIntent().getExtras();
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = bundle.getString("USERID");//userDetails.get(UserSessionManager.USER_ID);
        // user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_type = "USER";
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        country_code = userDetails.get(UserSessionManager.COUNTRY_CODE);
        mobile_no = userDetails.get(UserSessionManager.MOBILE_NO);
        Log.d("DEVICE", device_id + "" + country_code + "" + mobile_no+"////"+user_id);
        pprogressDialog = new ProgressDialog(LoginActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        forget_txt = (TextView) findViewById(R.id.forget_txt);
        forget_txt.setTypeface(typeface);
        forget_txt.setOnClickListener(this);
        login_txt = (TextView) findViewById(R.id.login_txt);
        login_txt.setTypeface(typeface2);
        sign_in_txt = (TextView) findViewById(R.id.sign_in_txt);
        sign_in_txt.setTypeface(typeface);
        sign_in_txt.setOnClickListener(this);
        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setTypeface(typeface);
        pasword_edt = (EditText) findViewById(R.id.pasword_edt);
        pasword_edt.setTypeface(typeface);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        email_til.setTypeface(typeface);
        password_til = (TextInputLayout) findViewById(R.id.password_til);
        password_til.setTypeface(typeface);
        login_btn = (ImageView) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(this);
        facebook_img = (ImageView) findViewById(R.id.facebook_img);
        facebook_img.setOnClickListener(this);
        google_img = (ImageView) findViewById(R.id.google_img);
        google_img.setOnClickListener(this);
        login_radio_group = (RadioGroup) findViewById(R.id.login_radio_group);
        radio_buttn_user_login = (RadioButton) findViewById(R.id.radio_buttn_user_login);
        radio_buttn_user_login.setOnClickListener(this);
        radio_buttn_user_login.setTypeface(typeface);
        radio_buttn_sponser_login = (RadioButton) findViewById(R.id.radio_buttn_sponser_login);
        radio_buttn_sponser_login.setOnClickListener(this);
        radio_buttn_sponser_login.setTypeface(typeface);
        login_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton rb = (RadioButton) findViewById(checkedId);
                if (checkedId == R.id.radio_buttn_user_login) {
                    user_type = "USER";

                } else if (checkedId == R.id.radio_buttn_sponser_login) {
                    user_type = "SPONSOR";
                }
            }
        });

        initializeGPlusSettings();
        initializeFacebookSettings();
        try
        {
            PackageInfo info = getPackageManager().getPackageInfo("in.activitychallenge.activitychallenge", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//5wI9pDN/WQ0ALdS/+TqIPIWhtt4=
            }
        } catch (PackageManager.NameNotFoundException e)
        {

        } catch (NoSuchAlgorithmException e) {

        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREFERENCE), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        Log.d("TOKEVELUE", token);
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult)
                    {

                        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                JSONObject json = response.getJSONObject();
                                Log.d("LOGFBBB", json.toString());
                                if (json != null)
                                {
                                    try {
                                        sendFBAccessToken = AccessToken.getCurrentAccessToken().getToken();
                                        AccessToken token1 = AccessToken.getCurrentAccessToken();
                                        if (token1 != null) {
                                            Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());
                                        }
                                      //  Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());

                                      //  verified_data = json.getString("verified");
                                        fb_user_id_data = json.getString("id");
                                        sendFBUserID = fb_user_id_data;
                                        email_data = json.getString("email");
                                        name_data = json.getString("name");
                                      //  gender_data = json.getString("gender");
                                        String profile_ptah = "http://graph.facebook.com/" + fb_user_id_data + "/picture?type=large";
                                        Log.d("FACEBOOKPROFILE", verified_data + "," + fb_user_id_data + "," + sendFBUserID + ", " + email_data + ", " + email_data + ", " + gender_data + ",," + profile_ptah);
                                       /* userSessionManager.createUserLoginSession(AccessToken.getCurrentAccessToken().getToken(), fb_user_id_data, name_data, "", email_data, "", "facebook");
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                       */

                                        // registerSocialDataFb(fb_user_id_data,name_data, email_data,profile_ptah, "Facebook");
                                        //     String nm="",lnm="";

                                        if (name_data.split("\\w+").length > 1) {

                                            lnm = name_data.substring(name_data.lastIndexOf(" ") + 1);
                                            nm = name_data.substring(0, name_data.lastIndexOf(' '));
                                        } else {
                                            //  firstName = name;
                                        }


                                        Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                                        i.putExtra("USERTYPE", user_type);
                                        i.putExtra("USERID", user_id);
                                        i.putExtra("user_data_id", fb_user_id_data);
                                        i.putExtra("name", nm);
                                        i.putExtra("lastname", lnm);
                                        i.putExtra("email", email_data);
                                        i.putExtra("gender", gender_data);
                                        i.putExtra("provider", "Facebook");
                                        i.putExtra("LOGIN", "Facebook");

                                        Log.d("FBBB", user_type + "//" + nm + "//" + lnm + "//" + fb_user_id_data + "//" + email_data + "//" + gender_data+"///"+user_id);
                                        startActivity(i);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("EXCEPTION", exception.toString());
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void initializeGPlusSettings() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initializeFacebookSettings() {
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void registerSocialDataFb(final String fb_user_data, final String name_data, final String email_data, final String profile_ptah, final String facebook) {
        if (checkInternet) {
            pprogressDialog.show();
            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SOCIAL_AUTH,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("LOGINSOCIALFB", response);
                            try {

                                JSONObject jsonObject = new JSONObject(response);
//9030939662
                                Log.d("LOGINSOCIALRESPFB", response);

                                String editSuccessResponceCode = jsonObject.getString("response_code");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject2 = jsonObject.getJSONObject("data");
                                    Toast.makeText(LoginActivity.this, " Login Succefully", Toast.LENGTH_LONG).show();
                                    Log.d("JOBJFB", jsonObject2.toString());
                                    String token = jsonObject2.getString("token");
                                    String user_id = jsonObject2.getString("user_id");
                                    String user_type = jsonObject2.getString("user_type");
                                    String is_register = jsonObject2.getString("is_register");
                                    String provided_acc_info = jsonObject2.getString("provided_acc_info");

                                    if (provided_acc_info.equals("false")) {
                                        Intent iacc_page = new Intent(LoginActivity.this, AddMoneyToAccountActivity.class);
                                        startActivity(iacc_page);
                                    } else {
                                        Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                        imain.putExtra("USERTYPE", user_type);
                                        startActivity(imain);
                                    }

                                    String[] parts = token.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {
                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSFSFFB", decodedString);
                                        JSONObject jsonObject3 = new JSONObject(decodedString);
                                        Log.d("JSONDATAsFB", jsonObject2.toString());
                                        String username = jsonObject3.getString("name");
                                        String email = jsonObject3.getString("email");
                                        String mobile = jsonObject3.getString("mobile");
                                        String status = jsonObject3.getString("status");
                                        Log.d("PROFILE_PIC", profile_ptah);
                                        // userSessionManager.createUserLoginSession(token, username, email, mobile, status, profile_ptah,user_type);

                                       /* Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(imain);*/

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    /*  String user_id = jsonObject1.getString("id");
                                String user_name = jsonObject1.getString("name");
                                String mobile = jsonObject1.getString("mobile");
                                String email = jsonObject1.getString("email_address");
                                //String profile_status = jsonObject1.getString("profile_status");
                                String token = jsonObject1.getString("token");
                                userSessionManager.createUserLoginSession(token,user_id, user_name, mobile, email, "", social_type);
                                Toast.makeText(getApplicationContext(),"You Logged in Successfully...!",Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);*/
                                }


                            } catch (JSONException e) {
                                pprogressDialog.show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("provider", facebook);
                    params.put("identifier", fb_user_data);
                    params.put("user_type", user_type);
                    params.put("first_name", name_data);
                    params.put("mobile", mobile_no);
                    params.put("country_code", country_code);
                    params.put("device_id", device_id);
                    params.put("force_login", "0");
                    params.put("photo_url", profile_ptah);
                    Log.d("FBSENDSOCIALNETWA", params.toString());

                    return params;
                }
            };
            stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue2 = Volley.newRequestQueue(LoginActivity.this);
            requestQueue2.add(stringRequest2);
        } else {
            Toast.makeText(LoginActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }


    }


    @Override
    public void onClick(View view) {


        if (view == login_btn)
        {

            checkInternet = NetworkChecking.isConnected(LoginActivity.this);
            if (validate())
            {
                if (checkInternet)
                {
                    send_email = email_edt.getText().toString();
                    send_pasword = pasword_edt.getText().toString();
                 // check weather email or mobile
                       if(isValidMobile(send_email))
                       {
                            if(mobile_no.equals(send_email))
                            {

                                generalLogin();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Entered mobile no. is not matched with device register number..!", Toast.LENGTH_SHORT).show();
                            }
                       }
                       else
                       {
                           generalLogin();

                       }


                }
                else
                    {
                    pprogressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();

                   }
            }
        }

        if (view == sign_in_txt) {
            Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
            i.putExtra("USERTYPE", user_type);
            i.putExtra("USERID", user_id);
            i.putExtra("LOGIN", "Manual");
            //E7:02:3D:A4:33:7F:59:0D:00:2D:D4:BF:F9:3A:88:3C:85:A1:B6:DE
            startActivity(i);
        }

        if (view == forget_txt) {
            Intent i = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
            startActivity(i);
        }
        if (view == google_img) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
            } else {
                Toast.makeText(LoginActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == facebook_img) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
            } else {
                Toast.makeText(LoginActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void generalLogin()
    {
        Log.d("LOGINURL", AppUrls.BASE_URL + AppUrls.LOGIN);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>()
        {



            @Override
            public void onResponse(String response) {
                Log.d("LGOINRESP", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String successResponceCode = jsonObject.getString("response_code");
                    if (successResponceCode.equals("10100")) {
                        // getToken();
                        pprogressDialog.dismiss();
                        JSONObject jobj = jsonObject.getJSONObject("data");
                        Log.d("JOBJ", jobj.toString());
                        String token = jobj.getString("token");
                        String user_id = jobj.getString("user_id");
                        String user_type = jobj.getString("user_type");
                        String is_register = jobj.getString("is_register");
                        String provided_acc_info = jobj.getString("provided_acc_info");
                        Log.d("JWT", token + "//" + provided_acc_info);
                        String[] parts = token.split("\\.");
                        byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                        String decodedString = "";
                        try {
                            decodedString = new String(dataDec, "UTF-8");
                            Log.d("TOKENSFSF", decodedString);
                            JSONObject jsonObject2 = new JSONObject(decodedString);
                            Log.d("JSONDATAsfa", jsonObject2.toString());
                            String username = jsonObject2.getString("name");
                            String email = jsonObject2.getString("email");
                            String mobile = jsonObject2.getString("mobile");
                            String status = jsonObject2.getString("status");
                            String profile_pic = jsonObject2.getString("profile_pic");

                            if(mobile.equals(country_code+mobile_no))
                            {
                                Log.d("ffffff","ffffff");
                                userSessionManager.createUserLoginSession(token, username, email, mobile, status, user_type,user_id,provided_acc_info);
                                if (provided_acc_info.equals("false")) {
                                    Toast.makeText(LoginActivity.this, "In order to validate your payment method, we will deduct $ 1 and this will be deposited into your application wallet account", Toast.LENGTH_SHORT).show();
                                    Intent iacc_page = new Intent(LoginActivity.this, PaypalActivity.class);
                                    iacc_page.putExtra("activity", "Login");
                                    iacc_page.putExtra("paymentAmount", "1");
                                    startActivity(iacc_page);
                                } else {
                                    Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                    imain.putExtra("USERTYPE", user_type);
                                    startActivity(imain);
                                }
                            }else{
                                Toast.makeText(getApplicationContext(), "Entered email is not matched with device register email..!", Toast.LENGTH_SHORT).show();
                            }



                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    if (successResponceCode.equals("10200")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10300")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "User does not Exist..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10400")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Account is Deactivated please contact to Admin..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10500")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Account is Blocked please contact to Admin..!", Toast.LENGTH_SHORT).show();
                    }

                    if (successResponceCode.equals("10600")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Invalid Credentials, Pleae Try Again..!", Toast.LENGTH_SHORT).show();
                    }

                    if (successResponceCode.equals("10700")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "It Seen you are login from Differnet Device!", Toast.LENGTH_SHORT).show();
                        forceLogin();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pprogressDialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_type", user_type);
                params.put("email", send_email);      // here if we login with mobile no. ,same field and variable use
                params.put("password", send_pasword);
                params.put("device_id", device_id);
                params.put("country_code", country_code);
                params.put("force_login", "0");
                Log.d("LOGINPARAM", params.toString());
                return params;// {force_login =0, email=9370744745, password=123456, country_code=+91, device_id=451e23be6ccc4b18, user_type=USER}
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);

    }

    private boolean isValidMobile(String send_email)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", send_email)) {
            if(send_email.length() < 10 || send_email.length() > 14) {
                // if(phone.length() != 10) {
                check = false;
             //   txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

    private boolean validate() {
        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String MOBILE_REGEX = "^[789]\\d{9}$";
        boolean result = true;
        String userName = email_edt.getText().toString().trim();
        if (userName == null || userName.equals("")) {
            email_til.setError("Invalid Data");
            result = false;
        } else
            email_til.setErrorEnabled(false);
        String psw = pasword_edt.getText().toString().trim();
        if (psw == null || psw.equals("") || psw.length() < 6) {
            password_til.setError("Invalid Password/ Min 6 Character");
            result = false;
        } else
            password_til.setErrorEnabled(false);
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGPlusSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleGPlusSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result);
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            //userSessionManager.createUserLoginSession("ASAFAFDFAFAFAF", acct.getId(), acct.getDisplayName(), "", acct.getEmail(), "", "google");
            Log.e(TAG, "DETAIL" + personName + "\n" + acct.getId() + "\n" + acct.getEmail() + "\n" + acct.getPhotoUrl() + "\n" + acct.getFamilyName());
            if (personName.split("\\w+").length > 1) {
                lnm = personName.substring(personName.lastIndexOf(" ") + 1);
                nm = personName.substring(0, personName.lastIndexOf(' '));
            } else {
                //  firstName = name;
            }
            Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
            i.putExtra("USERTYPE", user_type);
            i.putExtra("USERID", user_id);
            i.putExtra("name", nm);
            i.putExtra("lastname", lnm);
            i.putExtra("email", acct.getEmail());
            i.putExtra("user_gmail_id", acct.getId());
            i.putExtra("provider", "Google");
            i.putExtra("LOGIN", "Google");
            Log.d("GPLUS", user_type + "//" + nm + "//" + lnm + "//" + acct.getEmail() + "//" + acct.getId());
            startActivity(i);
            //  registerGoogleSocialData(acct.getDisplayName(), acct.getId(),acct.getPhotoUrl(), "Google");
        } else {

        }
    }

    private void registerGoogleSocialData(final String displayName, final String id, final Uri photo_url, final String google) {
        if (checkInternet) {
            pprogressDialog.show();
            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SOCIAL_AUTH,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("LOGINSOCIALGOOGLE", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("LOGINSOCIALRESGOOGL", response);
                                String editSuccessResponceCode = jsonObject.getString("response_code");
                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    Toast.makeText(LoginActivity.this, " Login  Succefully", Toast.LENGTH_LONG).show();
                                    String token = jsonObject1.getString("token");
                                    String user_id = jsonObject1.getString("user_id");
                                    String user_type = jsonObject1.getString("user_type");
                                    String is_register = jsonObject1.getString("is_register");
                                    String provided_acc_info = jsonObject1.getString("provided_acc_info");
                                    Log.d("JWT", token + "//" + provided_acc_info);
                                    if (provided_acc_info.equals("false")) {
                                        Intent iacc_page = new Intent(LoginActivity.this, AddMoneyToAccountActivity.class);
                                        startActivity(iacc_page);
                                    } else {
                                        Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                        imain.putExtra("USERTYPE", user_type);
                                        startActivity(imain);
                                    }
                                    String[] parts = token.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {
                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSFSFGOOGLE", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATAsfaGOOGLE", jsonObject2.toString());
                                        String username = jsonObject2.getString("name");
                                        String email = jsonObject2.getString("email");
                                        String mobile = jsonObject2.getString("mobile");
                                        String status = jsonObject2.getString("status");
                                        //   userSessionManager.createUserLoginSession(token,username,email,mobile,status,String.valueOf(photo_url),user_type);

                                      /*  Intent imain=new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(imain);*/
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }


                                }


                            } catch (JSONException e) {
                                pprogressDialog.show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("provider", google);
                    params.put("identifier", id);
                    params.put("user_type", user_type);
                    params.put("first_name", displayName);
                    params.put("mobile", mobile_no);
                    params.put("country_code", country_code);
                    params.put("device_id", device_id);
                    params.put("force_login", "0");
                    params.put("photo_url", String.valueOf(photo_url));
                    Log.d("SENDPARAMGOOGLE", params.toString());

                    return params;
                }
            };
            stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue2 = Volley.newRequestQueue(LoginActivity.this);
            requestQueue2.add(stringRequest2);
        } else {
            Toast.makeText(LoginActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    public void graphRequest(AccessToken token) {
        GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_LONG).show();
            }
        });

        Bundle b = new Bundle();
        b.putString("fields", "id,email,first_name,last_name,picture.type(large)");
        request.setParameters(b);
        request.executeAsync();
    }

    public void forceLogin() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("You already logged in from a different device (Multiple device login is not possible)" + "\n" + "Do you want to continue to login click Yes..?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkInternet = NetworkChecking.isConnected(LoginActivity.this);
                        if (validate()) {
                            if (checkInternet) {

                                send_email = email_edt.getText().toString();
                                send_pasword = pasword_edt.getText().toString();
                                Log.d("LOGINURL", AppUrls.BASE_URL + AppUrls.LOGIN);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("LGOINRESP", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String successResponceCode = jsonObject.getString("response_code");
                                            if (successResponceCode.equals("10100")) {
                                                pprogressDialog.dismiss();
                                                JSONObject jobj = jsonObject.getJSONObject("data");
                                                Log.d("JOBJ", jobj.toString());
                                                String token = jobj.getString("token");
                                                String user_id = jobj.getString("user_id");
                                                String user_type = jobj.getString("user_type");
                                                String is_register = jobj.getString("is_register");
                                                String provided_acc_info = jobj.getString("provided_acc_info");
                                                Log.d("JWT", token + "//" + provided_acc_info);
                                                String[] parts = token.split("\\.");
                                                byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                                String decodedString = "";
                                                try {
                                                    decodedString = new String(dataDec, "UTF-8");
                                                    Log.d("TOKENSFSF", decodedString);
                                                    JSONObject jsonObject2 = new JSONObject(decodedString);
                                                    Log.d("JSONDATAsfa", jsonObject2.toString());
                                                    String username = jsonObject2.getString("name");
                                                    String email = jsonObject2.getString("email");
                                                    String mobile = jsonObject2.getString("mobile");
                                                    String status = jsonObject2.getString("status");
                                                    String profile_pic = jsonObject2.getString("profile_pic");
                                                    userSessionManager.createUserLoginSession(token, username, email, mobile, status, user_type,user_id, provided_acc_info);
                                                    if (provided_acc_info.equals("false")) {
                                                        Toast.makeText(LoginActivity.this, "In order to validate your payment method, we will deduct $ 1 and this will be deposited into your application wallet account", Toast.LENGTH_SHORT).show();
                                                        Intent iacc_page = new Intent(LoginActivity.this, PaypalActivity.class);
                                                        iacc_page.putExtra("activity", "Login");
                                                        iacc_page.putExtra("paymentAmount", "1");
                                                        startActivity(iacc_page);
                                                    } else {
                                                        Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                                        imain.putExtra("USERTYPE", user_type);
                                                        startActivity(imain);
                                                    }
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (successResponceCode.equals("10200")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                            }
                                            if (successResponceCode.equals("10300")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                                            }
                                            if (successResponceCode.equals("10400")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Account is Deactivatred please contact to Admin..!", Toast.LENGTH_SHORT).show();
                                            }
                                            if (successResponceCode.equals("10500")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Account is Blocked please contact to Admin..!", Toast.LENGTH_SHORT).show();
                                            }

                                            if (successResponceCode.equals("10600")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Invalid Credential, Pleae Try Again..!", Toast.LENGTH_SHORT).show();
                                            }

                                            if (successResponceCode.equals("10700")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "It Seenm you are login from Differnet Device!", Toast.LENGTH_SHORT).show();
                                                // forceLogin();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        pprogressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_type", user_type);
                                        params.put("email", send_email);      // here if we login with mobile no. ,same field and variable use
                                        params.put("password", send_pasword);
                                        params.put("device_id", device_id);
                                        params.put("country_code", country_code);
                                        params.put("force_login", "1");
                                        Log.d("LOGINPARAM", params.toString());
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                                requestQueue.add(stringRequest);

                            } else {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void getToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            Log.d("dkfdf", regId);
            sendFCMTokes(user_id, user_type, regId);
        } else {
            Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
       // NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void sendFCMTokes(final String user_id, final String user_type, final String token_fcm) {
        checkInternet = NetworkChecking.isConnected(LoginActivity.this);
        if (checkInternet) {
            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FCM);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.FCM,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Toast.makeText(LoginActivity.this, "Token Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("fcm_register_id", token_fcm);
                    params.put("device_platform", "ANDROID");
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(LoginActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
}


