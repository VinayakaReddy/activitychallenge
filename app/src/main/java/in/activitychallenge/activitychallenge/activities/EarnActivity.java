package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.EarnMoneyAdvertisement;
import in.activitychallenge.activitychallenge.fragments.EarnMoneyLotteryFragment;
import in.activitychallenge.activitychallenge.fragments.EarnMoneyScratchFragment;
import in.activitychallenge.activitychallenge.holder.MonthHolder;
import in.activitychallenge.activitychallenge.models.MonthModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class EarnActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close, lotResultButt;
    ViewPager view_pager_earn_money;
    TabLayout tab;
    MyChllengesViewPagerAdapter earnMoneyadapter;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    RecyclerView mo_recyc;
    LinearLayoutManager llay;
    MonthAdapter monAdap;
    ArrayList<MonthModel> moList = new ArrayList<MonthModel>();
    GridLayoutManager gridLayoutManagerVertical;
    BottomSheetBehavior behavior;
    RelativeLayout click_layout;
    View bottomSheet;
    TextView lottey_history;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SELOTTONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        lottey_history = (TextView) findViewById(R.id.lottey_history);
        lottey_history.setOnClickListener(this);

        lotResultButt = (ImageView) findViewById(R.id.lotResultButt);
        click_layout = (RelativeLayout) findViewById(R.id.click_layout);
        lotResultButt.setOnClickListener(this);
        tab = (TabLayout) findViewById(R.id.tabLayout_earn_money);
        view_pager_earn_money = (ViewPager) findViewById(R.id.view_pager_earn_money);
      //  setupViewPager(view_pager_earn_money,userActive);
      //  tab.setupWithViewPager(view_pager_earn_money);
        bottomSheet = findViewById(R.id.design_bottom_sheet);
        bottomSheet.setOnClickListener(this);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });
        lotResultButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    lotResultButt.setImageResource(R.drawable.ic_calendar_white_24dp);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        getEarnActivityStatus();
        mo_recyc = (RecyclerView) findViewById(R.id.mo_recyc);
        mo_recyc.setNestedScrollingEnabled(false);
        llay = new LinearLayoutManager(EarnActivity.this);
        mo_recyc.setLayoutManager(new LinearLayoutManager(EarnActivity.this, LinearLayoutManager.HORIZONTAL, false));
        monAdap = new MonthAdapter(EarnActivity.this, moList, R.layout.row_months);
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManagerVertical = new GridLayoutManager(EarnActivity.this, 2, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager.setReverseLayout(true);
        mo_recyc.setLayoutManager(gridLayoutManagerVertical);
        getMonth();

    }

    @Override
    public void onBackPressed() {
        //Toast.makeText(this, "BACK SUCC", Toast.LENGTH_SHORT).show();
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

   //show Earn activity user active ness or not
    private void getEarnActivityStatus(){
        checkInternet = NetworkChecking.isConnected(EarnActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_USER_ACTIVENESS_URL + user_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                          //  pprogressDialog.dismiss();

                            Log.d("User Active", AppUrls.BASE_URL + AppUrls.GET_USER_ACTIVENESS_URL + user_id);
                            Log.d("Active", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    pprogressDialog.dismiss();
                                    JSONObject tmp= jsonObject.getJSONObject("data");
                                     int userActive=(int)tmp.getInt("enable_earning");
                                     Log.d("UserActive", String.valueOf(userActive));
                                  setupViewPager(view_pager_earn_money,userActive);
                                    tab.setupWithViewPager(view_pager_earn_money);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    //    Toast.makeText(EarnActivity.this, "No data  ", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(EarnActivity.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(EarnActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }




    private void getMonth() {
        checkInternet = NetworkChecking.isConnected(EarnActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LOT_HIS,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("LOT_HIS", AppUrls.BASE_URL + AppUrls.LOT_HIS);
                            Log.d("LOTHISRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    pprogressDialog.dismiss();
                                    lotResultButt.setVisibility(View.VISIBLE);
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        MonthModel moMo = new MonthModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        moMo.setId(itemArray.getString("id"));
                                        moMo.setFor_month(itemArray.getString("for_month"));
                                        moMo.setPer_ticket_price(itemArray.getString("per_ticket_price"));
                                        moMo.setLottery_amount(itemArray.getString("lottery_amount"));
                                        moMo.setStatus(itemArray.getString("status"));
                                        moMo.setAmount_collected(itemArray.getString("amount_collected"));
                                        moMo.setCreated_on(itemArray.getString("created_on"));
                                        moMo.setCreated_on_txt(itemArray.getString("created_on_txt"));
                                        moList.add(moMo);
                                    }
                                    mo_recyc.setAdapter(monAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                //    Toast.makeText(EarnActivity.this, "No data  ", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(EarnActivity.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(EarnActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    private void setupViewPager(ViewPager view_pager_suggest,int active) {
        earnMoneyadapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());
        if(active==1){
            earnMoneyadapter.addFrag(new EarnMoneyLotteryFragment(), "Lottery");
            earnMoneyadapter.addFrag(new EarnMoneyScratchFragment(), "Scratch Cards");
            earnMoneyadapter.addFrag(new EarnMoneyAdvertisement(), "Advertisements");
        }else {
           // tab.setTabMode(TabLayout.MODE_FIXED);
            earnMoneyadapter.addFrag(new EarnMoneyLotteryFragment(), "Lottery");
            earnMoneyadapter.addFrag(new EarnMoneyAdvertisement(), "Advertisements");
        }

        view_pager_suggest.setAdapter(earnMoneyadapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_earn_money) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 1) {
                            lotResultButt.setVisibility(View.GONE);
                            lottey_history.setVisibility(View.GONE);
                        }
                       else if(tab.getPosition() == 0)
                       {
                           lottey_history.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
        if (view == close) {
            finish();
        } else if (view == bottomSheet) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

          if(view==lottey_history)
          {
              if (view == lottey_history) {
                  checkInternet = NetworkChecking.isConnected(getApplicationContext());
                  if (checkInternet) {
                      Intent profile = new Intent(EarnActivity.this, HistoryLotteryCodesActivity.class);
                      startActivity(profile);

                  } else {
                      Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                  }
              }
          }

    }

    public class MonthAdapter extends RecyclerView.Adapter<MonthHolder> {
        EarnActivity context;
        int resource;
        ArrayList<MonthModel> monList;
        LayoutInflater lInfla;

        public MonthAdapter(EarnActivity context, ArrayList<MonthModel> monList, int resource) {
            this.context = context;
            this.resource = resource;
            this.monList = monList;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MonthHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MonthHolder rHol = new MonthHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MonthHolder holder, final int position)
        {
             Log.d("FORMIONTH",monList.get(position).getFor_month());
            holder.mo_txt.setText(monList.get(position).getFor_month());

            holder.monLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent imain = new Intent(EarnActivity.this, WinnerList.class);
                    imain.putExtra("Lotter_ID", moList.get(position).getId());
                    startActivity(imain);
                    Log.d("ADCFG", moList.get(position).getId().toString());
                }
            });
        }

        @Override
        public int getItemCount() {
            return monList.size();
        }
    }
}

