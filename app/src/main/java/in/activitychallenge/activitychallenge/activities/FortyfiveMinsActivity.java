package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.filters.ActivityFilterList;
import in.activitychallenge.activitychallenge.holder.DailyStepsChallengeHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.DailyStepsChallengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class FortyfiveMinsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private HorizontalCalendar horizontalCalendar;
    ImageView close;
    Spinner spinner,long_forty_five_spinner;
    DailyFortyFiveMinsAdapter dailyStepsChallengeAdapter;
    private boolean checkInternet;
    ArrayList<DailyStepsChallengeModel> dailyStepsChallengeModels = new ArrayList<DailyStepsChallengeModel>();
    private RecyclerView daily_challenges_recyclerview,long_forty_five_challenges_recyclerview;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    WeeklyFortyFiveMinsAdapter weeklyStepsChallengeAdapter;
    ArrayList<DailyStepsChallengeModel> weeklyStepsChallengeModels = new ArrayList<DailyStepsChallengeModel>();
    private RecyclerView weekly_challenges_recyclerview;
    String id = "", user_id = "", token = "", device_id = "", weekly_challenges = "", daily_challenges = "",longrun_challenges="";
    UserSessionManager session;

    LongRunningFortyfiveChallengeAdapter longrunningFortyfiveChallengeAdapter;
    ArrayList<DailyStepsChallengeModel> longRunFortyfiveChallengeModels = new ArrayList<DailyStepsChallengeModel>();
    ////////////////////////////////////
    ImageView user_profile_image, sponsor_iv, challenge_iv, mesage_iv, banner_image;
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text, challeng_req_toolbar_title;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges, daily_challenges_layout, weekly_challenges_layout,long_forty_five_challenges_layout;
    ////////////////////////////////////////
    String daily_date = "", weekly_time = "", long_run_days="",location_name = "", location_lat = "", location_lng = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fortyfive_mins);
        Toast.makeText(FortyfiveMinsActivity.this,"User can take the challenge from Tomorrow..!",Toast.LENGTH_LONG).show();
        id = getIntent().getExtras().getString("id");
        weekly_challenges = getIntent().getExtras().getString("weekly_challenges");
        daily_challenges = getIntent().getExtras().getString("daily_challenges");
        longrun_challenges = getIntent().getExtras().getString("long_challenges");
        location_name = getIntent().getExtras().getString("location_name");
        location_lat = getIntent().getExtras().getString("location_lat");
        location_lng = getIntent().getExtras().getString("location_lng");
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        daily_challenges_layout = (LinearLayout) findViewById(R.id.daily_challenges_layout);
        weekly_challenges_layout = (LinearLayout) findViewById(R.id.weekly_challenges_layout);
        long_forty_five_challenges_layout = (LinearLayout) findViewById(R.id.long_forty_five_challenges_layout);
        if (daily_challenges.equals("0")) {
            daily_challenges_layout.setVisibility(View.GONE);
        } else {
            daily_challenges_layout.setVisibility(View.VISIBLE);
        }
        if (weekly_challenges.equals("0")) {
            weekly_challenges_layout.setVisibility(View.GONE);
        } else {
            weekly_challenges_layout.setVisibility(View.VISIBLE);
        }
        try {
            if (longrun_challenges.equals("0")) {
                long_forty_five_challenges_layout.setVisibility(View.GONE);
            } else {
                long_forty_five_challenges_layout.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        sponsor_iv = (ImageView) findViewById(R.id.sponsor_iv);
        challenge_iv = (ImageView) findViewById(R.id.challenge_iv);
        mesage_iv = (ImageView) findViewById(R.id.mesage_iv);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        challeng_req_toolbar_title = (TextView) findViewById(R.id.challeng_req_toolbar_title);
        challeng_req_toolbar_title.setText(getIntent().getExtras().getString("activity_name") + " Challenges");
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        session = new UserSessionManager(FortyfiveMinsActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        /** end after 2 months from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.WEEK_OF_MONTH, 2);
        /** start 2 months ago from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);//   startDate.add(Calendar.DAY_OF_WEEK, 0);
        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.add(Calendar.DAY_OF_WEEK, 0);
        defaultDate.add(Calendar.DAY_OF_WEEK, 0);
      /*  Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        daily_date = df.format(c.getTime());*/
        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .showDayName(true)
                .showMonthName(true)
                //.selectedDateBackground(ContextCompat.getDrawable(this, R.drawable.sample_selected_background))
                .defaultSelectedDate(startDate.getTime())
                .textColor(Color.LTGRAY, Color.WHITE)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
              //  Toast.makeText(FortyfiveMinsActivity.this, DateFormat.getDateInstance().format(date) + " is selected!", Toast.LENGTH_SHORT).show();
                Log.d("Selected Item: ", DateFormat.getDateInstance().format(date));
                daily_date = DateFormat.getDateInstance().format(date);
                Log.d("dfjgbdfdf", daily_date);
                String date1 = daily_date;
                SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy");
                Date newDate = null;
                try {
                    newDate = spf.parse(date1);
                    spf = new SimpleDateFormat("yyyy-MM-dd");
                    date1 = spf.format(newDate);
                    System.out.println(date1);

                    daily_date = date1;
                    Log.d("dfbgsdfbvsdf", daily_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        });
        weekly_time = "3";
        long_run_days = "15";
        daily_challenges_recyclerview = (RecyclerView) findViewById(R.id.daily_challenges_recyclerview);
        daily_challenges_recyclerview.setHasFixedSize(true);
        dailyStepsChallengeAdapter = new DailyFortyFiveMinsAdapter(dailyStepsChallengeModels, FortyfiveMinsActivity.this, R.layout.row_daily_steps_challenges, location_name, location_lat, location_lng);
        daily_challenges_recyclerview.setLayoutManager(new LinearLayoutManager(FortyfiveMinsActivity.this, LinearLayoutManager.HORIZONTAL, false));
        weekly_challenges_recyclerview = (RecyclerView) findViewById(R.id.weekly_challenges_recyclerview);
        weekly_challenges_recyclerview.setHasFixedSize(true);
        weeklyStepsChallengeAdapter = new WeeklyFortyFiveMinsAdapter(weeklyStepsChallengeModels, FortyfiveMinsActivity.this, R.layout.row_daily_steps_challenges, location_name, location_lat, location_lng);
        weekly_challenges_recyclerview.setLayoutManager(new LinearLayoutManager(FortyfiveMinsActivity.this,  LinearLayoutManager.HORIZONTAL, false));

        long_forty_five_challenges_recyclerview = (RecyclerView) findViewById(R.id.long_forty_five_challenges_recyclerview);
        long_forty_five_challenges_recyclerview.setHasFixedSize(true);
        longrunningFortyfiveChallengeAdapter = new LongRunningFortyfiveChallengeAdapter(longRunFortyfiveChallengeModels, FortyfiveMinsActivity.this, R.layout.row_daily_steps_challenges, location_name, location_lat, location_lng);
        long_forty_five_challenges_recyclerview.setLayoutManager(new LinearLayoutManager(FortyfiveMinsActivity.this, LinearLayoutManager.HORIZONTAL, false));


        getDailyChallenges();
        getWeeklyChallenges();
        getLongRunningChallenges();

        getCount();
        // Spinner element
        spinner = (Spinner) findViewById(R.id.spinner);
        long_forty_five_spinner = (Spinner) findViewById(R.id.long_forty_five_spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("3 days");
        categories.add("4 days");
        categories.add("5 days");
        categories.add("6 days");
        categories.add("7 days");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        //THIS IS FOR LONG RUNNING
        List<String> long_run_categories = new ArrayList<String>();
        long_run_categories.add("15 days");
        long_run_categories.add("30 days");
        long_run_categories.add("45 days");
        long_run_categories.add("60 days");
        long_run_categories.add("75 days");
        long_run_categories.add("90 days");
        long_run_categories.add("105 days");
        long_run_categories.add("120 days");
        long_run_categories.add("135 days");
        long_run_categories.add("150 days");
        long_run_categories.add("165 days");
        long_run_categories.add("180 days");
        ArrayAdapter<String> longdataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, long_run_categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        long_forty_five_spinner.setAdapter(longdataAdapter);

        long_forty_five_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                String item = adapterView.getItemAtPosition(i).toString();
                long_run_days = item.replace(" days", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(FortyfiveMinsActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(FortyfiveMinsActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(FortyfiveMinsActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");
            startActivity(msg);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();
        // Showing selected spinner item
      //  Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
        weekly_time = item.replace(" days", "");
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void getDailyChallenges() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            dailyStepsChallengeModels.clear();
            String url = AppUrls.BASE_URL + AppUrls.WEEKLYCHALLENGES + id + AppUrls.TYPE + "daily";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonobj = jsonArray.getJSONObject(0);
                                    String id = jsonobj.getString("id");
                                    String activity_id = jsonobj.getString("activity_id");
                                    String challenge_sub_logo = jsonobj.getString("challenge_sub_logo");
                                    JSONArray challenges_array = jsonobj.getJSONArray("challenges");
                                    for (int i = 0; i < challenges_array.length(); i++) {
                                        JSONObject jsonObject1 = challenges_array.getJSONObject(i);
                                        DailyStepsChallengeModel am = new DailyStepsChallengeModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setFactor_eval(jsonObject1.getString("factor_eval"));
                                        am.setFactor_eval_unit(jsonObject1.getString("factor_eval_unit"));
                                        am.setFactor_goal_val(jsonObject1.getString("factor_goal_val"));
                                        am.setImage(AppUrls.BASE_IMAGE_URL + challenge_sub_logo);
                                        dailyStepsChallengeModels.add(am);
                                        Log.d("fjbvsdfkvsdf", dailyStepsChallengeModels.toString());
                                    }
                                    daily_challenges_recyclerview.setAdapter(dailyStepsChallengeAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FortyfiveMinsActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getWeeklyChallenges() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            weeklyStepsChallengeModels.clear();
            String url = AppUrls.BASE_URL + AppUrls.WEEKLYCHALLENGES + id + AppUrls.TYPE + "weekly";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonobj = jsonArray.getJSONObject(0);
                                    String id = jsonobj.getString("id");
                                    String activity_id = jsonobj.getString("activity_id");
                                    String challenge_sub_logo = jsonobj.getString("challenge_sub_logo");
                                    JSONArray challenges_array = jsonobj.getJSONArray("challenges");
                                    for (int i = 0; i < challenges_array.length(); i++) {
                                        JSONObject jsonObject1 = challenges_array.getJSONObject(i);
                                        DailyStepsChallengeModel am = new DailyStepsChallengeModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setFactor_eval(jsonObject1.getString("factor_eval"));
                                        am.setFactor_eval_unit(jsonObject1.getString("factor_eval_unit"));
                                        am.setFactor_goal_val(jsonObject1.getString("factor_goal_val"));
                                        am.setImage(AppUrls.BASE_IMAGE_URL + challenge_sub_logo);
                                        weeklyStepsChallengeModels.add(am);
                                        Log.d("fjbvsdfkvsdf", weeklyStepsChallengeModels.toString());
                                    }
                                    weekly_challenges_recyclerview.setAdapter(weeklyStepsChallengeAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FortyfiveMinsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getLongRunningChallenges()
    {
        //AppUrls.BASE_URL + AppUrls.WEEKLYCHALLENGES + id + AppUrls.TYPE + "longrun";

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            longRunFortyfiveChallengeModels.clear();
            String url = AppUrls.BASE_URL + AppUrls.WEEKLYCHALLENGES + id + AppUrls.TYPE + "longrun";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonobj = jsonArray.getJSONObject(0);
                                    String id = jsonobj.getString("id");
                                    String activity_id = jsonobj.getString("activity_id");
                                    String challenge_sub_logo = jsonobj.getString("challenge_sub_logo");
                                    JSONArray challenges_array = jsonobj.getJSONArray("challenges");
                                    for (int i = 0; i < challenges_array.length(); i++) {
                                        JSONObject jsonObject1 = challenges_array.getJSONObject(i);
                                        DailyStepsChallengeModel am = new DailyStepsChallengeModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setFactor_eval(jsonObject1.getString("factor_eval"));
                                        am.setFactor_eval_unit(jsonObject1.getString("factor_eval_unit"));
                                        am.setFactor_goal_val(jsonObject1.getString("factor_goal_val"));
                                        am.setImage(AppUrls.BASE_IMAGE_URL + challenge_sub_logo);
                                        longRunFortyfiveChallengeModels.add(am);
                                        Log.d("fjbvsdfkvsdf", longRunFortyfiveChallengeModels.toString());
                                    }
                                    long_forty_five_challenges_recyclerview.setAdapter(longrunningFortyfiveChallengeAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(FortyfiveMinsActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FortyfiveMinsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                Intent intent = new Intent(FortyfiveMinsActivity.this, RunningChallengesActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class DailyFortyFiveMinsAdapter extends RecyclerView.Adapter<DailyStepsChallengeHolder> {

        public ArrayList<DailyStepsChallengeModel> listModels, filterList;
        FortyfiveMinsActivity context;
        LayoutInflater li;
        int resource;
        Typeface typeface, typeface2;
        private boolean checkInternet;
        ProgressDialog progressDialog;
        private static final int PLACE_PICKER_REQUEST = 1;
        Sensor stepSensor;
        SensorManager sManager;
        GPSTracker gps;
        ActivityFilterList filter;
        String location_name, location_lat, location_lng;

        public DailyFortyFiveMinsAdapter(ArrayList<DailyStepsChallengeModel> listModels, FortyfiveMinsActivity context, int resource, String location_name, String location_lat, String location_lng) {
            this.listModels = listModels;
            this.context = context;
            this.resource = resource;
            this.filterList = listModels;
            this.location_name = location_name;
            this.location_lat = location_lat;
            this.location_lng = location_lng;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            gps = new GPSTracker(context);

        }

        @Override
        public DailyStepsChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            DailyStepsChallengeHolder slh = new DailyStepsChallengeHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final DailyStepsChallengeHolder holder, final int position) {
            holder.share_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take 45 Mins Challenges in Daily");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            holder.name.setText(listModels.get(position).getFactor_goal_val() + "\n" + listModels.get(position).getFactor_eval_unit());
            Picasso.with(context)
                    .load(listModels.get(position).getImage())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.activity_image);
            Log.d("xfkldfbng", holder.name.getText().toString());
            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    checkInternet = NetworkChecking.isConnected(getApplicationContext());
                    if (checkInternet) {
                    new AlertDialog.Builder(context)
                            .setMessage("In order to carry out your challenge we with hold the challenged amount and will be released back based on the result of the challenge")
                            .setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String id = ((Activity) context).getIntent().getExtras().getString("id");
                                    Intent intent = new Intent(context, ActivityPrice.class);
                                    intent.putExtra("activity_id", id);
                                    intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                    intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                    intent.putExtra("challenge_id", listModels.get(pos).getId());
                                    intent.putExtra("challenge_goal", listModels.get(pos).getFactor_goal_val());
                                    intent.putExtra("evaluation_factor", listModels.get(pos).getFactor_eval());
                                    intent.putExtra("evaluation_factor_unit", listModels.get(pos).getFactor_eval_unit());
                                    intent.putExtra("type", "DAILY");
                                    intent.putExtra("span", "1");
                                    intent.putExtra("date", daily_date);
                                    intent.putExtra("location_name", location_name);
                                    intent.putExtra("location_lat", location_lat);
                                    intent.putExtra("location_lng", location_lng);
                                    context.startActivity(intent);
                                }
                            }).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.listModels.size();
        }

        public String firstLetterCaps(String data) {
            String firstLetter = data.substring(0, 1).toUpperCase();
            String restLetters = data.substring(1).toLowerCase();
            return firstLetter + restLetters;
        }


    }

    public class WeeklyFortyFiveMinsAdapter extends RecyclerView.Adapter<DailyStepsChallengeHolder> {

        public ArrayList<DailyStepsChallengeModel> listModels, filterList;
        FortyfiveMinsActivity context;
        LayoutInflater li;
        int resource;
        Typeface typeface, typeface2;
        private boolean checkInternet;
        ProgressDialog progressDialog;
        private static final int PLACE_PICKER_REQUEST = 1;
        Sensor stepSensor;
        SensorManager sManager;
        GPSTracker gps;
        ActivityFilterList filter;
        String location_name, location_lat, location_lng;

        public WeeklyFortyFiveMinsAdapter(ArrayList<DailyStepsChallengeModel> listModels, FortyfiveMinsActivity context, int resource, String location_name, String location_lat, String location_lng) {
            this.listModels = listModels;
            this.context = context;
            this.resource = resource;
            this.filterList = listModels;
            this.location_name = location_name;
            this.location_lat = location_lat;
            this.location_lng = location_lng;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            gps = new GPSTracker(context);

        }

        @Override
        public DailyStepsChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            DailyStepsChallengeHolder slh = new DailyStepsChallengeHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final DailyStepsChallengeHolder holder, final int position) {
            holder.share_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take 45 Mins Challenges in Weekly");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            holder.name.setText(listModels.get(position).getFactor_goal_val() + "\n" + listModels.get(position).getFactor_eval_unit()+" / day");
            Picasso.with(context)
                    .load(listModels.get(position).getImage())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.activity_image);
            Log.d("xfkldfbng", holder.name.getText().toString());
            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    checkInternet = NetworkChecking.isConnected(getApplicationContext());
                    if (checkInternet) {
                    new AlertDialog.Builder(context)
                            .setMessage("In order to carry out your challenge we with hold the challenged amount and will be released back based on the result of the challenge. \n The challenge starts automatically on upcoming Monday.")
                            .setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String id = ((Activity) context).getIntent().getExtras().getString("id");
                                    Intent intent = new Intent(context, ActivityPrice.class);
                                    intent.putExtra("activity_id", id);
                                    intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                    intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                    intent.putExtra("challenge_id", listModels.get(pos).getId());
                                    intent.putExtra("challenge_goal", listModels.get(pos).getFactor_goal_val());
                                    intent.putExtra("evaluation_factor", listModels.get(pos).getFactor_eval());
                                    intent.putExtra("evaluation_factor_unit", listModels.get(pos).getFactor_eval_unit());
                                    intent.putExtra("type", "WEEKLY");
                                    intent.putExtra("span", weekly_time);
                                    intent.putExtra("date", "0000-00-00");
                                    intent.putExtra("location_name", location_name);
                                    intent.putExtra("location_lat", location_lat);
                                    intent.putExtra("location_lng", location_lng);
                                    context.startActivity(intent);
                                }
                            }).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.listModels.size();
        }

        public String firstLetterCaps(String data) {
            String firstLetter = data.substring(0, 1).toUpperCase();
            String restLetters = data.substring(1).toLowerCase();
            return firstLetter + restLetters;
        }


    }
    public class LongRunningFortyfiveChallengeAdapter extends RecyclerView.Adapter<DailyStepsChallengeHolder> {

        public ArrayList<DailyStepsChallengeModel> listModels, filterList;
        FortyfiveMinsActivity context;
        LayoutInflater li;
        int resource;
        Typeface typeface, typeface2;
        private boolean checkInternet;
        ProgressDialog progressDialog;
        private static final int PLACE_PICKER_REQUEST = 1;
        Sensor stepSensor;
        SensorManager sManager;
        GPSTracker gps;
        ActivityFilterList filter;
        String location_name, location_lat, location_lng;

        public LongRunningFortyfiveChallengeAdapter(ArrayList<DailyStepsChallengeModel> listModels, FortyfiveMinsActivity context, int resource, String location_name, String location_lat, String location_lng) {
            this.listModels = listModels;
            this.context = context;
            this.resource = resource;
            this.filterList = listModels;
            this.location_name = location_name;
            this.location_lat = location_lat;
            this.location_lng = location_lng;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            gps = new GPSTracker(context);
        }

        @Override
        public DailyStepsChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            DailyStepsChallengeHolder slh = new DailyStepsChallengeHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final DailyStepsChallengeHolder holder, final int position) {
            holder.share_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take Step Challenges in Weekly");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            holder.name.setText(listModels.get(position).getFactor_goal_val() + "\n" + listModels.get(position).getFactor_eval_unit()+" / day");
            Picasso.with(context)
                    .load(listModels.get(position).getImage())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.activity_image);
            Log.d("xfkldfbng", holder.name.getText().toString());
            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    checkInternet = NetworkChecking.isConnected(getApplicationContext());
                    if (checkInternet) {
                        new AlertDialog.Builder(context)
                                .setMessage("In order to carry out your challenge we with hold the challenged amount and will be released back based on the result of the challenge. \n The challenge starts automatically on upcoming Monday.")
                                .setCancelable(false)
                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        dialogInterface.cancel();

                                    }
                                })
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String id = ((Activity) context).getIntent().getExtras().getString("id");
                                        Intent intent = new Intent(context, ActivityPrice.class);
                                        intent.putExtra("activity_id", id);
                                        intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                        intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                        intent.putExtra("challenge_id", listModels.get(pos).getId());
                                        intent.putExtra("challenge_goal", listModels.get(pos).getFactor_goal_val());
                                        intent.putExtra("evaluation_factor", listModels.get(pos).getFactor_eval());
                                        intent.putExtra("evaluation_factor_unit", listModels.get(pos).getFactor_eval_unit());
                                        intent.putExtra("type", "LONGRUN");
                                        intent.putExtra("span", long_run_days);
                                        intent.putExtra("date", "0000-00-00");
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);
                                    }
                                }).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.listModels.size();
        }

        public String firstLetterCaps(String data) {
            String firstLetter = data.substring(0, 1).toUpperCase();
            String restLetters = data.substring(1).toLowerCase();
            return firstLetter + restLetters;
        }
    }

}
