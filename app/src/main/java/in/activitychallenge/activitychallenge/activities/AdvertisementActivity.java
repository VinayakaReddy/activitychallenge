package in.activitychallenge.activitychallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class AdvertisementActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    TextView contact_toolbar_title;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
