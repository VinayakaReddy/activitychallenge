package in.activitychallenge.activitychallenge.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import in.activitychallenge.activitychallenge.R;

public class GPSTrackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpstrack);
        WebView webView = (WebView) findViewById(R.id.webview);
         String challengeId=getIntent().getStringExtra("challengeId");
         String user=getIntent().getStringExtra("userid");
        WebSettings webSettings = webView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("http://www.activity-challenge.com/site/get_map?challenge_id="+challengeId+"&user_id="+user);
    }
}
