package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.ChallengeRequestAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeRequestModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class ChallengeRequestActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView no_requests_img, close;
    TextView my_challenges_txt;
    RecyclerView challenge_request_recyclerview;
    LinearLayoutManager layoutManager;
    ChallengeRequestAdapter adapter;
    ArrayList<ChallengeRequestModel> challengeRequestModels = new ArrayList<ChallengeRequestModel>();
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String user_id = "", user_type = "", device_id = "", token = "";
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges_list);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        no_requests_img = (ImageView) findViewById(R.id.no_requests_img);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        my_challenges_txt = (TextView) findViewById(R.id.my_challenges_txt);
        my_challenges_txt.setOnClickListener(this);
        challenge_request_recyclerview = (RecyclerView) findViewById(R.id.challenge_request_recyclerview);
        adapter = new ChallengeRequestAdapter(challengeRequestModels, ChallengeRequestActivity.this, R.layout.row_challenges_list);
        layoutManager = new LinearLayoutManager(this);
        challenge_request_recyclerview.setNestedScrollingEnabled(false);
        challenge_request_recyclerview.setLayoutManager(layoutManager);
        getChallengeRequest();
    }

    private void getChallengeRequest() {
        checkInternet = NetworkChecking.isConnected(ChallengeRequestActivity.this);
        if (checkInternet) {
            Log.d("CHALLENGEREQ", AppUrls.BASE_URL + AppUrls.CHALLENGE_REQUEST + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_REQUEST + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("REPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);
                                        ChallengeRequestModel crm = new ChallengeRequestModel();
                                        crm.setId(jsonObject1.getString("id"));
                                        crm.setStatus(jsonObject1.getString("status"));
                                        crm.setAmount(jsonObject1.getString("amount"));
                                        crm.setUser_wallet(jsonObject1.getString("user_wallet"));
                                        crm.setGroup_wallet(jsonObject1.getString("group_wallet"));
                                        crm.setUser_name(jsonObject1.getString("user_name"));
                                        crm.setGroup_name(jsonObject1.getString("group_name"));
                                        crm.setActivity_name(jsonObject1.getString("activity_name"));
                                        crm.setUser_rank(jsonObject1.getString("user_rank"));
                                        crm.setGroup_rank(jsonObject1.getString("group_rank"));
                                        crm.setChallenge_goal(jsonObject1.getString("challenge_goal"));
                                        crm.setChallenge_type(jsonObject1.getString("challenge_type"));
                                        crm.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                        crm.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                        crm.setUser_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("user_pic"));
                                        crm.setGroup_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("group_pic"));
                                        String type=jsonObject1.getString("challenge_type");
                                        if(type.equalsIgnoreCase("SPECIAL_EVENT")) {
                                            crm.setLocationName(jsonObject1.getString("location_name"));
                                            crm.setSpecial_eventId(jsonObject1.getString("special_event_id"));
                                        }
                                        challengeRequestModels.add(crm);
                                    }
                                    challenge_request_recyclerview.setAdapter(adapter);

                                    if (jarray.length() == 0) {
                                        no_requests_img.setVisibility(View.VISIBLE);
                                    }
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_requests_img.setVisibility(View.VISIBLE);
                                   // Toast.makeText(ChallengeRequestActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengeRequestActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeRequestActivity.this);
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(ChallengeRequestActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            Intent intent = new Intent(ChallengeRequestActivity.this, MainActivity.class);
            startActivity(intent);
        }

        if (view == my_challenges_txt) {

            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent intent = new Intent(ChallengeRequestActivity.this, MyChallengeActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }



        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);

        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted)
                        Toast.makeText(this, "Permission Granted, Now you can access location", Toast.LENGTH_SHORT).show();

                    else {
                        Toast.makeText(this, "Permission Denied, You cannot access location", Toast.LENGTH_SHORT).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access the Location Permission",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ChallengeRequestActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
