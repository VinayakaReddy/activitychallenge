package in.activitychallenge.activitychallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class ChallengeGroupActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView close;
    TextView chlnge_group_toolbar_title;
    Typeface typeface, typeface_bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_group);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
