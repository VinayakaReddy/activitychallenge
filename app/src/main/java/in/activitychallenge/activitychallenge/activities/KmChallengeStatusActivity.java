package in.activitychallenge.activitychallenge.activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.DayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.adapter.OpponentDayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.models.DayDateWiseStatusModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.CalculateDistance;
import in.activitychallenge.activitychallenge.utilities.ChallengeFinishedTime;
import in.activitychallenge.activitychallenge.utilities.ConnectivityReceiver;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.StatusKmsChallengeDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class KmChallengeStatusActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    ImageView close, challenger_one_image, challenger_two_image,gps_track_img;

    TextView toolbar_title, participent_oponent_type, participent_user_type, participent_user_name, participent_oponent_name, vsTxt, memb_count_top, mem_cccount_down;
    Typeface typeface3;
    private boolean checkInternet;
    Location lastlocation = new Location("last");
    UserSessionManager session;
    String token = "", device_id = "", challenge_id;
    ProgressDialog progressDialog;
    int id = 1;
    int msgflag = 0;
    int serviceflag=0;
    String opponentType;
    float latestDistance;
     Intent myGpsService;
    Sensor stepSensor;
    SensorManager sManager;
    private long steps;
    int lateststeps;
    String steps_count = "";
    String distance_calc = "";
    String challenge_goal, opponent_type, opponent_usertype, usd_usertype, usd_type_in;
    TextView activity_name, goal, cash_price, challenger_two_steps, remaining_challenger_one_steps, remaining_challenger_two_steps, challenger_one_time_remaining, challenger_two_time_remaining, challenger_one_rank, challenger_two_rank, update;
    LinearLayout ll_top, ll_down, ll_days_status_down, ll_days_status_top;
    private long timestamp;
    String user_id, challenge_type;
    int timeinsec;
    String evaluation_factor, android_id;
    String evaluation_factor_unit, user_type_for_challenge, group_id;
    String user_completed_goal = "0";
    StatusKmsChallengeDB statusKmsChallengeDB;
    SharedPreferences sensorPreference;
    SharedPreferences.Editor sensoreditor;
     TextView challenger_one_steps;
    ContentValues values;
    private Handler mHandler = new Handler();
    SensorEventListener eventListener;
    float distance;
    boolean isStarted;
    boolean isFirstTime;
    RecyclerView recyclerview_days_status_top, recyclerview_days_status_down;
    DayDateWiseChallengeStatusAdapter dayDateWiseChallengeStatusAdapter;
    ArrayList<DayDateWiseStatusModel> dayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    ArrayList<DayDateWiseStatusModel> opponentdayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    OpponentDayDateWiseChallengeStatusAdapter opponentDayDateWiseChallengeStatusAdapter;
    RecyclerView.LayoutManager layoutManager;
    ChallengeFinishedTime finishedTime;
    private Handler handler;
   double latitude = 0, longitude = 0;
    private boolean isRunning = true;
    Button serviceBtn;
    private static boolean mIsSensorUpdateEnabled = false;
    private Timer mTimer = null;
    GPSTracker gps;
    private SharedPreferences  sharedPreferences;
    private LocationManager mLocationManager;

    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_km_challenge_status);
        statusKmsChallengeDB = new StatusKmsChallengeDB(KmChallengeStatusActivity.this);
        finishedTime=new ChallengeFinishedTime(this);
        finishedTime.challengeFinishTime();
        values = new ContentValues();
        android_id = Settings.Secure.getString(KmChallengeStatusActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        steps_count = "0";
        distance_calc = "0 Km";

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsec = (tz.getOffset(cal.getTimeInMillis())) / 1000;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        opponentType = bundle.getString("opponent_type");
        typeface3 = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.hermes));
        serviceBtn=(Button)findViewById(R.id.service_tooglebtn);
        serviceBtn.setOnClickListener(this);
        vsTxt = (TextView) findViewById(R.id.vsTxt);
        vsTxt.setTypeface(typeface3);
        participent_oponent_type = (TextView) findViewById(R.id.participent_oponent_type);
        participent_user_type = (TextView) findViewById(R.id.participent_user_type);
        participent_user_name = (TextView) findViewById(R.id.participent_user_name);
        participent_oponent_name = (TextView) findViewById(R.id.participent_oponent_name);
        vsTxt = (TextView) findViewById(R.id.vsTxt);
        ll_top = (LinearLayout) findViewById(R.id.ll_top);
        ll_down = (LinearLayout) findViewById(R.id.ll_down);
        ll_days_status_down = (LinearLayout) findViewById(R.id.ll_days_status_down);
        ll_days_status_top = (LinearLayout) findViewById(R.id.ll_days_status_top);
        recyclerview_days_status_top = (RecyclerView) findViewById(R.id.recyclerview_days_status_top);
        recyclerview_days_status_down = (RecyclerView) findViewById(R.id.recyclerview_days_status_down);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        gps_track_img = (ImageView) findViewById(R.id.gps_track_img);
        gps_track_img.setOnClickListener(this);
        memb_count_top = (TextView) findViewById(R.id.memb_count_top);
        memb_count_top.setOnClickListener(this);
        mem_cccount_down = (TextView) findViewById(R.id.mem_cccount_down);
        mem_cccount_down.setOnClickListener(this);
        update = (TextView) findViewById(R.id.update);
        update.setOnClickListener(this);
        update.setVisibility(View.GONE);

        challenger_one_image = (ImageView) findViewById(R.id.challenger_one_image);
        challenger_two_image = (ImageView) findViewById(R.id.challenger_two_image);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        activity_name = (TextView) findViewById(R.id.activity_name);
        goal = (TextView) findViewById(R.id.goal);
        cash_price = (TextView) findViewById(R.id.cash_price);
        challenger_one_steps = (TextView) findViewById(R.id.challenger_one_steps);
        challenger_two_steps = (TextView) findViewById(R.id.challenger_two_steps);
        remaining_challenger_one_steps = (TextView) findViewById(R.id.remaining_challenger_one_steps);
        remaining_challenger_two_steps = (TextView) findViewById(R.id.remaining_challenger_two_steps);
        challenger_one_time_remaining = (TextView) findViewById(R.id.challenger_one_time_remaining);
        challenger_two_time_remaining = (TextView) findViewById(R.id.challenger_two_time_remaining);
        challenger_one_rank = (TextView) findViewById(R.id.challenger_one_rank);
        challenger_two_rank = (TextView) findViewById(R.id.challenger_two_rank);
        getKMChallangesStatus();
        String gpsStatus=bundle.getString("gps");
        Log.v("GPS",gpsStatus);
        if(gpsStatus!=null){
            if(gpsStatus.equals("1"))
                gps_track_img.setVisibility(View.VISIBLE);

        }
        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
                != null) {
            // Toast.makeText(KmChallengeStatusActivity.this, "sensor Working", Toast.LENGTH_SHORT).show();
        } else {
            // Toast.makeText(KmChallengeStatusActivity.this, "sensor not Working", Toast.LENGTH_SHORT).show();
        }

        //checkConnection();
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                msgflag = 1;
                updateThread();
            }
        }, 0, 5, TimeUnit.MINUTES);


        eventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                Sensor sensor = event.sensor;
                float[] values = event.values;
                int value = -1;

                if (!mIsSensorUpdateEnabled) {
                    stopSensors();
                    Log.v("SensorMM", "SensorUpdate KM disabled. returning");
                    return;
                }
                if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
                    SharedPreferences preferences = getSharedPreferences("Distance", MODE_PRIVATE);
                    boolean isupdated=sensorPreference.getBoolean("isStarted",false);
                    Log.v("ServiceValue","&&&&&"+isupdated);
                    if(isupdated){
                        steps++;
                        Log.v("Count", ">>" + steps);
                        try {

                            getDistanceRun(steps);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

               }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
    }
    private void stopSensors(){
        sManager.unregisterListener(eventListener);
        mIsSensorUpdateEnabled =false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String text=serviceBtn.getText().toString();
        if(text.equalsIgnoreCase("STOP")){
            mTimer.cancel();
            startService(new Intent(KmChallengeStatusActivity.this,CalculateDistance.class));
        }else {
            stopService(new Intent(KmChallengeStatusActivity.this,CalculateDistance.class));
        }
        isRunning=false;
    }

    private void getKMChallangesStatus() {
        checkInternet = NetworkChecking.isConnected(KmChallengeStatusActivity.this);

        if (checkInternet) {
            //  statusKmsChallengeDB.emptyDBBucket();
            Log.d("KMMEMCHALLENGESTATUS", AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("KMETESTATUSRESP", response);
                            try {
                                values.put(statusKmsChallengeDB.CHALLENGE_ID, challenge_id);
                                Log.v("InsertedCh", ">>>" + challenge_id);
                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("activity");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String challenge_id = jsonObject2.getString("challenge_id");
                                        String user_id_ = jsonObject2.getString("user_id");
                                        user_type_for_challenge = jsonObject2.getString("user_type");
                                        if (user_type_for_challenge.equals("USER")) {
                                            user_id = user_id;
                                        } else {
                                            group_id = user_id_;
                                        }
                                        String opponent_id = jsonObject2.getString("opponent_id");
                                        opponent_type = jsonObject2.getString("opponent_type");
                                        participent_user_type.setText(user_type_for_challenge);
                                        participent_oponent_type.setText(opponent_type);
                                        if (opponent_type.equals("ADMIN")) {
                                            ll_down.setVisibility(View.GONE);
                                            vsTxt.setVisibility(View.GONE);
                                        } else {
                                            ll_down.setVisibility(View.VISIBLE);
                                            vsTxt.setVisibility(View.VISIBLE);
                                        }
                                        challenge_type = jsonObject2.getString("challenge_type");
                                        if (challenge_type.equals("DAILY")) {

                                            recyclerview_days_status_down.setVisibility(View.GONE);
                                            recyclerview_days_status_top.setVisibility(View.GONE);
                                        } else {
                                            recyclerview_days_status_down.setVisibility(View.VISIBLE);
                                            recyclerview_days_status_top.setVisibility(View.VISIBLE);
                                        }
                                        challenge_goal = jsonObject2.getString("challenge_goal");
                                        Log.v("InsertGoal", challenge_goal);
                                        String amount = jsonObject2.getString("amount");
                                        values.put(StatusKmsChallengeDB.AMOUNT, " $" + amount);
                                        cash_price.setText(" $" + amount);
                                        evaluation_factor = jsonObject2.getString("evaluation_factor");
                                        String activity_name_string = jsonObject2.getString("activity_name");
                                        values.put(StatusKmsChallengeDB.ACTIVITY_NAME, activity_name_string);
                                        activity_name.setText(activity_name_string);
                                        String activity_image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image");
                                        evaluation_factor_unit = jsonObject2.getString("evaluation_factor_unit");
                                        goal.setText(challenge_goal + " " + evaluation_factor_unit);
                                        values.put(StatusKmsChallengeDB.CHALLENGE_GOAL, challenge_goal + " " + evaluation_factor_unit);
                                        String completed_on_txt = jsonObject2.getString("completed_on_txt");

                                    }

                                    String user_count = jsonObject1.getString("user_count");

                                    if (!user_count.equals("1")) {
                                        memb_count_top.setVisibility(View.VISIBLE);
                                        memb_count_top.setText("members " + user_count + "\n" + "see more..");

                                    } else {
                                        memb_count_top.setVisibility(View.GONE);
                                    }

                                    String opponent_count = jsonObject1.getString("opponent_count");
                                    if (!opponent_count.equals("1")) {
                                        mem_cccount_down.setVisibility(View.VISIBLE);
                                        mem_cccount_down.setText("members " + opponent_count + "\n" + "see more..");

                                    } else {
                                        mem_cccount_down.setVisibility(View.GONE);
                                    }

                                    challenger_one_steps.setText(user_count);
                                    challenger_two_steps.setText(opponent_count);
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("user_details");
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    usd_usertype = jsonObject3.getString("user_type");
                                    user_completed_goal = jsonObject3.getString("user_completed_goal");
                                    distance_calc = user_completed_goal;

                                    ///////////////////////////////////////////////Day WISE STATUS TOP/////////////////////////
                                    //  dayDateWiseStatusModel.clear();
                                    JSONArray jsonchallenger = jsonObject1.getJSONArray("challenger_daywise_status");
                                    Log.d("KDKDKDKDK", jsonchallenger.toString());
                                    for (int i = 0; i < jsonchallenger.length(); i++) {
                                        JSONObject jdataobj = jsonchallenger.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        dayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", dayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_top.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(KmChallengeStatusActivity.this);
                                    recyclerview_days_status_top.setLayoutManager(new LinearLayoutManager(KmChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    dayDateWiseChallengeStatusAdapter = new DayDateWiseChallengeStatusAdapter(dayDateWiseStatusModel, KmChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    recyclerview_days_status_top.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_top.setAdapter(dayDateWiseChallengeStatusAdapter);

                                    ////////////////////////////////////////END/////////////////////////////////////////////////////


                                    String user_name = jsonObject3.getString("user_name");
                                    //  participent_user_name.setText(user_name);
                                    String user_rank = jsonObject3.getString("user_rank");
                                    //   challenger_one_rank.setText(user_rank);
                                    String image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("image");
                                    String group_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("group_image");
                                    String group_name = jsonObject3.getString("group_name");
                                    String group_rank = jsonObject3.getString("group_rank");

                                    String super_admin_name = jsonObject3.getString("super_admin_name");
                                    String super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("super_admin_image");
                                    String super_admin_rank = jsonObject3.getString("super_admin_rank");
                                    String distancevalue = "";
                                    if (usd_usertype.equals("USER")) {

                                        float latest = statusKmsChallengeDB.latestGoal(challenge_id);

                                        if(latest==0){
                                            latestDistance = Float.parseFloat(user_completed_goal);
                                        }else {
                                            latestDistance=latest;
                                       }
                                       SharedPreferences preferences=getSharedPreferences("Distance",MODE_PRIVATE);
                                       SharedPreferences.Editor editor=preferences.edit();
                                        editor.putFloat("latestdistance",latestDistance);
                                        editor.putString("challengeId",challenge_id);
                                        editor.apply();
                                        distancevalue = String.valueOf(latestDistance);
                                        Log.v("Latest123", ">>" + latestDistance);
                                        participent_user_name.setText(user_name);
                                        challenger_one_rank.setText(user_rank);
                                        participent_user_type.setText(usd_usertype);
                                        challenger_one_steps.setText("" + latestDistance + " KM(S)");

                                        Picasso.with(KmChallengeStatusActivity.this)
                                                .load(image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusKmsChallengeDB.USER_NAME, user_name);
                                        values.put(StatusKmsChallengeDB.USER_TYPE, usd_usertype);
                                        values.put(StatusKmsChallengeDB.USER_COMPLETED_GOAL, distancevalue);
                                        Log.v("Interserted", ">>" + latestDistance);
                                        values.put(StatusKmsChallengeDB.USER_RANK, user_rank);
                                        values.put(StatusKmsChallengeDB.IMAGE, image);
                                    } else {
                                        float latest = statusKmsChallengeDB.latestGoal(challenge_id);
                                        if(latest==0){
                                            latestDistance = Float.parseFloat(user_completed_goal);
                                        }else {
                                            latestDistance=latest;
                                        }
                                        distancevalue = String.valueOf(latestDistance);
                                        participent_user_name.setText(group_name);
                                        challenger_one_rank.setText(group_rank);
                                        participent_user_type.setText(usd_usertype);
                                        challenger_one_steps.setText(latestDistance + " KM(S)");
                                        Picasso.with(KmChallengeStatusActivity.this)
                                                .load(group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusKmsChallengeDB.USER_NAME, group_name);
                                        values.put(StatusKmsChallengeDB.USER_TYPE, usd_usertype);
                                        values.put(StatusKmsChallengeDB.USER_COMPLETED_GOAL, distancevalue);
                                        values.put(StatusKmsChallengeDB.USER_RANK, group_rank);
                                        values.put(StatusKmsChallengeDB.IMAGE, group_image);
                                    }
                                  /*  Picasso.with(KmChallengeStatusActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.dummy_user32)
                                            .into(challenger_one_image);*/
                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("opponent_details");
                                    JSONObject jsonObject4 = jsonArray2.getJSONObject(0);
                                    opponent_usertype = jsonObject4.getString("opponent_type");
                                    String opponent_completed_goal = jsonObject4.getString("opponent_completed_goal");

                                    ///////////////////////////////////////////////Day WISE STATUS DOWN/////////////////////////


                                    JSONArray jsonopponent = jsonObject1.getJSONArray("opponent_daywise_status");
                                    Log.d("KDKDKDKDK", jsonopponent.toString());
                                    for (int i = 0; i < jsonopponent.length(); i++) {
                                        JSONObject jdataobj = jsonopponent.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        opponentdayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", opponentdayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_down.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(KmChallengeStatusActivity.this);
                                    recyclerview_days_status_down.setLayoutManager(new LinearLayoutManager(KmChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    opponentDayDateWiseChallengeStatusAdapter = new OpponentDayDateWiseChallengeStatusAdapter(opponentdayDateWiseStatusModel, KmChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    recyclerview_days_status_down.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_down.setAdapter(opponentDayDateWiseChallengeStatusAdapter);
                                    if(serviceflag==0)
                                        getStatus();

////////////////////////////////////////END/////////////////////////////////////////////////////

                                    String opponent_name = jsonObject4.getString("opponent_name");

                                    String opponent_rank = jsonObject4.getString("opponent_rank");


                                    String opponent_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_image");
                                    String opponent_group_name = jsonObject4.getString("opponent_group_name");
                                    String opponent_group_rank = jsonObject4.getString("opponent_group_rank");
                                    String opponent_group_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_group_image");
                                    String opponent_super_admin_name = jsonObject4.getString("opponent_super_admin_name");
                                    String opponent_super_admin_rank = jsonObject4.getString("opponent_super_admin_rank");
                                    String opponent_super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_super_admin_image");


                                    if (opponent_usertype.equals("USER")) {
                                        participent_oponent_name.setText(opponent_name);
                                        challenger_two_rank.setText(opponent_rank);
                                        participent_oponent_type.setText(opponent_usertype);
                                        challenger_two_steps.setText(opponent_completed_goal + " KM(S)");

                                        Picasso.with(KmChallengeStatusActivity.this)
                                                .load(opponent_image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusKmsChallengeDB.OPPONENT_NAME, opponent_name);
                                        values.put(StatusKmsChallengeDB.OPPONENT_RANK, opponent_rank);
                                        values.put(StatusKmsChallengeDB.OPPONENT_TYPE, opponent_usertype);
                                        values.put(StatusKmsChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_completed_goal);
                                        values.put(StatusKmsChallengeDB.OPPONENT_IMAGE, opponent_image);
                                    } else {
                                        participent_oponent_name.setText(opponent_group_name);
                                        challenger_two_rank.setText(opponent_group_rank);
                                        participent_oponent_type.setText(opponent_usertype);
                                        challenger_two_steps.setText(opponent_completed_goal + " KM(S)");
                                        Picasso.with(KmChallengeStatusActivity.this)
                                                .load(opponent_group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusKmsChallengeDB.OPPONENT_NAME, opponent_group_name);
                                        values.put(StatusKmsChallengeDB.OPPONENT_RANK, opponent_group_rank);
                                        values.put(StatusKmsChallengeDB.OPPONENT_TYPE, opponent_usertype);
                                        values.put(StatusKmsChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_completed_goal);
                                        values.put(StatusKmsChallengeDB.OPPONENT_IMAGE, opponent_group_image);
                                    }
                                    Log.d("dfjgsdfhh", "" + values);
                                    boolean isExit=statusKmsChallengeDB.CheckIsDataAlreadyInDBorNot(challenge_id);
                                    if(!isExit){
                                        statusKmsChallengeDB.addKms(values);
                                    }

                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(KmChallengeStatusActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(KmChallengeStatusActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("KMREPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(KmChallengeStatusActivity.this);
            requestQueue.add(strRe);
        } else {
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            getStatus();
            statusKmsData();
            //Toast.makeText(KmChallengeStatusActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        msgflag = 0;
                        String total="";
                        float latest = statusKmsChallengeDB.latestGoal(challenge_id);
                        if(latest>0){
                            total=String.valueOf(latest);
                        }else {
                            String tmp = challenger_one_steps.getText().toString();
                            String[] totald = tmp.split(" ");
                            total=totald[0];
                        }

                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, total, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                    KmChallengeStatusActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {


                    KmChallengeStatusActivity.super.onBackPressed();
                }
            });
            builder.show();
        }

        if (v == memb_count_top) {
            Intent top = new Intent(KmChallengeStatusActivity.this, MemberCountChllengeStatusActivity.class);
            top.putExtra("challenge_id", challenge_id);
            top.putExtra("group_id", group_id);
            startActivity(top);
        }
        if (v == mem_cccount_down) {
            Intent bottom = new Intent(KmChallengeStatusActivity.this, MemberCountChllengeStatusActivity.class);
            bottom.putExtra("challenge_id", challenge_id);
            bottom.putExtra("group_id", group_id);
            startActivity(bottom);
        }
        if(v==serviceBtn){
            sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
            // sensoreditor.putString("challengeId",challenge_id);
            boolean isStarted=sensorPreference.getBoolean("isStarted",false);
            String challengeId=sensorPreference.getString("challengeId","");
            //if(challengeId!=null && challengeId.length()>0){
            if(!challengeId.equalsIgnoreCase(challenge_id) && isStarted){
                String name=sensorPreference.getString("activity","");
                showPreviousService(name);
            }else {
                sensoreditor.putString("challengeId",challenge_id);
                sensoreditor.putString("activity",activity_name.getText().toString());
                String text=serviceBtn.getText().toString();
                if(text.equalsIgnoreCase("STOP")){
                    serviceBtn.setText("START");
                    sensoreditor.putBoolean("isStarted",false);
                     sensoreditor.apply();
                    update.setVisibility(View.GONE);
                    serviceBtn.setBackgroundColor(Color.GREEN);
                    if(myGpsService==null)
                        myGpsService=new Intent(KmChallengeStatusActivity.this,GpsService.class);
                    stopService(myGpsService);
                    if(mTimer!=null)
                    mTimer.cancel();
                    clearData();

                }else {
                    serviceBtn.setText("STOP");
                    sensoreditor.putBoolean("isStarted",true);
                    // sensoreditor.putBoolean("isService",true);
                    serviceBtn.setBackgroundColor(Color.RED);
                    mIsSensorUpdateEnabled =true;
                     isFirstTime=true;
                    SharedPreferences sharedPreferences=getSharedPreferences("KM",MODE_PRIVATE);
                    if (gps.canGetLocation()) {
                        double newLatitude = gps.getLatitude();
                        double newLongitude = gps.getLongitude();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("Latitude", String.valueOf(newLatitude));
                        editor.putString("Longitude", String.valueOf(newLongitude));
                        editor.apply();
                    }
                    initGpsListeners();
                    mTimer = new Timer();
                    mTimer.scheduleAtFixedRate(new GetLocation(), 0, 60000);

                }
                sensoreditor.apply();
            }

        }
        if(v==update){
            try {
                msgflag = 0;
                String total="";
                float latest = statusKmsChallengeDB.latestGoal(challenge_id);
                if(latest>0){
                    total=String.valueOf(latest);
                }else {
                    String tmp = challenger_one_steps.getText().toString();
                    String[] totald = tmp.split(" ");
                    total=totald[0];
                }

                sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, total, android_id, Build.BRAND, Build.DEVICE);
            } catch (Exception e) {

            }
        }
        if(v==gps_track_img){
            Intent it=new Intent(KmChallengeStatusActivity.this,GPSTrackActivity.class);
            it.putExtra("challengeId",challenge_id);
            it.putExtra("userid",user_id);
            startActivity(it);
        }
    }


    private void initGpsListeners()
    {
        SharedPreferences preferences=getSharedPreferences("GPS",MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("challengeId",challenge_id);
        editor.apply();
        myGpsService = new Intent(KmChallengeStatusActivity.this, GpsService.class);
        startService(myGpsService);

    }

    //show Alert with previous Challenge
    private void  showPreviousService(String activity){

        AlertDialog alertDialog = new AlertDialog.Builder(
                KmChallengeStatusActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Challenge");

        // Setting Dialog Message
        alertDialog.setMessage("Already Running "+ Html.fromHtml("<font color='#026c9b'>"+activity+"</font>")+"Challenge\nPlease stop challenge before start it.");

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public float getDistanceRun(long steps) {
        checkInternet = NetworkChecking.isConnected(KmChallengeStatusActivity.this);


        if (checkInternet) {
            update.setVisibility(View.VISIBLE);
            distance = (float) (steps * 78) / (float) 100000;
            Log.d("fbxxbnxvb", "" + distance);

            steps_count = String.valueOf(steps);
            float latest = latestDistance + distance;
            Log.v("stepslatest", ">>>" + distance);
          //  Log.v("stepslast", ">>>" + latestDistance);
            distance_calc = String.valueOf(latest);
           // Log.v("stepstotal", ">>>" + latest);
            SharedPreferences preferences = getSharedPreferences("Distance", MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();
            editor.putString("distance",distance_calc);
            editor.apply();
            challenger_one_steps.setText(distance_calc + " KM(S)");
            statusKmsChallengeDB.updateGoal(String.valueOf(distance_calc), challenge_id);

       /* mBuilder.setContentTitle(steps_count)
                .setContentText(distance_calc+" meters")
                .setSmallIcon(R.drawable.challenges_icon);*/
            String value = steps_count;
            try {
                int progress = Integer.parseInt(value);
                if (progress >= 0 && progress <= 100) {
               /* mBuilder.setProgress(Integer.parseInt(challenge_goal), progress, false);
                mNotifyManager.notify(id, mBuilder.build());*/
                }
            } catch (Exception e) {
                Log.e("", "Value set:" + value);
                e.printStackTrace();
            }
           /* new Thread(new Runnable() {
                @Override
                public void run() {

                    while (true) {
                        try {
                            Thread.sleep(100000);
                            sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, distance_calc, android_id, Build.BRAND, Build.DEVICE);
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();*/
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        msgflag = 0;
                        String total="";
                        float latest = statusKmsChallengeDB.latestGoal(challenge_id);
                        if(latest>0){
                            total=String.valueOf(latest);
                        }else {
                            String tmp = challenger_one_steps.getText().toString();
                            String[] totald = tmp.split(" ");
                            total=totald[0];
                        }

                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, total, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });
        } else {
            update.setVisibility(View.VISIBLE);
            distance = (float) (steps * 78) / (float) 100000;
            //  float challengecount = Float.parseFloat(user_completed_goal);
            //  float total_value = distance+challengecount;
            Log.d("fbxxbnxvb", "" + distance);
            steps_count = String.valueOf(steps);
            distance_calc = String.valueOf(distance);

            float latest = latestDistance + distance;
           /* Log.v("stepslatest", ">>>" + distance);
            Log.v("stepslast", ">>>" + latestDistance);*/
            distance_calc = String.valueOf(latest);
           // Log.v("stepstotal", ">>>" + latest);
            statusKmsChallengeDB.updateGoal(String.valueOf(latest), challenge_id);
            challenger_one_steps.setText("" + latest + " KM(S)");
            SharedPreferences preferences = getSharedPreferences("Distance", MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();
            editor.putString("distance",distance_calc);
            editor.apply();

            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        SharedPreferences preferences = getSharedPreferences("Distance", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("distance", "");
                        editor.apply();
                        String total = challenger_one_steps.getText().toString();
                        String[] totald = total.split(" ");

                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totald[0], android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });
        }
        return distance;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //   sManager.unregisterListener(this, stepSensor);
    }

    @Override
    protected void onPause() {
        super.onPause();
        String text=serviceBtn.getText().toString();
        if(text.equalsIgnoreCase("STOP")){
            mTimer.cancel();
            startService(new Intent(KmChallengeStatusActivity.this,CalculateDistance.class));
        }else {
            stopService(new Intent(KmChallengeStatusActivity.this,CalculateDistance.class));
        }
       isRunning = false;
       // detectorTimeStampUpdaterThread.interrupt();

    }

    @Override
    protected void onResume() {
        super.onResume();

       try {
            float latest = statusKmsChallengeDB.latestGoal(challenge_id);
            challenger_one_steps.setText(""+latest+" KM(S)");

            latestDistance=latest;
            Log.v("distance13333",">>>>"+latestDistance);
        }catch (Exception e){
            e.printStackTrace();
        }
        stopService(new Intent(KmChallengeStatusActivity.this,CalculateDistance.class));
       if(!isRunning){
           String text=serviceBtn.getText().toString();
           if(text !=null && text.equalsIgnoreCase("STOP")){
               mTimer = new Timer();
               mTimer.scheduleAtFixedRate(new GetLocation(), 0, 60000);
           }
       }




    }

    public void sendChallenge(final String activity_id, final String challenge_id, final String user_id, final String evaluation_factor, final String evaluation_factor_unit, final String challenge_goal, final String android_id, final String brand, final String model) {
        checkInternet = NetworkChecking.isConnected(KmChallengeStatusActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS;
            Log.d("fbnxbjkx", url);

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHAL_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                      serviceflag=1;
                                      getKMChallangesStatus();
                                    if (msgflag == 0)
                                       Toast.makeText(KmChallengeStatusActivity.this, "Challenge Updating", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(KmChallengeStatusActivity.this, "Challenge Updating Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(KmChallengeStatusActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    if (user_type_for_challenge.equals("USER")) {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", "");
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec", String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    } else {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", group_id);
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec", String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    }
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(KmChallengeStatusActivity.this);
            requestQueue.add(stringRequest);

        } else {

            // Toast.makeText(KmChallengeStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {

            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //show a No Internet Alert or Dialog
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {

            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);

            // Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            // dismiss the dialog or refresh the activity
        }
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {

            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //show a No Internet Alert or Dialog
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {

            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);

            // Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            // dismiss the dialog or refresh the activity
        }
    }

    private void statusKmsData() {
        //  Toast.makeText(KmChallengeStatusActivity.this, "Fetching", Toast.LENGTH_LONG).show();

        String challengeId = statusKmsChallengeDB.ChallengeIDstr(challenge_id);
        Log.v("ChallengeId", challengeId);


        List<String> challeneg_Ids = statusKmsChallengeDB.getChallengeID();

        List<String> activityName = statusKmsChallengeDB.getActivityName();

        List<String> user_type = statusKmsChallengeDB.getUserType();
        List<String> challenge_goal = statusKmsChallengeDB.getChallengeGoal();
        List<String> amount = statusKmsChallengeDB.getAmount();
        List<String> activity_name_string = statusKmsChallengeDB.getActivityName();

        List<String> user_name = statusKmsChallengeDB.getUserName();
        List<String> user_rank = statusKmsChallengeDB.getUserRank();
        List<String> image = statusKmsChallengeDB.getImage();
        List<String> oppentName = statusKmsChallengeDB.getopponentName();
        List<String> oppentrank = statusKmsChallengeDB.getopponentrank();

        List<String> oppentImage = statusKmsChallengeDB.getopponentImge();
        List<String> oppenttype = statusKmsChallengeDB.getopponentType();

        List<String> user_complet_goal = statusKmsChallengeDB.getUserCompletedGoal();

        List<String> opponentgoal = statusKmsChallengeDB.getopponentgoal();

        for (int i = 0; i < challeneg_Ids.size(); i++) {
            if (challeneg_Ids.get(i).equalsIgnoreCase(challengeId)) {
                activity_name.setText(activity_name_string.get(i));
                Log.v("User", activity_name_string.get(i));
                cash_price.setText(amount.get(i));
                activity_name.setText(activity_name_string.get(i));
                usd_type_in = user_type.get(i);
                goal.setText(challenge_goal.get(i));
                participent_user_name.setText(user_name.get(i));
                participent_user_type.setText(usd_type_in);
                Log.v("latestDOff", user_complet_goal.get(i));
                latestDistance = Float.parseFloat(user_complet_goal.get(i));

                challenger_one_steps.setText(latestDistance + " KM(S)");
                challenger_one_rank.setText(user_rank.get(i));
                Picasso.with(KmChallengeStatusActivity.this)
                        .load(image.get(i))
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(challenger_one_image);


                String opponenttype = oppenttype.get(i);
                if (opponenttype.equals("ADMIN")) {
                    ll_down.setVisibility(View.GONE);
                    vsTxt.setVisibility(View.GONE);
                } else {
                    ll_down.setVisibility(View.VISIBLE);
                    vsTxt.setVisibility(View.VISIBLE);
                    participent_oponent_name.setText(oppentName.get(i));
                    if(opponentType.contains("Group") || opponentType.contains("GROUP"))
                        participent_oponent_type.setText("GROUP");
                    else
                        participent_oponent_type.setText("USER");
                    challenger_two_rank.setText(oppentrank.get(i));
                    challenger_two_steps.setText(opponentgoal.get(i) + " KM(S)");
                    Picasso.with(KmChallengeStatusActivity.this)
                            .load(oppentImage.get(i))
                            .placeholder(R.drawable.dummy_user_profile)
                            .into(challenger_two_image);

                }

            }
        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    msgflag = 0;

                    String total="";
                    float latest = statusKmsChallengeDB.latestGoal(challenge_id);
                    if(latest>0){
                        total=String.valueOf(latest);
                    }else {
                        String tmp = challenger_one_steps.getText().toString();
                        String[] totald = tmp.split(" ");
                        total=totald[0];
                    }
                    sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, total, android_id, Build.BRAND, Build.DEVICE);
                } catch (Exception e) {

                }
                KmChallengeStatusActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                KmChallengeStatusActivity.super.onBackPressed();

            }
        });
        builder.show();
    }

    public void updateThread() {
        String total="";
        float latest = statusKmsChallengeDB.latestGoal(challenge_id);
        if(latest>0){
            total=String.valueOf(latest);
        }else {
            String tmp = challenger_one_steps.getText().toString();
            String[] totald = tmp.split(" ");
            total=totald[0];
        }


        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, total, android_id, Build.BRAND, Build.DEVICE);
    }




  private void clearData(){
      SharedPreferences sharedPreferences=getSharedPreferences("KM",MODE_PRIVATE);
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString("Latitude","");
      editor.putString("Longitude", "");
      editor.apply();
      new GetLocation().cancel();
  }



    class GetLocation extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    calculateDistance();
                }
            });
        }
    }


    private void calculateDistance(){
        Log.v("TTTTTTTTTT","FFFFFFFFFFFFFFFFFF");
        SharedPreferences sharedPreferences=getSharedPreferences("KM",MODE_PRIVATE);
        if (sharedPreferences.getString("Latitude", "").length() > 0) {
            latitude = Double.parseDouble(sharedPreferences.getString("Latitude", ""));
            longitude = Double.parseDouble(sharedPreferences.getString("Longitude", ""));
        }

        // display toast
        GPSTracker gps = new GPSTracker(this);

        // check if GPS enabled
        if (gps.canGetLocation()) {
            double newLatitude = gps.getLatitude();
            double newLongitude = gps.getLongitude();
                      //  Log.v("old Lat: " + latitude, "" + "old Long: " + longitude);
                     //  Log.v("Lat: " + newLatitude, "" + "Long: " + newLongitude);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Latitude", String.valueOf(newLatitude));
            editor.putString("Longitude", String.valueOf(newLongitude));
            editor.apply();

            if (latitude > 0) {
                Location crntLocation = new Location("crntlocation");
                crntLocation.setLatitude(latitude);
                crntLocation.setLongitude(longitude);

                Location newLocation = new Location("newlocation");
                newLocation.setLatitude(newLatitude);
                newLocation.setLongitude(newLongitude);

                float distance = crntLocation.distanceTo(newLocation);       //   in meters
                Log.v("distance", " >>> " + distance);
                if(distance>5){
                    update.setVisibility(View.VISIBLE);
                    double value=distance * 0.001;
                    float latest=latestDistance+Float.parseFloat(String.valueOf(value));
                    statusKmsChallengeDB.updateGoal(String.valueOf(round(latest,4)),challenge_id);
                    SharedPreferences preferences=getSharedPreferences("Distance",MODE_PRIVATE);
                    SharedPreferences.Editor editor1=preferences.edit();
                    editor1.putFloat("latestdistance",round(latest,4));
                    editor1.apply();
                    challenger_one_steps.setText(""+round(latest,4)+" KM(S)");
                }



            }

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }




    //get start and stop based on status
    private void getStatus(){
        gps = new GPSTracker(this);
        sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
        sensoreditor = sensorPreference.edit();
        isStarted=sensorPreference.getBoolean("isStarted",false);
        final String challengeId=sensorPreference.getString("challengeId","");
        if(challengeId.equalsIgnoreCase(challenge_id)){
            if(isStarted){
                serviceBtn.setText("STOP");
                serviceBtn.setBackgroundColor(Color.RED);
                mIsSensorUpdateEnabled =true;
                SharedPreferences sharedPreferences=getSharedPreferences("KM",MODE_PRIVATE);
                if (gps.canGetLocation()) {
                    double newLatitude = gps.getLatitude();
                    double newLongitude = gps.getLongitude();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("Latitude", String.valueOf(newLatitude));
                    editor.putString("Longitude", String.valueOf(newLongitude));
                    editor.apply();
                }
                 initGpsListeners();
                mTimer = new Timer();
                mTimer.scheduleAtFixedRate(new GetLocation(), 0, 60000);

            }else{
                serviceBtn.setText("START");
                serviceBtn.setBackgroundColor(Color.GREEN);
                if(mTimer!=null)
                    mTimer.cancel();
                clearData();
                if(myGpsService==null)
                    myGpsService=new Intent(KmChallengeStatusActivity.this,GpsService.class);
                stopService(myGpsService);
            }


        }
    }

    public static float round(float d, int decimalPlace) {
        return BigDecimal.valueOf(d).setScale(decimalPlace,BigDecimal.ROUND_HALF_UP).floatValue();
    }
}
