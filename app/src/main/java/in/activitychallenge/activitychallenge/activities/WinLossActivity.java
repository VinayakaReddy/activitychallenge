package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.adapter.WinLossChalngeAdapter;
import in.activitychallenge.activitychallenge.fragments.AllWinLossFragment;
import in.activitychallenge.activitychallenge.fragments.CategoryWiseWinLossFragment;
import in.activitychallenge.activitychallenge.models.WinLossChllengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class WinLossActivity extends AppCompatActivity implements View.OnClickListener{

    ViewPager view_pager_win_loss;
    TabLayout tab;
    MyChllengesViewPagerAdapter viewpageradapter;
    ImageView close, button_filter;
    MenuItem menuitem;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id,mState;

    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    Integer tabSelected = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(WinLossActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);



        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
     // button_filter = (ImageView) findViewById(R.id.button_filter);
      //  button_filter.setOnClickListener(this);

        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);


        tab = (TabLayout) findViewById(R.id.tabLayout_win_loss);
        view_pager_win_loss = (ViewPager) findViewById(R.id.view_pager_win_loss);
        setupViewPager(view_pager_win_loss);
        tab.setupWithViewPager(view_pager_win_loss);

        getCount();


    }

    private void setupViewPager(ViewPager view_pager_suggest) {
        viewpageradapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());
        viewpageradapter.addFrag(new AllWinLossFragment(), "ALL");
        viewpageradapter.addFrag(new CategoryWiseWinLossFragment(), "ACTIVITY WISE");

        view_pager_suggest.setAdapter(viewpageradapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_win_loss) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 1)
                        {
                              Toolbar toolbar= (Toolbar) WinLossActivity.this.findViewById(R.id.toolbar);
                              toolbar.getMenu().clear();



                        }
                        else if(tab.getPosition() == 0)
                        {

                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }



    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
       /* if (view == button_filter) {
            Intent intent = new Intent(WinLossActivity.this, FilterChllengesActivity.class);
            startActivityForResult(intent, 2);
        }*/

        if (view == linear_sponsor) {
            Intent sponsor = new Intent(WinLossActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);

        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(WinLossActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);

        }
        if (view == linear_messges) {
            Intent msg = new Intent(WinLossActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);

        }
    }


    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(WinLossActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(WinLossActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }



}
