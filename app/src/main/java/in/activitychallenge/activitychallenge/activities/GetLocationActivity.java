package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.activitychallenge.activitychallenge.BannerLayout;
import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.WebBannerAdapter;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class GetLocationActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final int PLACE_PICKER_REQUEST = 1;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    EditText current_location;
    Button next;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id = "", email = "", mobile = "", payment_info = "", device_id = "", token = "";
    private ImageView image;
    private float currentDegree = 0f;
    SensorManager mSensorManager;
    TextView tvHeading, loc, more, location_text;
    LinearLayout current_location_layout, other_location_layout, pick_layout, spinner_layout;
    String send_lat = "", send_lng = "", send_city = "", send_area = "", send_pincode = "";
    String location_name = "", location_lat = "", location_lng = "";
    ImageView close, banner_image;
    String startdate, enddate,goal;
    int noofdays;
    String event;
    JSONArray locationArr;
    AppCompatSpinner location_spinner;
    ArrayList<String> location_drop_list = new ArrayList<>();
    ArrayList<String> location_lattitude_list = new ArrayList<>();
    ArrayList<String> location_longitude_list = new ArrayList<>();
    /////////////////////////
    BannerLayout bannerVertical;
    WebBannerAdapter webBannerAdapter;
    ArrayList<AllPromotionsModel> promoList = new ArrayList<AllPromotionsModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);
        bannerVertical = findViewById(R.id.recycler_ver);
        close = (ImageView) findViewById(R.id.close);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        close.setOnClickListener(this);
        image = (ImageView) findViewById(R.id.imageViewCompass);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        loc = (TextView) findViewById(R.id.loc);
        more = (TextView) findViewById(R.id.more);
        event = getIntent().getStringExtra("event");
        if (event.equalsIgnoreCase("special")) {
            startdate = getIntent().getStringExtra("startdate");
            enddate = getIntent().getStringExtra("enddate");
            noofdays = getIntent().getIntExtra("noofdays", 0);
            goal=getIntent().getStringExtra("minvalue");
            String tmparr = getIntent().getStringExtra("location");
            try {
                locationArr = new JSONArray(tmparr);
                location_drop_list.clear();
                if (locationArr.length() > 0) {
                    for (int i = 0; i < locationArr.length(); i++) {

                        JSONObject jsonObject = (JSONObject) locationArr.get(i);
                        String address = (String) jsonObject.get("address");
                        String lattitude= String.valueOf( jsonObject.get("latitude"));
                        String longtitude=  String.valueOf( jsonObject.get("latitude"));

                        location_drop_list.add(address);
                        location_lattitude_list.add(lattitude);
                        location_longitude_list.add(longtitude);

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        location_text = (TextView) findViewById(R.id.location_text);
        pick_layout = (LinearLayout) findViewById(R.id.pick_layout);
        spinner_layout = (LinearLayout) findViewById(R.id.spinner_layout);
        location_spinner = (AppCompatSpinner) findViewById(R.id.spinner_location);
        current_location_layout = (LinearLayout) findViewById(R.id.current_location_layout);
        other_location_layout = (LinearLayout) findViewById(R.id.other_location_layout);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        session = new UserSessionManager(GetLocationActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        email = userDetails.get(UserSessionManager.USER_EMAIL);
        mobile = userDetails.get(UserSessionManager.MOBILE_NO);
        payment_info = userDetails.get(UserSessionManager.PAYMENT_STATUS);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("dgafd", user_id + "////" + mobile + "////" + payment_info + "////" + email);
        current_location = (EditText) findViewById(R.id.current_location);
        next = (Button) findViewById(R.id.next);
        gps = new GPSTracker(GetLocationActivity.this);
        geocoder = new Geocoder(this, Locale.getDefault());
        getAds();
        if (event.equalsIgnoreCase("special")) {
            pick_layout.setVisibility(View.GONE);
            spinner_layout.setVisibility(View.VISIBLE);
            if (location_drop_list.size() > 1)
                location_text.setVisibility(View.VISIBLE);
            else
                location_text.setVisibility(View.GONE);
            location_name = location_drop_list.get(0);
            location_lat=location_longitude_list.get(0);
            location_lng=location_longitude_list.get(0);
            location_spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, location_drop_list));
            location_spinner.setOnItemSelectedListener(this);


        } else {
            pick_layout.setVisibility(View.VISIBLE);
            spinner_layout.setVisibility(View.GONE);
        }
        webBannerAdapter = new WebBannerAdapter(GetLocationActivity.this, promoList);
        webBannerAdapter.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (promoList.get(position).getEntity_id().equals(user_id)) {

                } else {
                    if (promoList.get(position).getEntity_type().equals("USER")) {
                        Intent intent = new Intent(GetLocationActivity.this, MemberDetailActivity.class);
                        intent.putExtra("MEMBER_ID", promoList.get(position).getEntity_id());
                        intent.putExtra("MEMBER_NAME", promoList.get(position).getEntity_name());
                        intent.putExtra("member_user_type", promoList.get(position).getEntity_type());
                        startActivity(intent);
                    } else if (promoList.get(position).getEntity_type().equals("GROUP")) {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(GetLocationActivity.this, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoList.get(position).getEntity_id());
                        intent.putExtra("grp_name", promoList.get(position).getEntity_name());
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoList.get(position).getEntity_type());
                        intent.putExtra("grp_admin_id", promoList.get(position).getAdmin_id());

                        startActivity(intent);
                    } else {

                    }
                }
                //  Toast.makeText(AllPromotionsActivity.this, " " + position, Toast.LENGTH_SHORT).show();
            }
        });

        current_location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(GetLocationActivity.this);
                if (checkInternet) {
                    try {
                        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                        Intent intent = intentBuilder.build(GetLocationActivity.this);
                        startActivityForResult(intent, PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException
                            | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(GetLocationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(GetLocationActivity.this);
                if (checkInternet) {

                    Intent more = new Intent(GetLocationActivity.this, AllPromotionsActivity.class);
                    startActivity(more);
                } else {
                    Toast.makeText(GetLocationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        other_location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(GetLocationActivity.this);
                if (checkInternet) {

                    final Dialog dialog = new Dialog(GetLocationActivity.this);
                    dialog.setContentView(R.layout.custom_dialog_layout);
                    dialog.setTitle("Custom Dialog");
                    final EditText text = (EditText) dialog.findViewById(R.id.textDialog);
                    dialog.show();
                    Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
                    declineButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (text.getText().toString().length() > 6) {
                                dialog.dismiss();
                                loc.setVisibility(View.VISIBLE);
                                loc.setText("Other Location");
                                current_location.setVisibility(View.VISIBLE);
                                current_location.setText(text.getText().toString());
                                location_name = current_location.getText().toString().trim();
                            } else {
                                Toast.makeText(GetLocationActivity.this, "Please provide correct Location Name", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(GetLocationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // check if GPS enabled
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //   Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);

            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                location_name = current_location.getText().toString();
                location_lat = lat;
                location_lng = lng;
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(GetLocationActivity.this);
                if (checkInternet) {

                    if (!location_name.equals("")) {
                        if(event.equalsIgnoreCase("special")){
                            Intent it=new Intent(GetLocationActivity.this,SpecialEventCalenderActivity.class);
                            it.putExtra("activity","Main");
                            it.putExtra("startdate",startdate);
                            it.putExtra("enddate",enddate);
                            it.putExtra("noofdays",noofdays);
                            it.putExtra("locationname",location_name);
                            it.putExtra("lattitude",location_lat);
                            it.putExtra("longitude",location_lng);
                            it.putExtra("goal",goal);
                            it.putExtra("specialeventtype",getIntent().getExtras().getString("type"));
                            it.putExtra("activityname",getIntent().getExtras().getString("activity_name"));
                            it.putExtra("specialactivityid",getIntent().getExtras().getString("id"));
                            startActivity(it);
                        }else {
                            if (getIntent().getExtras().getString("type").equals("5000steps")) {
                                Intent intent = new Intent(GetLocationActivity.this, StepsChallengeActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else if (getIntent().getExtras().getString("type").equals("30mins")) {
                                Intent intent = new Intent(GetLocationActivity.this, ThirtyMinsActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else if (getIntent().getExtras().getString("type").equals("45mins")) {
                                Intent intent = new Intent(GetLocationActivity.this, FortyfiveMinsActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else if (getIntent().getExtras().getString("type").equals("60mins")) {
                                Intent intent = new Intent(GetLocationActivity.this, SixtyMinsActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else if (getIntent().getExtras().getString("type").equals("5km")) {
                                Intent intent = new Intent(GetLocationActivity.this, FiveKmsActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else if (getIntent().getExtras().getString("type").equals("hollow_pictures")) {
                                Intent intent = new Intent(GetLocationActivity.this, PicturePosesActivity.class);
                                intent.putExtra("id", getIntent().getExtras().getString("id"));
                                intent.putExtra("user_id", getIntent().getExtras().getString("user_id"));
                                intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                                intent.putExtra("daily_challenges", getIntent().getExtras().getString("daily_challenges"));
                                intent.putExtra("weekly_challenges", getIntent().getExtras().getString("weekly_challenges"));
                                intent.putExtra("long_challenges", getIntent().getExtras().getString("long_challenges"));
                                intent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
                                intent.putExtra("location_name", current_location.getText().toString());
                                intent.putExtra("location_lat", location_lat);
                                intent.putExtra("location_lng", location_lng);
                                startActivity(intent);
                            } else {
                                Toast.makeText(GetLocationActivity.this, "No Challenges Found", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        Toast.makeText(GetLocationActivity.this, "please provide the location", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(GetLocationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(GetLocationActivity.this, data);
            String stringlat = place.getLatLng().toString();
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            final LatLng latlng = place.getLatLng();
            double lat_value = latlng.latitude;
            String lat = String.valueOf(lat_value);
            double lng_value = latlng.longitude;
            String lng = String.valueOf(lng_value);
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            // Toast.makeText(GetLocationActivity.this, "" + name + " - " + address, Toast.LENGTH_SHORT).show();
            Log.d("map_selected_adderss", "" + name + " - " + address);
            loc.setVisibility(View.VISIBLE);
            loc.setText("Current Location");
            current_location.setVisibility(View.VISIBLE);
            current_location.setText("" + name + " - " + address);
            location_name = current_location.getText().toString();
            location_lat = lat;
            location_lng = lng;

        } else {
            // super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void sendUserData(final String user_id, final String device_id, final String lat, final String lng, final String city, final String country, final String state, final String place, final String token) {
        checkInternet = NetworkChecking.isConnected(GetLocationActivity.this);
        if (checkInternet) {


            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FEEDBACK);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CURRENT_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("REPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    //     Toast.makeText(GetLocationActivity.this, "Location Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("lat", lat);
                    params.put("long", lng);
                    params.put("city", city);
                    params.put("country", country);
                    params.put("state", state);
                    params.put("place", place);
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(GetLocationActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(GetLocationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        float degree = Math.round(sensorEvent.values[0]);
        tvHeading.setText("Heading: " + Float.toString(degree) + " degrees");
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        ra.setDuration(210);//Vinayak@7
        ra.setFillAfter(true);
        image.startAnimation(ra);
        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    private void getAds() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("PROMOTION_BANNER_RESPON", response);
                                int count = jsonObject.getInt("count");
                                if (count > 1) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.VISIBLE);
                                }
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AllPromotionsModel rhm = new AllPromotionsModel();
                                        JSONObject itemArray = jsonArray.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setBanner_path(AppUrls.BASE_IMAGE_URL + itemArray.getString("banner_path"));
                                        rhm.setStatus(itemArray.getString("status"));
                                        rhm.setEntity_id(itemArray.getString("entity_id"));
                                        rhm.setEntity_type(itemArray.getString("entity_type"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setEntity_name(itemArray.getString("entity_name"));
                                        promoList.add(rhm);
                                    }
                                    bannerVertical.setAdapter(webBannerAdapter);
                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray1.getJSONObject(0);
                                    String image = AppUrls.BASE_IMAGE_URL + jobj.getString("banner_path");
                                    final String banner_user_id = jobj.getString("entity_id");
                                    final String banner_status = jobj.getString("status");
                                    final String banner_user_type = jobj.getString("entity_type");
                                    final String banner_user_name = jobj.getString("entity_name");
                                    final String admin_id = jobj.getString("admin_id");


                                    if (banner_status.equals("PAID")) {
                                        if (banner_user_id.equals(user_id)) {

                                        } else {
                                            banner_image.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (banner_user_type.equals("USER")) {
                                                        Intent intent = new Intent(GetLocationActivity.this, MemberDetailActivity.class);
                                                        intent.putExtra("MEMBER_ID", banner_user_id);
                                                        intent.putExtra("MEMBER_NAME", banner_user_name);
                                                        intent.putExtra("member_user_type", banner_user_type);
                                                        startActivity(intent);
                                                    } else if (banner_user_type.equals("GROUP")) {
                                                        Intent intent = new Intent(GetLocationActivity.this, GroupDetailActivity.class);
                                                        intent.putExtra("grp_id", banner_user_id);
                                                        intent.putExtra("MEMBER_NAME", banner_user_name);
                                                        intent.putExtra("GROUP_CONVERSATION_TYPE", banner_user_type);
                                                        intent.putExtra("grp_admin_id", admin_id);
                                                        startActivity(intent);
                                                        Toast.makeText(GetLocationActivity.this, "Group", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                    }
                                                }
                                            });
                                        }
                                    } else {

                                    }


                                    Picasso.with(GetLocationActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(banner_image);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GetLocationActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        location_name = location_drop_list.get(i);
        location_lat=location_longitude_list.get(i);
        location_lng=location_longitude_list.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
