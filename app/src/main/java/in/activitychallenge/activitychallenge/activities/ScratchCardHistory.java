package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.ScaratchCardHistoryAdapter;
import in.activitychallenge.activitychallenge.models.ScratchcardHistoryModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ScratchCardHistory extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    ImageView close;
    CollapsingToolbarLayout collapse_card_history_toolbar;
    String user_id, user_type, token, devive_id;
    RecyclerView recycler_card_view_history;
    ScaratchCardHistoryAdapter scratch_card_historyAdapter;
    ArrayList<ScratchcardHistoryModel> historyListModel = new ArrayList<ScratchcardHistoryModel>();
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    TextView reward;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_card_history);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        devive_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("DETAI", user_id + "//" + user_type);
        recycler_card_view_history = (RecyclerView) findViewById(R.id.recycler_card_view_history);
        recycler_card_view_history.setHasFixedSize(true);
        scratch_card_historyAdapter = new ScaratchCardHistoryAdapter(historyListModel, ScratchCardHistory.this, R.layout.row_scratch_history);
        gridLayoutManager = new GridLayoutManager(this, 2);
        layoutManager = new LinearLayoutManager(this);
        recycler_card_view_history.setLayoutManager(gridLayoutManager);
        collapse_card_history_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_card_history_toolbar);
        collapse_card_history_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_card_history_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_card_history_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);

        getCardHistory();
    }

    public void getCardHistory() {
        historyListModel.clear();
        checkInternet = NetworkChecking.isConnected(ScratchCardHistory.this);
        if (checkInternet) {
            progressDialog.show();
            //http://192.168.1.61:8090/api/v1/payment/scratch_card_history?user_id=59f03caf1f3c56e80e000029&user_type=USER
            final String url = AppUrls.BASE_URL + AppUrls.SCRATCH_CARD_HISTORY + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("historyURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RESPOHIST", response);
                                String editSuccessResponceCode = jsonObject.getString("response_code");
                                String total_amount = jsonObject.getString("amount");
                                Log.d("AMOUNT", total_amount);
                                collapse_card_history_toolbar.setTitle(Html.fromHtml("&#36;" + "<b>" + total_amount + "</b>"));
                                if (editSuccessResponceCode.equalsIgnoreCase("10100"))
                                {
                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                        ScratchcardHistoryModel history = new ScratchcardHistoryModel();
                                        history.setTitle(jsonObject1.getString("title"));
                                        history.setTxn_type(jsonObject1.getString("txn_type"));
                                        history.setTxn_flag(jsonObject1.getString("txn_flag"));
                                        history.setTxn_status(jsonObject1.getString("txn_status"));
                                        history.setAmount(jsonObject1.getString("amount"));
                                        history.setCreated_on_txt(jsonObject1.getString("created_on_txt"));
                                        history.setCreated_on(jsonObject1.getString("created_on"));
                                        historyListModel.add(history);

                                    }
                                    recycler_card_view_history.setAdapter(scratch_card_historyAdapter);
                                }
                                if(editSuccessResponceCode.equalsIgnoreCase("10200"))
                                {
                                    Toast.makeText(ScratchCardHistory.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if(editSuccessResponceCode.equalsIgnoreCase("10300"))
                                {
                                    Toast.makeText(ScratchCardHistory.this, "No Data Found", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            // no_data.setVisibility(View.VISIBLE);
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", devive_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ScratchCardHistory.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(ScratchCardHistory.this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
