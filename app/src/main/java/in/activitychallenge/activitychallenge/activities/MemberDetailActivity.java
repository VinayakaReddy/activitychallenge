package in.activitychallenge.activitychallenge.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.GrpInMemberDetailAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberIndividCompletAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberIndividRunAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberIndividUpcomAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.models.GrpInMemberDetailModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.SinchService;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MemberDetailActivity extends AppCompatActivity implements View.OnClickListener,ServiceConnection,SinchService.StartFailedListener {
    //MEMBER_ID
    TextView follow, promote, sponsor, user_block, challeng_req_toolbar_title, user_rank, user_name, user_memb_percent, user_won_cnt, user_lost_cnt, text_mail, text_call;
    ImageView close, send_message, user_img, user_iv_one, user_iv_two, user_iv_three, user_iv_four, user_iv_five;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    TextView running_more_text, upcoming_more_text, completed_more_text, member_grp_more_text;
    String token, user_id, user_type, device_id, member_id, member_user_type, member_name, amount, time_span, from_on, to_on, file, member_mobile_call,
            md_user_name, overall_rank, member_country_code_call, member_mail_id, profile_pic,session_user_type;
    RecyclerView recycler_grp_in_memberdetail;
    String userName;
    GrpInMemberDetailAdapter grpInMemberDetailAdapter;
    ArrayList<GrpInMemberDetailModel> grpinMembListModel = new ArrayList<GrpInMemberDetailModel>();
    LinearLayoutManager layoutManager, layoutManager0, layoutManager1, layoutManager2;
    RecyclerView recycler_individual_running, recycler_individual_upcoming, recycler_individual_completed;
    GridLayoutManager gridLayoutManager;
    LinearLayout ll_grp_text_title;
    int block_status;
    //Running Adapter
    MemberIndividRunAdapter challengIndvidRunAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidRunList = new ArrayList<ChallengeIndividualRunningModel>();
    //Upcoming Adapter
    MemberIndividUpcomAdapter challengIndvidUppAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidUpcomList = new ArrayList<ChallengeIndividualRunningModel>();
    //Complet Adapter
    MemberIndividCompletAdapter challengIndvidCompletAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidCompletcomList = new ArrayList<ChallengeIndividualRunningModel>();
    private static final int MAKE_CALL_PERMISSION_REQUEST_CODE = 1;
    LinearLayout ll_running, ll_upcoming, ll_completed;
    Dialog dialog;
    //declare interface
    private SinchService.SinchServiceInterface mSinchServiceInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        getApplicationContext().bindService(new Intent(this, SinchService.class), this,
                BIND_AUTO_CREATE);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        Bundle bundle = getIntent().getExtras();
        member_id = bundle.getString("MEMBER_ID");
        member_name = bundle.getString("MEMBER_NAME");
        member_user_type = bundle.getString("member_user_type");
        Log.d("MEMID:", member_id + "\n" + member_user_type);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        session_user_type = userDetails.get(UserSessionManager.USER_TYPE);
        userName=userDetails.get(UserSessionManager.USER_NAME);
        pprogressDialog = new ProgressDialog(MemberDetailActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        // no_data_image=(ImageView)findViewById(R.id.no_data_image);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        user_img = (ImageView) findViewById(R.id.user_img);
        user_iv_one = (ImageView) findViewById(R.id.iv_one);
        user_iv_two = (ImageView) findViewById(R.id.iv_two);
        user_iv_three = (ImageView) findViewById(R.id.iv_three);
        user_iv_four = (ImageView) findViewById(R.id.iv_four);
        user_iv_five = (ImageView) findViewById(R.id.iv_five);
        send_message = (ImageView) findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        member_grp_more_text = (TextView) findViewById(R.id.member_grp_more_text);
        member_grp_more_text.setOnClickListener(this);
        running_more_text = (TextView) findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        upcoming_more_text = (TextView) findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        completed_more_text = (TextView) findViewById(R.id.completed_more_text);
        completed_more_text.setOnClickListener(this);
        ll_running = (LinearLayout) findViewById(R.id.ll_running);
        ll_upcoming = (LinearLayout) findViewById(R.id.ll_upcoming);
        ll_completed = (LinearLayout) findViewById(R.id.ll_completed);
        ll_grp_text_title = (LinearLayout) findViewById(R.id.ll_grp_text_title);
        user_rank = (TextView) findViewById(R.id.user_rank);
        user_rank.setTypeface(typeface);
        user_name = (TextView) findViewById(R.id.user_name);
        user_name.setTypeface(typeface);
        user_memb_percent = (TextView) findViewById(R.id.user_memb_percent);
        user_memb_percent.setTypeface(typeface);
        user_won_cnt = (TextView) findViewById(R.id.user_won_cnt);
        user_won_cnt.setTypeface(typeface);
        user_lost_cnt = (TextView) findViewById(R.id.user_lost_cnt);
        user_lost_cnt.setTypeface(typeface);
        challeng_req_toolbar_title = (TextView) findViewById(R.id.challeng_req_toolbar_title);
        challeng_req_toolbar_title.setTypeface(typeface);
        String str = member_name;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        challeng_req_toolbar_title.setText(converted_string);
        follow = (TextView) findViewById(R.id.follow);
        follow.setOnClickListener(this);
        follow.setTypeface(typeface);
        text_call = (TextView) findViewById(R.id.text_call);
        text_call.setOnClickListener(this);
        text_call.setTypeface(typeface);
        text_mail = (TextView) findViewById(R.id.text_mail);
        text_mail.setOnClickListener(this);
        text_mail.setTypeface(typeface);
        promote = (TextView) findViewById(R.id.promote);
        promote.setOnClickListener(this);
        promote.setTypeface(typeface);
        if (member_id.equals(user_id)) {
            promote.setVisibility(View.VISIBLE);
        } else {
            promote.setVisibility(View.INVISIBLE);
            text_call.setVisibility(View.VISIBLE);
            text_mail.setVisibility(View.VISIBLE);
        }
        if(session_user_type.equals("SPONSOR"))
        {
            follow.setVisibility(View.VISIBLE);
            send_message.setVisibility(View.VISIBLE);
        }
        else
        {

        }


        sponsor = (TextView) findViewById(R.id.sponsor);
        sponsor.setOnClickListener(this);
        sponsor.setTypeface(typeface);
        user_block = (TextView) findViewById(R.id.user_block);
        user_block.setOnClickListener(this);
        user_block.setTypeface(typeface);
        recycler_grp_in_memberdetail = (RecyclerView) findViewById(R.id.recycler_grp_in_memberdetail);
        grpInMemberDetailAdapter = new GrpInMemberDetailAdapter(grpinMembListModel, MemberDetailActivity.this, R.layout.row_grp_in_memberdetail);
        layoutManager = new LinearLayoutManager(this);
        recycler_grp_in_memberdetail.setNestedScrollingEnabled(false);
        recycler_grp_in_memberdetail.setLayoutManager(layoutManager);
        recycler_individual_running = (RecyclerView) findViewById(R.id.recycler_individual_running);
        recycler_individual_upcoming = (RecyclerView) findViewById(R.id.recycler_individual_upcoming);
        recycler_individual_completed = (RecyclerView) findViewById(R.id.recycler_individual_completed);
        challengIndvidRunAdpter = new MemberIndividRunAdapter(challengIndvidRunList, MemberDetailActivity.this, R.layout.row_my_challenges);
        layoutManager0 = new LinearLayoutManager(this);
        recycler_individual_running.setNestedScrollingEnabled(false);
        recycler_individual_running.setLayoutManager(layoutManager0);
        challengIndvidUppAdpter = new MemberIndividUpcomAdapter(challengIndvidUpcomList, MemberDetailActivity.this, R.layout.row_my_challenges);
        layoutManager1 = new LinearLayoutManager(this);
        recycler_individual_upcoming.setNestedScrollingEnabled(false);
        recycler_individual_upcoming.setLayoutManager(layoutManager1);
        challengIndvidCompletAdpter = new MemberIndividCompletAdapter(challengIndvidCompletcomList, MemberDetailActivity.this, R.layout.row_my_challenges);
        layoutManager2 = new LinearLayoutManager(this);
        recycler_individual_completed.setNestedScrollingEnabled(false);
        recycler_individual_completed.setLayoutManager(layoutManager2);

        getMemberDetail();
        getFollowingStatus();
        getIndividualChallenges();
    }

    public void getIndividualChallenges() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            challengIndvidRunList.clear();
            challengIndvidUpcomList.clear();
            challengIndvidCompletcomList.clear();
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + member_id + "&user_type=USER&type=" + "INDIVIDUAL";
            Log.d("MEMCHALLURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("MEMCHALLINDVRESP:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jObjData = jsonObject.getJSONObject("data");
                                    int running_cnt = jObjData.getInt("running_cnt");
                                    int upcomming_cnt = jObjData.getInt("upcomming_cnt");
                                    int completed_cnt = jObjData.getInt("completed_cnt");
                                    if (running_cnt > 2) {
                                       // running_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (upcomming_cnt > 2) {
                                      //  upcoming_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (completed_cnt > 2) {
                                     //   completed_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (running_cnt != 0) {
                                        JSONArray jsonArrayrunning = jObjData.getJSONArray("running");
                                        Log.d("RUNNNN", jsonArrayrunning.toString());
                                        for (int i = 0; i < jsonArrayrunning.length(); i++) {
                                            JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                            ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                                            am_runn.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_runn.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_runn.setUser_name(jsonObject1.getString("user_name"));
                                            am_runn.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_runn.setStatus(jsonObject1.getString("status"));
                                            am_runn.setAmount(jsonObject1.getString("amount"));
                                            am_runn.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_runn.setStart_on(jsonObject1.getString("start_on"));
                                            am_runn.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_runn.setPause_access(jsonObject1.getString("pause_access"));
                                            am_runn.setResume_on(jsonObject1.getString("resume_on"));
                                            am_runn.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_runn.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_runn.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_runn.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_runn.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidRunList.add(am_runn);
                                        }
                                    } else {
                                        ll_running.setVisibility(View.GONE);
                                    }
                                    //upcoming
                                    if (upcomming_cnt != 0) {
                                        JSONArray jsonArrayupcom = jObjData.getJSONArray("upcomming");
                                        Log.d("UPPPP", jsonArrayupcom.toString());
                                        for (int j = 0; j < jsonArrayupcom.length(); j++) {
                                            JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                            ChallengeIndividualRunningModel am_up = new ChallengeIndividualRunningModel();
                                            am_up.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_up.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_up.setUser_name(jsonObject1.getString("user_name"));
                                            am_up.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_up.setStatus(jsonObject1.getString("status"));
                                            am_up.setAmount(jsonObject1.getString("amount"));
                                            am_up.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_up.setStart_on(jsonObject1.getString("start_on"));
                                            am_up.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_up.setPause_access(jsonObject1.getString("pause_access"));
                                            am_up.setResume_on(jsonObject1.getString("resume_on"));
                                            am_up.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_up.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_up.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_up.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_up.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidUpcomList.add(am_up);
                                        }
                                    } else {
                                        ll_upcoming.setVisibility(View.GONE);
                                    }
                                    //complete
                                    if (completed_cnt != 0) {
                                        JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                        Log.d("COMPL", jsonArraycomplet.toString());
                                        for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                            JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                            ChallengeIndividualRunningModel am_complet = new ChallengeIndividualRunningModel();
                                            am_complet.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_complet.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_complet.setUser_name(jsonObject1.getString("user_name"));
                                            am_complet.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_complet.setStatus(jsonObject1.getString("status"));
                                            am_complet.setAmount(jsonObject1.getString("amount"));
                                            am_complet.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_complet.setStart_on(jsonObject1.getString("start_on"));
                                            am_complet.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_complet.setPause_access(jsonObject1.getString("pause_access"));
                                            am_complet.setResume_on(jsonObject1.getString("resume_on"));
                                            am_complet.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_complet.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_complet.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_complet.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_complet.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidCompletcomList.add(am_complet);
                                        }
                                    } else {
                                        ll_completed.setVisibility(View.GONE);
                                    }
                                    recycler_individual_running.setAdapter(challengIndvidRunAdpter);
                                    recycler_individual_upcoming.setAdapter(challengIndvidUppAdpter);
                                    recycler_individual_completed.setAdapter(challengIndvidCompletAdpter);
                                }
                                if (status.equals("10200")) {
                                    Toast.makeText(MemberDetailActivity.this, "Invalid input...!", Toast.LENGTH_LONG).show();
                                }
                                if (status.equals("10300")) {
                                    Toast.makeText(MemberDetailActivity.this, "No data Found..!", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MEM_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


    private void getMemberDetail() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String user_url = AppUrls.BASE_URL + AppUrls.USER_MEMBER_DETAIL + "?user_id=" + user_id + "&profile_id=" + member_id;
            Log.d("USERDetailsUrl", user_url);
            StringRequest strRe = new StringRequest(Request.Method.GET, user_url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("USERDetailsResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                   // Toast.makeText(MemberDetailActivity.this, "Data Fetched Successfully", Toast.LENGTH_SHORT).show();
                                    JSONObject getData = jsonObject.getJSONObject("data");
                                    block_status = getData.getInt("is_blocked");
                                    JSONObject bsicInfo = getData.getJSONObject("basic_info");
                                    member_id = bsicInfo.getString("id");
                                    member_user_type = bsicInfo.getString("user_type");
                                    md_user_name = bsicInfo.getString("user_name");
                                    String convert_user_name = firstLetterCaps(md_user_name);
                                    user_name.setText(convert_user_name);
                                    overall_rank = bsicInfo.getString("overall_rank");
                                    user_rank.setText(overall_rank);
                                    String win_challenges = bsicInfo.getString("win_challenges");
                                    user_won_cnt.setText(win_challenges);
                                    String rank_lost_challenges = bsicInfo.getString("rank_lost_challenges");
                                    user_lost_cnt.setText(rank_lost_challenges);
                                    member_mail_id = bsicInfo.getString("email");
                                    member_mobile_call = bsicInfo.getString("mobile");
                                    member_country_code_call = bsicInfo.getString("country_code");
                                   //user_block
                                    if (block_status==1)
                                    {
                                        user_block.setText("UnBlock");
                                        user_block.setTextColor(Color.parseColor("#ffffff"));
                                        user_block.setBackgroundResource(R.color.days_incomplet);
                                        user_block.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getUnBlockUser();
                                            }
                                        });
                                    }
                                    else
                                    {
                                        user_block.setText("Block");
                                        //  follow.setTextColor(Color.parseColor("#000000"));
                                        user_block.setBackgroundResource(R.drawable.background_for_block);
                                        user_block.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getBlockUser();
                                            }
                                        });
                                    }




                                    String winning_percentage = bsicInfo.getString("winning_percentage");
                                    if (winning_percentage.equals(null) || winning_percentage.equals("null")) {
                                        user_memb_percent.setText(Html.fromHtml("0%"));
                                    } else {
                                        user_memb_percent.setText(Html.fromHtml(winning_percentage + "%"));
                                    }
                                    profile_pic = AppUrls.BASE_IMAGE_URL + bsicInfo.getString("profile_pic");
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.dummy_white_user_profile)
                                            .into(user_img);
                                    String getUserActImgs1 = AppUrls.BASE_IMAGE_URL + bsicInfo.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1");
                                    String getUserActImgs2 = AppUrls.BASE_IMAGE_URL + bsicInfo.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2");
                                    String getUserActImgs3 = AppUrls.BASE_IMAGE_URL + bsicInfo.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3");
                                    String getUserActImgs4 = AppUrls.BASE_IMAGE_URL + bsicInfo.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4");
                                    String getUserActImgs5 = AppUrls.BASE_IMAGE_URL + bsicInfo.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5");
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(getUserActImgs1)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(user_iv_one);
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(getUserActImgs2)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(user_iv_two);
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(getUserActImgs3)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(user_iv_three);
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(getUserActImgs4)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(user_iv_four);
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(getUserActImgs5)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(user_iv_five);
                                    //Group SECTION//
                                    JSONArray jgrpArray = getData.getJSONArray("group_details");
                                    int member_grp_count = getData.getInt("group_counts");
                                    if(member_grp_count==0)
                                    {
                                        ll_grp_text_title.setVisibility(View.GONE);
                                    }

                                    Log.d("group_counts", String.valueOf(member_grp_count));
                                    if (member_grp_count > 2) {
                                        member_grp_more_text.setVisibility(View.VISIBLE);
                                    }
                                    Log.d("GROUPARRAY", jgrpArray.toString());
                                    for (int i = 0; i < jgrpArray.length(); i++) {
                                        JSONObject jobjMemArr = jgrpArray.getJSONObject(i);
                                        GrpInMemberDetailModel grpInMemberDetailList = new GrpInMemberDetailModel();
                                        grpInMemberDetailList.setGroup_id(jobjMemArr.getString("id"));
                                        grpInMemberDetailList.setGroup_name(jobjMemArr.getString("group_name"));
                                        grpInMemberDetailList.setAdmin_id(jobjMemArr.getString("admin_id"));
                                        grpInMemberDetailList.setConversation_type(jobjMemArr.getString("conversation_type"));
                                        grpInMemberDetailList.setGroup_members(jobjMemArr.getString("group_members"));
                                        grpInMemberDetailList.setProfile_pic(AppUrls.BASE_IMAGE_URL + jobjMemArr.getString("group_pic"));
                                        grpInMemberDetailList.setWin_challenges(jobjMemArr.getString("win_challenges"));
                                        grpInMemberDetailList.setLost_challenges(jobjMemArr.getString("lost_challenges"));
                                        grpInMemberDetailList.setGroup_rank(jobjMemArr.getString("group_rank"));
                                        grpInMemberDetailList.setWinning_percentage(jobjMemArr.getString("winning_percentage"));
                                        grpInMemberDetailList.setImgv1(AppUrls.BASE_IMAGE_URL + jobjMemArr.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        grpInMemberDetailList.setImgv2(AppUrls.BASE_IMAGE_URL + jobjMemArr.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        grpInMemberDetailList.setImgv3(AppUrls.BASE_IMAGE_URL + jobjMemArr.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        grpInMemberDetailList.setImgv4(AppUrls.BASE_IMAGE_URL + jobjMemArr.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        grpInMemberDetailList.setImgv5(AppUrls.BASE_IMAGE_URL + jobjMemArr.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        grpinMembListModel.add(grpInMemberDetailList);

                                    }
                                    recycler_grp_in_memberdetail.setAdapter(grpInMemberDetailAdapter);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MemberDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("USERDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    private void getFollowingStatus() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.FOLLOWING_STATUS;
            Log.d("FOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String status = jobcode.getString("status");
                        String response_code = jobcode.getString("response_code");
                        if (status.equals("1"))
                        {
                            follow.setText("UnFollow");
                            follow.setTextColor(Color.parseColor("#ffffff"));
                            follow.setBackgroundResource(R.drawable.bg_fill);
                            follow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getUnFollowUser();
                                }
                            });
                        }
                        else
                            {
                            follow.setText("Follow");
                          //  follow.setTextColor(Color.parseColor("#000000"));
                            follow.setBackgroundResource(R.drawable.background_for_login_and_signup);
                            follow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getFollowUser();
                                }
                            });
                        }
                        if (response_code.equals("10200")) {
                            follow.setText("UnFollow");
                            follow.setTextColor(Color.parseColor("#ffffff"));
                            follow.setBackgroundResource(R.drawable.bg_fill);
                          //  Toast.makeText(MemberDetailActivity.this, "Unable to Send Request..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10100")) {
                            pprogressDialog.dismiss();
                           // follow.setTextColor(Color.parseColor("#000000"));
                           // follow.setBackgroundResource(R.drawable.bg_fill);
                        //    Toast.makeText(MemberDetailActivity.this, "You Can Send Request!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == send_message) {
            Intent inten = new Intent(MemberDetailActivity.this, MessageDetailActivity.class);
            inten.putExtra("FROM_ID", member_id); // friend id
            inten.putExtra("FROM_NAME", member_name); // friend name
            inten.putExtra("FROM_TYPE", member_user_type);   // friend type
            inten.putExtra("FROM_PROFILEPIC", profile_pic); //progile pic
            inten.putExtra("FROM_TYPE", "USER"); //progile pic
            startActivity(inten);
        }
        if (view == text_mail) {
            String[] recipients = new String[]{member_mail_id, ""};
            Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
            testIntent.setType("message/rfc822");
            testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
            startActivity(testIntent);
        }
        if (view == promote) {
            Log.d("PROMOTIONURL", AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + user_type + "&entity_id=" + user_id);
            checkInternet = NetworkChecking.isConnected(MemberDetailActivity.this);
            if (checkInternet) {
                StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + user_type + "&entity_id=" + user_id,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                pprogressDialog.dismiss();
                                Log.d("PROMOTION RESP", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String success = jsonObject.getString("success");
                                    String message = jsonObject.getString("message");
                                    String response_code = jsonObject.getString("response_code");
                                    if (response_code.equals("10100")) {
                                        pprogressDialog.dismiss();
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String id = jsonObject1.getString("id");
                                        String promotion_id = jsonObject1.getString("promotion_id");
                                        String entity_id = jsonObject1.getString("entity_id");
                                        String entity_type = jsonObject1.getString("entity_type");
                                        amount = jsonObject1.getString("amount");
                                        time_span = jsonObject1.getString("time_span");
                                        String status = jsonObject1.getString("status");
                                        file = jsonObject1.getString(AppUrls.BASE_IMAGE_URL + "banner_path");
                                        from_on = jsonObject1.getString("from_on");
                                        to_on = jsonObject1.getString("to_on");
                                        if (to_on.equals("")) {

                                        } else {
                                            addPromote();
                                        }
                                        String created_on = jsonObject1.getString("created_on");
                                        String updated_on = jsonObject1.getString("updated_on");
                                    }
                                    if (response_code.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(MemberDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();
                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token", token);
                        headers.put("x-device-id", device_id);
                        headers.put("x-device-platform", "ANDROID");
                        Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                        return headers;
                    }

                };
                strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
                requestQueue.add(strRe);
            } else {
                pprogressDialog.cancel();
                Toast.makeText(MemberDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }

        }
        if (view == sponsor)
        {
            dialog = new Dialog(MemberDetailActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialolg_sponsor_request);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView dummy_text = (TextView) dialog.findViewById(R.id.dummy_text);
            dummy_text.setTypeface(typeface);
            TextView member_name_text = (TextView) dialog.findViewById(R.id.member_name_text);
            TextView member_rank_text = (TextView) dialog.findViewById(R.id.member_rank_text);
            final EditText member_sponsor_amount_edt = (EditText) dialog.findViewById(R.id.member_sponsor_amount_edt);
            Button sponsor_submit_btn = (Button) dialog.findViewById(R.id.sponsor_submit_btn);
            member_name_text.setText(md_user_name);
            member_name_text.setTypeface(typeface);
            member_rank_text.setText(overall_rank);
            member_rank_text.setTypeface(typeface);
            dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
            sponsor_submit_btn.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {
                    String amt_value = member_sponsor_amount_edt.getText().toString();
                    if (amt_value.equals("") || amt_value.equals("0")) {
                        Toast.makeText(MemberDetailActivity.this, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MemberDetailActivity.this, ChallengePaymentActivity.class);
                        intent.putExtra("activity", "MemberDetail");
                        intent.putExtra("member_id", member_id);
                        intent.putExtra("member_user_type", member_user_type);
                        intent.putExtra("sponsorAmount", amt_value);
                        Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                        startActivity(intent);
                    }
                }

            });
            dialog.show();

        }
        if (view == text_call) {
            call();
        }

        if (view == member_grp_more_text) {
            Intent i = new Intent(MemberDetailActivity.this, AllMoreGroupFromMDActivity.class);
            i.putExtra("MEMBERID", member_id);
            startActivity(i);
        }
        if (view == running_more_text) {
            Intent i = new Intent(MemberDetailActivity.this, AllRunningChallengesActivity.class);
            startActivity(i);
        }
        if (view == upcoming_more_text) {
            Intent i = new Intent(MemberDetailActivity.this, AllUpcomingChallengesActivity.class);
            startActivity(i);
        }
        if (view == completed_more_text) {
            Intent i = new Intent(MemberDetailActivity.this, AllCompletedChallengesActivity.class);
            startActivity(i);
        }
    }

    private void call() {

        if (!userName.equals(getSinchServiceInterface().getUserName())) {
            getSinchServiceInterface().stopClient();
        }

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);

        } else {
            calltoUser();
        }

        /*try {
            String uri = member_country_code_call + member_mobile_call;
            if (!TextUtils.isEmpty(uri)) {
                if (checkPermission(Manifest.permission.CALL_PHONE)) {
                    String dial = "tel:" + uri;
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                } else {
                    Toast.makeText(MemberDetailActivity.this, "Call Permission denied", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MemberDetailActivity.this, "Enter a phone number", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Your call has failed...", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
*/
        if (checkPermission(Manifest.permission.CALL_PHONE)) {
            text_call.setEnabled(true);
        } else {
            text_call.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MAKE_CALL_PERMISSION_REQUEST_CODE);
        }

    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MAKE_CALL_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    text_call.setEnabled(true);
                    Toast.makeText(this, "You can call the number by clicking on the button", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }


    private void addPromote() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ADD_PROMOTE;
            Log.d("ADDPROMOTE", url);

            StringRequest req_members = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("ADDPROMOTERESP", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        String message = jsonObject.getString("message");
                        String response_code = jsonObject.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            String ref_id = jsonObject1.getString("ref_id");
                            String promotion_id = jsonObject1.getString("promotion_id");
                         //   Toast.makeText(MemberDetailActivity.this, "Add Promote..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_type", user_type);
                    params.put("user_id", user_id);
                    params.put("amount", amount);
                    params.put("time_span", time_span);
                    params.put("from_on", from_on);
                    params.put("to_on", to_on);
                    params.put("file", file);
                    Log.d("ADDPROMOTEPARAMS:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getFollowUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.FOLLOW_USER;
            Log.d("FOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("FOLOWMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "Followed successfully", Toast.LENGTH_LONG).show();
                            getFollowingStatus();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    public void getUnFollowUser()
    {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.UNFOLLOW_USER;
            Log.d("UNFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("UNFOLOWMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "UnFollow successfully", Toast.LENGTH_LONG).show();
                            getFollowingStatus();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("UNFOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    //call functionality service methods
    protected void onServiceConnected() {
        // for subclasses
        getSinchServiceInterface().setStartListener(this);
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {

    }

    private void calltoUser(){
        try {
            Call call = getSinchServiceInterface().callUser(member_name);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(this, "Service is not started. Try stopping the service and starting it again before "
                        + "placing a call.", Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(this, CallScreen.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(this, new String[]{e.getRequiredPermission()}, 0);
        }
    }

    private void getBlockUser()
    {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.BLOCK_USER;
            Log.d("BLOCKURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("BLOCKMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                          //  JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "Blocked successfully", Toast.LENGTH_LONG).show();
                            getMemberDetail();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("blocked_user_id", member_id);
                    Log.d("blockPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }
    private void getUnBlockUser()
    {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.UNBLOCK_USER;
            Log.d("UNBLOCKURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("UNBLOCKMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(MemberDetailActivity.this, "UnBlocked successfully", Toast.LENGTH_LONG).show();
                            getMemberDetail();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("blocked_user_id", member_id);
                    Log.d("unblockPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }
}
