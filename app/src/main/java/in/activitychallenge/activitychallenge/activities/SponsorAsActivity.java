package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.SponsorAsActivtyAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SponsorAsActivity extends AppCompatActivity implements  View.OnClickListener
{

      ImageView close;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    SearchView sponsor_activiyt_search;
    RecyclerView recycler_sponser_activityies;
    LinearLayoutManager  layoutManager ;
    SponsorAsActivtyAdapter sponsorAsActivtyAdapter;
    ArrayList<ActivityModel> activitylist = new ArrayList<ActivityModel>();
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_as);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        pprogressDialog = new ProgressDialog(SponsorAsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
      // view = findViewById(R.id.custom_tab);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);

        sponsor_activiyt_search = (SearchView) findViewById(R.id.sponsor_activiyt_search);
        sponsor_activiyt_search.setOnClickListener(this);
        sponsor_activiyt_search.setIconified(false);
        sponsor_activiyt_search.clearFocus();
        View view = findViewById(R.id.custom_tab);

        sponsor_activiyt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sponsor_activiyt_search.setIconified(false);
            }
        });

        sponsor_activiyt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                sponsorAsActivtyAdapter.getFilter().filter(query);
                return false;
            }
        });

        recycler_sponser_activityies = (RecyclerView) findViewById(R.id.recycler_sponser_activityies);
        sponsorAsActivtyAdapter = new SponsorAsActivtyAdapter(activitylist, SponsorAsActivity.this, R.layout.row_sponsor_as_activty);
        layoutManager = new LinearLayoutManager(this);
        recycler_sponser_activityies.setNestedScrollingEnabled(false);
        recycler_sponser_activityies.setLayoutManager(layoutManager);

        getActivities();
        getCount();
    }
    private void getActivities() {
        checkInternet = NetworkChecking.isConnected(SponsorAsActivity.this);
        if (checkInternet) {

           // activitylist.clear();

            String url = AppUrls.BASE_URL + AppUrls.ACTIVITY;
            Log.d("SPPLISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SPACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    ContentValues values = new ContentValues();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                         ActivityModel am = new ActivityModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setActivity_name(jsonObject1.getString("activity_name"));
                                        am.setActivity_no(jsonObject1.getString("activity_no"));
                                        am.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                        am.setTools_required(jsonObject1.getString("tools_required"));
                                        am.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        activitylist.add(am);
                                        //Log.d("fjbvsdfkvsdf",activitylist.toString());
                                    }

                                    recycler_sponser_activityies.setAdapter(sponsorAsActivtyAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(SponsorAsActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public void onClick(View view)
    {
        if(view==close)
        {
            finish();
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(SponsorAsActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(SponsorAsActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(SponsorAsActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(SponsorAsActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

}
