package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.LotteryCodesAdapter;
import in.activitychallenge.activitychallenge.models.LotteryCodesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class LotteryCodesActivity extends AppCompatActivity {

    RecyclerView lottery_recyclerview;
    LinearLayoutManager layoutManager;
    LotteryCodesAdapter adapter;
    ArrayList<LotteryCodesModel> lotteryCodesModels = new ArrayList<LotteryCodesModel>();
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String ref_txn_id = "", paymentId = "", amount = "", wallet_amount = "", device_id, token;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery_codes);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Bundle bundle = getIntent().getExtras();
        ref_txn_id = bundle.getString("ref_txn_id");
        paymentId = bundle.getString("paymentId");
        amount = bundle.getString("amount");
        wallet_amount = bundle.getString("wallet_amount");
        Log.d("BUNDLEDETAILS", ref_txn_id + "\n" + paymentId + "\n" + amount + "\n" + wallet_amount);
        lottery_recyclerview = (RecyclerView) findViewById(R.id.lottery_recyclerview);
        adapter = new LotteryCodesAdapter(lotteryCodesModels, LotteryCodesActivity.this, R.layout.row_lottery_codes);
        layoutManager = new LinearLayoutManager(this);
        lottery_recyclerview.setNestedScrollingEnabled(false);
        lottery_recyclerview.setLayoutManager(layoutManager);

        getLotteryCodes();
    }

    private void getLotteryCodes() {
        checkInternet = NetworkChecking.isConnected(LotteryCodesActivity.this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.EARN_MONEY_AFTER_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("REPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LotteryCodesActivity.this, "Payment Successful..!", Toast.LENGTH_SHORT).show();
                                    JSONObject jobj = jsonObject.getJSONObject("data");
                                    JSONObject res_data = jobj.getJSONObject("res_data");
                                    JSONArray jarray = res_data.getJSONArray("codes");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        LotteryCodesModel rhm = new LotteryCodesModel();
                                        rhm.setLottery_codes(jarray.getString(i));
                                        lotteryCodesModels.add(rhm);
                                    }
                                    lottery_recyclerview.setAdapter(adapter);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LotteryCodesActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("txn_id", ref_txn_id);
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("txn_status", "COMPLETED");
                    params.put("amount", amount);
                    params.put("amount_paypal", amount);
                    //params.put("amount_wallet", wallet_amount);
                    params.put("amount_wallet", "0");
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(LotteryCodesActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(LotteryCodesActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
}
