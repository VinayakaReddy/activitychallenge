package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ForgetPassOtpActivity extends AppCompatActivity implements View.OnClickListener {

    TextView vefiy_forget_otptxt;
    EditText forget_otp_edt;
    ImageView forget_otp_verify_btn;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    Typeface typeface, typeface2;
    UserSessionManager userSessionManager;
    String user_id, device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_pass_otp);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("GETTT", user_id + "///" + device_id);
        pprogressDialog = new ProgressDialog(ForgetPassOtpActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        vefiy_forget_otptxt = (TextView) findViewById(R.id.vefiy_forget_otptxt);
        vefiy_forget_otptxt.setTypeface(typeface2);
        forget_otp_edt = (EditText) findViewById(R.id.forget_otp_edt);
        forget_otp_edt.setTypeface(typeface);
        forget_otp_verify_btn = (ImageView) findViewById(R.id.forget_otp_verify_btn);
        forget_otp_verify_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == forget_otp_verify_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    String forgeturl = AppUrls.BASE_URL + AppUrls.VERIFY_FORGETPASSWORD_OTP;
                    Log.d("FURL", forgeturl);
                    final String otp = forget_otp_edt.getText().toString().trim();
                    StringRequest strfrogetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFY_FORGETPASSWORD_OTP, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("FOROTPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {//Otp has been verified
                                    pprogressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String nonce = jsonObject1.getString("nonce");
                                    Intent i = new Intent(ForgetPassOtpActivity.this, ForgetChangePassword.class);
                                    i.putExtra("NONCE", nonce);
                                    startActivity(i);
                                }

                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "User does not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10400")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Unable to Send OTP..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10500")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid OTP..", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("otp", otp);
                            params.put("device_id", device_id);
                            Log.d("FORGETOTPPARAM:", params.toString());
                            return params;
                        }
                    };

                    strfrogetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(ForgetPassOtpActivity.this);
                    requestQueue.add(strfrogetReq);
                } else {
                    Toast.makeText(ForgetPassOtpActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    private boolean validate() {
        boolean result = true;
        String otp = forget_otp_edt.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            forget_otp_edt.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

}
