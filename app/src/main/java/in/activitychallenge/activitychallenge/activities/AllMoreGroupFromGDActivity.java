package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.AllMoreGroupMembersAdapter;
import in.activitychallenge.activitychallenge.models.GetGroupMembersModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllMoreGroupFromGDActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id, token, grp_id;
    UserSessionManager session;
    ImageView close;
    LinearLayoutManager llm;
    ArrayList<GetGroupMembersModel> allMoregrpMemList = new ArrayList<GetGroupMembersModel>();
    AllMoreGroupMembersAdapter allMoregrpMemAdap;
    RecyclerView allmore_groupmemberes_recylerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_more_group_from_gd);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        grp_id = getIntent().getExtras().getString("GROUPID");
        allmore_groupmemberes_recylerview = (RecyclerView) findViewById(R.id.allmore_groupmemberes_recylerview);
        allMoregrpMemAdap = new AllMoreGroupMembersAdapter(AllMoreGroupFromGDActivity.this, allMoregrpMemList, R.layout.row_get_group_members);
        llm = new LinearLayoutManager(this);
        allmore_groupmemberes_recylerview.setNestedScrollingEnabled(false);
        allmore_groupmemberes_recylerview.setLayoutManager(llm);
        getAllMoreGroupMembers();
    }

    private void getAllMoreGroupMembers() {
        allMoregrpMemList.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("MOREGroupDetailsUrl", AppUrls.BASE_URL + AppUrls.GROUP_DETIL_MEMBER_MORE + "?user_id=" + user_id + "&group_id=" + grp_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GROUP_DETIL_MEMBER_MORE + "?user_id=" + user_id + "&group_id=" + grp_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("MOREGDRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                 //   Toast.makeText(AllMoreGroupFromGDActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();
                                    JSONObject getData = jsonObject.getJSONObject("data");
                                    //GET_MEMBERS_SECTION
                                    JSONArray getMemData = getData.getJSONArray("members");
                                    for (int i = 0; i < getMemData.length(); i++) {
                                        GetGroupMembersModel gMemberList = new GetGroupMembersModel();
                                        JSONObject jMemArr = getMemData.getJSONObject(i);
                                        gMemberList.setGroup_user_type_M(jMemArr.getString("group_user_type"));
                                        gMemberList.setId_M(jMemArr.getString("id"));
                                        gMemberList.setUser_name_M(jMemArr.getString("user_name"));
                                        gMemberList.setProfile_pic_M(AppUrls.BASE_IMAGE_URL + jMemArr.getString("profile_pic"));
                                        gMemberList.setUser_rank_M(jMemArr.getInt("user_rank"));
                                        gMemberList.setWin_challenges_M(jMemArr.getInt("win_challenges"));
                                        gMemberList.setLost_challenges_M(jMemArr.getInt("lost_challenges"));
                                        gMemberList.setWinning_percentage_M(jMemArr.getInt("winning_percentage"));
                                        gMemberList.setImg1_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        gMemberList.setImg2_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        gMemberList.setImg3_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        gMemberList.setImg4_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        gMemberList.setImg5_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        allMoregrpMemList.add(gMemberList);
                                    }
                                    allmore_groupmemberes_recylerview.setAdapter(allMoregrpMemAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(AllMoreGroupFromGDActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(AllMoreGroupFromGDActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {
            pprogressDialog.cancel();
            Toast.makeText(AllMoreGroupFromGDActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
