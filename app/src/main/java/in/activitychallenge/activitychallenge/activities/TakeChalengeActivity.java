package in.activitychallenge.activitychallenge.activities;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.ActivityAdapter;
import in.activitychallenge.activitychallenge.adapter.ChallengeAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.ActivityDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;

public class TakeChalengeActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<ActivityModel> activitylist = new ArrayList<ActivityModel>();
    private RecyclerView take_challenge_recyclerview;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    ChallengeAdapter challengeAdapter;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id, token, device_id;
    String startDate,endDate;
    int noOfDays;
    JSONArray locationArr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_chalenge);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String eventId=getIntent().getStringExtra("eventid");
        startDate=getIntent().getStringExtra("startdate");
        endDate=getIntent().getStringExtra("enddate");
        noOfDays=getIntent().getIntExtra("noofdays",0);

        if(eventId!=null && eventId.length()>0){
            Utils.getInstance().setEventtype(eventId);
        }
        String tmpArr= getIntent().getStringExtra("location");
        try {
            locationArr = new JSONArray(tmpArr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
         Log.v("FetchingValues",">>"+startDate+"//"+endDate+"//"+locationArr+"//"+noOfDays);
        take_challenge_recyclerview=(RecyclerView)findViewById(R.id.take_chalenge_recycleview);
        session = new UserSessionManager(TakeChalengeActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        ImageView close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);
        getActivities(eventId);

    }


    private void getActivities(String id) {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            activitylist.clear();
            String url = AppUrls.BASE_URL + AppUrls.TAKE_CHALLENGE_SPECIAL_EVENT +id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    ContentValues values = new ContentValues();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                         ActivityModel am = new ActivityModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setActivity_name(jsonObject1.getString("activity_name"));
                                        am.setActivity_no(jsonObject1.getString("activity_no"));
                                        am.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                        am.setTools_required(jsonObject1.getString("tools_required"));
                                        am.setDaily_challenges(jsonObject1.getString("daily_challenges"));
                                        am.setLongrun_challenges(jsonObject1.getString("longrun_challenges"));
                                        am.setMin_value(jsonObject1.getString("min_value"));
                                        am.setMin_value_unit(jsonObject1.getString("min_value_unit"));
                                        am.setWeekly_challenges(jsonObject1.getString("weekly_challenges"));
                                        am.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        activitylist.add(am);
                                        Log.d("fjbvsdfkvsdf",activitylist.toString());
                                    }
                                    //activityList();
                                    take_challenge_recyclerview.setHasFixedSize(true);
                                    challengeAdapter = new ChallengeAdapter(activitylist, TakeChalengeActivity.this, R.layout.take_challenge_row,startDate,endDate,noOfDays,locationArr);
                                    gridLayoutManager = new GridLayoutManager(TakeChalengeActivity.this, 3);
                                    layoutManager = new LinearLayoutManager(TakeChalengeActivity.this);
                                    take_challenge_recyclerview.setLayoutManager(gridLayoutManager);
                                    take_challenge_recyclerview.setAdapter(challengeAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(TakeChalengeActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // activityList();
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.close){
            finish();
        }
    }
}
