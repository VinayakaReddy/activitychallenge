package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.GroupDetailAdapter;
import in.activitychallenge.activitychallenge.dbhelper.GroupChatDB;
import in.activitychallenge.activitychallenge.models.GroupConversationModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class GroupChatDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, group_profile_pic, grp_send_mesage;
    EditText grp_mesagetext_edt;
    TextView grp_detail_toolbar_title;
    Typeface typeface, typeface2;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    String user_id, user_type, token, device_id, g_fromname, group_id, g_fromtype, g_profile_pic, group_type, group_conversation_type;
    RecyclerView recycler_group_chatconversn;
    GroupDetailAdapter groupDetailAdapter;
    LinearLayoutManager layoutManager;
    ArrayList<GroupConversationModel> groupDetailModalList = new ArrayList<GroupConversationModel>();
    GroupChatDB groupChatDB;
    ContentValues values;
    String FROM_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_detail);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        groupChatDB=new GroupChatDB(GroupChatDetailActivity.this);
        Bundle bundle = getIntent().getExtras();
        group_id = bundle.getString("group_id");
        g_fromname = bundle.getString("group_name");
        g_profile_pic = bundle.getString("profile_pic");
        group_type = bundle.getString("GROUP_TYPE");
        group_conversation_type = bundle.getString("GROUP_CONVERSATION_TYPE");
        FROM_TYPE = bundle.getString("FROM_TYPE");
        grp_detail_toolbar_title = (TextView) findViewById(R.id.grp_detail_toolbar_title);
        grp_detail_toolbar_title.setText(g_fromname);
        Log.d("M_DETAIL", group_id + "//" + g_fromname + "//" + group_type + "///" + group_type);
        userSessionManager = new UserSessionManager(GroupChatDetailActivity.this);
        values = new ContentValues();
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("M_DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);
        progressDialog = new ProgressDialog(GroupChatDetailActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        group_profile_pic = (ImageView) findViewById(R.id.group_profile_pic);
        group_profile_pic.setOnClickListener(this);
        grp_send_mesage = (ImageView) findViewById(R.id.grp_send_mesage);
        grp_send_mesage.setOnClickListener(this);
        grp_mesagetext_edt = (EditText) findViewById(R.id.grp_mesagetext_edt);
        grp_mesagetext_edt.setOnClickListener(this);
        Picasso.with(GroupChatDetailActivity.this)
                .load(g_profile_pic)
                .placeholder(R.drawable.dummy_group_profile)
                .into(group_profile_pic);
        recycler_group_chatconversn = (RecyclerView) findViewById(R.id.recycler_group_chatconversn);
        groupDetailAdapter = new GroupDetailAdapter(groupDetailModalList, GroupChatDetailActivity.this, R.layout.row_group_chat_list,FROM_TYPE);
        layoutManager = new LinearLayoutManager(this);
        recycler_group_chatconversn.setNestedScrollingEnabled(false);
        recycler_group_chatconversn.setLayoutManager(layoutManager);
        recycler_group_chatconversn.setItemAnimator(null);
        layoutManager.setStackFromEnd(true);
        getGroupChatConversation();

    }

    private void getGroupChatConversation() {
        checkInternet = NetworkChecking.isConnected(GroupChatDetailActivity.this);
        if (checkInternet) {
            String urlgroup = AppUrls.BASE_URL + AppUrls.GROUP_MESSAGES_CONVERSATION + "?group_id=" + group_id + "&user_id=" + user_id + "&user_type=" + user_type + "&conversation_type=" + group_conversation_type;
            Log.d("GROUPCONVERYURL", urlgroup);
            StringRequest reqgroup = new StringRequest(Request.Method.GET, urlgroup, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("GROUPCONVRESP:", response);
                    try {
                        JSONObject jobj = new JSONObject(response);
                        String response_code = jobj.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobj.getJSONArray("data");
                            progressDialog.cancel();

                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                GroupConversationModel groupChatDetail = new GroupConversationModel();
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                groupChatDetail.setMsg_id(jsonObject1.getString("msg_id"));
                                groupChatDetail.setFrom_id(jsonObject1.getString("from_id"));
                                groupChatDetail.setFrom_name(jsonObject1.getString("from_name"));
                                groupChatDetail.setProfile_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("profile_pic"));
                                groupChatDetail.setRecipient(jsonObject1.getString("recipient"));
                                groupChatDetail.setMsg(jsonObject1.getString("msg"));
                                groupChatDetail.setIs_read(jsonObject1.getString("is_read"));
                                groupChatDetail.setSent_on_txt(jsonObject1.getString("sent_on_txt"));
                                groupChatDetail.setSent_on(jsonObject1.getString("sent_on"));
                                groupDetailModalList.add(groupChatDetail);

                               /* JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                 values.put(GroupChatDB.ID,jsonObject1.getString("msg_id"));
                                 values.put(GroupChatDB.FROM_NAME,jsonObject1.getString("from_name"));
                                values.put(GroupChatDB.RECIPIENT,jsonObject1.getString("recipient"));
                                values.put(GroupChatDB.MSG,jsonObject1.getString("msg"));
                                values.put(GroupChatDB.IS_READ,jsonObject1.getString("is_read"));
                                values.put(GroupChatDB.SET_ON_TEXT,jsonObject1.getString("sent_on_txt"));
                                values.put(GroupChatDB.PROFILE_PIC,AppUrls.BASE_IMAGE_URL + jsonObject1.getString("profile_pic"));
                                values.put(GroupChatDB.SET_ON,jsonObject1.getString("sent_on"));
                                Log.d("CHATINSERT",""+values);
                               groupChatDB.addGroupChatList(values);*/


                            }
                          //  groupChatDetailList();
                           recycler_group_chatconversn.setAdapter(groupDetailAdapter);
                        }

                        if (response_code.equals("10200")) {
                            progressDialog.cancel();
                            Toast.makeText(GroupChatDetailActivity.this, "Invlid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (response_code.equals("10300")) {
                            Toast.makeText(GroupChatDetailActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException json) {
                        json.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupChatDetailActivity.this);
            requestQueue.add(reqgroup);
        } else
            {
               // groupChatDetailList();
                Toast.makeText(GroupChatDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void groupChatDetailList()
    {
        Log.d("CHATINNN","CHATINNN");
        List<String> msg_content = groupChatDB.getMsgName();
        Log.d("CHATDATAAAAA",""+msg_content);
        if (msg_content.size() > 0)
        {
            List<String> msgtime = groupChatDB.getMessageTimeStamp();
            List<String> msg_receptint = groupChatDB.getReceipent();
            List<String> from_name = groupChatDB.getFromName();
            for (int i = 0; i < msg_content.size(); i++)
            {
                GroupConversationModel groupChatDetail = new GroupConversationModel();
                groupChatDetail.setMsg(msg_content.get(i));
                groupChatDetail.setSent_on_txt(msgtime.get(i));
                groupChatDetail.setRecipient(msg_receptint.get(i));
                groupChatDetail.setFrom_name(from_name.get(i));
                groupDetailModalList.add(groupChatDetail);
            }
            recycler_group_chatconversn.setAdapter(groupDetailAdapter);

        } else {
            //progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(final View view) {
        if (view == close) {
            finish();
        }
        if (view == grp_send_mesage) {
            groupDetailModalList.clear();

            checkInternet = NetworkChecking.isConnected(this);
            if (validate()) {
                if (checkInternet) {
                    Log.d("GRPSENDURL:", AppUrls.BASE_URL + AppUrls.GROUP_SEND_MESSAGES);
                    final String msg_text = grp_mesagetext_edt.getText().toString().trim();
                    StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GROUP_SEND_MESSAGES, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MESRESPGRP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    Toast.makeText(getApplicationContext(), "Message sent successfully..", Toast.LENGTH_SHORT).show();
                                    hideSoftKeyboard(view);
                                    getGroupChatConversation();
                                    grp_mesagetext_edt.setText("");
                                }
                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("group_id", group_id);
                            params.put("user_id", user_id);
                            params.put("user_type", user_type);
                            params.put("conversation_type", group_conversation_type);
                            params.put("msg", msg_text);
                            Log.d("GRPSENPARAM:", params.toString());
                            return params;

                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("MSGHEAED", headers.toString());
                            return headers;
                        }
                    };
                    forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(GroupChatDetailActivity.this);
                    requestQueue.add(forgetReq);

                } else {
                    Toast.makeText(GroupChatDetailActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private boolean validate() {
        boolean result = true;
        int flag = 0;
        String name = grp_mesagetext_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 1)) {
            grp_mesagetext_edt.setError("Minimum 3 characters required");
            result = false;
        }
        return result;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
