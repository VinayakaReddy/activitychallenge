package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.filters.CustomFilterForAllSponsorsList;
import in.activitychallenge.activitychallenge.holder.AllMembersHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.AllMembersItemClickListener;
import in.activitychallenge.activitychallenge.models.AllMembersModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllSponsorActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView close, no_data_image;
    TextView allmembers_toolbar_title;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    SearchView allsponsors_search;
    RecyclerView recycler_all_sponsors;
    AllSponsorsAdapter all_sponsorsAdapter;
    ArrayList<AllMembersModel> allsponsorMoldelList = new ArrayList<AllMembersModel>();
    RecyclerView.LayoutManager layoutManager;
   /* TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;*/

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_sponsor);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        pprogressDialog = new ProgressDialog(AllSponsorActivity.this);

        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        no_data_image = (ImageView) findViewById(R.id.no_data_image);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);


        recycler_all_sponsors = (RecyclerView) findViewById(R.id.recycler_all_sponsors);
        all_sponsorsAdapter = new AllSponsorsAdapter(allsponsorMoldelList, AllSponsorActivity.this, R.layout.row_all_members);
        layoutManager = new LinearLayoutManager(this);
        recycler_all_sponsors.setNestedScrollingEnabled(false);
        recycler_all_sponsors.setLayoutManager(layoutManager);

        allsponsors_search = (SearchView) findViewById(R.id.allsponsors_search);

        allsponsors_search.setOnClickListener(this);
        View view = findViewById(R.id.custom_tab);
        allsponsors_search.setIconified(false);
        allsponsors_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allsponsors_search.setIconified(false);
            }
        });
        allsponsors_search.clearFocus();
        allsponsors_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
               if(all_sponsorsAdapter!=null)
                all_sponsorsAdapter.getFilter().filter(query);
                return false;
            }
        });

        getAllSponsors();

    }

    private void getAllSponsors() {
        allsponsorMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            //
            String url_all_sponsors = AppUrls.BASE_URL + AppUrls.SPONSOR_ALL_LIST + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("ALLSPONSORURL", url_all_sponsors);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_sponsors, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("ALLSPONSORRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMembersModel allmember = new AllMembersModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                JSONArray jarrActivityimg = jdataobj.getJSONArray("recent_five_activity");
                                allmember.setImage1(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(0).getString("img1"));
                                allmember.setImage2(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(1).getString("img2"));
                                allmember.setImage3(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(2).getString("img3"));
                                allmember.setImage4(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(3).getString("img4"));
                                allmember.setImage5(AppUrls.BASE_IMAGE_URL + jarrActivityimg.getJSONObject(4).getString("img5"));
                                allsponsorMoldelList.add(allmember);
                            }
                            recycler_all_sponsors.setAdapter(all_sponsorsAdapter);

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(AllSponsorActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            no_data_image.setVisibility(View.VISIBLE);
                            //Toast.makeText(AllMembersActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllSponsorActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(AllSponsorActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view)
    {
        if(view==close)
        {finish();}

    }

  public class AllSponsorsAdapter extends RecyclerView.Adapter<AllMembersHolder> implements Filterable
    {

        public ArrayList<AllMembersModel> allsponsorMoldelList, allmembersfilterList;
        AllSponsorActivity context;
        LayoutInflater li;
        int resource;
        Typeface typeface, typeface2;

        CustomFilterForAllSponsorsList filter;


        public AllSponsorsAdapter(ArrayList<AllMembersModel> allmemberlistModels, AllSponsorActivity context, int resource) {
            this.allsponsorMoldelList = allmemberlistModels;
            this.context = context;
            this.resource = resource;
            this.allmembersfilterList = allmemberlistModels;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
            typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
        }

        @Override
        public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            AllMembersHolder slh = new AllMembersHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final AllMembersHolder holder, final int position) {

            String str = allsponsorMoldelList.get(position).getName();
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.all_member_user_name.setText(Html.fromHtml(converted_string));

            holder.all_member_ranking_text.setText(Html.fromHtml(allsponsorMoldelList.get(position).getOverall_rank()));
            holder.all_member_percentage_text.setText(Html.fromHtml(allsponsorMoldelList.get(position).getWinning_per() + " %"));
            holder.all_member_won_text.setText(Html.fromHtml(allsponsorMoldelList.get(position).getTotal_win()));
            holder.all_member_loss_text.setText(Html.fromHtml(allsponsorMoldelList.get(position).getTotal_loss()));


            String img_profile = allsponsorMoldelList.get(position).getProfile_pic();

            Log.d("PPPROFILE", img_profile);
            if (img_profile.equals("null") && img_profile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.placeholder_dummy)
                        .placeholder(R.drawable.dummy_user_profile)
                        .resize(60, 60)
                        .into(holder.all_member_image);
            } else {
                Picasso.with(context)
                        .load(allsponsorMoldelList.get(position).getProfile_pic())
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(holder.all_member_image);
            }


            Picasso.with(context)
                    .load(allsponsorMoldelList.get(position).getImage1())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.iv_one);

            Picasso.with(context)
                    .load(allsponsorMoldelList.get(position).getImage2())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.iv_two);

            Picasso.with(context)
                    .load(allsponsorMoldelList.get(position).getImage3())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.iv_three);

            Picasso.with(context)
                    .load(allsponsorMoldelList.get(position).getImage4())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.iv_four);

            Picasso.with(context)
                    .load(allsponsorMoldelList.get(position).getImage5())
                    .placeholder(R.drawable.img_circle_placeholder)
                    .into(holder.iv_five);


            holder.setItemClickListener(new AllMembersItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                    Intent intent=new Intent(context, MemberDetailActivity.class);
                    Log.d("IDDD:",allsponsorMoldelList.get(position).getId());
                    intent.putExtra("MEMBER_ID",allsponsorMoldelList.get(position).getId());
                    intent.putExtra("MEMBER_NAME",allsponsorMoldelList.get(position).getName());
                    intent.putExtra("member_user_type",allsponsorMoldelList.get(position).getUser_type());
                    context.startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {
            return this.allsponsorMoldelList.size();
        }


        @Override
        public Filter getFilter()
        {
            if (filter == null) {
                filter = new CustomFilterForAllSponsorsList(allmembersfilterList, this);
            }

            return filter;
        }
    }

}
