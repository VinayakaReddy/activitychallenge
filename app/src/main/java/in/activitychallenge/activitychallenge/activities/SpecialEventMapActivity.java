package in.activitychallenge.activitychallenge.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;


public class SpecialEventMapActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {


    ImageView close;
    GoogleMap event_loc_map;
    JSONArray locationArr;
    SupportMapFragment mapFragment;
    ArrayList<HashMap<String, String>> location = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map;
    // Latitude & Longitude

    private Double Latitude = 0.00;

    private Double Longitude = 0.00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_event_map);

        // close= (ImageView)findViewById(R.id.close);
        // close.setOnClickListener(this);


// *** Display Google Map
        // event_loc_map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap)).getMap;

        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap));
        //event_loc_map = mapFragment.getMap();
        // mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        String tmpArr = getIntent().getStringExtra("location");
        try {
            locationArr = new JSONArray(tmpArr);
            Log.d("LOCARRAY", locationArr.toString());  // [{"address":"Hyderabad","latitude":17.4525712,"longitude":78.23456}]

            addMarkersToMap(locationArr);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void addMarkersToMap(JSONArray loc) {
        Type stringStringMap = new TypeToken<ArrayList<Object>>() {
        }.getType();
        ArrayList<Object> tmpObj = new Gson().fromJson(String.valueOf(loc), stringStringMap);
        for (int i = 0; i < tmpObj.size(); i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            Map<String, Object> tmp = (Map<String, Object>) tmpObj.get(i);
            hashMap.put("Lattitude", tmp.get("latitude").toString());
            hashMap.put("Longitude", tmp.get("longitude").toString());
            hashMap.put("LocationName", tmp.get("address").toString());
            location.add(hashMap);

        }

    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        Latitude = Double.parseDouble(location.get(0).get("Lattitude").toString());
       Longitude = Double.parseDouble(location.get(0).get("Longitude").toString());
        LatLng coordinate = new LatLng(Latitude, Longitude);


       googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 10));
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        for (int i = 0; i < location.size(); i++) {

            Latitude = Double.parseDouble(location.get(i).get("Lattitude").toString());

            Longitude = Double.parseDouble(location.get(i).get("Longitude").toString());

            String name = location.get(i).get("LocationName").toString();

            MarkerOptions marker = new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(name);

            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_24));


            googleMap.addMarker(marker).showInfoWindow();

        }

       /* event_loc_map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);*/
    }
}
