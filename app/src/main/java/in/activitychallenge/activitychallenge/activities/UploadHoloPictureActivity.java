package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;


import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ImagePermissions;
import in.activitychallenge.activitychallenge.utilities.MyMultipartEntity;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class UploadHoloPictureActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, choose_img, selected_img;
    Button submit_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String device_id, access_token, user_id, user_type;
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "", challenge_id, activity_id, deviceModel, deviceName,group_id,user_typee;
    Bitmap bitmap;
    PictureChallengeStatusActivity pictObj;
    int timeinsec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_holo_picture);

        deviceModel = Build.MODEL;
        deviceName = Build.DEVICE;
        Log.d("DEVICEDETAILS", deviceModel + "\n" + deviceName);
        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        activity_id = bundle.getString("activity_id");
        group_id = bundle.getString("group_id");
       // user_typee = bundle.getString("USER_TYPE");
      //  Log.d("UUUSTYPE",user_typee);
        Log.d("BundleData", activity_id + "\n" + challenge_id);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id);

         pictObj=new PictureChallengeStatusActivity();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        choose_img = (ImageView) findViewById(R.id.choose_img);
        choose_img.setOnClickListener(this);
        selected_img = (ImageView) findViewById(R.id.selected_img);
        submit_btn = (Button) findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsec = (tz.getOffset(cal.getTimeInMillis()))/1000;
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
        if (v == choose_img) {

            selectOption();
        }
        if (v == submit_btn) {
            uploadImages();
        }
    }

    private void selectOption() {
        final Dialog dialog = new Dialog(UploadHoloPictureActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.group_camera_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button mCamerabtn = (Button) dialog.findViewById(R.id.cameradialogbtn);
        Button mGallerybtn = (Button) dialog.findViewById(R.id.gallerydialogbtn);
        Button btnCancel = (Button) dialog.findViewById(R.id.canceldialogbtn);
        final boolean result = ImagePermissions.checkPermission(UploadHoloPictureActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        mCamerabtn.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                if (result)

                    if (ContextCompat.checkSelfPermission(UploadHoloPictureActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(UploadHoloPictureActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(UploadHoloPictureActivity.this,
                                android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(UploadHoloPictureActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                ActivityCompat.requestPermissions(UploadHoloPictureActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);

                    }
                dialog.cancel();
            }

        });

        mGallerybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();
            }

        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }

        });
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 0:

                if (resultCode == RESULT_OK) {

                    Log.d("PPP", outputFileUri.toString());

                    if (outputFileUri != null) {

                        bitmap = decodeSampledBitmapFromUri(outputFileUri, selected_img.getWidth(), selected_img.getHeight());
                        if (bitmap == null) {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();

                        } else {
                            selectedImagePath = getRealPathFromURI(UploadHoloPictureActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            selected_img.setImageBitmap(bitmap);
                            //   uploadImages();
                        }
                    }
                }

                break;

            case 1:

                if (resultCode == RESULT_OK) {

                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, selected_img.getWidth(), selected_img.getHeight());

                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();

                    } else {

                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);

                        selected_img.setImageBitmap(bitmap);
                        //uploadImages();
                    }
                }
                break;

            default:

                break;
        }
    }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }

        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        String filePath =getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return bm;


       /* Bitmap bm = null;
        try {

// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return bm;*/
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(UploadHoloPictureActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(UploadHoloPictureActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(UploadHoloPictureActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(UploadHoloPictureActivity.this);
                    }
                });
        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private void uploadImages() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            if(selectedImagePath==null ||selectedImagePath.equals(""))
            {
                Toast.makeText(UploadHoloPictureActivity.this, "Please Select Image..!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                String urllllll = AppUrls.BASE_URL + AppUrls.UPLOAD_HOLO_PIC;
                Log.d("GROUPCREATIONURL:", urllllll);
                Log.d("PATHHHHH", selectedImagePath);

                UploadHoloPictureActivity.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {

                    }
                });
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);

                new UploadHoloPictureActivity.HttpUpload(this, selectedImagePath).execute();
            }


        }
        else
            {
            progressDialog.cancel();
            Toast.makeText(UploadHoloPictureActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;
        private HttpClient client;
        private ProgressDialog pd;
        private long totalSize;
        public String url = AppUrls.BASE_URL + AppUrls.UPLOAD_HOLO_PIC;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            client = new DefaultHttpClient(httpParameters);
        }


        @Override
        protected Void doInBackground(Void... params) {
            try {

                Bitmap bmp = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 15, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                HttpPost post = new HttpPost(url);
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                ContentBody cbFile = new InputStreamBody(in, "image/jpeg", "jpeg");
                entity.addPart("activity_id", new StringBody(activity_id));
                entity.addPart("challenge_id", new StringBody(challenge_id));
                entity.addPart("user_id", new StringBody(user_id));
                entity.addPart("timezone_in_sec",new StringBody(String.valueOf(timeinsec)));
              /*  if(user_typee.equals("USER"))
                {
                    entity.addPart("group_id", new StringBody(""));
                }
                else
                {
                    entity.addPart("group_id", new StringBody(group_id));
                }*/
                //before==""
                entity.addPart("evaluation_factor", new StringBody("IMAGE"));
                entity.addPart("evaluation_factor_unit", new StringBody("HAMMING_DISTANCE"));
                entity.addPart("file", cbFile);
                entity.addPart("device_id", new StringBody(device_id));
                entity.addPart("device_name", new StringBody(deviceName));
                entity.addPart("device_model", new StringBody(deviceModel));
                entity.addPart("group_id", new StringBody(group_id));
                Log.d("UploadData", cbFile + "\n" + activity_id + "\n" + challenge_id + "\n" + user_id + "\n" + device_id + "\n" + deviceName + "\n" + deviceModel+"///"+group_id+"//"+timeinsec);
                totalSize = entity.getContentLength();
                post.setEntity(entity);
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
// org.apache.http.entity.mime.content.InputStreamBody@44cda96
                if (statusCode == HttpStatus.SC_OK) {
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();

                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(UploadHoloPictureActivity.this, "Uploaded Successfully", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
