package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;

public class OtpValidationActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView otp_verify_btn;
    TextInputLayout otp_til;
    EditText otp_edt;
    TextView vefiy__otptxt;
    String user_reg_mobile, country_code, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    Typeface typeface, typeface2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otp_validation);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        pprogressDialog = new ProgressDialog(OtpValidationActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        user_reg_mobile = bundle.getString("MOBILE");
        country_code = bundle.getString("CC");
        user_id = bundle.getString("USERID");
        Log.d("MOBCC", user_reg_mobile + "//" + country_code + "//" + user_id);
        otp_edt = (EditText) findViewById(R.id.otp_edt);
        otp_edt.setTypeface(typeface);
        vefiy__otptxt = (TextView) findViewById(R.id.vefiy__otptxt);
        vefiy__otptxt.setTypeface(typeface2);
        otp_verify_btn = (ImageView) findViewById(R.id.otp_verify_btn);
        otp_verify_btn.setOnClickListener(this);

        getOTP();

    }

    private void getOTP() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            Log.d("GETOTPURL:", AppUrls.BASE_URL + AppUrls.GET_OTP);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_OTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GETOTPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Toast.makeText(OtpValidationActivity.this, "OTP has been Sent to your registered Mobile Number", Toast.LENGTH_SHORT).show();
                                   // Toast.makeText(OtpValidationActivity.this, "Please use the One-Time-Password (OTP) number sent to you to register", Toast.LENGTH_SHORT).show();
                                    pprogressDialog.dismiss();
                                    // JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "User Not Exixt..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10400")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("country_code", country_code);
                    params.put("mobile", user_reg_mobile);
                    Log.d("GETOTPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(OtpValidationActivity.this);
            requestQueue.add(stringRequest);
        } else {

        }
    }

    @Override
    public void onClick(View view) {
        if (view == otp_verify_btn) {
            if (otp_edt.getText().toString().length() ==6 ){
                verifyOtp();
            }
        } else {
            //something went wrong
        }

    }

    private boolean validate() {
        boolean result = true;
        String otp = otp_edt.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            otp_edt.setError("Invalid OTP");
            result = false;
        }
        return result;
    }
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.d("AUTOREAD", message);
                try {
                    Pattern p = Pattern.compile("(|^)\\d{6}");


                    if (message != null) {
                        Matcher m = p.matcher(message);
                        if (m.find()) {
                            otp_edt.setText(m.group(0));
                            if (otp_edt.getText().toString().length() ==6 ){
                                verifyOtp();
                            }
                        } else {
                            //something went wrong
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    public void verifyOtp(){
        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {
            if (checkInternet) {
                Log.d("VERIFOTPURL", AppUrls.BASE_URL + AppUrls.GET_VERIFIED_OTP);
                StringRequest strveriFyOtp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_VERIFIED_OTP, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pprogressDialog.dismiss();
                        Log.d("VERIFYOTPRESP", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(OtpValidationActivity.this, "Mobile has been Verified", Toast.LENGTH_LONG).show();

                                Intent i = new Intent(OtpValidationActivity.this, LoginActivity.class);
                                i.putExtra("USERID",user_id);
                                startActivity(i);

                            }
                            if (successResponceCode.equals("10200")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Mobile is Already Verified..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10500")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "User to verify OTP..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10600")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Invlid OTP..!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pprogressDialog.cancel();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pprogressDialog.cancel();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", otp_edt.getText().toString());
                        params.put("user_id", user_id);
                        Log.d("VERIFIOTPPPARAM:", params.toString());
                        return params;
                    }
                };
                strveriFyOtp.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(OtpValidationActivity.this);
                requestQueue.add(strveriFyOtp);
            } else {
                Toast.makeText(OtpValidationActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }

}
