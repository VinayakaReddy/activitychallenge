package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clock.scratch.ScratchView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class CongratsScratchActivity extends AppCompatActivity implements View.OnClickListener
{
     ProgressDialog pgd;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id,access_token,device_id ,user_type,challng_id,videoUrl,is_group_admin,scratch_card_id,is_scratched,scratch_amt,ref_txn_id = "";
    ImageView close;
    TextView prize_text,tell_to_button;
    ScratchView congrts_scratch_view;
    CardView congrts_frame;
    Typeface typeface,typeface2;
    DisplayMetrics dm;
    MediaController media_Controller;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrats_scratch);

        MobileAds.initialize(CongratsScratchActivity.this, getString(R.string.admob_app_id));



        initializeInterstitialAd();
        loadInterstitialAd();
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SSDERGNDATA", user_id + "\n" + access_token + "\n" + device_id);

        tell_to_button = (TextView) findViewById(R.id.tell_to_button);
        tell_to_button.setOnClickListener(this);
        pgd = new ProgressDialog(this);
        pgd.setMessage("Please wait......");
        pgd.setProgressStyle(R.style.DialogTheme);

        challng_id = getIntent().getExtras().getString("challenge_id");
        is_group_admin = getIntent().getExtras().getString("is_group_admin");

        Log.d("CHALG_ID",challng_id+"IS-"+is_group_admin);
        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);

        prize_text=(TextView)findViewById(R.id.prize_text);
        prize_text.setTypeface(typeface);
        congrts_scratch_view=(ScratchView)findViewById(R.id.congrts_scratch_view);

        getTieData();

    }

    private void getTieData()
    {
        checkInternet = NetworkChecking.isConnected(CongratsScratchActivity.this);
        if(checkInternet)
        {
            String url= AppUrls.BASE_URL+AppUrls.TIE_SCRATCH_CARD+"?challenge_id="+challng_id+"&user_id="+user_id;
            Log.d("congSCRURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("CONGSCRTRESP:",response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100"))
                                {

                                    JSONObject jsonObjData = jsonObject.getJSONObject("data");
                                    JSONObject jsonObjTie = jsonObjData.getJSONObject("tie_sc");

                                    scratch_amt=jsonObjTie.getString("amount");
                                    scratch_card_id=jsonObjTie.getString("scratch_card_id");
                                    is_scratched=jsonObjTie.getString("is_scratched");
                                    String scratching_date=jsonObjTie.getString("scratching_date");
                                    videoUrl=AppUrls.BASE_IMAGE_URL+jsonObjTie.getString("video_path");
                                    Log.d("dafasdf",scratch_card_id+"///"+videoUrl);
                                    if (videoUrl.equals(""))
                                    {
                                        mInterstitialAd.show();
                                        mInterstitialAd.setAdListener(new AdListener() {
                                            // Listen for when user closes ad
                                            public void onAdClosed()
                                            {
                                                // When user closes ad end this activity (go back to first activity)
                                                congrts_scratch_view.setEraseStatusListener(new ScratchView.EraseStatusListener()
                                                {
                                                    @Override
                                                    public void onProgress(int percent)
                                                    {
                                                        if (scratch_amt.equals("") || scratch_amt.equals("0")) {
                                                            prize_text.setText("Better luck  \n next time..!");
                                                        } else {

                                                            prize_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + scratch_amt + "</b>"));
                                                        }
                                                    }

                                                    @Override
                                                    public void onCompleted(View view)
                                                    {
                                                        congrts_scratch_view.clear();
                                                        sendScratchId(scratch_card_id);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                    else
                                        {
                                        getPlayVideo(videoUrl, scratch_amt, scratch_card_id);
                                    }

                                }
                                if(status.equals("10200"))
                                {
                                    Toast.makeText(CongratsScratchActivity.this,"Invalid input...!",Toast.LENGTH_LONG).show();
                                }
                                if(status.equals("10300"))
                                {
                                 //  Toast.makeText(CongratsScratchActivity.this,"Scratch card not created, Yet..!",Toast.LENGTH_LONG).show();
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(CongratsScratchActivity.this);
            requestQueue.add(stringRequest);

        }
        else
        {
            Toast.makeText(CongratsScratchActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();
        }
     }

    private void getPlayVideo(String videoUrl, final String amt, final String scrtch_id)
    {

        Log.d("VVVVVVVV",videoUrl+"------"+amt);
        final Dialog dialog = new Dialog(CongratsScratchActivity.this);// add here your class name
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.scratch_custom_videoview_dialog);
        final   VideoView scratch_vid_view = (VideoView) dialog.findViewById(R.id.scratch_vid_view);

        dialog.show();

        media_Controller = new MediaController(this);

        dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;
        int width = dm.widthPixels;
        scratch_vid_view.setMinimumWidth(width);
        scratch_vid_view.setMinimumHeight(height);
        String uriPath = videoUrl;
        try {
            Uri video = Uri.parse(uriPath);
            scratch_vid_view.setVideoURI(video);
            scratch_vid_view.setMediaController(media_Controller);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        scratch_vid_view.requestFocus();
        scratch_vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {

                scratch_vid_view.start();

            }
        });
      /*  scratch_vid_view.setMediaController(media_Controller);
        scratch_vid_view.setVideoURI(Uri.parse(videoUrl));
        scratch_vid_view.start();*/
        dialog.setCanceledOnTouchOutside(false);

        scratch_vid_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer)
            {
                dialog.dismiss();
            }
        });
//////////////////////after video completion////scratch card

            congrts_scratch_view.setEraseStatusListener(new ScratchView.EraseStatusListener()
            {
                @Override
                public void onProgress(int percent)
                {
                    if (amt.equals("") || amt.equals("0")) {
                        prize_text.setText("Better luck  \n next time..!");
                    } else {

                        prize_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + amt + "</b>"));
                    }
                }

                @Override
                public void onCompleted(View view)
                {
                    congrts_scratch_view.clear();
                     sendScratchId(scrtch_id);
                }
            });


    }
    private void sendScratchId(String scrtch_id)
    {
        checkInternet = NetworkChecking.isConnected(CongratsScratchActivity.this);
        final String _sratchcard = scrtch_id;

        if (checkInternet) {
            //   progressDialog.show();
            Log.d("TIESCRTIDURL:", AppUrls.BASE_URL + AppUrls.SCRATCH_CARD_DO);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SCRATCH_CARD_DO,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pgd.dismiss();
                            Log.d("TIESCRTIDRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                  //  Toast.makeText(CongratsScratchActivity.this, "Scratch card updated successfully..!", Toast.LENGTH_LONG).show();
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    ref_txn_id = data.getString("ref_txn_id");
                                   // afterScratch();

                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(CongratsScratchActivity.this, "Invalid input..!", Toast.LENGTH_LONG).show();
                                 //   afterScratch();

                                }
                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(CongratsScratchActivity.this, "Scratch Card not found..!", Toast.LENGTH_LONG).show();
                                //    afterScratch();

                                }
                                if (successResponceCode.equals("10400")) {
                              //      Toast.makeText(CongratsScratchActivity.this, "Scratch card is already scratched..!", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                pgd.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pgd.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("scratch_card_id", _sratchcard);
                    Log.d("TIESCRTCHPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CongratsScratchActivity.this);
            requestQueue.add(stringRequest);

        } else {
            pgd.cancel();
            Toast.makeText(CongratsScratchActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view)
    {
     if(view==close)
     {
        Log.d("scratch_amt",scratch_amt);
         if (ref_txn_id.equals(""))
         {
             congrts_scratch_view.clear();
             ref_txn_id = scratch_amt;

         }
         else {

             Intent returnIntent = new Intent();
             returnIntent.putExtra("AMOUNTSEND",scratch_amt);
             CongratsScratchActivity.this.setResult(Activity.RESULT_OK,returnIntent);
             finish();


         }
        // finish();
     }

        if (view == tell_to_button) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I have won the amount  " +"$"+scratch_amt+ " in ActivityChallenge App. To stay active and earn money Why don't you join?. "+AppUrls.SHARE_APP_URL);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
    }

    @Override
    public void onBackPressed()
    {

        Log.d("scratch_amt",scratch_amt);
        if (ref_txn_id.equals(""))
        {
            congrts_scratch_view.clear();
            ref_txn_id = scratch_amt;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("AMOUNTSEND",scratch_amt);
            CongratsScratchActivity.this.setResult(Activity.RESULT_OK,returnIntent);
            finish();

        }
        else {

//nothing


        }
       // super.onBackPressed();
    }





    private void loadInterstitialAd()
    {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }
    private void initializeInterstitialAd()
    {
        mInterstitialAd = new InterstitialAd(CongratsScratchActivity.this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
                Toast.makeText(CongratsScratchActivity.this, "Interstitial Ad Closed",
                        Toast.LENGTH_SHORT).show();
                //Load the ad and keep so that it can be used
                // next time the ad needs to be displayed
                loadInterstitialAd();

            }
        });
    }
}
