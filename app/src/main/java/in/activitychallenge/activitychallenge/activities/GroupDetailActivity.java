package in.activitychallenge.activitychallenge.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.GetGroupMembersAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberGroupDetailCompletAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberGroupDetailRunAdapter;
import in.activitychallenge.activitychallenge.adapter.MemberGroupDetailUpcomAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeGroupDetailModel;
import in.activitychallenge.activitychallenge.models.GetGroupMembersModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class GroupDetailActivity extends AppCompatActivity implements View.OnClickListener {

    String grp_name_, grp_id, group_id_G, group_name_G, group_pic_G, adm_img_A, grp_admin_id;
    int wallet_amtI, paused_challengesI, running_challengesI, pending_challengesI, upcoming_challengesI, overall_rank_G, win_challenges_G, lost_challenges_G, members_cnt_G, winning_percentage_G, user_rank_A, win_challenges_A, lost_challenges_A, winning_percentage_A;
    String group_user_type_A, id_A, user_name_A;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id, token;
    UserSessionManager session;
    LinearLayout deletGrpButt, exitGrpButt;
    ImageView close, edit_group, send_message, grp_img, grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5, adm_img, adm_img_1, adm_img_2, adm_img_3, adm_img_4, adm_img_5;
    TextView text_call, sponsor, request, text_mail, promote, grp_rank, grp_name, grp_memb_cnt, grp_won_cnt, grp_lost_cnt, grp_percent, grp_member_more_text, adm_rank, adm_name, adm_Type, adm_won_cnt, adm_lost_cnt, adm_percent, follow, challeng_req_toolbar_title;
    String country_code, call_mobile_no, admin_mail_id;
    Typeface typeface, typeface2;
    RecyclerView grp_mem_recycler, grp_detail_running_recycler, grp_detail_upcoming_recycler, grp_detail_completed_recycler;
    TextView running_more_text, upcoming_more_text, complet_more_text;
    LinearLayoutManager llm, layoutManager0, layoutManager1, layoutManager2;
    ArrayList<GetGroupMembersModel> grpMemList = new ArrayList<GetGroupMembersModel>();
    GetGroupMembersAdapter grpMemAdap;
    private static final int MAKE_CALL_PERMISSION_REQUEST_CODE = 1;
    String conver_flat_type = "GROUP_NOT_USER",ismember;
    //Running Adapter
    MemberGroupDetailRunAdapter challengGroupDetailRunAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupDetailRunList = new ArrayList<ChallengeGroupDetailModel>();
    //Upcoming Adapter
    MemberGroupDetailUpcomAdapter challengGroupDetailUppAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupDetailUpcomList = new ArrayList<ChallengeGroupDetailModel>();
    //Complet Adapter
    MemberGroupDetailCompletAdapter challengGroupDetailCompletAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupDetailCompletcomList = new ArrayList<ChallengeGroupDetailModel>();
    LinearLayout ll_running, ll_upcoming, ll_completed;
    Dialog dialog;
    String amount, time_span, from_on, to_on, has_running_challenges,session_user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        session_user_type = userDetails.get(UserSessionManager.USER_TYPE);


        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        edit_group = (ImageView) findViewById(R.id.edit_group);
        edit_group.setOnClickListener(this);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        send_message = (ImageView) findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        grp_id = getIntent().getExtras().getString("grp_id");
        grp_name_ = getIntent().getExtras().getString("grp_name");
        grp_admin_id = getIntent().getExtras().getString("grp_admin_id");
        conver_flat_type = getIntent().getExtras().getString("GROUP_CONVERSATION_TYPE");
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id + "////" + grp_admin_id + "//" + conver_flat_type);
        exitGrpButt = (LinearLayout) findViewById(R.id.exitGrpButt);
        exitGrpButt.setOnClickListener(this);
        follow = (TextView) findViewById(R.id.follow);
        text_call = (TextView) findViewById(R.id.text_call);
        request = (TextView) findViewById(R.id.request);
        promote = (TextView) findViewById(R.id.promote);
        sponsor = (TextView) findViewById(R.id.sponsor);
        sponsor.setOnClickListener(this);
        deletGrpButt = (LinearLayout) findViewById(R.id.deletGrpButt);
        deletGrpButt.setOnClickListener(this);
        text_call.setOnClickListener(this);
        text_mail = (TextView) findViewById(R.id.text_mail);
        text_mail.setOnClickListener(this);
        if (grp_admin_id.equals(user_id)) {
            edit_group.setVisibility(View.VISIBLE);
            promote.setVisibility(View.VISIBLE);
          //  request.setVisibility(View.VISIBLE);
            send_message.setVisibility(View.VISIBLE);
            follow.setVisibility(View.GONE);
            text_call.setVisibility(View.GONE);
            text_mail.setVisibility(View.GONE);
            sponsor.setVisibility(View.GONE);

            deletGrpButt.setVisibility(View.VISIBLE);
            conver_flat_type = "GROUP_INTERNAL";
        }
        else
        {}

        if(session_user_type.equals("SPONSOR"))
        {
            follow.setVisibility(View.GONE);
            send_message.setVisibility(View.GONE);
        }
        else
        {

        }

        ll_running = (LinearLayout) findViewById(R.id.ll_running);
        ll_upcoming = (LinearLayout) findViewById(R.id.ll_upcoming);
        ll_completed = (LinearLayout) findViewById(R.id.ll_completed);
        adm_img = (ImageView) findViewById(R.id.adm_img);
        adm_img_1 = (ImageView) findViewById(R.id.adm_img_1);
        adm_img_2 = (ImageView) findViewById(R.id.adm_img_2);
        adm_img_3 = (ImageView) findViewById(R.id.adm_img_3);
        adm_img_4 = (ImageView) findViewById(R.id.adm_img_4);
        adm_img_5 = (ImageView) findViewById(R.id.adm_img_5);

        grp_img_1 = (ImageView) findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) findViewById(R.id.grp_img_5);
        grp_img = (ImageView) findViewById(R.id.grp_img);

        challeng_req_toolbar_title = (TextView) findViewById(R.id.challeng_req_toolbar_title);
        try {
            String convert_cap = grp_name_.substring(0, 1).toUpperCase() + grp_name_.substring(1);
            challeng_req_toolbar_title.setText(convert_cap);
        }catch (Exception e){

        }


        grp_member_more_text = (TextView) findViewById(R.id.grp_member_more_text);
        grp_member_more_text.setOnClickListener(this);
        running_more_text = (TextView) findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        upcoming_more_text = (TextView) findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        complet_more_text = (TextView) findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);

        promote = (TextView) findViewById(R.id.promote);
        adm_rank = (TextView) findViewById(R.id.adm_rank);
        adm_name = (TextView) findViewById(R.id.adm_name);
        adm_Type = (TextView) findViewById(R.id.adm_Type);
        adm_won_cnt = (TextView) findViewById(R.id.adm_won_cnt);
        adm_lost_cnt = (TextView) findViewById(R.id.adm_lost_cnt);
        adm_percent = (TextView) findViewById(R.id.adm_percent);

        grp_name = (TextView) findViewById(R.id.grp_name);
        grp_memb_cnt = (TextView) findViewById(R.id.grp_memb_cnt);
        grp_rank = (TextView) findViewById(R.id.grp_rank);
        grp_won_cnt = (TextView) findViewById(R.id.grp_won_cnt);
        grp_lost_cnt = (TextView) findViewById(R.id.grp_lost_cnt);
        grp_percent = (TextView) findViewById(R.id.grp_percent);

        grp_detail_running_recycler = (RecyclerView) findViewById(R.id.grp_detail_running_recycler);
        grp_detail_upcoming_recycler = (RecyclerView) findViewById(R.id.grp_detail_upcoming_recycler);
        grp_detail_completed_recycler = (RecyclerView) findViewById(R.id.grp_detail_completed_recycler);

        challengGroupDetailRunAdpter = new MemberGroupDetailRunAdapter(challengGroupDetailRunList, GroupDetailActivity.this, R.layout.row_my_challenges_group_detail);
        layoutManager0 = new LinearLayoutManager(this);
        grp_detail_running_recycler.setNestedScrollingEnabled(false);
        grp_detail_running_recycler.setLayoutManager(layoutManager0);

        challengGroupDetailUppAdpter = new MemberGroupDetailUpcomAdapter(challengGroupDetailUpcomList, GroupDetailActivity.this, R.layout.row_my_challenges_group_detail);
        layoutManager1 = new LinearLayoutManager(this);
        grp_detail_upcoming_recycler.setNestedScrollingEnabled(false);
        grp_detail_upcoming_recycler.setLayoutManager(layoutManager1);

        challengGroupDetailCompletAdpter = new MemberGroupDetailCompletAdapter(challengGroupDetailCompletcomList, GroupDetailActivity.this, R.layout.row_my_challenges_group_detail);
        layoutManager2 = new LinearLayoutManager(this);
        grp_detail_completed_recycler.setNestedScrollingEnabled(false);
        grp_detail_completed_recycler.setLayoutManager(layoutManager2);

        grp_mem_recycler = (RecyclerView) findViewById(R.id.grp_mem_recycler);
        grpMemAdap = new GetGroupMembersAdapter(GroupDetailActivity.this, grpMemList, R.layout.row_get_group_members);
        llm = new LinearLayoutManager(this);
        grp_mem_recycler.setNestedScrollingEnabled(false);
        grp_mem_recycler.setLayoutManager(llm);
        promote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promoteMyGroup();
            }
        });
        getGroupDetail();
        getGroupFollowingStatus();
        getGroupChallenges();

    }

    private void getGroupChallenges() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            challengGroupDetailRunList.clear();
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + grp_id + "&user_type=GROUP&type="+"GROUP";
            Log.d("GROUPPCHALLURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("GGGRPOCHALLINDVRESP:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jObjData = jsonObject.getJSONObject("data");
                                    int running_cnt = jObjData.getInt("running_cnt");
                                    int upcomming_cnt = jObjData.getInt("upcomming_cnt");
                                    int completed_cnt = jObjData.getInt("completed_cnt");
                                    if (running_cnt > 2) {
                                    //    running_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (upcomming_cnt > 2) {
                                   //     upcoming_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (completed_cnt > 2) {
                                   //     complet_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (running_cnt != 0) {
                                        JSONArray jsonArrayrunning = jObjData.getJSONArray("running");
                                        Log.d("RUNNNN", jsonArrayrunning.toString());
                                        for (int i = 0; i < jsonArrayrunning.length(); i++) {
                                            JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                            ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                                            am_runn.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_runn.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_runn.setUser_name(jsonObject1.getString("user_name"));

                                            am_runn.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_runn.setStatus(jsonObject1.getString("status"));
                                            am_runn.setAmount(jsonObject1.getString("amount"));
                                            am_runn.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_runn.setStart_on(jsonObject1.getString("start_on"));
                                            am_runn.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_runn.setPause_access(jsonObject1.getString("pause_access"));
                                            am_runn.setResume_on(jsonObject1.getString("resume_on"));
                                            am_runn.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_runn.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_runn.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_runn.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_runn.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupDetailRunList.add(am_runn);
                                        }
                                    } else {
                                        ll_running.setVisibility(View.GONE);
                                    }
                                    //upcoming
                                    if (upcomming_cnt != 0) {
                                        JSONArray jsonArrayupcom = jObjData.getJSONArray("upcomming");
                                        Log.d("UPPPP", jsonArrayupcom.toString());
                                        for (int j = 0; j < jsonArrayupcom.length(); j++) {
                                            JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                            ChallengeGroupDetailModel am_up = new ChallengeGroupDetailModel();
                                            am_up.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_up.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_up.setUser_name(jsonObject1.getString("user_name"));
                                            am_up.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_up.setStatus(jsonObject1.getString("status"));
                                            am_up.setAmount(jsonObject1.getString("amount"));
                                            am_up.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_up.setStart_on(jsonObject1.getString("start_on"));
                                            am_up.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_up.setPause_access(jsonObject1.getString("pause_access"));
                                            am_up.setResume_on(jsonObject1.getString("resume_on"));
                                            am_up.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_up.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_up.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_up.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_up.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupDetailUpcomList.add(am_up);
                                        }
                                    } else {
                                        ll_upcoming.setVisibility(View.GONE);
                                    }
                                    //complete
                                    if (completed_cnt != 0) {
                                        JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                        Log.d("COMPL", jsonArraycomplet.toString());
                                        for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                            JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                            ChallengeGroupDetailModel am_complet = new ChallengeGroupDetailModel();
                                            am_complet.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_complet.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_complet.setUser_name(jsonObject1.getString("user_name"));
                                            am_complet.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_complet.setStatus(jsonObject1.getString("status"));
                                            am_complet.setAmount(jsonObject1.getString("amount"));
                                            am_complet.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_complet.setStart_on(jsonObject1.getString("start_on"));
                                            am_complet.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_complet.setPause_access(jsonObject1.getString("pause_access"));
                                            am_complet.setResume_on(jsonObject1.getString("resume_on"));
                                            am_complet.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_complet.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_complet.setPaused_by(jsonObject1.getString("paused_by"));
                                            am_complet.setChallenge_type(jsonObject1.getString("challenge_type"));
                                            am_complet.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupDetailCompletcomList.add(am_complet);
                                        }
                                    } else {
                                        ll_completed.setVisibility(View.GONE);
                                    }

                                    grp_detail_running_recycler.setAdapter(challengGroupDetailRunAdpter);
                                    grp_detail_upcoming_recycler.setAdapter(challengGroupDetailUppAdpter);
                                    grp_detail_completed_recycler.setAdapter(challengGroupDetailCompletAdpter);
                                }
                                if (status.equals("10200")) {
                                    Toast.makeText(GroupDetailActivity.this, "Invalid input...!", Toast.LENGTH_LONG).show();
                                }
                                if (status.equals("10300")) {
                                    Toast.makeText(GroupDetailActivity.this, "No data Found..!", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GGGGG_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getGroupFollowingStatus() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_FOLLOWING_STATUS;
            Log.d("GROUPFOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("GroupFOLOWSTATURESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String status = jobcode.getString("status");
                        String response_code = jobcode.getString("response_code");
                        if (status.equals("1")) {
                            follow.setText("UnFollow");
                            follow.setTextColor(Color.parseColor("#ffffff"));
                            follow.setBackgroundResource(R.drawable.bg_fill);
                            follow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getUnFollowGroup();
                                }
                            });

                        } else {
                            follow.setText("Follow");
                            follow.setBackgroundResource(R.drawable.background_for_login_and_signup);
                            follow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getFollowGroup();
                                }
                            });
                        }


                        if (response_code.equals("10200"))
                        {
                            follow.setText("UnFollow");
                            follow.setTextColor(Color.parseColor("#ffffff"));
                            follow.setBackgroundResource(R.drawable.bg_fill);
                          //  Toast.makeText(GroupDetailActivity.this, "Unable to Send Request..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10100")) {
                            pprogressDialog.dismiss();
                           // follow.setTextColor(Color.parseColor("#ffffff"));
                          //  follow.setBackgroundColor(Color.parseColor("#ff80cbc4"));
                           // Toast.makeText(GroupDetailActivity.this, "You Can Follow Group!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                         //   Toast.makeText(GroupDetailActivity.this, "Not following group!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("GRPFOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUnFollowGroup() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_UNFOLLOW;
            Log.d("UNGRPFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("UNGRPFOLOWRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            //JSONObject jobj=jobcode.getJSONObject("data");
                            Toast.makeText(GroupDetailActivity.this, "UnFollow Group successfully", Toast.LENGTH_LONG).show();
                            getGroupFollowingStatus();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("UNFGRPOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getFollowGroup() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_FOLLOW;
            Log.d("GRPFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("FOLOWGRPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            // JSONObject jobj=jobcode.getJSONObject("data");
                            Toast.makeText(GroupDetailActivity.this, "Following group successfully", Toast.LENGTH_LONG).show();
                            getGroupFollowingStatus();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("FOLLOWFGRPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void  getGroupDetail() {
        grpMemList.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("GroupDetailsUrl", AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + grp_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + grp_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GroupDetailsResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                  //  Toast.makeText(GroupDetailActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();
                                    JSONObject getData = jsonObject.getJSONObject("data");

                                    has_running_challenges=getData.getString("has_running_challenges");
                                    if(has_running_challenges.equals("1"))
                                    {
                                        edit_group.setVisibility(View.GONE);
                                    }
                                    else
                                        {
                                            if(grp_admin_id.equals(user_id))
                                            {
                                                edit_group.setVisibility(View.VISIBLE);
                                            }
                                            else
                                            {
                                                edit_group.setVisibility(View.GONE);
                                            }

                                        }

                                    int grp_member_count = getData.getInt("member_count");
                                    if (grp_member_count > 2) {
                                        grp_member_more_text.setVisibility(View.VISIBLE);
                                    }
                                    //GET_GROUP_SECTION
                                    JSONObject getGroupData = getData.getJSONObject("group_details");
                                    group_id_G = getGroupData.getString("group_id");
                                    group_name_G = getGroupData.getString("group_name");
                                    String str = group_name_G;
                                    String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
                                    grp_name.setText("" + converted_string);
                                    group_pic_G = AppUrls.BASE_IMAGE_URL + getGroupData.getString("group_pic");
                                    Log.d("PPPP:", group_pic_G);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(group_pic_G)
                                            .placeholder(R.drawable.dummy_white_group_profile)
                                            //   .resize(60,60)
                                            .into(grp_img);
                                    overall_rank_G = getGroupData.getInt("overall_rank");
                                    grp_rank.setText(String.valueOf(overall_rank_G));
                                    win_challenges_G = getGroupData.getInt("win_challenges");
                                    grp_won_cnt.setText(String.valueOf(win_challenges_G));
                                    lost_challenges_G = getGroupData.getInt("lost_challenges");
                                    grp_lost_cnt.setText(String.valueOf(lost_challenges_G));
                                    winning_percentage_G = getGroupData.getInt("winning_percentage");
                                    grp_percent.setText(String.valueOf(winning_percentage_G) + " " + "%");
                                    members_cnt_G = getGroupData.getInt("members_cnt");
                                     grp_memb_cnt.setText(String.valueOf(members_cnt_G) + " " + "members");
                                    ismember = getGroupData.getString("is_member_of_group");
                                    if(ismember.equals("0"))
                                    {
                                        exitGrpButt.setVisibility(View.GONE);
                                       deletGrpButt.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        if(grp_admin_id.equals(user_id))
                                        {
                                            deletGrpButt.setVisibility(View.VISIBLE);
                                        }
                                        else
                                        {
                                            exitGrpButt.setVisibility(View.VISIBLE);
                                        }
                                       //

                                    }

                                    String getGrp5Imgs1 = AppUrls.BASE_IMAGE_URL + getGroupData.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1");
                                    String getGrp5Imgs2 = AppUrls.BASE_IMAGE_URL + getGroupData.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2");
                                    String getGrp5Imgs3 = AppUrls.BASE_IMAGE_URL + getGroupData.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3");
                                    String getGrp5Imgs4 = AppUrls.BASE_IMAGE_URL + getGroupData.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4");
                                    String getGrp5Imgs5 = AppUrls.BASE_IMAGE_URL + getGroupData.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5");

                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getGrp5Imgs1)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(grp_img_1);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getGrp5Imgs2)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(grp_img_2);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getGrp5Imgs3)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(grp_img_3);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getGrp5Imgs4)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(grp_img_4);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getGrp5Imgs5)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(grp_img_5);
                                    //GET_ADMIN_SECTION
                                    JSONObject getAdmData = getData.getJSONArray("group_admin_details").getJSONObject(0);
                                    group_user_type_A = getAdmData.getString("group_user_type");
                                    adm_Type.setText(String.valueOf(group_user_type_A));
                                    id_A = getAdmData.getString("id");
                                    if (id_A.equals(user_id)) {
                                        promote.setVisibility(View.VISIBLE);
                                    } else {
                                        promote.setVisibility(View.INVISIBLE);
                                    }
                                    user_name_A = getAdmData.getString("user_name");
                                    String str_nnn = user_name_A;
                                    String converted_string_name = str_nnn.substring(0, 1).toUpperCase() + str_nnn.substring(1);
                                    adm_name.setText(String.valueOf(converted_string_name));
                                    user_rank_A = getAdmData.getInt("user_rank");
                                    adm_rank.setText(String.valueOf(user_rank_A));
                                    win_challenges_A = getAdmData.getInt("win_challenges");
                                    adm_won_cnt.setText(String.valueOf(win_challenges_A));
                                    lost_challenges_A = getAdmData.getInt("lost_challenges");
                                    adm_lost_cnt.setText(String.valueOf(lost_challenges_A));
                                    winning_percentage_A = getAdmData.getInt("winning_percentage");
                                    adm_percent.setText(String.valueOf(winning_percentage_A + " %"));
                                    adm_img_A = AppUrls.BASE_IMAGE_URL + getAdmData.getString("profile_pic");
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(adm_img_A)
                                          //   .resize(70,70)
                                            .placeholder(R.drawable.dummy_user_profile)
                                            .into(adm_img);

                                    call_mobile_no = getAdmData.getString("mobile");
                                    country_code = getAdmData.getString("country_code");
                                    admin_mail_id = getAdmData.getString("email");
                                    //  call_mobile_no = getAdmData.getInt("mobile");
                                    String getAdm5Imgs1 = AppUrls.BASE_IMAGE_URL + getAdmData.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1");
                                    String getAdm5Imgs2 = AppUrls.BASE_IMAGE_URL + getAdmData.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2");
                                    String getAdm5Imgs3 = AppUrls.BASE_IMAGE_URL + getAdmData.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3");
                                    String getAdm5Imgs4 = AppUrls.BASE_IMAGE_URL + getAdmData.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4");
                                    String getAdm5Imgs5 = AppUrls.BASE_IMAGE_URL + getAdmData.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5");

                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getAdm5Imgs1)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(adm_img_1);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getAdm5Imgs2)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(adm_img_2);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getAdm5Imgs3)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(adm_img_3);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getAdm5Imgs4)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(adm_img_4);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(getAdm5Imgs5)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .resize(30, 30)
                                            .into(adm_img_5);

                                    //GET_MEMBERS_SECTION
                                    JSONArray getMemData = getData.getJSONArray("member_details");
                                    for (int i = 0; i < getMemData.length(); i++) {
                                        GetGroupMembersModel gMemberList = new GetGroupMembersModel();
                                        JSONObject jMemArr = getMemData.getJSONObject(i);

                                        gMemberList.setGroup_user_type_M(jMemArr.getString("group_user_type"));
                                        gMemberList.setId_M(jMemArr.getString("id"));
                                        gMemberList.setUser_name_M(jMemArr.getString("user_name"));
                                        gMemberList.setProfile_pic_M(AppUrls.BASE_IMAGE_URL + jMemArr.getString("profile_pic"));
                                        gMemberList.setUser_rank_M(jMemArr.getInt("user_rank"));
                                        gMemberList.setWin_challenges_M(jMemArr.getInt("win_challenges"));
                                        gMemberList.setLost_challenges_M(jMemArr.getInt("lost_challenges"));
                                        gMemberList.setWinning_percentage_M(jMemArr.getInt("winning_percentage"));

                                        gMemberList.setImg1_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        gMemberList.setImg2_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        gMemberList.setImg3_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        gMemberList.setImg4_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        gMemberList.setImg5_M(AppUrls.BASE_IMAGE_URL + jMemArr.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));

                                        grpMemList.add(gMemberList);
                                    }
                                    grp_mem_recycler.setAdapter(grpMemAdap);


                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(GroupDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();

                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(GroupDetailActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {
            pprogressDialog.cancel();
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == deletGrpButt) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.delete_dialog);
            TextView cancelButt = (TextView) dialog.findViewById(R.id.cancelButt);
            Button DeleteButt = (Button) dialog.findViewById(R.id.DeleteButt);
            TextView running_chal = (TextView) dialog.findViewById(R.id.running_chal);
            running_chal.setText(String.valueOf(running_challengesI));
            TextView pending_chal = (TextView) dialog.findViewById(R.id.pending_chal);
            pending_chal.setText(String.valueOf(pending_challengesI));
            TextView upcoming_chal = (TextView) dialog.findViewById(R.id.upcoming_chal);
            upcoming_chal.setText(String.valueOf(upcoming_challengesI));
            TextView pause_chal = (TextView) dialog.findViewById(R.id.pause_chal);
            pause_chal.setText(String.valueOf(paused_challengesI));
            TextView wallet_amt = (TextView) dialog.findViewById(R.id.wallet_amt);
            wallet_amt.setText(String.valueOf(wallet_amtI));
            dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            DeleteButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cnfmDeleteGroup();
                }

            });
            cancelButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel(); // dismissing the popup
                }

            });
            dialog.show();
            getDeletegrp();
        }

        if (view == edit_group) {
            Intent craetefroup = new Intent(GroupDetailActivity.this, UpdateGroupActivity.class);
            craetefroup.putExtra("GROUPEDIT", "editgroup");
            craetefroup.putExtra("GroupId", grp_id);
            startActivity(craetefroup);
        }
        if (view == send_message) {
            Intent ingrpdetail = new Intent(GroupDetailActivity.this, GroupChatDetailActivity.class);
            ingrpdetail.putExtra("group_id", grp_id);
            ingrpdetail.putExtra("group_name", grp_name_);
            ingrpdetail.putExtra("profile_pic", group_pic_G);
            ingrpdetail.putExtra("condition", "GROUP_NOT_USER");
            ingrpdetail.putExtra("GROUP_CONVERSATION_TYPE", conver_flat_type);
            ingrpdetail.putExtra("FROM_TYPE", "GROUP");
            startActivity(ingrpdetail);
            Log.d("PARAMMMM", grp_id + "//" + grp_name_ + "//" + group_pic_G + "//" + conver_flat_type);
        }

        if (view == text_call) {
            call();
        }

        if (view == text_mail)
        {
            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?subject=" + "" + "&body=" + "" + "&to=" + admin_mail_id);
            testIntent.setData(data);
            startActivity(testIntent);
           /* String[] recipients = new String[]{admin_mail_id, ""};
            Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
            testIntent.setType("message/rfc822");
            testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
            startActivity(testIntent);*/
        }
        if (view == sponsor) {

            dialog = new Dialog(GroupDetailActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialolg_group_sponsor_request);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView dummy_text = (TextView) dialog.findViewById(R.id.dummy_text);
            dummy_text.setTypeface(typeface);
            TextView group_name_text = (TextView) dialog.findViewById(R.id.group_name_text);
            TextView group_rank_text = (TextView) dialog.findViewById(R.id.group_rank_text);
            final EditText group_sponsor_amount_edt = (EditText) dialog.findViewById(R.id.group_sponsor_amount_edt);
            Button sponsor_submit_btn = (Button) dialog.findViewById(R.id.sponsor_submit_btn);
            group_name_text.setText(group_name_G);
            group_name_text.setTypeface(typeface);
            group_rank_text.setText(String.valueOf(overall_rank_G));
            group_rank_text.setTypeface(typeface);
            dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
            sponsor_submit_btn.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {
                    String amt_value = group_sponsor_amount_edt.getText().toString();
                    if (amt_value.equals("") || amt_value.equals("0")) {
                        Toast.makeText(GroupDetailActivity.this, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(GroupDetailActivity.this, ChallengePaymentActivity.class);
                        intent.putExtra("activity", "GroupDetail");
                        intent.putExtra("group_id", group_id_G);
                        intent.putExtra("group_user_type", "GROUP");
                        intent.putExtra("sponsorAmount", amt_value);
                        Log.d("GROUPSPONSOR", group_id_G + "\n" + group_user_type_A + "\n" + amt_value);
                        startActivity(intent);
                    }
                }

            });
            dialog.show();
        }

        if (view == grp_member_more_text) {
            Intent i = new Intent(GroupDetailActivity.this, AllMoreGroupFromGDActivity.class);
            i.putExtra("GROUPID", grp_id);
            startActivity(i);
        }
        if (view == running_more_text) {
            Intent i = new Intent(GroupDetailActivity.this, AllRunningChallengesActivity.class);
            startActivity(i);
        }
        if (view == upcoming_more_text) {
            Intent i = new Intent(GroupDetailActivity.this, AllUpcomingChallengesActivity.class);
            startActivity(i);
        }
        if (view == complet_more_text) {
            Intent i = new Intent(GroupDetailActivity.this, AllCompletedChallengesActivity.class);
            startActivity(i);
        }

        if (view == exitGrpButt) {
            groupExitStatus();
        }
    }

    private void cnfmDeleteGroup() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + "group/deleteGroup?user_id=" + "getDeleteStatus?user_id=" + user_id + "&group_id=" + grp_id;
            Log.d("GRP_CNFM_deletURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("Grp_CNFM_Del_RPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            Toast.makeText(GroupDetailActivity.this, "Group Deleted successfully", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            Intent ii = new Intent(GroupDetailActivity.this, MyGroupActivity.class);
                            startActivity(ii);
                            finish();

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GRP_CNFM_DEL_HEADERS", headers.toString());
                    return headers;
                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }

    }


    private void getDeletegrp() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_DEL + "getDeleteStatus?user_id=" + user_id + "&group_id=" + grp_id;
            Log.d("GRPdeletURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("Grp_Del_RPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                          //  Toast.makeText(GroupDetailActivity.this, "Group Delete Details Fetched", Toast.LENGTH_LONG).show();
                            JSONObject jobj = jobcode.getJSONObject("data");
                            paused_challengesI = jobj.getInt("paused_challenges");
                            upcoming_challengesI = jobj.getInt("upcoming_challenges");
                            running_challengesI = jobj.getInt("running_challenges");
                            pending_challengesI = jobj.getInt("pending_challenges");
                            wallet_amtI = jobj.getInt("wallet_amt");
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GRP_DEL_HEADERS", headers.toString());
                    return headers;
                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }


    }

    private void call() {
        try {
            String uri = country_code + call_mobile_no;
            if (!TextUtils.isEmpty(uri)) {
                if (checkPermission(Manifest.permission.CALL_PHONE)) {
                    String dial = "tel:" + uri;
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                } else {
                    Toast.makeText(GroupDetailActivity.this, "Call Permission denied", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(GroupDetailActivity.this, "Enter a phone number", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Your call has failed...", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        if (checkPermission(Manifest.permission.CALL_PHONE)) {
            text_call.setEnabled(true);
        } else {
            text_call.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MAKE_CALL_PERMISSION_REQUEST_CODE);
        }

    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MAKE_CALL_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    text_call.setEnabled(true);
                    Toast.makeText(this, "You can call the number by clicking on the button", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    private void groupExitStatus() {

        checkInternet = NetworkChecking.isConnected(GroupDetailActivity.this);
        if (checkInternet) {
            String exit_url = AppUrls.BASE_URL + AppUrls.GROUP_MEMBER_DELETE_STATUS + "?member_id=" + user_id + "&group_id=" + grp_id;
            Log.d("EXIT_URL", exit_url);
            StringRequest reqExit = new StringRequest(Request.Method.GET, exit_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("EXITURLRPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            //Toast.makeText(GroupDetailActivity.this, "Group Exit Details Fetched", Toast.LENGTH_LONG).show();
                            JSONObject jobj = jobcode.getJSONObject("data");
                            String has_restriction = jobj.getString("has_restriction");
                            String upcomming = jobj.getString("upcomming");
                            String running = jobj.getString("running");
                            Log.d("ExitDetails", has_restriction + "//" + upcomming + "//" + running);

                            if (has_restriction.equals("1")) {
                                LayoutInflater inflater = getLayoutInflater();
                                View alertLayout = inflater.inflate(R.layout.layout_exitgroup_detail_dialog, null);
                                final TextView running_text = alertLayout.findViewById(R.id.running_text);
                                final TextView upcoming_text = alertLayout.findViewById(R.id.upcoming_text);
                                AlertDialog.Builder alert = new AlertDialog.Builder(GroupDetailActivity.this);
                                alert.setTitle("You Can't Exit from this Group,Please Contact to Group Admin...!");
                                // this is set the view from XML inside AlertDialog
                                alert.setView(alertLayout);
                                running_text.setText(running);
                                upcoming_text.setText(upcomming);
                                // disallow cancel of AlertDialog on click of back button and outside touch
                                alert.setCancelable(false);
                                alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                AlertDialog dialog = alert.create();
                                dialog.show();
                            } else {
                                exitFromGroup();
                            }
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("EXITGRP_HEADERS", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(reqExit);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void exitFromGroup() {
        checkInternet = NetworkChecking.isConnected(GroupDetailActivity.this);
        if (checkInternet) {
            pprogressDialog.show();
            Log.d("DELETURL", AppUrls.BASE_URL + AppUrls.GROUP_REMOVE_EXIT + "/" + grp_id + "/" + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, AppUrls.BASE_URL + AppUrls.GROUP_REMOVE_EXIT + "/" + grp_id + "/" + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("DELTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Toast.makeText(GroupDetailActivity.this, "You Are Successfullay Exit", Toast.LENGTH_LONG);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(GroupDetailActivity.this, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(GroupDetailActivity.this, "User is not member of the group..", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("EXITGRP_HEADERS", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


   /* @Override
    protected void onResume() {
        super.onResume();
        getGroupDetail();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getGroupDetail();
    }*/
    public void promoteMyGroup(){
        Log.d("PROMOTIONURL", AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + "GROUP" + "&entity_id=" + user_id);
        checkInternet = NetworkChecking.isConnected(GroupDetailActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + "GROUP" + "&entity_id=" + grp_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("PROMOTIONRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String id = jsonObject1.getString("id");
                                    String promotion_id = jsonObject1.getString("promotion_id");
                                    String entity_id = jsonObject1.getString("entity_id");
                                    String entity_type = jsonObject1.getString("entity_type");
                                    amount = jsonObject1.getString("amount");
                                    time_span = jsonObject1.getString("time_span");
                                    String status = jsonObject1.getString("status");
                                    String file = AppUrls.BASE_IMAGE_URL + jsonObject1.getString("banner_path");
                                    from_on = jsonObject1.getString("from_on");
                                    to_on = jsonObject1.getString("to_on");
                                    String last_apply_on_txt = jsonObject1.getString("last_apply_on_txt");
                                    String created_on = jsonObject1.getString("created_on");
                                    String week_completed = jsonObject1.getString("week_completed");
                                    int count = Integer.parseInt(week_completed);
                                    Log.d("COUNT", String.valueOf(count));
                                    if (count > 6) {
                                      //  Toast.makeText(GroupDetailActivity.this, "Redirecting to Next Page..!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(GroupDetailActivity.this, AddToPromoteActivity.class);
                                        intent.putExtra("user_type","GROUP");
                                        intent.putExtra("user_id",grp_id);
                                        startActivity(intent);
                                    } else {
                                        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(GroupDetailActivity.this);
                                        alertDialog.setTitle("You are not eligible to promote..!");
                                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                        alertDialog.show();
                                    }


                                        /*String pastDate = to_on.substring(0,10);

                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                        try {
                                            Date date = format.parse(pastDate);
                                            Log.d("DATEEEEEE", String.valueOf(date));

                                            Calendar past = Calendar.getInstance();
                                            Calendar future = Calendar.getInstance();
                                            try {
                                                past.setTime(format.parse(pastDate));
                                                future.add(past.DATE, 48);

                                                String one  = format.format(past.getTime());
                                                String two  = format.format(future.getTime());
                                                String three  = format.format(Calendar.getInstance().getTime());

                                                Log.d("OOOOO",one+"\n"+two+"\n"+three);
                                                Log.d("FFFFFFF", String.valueOf(past.DATE));
                                                Log.d("CHECKSTRING",pastDate+"\n"+future.toString());
                                                Log.d("CHEKKKKKKK",past+"\n"+future);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            if (Calendar.getInstance().after(future)){
                                                Toast.makeText(MyProfileActivity.this, "Redirecting to Next Page..!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(MyProfileActivity.this,AddToPromoteActivity.class);
                                                startActivity(intent);

                                            }else {
                                                Toast.makeText(MyProfileActivity.this, "You are not eligible to promote..!", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        }*/

                                }
                                if (response_code.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(GroupDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }if (response_code.equals("10300")) {

                                    Intent intent = new Intent(GroupDetailActivity.this, AddToPromoteActivity.class);
                                    intent.putExtra("user_type","GROUP");
                                    intent.putExtra("group_id",grp_id);
                                    intent.putExtra("action_type","GROUPPROMOTE");

                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }


    }


}
