package in.activitychallenge.activitychallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class StatusDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, share;
    TextView statu_detail_toolbar_title;
    Typeface typeface, typeface_bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_detail);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.innasoft.activitychallenge&hl=en");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
    }
}
