package in.activitychallenge.activitychallenge.activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.messenger.ShareToMessengerParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.DayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.adapter.OpponentDayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.models.DayDateWiseStatusModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ChallengeFinishedTime;
import in.activitychallenge.activitychallenge.utilities.ConnectivityReceiver;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.StatusMinuiteChallengeDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MinuiteChallengeStatusActivity extends AppCompatActivity implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {
    ImageView close, challenger_one_image, challenger_two_image,gps_track_img;

    TextView toolbar_title, vsTxt, participent_oponent_type, participent_user_type, participent_user_name, participent_oponent_name,memb_count_top,mem_cccount_down;
    Typeface typeface3;
    private boolean checkInternet;
    UserSessionManager session;
    boolean isTimer=false;
    String token = "", device_id = "", challenge_id, opponent_type,opponentType;
    ProgressDialog progressDialog;
    int timeinsec;
    int id = 1;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    Sensor stepSensor;
    SensorManager sManager;
    private long steps = 0;
    String steps_count = "";
    String distance_calc = "";
    long secondsRemaing;
    String challenge_goal,opponent_usd_type,usd_type,challenge_type,usd_type_in;
    TextView activity_name, goal, cash_price, challenger_one_steps, challenger_two_steps, remaining_challenger_one_steps, remaining_challenger_two_steps,
            challenger_one_time_remaining, challenger_two_time_remaining, challenger_one_rank, challenger_two_rank,acc_speed,update;
    LinearLayout ll_top, ll_down,ll_days_status_down,ll_days_status_top;

    String user_id, evaluation_factor, evaluation_factor_unit, android_id,user_type_for_challenge,group_id;

    private SensorManager sensorMan;
    private Sensor accelerometer;
    String user_completed_goal;

    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    String ti;
    int time_server;
    int time_calc;
    int msgflag=0;
    StatusMinuiteChallengeDB statusMinuiteChallengeDB;
    ContentValues values;
    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
    long totalSeconds;
    long intervalSeconds = 2;
    MyCountDownTimer timer;
    boolean isStarted;
    long starttime;
    Intent myGpsService;
    RecyclerView recyclerview_days_status_top,recyclerview_days_status_down;
    DayDateWiseChallengeStatusAdapter dayDateWiseChallengeStatusAdapter;
    ArrayList<DayDateWiseStatusModel> dayDateWiseStatusModel=new ArrayList<DayDateWiseStatusModel>();
    ArrayList<DayDateWiseStatusModel>opponentdayDateWiseStatusModel=new ArrayList<DayDateWiseStatusModel>();
    OpponentDayDateWiseChallengeStatusAdapter opponentDayDateWiseChallengeStatusAdapter;
    RecyclerView.LayoutManager layoutManager;
    private Thread detectorTimeStampUpdaterThread;
    ChallengeFinishedTime finishedTime;
    private Handler handler;
    Button serviceBtn;
    private boolean isRunning = true;
    SensorEventListener eventListener;
    SharedPreferences sensorPreference;
    SharedPreferences.Editor sensoreditor;
    private static boolean mIsSensorUpdateEnabled = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minuite_challenge_status);
        statusMinuiteChallengeDB = new StatusMinuiteChallengeDB(MinuiteChallengeStatusActivity.this);
        values = new ContentValues();
// In onCreate method
        sensorMan = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
        android_id = Settings.Secure.getString(MinuiteChallengeStatusActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        steps_count = "0";
        distance_calc = "0 sec";
        finishedTime=new ChallengeFinishedTime(this);
        finishedTime.challengeFinishTime();

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsec = (tz.getOffset(cal.getTimeInMillis()))/1000;

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        opponentType = bundle.getString("opponent_type");
        close = (ImageView) findViewById(R.id.close);
        challenger_one_image = (ImageView) findViewById(R.id.challenger_one_image);
        challenger_two_image = (ImageView) findViewById(R.id.challenger_two_image);
        close.setOnClickListener(this);
        gps_track_img = (ImageView) findViewById(R.id.gps_track_img);
        gps_track_img.setOnClickListener(this);
        memb_count_top = (TextView) findViewById(R.id.memb_count_top);
        memb_count_top.setOnClickListener(this);
        mem_cccount_down = (TextView) findViewById(R.id.mem_cccount_down);
        mem_cccount_down.setOnClickListener(this);
        update = (TextView) findViewById(R.id.update);
        update.setVisibility(View.GONE);
        ll_days_status_down = (LinearLayout) findViewById(R.id.ll_days_status_down);
        ll_days_status_top = (LinearLayout) findViewById(R.id.ll_days_status_top);
        recyclerview_days_status_top = (RecyclerView)findViewById(R.id.recyclerview_days_status_top);
        recyclerview_days_status_down = (RecyclerView)findViewById(R.id.recyclerview_days_status_down);
        ll_top = (LinearLayout) findViewById(R.id.ll_top);
        ll_down = (LinearLayout) findViewById(R.id.ll_down);
        typeface3 = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.hermes));
        serviceBtn=(Button)findViewById(R.id.service_tooglebtn);
        serviceBtn.setOnClickListener(this);
        vsTxt = (TextView) findViewById(R.id.vsTxt);
        acc_speed = (TextView) findViewById(R.id.acc_speed);
        vsTxt.setTypeface(typeface3);
        participent_oponent_type = (TextView) findViewById(R.id.participent_oponent_type);
        participent_user_type = (TextView) findViewById(R.id.participent_user_type);
        participent_user_name = (TextView) findViewById(R.id.participent_user_name);
        participent_oponent_name = (TextView) findViewById(R.id.participent_oponent_name);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        activity_name = (TextView) findViewById(R.id.activity_name);
        goal = (TextView) findViewById(R.id.goal);
        cash_price = (TextView) findViewById(R.id.cash_price);
        challenger_one_steps = (TextView) findViewById(R.id.challenger_one_steps);
        challenger_two_steps = (TextView) findViewById(R.id.challenger_two_steps);
        remaining_challenger_one_steps = (TextView) findViewById(R.id.remaining_challenger_one_steps);
        remaining_challenger_two_steps = (TextView) findViewById(R.id.remaining_challenger_two_steps);
        challenger_one_time_remaining = (TextView) findViewById(R.id.challenger_one_time_remaining);
        challenger_two_time_remaining = (TextView) findViewById(R.id.challenger_two_time_remaining);
        challenger_one_rank = (TextView) findViewById(R.id.challenger_one_rank);
        challenger_two_rank = (TextView) findViewById(R.id.challenger_two_rank);
        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        setupDetectorTimestampUpdaterThread();

        getMiniuiteChallengeStatus();
        String gpsStatus=bundle.getString("gps");
        if(gpsStatus!=null){
            if(gpsStatus.equals("1"))
                gps_track_img.setVisibility(View.VISIBLE);

        }
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                msgflag=1;
               updateThread();
            }
        }, 0, 2, TimeUnit.MINUTES);

        sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
        sensoreditor = sensorPreference.edit();
        isStarted=sensorPreference.getBoolean("isStarted",false);
        final String challengeId=sensorPreference.getString("challengeId","");
        serviceBtn.setText("START");
        serviceBtn.setBackgroundColor(Color.GREEN);
        sManager.unregisterListener(eventListener);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(challengeId.equalsIgnoreCase(challenge_id)) {

                    if (isStarted) {
                        serviceBtn.setText("STOP");
                        serviceBtn.setBackgroundColor(Color.RED);
                        mIsSensorUpdateEnabled=true;
                        initGpsListeners();
                        sManager.registerListener(eventListener, stepSensor, SensorManager.SENSOR_DELAY_FASTEST);
                    } else {
                        serviceBtn.setText("START");
                        serviceBtn.setBackgroundColor(Color.GREEN);
                        stopSensors();
                        //sManager.unregisterListener(eventListener);
                    }
                }


            }
        }, 1000);

        eventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                Sensor sensor = event.sensor;
                float[] values = event.values;
                int value = -1;
               if (!mIsSensorUpdateEnabled) {
                    stopSensors();
                    Log.v("SensorMM", "SensorUpdate  TIME disabled. returning");
                    return;
                }
                if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    SharedPreferences preferences = getSharedPreferences("Time", MODE_PRIVATE);
                    boolean isupdated=sensorPreference.getBoolean("isStarted",false);

                    if(isupdated){
                        float speed=getAccelerometer(event.values);
                       if(speed >10.7 && speed<11)
                        {
                            if (!isTimer)
                            {
                               if(timer==null)
                                    timer=new MyCountDownTimer(totalSeconds,intervalSeconds);
                                 timer.start();
                                 isTimer = true;
                                try {
                                    time_server = Integer.parseInt(ti);
                                } catch (NumberFormatException e) {

                                }
                                int total = time_server + time_calc;
                                ti = String.valueOf(total);
                                Log.d("time_value_for_lose", "" + ti);

                                final long user_goal = Long.parseLong(ti);
                                preferences=getSharedPreferences("Time",MODE_PRIVATE);
                                SharedPreferences.Editor editor=preferences.edit();
                                editor.putString("seconds",ti);
                                editor.apply();
                               challenger_one_steps.setText(String.format("%d M, %d s",
                                        TimeUnit.MILLISECONDS.toMinutes(user_goal),
                                        TimeUnit.MILLISECONDS.toSeconds(user_goal) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(user_goal))
                                ));
                                secondsRemaing=user_goal;
                                Log.v("Seconds", ">>" + challenger_one_steps.getText().toString());

                                update.setVisibility(View.VISIBLE);
                                update.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        try {
                                            msgflag = 0;
                                            sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, ti, android_id, Build.BRAND, Build.DEVICE);
                                        } catch (Exception e) {
                                        }


                                    }
                                });

                            }



                        }else {
                           if(timer!=null){

                               statusMinuiteChallengeDB.updateGoal(ti, challenge_id);
                               isTimer=false;
                               timer.cancel();
                               timer=null;
                              long milliseconds = Long.parseLong(challenge_goal);
                               long remaing=milliseconds-secondsRemaing;
                               totalSeconds=TimeUnit.MILLISECONDS.toSeconds(remaing);
                           }
                       }


                    }


                }


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
    }

   // CountDownTimer class
    public class MyCountDownTimer extends CountDownTimer
    {

        public MyCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            Log.d("secondsFinish ", "Finished");
           // isTimer=false;
              //timer.cancel();
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            isTimer=true;
             Log.d("seconds elapsed: ", String.valueOf(( millisUntilFinished)));
            time_calc = Integer.parseInt(String.valueOf(totalSeconds * 1000 - millisUntilFinished*1000));
            Log.v("seconds^^^^^ ",">>>"+time_calc);
            Log.v("seconds%%%% ",">>>"+totalSeconds);

        }
    }
    private float getAccelerometer(float[] values) {
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta; // perform low-cut filter


        return mAccel;
    }

    private void stopSensors(){
        sManager.unregisterListener(eventListener);
        mIsSensorUpdateEnabled =false;
        if(timer!=null){
            timer.cancel();
        }
        if(myGpsService==null)
            myGpsService=new Intent(MinuiteChallengeStatusActivity.this,GpsService.class);
        stopService(myGpsService);
    }
    private void initGpsListeners()
    {
        SharedPreferences preferences=getSharedPreferences("GPS",MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("challengeId",challenge_id);
        editor.apply();
        myGpsService = new Intent(MinuiteChallengeStatusActivity.this, GpsService.class);
        startService(myGpsService);

    }
    private void getMiniuiteChallengeStatus() {
        checkInternet = NetworkChecking.isConnected(MinuiteChallengeStatusActivity.this);

        if (checkInternet) {

            Log.d("MINUTCHALLSTATUS", AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id);

            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("MINUTSTATUSRESP", response);
                            try {
                                values.put(StatusMinuiteChallengeDB.CHALLENGE_ID,challenge_id);
                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("activity");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String challenge_id = jsonObject2.getString("challenge_id");
                                        String user_id_ = jsonObject2.getString("user_id");
                                        user_type_for_challenge = jsonObject2.getString("user_type");
                                        String opponent_id = jsonObject2.getString("opponent_id");
                                        opponent_type = jsonObject2.getString("opponent_type");
                                        participent_user_type.setText(user_type_for_challenge);
                                        if (user_type_for_challenge.equals("USER")){
                                            user_id = user_id;
                                        }else {
                                            group_id = user_id_;
                                        }
                                        participent_oponent_type.setText(opponent_type);
                                        if (opponent_type.equals("ADMIN")) {
                                            ll_down.setVisibility(View.GONE);
                                            vsTxt.setVisibility(View.GONE);
                                        }
                                        else {
                                            ll_down.setVisibility(View.VISIBLE);
                                            vsTxt.setVisibility(View.VISIBLE);
                                        }

                                        challenge_type = jsonObject2.getString("challenge_type");
                                        if(challenge_type.equals("DAILY"))
                                        {

                                            recyclerview_days_status_down.setVisibility(View.GONE);
                                            recyclerview_days_status_top.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            recyclerview_days_status_down.setVisibility(View.VISIBLE);
                                            recyclerview_days_status_top.setVisibility(View.VISIBLE);
                                        }

                                        challenge_goal = jsonObject2.getString("challenge_goal");
                                        SharedPreferences preferences=getSharedPreferences("Minutes",MODE_PRIVATE);
                                        SharedPreferences.Editor editor=preferences.edit();
                                        editor.putString("millis",challenge_goal);
                                        editor.apply();
                                        String amount = jsonObject2.getString("amount");
                                        cash_price.setText(" $" + amount);
                                        values.put(StatusMinuiteChallengeDB.AMOUNT," $" + amount);
                                        evaluation_factor = jsonObject2.getString("evaluation_factor");
                                        String activity_name_string = jsonObject2.getString("activity_name");
                                        values.put(StatusMinuiteChallengeDB.ACTIVITY_NAME,activity_name_string);
                                        activity_name.setText(activity_name_string);
                                        String activity_image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image");
                                        evaluation_factor_unit =  jsonObject2.getString("evaluation_factor_unit");
                                        Log.d("millisec_sec",challenge_goal);
                                        long milliseconds = Long.parseLong(challenge_goal);


                                        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
                                        String val = String.valueOf(minutes);
                                        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);
                                        goal.setText(val + " " + evaluation_factor_unit);
                                       // totalSeconds = seconds;
                                        goal.setText(String.format("%d M",
                                                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds))
                                        ));
                                        values.put(StatusMinuiteChallengeDB.CHALLENGE_GOAL,goal.getText().toString());
                                        String completed_on_txt = jsonObject2.getString("completed_on_txt");
                                    }

                                    String user_count = jsonObject1.getString("user_count");
                                    if(!user_count.equals("1"))
                                    {
                                        memb_count_top.setVisibility(View.VISIBLE);
                                        memb_count_top.setText("members "+user_count+"\n"+"see more.." );

                                    }
                                    else
                                    {
                                        memb_count_top.setVisibility(View.GONE);
                                    }
                                    String opponent_count = jsonObject1.getString("opponent_count");
                                    if(!opponent_count.equals("1"))
                                    {
                                        mem_cccount_down.setVisibility(View.VISIBLE);
                                        mem_cccount_down.setText("members "+opponent_count+"\n"+"see more.." );

                                    }
                                    else
                                    {
                                        mem_cccount_down.setVisibility(View.GONE);
                                    }


                                    int u_count = Integer.parseInt(user_count);
                                    int o_count = Integer.parseInt(opponent_count);
                                    int total_count = Integer.parseInt(challenge_goal);
                                    int remaining_challenger_one = total_count - u_count;
                                    int remaining_challenger_two = total_count - o_count;
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("user_details");
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    usd_type = jsonObject3.getString("user_type");

                                    user_completed_goal = jsonObject3.getString("user_completed_goal");

                                    String latest=statusMinuiteChallengeDB.latestGoal(challenge_id);
                                    if(latest!=null &&latest.length()>0){
                                        ti = latest;
                                        long milliseconds = Long.parseLong(challenge_goal);
                                        long remaing=milliseconds-Long.parseLong(latest);
                                        totalSeconds=TimeUnit.MILLISECONDS.toSeconds(remaing);
                                    }
                                   else{
                                        ti = user_completed_goal;
                                        long milliseconds = Long.parseLong(challenge_goal);
                                        long remaing=milliseconds-Long.parseLong(user_completed_goal);
                                        totalSeconds=TimeUnit.MILLISECONDS.toSeconds(remaing);
                                    }
                                    timer = new MyCountDownTimer(totalSeconds, intervalSeconds);

                                    long user_goal = Long.parseLong(ti);
                                    challenger_one_steps.setText(String.format("%d M, %d s",
                                            TimeUnit.MILLISECONDS.toMinutes(user_goal),
                                            TimeUnit.MILLISECONDS.toSeconds(user_goal) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(user_goal))
                                    ));
                                    JSONArray jsonchallenger = jsonObject1.getJSONArray("challenger_daywise_status");
                                    Log.d("KDKDKDKDK",jsonchallenger.toString());
                                    for (int i = 0; i < jsonchallenger.length(); i++)
                                    {
                                        JSONObject jdataobj = jsonchallenger.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        dayDateWiseStatusModel.add(allmember);
                                        Log.d("dd",dayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_top.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(MinuiteChallengeStatusActivity.this);
                                    recyclerview_days_status_top.setLayoutManager(new LinearLayoutManager(MinuiteChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    dayDateWiseChallengeStatusAdapter = new DayDateWiseChallengeStatusAdapter(dayDateWiseStatusModel, MinuiteChallengeStatusActivity.this, R.layout.row_daywise_challegne,evaluation_factor);
                                    recyclerview_days_status_top.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_top.setAdapter(dayDateWiseChallengeStatusAdapter);


                                    String user_name = jsonObject3.getString("user_name");
                               //     participent_user_name.setText(user_name);
                                    String user_rank = jsonObject3.getString("user_rank");
                               //     challenger_one_rank.setText(user_rank);
                                    String image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("image");
                                    String group_name = jsonObject3.getString("group_name");
                                    String group_rank = jsonObject3.getString("group_rank");
                                    String group_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("group_image");
                                    String super_admin_name = jsonObject3.getString("super_admin_name");
                                    String super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("super_admin_image");
                                    String super_admin_rank = jsonObject3.getString("super_admin_rank");

                                    if(usd_type.equals("USER"))
                                    {
                                        participent_user_name.setText(user_name);
                                        challenger_one_rank.setText(user_rank);
                                        participent_user_type.setText(usd_type);
                                      //  challenger_one_steps.setText(user_completed_goal);
                                        Picasso.with(MinuiteChallengeStatusActivity.this)
                                                .load(image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusMinuiteChallengeDB.USER_NAME,user_name);
                                        values.put(StatusMinuiteChallengeDB.USER_TYPE,usd_type);
                                        values.put(StatusMinuiteChallengeDB.USER_COMPLETED_GOAL,ti);
                                        values.put(StatusMinuiteChallengeDB.USER_RANK,user_rank);
                                        values.put(StatusMinuiteChallengeDB.IMAGE,image);
                                    }
                                    else
                                    {
                                        participent_user_name.setText(group_name);
                                        challenger_one_rank.setText(group_rank);
                                        participent_user_type.setText(usd_type);
                                      //  challenger_one_steps.setText(user_completed_goal);
                                        Picasso.with(MinuiteChallengeStatusActivity.this)
                                                .load(group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusMinuiteChallengeDB.USER_NAME,group_name);
                                        values.put(StatusMinuiteChallengeDB.USER_TYPE,usd_type);
                                        values.put(StatusMinuiteChallengeDB.USER_COMPLETED_GOAL,user_completed_goal);
                                        values.put(StatusMinuiteChallengeDB.USER_RANK,group_rank);
                                        values.put(StatusMinuiteChallengeDB.IMAGE,group_image);

                                    }

                                   /* Picasso.with(MinuiteChallengeStatusActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.dummy_user32)
                                            .into(challenger_one_image);*/
                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("opponent_details");
                                    JSONObject jsonObject4 = jsonArray2.getJSONObject(0);
                                    opponent_usd_type = jsonObject4.getString("opponent_type");
                                    String opponent_completed_goal = jsonObject4.getString("opponent_completed_goal");
                                    long opponent_goal = Long.parseLong(opponent_completed_goal);


                                    String opponent_name = jsonObject4.getString("opponent_name");
                                    participent_oponent_name.setText(opponent_name);
                                    String opponent_rank = jsonObject4.getString("opponent_rank");
                                    challenger_two_rank.setText(opponent_rank);

                                    ///////////////////////////////////////////////Day WISE STATUS DOWN/////////////////////////



                                    JSONArray jsonopponent = jsonObject1.getJSONArray("opponent_daywise_status");
                                    Log.d("KDKDKDKDK",jsonopponent.toString());
                                    for (int i = 0; i < jsonopponent.length(); i++)
                                    {
                                        JSONObject jdataobj = jsonopponent.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        opponentdayDateWiseStatusModel.add(allmember);
                                        Log.d("dd",opponentdayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_down.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(MinuiteChallengeStatusActivity.this);
                                    recyclerview_days_status_down.setLayoutManager(new LinearLayoutManager(MinuiteChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    opponentDayDateWiseChallengeStatusAdapter = new OpponentDayDateWiseChallengeStatusAdapter(opponentdayDateWiseStatusModel, MinuiteChallengeStatusActivity.this, R.layout.row_daywise_challegne,evaluation_factor);
                                    recyclerview_days_status_down.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_down.setAdapter(opponentDayDateWiseChallengeStatusAdapter);


////////////////////////////////////////END/////////////////////////////////////////////////////



                                    String opponent_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_image");
                                    String opponent_group_name = jsonObject4.getString("opponent_group_name");
                                    String opponent_group_rank = jsonObject4.getString("opponent_group_rank");
                                    String opponent_group_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_group_image");
                                    String opponent_super_admin_name = jsonObject4.getString("opponent_super_admin_name");
                                    String opponent_super_admin_rank = jsonObject4.getString("opponent_super_admin_rank");
                                    String opponent_super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_super_admin_image");

                                    if(opponent_usd_type.equals("USER"))
                                    {
                                        participent_oponent_name.setText(opponent_name);
                                        challenger_two_rank.setText(opponent_rank);
                                        participent_oponent_type.setText(opponent_usd_type);
                                      //  challenger_two_steps.setText(opponent_completed_goal);
                                        challenger_two_steps.setText(String.format("%d M, %d s",
                                                TimeUnit.MILLISECONDS.toMinutes(opponent_goal),
                                                TimeUnit.MILLISECONDS.toSeconds(opponent_goal) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(opponent_goal))
                                        ));
                                        Picasso.with(MinuiteChallengeStatusActivity.this)
                                                .load(opponent_image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_NAME,opponent_name);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_RANK,opponent_rank);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_TYPE,opponent_usd_type);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_COMPLETED_GOAL,opponent_goal);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_IMAGE,opponent_image);
                                    }
                                    else
                                    {
                                        participent_oponent_name.setText(opponent_group_name);
                                        challenger_two_rank.setText(opponent_group_rank);
                                        participent_oponent_type.setText(opponent_usd_type);
                                       // challenger_two_steps.setText(opponent_completed_goal);
                                        challenger_two_steps.setText(String.format("%d M, %d s",
                                                TimeUnit.MILLISECONDS.toMinutes(opponent_goal),
                                                TimeUnit.MILLISECONDS.toSeconds(opponent_goal) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(opponent_goal))
                                        ));
                                        Picasso.with(MinuiteChallengeStatusActivity.this)
                                                .load(opponent_group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_two_image);

                                        values.put(StatusMinuiteChallengeDB.OPPONENT_NAME,opponent_group_name);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_RANK,opponent_group_rank);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_TYPE,opponent_usd_type);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_COMPLETED_GOAL,opponent_goal);
                                        values.put(StatusMinuiteChallengeDB.OPPONENT_IMAGE,opponent_group_image);
                                    }

                                    boolean isExit=statusMinuiteChallengeDB.CheckIsDataAlreadyInDBorNot(challenge_id);
                                    if(!isExit){
                                        statusMinuiteChallengeDB.addMinuites(values);
                                    }

                                   /* Picasso.with(MinuiteChallengeStatusActivity.this)
                                            .load(opponent_image)
                                            .placeholder(R.drawable.dummy_user32)
                                            .into(challenger_two_image);*/
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(MinuiteChallengeStatusActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(MinuiteChallengeStatusActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MinuiteChallengeStatusActivity.this);
            requestQueue.add(strRe);
        } else {
            progressDialog.cancel();
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            statusMinuitesData();

        }
    }


    private void setupDetectorTimestampUpdaterThread() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

            }
        };

        detectorTimeStampUpdaterThread = new Thread() {
            @Override
            public void run() {
                while (isRunning) {
                    try {
                        Thread.sleep(5000);
                        handler.sendEmptyMessage(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        detectorTimeStampUpdaterThread.start();
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        msgflag=0;
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, ti, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                    MinuiteChallengeStatusActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    MinuiteChallengeStatusActivity.super.onBackPressed();
                }
            });
            builder.show();
        }

        if (v == memb_count_top)
        {
            Intent top=new Intent(MinuiteChallengeStatusActivity.this,MemberCountChllengeStatusActivity.class);
            top.putExtra("challenge_id",challenge_id);
            top.putExtra("group_id",group_id);
            startActivity(top);
        }
        if (v == mem_cccount_down)
        {
            Intent bottom=new Intent(MinuiteChallengeStatusActivity.this,MemberCountChllengeStatusActivity.class);
            bottom.putExtra("challenge_id",challenge_id);
            bottom.putExtra("group_id",group_id);
            startActivity(bottom);
        }
        if(v==serviceBtn){
            sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
            // sensoreditor.putString("challengeId",challenge_id);
            boolean isStarted=sensorPreference.getBoolean("isStarted",false);
            String challengeId=sensorPreference.getString("challengeId","");
            //if(challengeId!=null && challengeId.length()>0){
            if(!challengeId.equalsIgnoreCase(challenge_id) && isStarted){
                String name=sensorPreference.getString("activity","");
                showPreviousService(name);
            }else {
                sensoreditor.putString("challengeId",challenge_id);
                sensoreditor.putString("activity",activity_name.getText().toString());
                String text=serviceBtn.getText().toString();
                if(text.equalsIgnoreCase("STOP")){
                    serviceBtn.setText("START");
                    sensoreditor.putBoolean("isStarted",false);

                    sensoreditor.apply();
                    update.setVisibility(View.GONE);

                    serviceBtn.setBackgroundColor(Color.GREEN);
                      stopSensors();


                }else {
                    serviceBtn.setText("STOP");
                    sensoreditor.putBoolean("isStarted",true);

                    serviceBtn.setBackgroundColor(Color.RED);
                    mIsSensorUpdateEnabled =true;
                    initGpsListeners();
                    sManager.registerListener(eventListener,stepSensor,SensorManager.SENSOR_DELAY_FASTEST);
                }
                sensoreditor.apply();
            }

        }
        if(v==gps_track_img){
            Intent it=new Intent(MinuiteChallengeStatusActivity.this,GPSTrackActivity.class);
            it.putExtra("challengeId",challenge_id);
            it.putExtra("userid",user_id);
            startActivity(it);
        }
    }


    //show Alert with previous Challenge
    private void  showPreviousService(String activity){

        AlertDialog alertDialog = new AlertDialog.Builder(
                MinuiteChallengeStatusActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Challenge");

        // Setting Dialog Message
        alertDialog.setMessage("Already Running "+ Html.fromHtml("<font color='#026c9b'>"+activity+"</font>")+"Challenge\nPlease stop challenge before start it.");

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume() {

        super.onResume();

    }
    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
        detectorTimeStampUpdaterThread.interrupt();

    }
    public void sendChallenge(final String activity_id, final String challenge_id, final String user_id, final String evaluation_factor, final String evaluation_factor_unit, final String challenge_goal, final String android_id, final String brand, final String model) {
        checkInternet = NetworkChecking.isConnected(MinuiteChallengeStatusActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS;
            Log.d("fbnxbjkx", url);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("CHAL_RESPONSE", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    getMiniuiteChallengeStatus();
                                   if(msgflag==0)
                                   Toast.makeText(MinuiteChallengeStatusActivity.this, "Challenge Updating", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(MinuiteChallengeStatusActivity.this, "Challenge Updating Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(MinuiteChallengeStatusActivity.this, "Invalid ", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    if (user_type_for_challenge.equals("USER")) {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", "");
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec",String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    }else {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", group_id);
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec",String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    }
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MinuiteChallengeStatusActivity.this);
            requestQueue.add(stringRequest);

        } else {

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected) {

            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);


        }else{

            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);

        }
    }

    private void statusMinuitesData() {

        List<String> activityName = statusMinuiteChallengeDB.getActivityName();

        String challengeId = statusMinuiteChallengeDB.ChallengeIDstr(challenge_id);
        Log.v("ChallengeId", challengeId);


        List<String> challeneg_Ids = statusMinuiteChallengeDB.getChallengeID();


        List<String> user_type = statusMinuiteChallengeDB.getUserType();
        List<String> challenge_goal = statusMinuiteChallengeDB.getChallengeGoal();
        List<String> amount = statusMinuiteChallengeDB.getAmount();
        List<String> activity_name_string = statusMinuiteChallengeDB.getActivityName();

        List<String> user_name = statusMinuiteChallengeDB.getUserName();
        List<String> user_rank = statusMinuiteChallengeDB.getUserRank();
        List<String> image = statusMinuiteChallengeDB.getImage();
        List<String> oppentName = statusMinuiteChallengeDB.getopponentName();
        List<String> oppentrank = statusMinuiteChallengeDB.getopponentrank();

        List<String> oppentImage = statusMinuiteChallengeDB.getopponentImge();
        List<String> oppenttype = statusMinuiteChallengeDB.getopponentType();

        List<String> user_complet_goal = statusMinuiteChallengeDB.getUserCompletedGoal();

        List<String> opponentgoal = statusMinuiteChallengeDB.getopponentgoal();


        for (int i = 0; i < challeneg_Ids.size(); i++) {
            if (challeneg_Ids.get(i).equalsIgnoreCase(challengeId)) {
                activity_name.setText(activity_name_string.get(i));
                Log.v("User",activity_name_string.get(i));
                cash_price.setText(amount.get(i));
                activity_name.setText(activity_name_string.get(i));
                usd_type_in = user_type.get(i);
                goal.setText(challenge_goal.get(i));
                participent_user_name.setText(user_name.get(i));
                participent_user_type.setText(usd_type_in);

                steps_count = user_complet_goal.get(i);
                SharedPreferences preferences=getSharedPreferences("Minutes",MODE_PRIVATE);
                String challengegoal=preferences.getString("millis","");
                long milliseconds=0;
                if(challengegoal.length()>0)
                 milliseconds = Long.parseLong(challengegoal);

                // long minutes = (milliseconds / 1000) / 60;
               long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);
                totalSeconds=seconds;
                Log.v("Goal",">>"+totalSeconds);
               long user_goal = Long.parseLong(steps_count);
               ti=String.valueOf(user_goal);
               challenger_one_steps.setText(String.format("%d M, %d s",
                        TimeUnit.MILLISECONDS.toMinutes(user_goal),
                        TimeUnit.MILLISECONDS.toSeconds(user_goal) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(user_goal))
                ));
                challenger_one_rank.setText(user_rank.get(i));
                Picasso.with(MinuiteChallengeStatusActivity.this)
                        .load(image.get(i))
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(challenger_one_image);


                String opponenttype=oppenttype.get(i);
                if (opponenttype.equals("ADMIN")) {
                    ll_down.setVisibility(View.GONE);
                    vsTxt.setVisibility(View.GONE);
                } else {
                    ll_down.setVisibility(View.VISIBLE);
                    vsTxt.setVisibility(View.VISIBLE);
                    participent_oponent_name.setText(oppentName.get(i));
                    if(opponentType.contains("Group") || opponentType.contains("GROUP"))
                        participent_oponent_type.setText("GROUP");
                    else
                        participent_oponent_type.setText("USER");
                    challenger_two_rank.setText(oppentrank.get(i));
                    String steps=opponentgoal.get(i);
                    long opponent_goal = Long.parseLong(steps);
                    challenger_two_steps.setText(String.format("%d M, %d s",
                            TimeUnit.MILLISECONDS.toMinutes(opponent_goal),
                            TimeUnit.MILLISECONDS.toSeconds(opponent_goal) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(opponent_goal))
                    ));
                    Picasso.with(MinuiteChallengeStatusActivity.this)
                            .load(oppentImage.get(i))
                            .placeholder(R.drawable.dummy_user_profile)
                            .into(challenger_two_image);

                }

            }
        }

    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {

                    sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, ti, android_id, Build.BRAND, Build.DEVICE);
                } catch (Exception e) {

                }
                MinuiteChallengeStatusActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                MinuiteChallengeStatusActivity.super.onBackPressed();
            }
        });
        builder.show();
    }
    public void updateThread(){
        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, ti, android_id, Build.BRAND, Build.DEVICE);
    }
}
