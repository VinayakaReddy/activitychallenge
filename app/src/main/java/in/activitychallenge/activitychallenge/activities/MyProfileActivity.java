package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.dbhelper.UserProfileDB;
import in.activitychallenge.activitychallenge.holder.UserTopFiveActivitiesHolder;
import in.activitychallenge.activitychallenge.models.UserTopFiveActivitiesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ImagePermissions;
import in.activitychallenge.activitychallenge.utilities.MyMultipartEntity;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close, userPic, verf_mob, verf_email, updatePhoto, shareButt;
    ToggleButton edit_pro;
    Bitmap bitmap;
    EditText fName, lName, edit_about_me,edit_transction_mail_id;
    TextView abtT, gendT, photoT, dobT, promote_txt, editDob, wAmount, userTyp, email, status, refCod, creatOntxt, lastLogintxt, countryCod, mobileNum,
             gender, dob, rank, sponsercnt, followerss, following,send_money,add_wallet_money,paypal_id_text;
    RadioGroup gend_rad_group;
    RadioButton gend_male, gend_femal, gend_other, radioButton;
    Typeface typeface, typeface_bold;
    LinearLayout llPhoto, ll_email_verify,ll_sp,lk;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String s;
    String user_id, access_token, device_id, user_type, radioValue, aboutEditS,transaction_EditS, refUserIdJ, refCodeJ;
    String id, user_typeJ, first_nameJ, last_nameJ, dobJ, genderJ, aboutJ,transaction_mailJ, emailJ, country_codeJ, mobileJ, wallet_amtJ, dob_date,
            statusJ, profile_picJ, refer_codeJ, created_on_txt, created_on, last_login_on_txt, last_login_on;
    int is_email_verifiedJ;
    int is_mobile_verifiedJ;
    int total_referencesJ;
    String overall_rankJ;
    Double rang_amount=25.00;
    Double add_wal_amount=0.00;
    Dialog dialog,dialog2;
    UserProfileDB userProfileDB;
  /*  int rank_total_invitesJ;
    int rank_getting_sponsorJ;
    int rank_sponsor_othersJ;
    int rank_apply_challengesJ;
    int rank_win_challengesJ;
    int rank_lost_challengesJ;
    int total_lottery_appliedJ;
    int total_scratch_card_rewardJ;*/
    String followers_cntJ;
    String following_users_cntJ;
    int following_challenges_cntJ;
    int blocked_users_cntJ;
    int blocked_me_cntJ;
    int following_groups_cntJ;
    int joint_groups_cntJ;
    String sponsor_by_cntJ;
    boolean payment_infoJ;
    public int mYear, mMonth, mDay;
    RecyclerView top_fivAct_recyc;
    LinearLayoutManager llManager;
    getTopFiveActivitiesAdapter tFAdap;
    ArrayList<UserTopFiveActivitiesModel> tFList = new ArrayList<UserTopFiveActivitiesModel>();
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "";
    String amount, time_span, from_on, to_on, file;
    private SimpleDateFormat dateFormatter;
    ContentValues values ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.dummy_activity_my_profile);

        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("asdfasdf",user_type);
        values = new ContentValues();
        userProfileDB=new UserProfileDB(MyProfileActivity.this);   //db object
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        top_fivAct_recyc = (RecyclerView) findViewById(R.id.top_fivAct_recyc);
        tFAdap = new getTopFiveActivitiesAdapter(MyProfileActivity.this, tFList, R.layout.row_top_five_activities);
        llManager = new LinearLayoutManager(this);
        top_fivAct_recyc.setNestedScrollingEnabled(false);
        top_fivAct_recyc.setLayoutManager(new LinearLayoutManager(MyProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        top_fivAct_recyc = (RecyclerView) findViewById(R.id.top_fivAct_recyc);
        ll_email_verify = (LinearLayout) findViewById(R.id.ll_email_verify);
        ll_email_verify.setOnClickListener(this);
        llPhoto = (LinearLayout) findViewById(R.id.llPhoto);
        ll_sp = (LinearLayout) findViewById(R.id.ll_sp);
        lk = (LinearLayout) findViewById(R.id.lk);
        edit_pro = (ToggleButton) findViewById(R.id.edit_pro);
        edit_pro.setOnClickListener(this);
        updatePhoto = (ImageView) findViewById(R.id.updatePhoto);
        updatePhoto.setOnClickListener(this);
        userPic = (ImageView) findViewById(R.id.userPic);
        verf_mob = (ImageView) findViewById(R.id.verf_mob);
        verf_email = (ImageView) findViewById(R.id.verf_email);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        gend_rad_group = (RadioGroup) findViewById(R.id.gend_rad_group);
        gend_male = (RadioButton) findViewById(R.id.gend_male);
        gend_femal = (RadioButton) findViewById(R.id.gend_femal);
        gend_other = (RadioButton) findViewById(R.id.gend_other);
        shareButt = (ImageView) findViewById(R.id.shareButt);
        shareButt.setOnClickListener(this);
        abtT = (TextView) findViewById(R.id.abtT);
        paypal_id_text = (TextView) findViewById(R.id.paypal_id_text);
        dobT = (TextView) findViewById(R.id.dobT);
        gendT = (TextView) findViewById(R.id.gendT);
        photoT = (TextView) findViewById(R.id.photoT);
        promote_txt = (TextView) findViewById(R.id.promote_txt);
        promote_txt.setOnClickListener(this);
        editDob = (TextView) findViewById(R.id.editDob);
        editDob.setOnClickListener(this);

        sponsercnt = (TextView) findViewById(R.id.sponsercnt);
        followerss = (TextView) findViewById(R.id.followerss);
        following = (TextView) findViewById(R.id.following);
        rank = (TextView) findViewById(R.id.rank);
        wAmount = (TextView) findViewById(R.id.wAmount);
        userTyp = (TextView) findViewById(R.id.userTyp);
        email = (TextView) findViewById(R.id.email);
        status = (TextView) findViewById(R.id.status);
        refCod = (TextView) findViewById(R.id.refCod);
        creatOntxt = (TextView) findViewById(R.id.creatOntxt);
        lastLogintxt = (TextView) findViewById(R.id.lastLogintxt);
        gender = (TextView) findViewById(R.id.gender);
        dob = (TextView) findViewById(R.id.dob);
        fName = (EditText) findViewById(R.id.fName);
        fName.setFocusable(false);
        lName = (EditText) findViewById(R.id.lName);
        lName.setFocusable(false);
        edit_about_me = (EditText) findViewById(R.id.edit_about_me);
        edit_about_me.setFocusable(false);
        edit_transction_mail_id = (EditText) findViewById(R.id.edit_transction_mail_id);
        edit_transction_mail_id.setFocusable(false);

        countryCod = (TextView) findViewById(R.id.countryCod);
        mobileNum = (TextView) findViewById(R.id.mobileNum);
        send_money = (TextView) findViewById(R.id.send_money);
        send_money.setOnClickListener(this);
        add_wallet_money = (TextView) findViewById(R.id.add_wallet_money);
        add_wallet_money.setOnClickListener(this);



        if(user_type.equals("SPONSOR"))
        {
           // promote_txt.setVisibility(View.GONE);
            ll_sp.setVisibility(View.GONE);
            lk.setVisibility(View.GONE);
        }
        else
            {

            }

        getProfile();
        getTopFiveActivities();
    }

    private void getTopFiveActivities() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("TopFiveURL", AppUrls.BASE_URL + AppUrls.TOP_FIVE_ACTIVITIES + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.TOP_FIVE_ACTIVITIES + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("TopFiveRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                 //   Toast.makeText(MyProfileActivity.this, "TOP Five Data Success", Toast.LENGTH_SHORT).show();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        UserTopFiveActivitiesModel utm = new UserTopFiveActivitiesModel();
                                        JSONObject itemArrayy = jArr.getJSONObject(i);
                                        utm.setActivity_id(itemArrayy.getString("activity_id"));
                                        utm.setTitle(itemArrayy.getString("title"));
                                        utm.setActivity_image(AppUrls.BASE_IMAGE_URL + itemArrayy.getString("activity_image"));
                                        tFList.add(utm);
                                    }
                                    top_fivAct_recyc.setAdapter(tFAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MyProfileActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            pprogressDialog.cancel();
            //Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public class getTopFiveActivitiesAdapter extends RecyclerView.Adapter<UserTopFiveActivitiesHolder> {
        public ArrayList<UserTopFiveActivitiesModel> listModelsTF;
        MyProfileActivity context;
        LayoutInflater li;
        int resource;

        public getTopFiveActivitiesAdapter(MyProfileActivity context, ArrayList<UserTopFiveActivitiesModel> listModelsTF, int resource) {
            this.context = context;
            this.listModelsTF = listModelsTF;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public UserTopFiveActivitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutReturn = li.inflate(resource, null);
            UserTopFiveActivitiesHolder msh = new UserTopFiveActivitiesHolder(layoutReturn);
            return msh;
        }


        @Override
        public void onBindViewHolder(UserTopFiveActivitiesHolder holder, int position) {
            Picasso.with(MyProfileActivity.this)
                    .load(listModelsTF.get(position).getActivity_image())
                    .into(holder.act_img);
            Log.d("DDffrree", listModelsTF.get(position).getActivity_image().toString());
        }

        @Override
        public int getItemCount() {
            return listModelsTF.size();
        }
    }

    private void getProfile() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
           // userProfileDB.emptyDB();
            Log.d("GETproFFFILEYURL", AppUrls.BASE_URL + "user/profile?user_id=" + user_id + "&user_type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "user/profile?user_id=" + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>()
                    {

                        @Override
                        public void onResponse(String response)
                        {
                            pprogressDialog.dismiss();
                            Log.d("GETproFiRRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONArray jArr = jsonObject.getJSONArray("data");

                                    JSONObject itemArray = jArr.getJSONObject(0);

                                    id = itemArray.getString("id");
                                    user_typeJ = itemArray.getString("user_type");
                                    userTyp.setText(user_typeJ);
                                    values.put(UserProfileDB.USER_TYPE,user_typeJ);
                                    first_nameJ = itemArray.getString("first_name");
                                    values.put(UserProfileDB.FNAME,first_nameJ);
                                    last_nameJ = itemArray.getString("last_name");
                                    values.put(UserProfileDB.LNAME,last_nameJ);
                                    fName.setText(first_nameJ);
                                    lName.setText(last_nameJ);
                                    dob_date = itemArray.getString("dob");
                                    s = dob_date.substring(0, 10);
                                    values.put(UserProfileDB.DOB,s);
                                    dob.setText(s);
                                    genderJ = itemArray.getString("gender");
                                    values.put(UserProfileDB.GENDER,genderJ);
                                    gender.setText(genderJ);
                                    String profile_pic = AppUrls.BASE_IMAGE_URL + itemArray.getString("profile_pic");
                                    selectedImagePath = profile_pic;
                                    values.put(UserProfileDB.PROFILE_IMAGE,profile_pic);
                                    Log.d("fhgzfbhz", profile_pic);
                                    Picasso.with(MyProfileActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.dummy_user_profile) // img_circle_placeholder
                                            // .resize(60,60)
                                            .into(userPic);
                                    emailJ = itemArray.getString("email");
                                    values.put(UserProfileDB.EMAIL,emailJ);
                                    email.setText(emailJ);
                                    is_email_verifiedJ = itemArray.getInt("is_email_verified");
                                    if (is_email_verifiedJ == 1) {
                                        verf_email.setImageResource(R.drawable.ic_verified);
                                    } else if (is_email_verifiedJ == 0) {
                                        verf_email.setImageResource(R.drawable.ic_un_verified);
                                    }
                                    country_codeJ = itemArray.getString("country_code");
                                    values.put(UserProfileDB.COUNTRYCODE,country_codeJ);
                                    countryCod.setText(country_codeJ);
                                    mobileJ = itemArray.getString("mobile");
                                    values.put(UserProfileDB.MOBILE,mobileJ);
                                    mobileNum.setText(mobileJ);
                                    is_mobile_verifiedJ = itemArray.getInt("is_mobile_verified");
                                    if (is_mobile_verifiedJ == 1) {
                                        verf_mob.setImageResource(R.drawable.ic_verified);
                                    } else if (is_mobile_verifiedJ == 0) {
                                        verf_mob.setImageResource(R.drawable.ic_un_verified);
                                    }
                                    statusJ = itemArray.getString("status");
                                    values.put(UserProfileDB.USER_STATUS,statusJ);
                                    status.setText(statusJ);
                                    refer_codeJ = itemArray.getString("refer_code");
                                    values.put(UserProfileDB.REFERCODE,refer_codeJ);
                                    refCod.setText(refer_codeJ);

                                  //  JSONObject aboutObj = itemArray.getJSONObject("about_me");
                                    aboutJ = itemArray.getString("about_me");
                                    values.put(UserProfileDB.ABOUT_ME,aboutJ);
                                    edit_about_me.setText(aboutJ);

                                    transaction_mailJ = itemArray.getString("paypal_registered_email");
                                    values.put(UserProfileDB.PAYPALID,transaction_mailJ);
                                    edit_transction_mail_id.setText(transaction_mailJ);

                                    wallet_amtJ = itemArray.getString("wallet_amt");
                                    values.put(UserProfileDB.WALLET_AMOUNT,wallet_amtJ);
                                    wAmount.setText(""+ wallet_amtJ + " " + "USD");

                                    //condition for hide and show send money button
                                    Double valu=Double.parseDouble(wallet_amtJ);
                                    if(valu >= rang_amount )
                                    {
                                        send_money.setVisibility(View.VISIBLE);
                                    }
                                    else
                                        {
                                          send_money.setVisibility(View.GONE);
                                        }

                                    if(add_wal_amount== valu)
                                    {
                                        add_wallet_money.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        add_wallet_money.setVisibility(View.VISIBLE);
                                    }

                                    payment_infoJ = itemArray.getBoolean("payment_info");

                                    created_on_txt = itemArray.getString("created_on_txt");

                                    last_login_on_txt = itemArray.getString("last_login_on_txt");
                                    values.put(UserProfileDB.LAST_LOGIN,last_login_on_txt);
                                  //  Log.d("DDDDATAA",created_on+"///"+last_login_on_txt);

                                    created_on = itemArray.getString("created_on");
                                    String createdtimedate=parseDateToddMMyyyy(created_on);
                                    values.put(UserProfileDB.ACCOUNT_CREATED_ON,createdtimedate);
                                    creatOntxt.setText(createdtimedate);
                                    lastLogintxt.setText(last_login_on_txt);

                                    last_login_on = itemArray.getString("last_login_on");

                                    overall_rankJ = itemArray.getString("overall_rank");
                                    values.put(UserProfileDB.RANK,overall_rankJ);
                                    rank.setText(String.valueOf(overall_rankJ));
                                    followers_cntJ = itemArray.getString("followers_cnt");
                                    values.put(UserProfileDB.FOLLOWERS,followers_cntJ);
                                    followerss.setText(String.valueOf(followers_cntJ));
                                    following_users_cntJ = itemArray.getString("following_users_cnt");
                                    values.put(UserProfileDB.FOLLOWINGS,following_users_cntJ);
                                    following.setText(String.valueOf(following_users_cntJ));
                                    sponsor_by_cntJ = itemArray.getString("sponsor_by_cnt");
                                    values.put(UserProfileDB.SPONSORBY,sponsor_by_cntJ);
                                    sponsercnt.setText(String.valueOf(sponsor_by_cntJ));
                                    Log.d("DDFFccc", genderJ + "nnn" + dobJ);
                                    Log.d("GETPROFFsssrs", id + "" + user_typeJ + "" + first_nameJ + "" + last_nameJ + "" + emailJ + "" + is_email_verifiedJ + "" + country_codeJ + "" + mobileJ + "" +
                                            is_mobile_verifiedJ + "" + statusJ + "" + profile_picJ + "" + refer_codeJ + "" + wallet_amtJ + "" + payment_infoJ + "" + created_on_txt + "" + created_on + "" + last_login_on_txt + "" + last_login_on);
                                    Log.d("DBVALUES",""+values);
                                    userProfileDB.addUserDetail(values);

                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MyProfileActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MyProfileActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("PROFILEGETADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else
            {
            pprogressDialog.cancel();
           // Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            userProfileDBData();
            }
    }

    private void userProfileDBData()
    {
       // Toast.makeText(MyProfileActivity.this,"DBDBDBDB",Toast.LENGTH_LONG).show();
        Log.d("DBDATA","DBDATA");
        List<String> f_Name = userProfileDB.getUserFName();
        Log.d("DATAAAAA",""+f_Name);
        if(f_Name.size() > 0)
        {

            List<String> user_f_name = userProfileDB.getUserFName();
            List<String> l_name = userProfileDB.getUserLName();
            List<String> profile_image = userProfileDB.getProfileImage();
            List<String> user_rank = userProfileDB.getRank();
            List<String> followers = userProfileDB.getFollower();
            List<String> followings = userProfileDB.getFollowing();
            List<String> sponsered_by = userProfileDB.getSponsorBy();
            List<String> wallet_amount = userProfileDB.getWalletAmt();
            List<String> refer_code = userProfileDB.getReferCode();
            List<String> about_me = userProfileDB.getAboutMe();
            List<String> paypal_id = userProfileDB.getPapalId();
            List<String> user_dob = userProfileDB.getDOB();
            List<String> user_gender = userProfileDB.getGender();
            List<String> country_code = userProfileDB.getCountryCode();
            List<String> mobile = userProfileDB.getMobile();
            List<String> user_email = userProfileDB.getEmail();
            List<String> user_type = userProfileDB.getUserType();
            List<String> user_status = userProfileDB.getStatus();
            List<String> acc_created_on = userProfileDB.getCreatedOn();
            List<String> last_login = userProfileDB.getLastLogin();
            for (int i = 0; i < f_Name.size(); i++)
            {
                fName.setText(user_f_name.get(0));
                lName.setText(l_name.get(0));
                userTyp.setText(user_type.get(0));
                sponsercnt.setText(sponsered_by.get(0));
                following.setText(followings.get(0));
                followerss.setText(followers.get(0));
                rank.setText(user_rank.get(0));
                creatOntxt.setText(acc_created_on.get(0));
               // lastLogintxt.setText(last_login.get(0));
                wAmount.setText(wallet_amount.get(0));
                edit_transction_mail_id.setText(paypal_id.get(0));
                edit_about_me.setText(about_me.get(0));
                refCod.setText(refer_code.get(0));
                status.setText(user_status.get(0));
                mobileNum.setText(mobile.get(0));
                countryCod.setText(country_code.get(0));
                email.setText(user_email.get(0));
                Picasso.with(MyProfileActivity.this)
                        .load(profile_image.get(0))
                        .placeholder(R.drawable.dummy_user_profile)  //img_circle_placeholder
                        // .resize(60,60)
                        .into(userPic);
                gender.setText(user_gender.get(0));
                dob.setText(user_dob.get(0));




            }
          //  setGender();

        }
        else
        {

        }
    }


    @Override
    public void onClick(View view) {
        if (view == shareButt) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Refer Code:" + refer_codeJ + "\n" + "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

        if (view == ll_email_verify) {
            if (is_email_verifiedJ == 1) {
                Toast.makeText(MyProfileActivity.this, "Email Already Verified", Toast.LENGTH_LONG).show();
            } else {
                sendEmailVerify();
            }
        }

        if (view == close) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
        if (view == add_wallet_money)
        {
            dialog2 = new Dialog(MyProfileActivity.this);
            dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog2.setContentView(R.layout.custom_dialog_add_wallet_money);
            final EditText edt_sendwallet_amount = (EditText) dialog2.findViewById(R.id.edt_send_amount);
            final Button addWalletMoneyButton = (Button) dialog2.findViewById(R.id.addWalletMoneyButton);

            addWalletMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_sendwallet_amount.getText().toString();

                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_sendwallet_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                        Intent intent = new Intent(MyProfileActivity.this, ChallengePaymentActivity.class);
                        intent.putExtra("activity", "MYPROFILE_ADDWALLET");

                        intent.putExtra("myprofile_addWallet", send_amount);
                        Log.d("WALLETAMT",  send_amount);
                        startActivity(intent);
                        //sendAddWalletAmount(send_amount);

                    }
                }
            });

            dialog2.show();
        }
        if (view == send_money)
        {

            dialog = new Dialog(MyProfileActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_send_money);
            final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
            final Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);

            sendMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();

                     if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                     {
                         edt_send_amount.setError("Please Enter Amount..!");
                     }
                    else
                      {
                          sendWalletAmount(send_amount);
                          if(Double.parseDouble(send_amount) >= Double.parseDouble(wallet_amtJ) && Double.parseDouble(send_amount) <= rang_amount )
                          {
                              Toast.makeText(MyProfileActivity.this,"Amount should be Less than Wallet Amount...! ",Toast.LENGTH_LONG).show();
                          }
                          else
                          {
                              sendWalletAmount(send_amount);
                          }
                     }
                }
            });

            dialog.show();
        }

        if (view == edit_pro) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                if (edit_pro.isChecked()) {
                    send_money.setVisibility(View.GONE);
                    edit_about_me.setFocusable(true);
                    edit_about_me.setFocusableInTouchMode(true);
                    edit_transction_mail_id.setFocusable(true);
                    edit_transction_mail_id.setFocusableInTouchMode(true);

                    fName.setFocusable(true);
                    fName.setFocusableInTouchMode(true);
                    lName.setFocusable(true);
                    lName.setFocusableInTouchMode(true);

                    editDob.setVisibility(View.VISIBLE);
                    gend_rad_group.setVisibility(View.VISIBLE);
                    gender.setVisibility(View.GONE);
                    llPhoto.setVisibility(View.VISIBLE);
                    promote_txt.setVisibility(View.GONE);
                    abtT.setTextColor(getResources().getColor(R.color.auth_text_color));
                    paypal_id_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                    gendT.setTextColor(getResources().getColor(R.color.auth_text_color));
                    photoT.setTextColor(getResources().getColor(R.color.auth_text_color));
                    dobT.setTextColor(getResources().getColor(R.color.auth_text_color));

                    setGender();
                } else if (!edit_pro.isChecked()) {
                    int selectedId = gend_rad_group.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(selectedId);
                    radioValue = radioButton.getText().toString();
                    gender.setVisibility(View.VISIBLE);
                    gender.setText(radioValue);
                    editDob.setVisibility(View.GONE);
                    gend_rad_group.setVisibility(View.GONE);
                    llPhoto.setVisibility(View.GONE);
                    promote_txt.setVisibility(View.VISIBLE);
                    aboutEditS = edit_about_me.getText().toString();
                    transaction_EditS = edit_transction_mail_id.getText().toString();
                    Log.d("GEndsd", radioValue);
                    abtT.setTextColor(getResources().getColor(R.color.pof));
                    paypal_id_text.setTextColor(getResources().getColor(R.color.pof));
                    gendT.setTextColor(getResources().getColor(R.color.pof));
                    photoT.setTextColor(getResources().getColor(R.color.pof));
                    dobT.setTextColor(getResources().getColor(R.color.pof));

                    updateProfile();
                }
            }else {
               // Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == updatePhoto) {
            selectOption();
        }
        if (view == editDob) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {                              //strDay + "-" + strMonth + "-" + year
                        strDay = dayOfMonth + "";
                    }
                    dob.setText(String.valueOf(year + "-" + strMonth + "-" + strDay));
                    dob.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (view == promote_txt) {
            Log.d("PROMOTIONURL", AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + user_type + "&entity_id=" + user_id);
            checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
            if (checkInternet) {
                StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTE_STATUS + user_type + "&entity_id=" + user_id,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                pprogressDialog.dismiss();
                                Log.d("PROMOTIONRESP", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String success = jsonObject.getString("success");
                                    String message = jsonObject.getString("message");
                                    String response_code = jsonObject.getString("response_code");
                                    if (response_code.equals("10100")) {
                                        pprogressDialog.dismiss();
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String id = jsonObject1.getString("id");
                                        String promotion_id = jsonObject1.getString("promotion_id");
                                        String entity_id = jsonObject1.getString("entity_id");
                                        String entity_type = jsonObject1.getString("entity_type");
                                        amount = jsonObject1.getString("amount");
                                        time_span = jsonObject1.getString("time_span");
                                        String status = jsonObject1.getString("status");
                                        String file = AppUrls.BASE_IMAGE_URL + jsonObject1.getString("banner_path");
                                        from_on = jsonObject1.getString("from_on");
                                        to_on = jsonObject1.getString("to_on");
                                        String last_apply_on_txt = jsonObject1.getString("last_apply_on_txt");
                                        String created_on = jsonObject1.getString("created_on");
                                        String week_completed = jsonObject1.getString("week_completed");
                                        int count = Integer.parseInt(week_completed);
                                        Log.d("COUNT", String.valueOf(count));
                                        if (count > 6) {
                                          //  Toast.makeText(MyProfileActivity.this, "Redirecting to Next Page..!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(MyProfileActivity.this, AddToPromoteActivity.class);
                                            intent.putExtra("user_type","USER");
                                            intent.putExtra("user_id",user_id);
                                            intent.putExtra("action_type","SELFPROMOTE");
                                            startActivity(intent);
                                            startActivity(intent);
                                        } else {
                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyProfileActivity.this);
                                            alertDialog.setTitle("You are not eligible to promote..!");
                                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            alertDialog.show();
                                        }


                                        /*String pastDate = to_on.substring(0,10);

                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                        try {
                                            Date date = format.parse(pastDate);
                                            Log.d("DATEEEEEE", String.valueOf(date));

                                            Calendar past = Calendar.getInstance();
                                            Calendar future = Calendar.getInstance();
                                            try {
                                                past.setTime(format.parse(pastDate));
                                                future.add(past.DATE, 48);

                                                String one  = format.format(past.getTime());
                                                String two  = format.format(future.getTime());
                                                String three  = format.format(Calendar.getInstance().getTime());

                                                Log.d("OOOOO",one+"\n"+two+"\n"+three);
                                                Log.d("FFFFFFF", String.valueOf(past.DATE));
                                                Log.d("CHECKSTRING",pastDate+"\n"+future.toString());
                                                Log.d("CHEKKKKKKK",past+"\n"+future);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            if (Calendar.getInstance().after(future)){
                                                Toast.makeText(MyProfileActivity.this, "Redirecting to Next Page..!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(MyProfileActivity.this,AddToPromoteActivity.class);
                                                startActivity(intent);

                                            }else {
                                                Toast.makeText(MyProfileActivity.this, "You are not eligible to promote..!", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        }*/

                                    }
                                    if (response_code.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(MyProfileActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                    }if (response_code.equals("10300")) {

                                        Intent intent = new Intent(MyProfileActivity.this, AddToPromoteActivity.class);
                                        intent.putExtra("user_type","USER");
                                        intent.putExtra("user_id",user_id);
                                        startActivity(intent);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token", access_token);
                        headers.put("x-device-id", device_id);
                        headers.put("x-device-platform", "ANDROID");

                        Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                        return headers;
                    }
                };
                strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
                requestQueue.add(strRe);
            } else {
                pprogressDialog.cancel();
                //Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    //ADD MONEY TO WALLET
  /*  private void sendAddWalletAmount(final String amount)
    {
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet)
        {

        }
        else
        {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }*/

    // //SEND MONEY TO PAYPAL ACCOUNT
    private void sendWalletAmount(final String amount)
    {
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);

        if (checkInternet)
        {
            dialog.cancel();
            String send_money_url = AppUrls.BASE_URL + AppUrls.TRANSFER_AMOUNT_TO_PAYPAL_ACC;


            Log.d("SENDMONEYLURL", send_money_url);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, send_money_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("RESPSENDMONEY", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(MyProfileActivity.this, "Payment transfered successfully..", Toast.LENGTH_LONG).show();

                            Intent redirect = new Intent(MyProfileActivity.this, MyProfileActivity.class);
                            startActivity(redirect);
                            finish();
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "User not exist.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Paypal Registered Email not Found..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10500")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Insufficient amount.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10600")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Payment transfer failed.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    param.put("amount", amount);

                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            //Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void uploadImages() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String urllllll = AppUrls.BASE_URL + AppUrls.CREATE_GROUP;
            Log.d("GROUPCREATIONURL:", urllllll);
            Log.d("PATHHHHH", selectedImagePath);
            MyProfileActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    //  if (progressDialog != null)
                    //   progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            new MyProfileActivity.HttpUpload(this, selectedImagePath).execute();
        } else {
            pprogressDialog.cancel();
           // Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateProfile()
    {

        checkInternet = NetworkChecking.isConnected(this);

         if(validate())
         {
             Log.d("2222", "2222222");
             if (checkInternet)
             {
                 Log.d("UPDATE_PRO_URL", AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE);
                 StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE,
                         new Response.Listener<String>() {
                             @Override
                             public void onResponse(String response) {
                                 pprogressDialog.dismiss();
                                 Log.d("UPDATE_PRO_RESP", response);
                                 try {
                                     JSONObject jsonObject = new JSONObject(response);
                                     String successResponceCode = jsonObject.getString("response_code");
                                     if (successResponceCode.equals("10100")) {
                                         pprogressDialog.dismiss();
                                         Toast.makeText(MyProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                         Intent intent = new Intent(MyProfileActivity.this, MyProfileActivity.class);
                                         startActivity(intent);
                                         finish();
                                     }
                                     if (successResponceCode.equals("10200")) {
                                         pprogressDialog.dismiss();
                                         Toast.makeText(MyProfileActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                     }
                                 } catch (JSONException e) {
                                     e.printStackTrace();
                                     pprogressDialog.cancel();
                                 }
                             }
                         },
                         new Response.ErrorListener() {
                             @Override
                             public void onErrorResponse(VolleyError error) {
                                 pprogressDialog.cancel();

                                 if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                 } else if (error instanceof AuthFailureError) {

                                 } else if (error instanceof ServerError) {

                                 } else if (error instanceof NetworkError) {

                                 } else if (error instanceof ParseError) {

                                 }
                             }
                         }) {

                     @Override
                     public Map<String, String> getHeaders() throws AuthFailureError {
                         Map<String, String> headers = new HashMap<>();
                         headers.put("x-access-token", access_token);
                         headers.put("x-device-id", device_id);
                         headers.put("x-device-platform", "ANDROID");
                         Log.d("UPDATEproHEADER", "HEADDER " + headers.toString());
                         return headers;
                     }

                     @Override
                     protected Map<String, String> getParams() throws AuthFailureError {
                         Map<String, String> params = new HashMap<String, String>();
                         params.put("user_id", user_id);
                         params.put("first_name", fName.getText().toString().trim());
                         params.put("last_name", lName.getText().toString().trim());
                         params.put("gender", radioValue);
                         params.put("dob", dob.getText().toString().trim());
                         params.put("about_me", aboutEditS);
                         params.put("paypal_registered_email", transaction_EditS);
                         Log.d("UPDATEproPARAM", params.toString());
                         return params;
                     }
                 };
                 stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                 RequestQueue requestQueue = Volley.newRequestQueue(this);
                 requestQueue.add(stringRequest);
             } else {
                 pprogressDialog.cancel();
                // Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
             }
         }
    }

/*
    private boolean validate()
    {

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        String MOBILE_REGEX = "^[789]\\d{9}$";


        boolean result = true;
        String userName = email_edt.getText().toString().trim();
        if (userName == null || userName.equals("")) {
            email_til.setError("Invalid Data");
            result = false;
        }
        else
            email_til.setErrorEnabled(false);

        String psw = pasword_edt.getText().toString().trim();

        if (psw == null || psw.equals("") ||psw.length()< 6)
        {
            password_til.setError("Invalid Password/ Min 6 Character");
            result = false;
        }
        else
            password_til.setErrorEnabled(false);

        return result;
    }*/

    private void setGender() {
        if(genderJ!=null && genderJ.length()>0){
            if (genderJ.equals("male") || genderJ.equals("Male") || genderJ.equals("MALE")) {
                gend_male.setChecked(true);
            } else if (genderJ.equals("female") || genderJ.equals("Female") || genderJ.equals("FEMALE")) {
                gend_femal.setChecked(true);
            } else if (genderJ.equals("others") || genderJ.equals("OTHERS") || genderJ.equals("Others")) {
                gend_other.setChecked(true);
            } else {
                Toast.makeText(this, "Gender Empty", Toast.LENGTH_SHORT).show();
            }
        }


    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void selectOption() {
        final Dialog dialog = new Dialog(MyProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.group_camera_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button mCamerabtn = (Button) dialog.findViewById(R.id.cameradialogbtn);
        Button mGallerybtn = (Button) dialog.findViewById(R.id.gallerydialogbtn);
        Button btnCancel = (Button) dialog.findViewById(R.id.canceldialogbtn);
        final boolean result = ImagePermissions.checkPermission(MyProfileActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        mCamerabtn.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                if (result)
                    if (ContextCompat.checkSelfPermission(MyProfileActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(MyProfileActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(MyProfileActivity.this,
                                android.Manifest.permission.CAMERA)

                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(MyProfileActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                // No explanation needed, we can request the permission.
                                ActivityCompat.requestPermissions(MyProfileActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);
                    }
                dialog.cancel();
            }

        });
        mGallerybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();

            }

        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel(); // dismissing the popup
            }

        });
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Log.d("PPP", outputFileUri.toString());
                    if (outputFileUri != null)
                    {
                        bitmap = decodeSampledBitmapFromUri(outputFileUri, userPic.getWidth(), userPic.getHeight());
                        if (bitmap == null)
                        {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();
                        } else {
                            selectedImagePath = getRealPathFromURI(MyProfileActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            userPic.setImageBitmap(bitmap);
                            uploadImages();
                        }
                    }
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, userPic.getWidth(), userPic.getHeight());
                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();
                    } else {
                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);
                        userPic.setImageBitmap(bitmap);
                        uploadImages();
                    }
                }
                break;
            default:
                break;
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }
        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        String filePath =getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // return filename;
       /* try {
// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }*/
        return bm;

       /* Bitmap bm = null;
        try {
// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return bm;*/
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MyProfileActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(MyProfileActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public static void saveToPreferences(Context context, String key, Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MyProfileActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(MyProfileActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    /*private String convertToBase64(String imagePath)
    {

        Bitmap bm = BitmapFactory.decodeFile(imagePath);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        return encodedImage;

    }
*/

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;
        private HttpClient client;
        private ProgressDialog pd;
        private long totalSize;
        public String url = AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE_PIC;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // File file = new File(imgPath);
                Bitmap bmp = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 15, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                Log.d("USERDETAIL", user_id + "//" + imgPath + "//" + file);
                //Create the POST object
                HttpPost post = new HttpPost(url);
                //Create the multipart entity object and add a progress listener
                //this is a our extended class so we can know the bytes that have been transfered
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                //Add the file to the content's body
                ContentBody cbFile = new InputStreamBody(in, "image/jpeg", "jpeg");
                entity.addPart("file", cbFile);
                entity.addPart("user_id", new StringBody(user_id));
                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();
                //We add the entity to the post request
                post.setEntity(entity);
                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();
                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }
            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog
            Toast.makeText(MyProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MyProfileActivity.this, MyProfileActivity.class);
            startActivity(intent);
        }
    }

    private void sendEmailVerify() {
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String emil_verify = AppUrls.BASE_URL + AppUrls.SEND_VERIFICATION_EMAIL;
            Log.d("EMVERYRPESP", emil_verify);
            StringRequest reqEmailVerify = new StringRequest(Request.Method.POST, emil_verify, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("EMVERYRPESPRPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100"))
                        {

                            Toast.makeText(MyProfileActivity.this, "Verification mail has been sent, Please check your mail", Toast.LENGTH_LONG).show();
                            //   verf_email.setImageResource(R.drawable.ic_verified);
                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(MyProfileActivity.this, "Email already Verified.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    Log.d("sendemailverfyParam:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(reqEmailVerify);
        } else {
           // Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validate() {
        boolean result = true;

        String EMAIL_REGEX ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";/*"/^(\\\".*\\\"|[A-Za-z]\\w*)@(\\[\\d{1,3}(\\.\\d{1,3}){3}]|[A-Za-z]\\w*(\\.[A-Za-z]\\w*)+)$/";*/


        String email = edit_transction_mail_id.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            edit_transction_mail_id.setError("Invalid Email");
            result = false;
        } else {
             edit_transction_mail_id.setError("Invalid Email");
        }


        return result;
    }


}
