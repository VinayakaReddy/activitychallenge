package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class VerifyAccountActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView verify_btn, country_flag;
    TextInputLayout mobile_til;
    EditText mobile_edt;
    TextView country_txt, vefiy__otptxt;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String country_code, device_id;
    UserSessionManager session;
    Typeface typeface, typeface2;
    private Boolean exit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_verify_account);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("KKKK",device_id);
        pprogressDialog = new ProgressDialog(VerifyAccountActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(VerifyAccountActivity.this);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(typeface);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        country_txt = (TextView) findViewById(R.id.country_txt);
        vefiy__otptxt = (TextView) findViewById(R.id.vefiy__otptxt);
        vefiy__otptxt.setTypeface(typeface2);
        country_txt.setOnClickListener(this);
        country_txt.setTypeface(typeface);
        country_flag = (ImageView) findViewById(R.id.country_flag);
        verify_btn = (ImageView) findViewById(R.id.verify_btn);
        verify_btn.setOnClickListener(this);
        country_txt.setText("+91");


    }

    @Override
    public void onClick(View view) {
        if (view == verify_btn) {
            if (validate())
            {
                checkInternet = NetworkChecking.isConnected(this);

                if (checkInternet)
                {
                    pprogressDialog.show();
                    Log.d("RegisterMobileURL", AppUrls.BASE_URL + AppUrls.GET_REGISTER_MOBILE);
                    final String code = country_txt.getText().toString().trim();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_REGISTER_MOBILE,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    pprogressDialog.dismiss();
                                    Log.d("RESPVERIFIEDWITHCODE", response);


                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("response_code");
                                        if (successResponceCode.equals("10100")) {

                                            pprogressDialog.dismiss();
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                           // session.createDeviceId(device_id);
                                            session.createUserId(device_id, code, mobile_edt.getText().toString());
                                            Intent i = new Intent(VerifyAccountActivity.this, OtpValidationActivity.class);
                                            i.putExtra("MOBILE", mobile_edt.getText().toString());
                                            i.putExtra("CC", code);
                                            i.putExtra("USERID", jsonObject1.getString("user_id"));
                                            startActivity(i);


                                        } else if (successResponceCode.equals("10200")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                        } else if (successResponceCode.equals("10300")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Mobile number already registered with us..!", Toast.LENGTH_SHORT).show();
                                            String jsonObjMsg = jsonObject.getString("message");
                                            Log.d("jsonObjMsg", jsonObjMsg.toString());

                                            if (jsonObjMsg.equals("User already exist")) {
                                                JSONObject jsonObjecdata = jsonObject.getJSONObject("data");
                                                String user_iid = jsonObjecdata.getString("user_id");
                                                String is_register = jsonObjecdata.getString("is_register");
                                                String is_mobile_verified = jsonObjecdata.getString("is_mobile_verified");

                                                if (is_mobile_verified.equals("0")) {
                                                    Intent i = new Intent(VerifyAccountActivity.this, OtpValidationActivity.class);
                                                    i.putExtra("MOBILE", mobile_edt.getText().toString());
                                                    i.putExtra("CC", code);
                                                    i.putExtra("USERID", jsonObjecdata.getString("user_id"));
                                                    startActivity(i);
                                                    finish();
                                                } else {
                                                   // session.createDeviceId(device_id);
                                                    session.createUserId(device_id, code, mobile_edt.getText().toString());
                                                    Intent regIntent = new Intent(VerifyAccountActivity.this, LoginActivity.class);
                                                    regIntent.putExtra("USERID",jsonObjecdata.getString("user_id"));
                                                    startActivity(regIntent);
                                                    finish();
                                                   /* if (is_register.equals("false"))
                                                    {
                                                        session.createUserId(user_iid, device_id, code, mobile_edt.getText().toString());
                                                        Log.d("jsonObjMsg", user_iid);
                                                        Intent regIntent = new Intent(VerifyAccountActivity.this, RegistrationActivity.class);
                                                        startActivity(regIntent);
                                                        finish();
                                                    } else {
                                                        session.createUserId(user_iid, device_id, code, mobile_edt.getText().toString());
                                                        Intent regIntent = new Intent(VerifyAccountActivity.this, LoginActivity.class);
                                                        startActivity(regIntent);
                                                        finish();
                                                    }*/
                                                }

                                            }


                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pprogressDialog.cancel();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    pprogressDialog.cancel();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("country_code", code);
                            params.put("mobile", mobile_edt.getText().toString());
                            params.put("device_id", device_id);
                            params.put("device_platform","ANDROID");
                            Log.d("VERIFIEDACCPPARAM:", params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(VerifyAccountActivity.this);
                    requestQueue.add(stringRequest);

                }
                else
                    {

                    Toast.makeText(VerifyAccountActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                }

            }
        }

        if (view == country_txt) {
            Intent countryIntent = new Intent(VerifyAccountActivity.this, CountryListActivity.class);
            startActivityForResult(countryIntent, 1);
        }


    }

    private boolean validate() {

        boolean result = true;
        int flag = 0;
        String MOBILE_REGEX = "^[789]\\d{9}$";

        String mobile = mobile_edt.getText().toString().trim();
        if ((mobile == null || mobile.equals("") || mobile.length() < 10)) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setErrorEnabled(false);
        }

        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                country_code = data.getStringExtra("result");

                country_txt.setText(country_code);

                String flagpath = data.getStringExtra("flag");

                Log.d("PATHHH", flagpath + "/" + country_code);

                Picasso.with(VerifyAccountActivity.this)
                        .load(flagpath)
                        .placeholder(R.drawable.no_image_found)
                        .into(country_flag);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onBackPressed() {
        //this.finishAffinity();

        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
