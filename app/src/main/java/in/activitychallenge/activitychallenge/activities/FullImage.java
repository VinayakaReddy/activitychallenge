package in.activitychallenge.activitychallenge.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import in.activitychallenge.activitychallenge.R;

public class FullImage extends AppCompatActivity implements View.OnClickListener
{
    ImageView holoimage,close;
    Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);
        holoimage=(ImageView)findViewById(R.id.fullimg);

        String imagePath = getIntent().getStringExtra("HOLOPATH");

        Picasso.with(FullImage.this)
                .load(imagePath)
                .placeholder(R.drawable.no_image_found)
                .into(holoimage);


    }

    @Override
    public void onClick(View view)
    {
        if(view==close)
        {
            finish();
        }
    }
}
