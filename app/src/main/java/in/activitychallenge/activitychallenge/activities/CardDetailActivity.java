package in.activitychallenge.activitychallenge.activities;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Calendar;

import in.activitychallenge.activitychallenge.R;


public class CardDetailActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout card_number_til, cvv_number_til, customer_name_til;
    EditText card_number_edt, cvv_number_edt, customer_name_edt;
    TextView card_exp_date_txt, carddetail_titletxt, text_for_amount_change;
    ImageView send_carddetail_btn;
    RadioGroup pay_radio_group;
    RadioButton detail_paypal_radio, detail_debitcard_radio;
    public int mYear, mMonth, mDay;
    Typeface typeface, typeface2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_card_detail);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        card_number_edt = (EditText) findViewById(R.id.card_number_edt);
        card_number_edt.setTypeface(typeface);
        cvv_number_edt = (EditText) findViewById(R.id.cvv_number_edt);
        cvv_number_edt.setTypeface(typeface);
        customer_name_edt = (EditText) findViewById(R.id.customer_name_edt);
        customer_name_edt.setTypeface(typeface);
        card_number_til = (TextInputLayout) findViewById(R.id.card_number_til);
        cvv_number_til = (TextInputLayout) findViewById(R.id.cvv_number_til);
        customer_name_til = (TextInputLayout) findViewById(R.id.customer_name_til);
        card_exp_date_txt = (TextView) findViewById(R.id.card_exp_date_txt);
        card_exp_date_txt.setOnClickListener(this);
        card_exp_date_txt.setTypeface(typeface);
        carddetail_titletxt = (TextView) findViewById(R.id.carddetail_titletxt);
        carddetail_titletxt.setOnClickListener(this);
        carddetail_titletxt.setTypeface(typeface2);
        text_for_amount_change = (TextView) findViewById(R.id.text_for_amount_change);
        text_for_amount_change.setOnClickListener(this);
        send_carddetail_btn = (ImageView) findViewById(R.id.send_carddetail_btn);
        send_carddetail_btn.setOnClickListener(this);
        pay_radio_group = (RadioGroup) findViewById(R.id.pay_radio_group);
        detail_paypal_radio = (RadioButton) findViewById(R.id.detail_paypal_radio);
        detail_paypal_radio.setOnClickListener(this);
        detail_paypal_radio.setTypeface(typeface);
        detail_debitcard_radio = (RadioButton) findViewById(R.id.detail_debitcard_radio);
        detail_debitcard_radio.setOnClickListener(this);
        detail_debitcard_radio.setTypeface(typeface);


    }

    @Override
    public void onClick(View v) {
        if (v == card_exp_date_txt) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    card_exp_date_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));
                    card_exp_date_txt.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }
}
