package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.MeAsASponsorFragment;
import in.activitychallenge.activitychallenge.fragments.MySponsoringFragment;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MySponserActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView close;
    ViewPager view_pager_mysponser_activity;
    TabLayout tab;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    MyChllengesViewPagerAdapter mysponsoring_activity_dapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sponser);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SELOTTONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        tab = (TabLayout) findViewById(R.id.tabLayout_mysponser_activity);
        view_pager_mysponser_activity = (ViewPager) findViewById(R.id.view_pager_mysponser_activity);

        setupViewPager(view_pager_mysponser_activity);
        tab.setupWithViewPager(view_pager_mysponser_activity);
    }

    @Override
    public void onClick(View view)
    {
       if(view==close)
       {finish();}

    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
            mysponsoring_activity_dapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());

            mysponsoring_activity_dapter.addFrag(new MeAsASponsorFragment(), "Me As a Sponsor");
            mysponsoring_activity_dapter.addFrag(new MySponsoringFragment(), "My Sponsorings");


        view_pager_suggest.setAdapter(mysponsoring_activity_dapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_mysponser_activity) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }
}
