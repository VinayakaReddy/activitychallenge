package in.activitychallenge.activitychallenge.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.Config;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.NotificationUtils;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView register_btn;
    CheckBox t_c_chk,eula_chk;
    TextView dob_text, reg_title_text, plus_refer,t_c_textview,eula_textview;
    TextInputLayout username_til, lastname_til, email_til, password_til;
    EditText username_edt, lastname_edt, email_edt, pasword_edt, refer_code;
    RadioGroup gender_group, user_type_radio_group;
    RadioButton male_radio, female_radio, other_radio, user_radio, sponsor_radio, genderStatus, usertypeStatus;
    int gunder_status = 0;
    int user_init_status = 0;
    public int mYear, mMonth, mDay;
    String user_id, send_name, send_email, send_lastname, send_pasword, sendGender, sendUserType, send_refercode, mobile, country_code, device_id;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    Typeface typeface, typeface2;
    String EMAIL_REG = "\"^[\\\\w_\\\\.+]*[\\\\w_\\\\.]\\\\@([\\\\w]+\\\\.)+[\\\\w]+[\\\\w]$\";";
    String login_type, send_fb_user_data, send_fb_name, send_fb_lastname, send_fb_email, send_fb_gender, send_fb_provider, send_google_name, send_google_lastname,
            send_google_email, send_google_id, send_google_provider,sendDOB;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registration);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        Bundle bundle = getIntent().getExtras();
        sendUserType = bundle.getString("USERTYPE");
        login_type = bundle.getString("LOGIN");
        Log.d("LOGINTYPE", login_type);
        pprogressDialog = new ProgressDialog(RegistrationActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id =  bundle.getString("USERID");//userDetails.get(UserSessionManager.USER_ID);
        mobile = userDetails.get(UserSessionManager.MOBILE_NO);
        country_code = userDetails.get(UserSessionManager.COUNTRY_CODE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("USERRRRRRRR", user_id + "///" + sendUserType + "" + mobile + "///" + country_code + "//" + device_id);


        t_c_chk = (CheckBox) findViewById(R.id.t_c_chk);
        eula_chk = (CheckBox) findViewById(R.id.eula_chk);
        t_c_textview = (TextView) findViewById(R.id.t_c_textview);
        eula_textview = (TextView) findViewById(R.id.eula_textview);
        t_c_textview.setTypeface(typeface);
        eula_textview.setTypeface(typeface);

        username_edt = (EditText) findViewById(R.id.username_edt);
        username_edt.setTypeface(typeface);
        lastname_edt = (EditText) findViewById(R.id.lastname_edt);
        lastname_edt.setTypeface(typeface);
        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setTypeface(typeface);
        pasword_edt = (EditText) findViewById(R.id.pasword_edt);
        pasword_edt.setTypeface(typeface);
        refer_code = (EditText) findViewById(R.id.refer_code);
        refer_code.setTypeface(typeface);
        username_til = (TextInputLayout) findViewById(R.id.username_til);
        lastname_til = (TextInputLayout) findViewById(R.id.lastname_til);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        password_til = (TextInputLayout) findViewById(R.id.password_til);
        dob_text = (TextView) findViewById(R.id.dob_text);
        dob_text.setTypeface(typeface);
        dob_text.setOnClickListener(this);
        reg_title_text = (TextView) findViewById(R.id.reg_title_text);
        reg_title_text.setTypeface(typeface2);
        plus_refer = (TextView) findViewById(R.id.plus_refer);
        plus_refer.setTypeface(typeface);
        plus_refer.setOnClickListener(this);
        register_btn = (ImageView) findViewById(R.id.register_btn);
        register_btn.setOnClickListener(this);
        gender_group = (RadioGroup) findViewById(R.id.gender_group);
        user_type_radio_group = (RadioGroup) findViewById(R.id.user_type_radio_group);
        male_radio = (RadioButton) findViewById(R.id.male_radio);
        male_radio.setOnClickListener(this);
        male_radio.setTypeface(typeface);
        female_radio = (RadioButton) findViewById(R.id.female_radio);
        female_radio.setOnClickListener(this);
        female_radio.setTypeface(typeface);
        other_radio = (RadioButton) findViewById(R.id.other_radio);
        other_radio.setOnClickListener(this);
        other_radio.setTypeface(typeface);
        user_radio = (RadioButton) findViewById(R.id.user_radio);
        user_radio.setOnClickListener(this);
        user_radio.setTypeface(typeface);
        sponsor_radio = (RadioButton) findViewById(R.id.sponsor_radio);
        sponsor_radio.setOnClickListener(this);
        sponsor_radio.setTypeface(typeface);
        if (login_type.equals("Facebook")) {
            Bundle bundle_fb = getIntent().getExtras();
            send_fb_user_data = bundle_fb.getString("user_data_id");
            send_fb_name = bundle_fb.getString("name");
            send_fb_lastname = bundle_fb.getString("lastname");
            send_fb_email = bundle_fb.getString("email");
            send_fb_gender = bundle_fb.getString("gender");
            send_fb_provider = bundle_fb.getString("provider");
            if (!send_fb_email.equals(EMAIL_REG)) {
                email_edt.setFocusable(true);
            }
            username_edt.setText(send_fb_name);
            username_edt.setFocusable(false);
            lastname_edt.setText(send_fb_lastname);
            lastname_edt.setFocusable(false);
            email_edt.setText(send_fb_email);
            email_edt.setFocusable(false);
            loginFB(send_fb_user_data, send_fb_name, send_fb_lastname, send_fb_email, send_fb_gender, send_fb_provider);
        }
        if (login_type.equals("Google")) {
            Bundle bundle_google = getIntent().getExtras();
            send_google_name = bundle_google.getString("name");
            send_google_lastname = bundle_google.getString("lastname");
            send_google_email = bundle_google.getString("email");
            send_google_id = bundle_google.getString("user_gmail_id");
            send_google_provider = bundle_google.getString("provider");
            username_edt.setText(send_google_name);
            username_edt.setFocusable(false);
            lastname_edt.setText(send_google_lastname);
            lastname_edt.setFocusable(false);
            email_edt.setText(send_google_email);
            email_edt.setFocusable(false);
            loginGooglePlus(send_google_name, send_google_lastname, send_google_email, send_google_id, send_google_provider);
        }

        SpannableString ss = new SpannableString("I agree to Terms and Conditions.");

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView)
            {
                Intent go=new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","TermsandConditions");
                go.putExtras(b);
                startActivity(go);
               // startActivity(new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class));
            }
            @Override
            public void updateDrawState(TextPaint ds)
            {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 11, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan( new UnderlineSpan(), 11, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        t_c_textview.setTypeface(typeface);
        t_c_textview.setText(ss);
        t_c_textview.setMovementMethod(LinkMovementMethod.getInstance());
        t_c_textview.setHighlightColor(Color.TRANSPARENT);


        SpannableString ss_eula_rules = new SpannableString("I agree to Eula Rules.");

        ClickableSpan clickableSpanEula = new ClickableSpan() {
            @Override
            public void onClick(View textView)
            {
                Intent go=new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","EULA");
                go.putExtras(b);
                startActivity(go);
               // startActivity(new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class));
            }
            @Override
            public void updateDrawState(TextPaint ds)
            {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss_eula_rules.setSpan(clickableSpanEula, 11, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss_eula_rules.setSpan( new UnderlineSpan(), 11, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        eula_textview.setTypeface(typeface);
        eula_textview.setText(ss_eula_rules);
        eula_textview.setMovementMethod(LinkMovementMethod.getInstance());
        eula_textview.setHighlightColor(Color.TRANSPARENT);


    }

    @Override
    public void onClick(View v) {
        if (v == dob_text) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {                              //strDay + "-" + strMonth + "-" + year
                        strDay = dayOfMonth + "";
                    }
                    dob_text.setText(String.valueOf(year + "-" + strMonth + "-" + strDay));

                    dob_text.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if (v == register_btn) {
            checkInternet = NetworkChecking.isConnected(RegistrationActivity.this);

            if (validate() )
            {
                if (checkInternet)
                {

                    send_name = username_edt.getText().toString();
                    send_lastname = lastname_edt.getText().toString();
                    send_email = email_edt.getText().toString();
                    send_pasword = pasword_edt.getText().toString();
                    send_refercode = refer_code.getText().toString();

                    if (male_radio.isChecked() || female_radio.isChecked() || other_radio.isChecked()) {
                        genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());

                        if (genderStatus.getId() == male_radio.getId()) {
                            sendGender = "Male";
                        }
                        if (genderStatus.getId() == female_radio.getId()) {
                            sendGender = "Female";
                        }
                        if (genderStatus.getId() == other_radio.getId()) {
                            sendGender = "Others";
                        }
                    }
                   // dob_text Log.d("VVVVV", sendDOB);
                   /* if(dob_text.getText().toString().equals("") || dob_text.getText().toString().equals(null))
                    {
                        Toast.makeText(getApplicationContext(), "Please Input DOB", Toast.LENGTH_SHORT).show();
                    }*/

                    Log.d("REGMANURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REGMANRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String token = jsonObject1.getString("token");
                                    String user_id = jsonObject1.getString("user_id");
                                    String user_type = jsonObject1.getString("user_type");
                                    String is_register = jsonObject1.getString("is_register");
                                    String provided_acc_info = jsonObject1.getString("provided_acc_info");
                                    Log.d("JWTMANN", token + "//" + provided_acc_info);
                                    String[] parts = token.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {
                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSMANN", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATMANN", jsonObject2.toString());
                                        String username = jsonObject2.getString("name");
                                        String email = jsonObject2.getString("email");
                                        String mobile = jsonObject2.getString("mobile");
                                        String status = jsonObject2.getString("status");
                                        userSessionManager.createUserLoginSession(token, username, email, mobile, status, user_type,user_id, provided_acc_info);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    if (provided_acc_info.equals("false")) {
                                        Intent iacc_page = new Intent(RegistrationActivity.this, PaypalActivity.class);
                                        iacc_page.putExtra("activity", "Registration");
                                        iacc_page.putExtra("paymentAmount", "1");
                                        startActivity(iacc_page);
                                        finish();
                                    } else {
                                        Intent imain = new Intent(RegistrationActivity.this, MainActivity.class);
                                        imain.putExtra("USERTYPE", user_type);
                                        startActivity(imain);
                                    }
                                } else if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Please provide all details..!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Account is deactivated, please contact administrator..!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10400")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Account is blocked, please contact administrator..!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10500")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Registered email is different from facebook registered email.!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10600")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Registered email is different from google registered email.!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10700")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "It seems you are login from different device..!", Toast.LENGTH_SHORT).show();
                                    // forceLogin();
                                } else if (successResponceCode.equals("10800")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Email is already exist.!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10900")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid refercode...!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            Log.d("LOGNNNNNNNN", login_type);
                            if (login_type.equals("Manual")) {
                                params.put("user_id", user_id);
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_name);
                                params.put("last_name", send_lastname);
                                params.put("gender", sendGender);
                                params.put("email", send_email);

                                params.put("dob", dob_text.getText().toString());
                                params.put("password", send_pasword);
                                params.put("mobile", mobile);
                                params.put("country_code", country_code);
                                params.put("device_id", device_id);
                                params.put("refercode", send_refercode);
                                params.put("provider", "Manual");
                                params.put("identifier", "xxxx");
                                params.put("force_login", "1");
                                Log.d("REGPARAMManual", params.toString());
                            }

                            if (login_type.equals("Google")) {
                                params.put("user_id", user_id);
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_google_name);
                                params.put("last_name", send_google_lastname);
                                params.put("gender", sendGender);
                                params.put("email", send_google_email);

                                params.put("dob", dob_text.getText().toString());
                                params.put("password", send_pasword);
                                params.put("mobile", mobile);
                                params.put("country_code", country_code);
                                params.put("device_id", device_id);
                                params.put("provider", "Google");
                                params.put("identifier", send_google_id);
                                params.put("refercode", send_refercode);
                                params.put("force_login", "1");
                                Log.d("REGPARAMGoogle", params.toString());
                            }
                            if (login_type.equals("Facebook")) {
                                params.put("user_id", user_id);
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_fb_name);
                                params.put("last_name", send_fb_lastname);
                                params.put("gender", sendGender);
                                params.put("email", send_fb_email);

                                params.put("dob", dob_text.getText().toString());
                                params.put("password", send_pasword);
                                params.put("mobile", mobile);
                                params.put("country_code", country_code);
                                params.put("device_id", device_id);
                                params.put("provider", "Facebook");
                                params.put("identifier", send_fb_user_data);
                                params.put("refercode", send_refercode);
                                params.put("force_login", "1");
                                Log.d("REGPARAMFacebook", params.toString());
                            }
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
                    requestQueue.add(stringRequest);
                } else {
                    pprogressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();
                }
            }

        }///CHECK INTERNET END
        if (v == plus_refer) {
            refer_code.setVisibility(View.VISIBLE);
        }
    }

    private boolean validate() {
        boolean result = true;
        int flag = 0;
        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String name = username_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            username_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                username_til.setError("Special characters not allowed");
                result = false;
            } else {
                username_til.setErrorEnabled(false);
            }
        }
        String lastname = lastname_edt.getText().toString().trim();
        if ((lastname == null || lastname.equals("") || lastname.length() < 3)) {
            lastname_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                lastname_til.setError("Special characters not allowed");
                result = false;
            } else {
                lastname_til.setErrorEnabled(false);
            }
        }
        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_til.setError("Invalid Email");
            result = false;
        } else {
            email_til.setErrorEnabled(false);
        }

        String password = pasword_edt.getText().toString().trim();
        if (password.isEmpty() || password.length() < 6) {
            password_til.setError("Minimum 6 characters required");
            result = false;
        } else {
            password_til.setErrorEnabled(false);
        }
        String dob = dob_text.getText().toString().trim();
        if (dob == null || dob.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Please Input DOB", Toast.LENGTH_SHORT).show();
            result = false;
        }
        if(!t_c_chk.isChecked())
        {
            Toast.makeText(getApplicationContext(), "Please Check Terms & Conditions", Toast.LENGTH_SHORT).show();
            result = false;
        } if(!eula_chk.isChecked())
        {
            Toast.makeText(getApplicationContext(), "Please Check Eula Rules", Toast.LENGTH_SHORT).show();
            result = false;
        }

        return result;
    }

    public void loginFB(String send_fb_user_data, String send_fb_name, String send_fb_lastname, String send_fb_email, String send_fb_gender, String send_fb_provider) {
    }

    public void loginGooglePlus(String send_google_name, String send_google_lastname, String send_google_email, String send_google_id, String send_google_provider) {
    }

    public void forceLogin() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("It seems you are login from different device..!" + "\n" + "Do you want to  force login..?");

        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkInternet = NetworkChecking.isConnected(RegistrationActivity.this);
                        if (validate()) {
                            if (checkInternet) {
                                send_name = username_edt.getText().toString();
                                send_lastname = lastname_edt.getText().toString();
                                send_email = email_edt.getText().toString();
                                send_pasword = pasword_edt.getText().toString();
                                if (male_radio.isChecked() || female_radio.isChecked() || other_radio.isChecked()) {
                                    genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());
                                    if (genderStatus.getId() == male_radio.getId()) {
                                        sendGender = "Male";

                                    }
                                    if (genderStatus.getId() == female_radio.getId()) {
                                        sendGender = "Female";

                                    }
                                    if (genderStatus.getId() == other_radio.getId()) {
                                        sendGender = "Others";
                                    }
                                }
                                if (dob_text.equals(null) || dob_text.equals("")) {
                                    Toast.makeText(RegistrationActivity.this, "Please Input All Details", Toast.LENGTH_LONG);
                                }
                                Log.d("REGMANURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("REGMANRESP", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String successResponceCode = jsonObject.getString("response_code");
                                            if (successResponceCode.equals("10100")) {
                                                //  getToken();
                                                pprogressDialog.dismiss();
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                String token = jsonObject1.getString("token");
                                                String user_id = jsonObject1.getString("user_id");
                                                String user_type = jsonObject1.getString("user_type");
                                                String is_register = jsonObject1.getString("is_register");
                                                String provided_acc_info = jsonObject1.getString("provided_acc_info");
                                                Log.d("JWTMANN", token + "//" + provided_acc_info);
                                                String[] parts = token.split("\\.");
                                                byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                                String decodedString = "";
                                                try {
                                                    decodedString = new String(dataDec, "UTF-8");
                                                    Log.d("TOKENSMANN", decodedString);
                                                    JSONObject jsonObject2 = new JSONObject(decodedString);
                                                    Log.d("JSONDATMANN", jsonObject2.toString());
                                                    String username = jsonObject2.getString("name");
                                                    String email = jsonObject2.getString("email");
                                                    String mobile = jsonObject2.getString("mobile");
                                                    String status = jsonObject2.getString("status");
                                                    userSessionManager.createUserLoginSession(token, username, email, mobile, status, user_type,user_id,provided_acc_info);

                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                                if (provided_acc_info.equals("false")) {
                                                    Toast.makeText(RegistrationActivity.this, "In order to validate your payment method, we will deduct $ 1 and this will be deposited into your application wallet account", Toast.LENGTH_SHORT).show();
                                                    Intent iacc_page = new Intent(RegistrationActivity.this, PaypalActivity.class);
                                                    iacc_page.putExtra("activity", "Registration");
                                                    iacc_page.putExtra("paymentAmount", "1");
                                                    startActivity(iacc_page);
                                                    finish();
                                                } else {
                                                    Intent imain = new Intent(RegistrationActivity.this, MainActivity.class);
                                                    imain.putExtra("USERTYPE", user_type);
                                                    startActivity(imain);
                                                }
                                            } else if (successResponceCode.equals("10200")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Please provide all details..!", Toast.LENGTH_SHORT).show();
                                            } else if (successResponceCode.equals("10300")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Account is deactivated, please contact administrator..!", Toast.LENGTH_SHORT).show();
                                            } else if (successResponceCode.equals("10400")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Account is blocked, please contact administrator..!", Toast.LENGTH_SHORT).show();
                                            } else if (successResponceCode.equals("10500")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Registered email is different from facebook registered email.!", Toast.LENGTH_SHORT).show();
                                            } else if (successResponceCode.equals("10600")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Registered email is different from google registered email.!", Toast.LENGTH_SHORT).show();
                                            } else if (successResponceCode.equals("10700")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "It seems you are login from different device..!", Toast.LENGTH_SHORT).show();
                                                forceLogin();
                                            } else if (successResponceCode.equals("10800")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Email is already exist.!", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        pprogressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {

                                        Map<String, String> params = new HashMap<String, String>();
                                        Log.d("LOGNNNNNNNN", login_type);
                                        if (login_type.equals("Manual")) {
                                            params.put("user_id", user_id);
                                            params.put("user_type", sendUserType);
                                            params.put("first_name", send_name);
                                            params.put("last_name", send_lastname);
                                            params.put("gender", sendGender);
                                            params.put("email", send_email);
                                            params.put("dob", dob_text.getText().toString());
                                            params.put("password", send_pasword);
                                            params.put("mobile", mobile);
                                            params.put("country_code", country_code);
                                            params.put("device_id", device_id);
                                            params.put("provider", "Manual");
                                            params.put("identifier", "xxxx");
                                            params.put("force_login", "1");
                                            Log.d("REGPARAMManual", params.toString());
                                        }
                                        if (login_type.equals("Google")) {
                                            params.put("user_id", user_id);
                                            params.put("user_type", sendUserType);
                                            params.put("first_name", send_google_name);
                                            params.put("last_name", send_google_lastname);
                                            params.put("gender", sendGender);
                                            params.put("email", send_google_email);
                                            params.put("dob", dob_text.getText().toString());
                                            params.put("password", send_pasword);
                                            params.put("mobile", mobile);
                                            params.put("country_code", country_code);
                                            params.put("device_id", device_id);
                                            params.put("provider", "Google");
                                            params.put("identifier", send_google_id);
                                            params.put("force_login", "1");
                                            Log.d("REGPARAMGoogle", params.toString());
                                        }
                                        if (login_type.equals("Facebook")) {
                                            params.put("user_id", user_id);
                                            params.put("user_type", sendUserType);
                                            params.put("first_name", send_fb_name);
                                            params.put("last_name", send_fb_lastname);
                                            params.put("gender", sendGender);
                                            params.put("email", send_fb_email);
                                            params.put("dob", dob_text.getText().toString());
                                            params.put("password", send_pasword);
                                            params.put("mobile", mobile);
                                            params.put("country_code", country_code);
                                            params.put("device_id", device_id);
                                            params.put("provider", "Facebook");
                                            params.put("identifier", send_fb_user_data);
                                            params.put("force_login", "1");
                                            Log.d("REGPARAMFacebook", params.toString());
                                        }
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
                                requestQueue.add(stringRequest);
                            } else {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void getToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            sendFCMTokes(user_id, sendUserType, regId);
        } else {
            Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void sendFCMTokes(final String user_id, final String user_type, final String token_fcm) {
        checkInternet = NetworkChecking.isConnected(RegistrationActivity.this);
        if (checkInternet) {
            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FCM);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.FCM,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                   // Toast.makeText(RegistrationActivity.this, "Token Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("fcm_register_id", token_fcm);
                    params.put("device_platform", "ANDROID");
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(RegistrationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
}
