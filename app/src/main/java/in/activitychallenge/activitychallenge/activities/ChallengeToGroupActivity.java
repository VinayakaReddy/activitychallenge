package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.filters.ChallengeToGroupFilterList;
import in.activitychallenge.activitychallenge.holder.ChallengeToGroupHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeToGroupModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ChallengeToGroupActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView crate_group_plus, close, grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5, voice_search;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id;
    UserSessionManager session;
    SearchView searchMyGroup;
    RecyclerView my_group_recylerview;
    LinearLayoutManager lManager;
    ArrayList<ChallengeToGroupModel> myGlist = new ArrayList<ChallengeToGroupModel>();
    MyGroupAdapter myGroAdap;
    SearchView activity_search;
    EditText searchEditText;
    TextView count_sponsor_text, count_challenge_text, count_mesages_text;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    String amount, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng, main_user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_to_group);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SSDERGNDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        activity_search = (SearchView) findViewById(R.id.members_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText = (EditText) activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon = (ImageView) activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        grp_img_1 = (ImageView) findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) findViewById(R.id.grp_img_5);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);

        crate_group_plus = (ImageView) findViewById(R.id.crate_group_plus);
        crate_group_plus.setOnClickListener(this);
        searchMyGroup = (SearchView) findViewById(R.id.members_search);
        searchMyGroup.clearFocus();
        searchMyGroup.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                myGroAdap.getFilter().filter(query);
                return false;
            }
        });
        voice_search = (ImageView) findViewById(R.id.voice_search);
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getApplicationContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        user_id = getIntent().getExtras().getString("user_id");
        main_user_type = getIntent().getExtras().getString("user_type");
        amount = getIntent().getExtras().getString("amount");
        activity_id = getIntent().getExtras().getString("activity_id");
        challenge_id = getIntent().getExtras().getString("challenge_id");
        challenge_goal = getIntent().getExtras().getString("challenge_goal");
        evaluation_factor = getIntent().getExtras().getString("evaluation_factor");
        evaluation_factor_unit = getIntent().getExtras().getString("evaluation_factor_unit");
        type = getIntent().getExtras().getString("type");
        span = getIntent().getExtras().getString("span");
        date = getIntent().getExtras().getString("date");
        location_name = getIntent().getExtras().getString("location_name");
        location_lat = getIntent().getExtras().getString("location_lat");
        location_lng = getIntent().getExtras().getString("location_lng");
        my_group_recylerview = (RecyclerView) findViewById(R.id.my_group_recylerview);
        my_group_recylerview.setNestedScrollingEnabled(false);
        myGroAdap = new MyGroupAdapter(ChallengeToGroupActivity.this, myGlist, R.layout.row_challenge_to_group,
                user_id, amount, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng, main_user_type);
        lManager = new LinearLayoutManager(this);
        my_group_recylerview.setLayoutManager(lManager);

        getMyGroups();
        getCount();

    }

    private void getMyGroups() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_TO_GROUP + user_id + AppUrls.USER_TYPE + main_user_type,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("GetGroupUrl", AppUrls.BASE_URL + AppUrls.GET_GROUPS);
                            Log.d("GetGroupUrlResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                  //  Toast.makeText(ChallengeToGroupActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();

                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        ChallengeToGroupModel rhm = new ChallengeToGroupModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setGroup_pic(itemArray.getString("group_pic"));
                                        rhm.setGroup_members(itemArray.getString("group_members") + " " + "members");
                                        rhm.setTotal_win(itemArray.getString("total_win"));
                                        rhm.setTotal_loss(itemArray.getString("total_loss"));
                                        rhm.setOverall_rank(itemArray.getString("overall_rank"));
                                        rhm.setWinning_per(itemArray.getString("winning_per") + " " + "%");
                                        rhm.setAlready_applied(itemArray.getString("already_applied"));
                                        rhm.setImg1(itemArray.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        rhm.setImg2(itemArray.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        rhm.setImg3(itemArray.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        rhm.setImg4(itemArray.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        rhm.setImg5(itemArray.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        myGlist.add(rhm);
                                    }
                                    my_group_recylerview.setAdapter(myGroAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(ChallengeToGroupActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(ChallengeToGroupActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MyGroup_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {
            pprogressDialog.cancel();
            Toast.makeText(ChallengeToGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == crate_group_plus) {
            Intent intent = new Intent(ChallengeToGroupActivity.this, CreateGroupActivity.class);
            startActivity(intent);
        }

    }


    public class MyGroupAdapter extends RecyclerView.Adapter<ChallengeToGroupHolder> implements Filterable {
        ChallengeToGroupActivity context;
        LayoutInflater lInfla;
        int resource;
        public ArrayList<ChallengeToGroupModel> myList;
        ArrayList<ChallengeToGroupModel> filterlist;
        ChallengeToGroupFilterList filter;
        String user_id, amount, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng, main_user_type;

        public MyGroupAdapter(ChallengeToGroupActivity context, ArrayList<ChallengeToGroupModel> myList, int resource,
                              String user_id, String amount, String activity_id, String challenge_id, String challenge_goal,
                              String evaluation_factor, String evaluation_factor_unit, String type, String span, String date,
                              String location_name, String location_lat, String location_lng, String main_user_type) {
            this.context = context;
            this.myList = myList;
            this.resource = resource;
            this.filterlist = myList;
            this.user_id = user_id;
            this.amount = amount;
            this.activity_id = activity_id;
            this.challenge_id = challenge_id;
            this.challenge_goal = challenge_goal;
            this.evaluation_factor = evaluation_factor;
            this.evaluation_factor_unit = evaluation_factor_unit;
            this.type = type;
            this.span = span;
            this.date = date;
            this.location_name = location_name;
            this.location_lat = location_lat;
            this.location_lng = location_lng;
            this.main_user_type = main_user_type;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public ChallengeToGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = lInfla.inflate(resource, null);
            ChallengeToGroupHolder slh = new ChallengeToGroupHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(ChallengeToGroupHolder holder, final int position) {
            holder.grp_name.setText(myList.get(position).getName());
            holder.grp_memb_cnt.setText(myList.get(position).getGroup_members());
            holder.grp_lost_cnt.setText(myList.get(position).getTotal_loss());
            holder.grp_won_cnt.setText(myList.get(position).getTotal_win());
            holder.grp_percent.setText(myList.get(position).getWinning_per());
            holder.grp_rank.setText(myList.get(position).getOverall_rank());
           /* if (!myList.get(position).getOverall_rank().equals("0")) {
                holder.sendReq_butt.setVisibility(View.GONE);
            } else {
                holder.sendReq_butt.setVisibility(View.VISIBLE);
            }*/
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .into(holder.grp_img);
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg1())
                    .into(holder.grp_img_1);
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg2())
                    .into(holder.grp_img_2);
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg3())
                    .into(holder.grp_img_3);
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg4())
                    .into(holder.grp_img_4);
            Picasso.with(ChallengeToGroupActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg5())
                    .into(holder.grp_img_5);
            holder.setItemClickListener(new MyGroupItemClickListener() {
                @Override
                public void onItemClick(View view, int layoutPosition) {
                   /* Intent ii = new Intent(ChallengeToGroupActivity.this, GroupDetailActivity.class);
                    ii.putExtra("grp_id", myList.get(position).getId());
                    startActivity(ii);*/
                }
            });
            holder.sendReq_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkInternet = NetworkChecking.isConnected(context);
                    if (checkInternet) {
                    Intent intent = new Intent(context, ChallengePaymentActivity.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("user_type", main_user_type);
                    intent.putExtra("activity", "challengeto");
                    intent.putExtra("paymentAmount", amount);
                    intent.putExtra("activity_id", activity_id);
                    intent.putExtra("challenge_id", challenge_id);
                    intent.putExtra("challenge_goal", challenge_goal);
                    intent.putExtra("evaluation_factor", evaluation_factor);
                    intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                    intent.putExtra("type", type);
                    intent.putExtra("span", span);
                    intent.putExtra("date", date);
                    intent.putExtra("location_name", location_name);
                    intent.putExtra("location_lat", location_lat);
                    intent.putExtra("location_lng", location_lng);
                    intent.putExtra("opponent_id", myList.get(position).getId());
                    intent.putExtra("opponent_type", "GROUP");
                    context.startActivity(intent);
                    } else {
                        pprogressDialog.cancel();
                        Toast.makeText(ChallengeToGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return myList.size();
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new ChallengeToGroupFilterList(filterlist, this);
            }
            return filter;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    myGroAdap.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }


    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeToGroupActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

}