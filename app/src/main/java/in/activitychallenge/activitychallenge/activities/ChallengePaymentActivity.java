package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.models.LotteryCodesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;
import okhttp3.internal.Util;

public class ChallengePaymentActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    TextView challenge_amt_txt,toolbar_title;
    RadioGroup pay_rg;
    RadioButton wallet_rb, paypal_rb;
    Button proceed_btn;
    String activity,ref_txn_id, paymentAmount, challengeAmount, walletAmount, challenge_id, location_name,loc_name,location_lat,loc_lat, location_long,loc_long,
            user_id = "", user_name = "", user_type = "", token = "", device_id = "", promotion_id, promotionAmount, sponsorAmount,addwalletAmount, member_id,
            member_user_type,group_id,group_user_type;
    double wallet, challenge, result;
    private boolean checkInternet;
    UserSessionManager session;
    int timeinsecs;
    ProgressDialog progressDialog;
    String eventid="";
    String activity_id = "", challenge_goal = "", evaluation_factor = "", evaluation_factor_unit = "", type = "", span = "", date = "",
            opponent_id = "", opponent_type = "", paymentAmount_sendChallenge = "", user_id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_payment);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
          eventid= Utils.getInstance().getEventtype();
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        pay_rg = (RadioGroup) findViewById(R.id.pay_rg);
        wallet_rb = (RadioButton) findViewById(R.id.wallet_rb);
        pay_rg.check(wallet_rb.getId());
        paypal_rb = (RadioButton) findViewById(R.id.paypal_rb);
        proceed_btn = (Button) findViewById(R.id.proceed_btn);
        proceed_btn.setOnClickListener(this);
        challenge_amt_txt = (TextView) findViewById(R.id.challenge_amt_txt);

        session = new UserSessionManager(ChallengePaymentActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id_user = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

        //get Time zone
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsecs = (tz.getOffset(cal.getTimeInMillis()))/1000;


        getWalletAmount();

        Bundle bundle = getIntent().getExtras();
        activity = bundle.getString("activity");
        if (activity.equals("ChallengeRequestAdapter"))
        {
            challengeAmount = bundle.getString("challengeAmount");
            challenge_id = bundle.getString("challenge_id");
            loc_name = bundle.getString("location_name");
            loc_lat = bundle.getString("location_lat");
            loc_long = bundle.getString("location_long");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            user_id = userDetails.get(UserSessionManager.USER_ID);
            challenge = Double.parseDouble(challengeAmount);
            challenge_amt_txt.setText("$" + challengeAmount + "USD");
            Log.d("LOCATIONCHECKING",loc_lat+"\n"+loc_long+"\n"+loc_name);

        } else if (activity.equals("MyPromotionFragment")) {
            promotion_id = bundle.getString("promotion_id");
            promotionAmount = bundle.getString("promotionAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            user_id = userDetails.get(UserSessionManager.USER_ID);
            challenge = Double.parseDouble(promotionAmount);
            challenge_amt_txt.setText("$" + promotionAmount + "USD");
        } else if (activity.equals("challengeto")) {
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            paymentAmount_sendChallenge = bundle.getString("paymentAmount");
            activity_id = bundle.getString("activity_id");
            challenge_id = bundle.getString("challenge_id");
            challenge_goal = bundle.getString("challenge_goal");
            evaluation_factor = bundle.getString("evaluation_factor");
            evaluation_factor_unit = bundle.getString("evaluation_factor_unit");

            type = bundle.getString("type");
            span = bundle.getString("span");
            date = bundle.getString("date");
            location_name = bundle.getString("location_name");
            location_lat = bundle.getString("location_lat");
            location_long = bundle.getString("location_lng");
            opponent_id = bundle.getString("opponent_id");
            opponent_type = bundle.getString("opponent_type");
            challenge_amt_txt.setText("$" + paymentAmount_sendChallenge + "USD");
            Log.d("values_of_challenge", user_id + " /// " + paymentAmount_sendChallenge + " /// " + activity_id + " /// " + challenge_id + " /// " + challenge_goal + " /// " + evaluation_factor
                    + " /// " + evaluation_factor_unit + " /// " + type + " /// " + span + " /// " + date + " /// " + location_name + " /// " + location_lat
                    + " /// " + location_long + " /// " + opponent_id + " /// " + opponent_type);

        }
        else if (activity.equals("MemberDetail"))
        {
            member_id = bundle.getString("member_id");
            user_id = userDetails.get(UserSessionManager.USER_ID);
            member_user_type = bundle.getString("member_user_type");
            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor")){
              //  wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("sponsorAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("GroupDetail"))
        {
            group_id = bundle.getString("group_id");
            user_id = userDetails.get(UserSessionManager.USER_ID);
            group_user_type = bundle.getString("group_user_type");
            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor"))
            {
               // wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("sponsorAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("MeAsaSponsorAPP"))
        {

            user_id = userDetails.get(UserSessionManager.USER_ID);

            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor"))
            {
               // wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("sponsorAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("MeAsSponsorActivity"))
        {

            user_id = userDetails.get(UserSessionManager.USER_ID);

            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor"))
            {
                //wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("sponsorAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("Sponsor_special_event"))
        {

            user_id = userDetails.get(UserSessionManager.USER_ID);

            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor"))
            {
                //wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("sponsorAmount");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("EarnMoney"))
        {

            user_id = userDetails.get(UserSessionManager.USER_ID);

            if (user_type.equals("SPONSOR") || user_type.equals("sponsor") || user_type.equals("Sponsor"))
            {
                //wallet_rb.setVisibility(View.GONE);
                pay_rg.check(paypal_rb.getId());
                pay_rg.check(wallet_rb.getId());

            }
            sponsorAmount = bundle.getString("paymentAmount");

            ref_txn_id = bundle.getString("ref_txn_id");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(sponsorAmount);
            challenge_amt_txt.setText("$" + sponsorAmount + "USD");
        }
        else if (activity.equals("MYPROFILE_ADDWALLET"))
        {
            wallet_rb.setVisibility(View.GONE);
            pay_rg.check(paypal_rb.getId());
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);

            addwalletAmount = bundle.getString("myprofile_addWallet");
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            challenge = Double.parseDouble(addwalletAmount);
            challenge_amt_txt.setText("$" + addwalletAmount + "USD");
        }

        Log.d("CHALLENGEBUNDLE", bundle.toString());

    }

    private void getWalletAmount()
    {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet) {
            Log.d("WALLETAMOUNT", AppUrls.BASE_URL + AppUrls.WALLET_AMOUNT + user_id_user + "&user_type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.WALLET_AMOUNT + user_id_user + "&user_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.show();
                            Log.d("WalletResponce", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    walletAmount = jsonObject1.getString("wallet_amt");
                                    if (walletAmount.equals("0")) {
                                        wallet_rb.setVisibility(View.GONE);
                                        pay_rg.check(paypal_rb.getId());
                                    }
                                    wallet = Double.parseDouble(walletAmount);

                                    wallet_rb.setText("Use Wallet " + "($" + walletAmount + "USD)");
                                }

                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
        if (v == proceed_btn)
        {
            if (activity.equals("ChallengeRequestAdapter"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!challengeAmount.equals("0")) {
                    if (paypal_rb.isChecked()) {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "ChallengePayment");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", challengeAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", challengeAmount);
                            }
                        } else {
                            intent.putExtra("paymentAmount", challengeAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        intent.putExtra("challenge_id", challenge_id);
                        intent.putExtra("location_name", loc_name);
                        intent.putExtra("location_lat", loc_lat);
                        intent.putExtra("location_long", loc_long);
                        Log.d("IntentData",loc_lat+"\n"+loc_long+"\n"+loc_name);
                        startActivity(intent);
                    } else if (wallet_rb.isChecked()) {
                        //Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            acceptChallenge();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "ChallengePayment");
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", challengeAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", challengeAmount);
                            }
                            intent.putExtra("challenge_id", challenge_id);
                            intent.putExtra("location_name", loc_name);
                            intent.putExtra("location_lat", loc_lat);
                            intent.putExtra("location_long", loc_long);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            } else if (activity.equals("MyPromotionFragment"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!promotionAmount.equals("0")) {
                    if (paypal_rb.isChecked()) {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "MyPromotionFragment");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", promotionAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", promotionAmount);
                            }
                        } else {
                            intent.putExtra("paymentAmount", promotionAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        intent.putExtra("promotion_id", promotion_id);
                        startActivity(intent);
                    }
                    else if (wallet_rb.isChecked())
                    {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge)
                        {

                            promotionPay();
                        }
                        else
                            {

                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "MyPromotionFragment");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", promotionAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", promotionAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", promotionAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("promotion_id", promotion_id);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (activity.equals("challengeto"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!paymentAmount_sendChallenge.equals("0"))
                {
                    if (paypal_rb.isChecked())
                    {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "challengeto");
                        if (wallet_rb.isChecked())
                        {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            }
                        }
                        else
                            {
                            intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                            intent.putExtra("walletAmount", "0");
                        }
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                        intent.putExtra("activity_id", activity_id);
                        intent.putExtra("challenge_id", challenge_id);
                        intent.putExtra("challenge_goal", challenge_goal);
                        intent.putExtra("evaluation_factor", evaluation_factor);
                        intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                        intent.putExtra("type", type);
                        intent.putExtra("span", span);
                        intent.putExtra("date", date);
                        intent.putExtra("location_name", location_name);
                        intent.putExtra("location_lat", location_lat);
                        intent.putExtra("location_long", location_long);
                        intent.putExtra("opponent_id", opponent_id);
                        intent.putExtra("opponent_type", opponent_type);
                        startActivity(intent);

                    } else if (wallet_rb.isChecked()) {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            sendChallenge(user_id, paymentAmount_sendChallenge, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_long);
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "challengeto");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                                }
                            } else {
                                intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type", getIntent().getExtras().getString("user_type"));
                            intent.putExtra("activity_id", activity_id);
                            intent.putExtra("challenge_id", challenge_id);
                            intent.putExtra("challenge_goal", challenge_goal);
                            intent.putExtra("evaluation_factor", evaluation_factor);
                            intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                            intent.putExtra("type", type);
                            intent.putExtra("span", span);
                            intent.putExtra("date", date);
                            intent.putExtra("location_name", location_name);
                            intent.putExtra("location_lat", location_lat);
                            intent.putExtra("location_long", location_long);
                            intent.putExtra("opponent_id", opponent_id);
                            intent.putExtra("opponent_type", opponent_type);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (activity.equals("MemberDetail"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0")) {
                    if (paypal_rb.isChecked())
                    {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "MemberDetail");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else {
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked()) {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            sponsorPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "MemberDetail");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }


            else if (activity.equals("EarnMoney"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0")) {
                    if (paypal_rb.isChecked())
                    {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "EarnMoney");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else {
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                            intent.putExtra("ref_txn_id", ref_txn_id);
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked()) {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            lotteryWalletPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "EarnMoney");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                            intent.putExtra("ref_txn_id", ref_txn_id);

                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }






            else if (activity.equals("MeAsaSponsorAPP"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0"))
                {
                    if (paypal_rb.isChecked())
                    {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "MeAsaSponsorAPP");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else { //59def9dce25e4081ac0f1093");
                            //"APP");
                            intent.putExtra("member_id", "59def9dce25e4081ac0f1093");
                            intent.putExtra("member_user_type", "APP");
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked())
                    {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            sponsorPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "MeAsaSponsorAPP");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("member_id", "59def9dce25e4081ac0f1093");
                            intent.putExtra("member_user_type", "APP");
                           /* intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);*/
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (activity.equals("MeAsSponsorActivity"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0")) {
                    if (paypal_rb.isChecked()) {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "MeAsSponsorActivity");
                        if (wallet_rb.isChecked())
                        {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else {
                            intent.putExtra("member_id", getIntent().getExtras().getString("member_id") );
                            intent.putExtra("member_user_type", "ACTIVITY");
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked())
                    {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge)
                        {
                            sponsorPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "MeAsSponsorActivity");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            /*intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);*/
                            intent.putExtra("member_id", getIntent().getExtras().getString("member_id") );
                            intent.putExtra("member_user_type", "ACTIVITY");
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (activity.equals("Sponsor_special_event"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0")) {
                    if (paypal_rb.isChecked()) {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "Sponsor_special_event");
                        if (wallet_rb.isChecked())
                        {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else {
                            intent.putExtra("member_id", getIntent().getExtras().getString("member_id") );
                            intent.putExtra("member_user_type", "SPECIAL_EVENT");
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked())
                    {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge)
                        {
                            sponsorPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "Sponsor_special_event");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            /*intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);*/
                            intent.putExtra("member_id", getIntent().getExtras().getString("member_id") );
                            intent.putExtra("member_user_type", "SPECIAL_EVENT");
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }

            else if(activity.equals("MYPROFILE_ADDWALLET"))
            {
                if (!paymentAmount_sendChallenge.equals("0"))
                {
                    if (paypal_rb.isChecked())
                    {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "MYPROFILE_ADDWALLET");
                        intent.putExtra("member_id", user_id);
                        intent.putExtra("member_user_type", user_type);
                        intent.putExtra("paymentAmount", addwalletAmount);
                        intent.putExtra("walletAmount", "0");
                        startActivity(intent);
                    }


                }
                else
                {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (activity.equals("GroupDetail"))
            {
                result = challenge - wallet;
                paymentAmount = String.valueOf(result);
                if (!sponsorAmount.equals("0")) {
                    if (paypal_rb.isChecked()) {
                        Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                        intent.putExtra("activity", "GroupDetail");
                        if (wallet_rb.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", sponsorAmount);
                            }
                        } else {
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("group_user_type", group_user_type);
                            intent.putExtra("paymentAmount", sponsorAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } else if (wallet_rb.isChecked()) {
                        Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                        result = challenge - wallet;
                        paymentAmount = String.valueOf(result);
                        if (wallet > challenge || wallet == challenge) {
                            sponsorPay();
                        } else {
                            Intent intent = new Intent(ChallengePaymentActivity.this, PaypalActivity.class);
                            intent.putExtra("activity", "GroupDetail");
                            if (wallet_rb.isChecked()) {
                                if (wallet > challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                } else if (wallet < challenge) {
                                    intent.putExtra("paymentAmount", paymentAmount);
                                    intent.putExtra("walletAmount", walletAmount);
                                } else if (wallet == challenge) {
                                    intent.putExtra("paymentAmount", "0");
                                    intent.putExtra("walletAmount", sponsorAmount);
                                }
                            } else {
                                intent.putExtra("paymentAmount", sponsorAmount);
                                intent.putExtra("walletAmount", "0");
                            }
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("group_user_type", group_user_type);
                            startActivity(intent);
                        }
                    }
                } else {
                    Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void lotteryWalletPay() {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet)
        {
            Log.d("WALLPAYURL",AppUrls.BASE_URL + AppUrls.EARN_MONEY_AFTER_PAY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.EARN_MONEY_AFTER_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("WALLPAYRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Wallet Payment Successful, Lottery Code Generated..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), EarnActivity.class);
                                   /* intent.putExtra("paymentId", paymentId);
                                    intent.putExtra("ref_txn_id", ref_txn_id);
                                    intent.putExtra("amount", amount);
                                    intent.putExtra("wallet_amount", wallet_amount);*/
                                    startActivity(intent);
                                   // JSONObject jobj = jsonObject.getJSONObject("data");
                                    /*JSONObject res_data = jobj.getJSONObject("res_data");
                                    JSONArray jarray = res_data.getJSONArray("codes");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        LotteryCodesModel rhm = new LotteryCodesModel();
                                        rhm.setLottery_codes(jarray.getString(i));
                                        lotteryCodesModels.add(rhm);
                                    }
                                    lottery_recyclerview.setAdapter(adapter);*/
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("txn_id", ref_txn_id);
                    params.put("payment_id", "");
                  //  params.put("payment_mode", "WALLET");
                    params.put("txn_status", "COMPLETED");
                    params.put("amount", sponsorAmount);
                    if (wallet_rb.isChecked())
                    {
                        if (wallet > challenge) {
                            if (wallet_rb.isChecked())
                            {
                                params.put("amount_wallet", sponsorAmount);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }

                        } else if (wallet < challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", paymentAmount);
                                params.put("payment_mode", "WALLET_PAYPAL");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }
                        } else if (wallet == challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }
                        }
                    }
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sponsorPay() {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SPONSOR_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("SPONSORRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Sponsor Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ChallengePaymentActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (activity.equals("MemberDetail"))
                    {
                        params.put("to_user_id", member_id);
                        params.put("to_user_type", member_user_type);
                    }
                    else if (activity.equals("GroupDetail"))
                    {
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", group_user_type);
                    }
                    else if (activity.equals("MeAsaSponsorAPP"))
                    {
                        params.put("to_user_id", "59def9dce25e4081ac0f1093");
                        params.put("to_user_type", "APP");
                    }
                    else if (activity.equals("MeAsSponsorActivity"))
                    {
                        params.put("to_user_id",getIntent().getExtras().getString("member_id"));
                        params.put("to_user_id",getIntent().getExtras().getString("member_id"));
                        params.put("to_user_type", "ACTIVITY");
                    }
                    else if (activity.equals("Sponsor_special_event"))
                    {
                        params.put("to_user_id",getIntent().getExtras().getString("member_id"));
                        params.put("to_user_id",getIntent().getExtras().getString("member_id"));
                        params.put("to_user_type", "SPECIAL_EVENT");
                    }

                    params.put("gateway_response", "");
                    params.put("amount", sponsorAmount);
                    params.put("payment_id", "");
                    if (wallet_rb.isChecked())
                    {
                        if (wallet > challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", sponsorAmount);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }

                        } else if (wallet < challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", paymentAmount);
                                params.put("payment_mode", "WALLET_PAYPAL");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }
                        } else if (wallet == challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", sponsorAmount);
                                params.put("payment_mode", "PAYPAL");
                            }
                        }
                    }
                    else {
                        params.put("amount_wallet", "0");
                        params.put("amount_paypal", sponsorAmount);
                    }
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void acceptChallenge() {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_CHALLENGE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.show();
                            Log.d("AcceptChallengeResponce", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");
                                    Toast.makeText(ChallengePaymentActivity.this, "Paid Through Wallet..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ChallengePaymentActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_id", challenge_id);
                    if (activity.equals("ChallengeRequestAdapter")) {
                        params.put("location_name", loc_name);
                        params.put("location_lat", loc_lat);
                        params.put("location_long", loc_long);
                    }
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("gateway_response", "");
                    params.put("amount", challengeAmount);
                    params.put("payment_id", "");
                    params.put("payment_mode", "WALLET");
                    params.put("timezone_in_sec",String.valueOf(timeinsecs));
                    if (wallet_rb.isChecked()) {
                        if (wallet > challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", challengeAmount);
                                params.put("amount_paypal", "0");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", challengeAmount);
                            }

                        } else if (wallet < challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", paymentAmount);
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", challengeAmount);
                            }
                        } else if (wallet == challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", "0");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", challengeAmount);
                            }
                        }
                    } else {
                        params.put("amount_wallet", "0");
                        params.put("amount_paypal", challengeAmount);
                    }
                    Log.d("ACCEPTCHALLENGE:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void promotionPay() {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PROMOTION_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("PROMOTIONRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                    progressDialog.dismiss();
                                    JSONObject jobj = jsonObject.getJSONObject("data");
                                    String promotion_id = jobj.getString("promotion_id");
                                    Toast.makeText(ChallengePaymentActivity.this, "Promotion Transcation Successfully..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ChallengePaymentActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("promotion_id", promotion_id);
                    params.put("gateway_response", "");
                    params.put("amount", promotionAmount);
                    params.put("payment_id", "");
                    params.put("payment_mode", "WALLET");
                    if (wallet_rb.isChecked()) {
                        if (wallet > challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", promotionAmount);
                                params.put("amount_paypal", "0");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", promotionAmount);
                            }

                        } else if (wallet < challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", paymentAmount);
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", promotionAmount);
                            }
                        } else if (wallet == challenge) {
                            if (wallet_rb.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", "0");
                            } else if (paypal_rb.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", promotionAmount);
                            }
                        }
                    } else {
                        params.put("amount_wallet", "0");
                        params.put("amount_paypal", promotionAmount);
                    }
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendChallenge(final String user_id, final String amount, final String activity_id, final String challenge_id, final String challenge_goal, final String evaluation_factor, final String evaluation_factor_unit, final String type, final String span, final String date, final String location_name, final String location_lat, final String location_lng) {
        checkInternet = NetworkChecking.isConnected(ChallengePaymentActivity.this);
        if (checkInternet) {


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CHALLENGES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHALLENGE_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    Toast.makeText(ChallengePaymentActivity.this, "Challenge Request Sent", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ChallengePaymentActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(ChallengePaymentActivity.this, "Challenge Request Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(ChallengePaymentActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    if(type.equalsIgnoreCase("SPECIAL_EVENT"))
                     params.put("special_event_id", eventid);
                    params.put("activity_id", activity_id);
                    params.put("challenge_goal", challenge_goal);
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("challenge_type", type);
                    params.put("challenge_date", date);
                    params.put("evaluation_factor", evaluation_factor);
                    params.put("evaluation_factor_unit", evaluation_factor_unit);
                    params.put("amount", amount);
                    params.put("opponent_id", opponent_id);
                    params.put("opponent_type", opponent_type);
                    params.put("location_name", location_name);
                    params.put("location_lat", location_lat);
                    params.put("location_long", location_lng);
                    params.put("day_span", span);
                    params.put("gateway_response", "");
                    params.put("payment_id", "");
                    params.put("payment_mode", "WALLET");
                    params.put("amount_wallet", amount);
                    params.put("amount_paypal", "0");
                    params.put("timezone_in_sec", String.valueOf(timeinsecs));
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengePaymentActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(ChallengePaymentActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    private void onPressingBack()
    {
        if (activity.equals("Registration") || activity.equals("Login")){
            final Intent intent;
            intent = new Intent(ChallengePaymentActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ChallengePaymentActivity.this);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }else {
            final Intent intent;
            intent = new Intent(ChallengePaymentActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ChallengePaymentActivity.this);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }
}
