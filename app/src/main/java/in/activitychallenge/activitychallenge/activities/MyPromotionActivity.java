package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyPromotionAdapter;
import in.activitychallenge.activitychallenge.fragments.MyGroupPromotionFragment;
import in.activitychallenge.activitychallenge.fragments.MyPromotionFragment;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyPromotionActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    ViewPager view_pager;
    TabLayout tab;
    MyPromotionAdapter adapter;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_promotion);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        tab = (TabLayout) findViewById(R.id.tabLayout);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(view_pager);
        tab.setupWithViewPager(view_pager);
    }

    private void setupViewPager(ViewPager view_pager_suggest) {
        adapter = new MyPromotionAdapter(getSupportFragmentManager());
        adapter.addFrag(new MyPromotionFragment(), "Promotion");
        adapter.addFrag(new MyGroupPromotionFragment(), "Group Promotion");
        view_pager_suggest.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}

