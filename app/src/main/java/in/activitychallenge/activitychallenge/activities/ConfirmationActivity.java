package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;
import okhttp3.internal.Util;

public class ConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView username_txt, status, payment_id;
    Button next_btn;
    String user_name, user_type, paymentId, paymentStatus, activity, ref_txn_id = "", wallet_amount = "", token, challenge_id = "", location_name = "",
            location_lat = "",loc_lat = "",loc_lng = "", location_lng = "", device_id, walletAmount = "", promotion_id = "";
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String amount;
    String eventid="";
    String user_id = "", activity_id = "", challenge_goal = "", evaluation_factor = "", evaluation_factor_unit = "", type = "", span = "", date = "",
            opponent_id = "", opponent_type = "", member_id = "", member_user_type = "",group_id = "", group_user_type = "";
    int timeinsecs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        eventid= Utils.getInstance().getEventtype();
        progressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(ConfirmationActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        Bundle bundle = getIntent().getExtras();
        //amount = bundle.getString("PaymentAmount");
        activity = bundle.getString("activity");
        if (activity.equals("Registration")) {
            amount = bundle.getString("PaymentAmount");
        } else if (activity.equals("Login")) {
            amount = bundle.getString("PaymentAmount");
        } else if (activity.equals("EarnMoney")) {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            ref_txn_id = bundle.getString("ref_txn_id");
            wallet_amount = bundle.getString("wallet_amount");
            Log.d("CONFIRM", ref_txn_id + "\n" + wallet_amount);
        } else if (activity.equals("ChallengePayment")) {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            challenge_id = bundle.getString("challenge_id");
            location_name = bundle.getString("location_name");
            loc_lat = bundle.getString("location_lat");
            loc_lng = bundle.getString("location_long");
            walletAmount = bundle.getString("walletAmount");
        } else if (activity.equals("MyPromotionFragment")) {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            promotion_id = bundle.getString("promotion_id");
            walletAmount = bundle.getString("walletAmount");
        } else if (activity.equals("challengeto")) {
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            amount = bundle.getString("PaymentAmount");
            activity_id = bundle.getString("activity_id");
            challenge_id = bundle.getString("challenge_id");
            challenge_goal = bundle.getString("challenge_goal");
            evaluation_factor = bundle.getString("evaluation_factor");
            evaluation_factor_unit = bundle.getString("evaluation_factor_unit");
            type = bundle.getString("type");
            span = bundle.getString("span");
            date = bundle.getString("date");
            location_name = bundle.getString("location_name");
            location_lat = bundle.getString("location_lat");
            location_lng = bundle.getString("location_long");
            opponent_id = bundle.getString("opponent_id");
            opponent_type = bundle.getString("opponent_type");
            Log.d("values_of_challenge", user_id + " /// " + amount + " /// " + activity_id + " /// " + challenge_id + " /// " + challenge_goal + " /// " + evaluation_factor
                    + " /// " + evaluation_factor_unit + " /// " + type + " /// " + span + " /// " + date + " /// " + location_name + " /// " + location_lat
                    + " /// " + location_lng + " /// " + opponent_id + " /// " + opponent_type);
        } else if (activity.equals("MemberDetail")) {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            member_id = bundle.getString("member_id");
            member_user_type = bundle.getString("member_user_type");
            Log.d("MEMBERDETAIL+++++++++", amount);
        }
        else if (activity.equals("GroupDetail"))
        {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("group_id");
            group_user_type = bundle.getString("group_user_type");
            Log.d("GROUPDETAIL+++++++++", amount);
        }
        else if (activity.equals("MeAsaSponsorAPP"))
        {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
            Log.d("APPDETAIL+++++++++", amount);
        }
        else if (activity.equals("MeAsSponsorActivity"))
        {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
            Log.d("ACTIVITYDETAIL+++++++++", amount);
        }
        else if (activity.equals("MYPROFILE_ADDWALLET"))
        {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            user_type = userDetails.get(UserSessionManager.USER_TYPE);
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
            Log.d("PROFILEWALLET+++++++++", amount);
        }
        Log.d("CONPAYMENTDETAILS", bundle.toString());
        Intent intent = getIntent();
        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //get Time zone
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsecs = (tz.getOffset(cal.getTimeInMillis()))/1000;
    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException
    {
        username_txt = (TextView) findViewById(R.id.username_txt);
        username_txt.setText("Hi," + user_name);
        payment_id = (TextView) findViewById(R.id.payment_id);
        status = (TextView) findViewById(R.id.status);
        next_btn = (Button) findViewById(R.id.next_btn);
        next_btn.setOnClickListener(this);
        paymentId = jsonDetails.getString("id");
        paymentStatus = jsonDetails.getString("state");
        payment_id.setText(paymentId);
        status.setText("$ " + paymentAmount + " USD" + "\t" + "Payment Completed");
        Log.d("PAYMENTDETAILS", paymentId + "\n" + paymentStatus);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity.equals("Login"))
                {
                    initialPay();
                } else if (activity.equals("EarnMoney"))
                {
                    Intent intent = new Intent(getApplicationContext(), LotteryCodesActivity.class);
                    intent.putExtra("paymentId", paymentId);
                    intent.putExtra("ref_txn_id", ref_txn_id);
                    intent.putExtra("amount", amount);
                    intent.putExtra("wallet_amount", wallet_amount);
                    startActivity(intent);
                } else if (activity.equals("Registration")) {
                    initialPay();
                } else if (activity.equals("ChallengePayment")) {
                    acceptChallenge();
                } else if (activity.equals("challengeto")) {
                    // Toast.makeText(this, "Challenge To", Toast.LENGTH_SHORT).show();
                    sendChallenge(user_id, amount, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng);
                } else if (activity.equals("MyPromotionFragment")) {
                    Toast.makeText(getApplicationContext(), "PROMOTION PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    promotionPay();
                } else if (activity.equals("MemberDetail")) {
                    Toast.makeText(getApplicationContext(), "MEMBER SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    sponsorPay();
                }
                else if (activity.equals("GroupDetail"))
                {
                    Toast.makeText(getApplicationContext(), "GROUP SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    sponsorPay();
                }
                else if (activity.equals("MeAsaSponsorAPP"))
                {
                    Toast.makeText(getApplicationContext(), "APP SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    sponsorPay();
                }
                else if (activity.equals("MeAsSponsorActivity"))
                {
                    Toast.makeText(getApplicationContext(), "ACTIVITY SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    sponsorPay();
                }
                else if (activity.equals("MYPROFILE_ADDWALLET"))
                {
                    Toast.makeText(getApplicationContext(), "WALLET PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                    walletPay();
                }
            }
        }, 2000);

    }

    @Override
    public void onClick(View v) {
        if (v == next_btn) {
            if (activity.equals("Login")) {
                initialPay();
            } else if (activity.equals("EarnMoney")) {
                Intent intent = new Intent(getApplicationContext(), LotteryCodesActivity.class);
                intent.putExtra("paymentId", paymentId);
                intent.putExtra("ref_txn_id", ref_txn_id);
                intent.putExtra("amount", amount);
                intent.putExtra("wallet_amount", wallet_amount);
                startActivity(intent);
            } else if (activity.equals("Registration")) {
                initialPay();
            } else if (activity.equals("ChallengePayment")) {
                acceptChallenge();
            } else if (activity.equals("challengeto")) {
               // Toast.makeText(this, "Challenge To", Toast.LENGTH_SHORT).show();
                sendChallenge(user_id, amount, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng);
            } else if (activity.equals("MyPromotionFragment")) {
                Toast.makeText(this, "PROMOTION PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                promotionPay();
            } else if (activity.equals("MemberDetail")) {
                Toast.makeText(this, "MEMBER SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                sponsorPay();
            }else if (activity.equals("GroupDetail")){
                Toast.makeText(this, "GROUP SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                sponsorPay();
            }
            else if (activity.equals("MeAsaSponsorAPP")){
                Toast.makeText(this, "APP SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                sponsorPay();
            }
            else if (activity.equals("MeAsSponsorActivity")){
                Toast.makeText(this, "ACTIVITY SPONSOR PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                sponsorPay();
            }
            else if (activity.equals("MYPROFILE_ADDWALLET"))
            {
                Toast.makeText(getApplicationContext(), "WALLET PAY SUCCESS..!", Toast.LENGTH_SHORT).show();
                walletPay();
            }
        }
    }

    private void walletPay()
    {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if(checkInternet)
        {
            Log.d("WALLETURL",AppUrls.BASE_URL + AppUrls.USER_WALLET_PAY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.USER_WALLET_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("WALLETRESP", response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Amount Sent successfully.!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    Log.d("PAYWALLETPARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        }
        else
        {
            progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sponsorPay() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SPONSOR_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("SPONSORRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Sponsor Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (activity.equals("MemberDetail")){
                        params.put("to_user_id", member_id);
                        params.put("to_user_type", member_user_type);
                    }else if (activity.equals("GroupDetail")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", group_user_type);
                    }
                    else if (activity.equals("MeAsaSponsorAPP")){
                        params.put("to_user_id", "59def9dce25e4081ac0f1093");
                        params.put("to_user_type", "APP");
                    }
                    else if (activity.equals("MeAsSponsorActivity")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", "ACTIVITY");
                    }

                    params.put("gateway_response", paymentStatus);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    if (walletAmount.equals("0") || walletAmount.equals("")) {
                        params.put("payment_mode", "PAYPAL");
                    } else {
                        params.put("payment_mode", "WALLET_PAYPAL");
                    }
                    params.put("amount_wallet", walletAmount);
                    params.put("amount_paypal", amount);
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void promotionPay() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PROMOTION_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("PROMOTIONRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jobj = jsonObject.getJSONObject("data");
                                    String promotion_id = jobj.getString("promotion_id");
                                    Toast.makeText(ConfirmationActivity.this, "Promotion Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("promotion_id", promotion_id);
                    params.put("gateway_response", paymentStatus);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("amount_wallet", walletAmount);
                    params.put("amount_paypal", amount);
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void initialPay() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            Log.d("INTIALPAYURL",AppUrls.BASE_URL + AppUrls.INITIAL_PAYMENT);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.INITIAL_PAYMENT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("initialREPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jobj = jsonObject.getJSONObject("data");
                                    ref_txn_id = jobj.getString("ref_txn_id");
                                    String user_typ = jobj.getString("user_type");
                                    String user_id = jobj.getString("user_id");
                                    String first_name = jobj.getString("first_name");
                                    String last_name = jobj.getString("last_name");
                                    String email = jobj.getString("email");
                                    String country_code = jobj.getString("country_code");
                                    String mobile = jobj.getString("mobile");
                                    String status = jobj.getString("status");
                                    String provided_acc_info = jobj.getString("provided_acc_info");
                                    String username = first_name + " " + last_name;
                                    Log.d("", ref_txn_id + "" + user_typ + "" + username + "" + email + "" + country_code + "" + mobile + "" + status + "" + provided_acc_info);
                                    Toast.makeText(ConfirmationActivity.this, "Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    session.createUserLoginSession(token, username, email, mobile, status, user_typ,user_id,provided_acc_info);
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                }

                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Payment..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (paymentStatus.equals("approved")) {
                        params.put("txn_flag", "COMPLETED");
                    } else {
                        params.put("txn_flag", paymentStatus);
                    }
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);

                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void acceptChallenge() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_CHALLENGE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            Log.d("AcceptChallenge", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    // progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                // progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_id", challenge_id);
                    params.put("location_name", location_name);
                    params.put("location_lat", loc_lat);
                    params.put("location_long", loc_lng);
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("gateway_response", paymentStatus);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("amount_wallet", walletAmount);
                    params.put("amount_paypal", amount);
                    params.put("timezone_in_sec",String.valueOf(timeinsecs));

                    Log.d("ACCEPT:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendChallenge(final String user_id, final String amount, final String activity_id, final String challenge_id, final String challenge_goal, final String evaluation_factor, final String evaluation_factor_unit, final String type, final String span, final String date, final String location_name, final String location_lat, final String location_lng) {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CHALLENGES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHALLENGE_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(ConfirmationActivity.this, "Challenge Request Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    if(type.equalsIgnoreCase("SPECIAL_EVENT"))
                    params.put("special_event_id", eventid);
                    params.put("activity_id", activity_id);
                    params.put("challenge_goal", challenge_goal);
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("challenge_type", type);
                    params.put("challenge_date", date);
                    params.put("evaluation_factor", evaluation_factor);
                    params.put("evaluation_factor_unit", evaluation_factor_unit);
                    params.put("amount", amount);
                    params.put("opponent_id", opponent_id);
                    params.put("opponent_type", opponent_type);
                    params.put("location_name", location_name);
                    params.put("location_lat", location_lat);
                    params.put("location_long", location_lng);
                    params.put("day_span", span);
                    params.put("gateway_response", paymentStatus);
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("amount_wallet", "0");
                    params.put("amount_paypal", amount);
                    params.put("timezone_in_sec",String.valueOf(timeinsecs));
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ConfirmationActivity.this,MainActivity.class);
        startActivity(intent);
    }
}