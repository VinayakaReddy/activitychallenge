package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.EulaRulesFragment;
import in.activitychallenge.activitychallenge.fragments.FAQFragments;
import in.activitychallenge.activitychallenge.fragments.TermsAndCondition;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class FaqTermsConditionActivity extends AppCompatActivity  implements View.OnClickListener  {

    ImageView close;

    ViewPager view_pager_faq_termscondition;
    TabLayout tab;
    UserSessionManager session;
    String device_id, access_token, user_id,type;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    MyChllengesViewPagerAdapter faq_temsCodition_adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_terms_condition);

        Bundle b=getIntent().getExtras();

        type=b.getString("condition");

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SELOTTONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        tab = (TabLayout) findViewById(R.id.tabLayout_faq_termscondition);
        view_pager_faq_termscondition = (ViewPager) findViewById(R.id.view_pager_faq_termscondition);

        setupViewPager(view_pager_faq_termscondition);
        tab.setupWithViewPager(view_pager_faq_termscondition);

    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        faq_temsCodition_adapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());


        if(type.equals("NORMAL"))
        {
            view_pager_suggest.setCurrentItem(1);
            faq_temsCodition_adapter.addFrag(new FAQFragments(), "FAQ");
            faq_temsCodition_adapter.addFrag(new TermsAndCondition(), "Terms And Codnitions");
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
        }

        if(type.equals("TermsandConditions"))
        {
            view_pager_suggest.setCurrentItem(2);
            faq_temsCodition_adapter.addFrag(new TermsAndCondition(), "Terms And Codnitions");
            faq_temsCodition_adapter.addFrag(new FAQFragments(), "FAQ");
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
        }
        if(type.equals("EULA"))
        {
            view_pager_suggest.setCurrentItem(3);
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
            faq_temsCodition_adapter.addFrag(new FAQFragments(), "FAQ");
            faq_temsCodition_adapter.addFrag(new TermsAndCondition(), "Terms And Codnitions");

        }




        view_pager_suggest.setAdapter(faq_temsCodition_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_faq_termscondition) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);


                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }
    @Override
    public void onClick(View view)
    {
        if(view==close)
        {finish();}



    }
}
