package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class CongratulationsActivity extends Activity implements View.OnClickListener {

    ImageView close, share, cup_img;
    TextView toolbar_title,draw_scrtch_txt, congratulations_txt, one_won_txt,  receive_txt, prize_txt, notify_txt, you_won_txt, scratch_card_txt, scratch_txt;
    LinearLayout one_won_ll, both_won_ll;
    Typeface typeface;
    String got_prize, win_status,challng_id,is_group_admin,scratched_status;
    ProgressDialog pgd;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id,access_token,device_id ,user_type,fromwhere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SSDERGNDATA", user_id + "\n" + access_token + "\n" + device_id);

        pgd = new ProgressDialog(this);
        pgd.setMessage("Please wait......");
        pgd.setProgressStyle(R.style.DialogTheme);



        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(this);
        cup_img = (ImageView) findViewById(R.id.cup_img);

        Bundle b = new Bundle();
        got_prize = getIntent().getExtras().getString("amount");
        win_status = getIntent().getExtras().getString("win_status");
        challng_id = getIntent().getExtras().getString("challenge_id");
        is_group_admin = getIntent().getExtras().getString("is_group_admin");

        Log.d("PRIZE", got_prize + "//" + win_status+"///"+is_group_admin);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        congratulations_txt = (TextView) findViewById(R.id.congratulations_txt);
        one_won_txt = (TextView) findViewById(R.id.one_won_txt);
       receive_txt = (TextView) findViewById(R.id.receive_txt);
        prize_txt = (TextView) findViewById(R.id.prize_txt);
        notify_txt = (TextView) findViewById(R.id.notify_txt);
        you_won_txt = (TextView) findViewById(R.id.you_won_txt);
        scratch_card_txt = (TextView) findViewById(R.id.scratch_card_txt);
        scratch_txt = (TextView) findViewById(R.id.scratch_txt);
        draw_scrtch_txt = (TextView) findViewById(R.id.draw_scrtch_txt);
        draw_scrtch_txt.setOnClickListener(this);
        one_won_ll = (LinearLayout) findViewById(R.id.one_won_ll);
        both_won_ll = (LinearLayout) findViewById(R.id.both_won_ll);
        if (win_status.equals("WON"))
        {
            congratulations_txt.setText("Congratulations..!!");
            prize_txt.setText("$" + got_prize);
            one_won_txt.setText("You Won The Challenge");
           // draw_scrtch_txt.setVisibility(View.GONE);
        }
        else if(win_status.equals("LOSS"))
        {
            congratulations_txt.setText("Oops");
            one_won_txt.setText("You Lost The Challenge");
            one_won_ll.setVisibility(View.GONE);
          //  draw_scrtch_txt.setVisibility(View.GONE);
        }
        else
        {
            getStatusAmount();
            congratulations_txt.setText("Ooop's");
            one_won_txt.setText("Challenge is Draw");
            one_won_ll.setVisibility(View.GONE);
            draw_scrtch_txt.setVisibility(View.VISIBLE);
            fromwhere = getIntent().getExtras().getString("FROM_WHERE");

        }
      /*  if (getIntent().getExtras().getString("type").equals("GROUP")) {
            if (is_group_admin.equals("0")) {
                draw_scrtch_txt.setVisibility(View.GONE);
            } else {
                draw_scrtch_txt.setVisibility(View.VISIBLE);
            }
        }else {
            draw_scrtch_txt.setVisibility(View.VISIBLE);
        }*/

    }



    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
        if (v == share)
        {
            if(win_status.equals("WON"))
            {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I have won the amount  " +"$"+got_prize+ " in ActivityChallenge App. To stay active and earn money Why don't you join?. "+AppUrls.SHARE_APP_URL);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
            else
            {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I am inviting you in ActivityChallenge App. To stay active and earn money Why don't you join?. "+AppUrls.SHARE_APP_URL);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }



        }
        if(v==draw_scrtch_txt)
        {

           // if (getIntent().getExtras().getString("type").equals("GROUP"))
          //  {
               if (is_group_admin.equals("0"))
              {
               // draw_scrtch_txt.setVisibility(View.GONE);
                 // Log.d("DDDDDDD","DDDDDDD");
                  Toast.makeText(CongratulationsActivity.this,"Only Admin Can Scratch the Card",Toast.LENGTH_LONG).show();
              }
              else
               {
                draw_scrtch_txt.setVisibility(View.VISIBLE);
                Intent intdraw = new Intent(CongratulationsActivity.this, CongratsScratchActivity.class);
                intdraw.putExtra("challenge_id", challng_id);
                intdraw.putExtra("is_group_admin", is_group_admin);
                startActivityForResult(intdraw, 1);
                ;
               }
          //  }
           /* else
                {
                *//*Intent intdraw = new Intent(CongratulationsActivity.this, CongratsScratchActivity.class);
                intdraw.putExtra("challenge_id", challng_id);
                intdraw.putExtra("is_group_admin", is_group_admin);
                startActivityForResult(intdraw, 1);*//*
            }*/
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
              String  amount = data.getStringExtra("AMOUNTSEND");

                draw_scrtch_txt.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + amount + "</b>"));
                draw_scrtch_txt.setClickable(false);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public  void getStatusAmount()
    {
        checkInternet = NetworkChecking.isConnected(CongratulationsActivity.this);
        if(checkInternet)
        {
            String url= AppUrls.BASE_URL+AppUrls.TIE_SCRATCH_CARD_TIE_STATUS+"?challenge_id="+challng_id+"&user_id="+user_id;
            Log.d("congSCRURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("CONGSCRTRESP:",response);

                        String status = jsonObject.getString("response_code");
                        if (status.equals("10100"))
                        {

                            JSONObject jsonObjData = jsonObject.getJSONObject("data");

                            scratched_status=jsonObjData.getString("is_scratched");
                            String amount=jsonObjData.getString("amount");
                            Log.d("STATUS",scratched_status);
                            if(scratched_status.equals("1"))
                            {
                                draw_scrtch_txt.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + amount + "</b>"));
                                draw_scrtch_txt.setClickable(false);
                            }
                            else
                            {
                                draw_scrtch_txt.setText("Get Scratch Card");

                            }

                        }
                        if(status.equals("10200"))
                        {
                            Toast.makeText(CongratulationsActivity.this,"Invalid input...!",Toast.LENGTH_LONG).show();
                        }
                        if(status.equals("10300"))
                        {
                          //  Toast.makeText(CongratulationsActivity.this,"Scratch card not created, Yet..!",Toast.LENGTH_LONG).show();
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(CongratulationsActivity.this);
            requestQueue.add(stringRequest);

        }
        else
        {
            Toast.makeText(CongratulationsActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();
        }
    }
}
