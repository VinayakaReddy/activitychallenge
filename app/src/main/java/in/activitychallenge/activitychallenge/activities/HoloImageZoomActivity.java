package in.activitychallenge.activitychallenge.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import in.activitychallenge.activitychallenge.R;

public class HoloImageZoomActivity extends AppCompatActivity implements View.OnClickListener
{
  ImageView holoimage,close;
  Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holo_image_zoom);

        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);
        holoimage=(ImageView)findViewById(R.id.holoimage);

      String imagePath = getIntent().getStringExtra("HOLOPATH");

        Picasso.with(HoloImageZoomActivity.this)
                .load(imagePath)
                .placeholder(R.drawable.no_image_found)
                .into(holoimage);


    }

    @Override
    public void onClick(View view)
    {
     if(view==close)
     {
         finish();
     }
    }
}
