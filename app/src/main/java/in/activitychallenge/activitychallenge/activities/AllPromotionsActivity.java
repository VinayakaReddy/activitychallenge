package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.activitychallenge.activitychallenge.BannerLayout;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.AllPromotionsAdapter;
import in.activitychallenge.activitychallenge.adapter.WebBannerAdapter;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllPromotionsActivity extends AppCompatActivity {
    RecyclerView promo_recycler;
    AllPromotionsAdapter promoAdap;
    ArrayList<AllPromotionsModel> promoList = new ArrayList<AllPromotionsModel>();
    LinearLayoutManager lMang;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    UserSessionManager userSessionManager;
    ImageView close;
    BannerLayout bannerVertical;
    List<String> list;
    WebBannerAdapter webBannerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_promotions);
        bannerVertical =  findViewById(R.id.recycler_ver);
        list = new ArrayList<>();
        userSessionManager = new UserSessionManager(AllPromotionsActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(AllPromotionsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        promo_recycler = (RecyclerView) findViewById(R.id.promo_recycler);
        promoAdap = new AllPromotionsAdapter(promoList, AllPromotionsActivity.this, R.layout.row_all_promotions);
        lMang = new LinearLayoutManager(AllPromotionsActivity.this);
        promo_recycler.setNestedScrollingEnabled(false);
        promo_recycler.setLayoutManager(lMang);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getAds();
        webBannerAdapter=new WebBannerAdapter(AllPromotionsActivity.this,promoList);
        webBannerAdapter.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(promoList.get(position).getEntity_id().equals(user_id))
                {

                }
                else {
                    if (promoList.get(position).getEntity_type().equals("USER")) {
                        Intent intent = new Intent(AllPromotionsActivity.this, MemberDetailActivity.class);
                        intent.putExtra("MEMBER_ID", promoList.get(position).getEntity_id());
                        intent.putExtra("MEMBER_NAME", promoList.get(position).getEntity_name());
                        intent.putExtra("member_user_type", promoList.get(position).getEntity_type());
                        startActivity(intent);
                    } else if (promoList.get(position).getEntity_type().equals("GROUP")) {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AllPromotionsActivity.this, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoList.get(position).getEntity_id());
                        intent.putExtra("grp_name", promoList.get(position).getEntity_name());
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoList.get(position).getEntity_type());
                        intent.putExtra("grp_admin_id", promoList.get(position).getAdmin_id());

                        startActivity(intent);
                    } else {

                    }
                }
              //  Toast.makeText(AllPromotionsActivity.this, " " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAds() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AllPromotionsModel rhm = new AllPromotionsModel();
                                        JSONObject itemArray = jsonArray.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setBanner_path(AppUrls.BASE_IMAGE_URL + itemArray.getString("banner_path"));
                                        rhm.setStatus(itemArray.getString("status"));
                                        rhm.setEntity_id(itemArray.getString("entity_id"));
                                        rhm.setEntity_type(itemArray.getString("entity_type"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setEntity_name(itemArray.getString("entity_name"));
                                        list.add(AppUrls.BASE_IMAGE_URL + itemArray.getString("banner_path"));
                                        promoList.add(rhm);
                                    }
                                    promo_recycler.setAdapter(promoAdap);
                                    bannerVertical.setAdapter(webBannerAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllPromotionsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
