package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.MySponsoringHistoryHolder;
import in.activitychallenge.activitychallenge.models.MySponsoringsModel;
import in.activitychallenge.activitychallenge.models.ReportHistoryModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MySponsorings extends AppCompatActivity implements View.OnClickListener {

    ImageView close, unav;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id,user_type;
    UserSessionManager session;
    ArrayList<MySponsoringsModel> rList = new ArrayList<MySponsoringsModel>();
    RecyclerView report_history_recycler;
    LinearLayoutManager lLayMan;
    SponsoringAdapter rHisAdap;
    ArrayList<ReportHistoryModel> rhList = new ArrayList<ReportHistoryModel>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sponsoring);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        report_history_recycler = (RecyclerView) findViewById(R.id.report_history_recycler);
        rHisAdap = new SponsoringAdapter(MySponsorings.this, rList, R.layout.row_my_sponsorings);
        lLayMan = new LinearLayoutManager(this);
        report_history_recycler.setNestedScrollingEnabled(false);
        report_history_recycler.setLayoutManager(lLayMan);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        unav = (ImageView) findViewById(R.id.unav);

        getReportHistory();
    }

    private void getReportHistory() {
        checkInternet = NetworkChecking.isConnected(MySponsorings.this);
        if (checkInternet)
        {
            Log.d("UUUURLLL", AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS +user_id+ AppUrls.USER_TYPE + user_type);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS + user_id+AppUrls.USER_TYPE+user_type,
          //(static url)  StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS + "5a2946bacb1b391d2c67e996" + AppUrls.USER_TYPE + "USER",
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("HISTORY", AppUrls.BASE_URL + "user/feedback?id=" + user_id);
                            Log.d("REPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    unav.setVisibility(View.GONE);
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        MySponsoringsModel rhm = new MySponsoringsModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setProfile_pic(AppUrls.BASE_IMAGE_URL + itemArray.getString("profile_pic"));
                                        rhm.setStatus(itemArray.getString("status"));
                                        rhm.setDate(itemArray.getString("date"));
                                        rhm.setAmount(itemArray.getString("amount"));
                                        rhm.setTo_sponsor_type(itemArray.getString("to_sponsor_type"));
                                        rList.add(rhm);
                                    }
                                    report_history_recycler.setAdapter(rHisAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MySponsorings.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                    unav.setVisibility(View.VISIBLE);
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(MySponsorings.this, "No data Found", Toast.LENGTH_SHORT).show();
                                    unav.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MySponsorings.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(MySponsorings.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public class SponsoringAdapter extends RecyclerView.Adapter<MySponsoringHistoryHolder> {
        MySponsorings context;
        int resource;
        ArrayList<MySponsoringsModel> rhm;
        LayoutInflater lInfla;

        public SponsoringAdapter(MySponsorings context, ArrayList<MySponsoringsModel> rhm, int resource) {
            this.context = context;
            this.resource = resource;
            this.rhm = rhm;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MySponsoringHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MySponsoringHistoryHolder rHol = new MySponsoringHistoryHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MySponsoringHistoryHolder holder, int position) {

            String sponsertypeto=rhm.get(position).getTo_sponsor_type();
            holder.name.setText(rhm.get(position).getName());

            Log.d("PPPTAH",rhm.get(position).getProfile_pic());
            Picasso.with(MySponsorings.this)
                    .load(rhm.get(position).getProfile_pic())
                    .placeholder(R.drawable.dummy_user_profile)
                    .into(holder.sponcering_image);

            holder.status.setText(rhm.get(position).getStatus());
            holder.amount.setText("$"+rhm.get(position).getAmount());

            String timedate=parseDateToddMMyyyy(rhm.get(position).getDate());
            holder.date.setText(Html.fromHtml(timedate));
            holder.sponser_type_to.setText(sponsertypeto);

            if(sponsertypeto.equals("USER"))
            {
            //  holder.sponser_type_to.setText("Type : USER");
                Picasso.with(MySponsorings.this)
                        .load(rhm.get(position).getProfile_pic())
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(holder.sponcering_image);
            }
            else
            {
              //  holder.sponser_type_to.setText("Type : GROUP");
                Picasso.with(MySponsorings.this)
                        .load(rhm.get(position).getProfile_pic())
                        .placeholder(R.drawable.dummy_group_profile)
                        .into(holder.sponcering_image);
            }



            if (rhm.get(position).getStatus().equals("ACCEPT")) {
                holder.status.setTextColor(Color.GREEN);
            } else if (rhm.get(position).getStatus().equals("PENDING")) {
                holder.status.setTextColor(Color.GRAY);
            } else if (rhm.get(position).getStatus().equals("REJECT")) {
                holder.status.setTextColor(Color.RED);
            }
        }

        @Override
        public int getItemCount() {
            return rhm.size();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
