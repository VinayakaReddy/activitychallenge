package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.filters.MyGroupFilterFilterList;
import in.activitychallenge.activitychallenge.holder.MyGroupHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;
import in.activitychallenge.activitychallenge.models.MyGroupModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllMoreGroupFromMDActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String device_id, access_token, user_id, user_type, member_id;
    LinearLayoutManager lManager;
    RecyclerView allmore_my_group_recylerview;
    AllMoreGroupAdapter allMoreGroupAdapter;
    ArrayList<MyGroupModel> allmoremyGlist = new ArrayList<MyGroupModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_more_group_from_md);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id);
        Bundle bundle = getIntent().getExtras();
        member_id = bundle.getString("MEMBERID");
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        allmore_my_group_recylerview = (RecyclerView) findViewById(R.id.allmore_my_group_recylerview);
        allmore_my_group_recylerview.setNestedScrollingEnabled(false);
        allMoreGroupAdapter = new AllMoreGroupAdapter(AllMoreGroupFromMDActivity.this, allmoremyGlist, R.layout.row_mygroup);
        lManager = new LinearLayoutManager(this);
        allmore_my_group_recylerview.setLayoutManager(lManager);
        getMoreMyGroups();
    }

    private void getMoreMyGroups() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + "group/user?user_id=" + member_id + "&user_type=" + user_type;
            Log.d("MOREUUUUU", url);

            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/user?user_id="+member_id+"&user_type="+ user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GeMOREGroupUrlResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                  //  Toast.makeText(AllMoreGroupFromMDActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();

                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        MyGroupModel rhm = new MyGroupModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setGroup_pic(itemArray.getString("group_pic"));
                                        rhm.setGroup_members(itemArray.getString("group_members") + " " + "members");
                                        rhm.setTotal_win(itemArray.getString("total_win"));
                                        rhm.setTotal_loss(itemArray.getString("total_loss"));
                                        rhm.setOverall_rank(itemArray.getString("overall_rank"));
                                        rhm.setWinning_per(itemArray.getString("winning_per") + " " + "%");
                                        rhm.setImg1(itemArray.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        rhm.setImg2(itemArray.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        rhm.setImg3(itemArray.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        rhm.setImg4(itemArray.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        rhm.setImg5(itemArray.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        allmoremyGlist.add(rhm);
                                    }
                                    allmore_my_group_recylerview.setAdapter(allMoreGroupAdapter);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(AllMoreGroupFromMDActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(AllMoreGroupFromMDActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MyMOREPP_Header", "HEADER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {
            pprogressDialog.cancel();
            Toast.makeText(AllMoreGroupFromMDActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    public class AllMoreGroupAdapter extends RecyclerView.Adapter<MyGroupHolder>//implements Filterable
    {
        AllMoreGroupFromMDActivity context;
        LayoutInflater lInfla;
        int resource;
        public ArrayList<MyGroupModel> allmoremyGlistAdapter;
        ArrayList<MyGroupModel> filterlist;
        MyGroupFilterFilterList filter;

        public AllMoreGroupAdapter(AllMoreGroupFromMDActivity context, ArrayList<MyGroupModel> allmoremyGlistAdapter, int resource) {
            this.context = context;
            this.allmoremyGlistAdapter = allmoremyGlistAdapter;
            this.resource = resource;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = lInfla.inflate(resource, null);
            MyGroupHolder slh = new MyGroupHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(MyGroupHolder holder, final int position)
        {
            holder.take_challeng.setVisibility(View.GONE);
            String str = allmoremyGlistAdapter.get(position).getName();
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            String admin_id = allmoremyGlistAdapter.get(position).getAdmin_id();
            Log.d("dettttt", admin_id + "///////" + user_id);
            if (admin_id.equals(user_id)) {
                holder.take_challeng.setVisibility(View.VISIBLE);
                holder.ll_take.setVisibility(View.VISIBLE);
            } else {
                holder.take_challeng.setVisibility(View.GONE);
                holder.ll_take.setVisibility(View.GONE);
            }
            holder.grp_name.setText(converted_string);

            holder.grp_memb_cnt.setText(allmoremyGlistAdapter.get(position).getGroup_members());
            holder.grp_lost_cnt.setText(allmoremyGlistAdapter.get(position).getTotal_loss());
            holder.grp_won_cnt.setText(allmoremyGlistAdapter.get(position).getTotal_win());
            holder.grp_percent.setText(allmoremyGlistAdapter.get(position).getWinning_per());
            holder.grp_rank.setText(allmoremyGlistAdapter.get(position).getOverall_rank());
            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .resize(70, 70)
                    .into(holder.grp_img);

            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getImg1())
                    .into(holder.grp_img_1);
            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getImg2())
                    .into(holder.grp_img_2);
            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getImg3())
                    .into(holder.grp_img_3);
            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getImg4())
                    .into(holder.grp_img_4);
            Picasso.with(AllMoreGroupFromMDActivity.this)
                    .load(AppUrls.BASE_IMAGE_URL + allmoremyGlistAdapter.get(position).getImg5())
                    .into(holder.grp_img_5);


            holder.setItemClickListener(new MyGroupItemClickListener() {
                @Override
                public void onItemClick(View view, int layoutPosition)
                {
                   /* Intent ii = new Intent(AllMoreGroupFromMDActivity.this, GroupDetailActivity.class);
                    ii.putExtra("grp_id", allmoremyGlistAdapter.get(position).getId());
                    ii.putExtra("grp_name", allmoremyGlistAdapter.get(position).getName());
                    ii.putExtra("grp_admin_id", allmoremyGlistAdapter.get(position).getAdmin_id());
                    ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_INTERNAL");
                    Log.d("sdfdsf", allmoremyGlistAdapter.get(position).getId() + "///" + allmoremyGlistAdapter.get(position).getName() + "///" + allmoremyGlistAdapter.get(position).getAdmin_id());
                    startActivity(ii);*/
                }
            });
            holder.take_challeng.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, GroupMainActivity.class);
                    intent.putExtra("user_id", allmoremyGlistAdapter.get(position).getId());
                    intent.putExtra("user_type", "GROUP");
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return allmoremyGlistAdapter.size();
        }
    }
}
