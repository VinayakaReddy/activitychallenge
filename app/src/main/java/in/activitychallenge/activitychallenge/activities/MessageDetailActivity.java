package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MessagesDetailAdapter;
import in.activitychallenge.activitychallenge.dbhelper.UserMessageDetailDB;
import in.activitychallenge.activitychallenge.models.MessagesDetailModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MessageDetailActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close, profile_pic;
    TextView mesg_detail_toolbar_title;
    Typeface typeface, typeface2;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    EditText mesagetext_edt;
    ImageView send_mesage;
    String user_id, user_type, token, device_id, m_fromname, m_fromid, m_fromtype, m_profile_pic;
    RecyclerView recycler_list_chat;
    MessagesDetailAdapter messageDetailAdapter;
    LinearLayoutManager layoutManager;
    ArrayList<MessagesDetailModel> msgDetailModalList = new ArrayList<MessagesDetailModel>();
    UserMessageDetailDB userMessageDetailDB;
    ContentValues values ;
    String FROM_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        Bundle bundle = getIntent().getExtras();
        m_fromid = bundle.getString("FROM_ID");
        m_fromname = bundle.getString("FROM_NAME");
        m_fromtype = bundle.getString("FROM_TYPE");
        m_profile_pic = bundle.getString("FROM_PROFILEPIC");
        FROM_TYPE = bundle.getString("FROM_TYPE");//Abusing

        mesg_detail_toolbar_title = (TextView) findViewById(R.id.mesg_detail_toolbar_title);
        mesg_detail_toolbar_title.setText(m_fromname);
        userMessageDetailDB = new UserMessageDetailDB(MessageDetailActivity.this);
        values = new ContentValues();
        checkInternet = NetworkChecking.isConnected(this);
        Log.d("M_DETAIL", m_fromid + "//" + m_fromname + "//" + m_fromtype + "//" + m_profile_pic);
        userSessionManager = new UserSessionManager(MessageDetailActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("M_DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);
        progressDialog = new ProgressDialog(MessageDetailActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        profile_pic = (ImageView) findViewById(R.id.profile_pic);
        profile_pic.setOnClickListener(this);
        send_mesage = (ImageView) findViewById(R.id.send_mesage);
        send_mesage.setOnClickListener(this);
        mesagetext_edt = (EditText) findViewById(R.id.mesagetext_edt);
        mesagetext_edt.setOnClickListener(this);
        recycler_list_chat = (RecyclerView) findViewById(R.id.recycler_list_chat);
        messageDetailAdapter = new MessagesDetailAdapter(msgDetailModalList, MessageDetailActivity.this, R.layout.row_message_chat_list,FROM_TYPE);
        layoutManager = new LinearLayoutManager(this);
        recycler_list_chat.setNestedScrollingEnabled(false);
        recycler_list_chat.setLayoutManager(layoutManager);
        recycler_list_chat.setItemAnimator(null);
        layoutManager.setStackFromEnd(true);
        Picasso.with(MessageDetailActivity.this)
                .load(m_profile_pic)
                .placeholder(R.drawable.dummy_user_profile)
                .into(profile_pic);

        getMessageDetail();
    }

    private void getMessageDetail()
    {

       // userMessageDetailDB.emptyMessage();
        if (checkInternet)
        {
            Log.d("MSGDETILURL", AppUrls.BASE_URL + AppUrls.NOTIFICATION_CONVERSATION + "?entity_id=" + m_fromid + "&entity_type=" + m_fromtype);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.NOTIFICATION_CONVERSATION + "?entity_id=" + m_fromid + "&entity_type=" + m_fromtype,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("DETAILMESSAGRESP", response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MessagesDetailModel msgDetail = new MessagesDetailModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        msgDetail.setId(jsonObject1.getString("id"));
                                        msgDetail.setFrom_id(jsonObject1.getString("from_id"));
                                        msgDetail.setFrom_name(jsonObject1.getString("from_name"));
                                        msgDetail.setRecipient(jsonObject1.getString("recipient"));
                                        msgDetail.setMsg(jsonObject1.getString("msg"));
                                        msgDetail.setIs_read(jsonObject1.getString("is_read"));
                                        msgDetail.setSent_on_txt(jsonObject1.getString("sent_on_txt"));
                                        msgDetail.setSent_on(jsonObject1.getString("sent_on"));

                                        msgDetailModalList.add(msgDetail);
                                         //Insert into Local DB
                                      /*  JSONObject jsonObjectDB = jsonArray.getJSONObject(i);
                                        values.put(UserMessageDetailDB.ID, jsonObjectDB.getString("id"));
                                        values.put(UserMessageDetailDB.FROM_ID, jsonObjectDB.getString("from_id"));
                                        values.put(UserMessageDetailDB.FROM_NAME, jsonObjectDB.getString("from_name"));
                                        values.put(UserMessageDetailDB.RECIPIENT, jsonObjectDB.getString("recipient"));
                                        values.put(UserMessageDetailDB.MSG, jsonObjectDB.getString("msg"));
                                        values.put(UserMessageDetailDB.IS_READ, jsonObjectDB.getString("is_read"));
                                        values.put(UserMessageDetailDB.SET_ON_TEXT, jsonObjectDB.getString("sent_on_txt"));
                                        values.put(UserMessageDetailDB.SET_ON, jsonObjectDB.getString("sent_on"));
                                        Log.d("INSERT",""+values);
                                        userMessageDetailDB.addUserChatList(values);*/


                                    }
                                    //userMessageDetailList();

                                    recycler_list_chat.setAdapter(messageDetailAdapter);
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    Toast.makeText(MessageDetailActivity.this, "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {
                                    Toast.makeText(MessageDetailActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MessageDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
           // userMessageDetailList();
            Toast.makeText(MessageDetailActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();


        }
    }

    private void userMessageDetailList()
    {
     // msgDetailModalList.clear();
        Log.d("INNN","INNN");
        List<String> msg_content = userMessageDetailDB.getMsgName();
        Log.d("DATAAAAA",""+msg_content);
        if (msg_content.size() > 0)
        {
            List<String> msgtime = userMessageDetailDB.getMessageTimeStamp();
            List<String> msg_receptint = userMessageDetailDB.getReceipent();
            for (int i = 0; i < msg_content.size(); i++)
            {
                MessagesDetailModel msgDetail = new MessagesDetailModel();
                msgDetail.setMsg(msg_content.get(i));
                msgDetail.setSent_on_txt(msgtime.get(i));
                msgDetail.setRecipient(msg_receptint.get(i));
                msgDetailModalList.add(msgDetail);
            }
            recycler_list_chat.setAdapter(messageDetailAdapter);

        } else {
            //progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(final View view) {
        if (view == close) {
            finish();
        }
        if (view == send_mesage) {
            msgDetailModalList.clear();
            checkInternet = NetworkChecking.isConnected(this);
            if (validate()) {
                if (checkInternet) {

                    Log.d("SENDURL:", AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_SEND_MSG);
                    final String msg_text = mesagetext_edt.getText().toString().trim();
                    StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_SEND_MSG, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MESRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                  //  Toast.makeText(getApplicationContext(), "Message sent successfully..", Toast.LENGTH_SHORT).show();
                                    hideSoftKeyboard(view);
                                    // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                                  /*  messageDetailAdapter.notifyDataSetChanged();
                                    recycler_list_chat.setAdapter(messageDetailAdapter);*/
                                    getMessageDetail();
                                    mesagetext_edt.setText("");
                                }
                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {
                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("from_entity_id", user_id);
                            params.put("from_entity_type", user_type);
                            params.put("to_entity_id", m_fromid);
                            params.put("to_entity_type", m_fromtype);
                            params.put("msg", msg_text);
                            Log.d("SEMDMSGARAM:", params.toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("MSGHEAED", headers.toString());
                            return headers;
                        }
                    };

                    forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(MessageDetailActivity.this);
                    requestQueue.add(forgetReq);
                } else {
                    Toast.makeText(MessageDetailActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private boolean validate() {


        boolean result = true;
        int flag = 0;
        String name = mesagetext_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 1)) {
            mesagetext_edt.setError("Minimum 3 characters required");
            result = false;
        }
        return result;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
