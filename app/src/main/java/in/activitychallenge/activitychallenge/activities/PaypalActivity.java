package in.activitychallenge.activitychallenge.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.PayPalConfig;

public class PaypalActivity extends AppCompatActivity implements View.OnClickListener {

    TextView amount_txt;
    Button pay_btn;
    String paymentAmount, activity, ref_txn_id = "", wallet_amount = "", challenge_id = "", promotion_id = "", location_name = "", location_lat = "",
            location_lng = "", walletAmount = "";
    public static final int PAYPAL_REQUEST_CODE = 123;
    private boolean checkInternet;
    String user_id = "", activity_id = "", challenge_goal = "", evaluation_factor = "", evaluation_factor_unit = "", type = "", span = "", date = "",
            opponent_id = "", opponent_type = "", member_id = "", member_user_type = "", user_type = "",group_id = "", group_user_type = "";

    //Paypal Configuration Object
   // private static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    //Live Credentials   // if we want credit card pay then remove acceptCreditCards(false) ;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION).clientId(PayPalConfig.PAYPAL_CLIENT_ID).acceptCreditCards(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal);

        amount_txt = (TextView) findViewById(R.id.amount_txt);
        Bundle bundle = getIntent().getExtras();
        activity = bundle.getString("activity");
        if (activity.equals("Registration")) {
            paymentAmount = bundle.getString("paymentAmount");
            Log.d("PMPMPMPM",""+paymentAmount);
        } else if (activity.equals("Login")) {
            paymentAmount = bundle.getString("paymentAmount");
        } else if (activity.equals("EarnMoney")) {
            ref_txn_id = bundle.getString("ref_txn_id");
            wallet_amount = bundle.getString("wallet_amount");
            paymentAmount = bundle.getString("paymentAmount");
        } else if (activity.equals("MyPromotionFragment")) {
            promotion_id = bundle.getString("promotion_id");
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");

            Log.d("CheckLOGPayment", paymentAmount+"////"+walletAmount);
        } else if (activity.equals("ChallengePayment")) {
            challenge_id = bundle.getString("challenge_id");
            location_name = bundle.getString("location_name");
            location_lat = bundle.getString("location_lat");
            location_lng = bundle.getString("location_long");
            walletAmount = bundle.getString("walletAmount");
            paymentAmount = bundle.getString("paymentAmount");
            if (walletAmount.equals("0")) {
                amount_txt.setText("$ " + paymentAmount + " USD");
            } else if (paymentAmount.equals("0")) {
                amount_txt.setText("$ " + walletAmount + " USD");
            }
            Log.d("CHECKINGLOCATION",location_lat+"\n"+location_lng+"\n"+location_name);
        } else if (activity.equals("challengeto")) {
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            paymentAmount = bundle.getString("paymentAmount");
            activity_id = bundle.getString("activity_id");
            challenge_id = bundle.getString("challenge_id");
            challenge_goal = bundle.getString("challenge_goal");
            evaluation_factor = bundle.getString("evaluation_factor");
            evaluation_factor_unit = bundle.getString("evaluation_factor_unit");
            type = bundle.getString("type");
            span = bundle.getString("span");
            date = bundle.getString("date");
            location_name = bundle.getString("location_name");
            location_lat = bundle.getString("location_lat");
            location_lng = bundle.getString("location_long");
            opponent_id = bundle.getString("opponent_id");
            opponent_type = bundle.getString("opponent_type");
            Log.d("values_of_challenge", user_id + " /// " + paymentAmount + " /// " + activity_id + " /// " + challenge_id + " /// " + challenge_goal + " /// " + evaluation_factor
                    + " /// " + evaluation_factor_unit + " /// " + type + " /// " + span + " /// " + date + " /// " + location_name + " /// " + location_lat
                    + " /// " + location_lng + " /// " + opponent_id + " /// " + opponent_type);

            //amount_txt.setText("$ "+paymentAmount+" USD");
        } else if (activity.equals("MemberDetail")) {
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            member_id = bundle.getString("member_id");
            member_user_type = bundle.getString("member_user_type");

        }else if (activity.equals("GroupDetail")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("group_id");
            group_user_type = bundle.getString("group_user_type");
        }
        else if (activity.equals("MeAsaSponsorAPP")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }
        else if (activity.equals("MYPROFILE_ADDWALLET")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }
        else if (activity.equals("MeAsSponsorActivity")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }
        else if (activity.equals("Sponsor_special_event")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }

        Log.d("PAYMENTDETAILS", bundle.toString());

        amount_txt.setText("$ " + paymentAmount + " USD");
        //Log.d("CHECKAMOUNT",paymentAmount);
        pay_btn = (Button) findViewById(R.id.pay_btn);
        pay_btn.setOnClickListener(this);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK)
            {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null)
                {
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        Intent intent = new Intent(this, ConfirmationActivity.class);
                        intent.putExtra("PaymentDetails", paymentDetails);
                        intent.putExtra("activity", activity);
                        if (activity.equals("Registration")) {
                            intent.putExtra("PaymentAmount", paymentAmount);
                            Log.d("CHECKAMOUNT", paymentAmount);
                        } else if (activity.equals("Login")) {
                            intent.putExtra("PaymentAmount", paymentAmount);
                        } else if (activity.equals("EarnMoney")) {
                            intent.putExtra("ref_txn_id", ref_txn_id);
                            intent.putExtra("wallet_amount", wallet_amount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                        } else if (activity.equals("MyPromotionFragment")) {
                            intent.putExtra("promotion_id", promotion_id);
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                        } else if (activity.equals("ChallengePayment")) {
                            intent.putExtra("challenge_id", challenge_id);
                            intent.putExtra("location_name", location_name);
                            intent.putExtra("location_lat", location_lat);
                            intent.putExtra("location_long", location_lng);
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                        } else if (activity.equals("challengeto")) {
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type", user_type);
                            intent.putExtra("activity_id", activity_id);
                            intent.putExtra("challenge_id", challenge_id);
                            intent.putExtra("challenge_goal", challenge_goal);
                            intent.putExtra("evaluation_factor", evaluation_factor);
                            intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                            intent.putExtra("type", type);
                            intent.putExtra("span", span);
                            intent.putExtra("date", date);
                            intent.putExtra("location_name", location_name);
                            intent.putExtra("location_lat", location_lat);
                            intent.putExtra("location_long", location_lng);
                            intent.putExtra("opponent_id", opponent_id);
                            intent.putExtra("opponent_type", opponent_type);
                            intent.putExtra("PaymentAmount", paymentAmount);
                        } else if (activity.equals("MemberDetail")) {
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                        }else if (activity.equals("GroupDetail")){
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("group_user_type", group_user_type);
                        }
                        else if (activity.equals("MeAsaSponsorAPP")){
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("member_id", group_id);
                            intent.putExtra("member_user_type", group_user_type);
                        }
                        else if (activity.equals("MeAsSponsorActivity")){
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("member_id", group_id);
                            intent.putExtra("member_user_type", group_user_type);
                        }
                        else if (activity.equals("Sponsor_special_event")){
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("member_id", group_id);
                            intent.putExtra("member_user_type", group_user_type);
                        }
                        else if (activity.equals("MYPROFILE_ADDWALLET")){
                            intent.putExtra("walletAmount", walletAmount);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("member_id", group_id);
                            intent.putExtra("member_user_type", group_user_type);
                            Log.d("PPPPPPPP","PPPPPPPP");
                        }
                        startActivity(intent);
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == pay_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getPayment();
            } else {
                Toast.makeText(PaypalActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getPayment()
    {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Activity Challenge Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    private void onPressingBack() {
        if (activity.equals("Registration") || activity.equals("Login")){
            final Intent intent;
            intent = new Intent(PaypalActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(PaypalActivity.this);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }else {
            final Intent intent;
            intent = new Intent(PaypalActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(PaypalActivity.this);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

}

