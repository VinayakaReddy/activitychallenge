package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.MemberPictureCountStatusHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.PictureMemberCountClickListner;
import in.activitychallenge.activitychallenge.models.MemCountStatusChalngModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.PictureMemberCountDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class PictureMemberCountChallengeActivity extends AppCompatActivity  implements  View.OnClickListener

{
    private boolean checkInternet;
    UserSessionManager session;
    ImageView close;
    String user_id,token,device_id,challenge_id,group_id;
    ProgressDialog progressDialog;
    RecyclerView recycler_picture_count_members;
    MemberCountChllengePictureAdapter memberpictureCountChllengeAdapter;
    ArrayList<MemCountStatusChalngModel> memberPictureCountChllengeMoldelList = new ArrayList<MemCountStatusChalngModel>();
    RecyclerView.LayoutManager layoutManager;
    PictureMemberCountDB pictureMemberCountDB;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_member_count_challenge);
        //

        session = new UserSessionManager(PictureMemberCountChallengeActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pictureMemberCountDB=new PictureMemberCountDB(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        group_id = bundle.getString("group_id");

        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);

        recycler_picture_count_members=(RecyclerView) findViewById(R.id.recycler_picture_count_members);

        memberpictureCountChllengeAdapter = new MemberCountChllengePictureAdapter(memberPictureCountChllengeMoldelList, PictureMemberCountChallengeActivity.this, R.layout.row_member_hollow_count_status);
        layoutManager = new LinearLayoutManager(this);
        recycler_picture_count_members.setNestedScrollingEnabled(false);
        recycler_picture_count_members.setLayoutManager(layoutManager);

        getMemberCountPictureDetail();
    }

    private void getMemberCountPictureDetail()
    {
        memberPictureCountChllengeMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url_all_members = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS+"?user_id="+user_id+"&challenge_id="+challenge_id+"&group_id="+group_id;
            Log.d("MEMBERSURLCOUNT", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    Log.d("COUNTMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            JSONObject jdata = jobcode.getJSONObject("data") ;
                            JSONArray jsonArray = jdata.getJSONArray("challenge_details");

                            ContentValues values=new ContentValues();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MemCountStatusChalngModel allmembercount = new MemCountStatusChalngModel();
                                allmembercount.setUser_id(jdataobj.getString("user_id"));
                                allmembercount.setName(jdataobj.getString("name"));
                                allmembercount.setEvaluation_factor_value(AppUrls.BASE_IMAGE_URL+jdataobj.getString("evaluation_factor_value"));
                                allmembercount.setEvaluation_factor_unit(jdataobj.getString("evaluation_factor_unit"));
                                allmembercount.setEvaluation_factor(jdataobj.getString("evaluation_factor"));
                                allmembercount.setCompleted_on_txt(jdataobj.getString("completed_on_txt"));
                                allmembercount.setCompleted_on(jdataobj.getString("completed_on"));
                                 values.put(PictureMemberCountDB.NAME,jdataobj.getString("name"));
                                 values.put(PictureMemberCountDB.EVALUATION_FACTOR_VALUE,AppUrls.BASE_IMAGE_URL+jdataobj.getString("evaluation_factor_value"));
                                 pictureMemberCountDB.addPictureMembers(values);
                                memberPictureCountChllengeMoldelList.add(allmembercount);
                            }
                            recycler_picture_count_members.setAdapter(memberpictureCountChllengeAdapter);

                        }
                        if (response_code.equals("10200")) {
                            progressDialog.dismiss();
                            Toast.makeText(PictureMemberCountChallengeActivity.this, "No data Found...!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(PictureMemberCountChallengeActivity.this);
            requestQueue.add(req_members);
        } else {
            getPictureMembers();
            //Toast.makeText(PictureMemberCountChallengeActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getPictureMembers(){
        List<String> names = pictureMemberCountDB.getNames();
        List<String> evaluation = pictureMemberCountDB.getEvaluationFactorValues();
        if(names.size()>0){
            for(int i=0;i<names.size();i++){
                MemCountStatusChalngModel allmembercount = new MemCountStatusChalngModel();
                allmembercount.setName(names.get(i));
                allmembercount.setEvaluation_factor_value(evaluation.get(i));
                memberPictureCountChllengeMoldelList.add(allmembercount);
            }
            recycler_picture_count_members.setAdapter(memberpictureCountChllengeAdapter);

        }


    }
    @Override
    public void onClick(View view)
    {
        if(view==close)
        {finish();}
    }


    private class MemberCountChllengePictureAdapter extends RecyclerView.Adapter<MemberPictureCountStatusHolder> {
        PictureMemberCountChallengeActivity context;
        int resource;
        ArrayList<MemCountStatusChalngModel> rhm;
        LayoutInflater lInfla;
        String datetime;

        public MemberCountChllengePictureAdapter( ArrayList<MemCountStatusChalngModel> rhm,PictureMemberCountChallengeActivity context, int resource) {
            this.context = context;
            this.resource = resource;
            this.rhm = rhm;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MemberPictureCountStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MemberPictureCountStatusHolder rHol = new MemberPictureCountStatusHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MemberPictureCountStatusHolder holder, int position) {


            holder.namee.setText(rhm.get(position).getName());
          // holder.imgEval_faceto.setText(rhm.get(position).getEvaluation_factor_value());

            Picasso.with(context)
                    .load(rhm.get(position).getEvaluation_factor_value())
                    .placeholder(R.drawable.no_image_found)
                    .into(holder.imgEval_faceto);

            holder.setItemClickListener(new PictureMemberCountClickListner() {
                @Override
                public void onItemClick(View view, int layoutPosition)
                {
                    Intent top=new Intent(PictureMemberCountChallengeActivity.this,HoloImageZoomActivity.class);
                    top.putExtra("HOLOPATH",rhm.get(layoutPosition).getEvaluation_factor_value());
                    startActivity(top);
                }
            });

        }

        @Override
        public int getItemCount() {
            return rhm.size();
        }


    }

}
