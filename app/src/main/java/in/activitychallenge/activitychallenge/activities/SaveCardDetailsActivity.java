package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SaveCardDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title;
    EditText card_name_edt, card_number_edt, cc_year_edt, cc_month_edt, cvv_edt;
    Button submit_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String card_id, user_id, access_token, device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_card_details);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(SaveCardDetailsActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        card_name_edt = (EditText) findViewById(R.id.card_name_edt);
        card_number_edt = (EditText) findViewById(R.id.card_number_edt);
        cc_year_edt = (EditText) findViewById(R.id.cc_year_edt);
        cc_month_edt = (EditText) findViewById(R.id.cc_month_edt);
        cvv_edt = (EditText) findViewById(R.id.cvv_edt);
        submit_btn = (Button) findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
        if (v == submit_btn) {
            checkInternet = NetworkChecking.isConnected(SaveCardDetailsActivity.this);
            if (checkInternet) {
                if (validate()) {
                    final String name = card_name_edt.getText().toString().trim();
                    final String card_number = card_number_edt.getText().toString().trim();
                    final String cc_year = cc_year_edt.getText().toString().trim();
                    final String cc_month = cc_month_edt.getText().toString().trim();
                    final String cvv = cvv_edt.getText().toString().trim();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SAVE_CARD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();
                                    Log.d("SAVE CARD", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String success = jsonObject.getString("success");
                                        String message = jsonObject.getString("message");
                                        String response_code = jsonObject.getString("response_code");
                                        if (response_code.equals("10100")) {
                                            progressDialog.dismiss();
                                            JSONObject jobj = jsonObject.getJSONObject("data");
                                            card_id = jobj.getString("id");
                                          //  Toast.makeText(SaveCardDetailsActivity.this, "Card Details Saved Securely..!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(SaveCardDetailsActivity.this, SavedCardDetailsActivity.class);
                                            //intent.putExtra("card_id",card_id);
                                            startActivity(intent);
                                        }
                                        if (response_code.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(SaveCardDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        progressDialog.cancel();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.cancel();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", access_token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                            return headers;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("acc_type", "CC");
                            params.put("acc_no", card_number);
                            params.put("cc_year", cc_year);
                            params.put("cc_month", cc_month);
                            params.put("cc_name_on_card", name);
                            params.put("cc_cvv", cvv);
                            Log.d("SAVEPARAMS", "PARMS" + params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(SaveCardDetailsActivity.this);
                    requestQueue.add(stringRequest);

                } else {
                    Toast.makeText(this, "Invalid Card Details..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                progressDialog.cancel();
                Toast.makeText(SaveCardDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate() {

        boolean result = true;
        String name = card_name_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            card_name_edt.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                card_name_edt.setError("Special characters not allowed");
                result = false;
            } else {
                card_name_edt.setError(null);
            }
        }
        String card_number = card_number_edt.getText().toString().trim();
        if ((card_number.length() != 16)) {
            card_number_edt.setError("Invalid Card Number");
            result = false;
        } else {
            card_number_edt.setError(null);
        }
        String year = cc_year_edt.getText().toString().trim();
        if ((year.length() != 4)) {
            cc_year_edt.setError("Invalid Year");
            result = false;
        } else {
            cc_year_edt.setError(null);
        }
        String month = cc_month_edt.getText().toString().trim();
        if ((month.length() != 2)) {
            cc_month_edt.setError("Invalid Month");
            result = false;
        } else {
            cc_month_edt.setError(null);
        }
        String cvv = cvv_edt.getText().toString().trim();
        if ((cvv.length() != 3)) {
            cvv_edt.setError("Invalid CVV Number");
            result = false;
        } else {
            cvv_edt.setError(null);
        }
        return result;
    }
}
