package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.SpecialEventAdapter;
import in.activitychallenge.activitychallenge.models.SpecialEventModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SpecialEvent extends AppCompatActivity implements View.OnClickListener{
    RecyclerView specialevent_recycleview;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    SpecialEventAdapter eventAdapter;
    ImageView no_events_img;

    ArrayList<SpecialEventModel> specialEventModels=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        specialevent_recycleview = (RecyclerView) findViewById(R.id.special_event_recycleview);
        no_events_img=(ImageView)findViewById(R.id.no_events_img);
        ImageView close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("UserDetails", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        eventAdapter = new SpecialEventAdapter(specialEventModels, SpecialEvent.this, R.layout.special_event_row);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        specialevent_recycleview.setNestedScrollingEnabled(false);
        specialevent_recycleview.setLayoutManager(layoutManager);

        getSpecialEvents();
    }

    //show Earn activity user active ness or not
    private void getSpecialEvents() {
        checkInternet = NetworkChecking.isConnected(SpecialEvent.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_SPECIAL_EVENTS+user_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            //  pprogressDialog.dismiss();

                            Log.d("Specialevent", AppUrls.BASE_URL + AppUrls.GET_SPECIAL_EVENTS+user_id);
                            Log.d("Events", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);
                                        SpecialEventModel eventModel = new SpecialEventModel();
                                        eventModel.setId(jsonObject1.getString("id"));
                                        eventModel.setEventName(jsonObject1.getString("name"));
                                        eventModel.setEventDescription(jsonObject1.getString("description"));
                                        eventModel.setFollowstatus(jsonObject1.getInt("allow_follow"));
                                        eventModel.setChalengestatus(jsonObject1.getInt("take_challenge"));
                                        eventModel.setRoadmap(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("roadmap"));

                                        eventModel.setUserfollowed(jsonObject1.getInt("has_user_followed"));
                                        eventModel.setStart_on(jsonObject1.getString("start_on"));
                                        eventModel.setEnd_on(jsonObject1.getString("end_on"));
                                        eventModel.setNo_of_days(jsonObject1.getInt("no_of_days"));
                                        JSONArray tmp=jsonObject1.getJSONArray("location");
                                        Log.v("JsonArry",">>>"+jsonObject1.getJSONArray("location"));
                                         eventModel.setLocationArr(tmp);


                                        specialEventModels.add(eventModel);
                                    }
                                     specialevent_recycleview.setAdapter(eventAdapter);
                                    if (jarray.length() == 0) {
                                        no_events_img.setVisibility(View.VISIBLE);
                                    }


                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    no_events_img.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SpecialEvent.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(SpecialEvent.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.close){
            finish();
        }
    }
}
