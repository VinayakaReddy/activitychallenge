package in.activitychallenge.activitychallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.BannerLayout;
import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.ActivityPriceAdapter;
import in.activitychallenge.activitychallenge.adapter.WebBannerAdapter;
import in.activitychallenge.activitychallenge.models.ActivityPriceModel;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ActivityPrice extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    Typeface typeface, typeface_bold;
    ActivityPriceAdapter activityAdapter;
    ArrayList<ActivityPriceModel> activityModels = new ArrayList<ActivityPriceModel>();
    private RecyclerView activities_recyclerview;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    private boolean checkInternet;
    String id = "", user_id = "", token = "", device_id = "";
    UserSessionManager session;
    String specialeventId;
    ////////////////////////////////////
    ImageView user_profile_image, sponsor_iv, challenge_iv, mesage_iv, banner_image;
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text,more;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    String activity_id = "", challenge_id = "", challenge_goal = "", evaluation_factor = "", evaluation_factor_unit = "", type = "", span = "", date = "", location_name = "", location_lat = "", location_lng = "", main_user_id = "", main_user_type = "";
    /////////////////////////
    BannerLayout bannerVertical;
    WebBannerAdapter webBannerAdapter;
    ArrayList<AllPromotionsModel> promoList = new ArrayList<AllPromotionsModel>();
    int eventFlag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);
        bannerVertical =  findViewById(R.id.recycler_ver);
        main_user_id = getIntent().getExtras().getString("user_id");
        main_user_type = getIntent().getExtras().getString("user_type");
        activity_id = getIntent().getExtras().getString("activity_id");
        challenge_id = getIntent().getExtras().getString("challenge_id");

        challenge_goal = getIntent().getExtras().getString("challenge_goal");
        evaluation_factor = getIntent().getExtras().getString("evaluation_factor");
        evaluation_factor_unit = getIntent().getExtras().getString("evaluation_factor_unit");
        type = getIntent().getExtras().getString("type");
        eventFlag=getIntent().getIntExtra("eventflag",0);
        if(eventFlag==1)
           specialeventId=getIntent().getStringExtra("specialeventid");
        span = getIntent().getExtras().getString("span");
        date = getIntent().getExtras().getString("date");
        location_name = getIntent().getExtras().getString("location_name");
        location_lat = getIntent().getExtras().getString("location_lat");
        location_lng = getIntent().getExtras().getString("location_lng");
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        sponsor_iv = (ImageView) findViewById(R.id.sponsor_iv);
        challenge_iv = (ImageView) findViewById(R.id.challenge_iv);
        mesage_iv = (ImageView) findViewById(R.id.mesage_iv);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        more = (TextView) findViewById(R.id.more);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        session = new UserSessionManager(ActivityPrice.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        close = (ImageView) findViewById(R.id.close);

        close.setOnClickListener(this);
        activities_recyclerview = (RecyclerView) findViewById(R.id.activity_price_recyclerview);
        activities_recyclerview.setHasFixedSize(true);
        activityAdapter = new ActivityPriceAdapter(activityModels, ActivityPrice.this, R.layout.row_activity_price, activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng, main_user_id, main_user_type,eventFlag,specialeventId);
        gridLayoutManager = new GridLayoutManager(this, 2);
        layoutManager = new LinearLayoutManager(this);
        activities_recyclerview.setLayoutManager(gridLayoutManager);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(getApplicationContext());
                if (checkInternet) {
                Intent more = new Intent(ActivityPrice.this, AllPromotionsActivity.class);
                startActivity(more);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                }
            }
        });
        getActivityPrices();
        getCount();
        getAds();
        webBannerAdapter=new WebBannerAdapter(ActivityPrice.this,promoList);
        webBannerAdapter.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(promoList.get(position).getEntity_id().equals(user_id))
                {

                }
                else {
                    if (promoList.get(position).getEntity_type().equals("USER")) {
                        Intent intent = new Intent(ActivityPrice.this, MemberDetailActivity.class);
                        intent.putExtra("MEMBER_ID", promoList.get(position).getEntity_id());
                        intent.putExtra("MEMBER_NAME", promoList.get(position).getEntity_name());
                        intent.putExtra("member_user_type", promoList.get(position).getEntity_type());
                        startActivity(intent);
                    } else if (promoList.get(position).getEntity_type().equals("GROUP")) {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ActivityPrice.this, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoList.get(position).getEntity_id());
                        intent.putExtra("grp_name", promoList.get(position).getEntity_name());
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoList.get(position).getEntity_type());
                        intent.putExtra("grp_admin_id", promoList.get(position).getAdmin_id());

                        startActivity(intent);
                    } else {

                    }
                }
                //  Toast.makeText(AllPromotionsActivity.this, " " + position, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getActivityPrices() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            activityModels.clear();

            String url = AppUrls.BASE_URL + AppUrls.PRICE + getIntent().getExtras().getString("activity_id") + AppUrls.TYPE + getIntent().getExtras().getString("type") + AppUrls.ID + getIntent().getExtras().getString("challenge_id");
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        String price = jsonArray.getString(i);

                                        ActivityPriceModel am = new ActivityPriceModel();
                                        am.setPrice(price);
                                        activityModels.add(am);
                                        Log.d("fjbvsdfkvsdf", activityModels.toString());

                                    }

                                    activities_recyclerview.setAdapter(activityAdapter);


                                }
                                if (status.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ActivityPrice.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(ActivityPrice.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(ActivityPrice.this, SponsorRequestActivity.class);
            startActivity(sponsor);

        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(ActivityPrice.this, ChallengeRequestActivity.class);
            startActivity(chellenge);

        }
        if (view == linear_messges) {
            Intent msg = new Intent(ActivityPrice.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");
            startActivity(msg);

        }
    }
    private void getAds() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("PROMOTIONBANNERRESPONSE", response);
                                int count = jsonObject.getInt("count");
                                if (count > 1) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.VISIBLE);
                                }
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AllPromotionsModel rhm = new AllPromotionsModel();
                                        JSONObject itemArray = jsonArray.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setBanner_path(AppUrls.BASE_IMAGE_URL + itemArray.getString("banner_path"));
                                        rhm.setStatus(itemArray.getString("status"));
                                        rhm.setEntity_id(itemArray.getString("entity_id"));
                                        rhm.setEntity_type(itemArray.getString("entity_type"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setEntity_name(itemArray.getString("entity_name"));
                                        promoList.add(rhm);
                                    }

                                    bannerVertical.setAdapter(webBannerAdapter);
                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray1.getJSONObject(0);
                                    String image = AppUrls.BASE_IMAGE_URL + jobj.getString("banner_path");
                                    final String banner_user_id = jobj.getString("entity_id");
                                    final String banner_user_type = jobj.getString("entity_type");
                                    final String banner_status = jobj.getString("status");
                                    final String banner_user_name = jobj.getString("entity_name");
                                    final String admin_id = jobj.getString("admin_id");

                                    if(banner_status.equals("PAID"))
                                    {
                                        if(banner_user_id.equals(user_id))
                                        {

                                        }
                                        else
                                        {
                                            banner_image.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view)
                                                {
                                                    if (banner_user_type.equals("USER"))
                                                    {
                                                        Intent intent = new Intent(ActivityPrice.this, MemberDetailActivity.class);
                                                        intent.putExtra("MEMBER_ID",banner_user_id);
                                                        intent.putExtra("MEMBER_NAME",banner_user_name);
                                                        intent.putExtra("member_user_type",banner_user_type);
                                                        startActivity(intent);
                                                    }
                                                    else if(banner_user_type.equals("GROUP")) {
                                                        Intent intent = new Intent(ActivityPrice.this, GroupDetailActivity.class);
                                                        intent.putExtra("grp_id",banner_user_id);
                                                        intent.putExtra("MEMBER_NAME",banner_user_name);
                                                        intent.putExtra("GROUP_CONVERSATION_TYPE",banner_user_type);
                                                        intent.putExtra("grp_admin_id",admin_id);
                                                        startActivity(intent);
                                                        //  Toast.makeText(ActivityPrice.this, "Group", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else
                                                    {}
                                                }
                                            });
                                        }
                                    }
                                    else
                                        {

                                        }

                                    Picasso.with(ActivityPrice.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(banner_image);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ActivityPrice.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
