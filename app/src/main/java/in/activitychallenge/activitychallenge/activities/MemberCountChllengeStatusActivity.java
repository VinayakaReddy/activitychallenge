package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.MemberCountStatusHolder;
import in.activitychallenge.activitychallenge.models.MemCountStatusChalngModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MemberCountChllengeStatusActivity extends AppCompatActivity implements View.OnClickListener
{
    private boolean checkInternet;
    UserSessionManager session;
    ImageView close;
    String user_id,token,device_id,challenge_id,group_id;
    ProgressDialog progressDialog;
    RecyclerView recycler_count_members;
    MemberCountChllengeAdapter memberCountChllengeAdapter;
    ArrayList<MemCountStatusChalngModel> memberCountChllengeMoldelList = new ArrayList<MemCountStatusChalngModel>();
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_count_chllenge_status);

        session = new UserSessionManager(MemberCountChllengeStatusActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        group_id = bundle.getString("group_id");

        close=(ImageView)findViewById(R.id.close);
        close.setOnClickListener(this);

        recycler_count_members=(RecyclerView) findViewById(R.id.recycler_count_members);

        memberCountChllengeAdapter = new MemberCountChllengeAdapter(memberCountChllengeMoldelList, MemberCountChllengeStatusActivity.this, R.layout.row_member_count_status);
        layoutManager = new LinearLayoutManager(this);
        recycler_count_members.setNestedScrollingEnabled(false);
        recycler_count_members.setLayoutManager(layoutManager);

        getMemberCountStatusDetail();
    }

    private void getMemberCountStatusDetail()
    {
        memberCountChllengeMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url_all_members = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS+"?user_id="+user_id+"&challenge_id="+challenge_id+"&group_id="+group_id;
            Log.d("MEMBERSURLCOUNT", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    Log.d("COUNTMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {

                            JSONObject jdata = jobcode.getJSONObject("data") ;
                            JSONArray jsonArray = jdata.getJSONArray("challenge_details");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MemCountStatusChalngModel allmembercount = new MemCountStatusChalngModel();
                                allmembercount.setUser_id(jdataobj.getString("user_id"));
                                allmembercount.setName(jdataobj.getString("name"));
                                allmembercount.setEvaluation_factor_value(jdataobj.getString("evaluation_factor_value"));
                                allmembercount.setEvaluation_factor_unit(jdataobj.getString("evaluation_factor_unit"));
                                allmembercount.setEvaluation_factor(jdataobj.getString("evaluation_factor"));
                                allmembercount.setCompleted_on_txt(jdataobj.getString("completed_on_txt"));
                                allmembercount.setCompleted_on(jdataobj.getString("completed_on"));

                                memberCountChllengeMoldelList.add(allmembercount);
                            }
                            recycler_count_members.setAdapter(memberCountChllengeAdapter);

                        }
                        if (response_code.equals("10200")) {
                            progressDialog.dismiss();
                            Toast.makeText(MemberCountChllengeStatusActivity.this, "No data Found...!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberCountChllengeStatusActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberCountChllengeStatusActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view)
    {
      if(view==close)
      {finish();}
    }

    private class MemberCountChllengeAdapter extends RecyclerView.Adapter<MemberCountStatusHolder> {
        MemberCountChllengeStatusActivity context;
        int resource;
        ArrayList<MemCountStatusChalngModel> rhm;
        LayoutInflater lInfla;
        String datetime;

        public MemberCountChllengeAdapter( ArrayList<MemCountStatusChalngModel> rhm,MemberCountChllengeStatusActivity context, int resource) {
            this.context = context;
            this.resource = resource;
            this.rhm = rhm;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MemberCountStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MemberCountStatusHolder rHol = new MemberCountStatusHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MemberCountStatusHolder holder, int position) {


            holder.namee.setText(rhm.get(position).getName());
            holder.evalution.setText("Completed Goal : "+rhm.get(position).getEvaluation_factor_value());



        }

        @Override
        public int getItemCount() {
            return rhm.size();
        }


    }

}
