package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    TextView forget_pass_txt;
    Typeface typeface, typeface2;
    TextInputLayout mobile_til;
    ImageView forget_btn;
    EditText mobile_edt;
    RadioGroup mobile_email_radio_group;
    RadioButton mobile_radio, email_radio, genderStatus;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    String country_code, sendVia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_password);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        pprogressDialog = new ProgressDialog(ForgetPasswordActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        country_code = userDetails.get(UserSessionManager.COUNTRY_CODE);
        forget_pass_txt = (TextView) findViewById(R.id.forget_pass_txt);
        forget_pass_txt.setTypeface(typeface2);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        mobile_til.setTypeface(typeface);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(typeface);
        mobile_edt.setInputType(InputType.TYPE_CLASS_NUMBER);
        mobile_edt.setOnClickListener(this);
        forget_btn = (ImageView) findViewById(R.id.forget_btn);
        forget_btn.setOnClickListener(this);
        mobile_email_radio_group = (RadioGroup) findViewById(R.id.mobile_email_radio_group);
        mobile_radio = (RadioButton) findViewById(R.id.mobile_radio);
        email_radio = (RadioButton) findViewById(R.id.email_radio);
        mobile_email_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.mobile_radio:
                        mobile_edt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case R.id.email_radio:
                        mobile_edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        break;
                }
            }

        });
    }

    @Override
    public void onClick(View view) {

        if (view == forget_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                Log.d("FORGETURL:", AppUrls.BASE_URL + AppUrls.FORGET_PASSWORD);
                if (mobile_radio.isChecked() || email_radio.isChecked()) {
                    genderStatus = (RadioButton) findViewById(mobile_email_radio_group.getCheckedRadioButtonId());
                    if (genderStatus.getId() == mobile_radio.getId()) {
                        sendVia = "Mobile";
                    }
                    if (genderStatus.getId() == email_radio.getId()) {
                        sendVia = "Email";
                    }
                }

                StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FORGET_PASSWORD, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("FORGETRESP", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {
                                pprogressDialog.dismiss();
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                Intent i = new Intent(ForgetPasswordActivity.this, ForgetPassOtpActivity.class);
                                startActivity(i);
                            }

                            if (successResponceCode.equals("10200")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10300")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "User does not Exist..", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Unable to Send OTP..", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pprogressDialog.cancel();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();

                        Log.d("SENDVIA", sendVia);
                        if (sendVia.equals("Mobile"))
                        {
                            params.put("country_code", country_code);
                            params.put("mobile", mobile_edt.getText().toString());
                            Log.d("FORGETPARAmobile:", params.toString());
                        }
                        if (sendVia.equals("Email"))
                        {
                            params.put("email", mobile_edt.getText().toString());
                            Log.d("FORGETPARAemail:", params.toString());
                        }
                        Log.d("FORGETPARAM:", params.toString());
                        return params;

                    }
                };
                forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(ForgetPasswordActivity.this);
                requestQueue.add(forgetReq);

            } else {
                Toast.makeText(ForgetPasswordActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean validate() {

        boolean result = true;
        int flag = 0;
        String MOBILE_REGEX = "^[789]\\d{9}$";
        String mobile = mobile_edt.getText().toString().trim();
        if ((mobile == null || mobile.equals("") || mobile.length() < 6)) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setErrorEnabled(false);
        }
        return result;
    }

}
