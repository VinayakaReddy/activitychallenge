package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.CountryAdapter;
import in.activitychallenge.activitychallenge.models.CountriesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;

public class CountryListActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView country_recyclerview;
    SearchView country_search;
    CountryAdapter countryAdapter;
    ImageView close;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    LinearLayoutManager layoutManager;
    ArrayList<CountriesModel> countryListModel = new ArrayList<CountriesModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        pprogressDialog = new ProgressDialog(CountryListActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        country_search = (SearchView) findViewById(R.id.country_search);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        country_recyclerview = (RecyclerView) findViewById(R.id.country_recyclerview);
        countryAdapter = new CountryAdapter(countryListModel, CountryListActivity.this, R.layout.row_country_list);
        layoutManager = new LinearLayoutManager(this);
        country_recyclerview.setNestedScrollingEnabled(false);
        country_recyclerview.setLayoutManager(layoutManager);

        getCountryData();

        country_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country_search.setIconified(false);
            }
        });
        country_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                countryAdapter.getFilter().filter(query);
                countryAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    private void getCountryData() {
        // countryListModel.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("COUNTRYURL", AppUrls.BASE_URL + AppUrls.GET_COUNTRIES);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_COUNTRIES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("COUNTRYRESP", response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    pprogressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CountriesModel country = new CountriesModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        country.setId(jsonObject1.getString("id"));
                                        country.setName(jsonObject1.getString("name"));
                                        country.setCode(jsonObject1.getString("code"));
                                        country.setImg_path(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("img_path"));
                                        countryListModel.add(country);
                                    }
                                    country_recyclerview.setAdapter(countryAdapter);

                                }

                                if (responceCode.equals("12786")) {
                                    pprogressDialog.cancel();

                                    Toast.makeText(CountryListActivity.this, "No States Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(CountryListActivity.this, "All Fields Required...!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                pprogressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });


            RequestQueue requestQueue = Volley.newRequestQueue(CountryListActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
