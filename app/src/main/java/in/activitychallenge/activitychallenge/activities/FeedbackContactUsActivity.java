package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.ContactusFragment;
import in.activitychallenge.activitychallenge.fragments.FeedBackFragment;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class FeedbackContactUsActivity extends AppCompatActivity implements View.OnClickListener  {
//
    ImageView close;
    TextView report_h_butt;
    ViewPager view_pager_feedback_contact;
    TabLayout tab;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    MyChllengesViewPagerAdapter feedback_contactus_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_contact_us);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SELOTTONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        report_h_butt = (TextView) findViewById(R.id.report_h_butt);
        report_h_butt.setOnClickListener(this);
        tab = (TabLayout) findViewById(R.id.tabLayout_feedback_contact);
        view_pager_feedback_contact = (ViewPager) findViewById(R.id.view_pager_feedback_contact);

        setupViewPager(view_pager_feedback_contact);
        tab.setupWithViewPager(view_pager_feedback_contact);
    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        feedback_contactus_adapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());

        feedback_contactus_adapter.addFrag(new FeedBackFragment(), "FeedBack");
        feedback_contactus_adapter.addFrag(new ContactusFragment(), "Contact Us");


        view_pager_suggest.setAdapter(feedback_contactus_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_feedback_contact) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 1) {

                            report_h_butt.setVisibility(View.GONE);
                        }
                        else if(tab.getPosition() == 0)
                        {
                            report_h_butt.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
        if(view==close)
        {finish();}

        if(view==report_h_butt)
        {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent i = new Intent(FeedbackContactUsActivity.this, ReportHistory.class);
                startActivity(i);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
       }

    }
}
