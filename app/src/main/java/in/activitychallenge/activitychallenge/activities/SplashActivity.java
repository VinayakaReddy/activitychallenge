package in.activitychallenge.activitychallenge.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    UserSessionManager session;
    private boolean checkInternet;
    String user_id = "", mobile, payment_info, email, user_type;
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        if (getIntent() != null) {
           /* if (getIntent().getStringExtra("msg") != null) {
                String message = getIntent().getStringExtra("msg");
                Log.v("Message", message);
            }*/
            if (getIntent() != null && getIntent().getExtras() != null) {
                Bundle extras = getIntent().getExtras();
                String someData = extras.getString("msg");
                if (someData != null && someData.length() > 0)
                    Log.v("Message", someData);
            }
        }

        checkInternet = NetworkChecking.isConnected(this);
        videoView = (VideoView) findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/"
                + R.raw.splash_video;
        Uri uri = Uri.parse(path);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();
        session = new UserSessionManager(SplashActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        //   Log.d("daffdsadfsa",user_id);

        if (checkLocationPermission()) {
            if (checkInternet)
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        login_status();

                        /*if (session.isUserLoggedIn()) {
                       *//* Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                        startActivity(i);*//*
                            login_status();
                            //    Toast.makeText(getApplicationContext(), "INSIDE SPLS FIRST TIME", Toast.LENGTH_SHORT).show();
                            //finish();
                        } else {
                            //   Log.d("dfsa",user_id);
                            if (user_id == null || user_id.equals("null")) {
//                            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
//                            startActivity(i);
                                login_status();
                            } else {
                                // Toast.makeText(getApplicationContext(), "INSIDE LOUGOUT", Toast.LENGTH_SHORT).show();
                                login_status();
                            }
                        }*/
                    }
                }, SPLASH_TIME_OUT);
            } else {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra("USERTYPE", user_type);
                startActivity(intent);
            }


        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int telephonePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int sms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (telephonePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (sms != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                Log.d("DFDFDSFDSAFF", grantResults.toString());
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (session.checkIsFirstTime() != false) {
                                   /* Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                    startActivity(i);*/
                                    login_status();
                                    // finish();
                                } else {
                                    if (user_id.equals("") || user_id.equals("null")) {
                                       /* Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                        startActivity(i);*/
                                        login_status();
                                    } else {
                                        // Toast.makeText(getApplicationContext(), "INSIDE LOUGOUT", Toast.LENGTH_SHORT).show();
                                        login_status();
                                    }

                                }

                            }
                        }, SPLASH_TIME_OUT);
                    } else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK("External Storage And Location Permissions Required For This App", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkLocationPermission();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "Goto Settings And Enable Permissions", Toast.LENGTH_SHORT).show();
                            checkLocationPermission();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", okListener).create().show();
    }

    public void login_status() {

       /* if (checkInternet)
        {*/
        if (user_id == null || user_id.equals("")) {
            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
            startActivity(i);
            finish();
        } else {
            if (checkInternet) {//   Toast.makeText(getApplicationContext(), "INSIDE SERVICE", Toast.LENGTH_SHORT).show();
                Log.d("LOGINSTATUSURL", AppUrls.BASE_URL + AppUrls.LOGIN_SESION_STATUS + "?user_id=" + user_id);

                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LOGIN_SESION_STATUS + "?user_id=" + user_id,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("LOGINSTATUS >>> ", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("response_code");
                                    if (successResponceCode.equals("10100")) {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String userid = jsonObject1.getString("user_id");
                                        String user_type = jsonObject1.getString("user_type");
                                        String email = jsonObject1.getString("email");
                                        String country_code = jsonObject1.getString("country_code");
                                        String mobile = jsonObject1.getString("mobile");
                                        String status = jsonObject1.getString("status");
                                        String is_mobile_verified = jsonObject1.getString("is_mobile_verified");
                                        String payment_info = jsonObject1.getString("payment_info");
                                        // session.createUserType(user_type);
                                        if (is_mobile_verified.equals("false")) {
                                            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                            startActivity(i);
                                            finish();
                                        } else if (status.equals("DEACTIVATED")) {
                                            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                            startActivity(i);
                                            finish();
                                        } else if (status.equals("BLOCKED_ADMIN")) {
                                            Toast.makeText(getApplicationContext(), "Your Account is Blocked,Please Contact Admin..!", Toast.LENGTH_SHORT).show();
                                        } else if (status.equals("DISABLED")) {
                                            Toast.makeText(getApplicationContext(), "Your Account is Disabled,Please Contact Admin...!", Toast.LENGTH_SHORT).show();
                                        } else if (payment_info.equals("false") && !email.equals("")) {
                                            Intent intent = new Intent(SplashActivity.this, PaypalActivity.class);
                                            intent.putExtra("activity", "Login");
                                            intent.putExtra("paymentAmount", "1");
                                            startActivity(intent);
                                        } else if (userid.equals("")) {
                                            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                            startActivity(i);
                                            finish();
                                        } else if (email.equals("") && payment_info.equals("false") && !user_type.equals("")) {
                                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                        } else if (user_type.equals("")) {
                                            Intent intent = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                            startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                            intent.putExtra("USERTYPE", user_type);
                                            startActivity(intent);

                                        }
                                    }
                                    if (successResponceCode.equals("10200")) {
                                        Toast.makeText(getApplicationContext(), "Please provide userId...!", Toast.LENGTH_SHORT).show();
                                    }

                                    if (successResponceCode.equals("10180")) {
                                        Toast.makeText(getApplicationContext(), "User Doesn't Exist...!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                                        startActivity(intent);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
                requestQueue.add(stringRequest);
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        // }
       /* else
            {
            Log.d("LOGINSTATUSURL", "fghgdgfhgfh");
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setMessage("Internet Connection Required")
                        .setCancelable(false)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                        .setPositiveButton("Retry",
                                new DialogInterface.OnClickListener() {
                                    public void onClick( DialogInterface dialog, int id)
                                    {
                                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                        startActivity(i);
                                       // finish();
                                    }

                                });
                AlertDialog alert = builder.create();
                alert.show();

        }*/
    }



   /*   @Override
    protected void onRestart()
    {
         super.onRestart();
           Log.d("ONRESUME","ONRESUME");

        Intent i = new Intent(SplashActivity.this, SplashActivity.class);
        startActivity(i);
        finish();

          *//*  if(checkInternet)
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (session.checkIsFirstTime() != true) {
                        Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
                        startActivity(i);
                            login_status();
                            //    Toast.makeText(getApplicationContext(), "INSIDE SPLS FIRST TIME", Toast.LENGTH_SHORT).show();
                            //finish();
                        } else {
                            if (user_id.equals("") || user_id.equals("null")) {
//                            Intent i = new Intent(SplashActivity.this, VerifyAccountActivity.class);
//                            startActivity(i);
                                login_status();
                            } else {
                                // Toast.makeText(getApplicationContext(), "INSIDE LOUGOUT", Toast.LENGTH_SHORT).show();
                                login_status();
                            }
                        }
                    }
                }, SPLASH_TIME_OUT);
            }
            else
            {
                Log.d("LOGINSTATUSURL", "fghgdgfhgfh");
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setMessage("Internet Connection Required")
                        .setCancelable(false)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                        .setPositiveButton("Retry",
                                new DialogInterface.OnClickListener() {
                                    public void onClick( DialogInterface dialog, int id)
                                    {
                                        Intent i = new Intent(Settings.ACTION_SETTINGS);
                                        startActivity(i);
                                        // finish();
                                    }

                                });
                AlertDialog alert = builder.create();
                alert.show();
            }*//*


        }
*/


}
