package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.AllChallangRunningAdapter;
import in.activitychallenge.activitychallenge.models.AllChallengesStatusModel;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.AllChallengesDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.IndividualCompletedChallengesDB;
import in.activitychallenge.activitychallenge.utilities.IndividualRunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class AllRunningChallengesActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String device_id, token, user_id;
    UserSessionManager session;
    LinearLayoutManager layoutManager;
    RecyclerView recycler_allmore_running;
    ImageView close, refresh;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 0;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    String from_chalng_fragment, sendStatus;
    RelativeLayout bottomLayout;
    AllChallangRunningAdapter allChallangRunningAdapter;
    ArrayList<AllChallengesStatusModel> allchlngRunn = new ArrayList<AllChallengesStatusModel>();
    AllChallengesDB allChallengesDB;
    Intent myGpsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_running_challenges);
        session = new UserSessionManager(getApplicationContext());
        allChallengesDB = new AllChallengesDB(this,0);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Bundle b = new Bundle();
        from_chalng_fragment = getIntent().getExtras().getString("condition");
       /* if (from_chalng_fragment.equals("INDIVIDUALCHALNG")) {
            sendStatus = "INDIVIDUAL";
        } else {
            sendStatus = "GROUP";
        }*/
        initGpsListeners();
        progressDialog = new ProgressDialog(AllRunningChallengesActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        bottomLayout = (RelativeLayout) findViewById(R.id.loadItemsLayout_recyclerView);
        recycler_allmore_running = (RecyclerView) findViewById(R.id.recycler_allmore_running);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        refresh = (ImageView) findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        recycler_allmore_running.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(AllRunningChallengesActivity.this);
        recycler_allmore_running.setLayoutManager(layoutManager);
        allchlngRunn.clear();


       /* recycler_allmore_running.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                getAllRunChallangesMore(defaultPageNo);

                            } else {
                                Toast.makeText(AllRunningChallengesActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });*/
        getAllRunChallangesMore(0);

    }
    private void initGpsListeners()
    {
        myGpsService = new Intent(this, GpsService.class);
        startService(myGpsService);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(myGpsService!=null)
            stopService(myGpsService);
    }

    private void getAllRunChallangesMore(int defaultPageNo) {
        checkInternet = NetworkChecking.isConnected(AllRunningChallengesActivity.this);
        if (checkInternet) {

           allchlngRunn.clear();
           allChallengesDB.emptyDBBucket();
            String url = AppUrls.BASE_URL + AppUrls.ALLMORE_MYCHALLENGES + "?user_id=" + user_id + "&status=RUNNING&page=" + defaultPageNo + "&type=" + from_chalng_fragment;
            Log.d("RUNNLISTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RUNNLISTRESP12", response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    //  recycler_allmore_running.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    //    nodata_image.setVisibility(View.GONE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                 /*   int total_numberof_records = Integer.valueOf(jsonObject1.getString("total_challenges"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > allchlngRunn.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }*/
                                    ContentValues values = new ContentValues();
                                    JSONArray jsonArray = jsonObject1.getJSONArray("challenges");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        AllChallengesStatusModel itemRunnAll = new AllChallengesStatusModel();
                                        values.put(AllChallengesDB.CHALLENGE_ID, jsonObject2.getString("challenge_id"));
                                        values.put(AllChallengesDB.ACTIVITY_ID, jsonObject2.getString("activity_id"));
                                        values.put(AllChallengesDB.USER_NAME, jsonObject2.getString("user_name"));
                                        values.put(AllChallengesDB.OPPONENT_NAME, jsonObject2.getString("opponent_name"));
                                        values.put(AllChallengesDB.WINNING_STATUS, jsonObject2.getString("winning_status"));
                                        values.put(AllChallengesDB.IS_GROUP_ADMIN, jsonObject2.getString("is_group_admin"));
                                        values.put(AllChallengesDB.STATUS, jsonObject2.getString("status"));
                                        values.put(AllChallengesDB.AMOUNT, jsonObject2.getString("amount"));
                                        values.put(AllChallengesDB.WINNING_AMOUNT, jsonObject2.getString("winning_amount"));
                                        values.put(AllChallengesDB.ACTIVITY_NAME, jsonObject2.getString("activity_name"));
                                        values.put(AllChallengesDB.EVALUATION_FACTOR, jsonObject2.getString("evaluation_factor"));
                                        values.put(AllChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject2.getString("evaluation_factor_unit"));
                                        values.put(AllChallengesDB.START_ON, jsonObject2.getString("start_on"));
                                        values.put(AllChallengesDB.PAUSED_ON, jsonObject2.getString("paused_on"));
                                        values.put(AllChallengesDB.RESUME_ON, jsonObject2.getString("resume_on"));
                                        values.put(AllChallengesDB.COMPLETED_ON_TXT, jsonObject2.getString("completed_on_txt"));
                                        values.put(AllChallengesDB.COMPLETED_ON, jsonObject2.getString("completed_on"));
                                        values.put(AllChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image"));
                                        values.put(AllChallengesDB.WINNER, jsonObject2.getString("winner"));
                                        values.put(AllChallengesDB.PAUSED_BY, jsonObject2.getString("paused_by"));
                                        values.put(AllChallengesDB.CHALLENGE_TYPE, jsonObject2.getString("challenge_type"));
                                        values.put(AllChallengesDB.WIN_REWARD_TYPE, jsonObject2.getString("winning_reward_type"));
                                        values.put(AllChallengesDB.WIN_REWARD_VALUE, jsonObject2.getString("winning_reward_value"));
                                        values.put(AllChallengesDB.IS_SCRATCHED, jsonObject2.getString("is_scratched"));
                                        values.put(AllChallengesDB.GET_GPS, jsonObject2.getString("gps"));
                                        values.put(AllChallengesDB.OPPONENT_ID, jsonObject2.getString("opponent_id"));
                                        values.put(AllChallengesDB.PAUSE_ACCESS, jsonObject2.getString("pause_access"));
                                        values.put(AllChallengesDB.CHALLENGE_GROUP_ID, jsonObject2.getString("challenger_group_id"));
                                        values.put(AllChallengesDB.OPPONENT_TYPE, jsonObject2.getString("opponent_type"));

                                        allChallengesDB.addMoreRunningChallengesList(values);
                                        Log.d("dsvbjsdbvjksdv",values.toString());
                                        getMoreRunningChallenges();

/*
                                        itemRunnAll.setChallenge_id(jsonObject2.getString("challenge_id"));
                                        itemRunnAll.setActivity_id(jsonObject2.getString("activity_id"));
                                        itemRunnAll.setOpponent_type(jsonObject2.getString("opponent_type"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image");
                                        itemRunnAll.setActivity_image(image);
                                        itemRunnAll.setEvaluation_factor(jsonObject2.getString("evaluation_factor"));
                                        itemRunnAll.setEvaluation_factor_unit(jsonObject2.getString("evaluation_factor_unit"));
                                        itemRunnAll.setOpponent_name(jsonObject2.getString("opponent_name"));
                                        itemRunnAll.setUser_name(jsonObject2.getString("user_name"));
                                        itemRunnAll.setWinning_status(jsonObject2.getString("winning_status"));
                                        itemRunnAll.setStatus(jsonObject2.getString("status"));
                                        itemRunnAll.setAmount(jsonObject2.getString("amount"));
                                        itemRunnAll.setActivity_name(jsonObject2.getString("activity_name"));
                                        itemRunnAll.setStart_on(jsonObject2.getString("start_on"));
                                        itemRunnAll.setPaused_on(jsonObject2.getString("paused_on"));
                                        itemRunnAll.setCompleted_on_txt(jsonObject2.getString("completed_on_txt"));
                                        itemRunnAll.setCompleted_on(jsonObject2.getString("completed_on"));
                                        itemRunnAll.setChallenge_type(jsonObject2.getString("challenge_type"));
                                        allchlngRunn.add(itemRunnAll);
                                        Log.d("RUNNLISTITEM:", allchlngRunn.toString());*/
                                    }
                                    //    layoutManager.scrollToPositionWithOffset(displayedposition, allchlngRunn.size());
                                   // recycler_allmore_running.setAdapter(allChallangRunningAdapter);
                                    bottomLayout.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("10200")) {
                                    recycler_allmore_running.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    // nodata_image.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                //  nodata_image.setVisibility(View.VISIBLE);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    //  nodata_image.setVisibility(View.VISIBLE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GGGGG_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllRunningChallengesActivity.this);
            requestQueue.add(stringRequest);
        } else {
            getMoreRunningChallenges();
            //Toast.makeText(AllRunningChallengesActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }


    }

    private void getMoreRunningChallenges() {
        allchlngRunn.clear();
        List<String> activityName = allChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = allChallengesDB.getChallengeID();
            List<String> activity_id = allChallengesDB.getActivityID();
            List<String> user_name = allChallengesDB.getUserName();
            List<String> opponent_name = allChallengesDB.getOpponentName();
            List<String> opponent_type=allChallengesDB.getOpponentType();
            List<String> winning_status = allChallengesDB.getWinningStatus();
            List<String> is_group_admin = allChallengesDB.getIsGroupAdmin();
            List<String> status = allChallengesDB.getStatus();
            List<String> amount = allChallengesDB.getAmount();
            List<String> winning_amount = allChallengesDB.getWinningAmount();
            List<String> activity_name = allChallengesDB.getActivityName();
            List<String> evaluation_factor = allChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = allChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = allChallengesDB.getStartOn();
            List<String> paused_on = allChallengesDB.getPausedOn();
            List<String> resume_on = allChallengesDB.getResumeOn();
            List<String> completed_on_txt = allChallengesDB.getCompletedOnTxt();
            List<String> completed_on = allChallengesDB.getCompletedOn();
            List<String> activity_image = allChallengesDB.getActivityImage();
            List<String> winner = allChallengesDB.getWinner();
            List<String> paused_by = allChallengesDB.getPaused_by();
            List<String> challenge_type = allChallengesDB.getChallengeType();
            List<String> winning_reward_type = allChallengesDB.win_RewardType();
            List<String> winning_reward_value = allChallengesDB.win_RewardValue();
            List<String> is_scratched = allChallengesDB.isScratched();
            List<String> oppenttype = allChallengesDB.getOpponentType();
            List<String> gpss = allChallengesDB.getGPS();

            Log.v("Challenge",">>>"+challenge_id.toString());
            for (int i = 0; i < activityName.size(); i++) {
                AllChallengesStatusModel am_runn = new AllChallengesStatusModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setOpponent_type(opponent_type.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setOpponent_type(oppenttype.get(i));
                am_runn.setGetGps(gpss.get(i));
               allchlngRunn.add(am_runn);
                Log.d("sdgsdfdgjsdfg", allchlngRunn.toString());

            }
            allChallangRunningAdapter = new AllChallangRunningAdapter(allchlngRunn, AllRunningChallengesActivity.this, R.layout.row_all_challenges_more);
            recycler_allmore_running.setAdapter(allChallangRunningAdapter);

        }
    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == refresh) {
            allchlngRunn.clear();
            getAllRunChallangesMore(defaultPageNo);
        }
    }
}
