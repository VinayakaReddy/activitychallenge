package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class SponsorStatisticsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    float per_user, per_active_user, per_sponsor, per_total_activities;
    int total, users, active_users, sponsors, tot_activities;
    private static String TAG = "MainActivity";

    //     private float[] yData = {25.3f, 10.6f, 66.76f, 44.32f, 46.01f, 16.89f, 23.9f};
//    private String[] xData = {"Mitch", "Jessica" , "Mohammad" , "Kelsey", "Sam", "Robert", "Ashley"};
    PieChart pieChart;
    ArrayList<Entry> yEntrys = new ArrayList<>();
    ArrayList<String> xEntrys = new ArrayList<>();
    ArrayList<Float> yData = new ArrayList<>();
    ArrayList<String> xData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_statistics);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        pprogressDialog = new ProgressDialog(SponsorStatisticsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        pieChart = (PieChart) findViewById(R.id.idPieChart);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

       // pieChart.setDescription(" ");
        pieChart.setRotationEnabled(true);
        //pieChart.setUsePercentValues(true);
        //pieChart.setHoleColor(Color.BLUE);
        //pieChart.setCenterTextColor(Color.BLACK);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Sponsor Statistics");
        pieChart.setCenterTextSize(10);
        //pieChart.setDrawEntryLabels(true);
        //pieChart.setEntryLabelTextSize(20);
        //More options just check out the documentation!

        getStatisticsData();


      /*  pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

                Log.d(TAG, "onValueSelectedEntry: " + e.toString());
                Log.d(TAG, "onValueSelectedHighlight: " + h.toString());

                int pos1 = e.toString().indexOf("(sum): ");
                String sales = e.toString().substring(pos1 + 7);

                for(int i = 0; i < yData.length; i++){
                    if(yData[i] == Float.parseFloat(sales)){
                        pos1 = i;
                        break;
                    }
                }
                String employee = xData[pos1 + 1];
                Toast.makeText(SponsorStatisticsActivity.this, "Employee " + employee + "\n" + "Sales: $" + sales + "K", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });*/


    }

    private void getStatisticsData() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url_sponsor_statistics = AppUrls.BASE_URL + AppUrls.SPONSOR_STATISTICS + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("SPOSTSATURL", url_sponsor_statistics);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_sponsor_statistics, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("SPOSTSATRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject jData = jobcode.getJSONObject("data");


                            users = jData.getInt("users");
                            active_users = jData.getInt("active_users");
                            sponsors = jData.getInt("sponsors");
                            tot_activities = jData.getInt("activities");
                            total = jData.getInt("total");

                            xData.add("Users");
                            xData.add("Active Users");
                            xData.add("Sponsors");
                          //  xData.add("activities");

                            //calculating Percentage
                            per_user = users * 100 / total;
                            per_active_user = (active_users) * 100 / total;
                            per_sponsor = (sponsors) * 100 / total;
                            per_total_activities = (tot_activities) * 100 / total;


                            yData.add(per_user);
                            yData.add(per_active_user);
                            yData.add(per_sponsor);
                         //   yData.add(per_total_activities);

                            addDataSet();


                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(SponsorStatisticsActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();

                            Toast.makeText(SponsorStatisticsActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(SponsorStatisticsActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(SponsorStatisticsActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void addDataSet() {
        Log.d(TAG, "addDataSet started");


        for (int i = 0; i < yData.size(); i++) {
            yEntrys.add(new Entry(yData.get(i), i));
        }

        for (int i = 1; i < xData.size(); i++) {
            xEntrys.add(xData.get(i));
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(Color.WHITE);

        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.parseColor("#DD6B55"));
        colors.add(Color.parseColor("#239843"));
        colors.add(Color.parseColor("#0987be"));
        //colors.add(Color.GREEN);



        pieDataSet.setColors(colors);


        PieData data = new PieData(xData, pieDataSet);
        // In Percentage term
        data.setValueFormatter(new PercentFormatter());
        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);
        pieChart.setDescription("This is Pie Chart");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(25f);
        pieChart.setHoleRadius(25f);

        pieDataSet.setColors(colors);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.WHITE);
      //  pieChart.setOnChartValueSelectedListener(this);
        Legend l = pieChart.getLegend();
        l.setForm(Legend.LegendForm.SQUARE);

        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);

        pieChart.animateXY(1400, 1400);


    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
