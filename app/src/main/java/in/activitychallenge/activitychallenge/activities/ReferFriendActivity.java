package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ReferFriendActivity extends AppCompatActivity implements View.OnClickListener {


    Typeface typeface, typeface_bold;
    TextView refer_friend_text, refer_text_title;
    ImageView friend_share, close;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String user_id, access_token, device_id, user_type, refUserIdJ, refCodeJ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_friend);

        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        refer_text_title = (TextView) findViewById(R.id.refer_text_title);
        refer_friend_text = (TextView) findViewById(R.id.refer_friend_text);
        friend_share = (ImageView) findViewById(R.id.friend_share);
        friend_share.setOnClickListener(this);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        getRefCode();
    }

    private void getRefCode() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("GetReferalURL", AppUrls.BASE_URL + AppUrls.REFERAL_code + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.REFERAL_code + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GetReferalRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                  //  Toast.makeText(ReferFriendActivity.this, "Refer code Success", Toast.LENGTH_SHORT).show();
                                    JSONObject jObj = jsonObject.getJSONObject("data");
                                    refUserIdJ = jObj.getString("user_id");
                                    refCodeJ = jObj.getString("refer_code");
                                    refer_friend_text.setText(refCodeJ);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(ReferFriendActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GetReferal_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == friend_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App To stay active and earn money. Here's my Refer Code " + refCodeJ+". just enter it while Register"+ "\n" +AppUrls.SHARE_APP_URL);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        }
        if (view == close) {
            finish();
        }
    }
}
