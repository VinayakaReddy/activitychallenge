package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.UserRunningChallengesAdapter;
import in.activitychallenge.activitychallenge.models.AllChallengesStatusModel;
import in.activitychallenge.activitychallenge.models.UserRunningChallengesModel;
import in.activitychallenge.activitychallenge.utilities.AllChallengesDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.RunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class RunningChallengesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_data_image;
    TextView running_toolbar_title, acti_totalChalleng;
    Typeface typeface, typeface_bold;
    String id = "", user_id = "", token = "", device_id = "";
    int totalChallengI;
    UserSessionManager session;
    ProgressDialog pprogressDialog;
    private boolean checkInternet;
    ////////////////////////////////////
    ImageView user_profile_image, sponsor_iv, challenge_iv, mesage_iv, banner_image;
    TextView sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    RecyclerView run_acti_recycler;
    LinearLayoutManager lManager;
    ArrayList<UserRunningChallengesModel> myRunlist = new ArrayList<UserRunningChallengesModel>();
    UserRunningChallengesAdapter myRunAdap;
    RunningChallengesDB runningChallengesDB;
    Intent myGpsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);

        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        sponsor_iv = (ImageView) findViewById(R.id.sponsor_iv);
        challenge_iv = (ImageView) findViewById(R.id.challenge_iv);
        mesage_iv = (ImageView) findViewById(R.id.mesage_iv);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        acti_totalChalleng = (TextView) findViewById(R.id.acti_totalChalleng);
        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        session = new UserSessionManager(RunningChallengesActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        no_data_image = (ImageView) findViewById(R.id.no_data_image);
         initGpsListeners();
        runningChallengesDB = new RunningChallengesDB(this);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        run_acti_recycler = (RecyclerView) findViewById(R.id.run_acti_recycler);
        run_acti_recycler.setNestedScrollingEnabled(false);
        myRunAdap = new UserRunningChallengesAdapter(RunningChallengesActivity.this, myRunlist, R.layout.row_user_running_challenges);
        lManager = new LinearLayoutManager(this);
        run_acti_recycler.setLayoutManager(lManager);

        getCount();
        getUserRunningChallenge();
    }

    private void initGpsListeners()
    {
        myGpsService = new Intent(this, GpsService.class);
        startService(myGpsService);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(myGpsService!=null)
            stopService(myGpsService);
    }

    private void getUserRunningChallenge() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            myRunlist.clear();
            runningChallengesDB.emptyDBBucket();
            String url_follow_members = AppUrls.BASE_URL + "user/challenges?user_id=" + user_id + "&status=RUNNING" + "&page=0";
            Log.d("Run_Chal_URL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("Run_Chalg_RPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        ContentValues values = new ContentValues();
                        if (response_code.equals("10100")) {
                            // Toast.makeText(RunningChallengesActivity.this, "Running Success", Toast.LENGTH_LONG).show();
                            JSONObject jobj = jobcode.getJSONObject("data");
                            totalChallengI = jobj.getInt("total_challenges");
                            values.put(runningChallengesDB.TOTAL_CHALLENGES, totalChallengI);
                            acti_totalChalleng.setText(String.valueOf(totalChallengI));
                            JSONArray jArr = jobj.getJSONArray("challenges");
                            for (int i = 0; i < jArr.length(); i++) {
                                JSONObject jsonObject2 = jArr.getJSONObject(i);

                                values.put(RunningChallengesDB.CHALLENGE_ID, jsonObject2.getString("challenge_id"));
                                values.put(RunningChallengesDB.ACTIVITY_ID, jsonObject2.getString("activity_id"));
                                values.put(RunningChallengesDB.USER_NAME, jsonObject2.getString("user_name"));
                                values.put(RunningChallengesDB.OPPONENT_NAME, jsonObject2.getString("opponent_name"));
                                values.put(RunningChallengesDB.WINNING_STATUS, jsonObject2.getString("winning_status"));
                                values.put(RunningChallengesDB.IS_GROUP_ADMIN, jsonObject2.getString("is_group_admin"));
                                values.put(RunningChallengesDB.STATUS, jsonObject2.getString("status"));
                                values.put(RunningChallengesDB.AMOUNT, jsonObject2.getString("amount"));
                                values.put(RunningChallengesDB.WINNING_AMOUNT, jsonObject2.getString("winning_amount"));
                                values.put(RunningChallengesDB.ACTIVITY_NAME, jsonObject2.getString("activity_name"));
                                values.put(RunningChallengesDB.EVALUATION_FACTOR, jsonObject2.getString("evaluation_factor"));
                                values.put(RunningChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject2.getString("evaluation_factor_unit"));
                                values.put(RunningChallengesDB.START_ON, jsonObject2.getString("start_on"));
                                values.put(RunningChallengesDB.PAUSED_ON, jsonObject2.getString("paused_on"));
                                values.put(RunningChallengesDB.RESUME_ON, jsonObject2.getString("resume_on"));
                                values.put(RunningChallengesDB.COMPLETED_ON_TXT, jsonObject2.getString("completed_on_txt"));
                                values.put(RunningChallengesDB.COMPLETED_ON, jsonObject2.getString("completed_on"));
                                values.put(RunningChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image"));
                                values.put(RunningChallengesDB.WINNER, jsonObject2.getString("winner"));
                                values.put(RunningChallengesDB.PAUSED_BY, jsonObject2.getString("paused_by"));
                                values.put(RunningChallengesDB.CHALLENGE_TYPE, jsonObject2.getString("challenge_type"));
                                values.put(RunningChallengesDB.WIN_REWARD_TYPE, jsonObject2.getString("winning_reward_type"));
                                values.put(RunningChallengesDB.WIN_REWARD_VALUE, jsonObject2.getString("winning_reward_value"));
                                values.put(RunningChallengesDB.IS_SCRATCHED, jsonObject2.getString("is_scratched"));
                                values.put(RunningChallengesDB.GET_GPS, jsonObject2.getString("gps"));
                                values.put(RunningChallengesDB.OPPONENT_ID, jsonObject2.getString("opponent_id"));
                                values.put(RunningChallengesDB.PAUSE_ACCESS, jsonObject2.getString("pause_access"));
                                values.put(RunningChallengesDB.CHALLENGE_GROUP_ID, jsonObject2.getString("challenger_group_id"));
                                values.put(RunningChallengesDB.OPPONENT_TYPE, jsonObject2.getString("opponent_type"));
                                runningChallengesDB.addUserRunningChallengesList(values);
                                getMoreRunningChallenges();


                            }

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            no_data_image.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("Run_Chalg_HEADERS", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(RunningChallengesActivity.this);
            requestQueue.add(req_members);
        } else {
            getMoreRunningChallenges();
           // Toast.makeText(RunningChallengesActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(RunningChallengesActivity.this);
            requestQueue.add(stringRequest);

        } else {

          //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


    private void getMoreRunningChallenges() {
        myRunlist.clear();
        acti_totalChalleng.setText(runningChallengesDB.getTotalChallenges());
        List<String> activityName = runningChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = runningChallengesDB.getChallengeID();
            List<String> activity_id = runningChallengesDB.getActivityID();

            List<String> opponent_name = runningChallengesDB.getOpponentName();
            List<String> opponent_type = runningChallengesDB.getOpponentType();
            List<String> winning_status = runningChallengesDB.getWinningStatus();

            List<String> status = runningChallengesDB.getStatus();
            List<String> amount = runningChallengesDB.getAmount();

            List<String> activity_name = runningChallengesDB.getActivityName();
            List<String> evaluation_factor = runningChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = runningChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = runningChallengesDB.getStartOn();
            List<String> paused_on = runningChallengesDB.getPausedOn();

            List<String> completed_on_txt = runningChallengesDB.getCompletedOnTxt();
            List<String> completed_on = runningChallengesDB.getCompletedOn();
            List<String> activity_image = runningChallengesDB.getActivityImage();
            List<String> opponettypes = runningChallengesDB.getOpponentType();
            List<String> gps = runningChallengesDB.getGPS();


            for (int i = 0; i < activityName.size(); i++) {
                UserRunningChallengesModel am_runn = new UserRunningChallengesModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setActivity_id(activity_id.get(i));

                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setOpponent_type(opponent_type.get(i));
                am_runn.setWinning_status(winning_status.get(i));

                am_runn.setStatus(status.get(i));
                am_runn.setAmount(Integer.parseInt(amount.get(i)));

                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));

                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setOpponent_type(opponettypes.get(i));
                am_runn.setGetGps(gps.get(i));

                myRunlist.add(am_runn);
                Log.d("sdgsdfdgjsdfg", am_runn.toString());

            }

            run_acti_recycler.setAdapter(myRunAdap);

        }
    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(RunningChallengesActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(RunningChallengesActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(RunningChallengesActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
        }
    }
}