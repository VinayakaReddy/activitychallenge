package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodeActivity extends AppCompatActivity implements View.OnClickListener,ZXingScannerView.ResultHandler{
    ImageView close;
    private boolean checkInternet;
    TextView satus_text;
    EditText result_text,success_result_text;
    Button send_btn;
    RelativeLayout qrlayout;
    LinearLayout result_layout;
    ImageView status_img;
    String device_id, access_token, user_id;
    private ZXingScannerView mScanner;
    UserSessionManager userSessionManager;
    ProgressDialog pprogressDialog;
    String sponsorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponser_qrcode);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);



        mScanner = new ZXingScannerView(this);   // Programmatically initialize the scanner view<br />
        setContentView(mScanner);
        mScanner.setResultHandler(this); // Register ourselves as a handler for scan results.<br />
        mScanner.startCamera();


    }


    @Override
    protected void onPause() {
        super.onPause();
        mScanner.stopCamera();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mScanner.startCamera();

    }
    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }else if(view==send_btn){
            //String sponerid=result_text.getText().toString();
            sendQRCode(sponsorId);
        }
    }



    private void initizeUI(){
        result_text=(EditText) findViewById(R.id.result_text);
        success_result_text=(EditText) findViewById(R.id.success_result_text);
        qrlayout=(RelativeLayout)findViewById(R.id.qrcode_rl);
        result_layout=(LinearLayout)findViewById(R.id.result_layout);
        satus_text=(TextView)findViewById(R.id.status_qrcode_txt);
        status_img=(ImageView)findViewById(R.id.status_img);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        send_btn=(Button)findViewById(R.id.send_Btn);
        send_btn.setOnClickListener(this);
    }


    private void sendQRCode(final String sponserid){
        checkInternet = NetworkChecking.isConnected(QRCodeActivity.this);
        if (checkInternet) {


                Log.d("scanqrcode", AppUrls.BASE_URL + AppUrls.SCAN_QRCODE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SCAN_QRCODE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pprogressDialog.dismiss();
                                Log.d("scanQRcode", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("response_code");
                                    if (successResponceCode.equals("10100"))
                                    {
                                        pprogressDialog.dismiss();
                                        qrlayout.setVisibility(View.GONE);
                                        result_layout.setVisibility(View.VISIBLE);
                                        satus_text.setText("Success");
                                        success_result_text.setText("Successful \n please collect your drink now.");

                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        satus_text.setText("Failed");
                                        Typeface typeface = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.mp_regular));
                                        success_result_text.setText("Invalid QR Code");
                                        success_result_text.setTypeface(typeface);
//                                        Toast.makeText(getApplicationContext(), "Invalid QR Code", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("10400")) {
                                        pprogressDialog.dismiss();
                                        qrlayout.setVisibility(View.GONE);
                                        result_layout.setVisibility(View.VISIBLE);
                                        satus_text.setText("Failed");
                                        Typeface typeface = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.mp_regular));
                                        success_result_text.setText("You have already use this code \n"+sponserid);
                                        success_result_text.setTypeface(typeface);
                                       // Toast.makeText(getApplicationContext(),jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pprogressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token", access_token);
                        headers.put("x-device-id", device_id);
                        headers.put("x-device-platform", "ANDROID");
                        Log.d("sendBusiness_HEADER", "sendBusiness_HEADER " + headers.toString());
                        return headers;
                    }


                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put("sponsor_id",sponserid);


                        Log.d("scanQR_PARAM:", "scanQR_PARAM:" + params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(QRCodeActivity.this);
                requestQueue.add(stringRequest);
            }else {
                pprogressDialog.cancel();
                   Toast.makeText(QRCodeActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();

        }

    }



    @Override
    public void handleResult(Result result) {

         setContentView(R.layout.activity_sponser_qrcode);
             initizeUI();
             sponsorId=result.toString();
        qrlayout.setVisibility(View.VISIBLE);
        Typeface typeface = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.mp_regular));
        result_text.setText("Your successfully scanned \n Please send this code "+ sponsorId +" \nto get a drink");
        result_text.setTypeface(typeface);
        mScanner.resumeCameraPreview(this);
    }
}
