package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChallengeAdapter;
import in.activitychallenge.activitychallenge.models.MyChallengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyChallengeActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView no_requests_img, close;
    RecyclerView mychallenge_recyclerview;
    LinearLayoutManager layoutManager;
    MyChallengeAdapter adapter;
    ArrayList<MyChallengeModel> myChallengeModels = new ArrayList<MyChallengeModel>();
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String user_id = "", user_type = "", device_id = "", token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_challenge);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        no_requests_img = (ImageView) findViewById(R.id.no_requests_img);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        mychallenge_recyclerview = (RecyclerView) findViewById(R.id.mychallenge_recyclerview);
        adapter = new MyChallengeAdapter(myChallengeModels, MyChallengeActivity.this, R.layout.row_mychallenges_list);
        layoutManager = new LinearLayoutManager(this);
        mychallenge_recyclerview.setNestedScrollingEnabled(false);
        mychallenge_recyclerview.setLayoutManager(layoutManager);

        getMyChallenges();
    }

    public void getMyChallenges() {
        myChallengeModels.clear();
        checkInternet = NetworkChecking.isConnected(MyChallengeActivity.this);
        if (checkInternet) {
            //Log.d("MY_CHAL_URL", AppUrls.BASE_URL+AppUrls.MY_CHALLENGES+"user_id="+ user_id+"&type="+user_type);
            Log.d("MY_CHAL_URL", AppUrls.BASE_URL + AppUrls.MY_CHALLENGE + user_id + "&type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.MY_CHALLENGE + user_id + "&type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("REPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);
                                        MyChallengeModel crm = new MyChallengeModel();
                                        crm.setId(jsonObject1.getString("id"));
                                        crm.setStatus(jsonObject1.getString("status"));
                                        crm.setAmount(jsonObject1.getString("amount"));
                                        crm.setUser_wallet(jsonObject1.getString("user_wallet"));
                                        crm.setGroup_wallet(jsonObject1.getString("group_wallet"));
                                        crm.setUser_name(jsonObject1.getString("user_name"));
                                        crm.setGroup_name(jsonObject1.getString("group_name"));
                                        crm.setActivity_name(jsonObject1.getString("activity_name"));
                                        crm.setUser_rank(jsonObject1.getString("user_rank"));
                                        crm.setGroup_rank(jsonObject1.getString("group_rank"));
                                        crm.setUser_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("user_pic"));
                                        crm.setGroup_pic(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("group_pic"));
                                        myChallengeModels.add(crm);
                                    }
                                    mychallenge_recyclerview.setAdapter(adapter);

                                    if (jarray.length() == 0) {
                                        no_requests_img.setVisibility(View.VISIBLE);
                                    }
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_requests_img.setVisibility(View.VISIBLE);
                                    Toast.makeText(MyChallengeActivity.this, "No Challenges Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MyChallengeActivity.this);
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(MyChallengeActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
    }
}
