package in.activitychallenge.activitychallenge.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.DayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.adapter.OpponentDayDateWiseChallengeStatusAdapter;
import in.activitychallenge.activitychallenge.models.DayDateWiseStatusModel;
import in.activitychallenge.activitychallenge.utilities.AllChallengesDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.ChallengeFinishReceiver;
import in.activitychallenge.activitychallenge.utilities.ChallengeFinishedTime;
import in.activitychallenge.activitychallenge.utilities.ConnectivityReceiver;
import in.activitychallenge.activitychallenge.utilities.CurrentLocationDB;
import in.activitychallenge.activitychallenge.utilities.DaysWiseDB;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.MyApplication;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.StatusStepChallengeDB;
import in.activitychallenge.activitychallenge.utilities.StepsCountService;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;

public class StepChallengeStatusActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    ImageView close, challenger_one_image, challenger_two_image,gps_track_img;
    static StepChallengeStatusActivity instance;
    TextView toolbar_title, participent_user_type, participent_oponent_type, vsTxt, memb_count_top, mem_cccount_down;
    private boolean checkInternet;
    UserSessionManager session;
    String token = "", device_id = "", challenge_id;
    String opponentType;
    ProgressDialog progressDialog;
    int id = 1;
    int timeinsec;
    int lateststeps;
    String toogleValue;
    Context ctx;
    boolean activityStatus = false;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    // BroadcastReceiver receiver;
    Sensor stepSensor;
    SensorManager sManager;
    private long timestamp;

    SharedPreferences sharedPreferences;
    private int numStepsFromServer;
    private long steps = 0;
    String steps_count = "";
    String distance_calc = "";
    Intent serviceIntent;
     boolean isStarted;
    String challenge_goal;
    int total_steps;
    String android_id, opponent_type, challenge_type;
    Typeface typeface3;
    Button send_challenge_data;
    int msgflag = 0;
    String model = Build.MODEL;
    String user_id, opponent_id, evaluation_factor, evaluation_factor_unit, usd_type_in, opponenttype_in;
    public TextView activity_name, goal, cash_price, challenger_one_steps, challenger_two_steps, remaining_challenger_one_steps, remaining_challenger_two_steps, participent_oponent_name, participent_user_name;
    TextView challenger_one_time_remaining, challenger_two_time_remaining, challenger_one_rank, challenger_two_rank, update;
    LinearLayout ll_top, ll_down, ll_days_status_down, ll_days_status_top;
    CurrentLocationDB current_db;
    String user_completed_goal = "0", group_id = "", opponent_completed_goal;
    String user_type_for_challenge;
    HashMap<String, String> userDetails;
    NumberProgressBar number_progress_bar, number_progress_bar_opponent;
    StatusStepChallengeDB statusStepChallengeDB;
    DaysWiseDB daysWiseDB;
    ContentValues values;
    int latesttotalsteps;
    Button serviceBtn;
    Intent myGpsService;
    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
    RecyclerView recyclerview_days_status_top, recyclerview_days_status_down;
    DayDateWiseChallengeStatusAdapter dayDateWiseChallengeStatusAdapter;
    ArrayList<DayDateWiseStatusModel> dayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    ArrayList<DayDateWiseStatusModel> opponentdayDateWiseStatusModel = new ArrayList<DayDateWiseStatusModel>();
    OpponentDayDateWiseChallengeStatusAdapter opponentDayDateWiseChallengeStatusAdapter;
    RecyclerView.LayoutManager layoutManager, layoutManager2;
    SensorEventListener eventListener;
    private Thread detectorTimeStampUpdaterThread;
    ChallengeFinishedTime finishedTime;

    SharedPreferences sensorPreference;
    SharedPreferences.Editor sensoreditor;

    private Handler handler;

    private MyBroadcastReceiver myReceiver;

    private boolean isRunning = true;
    private static boolean mIsSensorUpdateEnabled = false;
    //////////////////////
    String t;
    PowerManager.WakeLock mWakeLock;
//    List<String> user_complet_goal;


    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_status);
        statusStepChallengeDB = new StatusStepChallengeDB(StepChallengeStatusActivity.this);
        finishedTime=new ChallengeFinishedTime(this);
        finishedTime.challengeFinishTime();
        values = new ContentValues();
        checkInternet = NetworkChecking.isConnected(StepChallengeStatusActivity.this);
        current_db = new CurrentLocationDB(this);
        session = new UserSessionManager(StepChallengeStatusActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        number_progress_bar = (NumberProgressBar) findViewById(R.id.number_progress_bar);
        number_progress_bar_opponent = (NumberProgressBar) findViewById(R.id.number_progress_bar_opponent);
        android_id = Settings.Secure.getString(StepChallengeStatusActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        steps_count = "0";
        distance_calc = "0 Km";
        instance = this;
        session = new UserSessionManager(getApplicationContext());
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsec = (tz.getOffset(cal.getTimeInMillis())) / 1000;

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        challenge_id = bundle.getString("challenge_id");
        opponentType = bundle.getString("opponent_type");


        typeface3 = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.hermes));
        serviceBtn=(Button)findViewById(R.id.service_tooglebtn);
        serviceBtn.setOnClickListener(this);
        ll_top = (LinearLayout) findViewById(R.id.ll_top);
        ll_down = (LinearLayout) findViewById(R.id.ll_down);
        ll_days_status_down = (LinearLayout) findViewById(R.id.ll_days_status_down);
        ll_days_status_top = (LinearLayout) findViewById(R.id.ll_days_status_top);
        recyclerview_days_status_top = (RecyclerView) findViewById(R.id.recyclerview_days_status_top);
        recyclerview_days_status_down = (RecyclerView) findViewById(R.id.recyclerview_days_status_down);
        challenger_one_image = (ImageView) findViewById(R.id.challenger_one_image);
        challenger_two_image = (ImageView) findViewById(R.id.challenger_two_image);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        gps_track_img = (ImageView) findViewById(R.id.gps_track_img);
        gps_track_img.setOnClickListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        vsTxt = (TextView) findViewById(R.id.vsTxt);
        vsTxt.setTypeface(typeface3);
        activity_name = (TextView) findViewById(R.id.activity_name);
        goal = (TextView) findViewById(R.id.goal);
        cash_price = (TextView) findViewById(R.id.cash_price);
        participent_oponent_type = (TextView) findViewById(R.id.participent_oponent_type);
        participent_user_type = (TextView) findViewById(R.id.participent_user_type);
        participent_user_name = (TextView) findViewById(R.id.participent_user_name);
        participent_oponent_name = (TextView) findViewById(R.id.participent_oponent_name);
        challenger_one_steps = (TextView) findViewById(R.id.challenger_one_steps);
        challenger_two_steps = (TextView) findViewById(R.id.challenger_two_steps);
        remaining_challenger_one_steps = (TextView) findViewById(R.id.remaining_challenger_one_steps);
        remaining_challenger_two_steps = (TextView) findViewById(R.id.remaining_challenger_two_steps);
        challenger_one_time_remaining = (TextView) findViewById(R.id.challenger_one_time_remaining);
        challenger_two_time_remaining = (TextView) findViewById(R.id.challenger_two_time_remaining);
        challenger_one_rank = (TextView) findViewById(R.id.challenger_one_rank);
        challenger_two_rank = (TextView) findViewById(R.id.challenger_two_rank);
        update = (TextView) findViewById(R.id.update);
        update.setVisibility(View.GONE);
        //   checkConnection();
        memb_count_top = (TextView) findViewById(R.id.memb_count_top);
        memb_count_top.setOnClickListener(this);
        mem_cccount_down = (TextView) findViewById(R.id.mem_cccount_down);
        mem_cccount_down.setOnClickListener(this);
        String gpsStatus=bundle.getString("gps");
        if(gpsStatus!=null){
            if(gpsStatus.equals("1"))
                  gps_track_img.setVisibility(View.VISIBLE);

        }

        send_challenge_data = (Button) findViewById(R.id.send_challenge_data);
        getChallengeStatus();
        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                msgflag = 1;
                updateThread();
            }
        }, 0, 5, TimeUnit.MINUTES);

        //  startService(new Intent(StepChallengeStatusActivity.this,StepsCountService.class));
       // registerForSensorEvents();
        setupDetectorTimestampUpdaterThread();
      //  challengeFinish();
        sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
        sensoreditor = sensorPreference.edit();
        isStarted=sensorPreference.getBoolean("isStarted",false);
        final String challengeId=sensorPreference.getString("challengeId","");
        serviceBtn.setText("START");
        serviceBtn.setBackgroundColor(Color.GREEN);
        sManager.unregisterListener(eventListener);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(challengeId.equalsIgnoreCase(challenge_id)) {
                    if (isStarted) {
                        serviceBtn.setText("STOP");
                        serviceBtn.setBackgroundColor(Color.RED);
                        mIsSensorUpdateEnabled =true;
                        initGpsListeners();
                        sManager.registerListener(eventListener, stepSensor, SensorManager.SENSOR_DELAY_UI);
                    } else {
                        serviceBtn.setText("START");
                        serviceBtn.setBackgroundColor(Color.GREEN);
                        stopSensors();
                       // sManager.unregisterListener(eventListener);
                    }
                }


            }
        }, 1000);
        Log.v("ServiceFirst","////"+isStarted);

        eventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                Sensor sensor = event.sensor;
                float[] values = event.values;
                int value = -1;
                if (!mIsSensorUpdateEnabled) {
                    stopSensors();
                    Log.v("SensorMM", "SensorUpdate STEPS disabled. returning");
                    return;
                }
                Log.v("ServiceSart", "...."+"afdafasf"+">>>>"+sensor.getType());
                Log.v("Steps before", "" + numStepsFromServer);
                if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
                    SharedPreferences preferences = getSharedPreferences("StepsCount", MODE_PRIVATE);

                     boolean isupdated=sensorPreference.getBoolean("isStarted",false);
                    Log.v("ServiceValue","&&&&&"+isupdated);
                     if(isupdated){

                    lateststeps++;
                   // SharedPreferences preferences = getSharedPreferences("StepsCount", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    Log.v("Count", ">>" + lateststeps);
                    editor.putInt("steps", lateststeps);
                    editor.apply();
                    latesttotalsteps++;
                    Log.v("Updating Steps ", ">>>>>>");

                    Log.v("ServiceSartInside", "...."+"afdafasf"+">>>>"+challenger_one_steps.getText().toString()+"%%%"+latesttotalsteps);
                    challenger_one_steps.setText("" +latesttotalsteps);
                    // Toast.makeText(StepChallengeStatusActivity.this,""+latesttotalsteps,Toast.LENGTH_SHORT).show();
                    getUpdateSteps();
                     }

                    //getDistanceRun(numStepsFromServer);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };

    }


    private void initGpsListeners()
    {
        SharedPreferences preferences=getSharedPreferences("GPS",MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("challengeId",challenge_id);
        editor.apply();
        myGpsService = new Intent(StepChallengeStatusActivity.this, GpsService.class);
         startService(myGpsService);

    }
    private void stopSensors(){
        sManager.unregisterListener(eventListener);
        mIsSensorUpdateEnabled =false;
       if(myGpsService==null)
           myGpsService=new Intent(StepChallengeStatusActivity.this,GpsService.class);
        stopService(myGpsService);
    }
    public void getChallengeStatus() {
        checkInternet = NetworkChecking.isConnected(StepChallengeStatusActivity.this);
        if (checkInternet) {
            //statusStepChallengeDB.emptyDBBucket();
            Log.d("CHALLENGESTATUS", AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + challenge_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("STATUSRESP", response);
                            try {
                                values.put(statusStepChallengeDB.CHALLENGE_ID, challenge_id);

                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                   // ContentValues dayvalues=new ContentValues();
                                    //dayvalues.put(DaysWiseDB.CHALLENGE_ID,challenge_id);
                                    JSONArray jsonArray = jsonObject1.getJSONArray("activity");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String challenge_id = jsonObject2.getString("challenge_id");
                                        String user_id_ = jsonObject2.getString("user_id");
                                        user_type_for_challenge = jsonObject2.getString("user_type");
                                        if (user_type_for_challenge.equals("USER")) {
                                            user_id = user_id;
                                        } else {
                                            group_id = user_id_;
                                        }
                                        //  participent_user_type.setText(user_type_for_challenge);

                                        challenge_type = jsonObject2.getString("challenge_type");
                                       // dayvalues.put(DaysWiseDB.CHALLENGE_TYPE,challenge_type);
                                        if (challenge_type.equals("DAILY")) {

                                            recyclerview_days_status_down.setVisibility(View.GONE);
                                            recyclerview_days_status_top.setVisibility(View.GONE);
                                        } else {
                                            recyclerview_days_status_down.setVisibility(View.VISIBLE);
                                            recyclerview_days_status_top.setVisibility(View.VISIBLE);
                                        }
                                        opponent_id = jsonObject2.getString("opponent_id");
                                        opponent_type = jsonObject2.getString("opponent_type");
                                        Log.v("Type", ">>" + opponent_type);
                                        values.put(StatusStepChallengeDB.OPPONENT_TYPE, opponent_type);
                                        if (opponent_type.equals("ADMIN")) {
                                            ll_down.setVisibility(View.GONE);
                                            vsTxt.setVisibility(View.GONE);
                                        } else {
                                            ll_down.setVisibility(View.VISIBLE);
                                            vsTxt.setVisibility(View.VISIBLE);
                                        }
                                        // participent_oponent_type.setText(opponent_type);
                                        challenge_goal = jsonObject2.getString("challenge_goal");
                                        total_steps = Integer.parseInt(challenge_goal);
                                        Log.d("dvkjvfsdkvjsdf", "" + total_steps);
                                        number_progress_bar.setMax(total_steps);
                                        number_progress_bar_opponent.setMax(total_steps);
                                        String amount = jsonObject2.getString("amount");
                                        values.put(StatusStepChallengeDB.AMOUNT, " $" + amount);
                                        cash_price.setText(" $" + amount);
                                        evaluation_factor = jsonObject2.getString("evaluation_factor");
                                        // dayvalues.put(DaysWiseDB.EVALULATION_FACTOR,evaluation_factor);
                                        String activity_name_string = jsonObject2.getString("activity_name");
                                        values.put(StatusStepChallengeDB.ACTIVITY_NAME, activity_name_string);
                                        activity_name.setText(activity_name_string);
                                        String activity_image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("activity_image");
                                        evaluation_factor_unit = jsonObject2.getString("evaluation_factor_unit");
                                        goal.setText(challenge_goal + " " + evaluation_factor_unit);
                                        values.put(StatusStepChallengeDB.CHALLENGE_GOAL, challenge_goal + " " + evaluation_factor_unit);
                                        String completed_on_txt = jsonObject2.getString("completed_on_txt");

                                    }
                                    String user_count = jsonObject1.getString("user_count");
                                    if (!user_count.equals("1")) {
                                        memb_count_top.setVisibility(View.VISIBLE);
                                        memb_count_top.setText("members " + user_count + "\n" + "see more..");

                                    } else {
                                        memb_count_top.setVisibility(View.GONE);
                                    }


                                    String opponent_count = jsonObject1.getString("opponent_count");
                                    if (!opponent_count.equals("1")) {
                                        mem_cccount_down.setVisibility(View.VISIBLE);
                                        mem_cccount_down.setText("members " + opponent_count + "\n" + "see more..");

                                    } else {
                                        mem_cccount_down.setVisibility(View.GONE);
                                    }
                                    // challenger_one_steps.setText(user_count);
                                    //  challenger_two_steps.setText(opponent_count);
                                    int u_count = Integer.parseInt(user_count);
                                    int o_count = Integer.parseInt(opponent_count);
                                    int total_count = Integer.parseInt(challenge_goal);
                                    int remaining_challenger_one = total_count - u_count;
                                    int remaining_challenger_two = total_count - o_count;
                                    // remaining_challenger_one_steps.setText(""+remaining_challenger_one);
                                    // remaining_challenger_two_steps.setText(""+remaining_challenger_two);
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("user_details");
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    usd_type_in = jsonObject3.getString("user_type");

                                    user_completed_goal = jsonObject3.getString("user_completed_goal");

                                    t = user_completed_goal;

                                    numStepsFromServer = Integer.parseInt(user_completed_goal);

                                   /* SharedPreferences preferences=getSharedPreferences("StepsCount",MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt("steps", Integer.parseInt(user_completed_goal));*/
                                    if (group_id != null && group_id.length() > 0)
                                        //  editor.putString("groupid",group_id);
                                        steps = Long.parseLong(user_completed_goal);
                                    if (total_steps <= Integer.parseInt(user_completed_goal)) {
                                        number_progress_bar.setProgress(total_steps);

                                    } else {
                                        number_progress_bar.setProgress(Integer.parseInt(user_completed_goal));

                                    }

                                    Log.d("steps_value_response", user_completed_goal);
                                    int user_completed_steps = Integer.parseInt(user_completed_goal);

                                    int total_goa = total_count - user_completed_steps;
                                    String total_goa_user = String.valueOf(total_goa);
                                    remaining_challenger_one_steps.setText(total_goa_user);

                                    remaining_challenger_one_steps.setText(total_goa_user);

///////////////////////////////////////////////Day WISE STATUS TOP/////////////////////////
                                    //  dayDateWiseStatusModel.clear();


                                    JSONArray jsonchallenger = jsonObject1.getJSONArray("challenger_daywise_status");
                                    Log.d("KDKDKDKDK", jsonchallenger.toString());

                                    for (int i = 0; i < jsonchallenger.length(); i++) {
                                        JSONObject jdataobj = jsonchallenger.getJSONObject(i);
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        dayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", dayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_top.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(StepChallengeStatusActivity.this);
                                    recyclerview_days_status_top.setLayoutManager(new LinearLayoutManager(StepChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    dayDateWiseChallengeStatusAdapter = new DayDateWiseChallengeStatusAdapter(dayDateWiseStatusModel, StepChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    recyclerview_days_status_top.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_top.setAdapter(dayDateWiseChallengeStatusAdapter);



////////////////////////////////////////END/////////////////////////////////////////////////////

                                    String user_name = jsonObject3.getString("user_name");

                                    String user_rank = jsonObject3.getString("user_rank");

                                    //  challenger_one_rank.setText(user_rank);

                                    String group_name = jsonObject3.getString("group_name");

                                    String group_rank = jsonObject3.getString("group_rank");


                                    String image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("image"); //if usertype is user

                                    String group_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("group_image");//if usertype is group

                                    String super_admin_name = jsonObject3.getString("super_admin_name");

                                    String super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject3.getString("super_admin_image");

                                    String super_admin_rank = jsonObject3.getString("super_admin_rank");


                                    if (usd_type_in.equals("USER")) {
                                        int steps=statusStepChallengeDB.latestGoal(challenge_id);
                                        if(steps==0){
                                            latesttotalsteps=Integer.parseInt(user_completed_goal);
                                        }else {
                                            latesttotalsteps=steps;
                                       }

                                        challenger_one_steps.setText("" + latesttotalsteps);
                                        Log.v("Latest123", ">>" + latesttotalsteps);
                                        participent_user_name.setText(user_name);
                                        challenger_one_rank.setText(user_rank);
                                        participent_user_type.setText(usd_type_in);
                                        Picasso.with(StepChallengeStatusActivity.this)
                                                .load(image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusStepChallengeDB.USER_NAME, user_name);
                                        values.put(StatusStepChallengeDB.USER_TYPE, usd_type_in);
                                        values.put(StatusStepChallengeDB.USER_COMPLETED_GOAL, latesttotalsteps);
                                        values.put(StatusStepChallengeDB.USER_RANK, user_rank);
                                        values.put(StatusStepChallengeDB.IMAGE, image);
                                    } else {
                                        int steps=statusStepChallengeDB.latestGoal(challenge_id);
                                        if(steps==0){
                                            latesttotalsteps=Integer.parseInt(user_completed_goal);
                                        }else {
                                            latesttotalsteps=steps;
                                        }
                                        participent_user_name.setText(group_name);
                                        participent_user_type.setText(usd_type_in);
                                        challenger_one_steps.setText(""+latesttotalsteps);
                                        challenger_one_rank.setText(group_rank);
                                        Picasso.with(StepChallengeStatusActivity.this)
                                                .load(group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_one_image);
                                        values.put(StatusStepChallengeDB.USER_NAME, group_name);
                                        values.put(StatusStepChallengeDB.USER_TYPE, usd_type_in);
                                        values.put(StatusStepChallengeDB.USER_COMPLETED_GOAL, latesttotalsteps);
                                        values.put(StatusStepChallengeDB.USER_RANK, group_rank);
                                        values.put(StatusStepChallengeDB.IMAGE, group_image);
                                    }

                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("opponent_details");
                                    JSONObject jsonObject4 = jsonArray2.getJSONObject(0);
                                    opponenttype_in = jsonObject4.getString("opponent_type");
                                    opponent_completed_goal = jsonObject4.getString("opponent_completed_goal");
                                    if (opponent_completed_goal != null && !opponent_completed_goal.equalsIgnoreCase("null"))
                                        if (total_steps <= Integer.parseInt(opponent_completed_goal)) {
                                            number_progress_bar_opponent.setProgress(total_steps);
                                        } else {
                                            number_progress_bar_opponent.setProgress(Integer.parseInt(opponent_completed_goal));
                                        }

                                    String opponent_name = jsonObject4.getString("opponent_name");
                                    // participent_oponent_name.setText(opponent_name);
                                    String opponent_rank = jsonObject4.getString("opponent_rank");
                                    // challenger_two_rank.setText(opponent_rank);
                                    int oponent_completed_steps = Integer.parseInt(opponent_completed_goal);
                                    int total = total_count - oponent_completed_steps;
                                    String total_goaL_opo = String.valueOf(total);
                                    if (total_goaL_opo.equals(challenge_goal)) {
                                        remaining_challenger_two_steps.setText("0");
                                    } else {
                                        remaining_challenger_two_steps.setText(total_goa_user);
                                    }
                                    remaining_challenger_two_steps.setText(total_goaL_opo);

                                    ///////////////////////////////////////////////Day WISE STATUS DOWN/////////////////////////


                                    JSONArray jsonopponent = jsonObject1.getJSONArray("opponent_daywise_status");
                                    Log.d("KDKDKDKDK", jsonopponent.toString());
                                    for (int i = 0; i < jsonopponent.length(); i++) {
                                        DayDateWiseStatusModel allmember = new DayDateWiseStatusModel();
                                        JSONObject jdataobj = jsonopponent.getJSONObject(i);

                                        allmember.setDate(jdataobj.getString("date"));
                                        allmember.setStatus(jdataobj.getString("status"));
                                        allmember.setValue(jdataobj.getString("value"));

                                        opponentdayDateWiseStatusModel.add(allmember);
                                        Log.d("dd", opponentdayDateWiseStatusModel.toString());

                                    }
                                    recyclerview_days_status_down.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(StepChallengeStatusActivity.this);
                                    recyclerview_days_status_down.setLayoutManager(new LinearLayoutManager(StepChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    opponentDayDateWiseChallengeStatusAdapter = new OpponentDayDateWiseChallengeStatusAdapter(opponentdayDateWiseStatusModel, StepChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluation_factor);
                                    recyclerview_days_status_down.setNestedScrollingEnabled(false);
                                    recyclerview_days_status_down.setAdapter(opponentDayDateWiseChallengeStatusAdapter);

////////////////////////////////////////END/////////////////////////////////////////////////////


                                    String opponent_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_image");
                                    String opponent_group_name = jsonObject4.getString("opponent_group_name");
                                    String opponent_group_rank = jsonObject4.getString("opponent_group_rank");
                                    String opponent_group_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_group_image");
                                    String opponent_super_admin_name = jsonObject4.getString("opponent_super_admin_name");
                                    String opponent_super_admin_rank = jsonObject4.getString("opponent_super_admin_rank");
                                    String opponent_super_admin_image = AppUrls.BASE_IMAGE_URL + jsonObject4.getString("opponent_super_admin_image");

                                    if (opponenttype_in.equals("USER")) {

                                        participent_oponent_name.setText(opponent_name);
                                        participent_oponent_type.setText(opponenttype_in);
                                        challenger_two_rank.setText(opponent_rank);
                                        challenger_two_steps.setText(opponent_completed_goal);
                                        Picasso.with(StepChallengeStatusActivity.this)
                                                .load(opponent_image)
                                                .placeholder(R.drawable.dummy_user_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusStepChallengeDB.OPPONENT_NAME, opponent_name);
                                        values.put(StatusStepChallengeDB.OPPONENT_RANK, opponent_rank);
                                        values.put(StatusStepChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_completed_goal);
                                        values.put(StatusStepChallengeDB.OPPONENT_IMAGE, opponent_image);
                                    } else {
                                        participent_oponent_name.setText(opponent_group_name);
                                        participent_oponent_type.setText(opponenttype_in);
                                        challenger_two_rank.setText(opponent_group_rank);
                                        challenger_two_steps.setText(opponent_completed_goal);
                                        Picasso.with(StepChallengeStatusActivity.this)
                                                .load(opponent_group_image)
                                                .placeholder(R.drawable.dummy_group_profile)
                                                .into(challenger_two_image);
                                        values.put(StatusStepChallengeDB.OPPONENT_NAME, opponent_group_name);
                                        values.put(StatusStepChallengeDB.OPPONENT_RANK, opponent_group_rank);
                                        values.put(StatusStepChallengeDB.OPPONENT_COMPLETED_GOAL, opponent_completed_goal);
                                        values.put(StatusStepChallengeDB.OPPONENT_IMAGE, opponent_group_image);
                                    }
                                     boolean isExit=statusStepChallengeDB.CheckIsDataAlreadyInDBorNot(challenge_id);
                                    if(!isExit){
                                        statusStepChallengeDB.addSteps(values);
                                    }


                                   /* editor.putString("activityid",getIntent().getExtras().getString("activity_id"));
                                    editor.putString("challenge_id",challenge_id);
                                    editor.putString("evaluation_factor",evaluation_factor);
                                    editor.putString("evaluation_factor_unit",evaluation_factor_unit);
                                    editor.putString("device_id", Settings.Secure.getString(StepChallengeStatusActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID));
                                    editor.apply();*/

                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(StepChallengeStatusActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (response_code.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(StepChallengeStatusActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER" + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(StepChallengeStatusActivity.this);
            requestQueue.add(strRe);
        } else {
            progressDialog.cancel();
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            statusStepsData();
           if(isFirstTime())
            Toast.makeText(StepChallengeStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        msgflag = 0;
                        String totalsteps = challenger_one_steps.getText().toString();
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                    StepChallengeStatusActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  StepChallengeStatusActivity.super.onBackPressed();

                }
            });
            builder.show();
        }

        if (v == memb_count_top) {
            Intent top = new Intent(StepChallengeStatusActivity.this, MemberCountChllengeStatusActivity.class);
            top.putExtra("challenge_id", challenge_id);
            top.putExtra("group_id", group_id);
            startActivity(top);
        }
        if (v == mem_cccount_down) {
            Intent bottom = new Intent(StepChallengeStatusActivity.this, MemberCountChllengeStatusActivity.class);
            bottom.putExtra("challenge_id", challenge_id);
            bottom.putExtra("group_id", group_id);
            startActivity(bottom);
        }
        if(v==serviceBtn){
             sensorPreference = getSharedPreferences("Sensor", MODE_PRIVATE);
            // sensoreditor.putString("challengeId",challenge_id);
            boolean isStarted=sensorPreference.getBoolean("isStarted",false);
            String challengeId=sensorPreference.getString("challengeId","");
            //if(challengeId!=null && challengeId.length()>0){
                if(!challengeId.equalsIgnoreCase(challenge_id) && isStarted){
                    String name=sensorPreference.getString("activity","");
                    showPreviousService(name);
                    //Toast.makeText(StepChallengeStatusActivity.this,"Stop Previous Services",Toast.LENGTH_SHORT).show();
                }else {
                    sensoreditor.putString("challengeId",challenge_id);
                    sensoreditor.putString("activity",activity_name.getText().toString());
                    String text=serviceBtn.getText().toString();
                    if(text.equalsIgnoreCase("STOP")){
                        serviceBtn.setText("START");
                        sensoreditor.putBoolean("isStarted",false);
                        sensoreditor.apply();
                        update.setVisibility(View.GONE);
                        serviceBtn.setBackgroundColor(Color.GREEN);
                        stopSensors();

                    }else {
                        serviceBtn.setText("STOP");
                        sensoreditor.putBoolean("isStarted",true);
                        serviceBtn.setBackgroundColor(Color.RED);
                        mIsSensorUpdateEnabled =true;
                        initGpsListeners();
                        sManager.registerListener(eventListener,stepSensor,SensorManager.SENSOR_DELAY_UI);
                    }
                    sensoreditor.apply();
                }
            /*}else {

            }*/



        }
        if(v==gps_track_img){
            Intent it=new Intent(StepChallengeStatusActivity.this,GPSTrackActivity.class);
            it.putExtra("challengeId",challenge_id);
            it.putExtra("userid",user_id);
            startActivity(it);
        }

    }

    //show Alert with previous Challenge
private void  showPreviousService(String activity){

    AlertDialog alertDialog = new AlertDialog.Builder(
            StepChallengeStatusActivity.this).create();

    // Setting Dialog Title
    alertDialog.setTitle("Challenge");

    // Setting Dialog Message
    alertDialog.setMessage("Already Running "+ Html.fromHtml("<font color='#026c9b'>"+activity+"</font>")+"Challenge\nPlease stop challenge before start it.");

      alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {

        }
    });

    // Showing Alert Message
    alertDialog.show();
}


    /*@Override
    public void onSensorChanged(SensorEvent event) {

        registerForSensorEvents();

       *//* float steps = event.values[0];
        challenger_one_steps.setText((int) steps + "");*//*

     *//*     if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);

        }*//*

     *//*Sensor sensor = event.sensor;
        float[] values = event.values;
        int value = -1;

        if (values.length > 0) {
            value = (int) values[0];
        }
        if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            steps++;

            Log.d("ONCHNG", "ONCHNG");
            getDistanceRun(steps);
        }

*//*
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }*/

    private void setupDetectorTimestampUpdaterThread() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
              /*  textViewStepDetector.setText(DateUtils
                        .getRelativeTimeSpanString(timestamp));*/
            }
        };

        detectorTimeStampUpdaterThread = new Thread() {
            @Override
            public void run() {
                while (isRunning) {
                    try {
                        Thread.sleep(5000);
                        handler.sendEmptyMessage(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        detectorTimeStampUpdaterThread.start();
    }


    //update steps values to server
    private void getUpdateSteps() {
        checkInternet = NetworkChecking.isConnected(StepChallengeStatusActivity.this);
        if (checkInternet) {
            update.setVisibility(View.VISIBLE);
            int remaining_steps = total_steps - numStepsFromServer;
            String remain_steps = String.valueOf(remaining_steps);
            remaining_challenger_one_steps.setText(remain_steps);
            number_progress_bar.setMax(total_steps);
            if (total_steps <= numStepsFromServer) {
                number_progress_bar.setProgress(total_steps);

            } else {
                number_progress_bar.setProgress(numStepsFromServer);

            }

            if (remaining_steps >= total_steps) {
                // Toast.makeText(this, "Challenge Completed", Toast.LENGTH_SHORT).show();
                remaining_challenger_one_steps.setText("0");
                send_challenge_data.setVisibility(View.GONE);
                final String totalsteps = challenger_one_steps.getText().toString();
                //statusStepChallengeDB.updateGoal(String.valueOf(latesttotalsteps),challenge_id);
                send_challenge_data.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    }
                });
            }
            final String totalsteps = challenger_one_steps.getText().toString();
            statusStepChallengeDB.updateGoal(String.valueOf(latesttotalsteps),challenge_id);
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        msgflag = 0;
                        SharedPreferences preferences = getSharedPreferences("StepsCount", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("steps", 0);
                        editor.apply();
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });

        } else {
            update.setVisibility(View.VISIBLE);
            String steps_count_value = String.valueOf(steps);
            int step_val = Integer.parseInt(steps_count_value);
            int total = Integer.parseInt(steps_count);
            int tot = step_val + total;
            t = String.valueOf(tot);

            statusStepChallengeDB.updateGoal(String.valueOf(latesttotalsteps),challenge_id);
            challenger_one_steps.setText("" + latesttotalsteps);
            number_progress_bar.setMax(total_steps);
            if (total_steps <= tot) {
                number_progress_bar.setProgress(total_steps);

            } else {
                number_progress_bar.setProgress(tot);

            }

            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        String totalsteps = challenger_one_steps.getText().toString();
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });
        }

    }


    public float getDistanceRun(long steps) {
        checkInternet = NetworkChecking.isConnected(StepChallengeStatusActivity.this);

        float distance = (float) (steps * 78) / (float) 100000;
        if (checkInternet) {
            update.setVisibility(View.VISIBLE);
            steps_count = String.valueOf(steps);
            Log.d("steps_COUNT", steps_count + "//" + user_completed_goal);

            distance_calc = String.valueOf(distance);
            // int total = Integer.parseInt(user_completed_goal)+Integer.parseInt(steps_count);
            int total = Integer.parseInt(steps_count);
            t = String.valueOf(total);
            Log.d("steps_value", t);
            challenger_one_steps.setText(t);
            int steps_coun = (int) steps;
            int remaining_steps = total_steps - steps_coun;
            String remain_steps = String.valueOf(remaining_steps);
            remaining_challenger_one_steps.setText("" + remain_steps);
            number_progress_bar.setMax(total_steps);
            if (total_steps <= total) {
                number_progress_bar.setProgress(total_steps);

            } else {
                number_progress_bar.setProgress(total);

            }
            if (remaining_steps >= total_steps) {
                // Toast.makeText(this, "Challenge Completed", Toast.LENGTH_SHORT).show();
                remaining_challenger_one_steps.setText("0");
                send_challenge_data.setVisibility(View.VISIBLE);
                final String totalsteps = challenger_one_steps.getText().toString();
                send_challenge_data.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* if(Intenumber_progress_bar.setProgress(100))
                        {

                        }
                        else
                        {
                            sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, t, android_id, Build.BRAND, Build.DEVICE);
                        }*/
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    }
                });
            }
            Log.d("BBBBBBB", "BBBBBBB");

            /*new Thread(new Runnable() {
                @Override
                public void run() {

                    while (true) {
                        try {
                            Thread.sleep(10000);
                            Log.d("CALLLLLL", "CLLLLLLLL");
                            sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, t, android_id, Build.BRAND, Build.DEVICE);
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();*/
            final String totalsteps = challenger_one_steps.getText().toString();
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });
            //  sendChallenge(getIntent().getExtras().getString("activity_id"),challenge_id,user_id,evaluation_factor,evaluation_factor_unit,challenge_goal,android_id,Build.BRAND,Build.DEVICE);
       /* mBuilder.setContentTitle(steps_count)
                .setContentText(distance_calc+" meters")
                .setSmallIcon(R.drawable.challenges_icon);*/
            // String value = steps_count;
       /* try {
            int progress = Integer.parseInt(value);
            if (progress >= 0 && progress <= 100) {
                //   mBuilder.setProgress(Integer.parseInt(challenge_goal), progress, false);
                //   mNotifyManager.notify(id, mBuilder.build());
            }
        } catch (Exception e) {
            Log.e("", "Value set:" + value);
            e.printStackTrace();
        }*/

        } else {
            update.setVisibility(View.VISIBLE);
            String steps_count_value = String.valueOf(steps);
            int step_val = Integer.parseInt(steps_count_value);
            int total = Integer.parseInt(steps_count);
            int tot = step_val + total;
            t = String.valueOf(tot);

            //statusStepChallengeDB.updateGoal(t);
            challenger_one_steps.setText(latesttotalsteps);
            number_progress_bar.setMax(total_steps);
            if (total_steps <= tot) {
                number_progress_bar.setProgress(total_steps);

            } else {
                number_progress_bar.setProgress(tot);

            }

            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        String totalsteps = challenger_one_steps.getText().toString();
                        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                    } catch (Exception e) {

                    }
                }
            });
        }
        return distance;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onStart() {
       /* Log.v("OnStart","service is stoped");
        stopService(new Intent(StepChallengeStatusActivity.this,StepsCountService.class));*/
        super.onStart();
    }

    @Override
    protected void onResume() {
        MyApplication.getInstance().setConnectivityListener(this);

       /* Log.v("OnResume","service is stoped");
        stopService(new Intent(StepChallengeStatusActivity.this,StepsCountService.class));*/
        super.onResume();

    }

    public void sendChallenge(final String activity_id, final String challenge_id, final String user_id, final String evaluation_factor, final String evaluation_factor_unit, final String challenge_goal, final String android_id, final String brand, final String model) {
        checkInternet = NetworkChecking.isConnected(StepChallengeStatusActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS;
            Log.d("fbnxbjkx", url);

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHAL_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    SharedPreferences preferences = getSharedPreferences("StepsCount", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt("steps", 0);
                                    editor.apply();
                                    getChallengeStatus();
                                    if (msgflag == 0)
                                        Toast.makeText(StepChallengeStatusActivity.this, "Challenge Updating", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(StepChallengeStatusActivity.this, "Challenge Updating Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(StepChallengeStatusActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    if (user_type_for_challenge.equals("USER")) {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", "");
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec", String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    } else {
                        params.put("activity_id", activity_id);
                        params.put("challenge_id", challenge_id);
                        params.put("user_id", user_id);
                        params.put("group_id", group_id);
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", challenge_goal);
                        params.put("device_id", android_id);
                        params.put("device_name", brand);
                        params.put("device_model", model);
                        params.put("timezone_in_sec", String.valueOf(timeinsec));
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    }
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(StepChallengeStatusActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(StepChallengeStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {

            //show a No Internet Alert or Dialog
            //  update.setVisibility(View.VISIBLE);
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {
            //  update.setVisibility(View.VISIBLE);
            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);
            try {
                String totalsteps = challenger_one_steps.getText().toString();

                sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
            } catch (Exception e) {

            }

            // dismiss the dialog or refresh the activity
        }
    }

    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            // update.setVisibility(View.VISIBLE);
            ll_down.setVisibility(View.GONE);
            vsTxt.setVisibility(View.GONE);
            //show a No Internet Alert or Dialog
            //  Toast.makeText(this, "DisConnected", Toast.LENGTH_SHORT).show();

        } else {
            //   update.setVisibility(View.VISIBLE);
            ll_down.setVisibility(View.VISIBLE);
            vsTxt.setVisibility(View.VISIBLE);
            try {
                String totalsteps = challenger_one_steps.getText().toString();
                sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
            } catch (Exception e) {

            }
            // Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            // dismiss the dialog or refresh the activity
        }
    }

    public void statusStepsData() {
        SharedPreferences preferences = getSharedPreferences("StepsCount", MODE_PRIVATE);
        int latest = preferences.getInt("steps", 0);
        String challengeId = statusStepChallengeDB.ChallengeIDstr(challenge_id);
        Log.v("ChallengeId", challengeId);


        List<String> challeneg_Ids = statusStepChallengeDB.getChallengeID();

        List<String> activityName = statusStepChallengeDB.getActivityName();

        List<String> user_type = statusStepChallengeDB.getUserType();
        List<String> challenge_goal = statusStepChallengeDB.getChallengeGoal();
        List<String> amount = statusStepChallengeDB.getAmount();
        List<String> activity_name_string = statusStepChallengeDB.getActivityName();

        List<String> user_name = statusStepChallengeDB.getUserName();
        List<String> user_rank = statusStepChallengeDB.getUserRank();
        List<String> image = statusStepChallengeDB.getImage();
        List<String> oppentName = statusStepChallengeDB.getopponentName();
        List<String> oppentrank = statusStepChallengeDB.getopponentrank();

        List<String> oppentImage = statusStepChallengeDB.getopponentImge();
        List<String> oppenttype = statusStepChallengeDB.getopponentType();

        List<String> user_complet_goal = statusStepChallengeDB.getUserCompletedGoal();

        List<String> opponentgoal = statusStepChallengeDB.getopponentgoal();

       for (int i = 0; i < challeneg_Ids.size(); i++) {
            if (challeneg_Ids.get(i).equalsIgnoreCase(challengeId)) {
                activity_name.setText(activity_name_string.get(i));
                Log.v("User",activity_name_string.get(i));
                cash_price.setText(amount.get(i));
                activity_name.setText(activity_name_string.get(i));
                goal.setText(challenge_goal.get(i));
                usd_type_in = user_type.get(i);
                total_steps = Integer.parseInt(challenge_goal.get(i).replace(" STEPS", ""));
                number_progress_bar.setMax(total_steps);
                participent_user_name.setText(user_name.get(i));
                participent_user_type.setText(usd_type_in);

                steps_count = user_complet_goal.get(i);
                Log.v("Goal",user_complet_goal.get(i));
                Integer.parseInt(user_complet_goal.get(i));
                latesttotalsteps= Integer.parseInt(user_complet_goal.get(i));;
                  int count=lateststeps+latest;
                challenger_one_steps.setText(""+latesttotalsteps);
                challenger_one_rank.setText(user_rank.get(i));
                Picasso.with(StepChallengeStatusActivity.this)
                        .load(image.get(i))
                        .placeholder(R.drawable.dummy_user_profile)
                        .into(challenger_one_image);
                if (total_steps <= Integer.parseInt(user_complet_goal.get(i))) {
                    number_progress_bar.setProgress(total_steps);

                } else {
                    number_progress_bar.setProgress(Integer.parseInt(user_complet_goal.get(i)));

                }

                String opponenttype=oppenttype.get(i);
                if (opponenttype.equals("ADMIN")) {
                    ll_down.setVisibility(View.GONE);
                    vsTxt.setVisibility(View.GONE);
                } else {
                    ll_down.setVisibility(View.VISIBLE);
                    vsTxt.setVisibility(View.VISIBLE);
                    participent_oponent_name.setText(oppentName.get(i));
                    if(opponentType.contains("Group") || opponentType.contains("GROUP"))
                        participent_oponent_type.setText("GROUP");
                    else
                        participent_oponent_type.setText("USER");
                    challenger_two_rank.setText(oppentrank.get(i));
                    challenger_two_steps.setText(opponentgoal.get(i));
                    Picasso.with(StepChallengeStatusActivity.this)
                            .load(oppentImage.get(i))
                            .placeholder(R.drawable.dummy_user_profile)
                            .into(challenger_two_image);

                }
           }
        }

    }


    private void getDaysWiseFromDB(){

        List<String> dates = daysWiseDB.getDates();
        List<String> statuslist = daysWiseDB.getStatuses();
        List<String> valueslist = daysWiseDB.getValues();
        String evaluationfactor=daysWiseDB.getEvalulationFactor();
        if(dates.size()>0){
            for (int i=0;i<dates.size();i++){
                DayDateWiseStatusModel daystastus=new DayDateWiseStatusModel();
                daystastus.setDate(dates.get(i));
                daystastus.setStatus(statuslist.get(i));
                daystastus.setValue(valueslist.get(i));
                dayDateWiseStatusModel.add(daystastus);
            }
            recyclerview_days_status_top.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(StepChallengeStatusActivity.this);
            recyclerview_days_status_top.setLayoutManager(new LinearLayoutManager(StepChallengeStatusActivity.this, LinearLayoutManager.HORIZONTAL, false));
            dayDateWiseChallengeStatusAdapter = new DayDateWiseChallengeStatusAdapter(dayDateWiseStatusModel, StepChallengeStatusActivity.this, R.layout.row_daywise_challegne, evaluationfactor);
            recyclerview_days_status_top.setNestedScrollingEnabled(false);
            recyclerview_days_status_top.setAdapter(dayDateWiseChallengeStatusAdapter);
        }

    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("User must send data to server otherwise data will not update. Click Save to save the data otherwise Click Discard ");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    String totalsteps = challenger_one_steps.getText().toString();
                    msgflag = 0;
                    sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
                } catch (Exception e) {

                }
                StepChallengeStatusActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                StepChallengeStatusActivity.super.onBackPressed();
            }
        });
        builder.show();
    }

    public void updateThread() {
        String totalsteps = challenger_one_steps.getText().toString();
        sendChallenge(getIntent().getExtras().getString("activity_id"), challenge_id, user_id, evaluation_factor, evaluation_factor_unit, totalsteps, android_id, Build.BRAND, Build.DEVICE);
    }

    public void acquireWakeLock() {
        final PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        releaseWakeLock();
        //Acquire new wake lock
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PARTIAL_WAKE_LOCK");
        mWakeLock.acquire();
    }

    public void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CLICKKKKKK", "CLICKKKKKK");

        isRunning = false;
        detectorTimeStampUpdaterThread.interrupt();
    }


    public void openWhatsApp() {
        Log.d("KKKTETERT", "KKKTETERT");

       /* Uri uri = Uri.parse("smsto:" + "8574585475");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "HIIIIIIIIII");
        i.setPackage("com.whatsapp");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);*/

       /* Intent intent = new Intent(this, ReferFriendActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/

        Thread thread = new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            runOnUiThread(new Runnable() {
                                public void run() {
                              /*  if (progressDialog.isShowing())
                                    progressDialog.dismiss();*/
                                    //getDistanceRun(steps);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                });
            }
        });
        thread.start();


    }


    //receive steps
    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Here you have the received broadcast
            // And if you added extras to the intent get them here too
            // this needs some null checks
            Log.v("Test", "....");
            Bundle b = intent.getExtras();
            int lateststep = b.getInt("steps");
            challenger_one_steps.setText("" + lateststep);


        }
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }


}
