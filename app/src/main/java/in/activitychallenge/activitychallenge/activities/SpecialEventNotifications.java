package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.SpecialEventAdapter;
import in.activitychallenge.activitychallenge.adapter.SpecialEventNotificationAdapter;
import in.activitychallenge.activitychallenge.models.SpecialEventModel;
import in.activitychallenge.activitychallenge.models.SpecialEventNotificationModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SpecialEventNotifications extends AppCompatActivity implements View.OnClickListener{
    RecyclerView specialevent_notification_recycleview;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    SpecialEventNotificationAdapter eventAdapter;
    ImageView no_notification_img;
    ArrayList<SpecialEventNotificationModel> specialEventNotificationModels=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_event_notifications);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String eventid=getIntent().getStringExtra("eventid");
        specialevent_notification_recycleview = (RecyclerView) findViewById(R.id.special_event_notifi_recyle);
        no_notification_img=(ImageView)findViewById(R.id.no_notifications_img);
        ImageView close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();

        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        eventAdapter = new SpecialEventNotificationAdapter(specialEventNotificationModels, SpecialEventNotifications.this, R.layout.specialevent_notification_row);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        specialevent_notification_recycleview.setNestedScrollingEnabled(false);
        specialevent_notification_recycleview.setLayoutManager(layoutManager);

        getSpecialEventNotifications(eventid);

    }

    //get special event notifications
    private void getSpecialEventNotifications(final String id) {
        checkInternet = NetworkChecking.isConnected(SpecialEventNotifications.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_NOTIFICATIONS+id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            //  pprogressDialog.dismiss();

                            Log.d("Special event", AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_NOTIFICATIONS+id);
                            Log.d("Events", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);
                                        SpecialEventNotificationModel eventModel = new SpecialEventNotificationModel();
                                        eventModel.setNotificationtext(jsonObject1.getString("text"));
                                        eventModel.setCreateddate(jsonObject1.getString("created_on"));
                                        specialEventNotificationModels.add(eventModel);

                                    }
                                    specialevent_notification_recycleview.setAdapter(eventAdapter);
                                    if (jarray.length() == 0) {
                                        no_notification_img.setVisibility(View.VISIBLE);
                                    }


                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SpecialEventNotifications.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(SpecialEventNotifications.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.close){
            finish();
        }
    }
}
