package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.UserNotificationsAdapter;
import in.activitychallenge.activitychallenge.models.UserNotification;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class UserNotificationsActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerNotification;
    UserSessionManager session;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    ImageView no_events_img;
    ArrayList<UserNotification> notificationArrayList;
    UserNotificationsAdapter notificationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notifications);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        recyclerNotification = (RecyclerView) findViewById(R.id.recyclerNotifiation);
        no_events_img = (ImageView) findViewById(R.id.no_events_img);


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("UserDetails", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        getNotificationData();

    }

    private void getNotificationData() {
        checkInternet = NetworkChecking.isConnected(UserNotificationsActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  pprogressDialog.dismiss();
                            Log.d("Notification", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    notificationArrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        UserNotification notification = new UserNotification(jsonArray.getJSONObject(i));
                                        notificationArrayList.add(notification);
                                    }

                                    notificationsAdapter = new UserNotificationsAdapter(notificationArrayList, UserNotificationsActivity.this, R.layout.user_notification_row);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(UserNotificationsActivity.this);
                                    recyclerNotification.setNestedScrollingEnabled(false);
                                    recyclerNotification.setLayoutManager(layoutManager);
                                    recyclerNotification.setAdapter(notificationsAdapter);

                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UserNotificationsActivity.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(UserNotificationsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
}
