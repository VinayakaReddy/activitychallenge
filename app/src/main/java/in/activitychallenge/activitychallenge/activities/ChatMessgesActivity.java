package in.activitychallenge.activitychallenge.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MyChllengesViewPagerAdapter;
import in.activitychallenge.activitychallenge.fragments.GroupChatFragment;
import in.activitychallenge.activitychallenge.fragments.SponsorChatFragment;
import in.activitychallenge.activitychallenge.fragments.UserChatFragment;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ChatMessgesActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    ViewPager view_pager_chat_msg;
    TabLayout tab;
    MyChllengesViewPagerAdapter chat_messageadapter;
    String type;
    ImageView user_chat_search,group_chat_search,sponsor_chat_search;
    String device_id, access_token, user_id,user_type;
    UserSessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messges);


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        user_chat_search = (ImageView) findViewById(R.id.user_chat_search);
        user_chat_search.setOnClickListener(this);
        group_chat_search = (ImageView) findViewById(R.id.group_chat_search);
        group_chat_search.setOnClickListener(this);
        sponsor_chat_search = (ImageView) findViewById(R.id.sponsor_chat_search);
        sponsor_chat_search.setOnClickListener(this);

        tab = (TabLayout) findViewById(R.id.tabLayout_chat_msg);
        Bundle b = getIntent().getExtras();
        type = b.getString("condition");
        view_pager_chat_msg = (ViewPager) findViewById(R.id.view_pager_chat_msg);

        setupViewPager(view_pager_chat_msg);
        tab.setupWithViewPager(view_pager_chat_msg);


    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        chat_messageadapter = new MyChllengesViewPagerAdapter(getSupportFragmentManager());
        if (type.equals("GROUP_NOT_USER"))
        {
            view_pager_suggest.setCurrentItem(2);
            chat_messageadapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_messageadapter.addFrag(new UserChatFragment(), "User Chat");
            chat_messageadapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        else if(user_type.equals("SPONSOR"))
        {
            chat_messageadapter.addFrag(new UserChatFragment(), "User Chat");
            chat_messageadapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_messageadapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        else
        {
            view_pager_suggest.setCurrentItem(1);
            chat_messageadapter.addFrag(new UserChatFragment(), "User Chat");
            chat_messageadapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_messageadapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        view_pager_suggest.setAdapter(chat_messageadapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_chat_msg) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 0)
                        {
                            user_chat_search.setVisibility(View.VISIBLE);
                            group_chat_search.setVisibility(View.GONE);
                            sponsor_chat_search.setVisibility(View.GONE);
                        }
                        else if(tab.getPosition() == 1)
                        {
                           user_chat_search.setVisibility(View.GONE);
                           sponsor_chat_search.setVisibility(View.GONE);
                           group_chat_search.setVisibility(View.VISIBLE);

                        }
                        else if(tab.getPosition() == 2)
                        {
                            user_chat_search.setVisibility(View.GONE);
                            group_chat_search.setVisibility(View.GONE);
                            sponsor_chat_search.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            //nothing
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
        if (view == close) {
            finish();
        }

        if (view == user_chat_search)
        {
            //Toast.makeText(ChatMessgesActivity.this, "USER!", Toast.LENGTH_LONG).show();
            Intent allmembers = new Intent(ChatMessgesActivity.this, AllMembersActivity.class);
            startActivity(allmembers);
        }

        if (view == group_chat_search)
        {
            Intent allmembers = new Intent(ChatMessgesActivity.this, AllGroupsActivity.class);
            startActivity(allmembers);
        }
        if (view == sponsor_chat_search)
        {
            Intent allmembers = new Intent(ChatMessgesActivity.this, AllSponsorActivity.class);
            startActivity(allmembers);
        }


    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userchat_search, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:

                Toast.makeText(ChatMessgesActivity.this,"CLICKED",Toast.LENGTH_LONG).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}
