package in.activitychallenge.activitychallenge.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MeAsSponsorActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    Button sponsor_user, sponsor_group,sponsor_app,sponsor_activity;
    Dialog dialog;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String user_id, access_token, device_id, user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me_as_sponsor);


        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        checkInternet = NetworkChecking.isConnected(this);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        sponsor_user = (Button) findViewById(R.id.sponsor_user);
        sponsor_user.setOnClickListener(this);
        sponsor_group = (Button) findViewById(R.id.sponsor_group);
        sponsor_group.setOnClickListener(this);
        sponsor_app = (Button) findViewById(R.id.sponsor_app);
        sponsor_app.setOnClickListener(this);
        sponsor_activity = (Button) findViewById(R.id.sponsor_activity);
        sponsor_activity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }

        if (v == sponsor_user) {
            Intent intent = new Intent(MeAsSponsorActivity.this, AllMembersActivity.class);
            startActivity(intent);
        }

        if (v == sponsor_group) {
            Intent intent = new Intent(MeAsSponsorActivity.this, AllGroupsActivity.class);
            startActivity(intent);
        }
        if (v == sponsor_activity) {
            Intent intent = new Intent(MeAsSponsorActivity.this, SponsorAsActivity.class);
            startActivity(intent);
        }
        if (v == sponsor_app)
        {
            dialog = new Dialog(MeAsSponsorActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_sponsor_app);
            final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
            final Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);

            sendMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();

                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_send_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                       sendWalletAmount(send_amount);

                    }
                }
            });

            dialog.show();
        }
    }

    private void sendWalletAmount(String send_amount)
    {
        Intent intent = new Intent(MeAsSponsorActivity.this, ChallengePaymentActivity.class);
        intent.putExtra("activity", "MeAsaSponsorAPP");

        intent.putExtra("sponsorAmount", send_amount);
      Log.d("SPONSOR",  send_amount);
        startActivity(intent);
    }
}
