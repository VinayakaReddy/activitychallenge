package in.activitychallenge.activitychallenge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ForgetChangePassword extends AppCompatActivity implements View.OnClickListener {

    String nonce, device_id, user_id, country_code, mobile_no;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    Typeface typeface, typeface2;
    EditText new_password_edt, confirm_pass_edt;
    TextView forget_changepass_txt;
    TextInputLayout new_password_til, confirm_pass_til;
    ImageView forget_chngpass_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_change_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mp_bold));
        Bundle bundle = getIntent().getExtras();
        nonce = bundle.getString("NONCE");
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        country_code = userDetails.get(UserSessionManager.COUNTRY_CODE);
        mobile_no = userDetails.get(UserSessionManager.MOBILE_NO);
        Log.d("DD", nonce + "//" + user_id + "//" + device_id);
        pprogressDialog = new ProgressDialog(ForgetChangePassword.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        new_password_edt = (EditText) findViewById(R.id.new_password_edt);
        new_password_edt.setTypeface(typeface);
        confirm_pass_edt = (EditText) findViewById(R.id.confirm_pass_edt);
        confirm_pass_edt.setTypeface(typeface);
        new_password_til = (TextInputLayout) findViewById(R.id.new_password_til);
        confirm_pass_til = (TextInputLayout) findViewById(R.id.confirm_pass_til);
        forget_changepass_txt = (TextView) findViewById(R.id.forget_changepass_txt);
        forget_changepass_txt.setTypeface(typeface2);
        forget_chngpass_btn = (ImageView) findViewById(R.id.forget_chngpass_btn);
        forget_chngpass_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == forget_chngpass_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    final String password = new_password_edt.getText().toString().trim();
                    Log.d("REGURL", AppUrls.BASE_URL + AppUrls.CHANGE_PASSWORD);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CHANGE_PASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    pprogressDialog.dismiss();
                                    Log.d("RESPONCELREG", response);
                                    try {

                                        JSONObject jsonObject = new JSONObject(response);
                                        String editSuccessResponceCode = jsonObject.getString("response_code");
                                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "password has been reset successfully....! ", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(ForgetChangePassword.this, LoginActivity.class);
                                            startActivity(intent);

                                        }
                                        if (editSuccessResponceCode.equals("10200")) {
                                            Toast.makeText(getApplicationContext(), "Invalid input..!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10300")) {
                                            Toast.makeText(getApplicationContext(), "User not exist..!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10400")) {
                                            Toast.makeText(getApplicationContext(), "Sorry, Error encountered while processing your request.!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10500")) {
                                            Toast.makeText(getApplicationContext(), " Sorry, Unable to process your request, invalid nonce..!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    pprogressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("password", password);
                            params.put("nonce", nonce);
                            params.put("device_id", device_id);
                            Log.d("RegisterREQUESTDATA:", params.toString());
                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(ForgetChangePassword.this);
                    requestQueue.add(stringRequest);

                } else {
                    Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private boolean validate() {
        int flag = 0;
        boolean result = true;
        String password = new_password_edt.getText().toString().trim();
        if (password.isEmpty() || password.length() < 6) {
            new_password_til.setError("Minimum 6 characters required");
            result = false;
        } else {
            new_password_til.setErrorEnabled(false);
        }
        String confrm_password = confirm_pass_edt.getText().toString().trim();
        if (confrm_password.isEmpty() || password.length() < 6) {
            confirm_pass_til.setError("Minimum 6 characters required");
            result = false;
        } else {
            confirm_pass_til.setErrorEnabled(false);
        }
        if (password != "" && confrm_password != "" && !confrm_password.equals(password) && flag == 0) {
            //password_edt.setError("Password and Confirm not Match");
            confirm_pass_til.setError("Password and Confirm Password not Match");
            result = false;
        } else {
            confirm_pass_til.setErrorEnabled(false);
        }
        return result;

    }
}
