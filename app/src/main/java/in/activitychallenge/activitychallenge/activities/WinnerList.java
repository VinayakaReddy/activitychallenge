package in.activitychallenge.activitychallenge.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.LotteryListHolder;
import in.activitychallenge.activitychallenge.models.LotteryListModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class WinnerList extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    RecyclerView lot_recyc;
    LinearLayoutManager llay;
    WinnerListAdapter lotAdap;
    UserSessionManager session;
    ArrayList<LotteryListModel> loList = new ArrayList<LotteryListModel>();
    String device_id, access_token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery_list);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SELWINNDATA", access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        lot_recyc = (RecyclerView) findViewById(R.id.lot_recyc);
        lotAdap = new WinnerListAdapter(WinnerList.this, loList, R.layout.row_lotterylist);
        llay = new LinearLayoutManager(this);
        lot_recyc.setNestedScrollingEnabled(false);
        lot_recyc.setLayoutManager(llay);

        getWinnerList();

    }

    private void getWinnerList() {
        checkInternet = NetworkChecking.isConnected(WinnerList.this);

        if (checkInternet) {
            String getLotId = getIntent().getExtras().getString("Lotter_ID");
            Log.d("fsdfdfd", getLotId);
            Log.d("WINNER_URL", AppUrls.BASE_URL + AppUrls.WINNER);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.WINNER + "?id=" + getLotId,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("WINNERRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();

                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        LotteryListModel moMo = new LotteryListModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        JSONObject jArrOb = itemArray.getJSONObject("user_id");
                                        moMo.set_id(jArrOb.getString("_id"));
                                        moMo.setFirst_name(jArrOb.getString("first_name"));
                                        moMo.setLast_name(jArrOb.getString("last_name"));
                                        moMo.setTicket(itemArray.getString("ticket"));
                                        String llAmo = itemArray.getString("amount");
                                        moMo.setAmount(llAmo + "\n" + "USD");
                                        moMo.setUser_type(itemArray.getString("user_type"));
                                        loList.add(moMo);
                                    }
                                    lot_recyc.setAdapter(lotAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(WinnerList.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("WINNER_HEADER", "HEdDDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(WinnerList.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(WinnerList.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }


    public class WinnerListAdapter extends RecyclerView.Adapter<LotteryListHolder> {
        WinnerList context;
        int resource;
        ArrayList<LotteryListModel> lotList;
        LayoutInflater lInfla;

        public WinnerListAdapter(WinnerList context, ArrayList<LotteryListModel> lotList, int resource) {
            this.context = context;
            this.resource = resource;
            this.lotList = lotList;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public LotteryListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            LotteryListHolder rHol = new LotteryListHolder(inf);
            return rHol;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBindViewHolder(LotteryListHolder holder, int position) {
            holder.fName.setText(lotList.get(position).getFirst_name());
            holder.lName.setText(lotList.get(position).getLast_name());
            holder.tktNumber.setText(lotList.get(position).getTicket());
            holder.amountWon.setText(lotList.get(position).getAmount());
            if (position == 0) {
                holder.card_lottery.setBackgroundResource(R.drawable.one_gra);
            }
            if (position == 1) {
                holder.card_lottery.setBackgroundResource(R.drawable.two_gra);
            }
            if (position == 2) {
                holder.card_lottery.setBackgroundResource(R.drawable.three_gra);
            }

        }

        @Override
        public int getItemCount() {
            return lotList.size();
        }
    }


}
