package in.activitychallenge.activitychallenge;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.activitychallenge.activitychallenge.filters.CustomFilterForAllActivityWinLossList;
import in.activitychallenge.activitychallenge.fragments.CategoryWiseWinLossFragment;
import in.activitychallenge.activitychallenge.models.ActivityDataModel;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder>  implements Filterable {
    public ArrayList<ActivityDataModel> activityDataList = new ArrayList<ActivityDataModel>();
    public ArrayList<ActivityDataModel> allmembersfilterList = new ArrayList<ActivityDataModel>();
   // private BannerAdapterHelper mBannerAdapterHelper = new BannerAdapterHelper();
    CategoryWiseWinLossFragment context;
    int  won_challenge,lost_challenge,draw_challenge;
    int resource;
    CustomFilterForAllActivityWinLossList filter;

    public CardAdapter(ArrayList<ActivityDataModel> activityDataList,CategoryWiseWinLossFragment context,int resource) {
        this.activityDataList = activityDataList;
        this.allmembersfilterList = activityDataList;
        this.context = context;
        this.resource = resource;
    }

   /* public ArrayList<ActivityDataModel> getList()
    {
        return activityDataList;
    }

    public void setList(ArrayList<ActivityDataModel> list)
    {
        activityDataList = list;
    }*/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_card_item, parent, false);
      //  mBannerAdapterHelper.onCreateViewHolder(parent, itemView);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        //  mBannerAdapterHelper.onBindViewHolder(holder.itemView, position, getItemCount());

        String path= activityDataList.get(position).getActivity_image();
        int total= Integer.parseInt(activityDataList.get(position).getTotal_challenges());

        Picasso.with(context.getActivity()).load(path).fit()
               .error(R.drawable.placeholder_dummy)
               .into(holder.mImageView);

        holder.winloss_act_name.setText(Html.fromHtml( activityDataList.get(position).getActivity_name()));
        holder.total_challenges.setText(Html.fromHtml("Total Challenges   :    "+activityDataList.get(position).getTotal_challenges()));
        holder.total_prize.setText(Html.fromHtml("Total Amount   :    "+"$"+activityDataList.get(position).getTotal_amount()));

        Log.d("DATAAA",activityDataList.get(position).getWin_challenges()+"/////"+activityDataList.get(position).getLoss_challenges()+"/////"+activityDataList.get(position).getTie_challenges());

        won_challenge= Integer.parseInt(activityDataList.get(position).getWin_challenges()) * 100/total;

        lost_challenge=Integer.parseInt(activityDataList.get(position).getLoss_challenges()) * 100/total;

        draw_challenge=Integer.parseInt(activityDataList.get(position).getTie_challenges()) * 100/total;

       Log.d("VALUE",won_challenge+"/////"+lost_challenge+"/////"+draw_challenge);

         holder.number_won_progress_bar.setMax(100);
        holder.number_won_progress_bar.setProgress(won_challenge);
         holder.number_lost_progress_bar.setMax(100);
        holder.number_lost_progress_bar.setProgress(lost_challenge);
         holder.number_draw_progress_bar.setMax(100);
        holder.number_draw_progress_bar.setProgress(draw_challenge);





       /* holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((RecyclerView) holder.itemView.getParent()).smoothScrollToPosition(position);
            }
        });*/
    }

    @Override
    public int getItemCount() {
         return this.activityDataList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForAllActivityWinLossList(allmembersfilterList, this);
        }

        return filter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public  ImageView mImageView;
        public  NumberProgressBar number_won_progress_bar,number_lost_progress_bar,number_draw_progress_bar;
        public  TextView winloss_act_name,total_challenges,total_prize;

        public ViewHolder(final View itemView)
        {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView);
            number_won_progress_bar = (NumberProgressBar) itemView.findViewById(R.id.number_won_progress_bar);
            number_lost_progress_bar = (NumberProgressBar) itemView.findViewById(R.id.number_lost_progress_bar);
            number_draw_progress_bar = (NumberProgressBar) itemView.findViewById(R.id.number_draw_progress_bar);

            winloss_act_name = (TextView) itemView.findViewById(R.id.winloss_act_name);
            total_challenges = (TextView) itemView.findViewById(R.id.total_challenges);
            total_prize = (TextView) itemView.findViewById(R.id.total_prize);
        }

    }

}
