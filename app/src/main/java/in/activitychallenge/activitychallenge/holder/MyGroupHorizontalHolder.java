package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;

/**
 * Created by admin on 12/18/2017.
 */

public class MyGroupHorizontalHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView grp_rank;

    public TextView grp_name;


    public CircleImageView grp_img;
    MyGroupItemClickListener myGroupItemClickListener;



    public MyGroupHorizontalHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        grp_rank = (TextView) itemView.findViewById(R.id.grp_rank);

        grp_name = (TextView) itemView.findViewById(R.id.group_name);

        grp_img = (CircleImageView) itemView.findViewById(R.id.grp_img);



    }


    @Override
    public void onClick(View view) {
        this.myGroupItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyGroupItemClickListener ic)
    {
        this.myGroupItemClickListener=ic;
    }



}
