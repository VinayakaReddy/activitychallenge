package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.LotteryCodesItemClickListener;

public class LotteryCodesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView lottery_code_txt;
    LotteryCodesItemClickListener lotteryCodesItemClickListener;

    public LotteryCodesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        lottery_code_txt = (TextView) itemView.findViewById(R.id.lottery_code_txt);

    }

    @Override
    public void onClick(View view) {

        this.lotteryCodesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(LotteryCodesItemClickListener ic)
    {
        this.lotteryCodesItemClickListener = ic;
    }
}
