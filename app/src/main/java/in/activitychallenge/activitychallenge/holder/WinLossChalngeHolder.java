package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.WinLossChalngItemClickListener;


public class WinLossChalngeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


   public ImageView activity_image;
    public TextView activity_name,cash_won,opponent_name,date_text,status_text,cash_won_title_text;






    WinLossChalngItemClickListener winlossChalangeItemClickListener;

    public WinLossChalngeHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        activity_image=(ImageView)itemView.findViewById(R.id.activity_image);

        activity_name=(TextView) itemView.findViewById(R.id.activity_name);
        cash_won=(TextView) itemView.findViewById(R.id.cash_won);
        opponent_name=(TextView) itemView.findViewById(R.id.opponent_name);
        date_text=(TextView) itemView.findViewById(R.id.date_text);
        status_text=(TextView) itemView.findViewById(R.id.status_text);
        cash_won_title_text=(TextView) itemView.findViewById(R.id.cash_won_title_text);
    }

    @Override
    public void onClick(View view) {
        this.winlossChalangeItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(WinLossChalngItemClickListener ic)
    {
        this.winlossChalangeItemClickListener =ic;
    }
}
