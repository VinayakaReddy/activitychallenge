package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.PictureMemberCountClickListner;

/**
 * Created by admin on 12/4/2017.
 */

public class MemberPictureCountStatusHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public  TextView namee;
    public ImageView imgEval_faceto;
    PictureMemberCountClickListner pictureMemberCountClickListner;

    public MemberPictureCountStatusHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        namee = (TextView) itemView.findViewById(R.id.namee);
        imgEval_faceto = (ImageView) itemView.findViewById(R.id.imgEval_faceto);


    }

    @Override
    public void onClick(View view) {
        this.pictureMemberCountClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(PictureMemberCountClickListner ic)
    {
        this.pictureMemberCountClickListner =ic;
    }
}
