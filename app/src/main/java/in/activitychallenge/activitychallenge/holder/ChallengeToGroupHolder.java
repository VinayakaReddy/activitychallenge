package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;

/**
 * Created by admin on 12/18/2017.
 */

public class ChallengeToGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView grp_rank;
    public TextView grp_percent;
    public TextView grp_name;
    public TextView grp_memb_cnt;
    public TextView grp_won_cnt;
    public TextView grp_lost_cnt;
    public ImageView grp_img, grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5;
    public Button sendReq_butt;
    MyGroupItemClickListener myGroupItemClickListener;



    public ChallengeToGroupHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        grp_rank = (TextView) itemView.findViewById(R.id.grp_rank);
        grp_percent = (TextView) itemView.findViewById(R.id.grp_percent);
        grp_name = (TextView) itemView.findViewById(R.id.grp_name);
        grp_memb_cnt = (TextView) itemView.findViewById(R.id.grp_memb_cnt);
        grp_won_cnt = (TextView) itemView.findViewById(R.id.grp_won_cnt);
        grp_lost_cnt = (TextView) itemView.findViewById(R.id.grp_lost_cnt);
        grp_img = (ImageView) itemView.findViewById(R.id.grp_img);
        grp_img_1 = (ImageView) itemView.findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) itemView.findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) itemView.findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) itemView.findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) itemView.findViewById(R.id.grp_img_5);
        sendReq_butt = (Button) itemView.findViewById(R.id.sendReq_butt);

    }


    @Override
    public void onClick(View view) {
        this.myGroupItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyGroupItemClickListener ic)
    {
        this.myGroupItemClickListener=ic;
    }



}
