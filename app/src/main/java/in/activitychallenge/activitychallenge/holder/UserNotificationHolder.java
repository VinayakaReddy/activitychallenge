package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MyChallengesListItemClickListener;

public class UserNotificationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView notification_txt;
    public TextView created_date_txt;
    public RelativeLayout relativeLayout;
    MyChallengesListItemClickListener myChallengesListItemClickListener;

    public UserNotificationHolder(View itemView) {
        super(itemView);
        notification_txt = (TextView) itemView.findViewById(R.id.user_notification_txt);
        created_date_txt = (TextView) itemView.findViewById(R.id.user_notification_date);
        relativeLayout = (RelativeLayout) itemView.findViewById(R.id.dd);
    }

    @Override
    public void onClick(View view) {

        this.myChallengesListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyChallengesListItemClickListener ic) {
        this.myChallengesListItemClickListener = ic;
    }
}
