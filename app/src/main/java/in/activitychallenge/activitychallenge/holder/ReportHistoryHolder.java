package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/4/2017.
 */

public class ReportHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public  TextView title_r, desc_r, date_r,type_r;

    public ReportHistoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        title_r = (TextView) itemView.findViewById(R.id.title_r);
        desc_r = (TextView) itemView.findViewById(R.id.desc_r);
        date_r = (TextView) itemView.findViewById(R.id.date_r);
        type_r = (TextView) itemView.findViewById(R.id.type_r);


    }

    @Override
    public void onClick(View view) {

    }
}
