package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.RecentHistoryActivityItemClickListener;

public class RecentHistoryActivityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView amount_txt,title_txt,txn_flag_txt,txn_type_txt,time_txt,updated_time_txt,status;
    public CardView card;
    public LinearLayout back_layout;

    RecentHistoryActivityItemClickListener recentHistoryActivityItemClickListener;

    public RecentHistoryActivityHolder(View itemView)
    {
        super(itemView);
        itemView.setOnClickListener(this);


        card = (CardView) itemView.findViewById(R.id.card);
        amount_txt = (TextView) itemView.findViewById(R.id.amount_txt);
        title_txt = (TextView) itemView.findViewById(R.id.title_txt);
        txn_flag_txt = (TextView) itemView.findViewById(R.id.txn_flag_txt);
        txn_type_txt = (TextView) itemView.findViewById(R.id.txn_type_txt);
        time_txt = (TextView) itemView.findViewById(R.id.time_txt);
        updated_time_txt = (TextView) itemView.findViewById(R.id.updated_time_txt);
        status = (TextView) itemView.findViewById(R.id.status);
        back_layout = (LinearLayout) itemView.findViewById(R.id.back_layout);
    }

    @Override
    public void onClick(View view) {

        this.recentHistoryActivityItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(RecentHistoryActivityItemClickListener ic)
    {
        this.recentHistoryActivityItemClickListener=ic;
    }
}
