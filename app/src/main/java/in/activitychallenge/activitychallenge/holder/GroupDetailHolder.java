package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.GroupDetailItemClickListener;


public class GroupDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public LinearLayout ll_left,ll_right;
    public TextView msg_left,msg_left_date,msg_right,msg_right_date,sender_right_name,sender_left_name,report_abuse_left;


    GroupDetailItemClickListener groupDetailItemClickListener;

    public GroupDetailHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        msg_left = (TextView) itemView.findViewById(R.id.msg_left);
        msg_left_date = (TextView) itemView.findViewById(R.id.msg_left_date);
        msg_right = (TextView) itemView.findViewById(R.id.msg_right);
        msg_right_date = (TextView) itemView.findViewById(R.id.msg_right_date);
        sender_right_name = (TextView) itemView.findViewById(R.id.sender_right_name);
        sender_left_name = (TextView) itemView.findViewById(R.id.sender_left_name);
        report_abuse_left = (TextView) itemView.findViewById(R.id.report_abuse_left);

        ll_left = (LinearLayout) itemView.findViewById(R.id.ll_left);
        ll_right = (LinearLayout) itemView.findViewById(R.id.ll_right);


    }

    @Override
    public void onClick(View view) {
        this.groupDetailItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupDetailItemClickListener ic)
    {
        this.groupDetailItemClickListener =ic;
    }
}
