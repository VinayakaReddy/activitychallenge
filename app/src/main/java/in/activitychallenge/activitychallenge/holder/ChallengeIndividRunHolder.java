package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeIndividRunItemClickListener;


/**
 * Created by Devolper on 5/18/2017.
 */

public class ChallengeIndividRunHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView status,challenger_names,dateandtime,cash_prize_amount, vsText, oponent_names,winner_name,won_cash,challenge_type;

    public ImageView activity_image,share_icon,pause_challenge;

    public LinearLayout ll_completed;


    ChallengeIndividRunItemClickListener challengIndRunItemClickListener;

    public ChallengeIndividRunHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        activity_image = (ImageView)itemView.findViewById(R.id.activity_image);
        share_icon = (ImageView)itemView.findViewById(R.id.share_icon);
        pause_challenge = (ImageView)itemView.findViewById(R.id.pause_challenge);

        status = (TextView) itemView.findViewById(R.id.status);
        vsText = (TextView) itemView.findViewById(R.id.vsText);
        oponent_names = (TextView) itemView.findViewById(R.id.oponent_names);
        challenger_names = (TextView) itemView.findViewById(R.id.challenger_names);
        dateandtime = (TextView) itemView.findViewById(R.id.dateandtime);
        cash_prize_amount = (TextView) itemView.findViewById(R.id.cash_prize_amount);
        challenge_type = (TextView) itemView.findViewById(R.id.challenge_type);

        winner_name = (TextView) itemView.findViewById(R.id.winner_name);
        won_cash = (TextView) itemView.findViewById(R.id.won_cash);
        ll_completed = (LinearLayout) itemView.findViewById(R.id.ll_completed);

    }

    @Override
    public void onClick(View view) {

        this.challengIndRunItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ChallengeIndividRunItemClickListener ic)
    {
        this.challengIndRunItemClickListener=ic;
    }
}
