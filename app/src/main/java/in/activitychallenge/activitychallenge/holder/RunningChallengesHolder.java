package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.UserRunningChallngItemClickListener;

/**
 * Created by admin on 12/26/2017.
 */

public class RunningChallengesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView acti_img;
    public TextView acti_status, acti_amount, acti_name, acti_oponent_name, acti_oponent_type, acti_start, acti_pause, acti_complet;
    UserRunningChallngItemClickListener  userRunningChallngItemClickListener;


    public RunningChallengesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        acti_img =  (ImageView) itemView.findViewById(R.id.acti_img);
        acti_status =  (TextView) itemView.findViewById(R.id.acti_status);
        acti_amount =  (TextView) itemView.findViewById(R.id.acti_amount);
        acti_name =  (TextView) itemView.findViewById(R.id.acti_name);
        acti_oponent_name =  (TextView) itemView.findViewById(R.id.acti_oponent_name);
        acti_oponent_type =  (TextView) itemView.findViewById(R.id.acti_oponent_type);
        acti_start =  (TextView) itemView.findViewById(R.id.acti_start);
        acti_pause =  (TextView) itemView.findViewById(R.id.acti_pause);
        acti_complet =  (TextView) itemView.findViewById(R.id.acti_complet);


    }

    @Override
    public void onClick(View view) {
        this.userRunningChallngItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(UserRunningChallngItemClickListener ic)
    {
        this.userRunningChallngItemClickListener=ic;
    }
}
