package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.GroupListItemClickListener;


public class GroupListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView group_name_text,mesage_desc_text,message_sent_time;
      public ImageView group_profile_pic;


    GroupListItemClickListener groupListItemClickListener;

    public GroupListHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        group_name_text = (TextView) itemView.findViewById(R.id.group_name_text);
        mesage_desc_text = (TextView) itemView.findViewById(R.id.mesage_desc_text);
        message_sent_time = (TextView) itemView.findViewById(R.id.message_sent_time);
        group_profile_pic = (ImageView) itemView.findViewById(R.id.group_profile_pic);


    }

    @Override
    public void onClick(View view) {
        this.groupListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupListItemClickListener ic)
    {
        this.groupListItemClickListener =ic;
    }
}
