package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/8/2017.
 */

public class LotteryListHolder extends RecyclerView.ViewHolder {
    public TextView fName, lName, tktNumber, amountWon;
    public LinearLayout card_lottery;

    public LotteryListHolder(View itemView) {
        super(itemView);
        fName = (TextView) itemView.findViewById(R.id.fName);
        lName = (TextView) itemView.findViewById(R.id.lName);
        tktNumber = (TextView) itemView.findViewById(R.id.tktNumber);
        amountWon = (TextView) itemView.findViewById(R.id.amountWon);
        card_lottery = (LinearLayout) itemView.findViewById(R.id.card_lottery);
    }
}
