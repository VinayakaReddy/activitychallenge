package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/19/2017.
 */

public class GetGroupMembersHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

  public ImageView grp_img, grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5;
  public TextView grp_rank, grp_name, grp_won_cnt, grp_lost_cnt, grp_percent;

    public GetGroupMembersHolder(View itemView) {

        super(itemView);
        grp_img = (ImageView) itemView.findViewById(R.id.grp_img);
        grp_img_1 = (ImageView) itemView.findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) itemView.findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) itemView.findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) itemView.findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) itemView.findViewById(R.id.grp_img_5);
        grp_rank = (TextView) itemView.findViewById(R.id.grp_rank);
        grp_name = (TextView) itemView.findViewById(R.id.grp_name);
        grp_won_cnt = (TextView) itemView.findViewById(R.id.grp_won_cnt);
        grp_lost_cnt = (TextView) itemView.findViewById(R.id.grp_lost_cnt);
        grp_percent = (TextView) itemView.findViewById(R.id.grp_percent);
    }

    @Override
    public void onClick(View view) {

    }
}
