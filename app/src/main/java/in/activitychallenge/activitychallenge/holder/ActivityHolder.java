package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;


/**
 * Created by Devolper on 5/18/2017.
 */

public class ActivityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public CardView card;
    public ImageView activity_image,sensor_not_support_image,share_icon;

    ActivityItemClickListener listInstituteItemClickListener;

    public ActivityHolder(View itemView)
    {
        super(itemView);
        itemView.setOnClickListener(this);
        activity_image = (ImageView)itemView.findViewById(R.id.activity_image);
        share_icon = (ImageView)itemView.findViewById(R.id.share_icon);
        card = (CardView) itemView.findViewById(R.id.card);
        sensor_not_support_image = (ImageView)itemView.findViewById(R.id.sensor_not_support_image);
        name = (TextView) itemView.findViewById(R.id.name);

    }

    @Override
    public void onClick(View view) {

        this.listInstituteItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ActivityItemClickListener ic)
    {
        this.listInstituteItemClickListener=ic;
    }
}
