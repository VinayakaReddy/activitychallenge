package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/21/2017.
 */

public class GroupPromotionHolder extends RecyclerView.ViewHolder {
    public TextView name, userTyp, promoCode, status, cre_date, amount,pay_txt;

    public GroupPromotionHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.name);
        userTyp = (TextView) itemView.findViewById(R.id.userTyp);
        promoCode = (TextView) itemView.findViewById(R.id.promoCode);
        status = (TextView) itemView.findViewById(R.id.status);
        cre_date = (TextView) itemView.findViewById(R.id.cre_date);
        amount = (TextView) itemView.findViewById(R.id.amount);
        pay_txt = (TextView) itemView.findViewById(R.id.pay_txt);

    }


}
