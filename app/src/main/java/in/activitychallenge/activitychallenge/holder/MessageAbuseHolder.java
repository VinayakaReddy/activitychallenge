package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.ReportAbuseItemClickListener;


public class MessageAbuseHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public RadioButton reasone_radio_dynamic_button;
    ReportAbuseItemClickListener deactivateReasonItemClickListener;

    public MessageAbuseHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        reasone_radio_dynamic_button = (RadioButton) itemView.findViewById(R.id.reasone_radio_dynamic_button);
    }

    @Override
    public void onClick(View view) {

        this.deactivateReasonItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ReportAbuseItemClickListener ic) {
        this.deactivateReasonItemClickListener = ic;
    }
}