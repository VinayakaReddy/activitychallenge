package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengesListItemClickListener;

public class ChallengeRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RelativeLayout rank_rl;
    public ImageView challenger_image,challenger_rank_img,accept_btn,reject_btn;
    public TextView challenger_rank,challenger_req_name,challenge_name,challenger_req_amount,challenger_req_amount_doller;
    public TextView challenge_goal_txt,challenge_evaluation_unit,challenge_type;
    public ImageView challenge_goal_img;
    ChallengesListItemClickListener challengesListItemClickListener;

    public ChallengeRequestHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        rank_rl = (RelativeLayout) itemView.findViewById(R.id.rank_rl);
        challenger_image = (ImageView) itemView.findViewById(R.id.challenger_image);
        challenger_rank_img = (ImageView) itemView.findViewById(R.id.challenger_rank_img);
        accept_btn = (ImageView) itemView.findViewById(R.id.accept_btn);
        reject_btn = (ImageView) itemView.findViewById(R.id.reject_btn);
        challenger_rank = (TextView) itemView.findViewById(R.id.challenger_rank);
        challenger_req_name = (TextView) itemView.findViewById(R.id.challenger_req_name);
        challenge_name = (TextView) itemView.findViewById(R.id.challenge_name);
        challenger_req_amount = (TextView) itemView.findViewById(R.id.challenger_req_amount);
        challenger_req_amount_doller = (TextView) itemView.findViewById(R.id.challenger_req_amount_doller);
        challenge_goal_txt=(TextView)itemView.findViewById(R.id.challenge_goal_txt);
        challenge_evaluation_unit=(TextView)itemView.findViewById(R.id.challenge_evaluation_unit);
        challenge_type=(TextView)itemView.findViewById(R.id.challenge_type);
        challenge_goal_img=(ImageView)itemView.findViewById(R.id.goal_img);

    }

    @Override
    public void onClick(View view) {

        this.challengesListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ChallengesListItemClickListener ic)
    {
        this.challengesListItemClickListener = ic;
    }
}
