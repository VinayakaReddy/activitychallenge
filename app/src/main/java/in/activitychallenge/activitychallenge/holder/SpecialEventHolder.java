package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class SpecialEventHolder extends  RecyclerView.ViewHolder {

    public TextView event_name_txt;
    public TextView event_description;
    public TextView follow_text;
    public TextView challenge_txt,sponsor_challenge_txt;
    public ImageView roadmapImg;
    public ImageView notification;
    public ImageView marker;

    public SpecialEventHolder(View itemView)
    {
        super(itemView);
        event_name_txt = (TextView) itemView.findViewById(R.id.special_event_name);
        event_description = (TextView)itemView.findViewById(R.id.special_event_dec);
        follow_text = (TextView) itemView.findViewById(R.id.follow_txt);
        challenge_txt = (TextView)itemView.findViewById(R.id.chalenge_txt);
        sponsor_challenge_txt = (TextView)itemView.findViewById(R.id.sponsor_challenge_txt);
        roadmapImg=(ImageView)itemView.findViewById(R.id.roadmap_img);
        notification=(ImageView)itemView.findViewById(R.id.challenge_notification_icon);
        marker=(ImageView)itemView.findViewById(R.id.marker_img);


    }
}
