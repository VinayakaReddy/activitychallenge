package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.GrpInMemberDetailItemClickListener;


/**
 * Created by Devolper on 5/18/2017.
 */

public class GrpInMemberDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView grpname_in_memberdetail,grp_membcnt_in_memberdetail,grp_rank,grpin_member_won_cnt,grpin_member_lost_cnt,grp_inmember_percent;

    public ImageView grp_in_mem_imge,grpin_memdetail_img_1,grpin_memdetail_img_2,grpin_memdetail_img_3,grpin_memdetail_img_4,grpin_memdetail_img_5;

    GrpInMemberDetailItemClickListener memberdetailInGrpInMemberDetailItemClickListener;

    public GrpInMemberDetailHolder(View itemView)
    {
        super(itemView);
        itemView.setOnClickListener(this);

        grp_in_mem_imge = (ImageView)itemView.findViewById(R.id.grp_in_mem_imge);
        grpin_memdetail_img_1 = (ImageView)itemView.findViewById(R.id.grpin_memdetail_img_1);
        grpin_memdetail_img_2 = (ImageView)itemView.findViewById(R.id.grpin_memdetail_img_2);
        grpin_memdetail_img_3 = (ImageView)itemView.findViewById(R.id.grpin_memdetail_img_3);
        grpin_memdetail_img_4 = (ImageView)itemView.findViewById(R.id.grpin_memdetail_img_4);
        grpin_memdetail_img_5 = (ImageView)itemView.findViewById(R.id.grpin_memdetail_img_5);

        grpname_in_memberdetail = (TextView) itemView.findViewById(R.id.grpname_in_memberdetail);
        grp_membcnt_in_memberdetail = (TextView) itemView.findViewById(R.id.grp_membcnt_in_memberdetail);
        grp_rank = (TextView) itemView.findViewById(R.id.grp_rank);
        grpin_member_won_cnt = (TextView) itemView.findViewById(R.id.grpin_member_won_cnt);
        grpin_member_lost_cnt = (TextView) itemView.findViewById(R.id.grpin_member_lost_cnt);
        grp_inmember_percent = (TextView) itemView.findViewById(R.id.grp_inmember_percent);

    }

    @Override
    public void onClick(View view) {

        this.memberdetailInGrpInMemberDetailItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GrpInMemberDetailItemClickListener ic)
    {
        this.memberdetailInGrpInMemberDetailItemClickListener =ic;
    }
}
