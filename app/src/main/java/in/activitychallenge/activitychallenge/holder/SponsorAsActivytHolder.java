package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.SponsorAsActivityItemClickListener;


public class SponsorAsActivytHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    public ImageView sp_acti_img;
    public TextView sp_act_name,sp_acti_sponsor_btn;



    SponsorAsActivityItemClickListener sponsorAsActivityItemClickListener;

    public SponsorAsActivytHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        sp_acti_img=(ImageView)itemView.findViewById(R.id.sp_acti_img);
        sp_act_name=(TextView)itemView.findViewById(R.id.sp_act_name);
        sp_acti_sponsor_btn=(TextView)itemView.findViewById(R.id.sp_acti_sponsor_btn);




    }

    @Override
    public void onClick(View view) {
        this.sponsorAsActivityItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SponsorAsActivityItemClickListener ic)
    {
        this.sponsorAsActivityItemClickListener =ic;
    }
}
