package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.SavedCardDetailsItemClickListener;

public class SavedCardDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name_on_card_txt,name_on_card,acc_type_txt,acc_type,card_number_txt,card_number,month_year_txt,month_year,created_time_txt,created_time;
    public Button update_btn,delete_btn;
    SavedCardDetailsItemClickListener savedCardDetailsItemClickListener;

    public SavedCardDetailsHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        name_on_card_txt = (TextView) itemView.findViewById(R.id.name_on_card_txt);
        name_on_card = (TextView) itemView.findViewById(R.id.name_on_card);
        acc_type_txt = (TextView) itemView.findViewById(R.id.acc_type_txt);
        acc_type = (TextView) itemView.findViewById(R.id.acc_type);
        card_number_txt = (TextView) itemView.findViewById(R.id.card_number_txt);
        card_number = (TextView) itemView.findViewById(R.id.card_number);
        month_year_txt = (TextView) itemView.findViewById(R.id.month_year_txt);
        month_year = (TextView) itemView.findViewById(R.id.month_year);
        created_time_txt = (TextView) itemView.findViewById(R.id.created_time_txt);
        created_time = (TextView) itemView.findViewById(R.id.created_time);

        delete_btn = (Button) itemView.findViewById(R.id.delete_btn);
    }

    @Override
    public void onClick(View view) {

        this.savedCardDetailsItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SavedCardDetailsItemClickListener ic)
    {
        this.savedCardDetailsItemClickListener = ic;
    }
}
