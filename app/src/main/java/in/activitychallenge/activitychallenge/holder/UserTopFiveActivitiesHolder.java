package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/15/2017.
 */

public class UserTopFiveActivitiesHolder extends RecyclerView.ViewHolder {

    public ImageView act_img;

    public UserTopFiveActivitiesHolder(View itemView) {
        super(itemView);

        act_img = (ImageView) itemView.findViewById(R.id.act_img);


    }
}
