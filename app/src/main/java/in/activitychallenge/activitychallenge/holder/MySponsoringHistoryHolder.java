package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.activitychallenge.activitychallenge.R;

/**
 * Created by admin on 12/4/2017.
 */

public class MySponsoringHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public  TextView name, status, amount,date,sponser_type_to;
    public CircleImageView sponcering_image;

    public MySponsoringHistoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        name = (TextView) itemView.findViewById(R.id.name);
        status = (TextView) itemView.findViewById(R.id.status);
        amount = (TextView) itemView.findViewById(R.id.amount);
        date = (TextView) itemView.findViewById(R.id.date);
        sponser_type_to = (TextView) itemView.findViewById(R.id.sponser_type_to);
        sponcering_image = (CircleImageView) itemView.findViewById(R.id.sponcering_image);


    }
    @Override
    public void onClick(View view) {

    }
}
