package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class SpecialEventNotificationHolder extends  RecyclerView.ViewHolder {
    public TextView notification_txt;
    public TextView created_date_txt;
    public SpecialEventNotificationHolder(View itemView) {
        super(itemView);
        notification_txt = (TextView) itemView.findViewById(R.id.notification_txt);
        created_date_txt = (TextView)itemView.findViewById(R.id.notification_date);
    }
}
