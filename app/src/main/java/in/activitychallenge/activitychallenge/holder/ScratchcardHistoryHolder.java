package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.ScratchcardHistoryItemClickListener;


public class ScratchcardHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView amountname;


    ScratchcardHistoryItemClickListener cardHistoryItemClickListener;

    public ScratchcardHistoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        amountname = (TextView) itemView.findViewById(R.id.amountname);

    }

    @Override
    public void onClick(View view)
    {
        this.cardHistoryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ScratchcardHistoryItemClickListener ic)
    {
        this.cardHistoryItemClickListener =ic;
    }
}
