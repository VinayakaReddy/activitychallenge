package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.CategoryWinLossClickListner;



public class DayDateWiseChallengeStatusHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView date,values;

    public ImageView dot_status,holo_img;
    public CardView cardView;
    CategoryWinLossClickListner afterRegCPListofUniversitiesItemClickListener;


    public DayDateWiseChallengeStatusHolder(View itemView) {

        super(itemView);

        date = (TextView) itemView.findViewById(R.id.date);
        values = (TextView) itemView.findViewById(R.id.values);
        dot_status = (ImageView) itemView.findViewById(R.id.dot_status);
        holo_img = (ImageView) itemView.findViewById(R.id.holo_img);
         cardView=(CardView)itemView.findViewById(R.id.card);


        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CategoryWinLossClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
