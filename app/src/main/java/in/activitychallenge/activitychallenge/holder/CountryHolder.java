package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.CountryItemClickListener;


public class CountryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView country_name;
    public ImageView flag_image;

    CountryItemClickListener countryItemClickListener;

    public CountryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        country_name = (TextView) itemView.findViewById(R.id.country_name);
        flag_image = (ImageView) itemView.findViewById(R.id.flag_image);
    }

    @Override
    public void onClick(View view) {
        this.countryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CountryItemClickListener ic)
    {
        this.countryItemClickListener =ic;
    }
}
