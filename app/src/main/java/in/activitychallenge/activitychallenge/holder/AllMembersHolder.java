package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.AllMembersItemClickListener;



public class AllMembersHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    public ImageView all_member_image,iv_one,iv_two,iv_three,iv_four,iv_five;
    public TextView all_member_user_name,all_member_ranking_text,all_member_percentage_text,all_member_won_text,all_member_loss_text,user_type;
    public RecyclerView recycler_activity_imag;



  //

    AllMembersItemClickListener allMembersItemClickListener;

    public AllMembersHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        all_member_image=(ImageView)itemView.findViewById(R.id.all_member_image);

        iv_one=(ImageView)itemView.findViewById(R.id.iv_one);
        iv_two=(ImageView)itemView.findViewById(R.id.iv_two);
        iv_three=(ImageView)itemView.findViewById(R.id.iv_three);
        iv_four=(ImageView)itemView.findViewById(R.id.iv_four);
        iv_five=(ImageView)itemView.findViewById(R.id.iv_five);

        all_member_ranking_text=(TextView) itemView.findViewById(R.id.all_member_ranking_text);
        all_member_user_name=(TextView) itemView.findViewById(R.id.all_member_user_name);
        all_member_percentage_text=(TextView) itemView.findViewById(R.id.all_member_percentage_text);
        all_member_won_text=(TextView) itemView.findViewById(R.id.all_member_won_text);
        all_member_loss_text=(TextView) itemView.findViewById(R.id.all_member_loss_text);
        user_type=(TextView) itemView.findViewById(R.id.user_type);
    }

    @Override
    public void onClick(View view) {
        this.allMembersItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllMembersItemClickListener ic)
    {
        this.allMembersItemClickListener =ic;
    }
}
