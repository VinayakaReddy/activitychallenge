package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MyChallengesListItemClickListener;

public class MyChallengeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView cancel_challenge_img,challenger_image,challenger_rank_img;
    public RelativeLayout rank_rl;
    public TextView challenger_rank,opponent_name_txt,challenge_name,challenger_req_amount,challenger_req_amount_doller,wallet_amount,wallet_amount_doller,status_txt;
    MyChallengesListItemClickListener myChallengesListItemClickListener;

    public MyChallengeHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        rank_rl = (RelativeLayout) itemView.findViewById(R.id.rank_rl);
        challenger_image = (ImageView) itemView.findViewById(R.id.challenger_image);
        cancel_challenge_img = (ImageView) itemView.findViewById(R.id.cancel_challenge_img);
        challenger_rank_img = (ImageView) itemView.findViewById(R.id.challenger_rank_img);
        challenger_rank = (TextView) itemView.findViewById(R.id.challenger_rank);
        opponent_name_txt = (TextView) itemView.findViewById(R.id.opponent_name_txt);
        challenge_name = (TextView) itemView.findViewById(R.id.challenge_name);
        challenger_req_amount = (TextView) itemView.findViewById(R.id.challenger_req_amount);
        challenger_req_amount_doller = (TextView) itemView.findViewById(R.id.challenger_req_amount_doller);
        wallet_amount = (TextView) itemView.findViewById(R.id.wallet_amount);
        wallet_amount_doller = (TextView) itemView.findViewById(R.id.wallet_amount_doller);
        status_txt = (TextView) itemView.findViewById(R.id.status_txt);
    }

    @Override
    public void onClick(View view) {

        this.myChallengesListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyChallengesListItemClickListener ic)
    {
        this.myChallengesListItemClickListener = ic;
    }
}
