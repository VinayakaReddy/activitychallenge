package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.SponsorRequestItemClickListener;

public class SponsorRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RelativeLayout rank_rl;
    public ImageView sponsor_image,accept_btn,reject_btn;
    public TextView sponsor_name,sponsored_req_amount,sponsor_req_amount_doller;
    SponsorRequestItemClickListener sponsorRequestItemClickListener;

    public SponsorRequestHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        sponsor_image = (ImageView) itemView.findViewById(R.id.sponsor_image);
        accept_btn = (ImageView) itemView.findViewById(R.id.accept_btn);
        reject_btn = (ImageView) itemView.findViewById(R.id.reject_btn);
        sponsor_name = (TextView) itemView.findViewById(R.id.sponsor_name);
        sponsored_req_amount = (TextView) itemView.findViewById(R.id.sponsored_req_amount);
        sponsor_req_amount_doller = (TextView) itemView.findViewById(R.id.sponsor_req_amount_doller);
    }

    @Override
    public void onClick(View view) {

        this.sponsorRequestItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SponsorRequestItemClickListener ic)
    {
        this.sponsorRequestItemClickListener = ic;
    }
}
