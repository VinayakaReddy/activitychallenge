package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MessagesItemClickListener;


public class MessagesHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView message_sent_time,message_name_text,mesage_desc_text,mesage_reply_text;
public ImageView profile_pic;
public LinearLayout parent_layout;

    MessagesItemClickListener messagesItemClickListener;

    public MessagesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        message_sent_time = (TextView) itemView.findViewById(R.id.message_sent_time);
        message_name_text = (TextView) itemView.findViewById(R.id.message_name_text);
        mesage_desc_text = (TextView) itemView.findViewById(R.id.mesage_desc_text);
        profile_pic = (ImageView) itemView.findViewById(R.id.profile_pic);
        parent_layout = (LinearLayout) itemView.findViewById(R.id.parent_layout);

    }

    @Override
    public void onClick(View view) {
        this.messagesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MessagesItemClickListener ic)
    {
        this.messagesItemClickListener =ic;
    }
}
