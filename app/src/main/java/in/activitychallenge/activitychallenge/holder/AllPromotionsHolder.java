package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;

public class AllPromotionsHolder extends RecyclerView.ViewHolder {

    public TextView promotion_name;
    public ImageView promotion_image;

    public AllPromotionsHolder(View itemView) {
        super(itemView);

        promotion_image = (ImageView) itemView.findViewById(R.id.promotion_image);
        promotion_name = (TextView) itemView.findViewById(R.id.promotion_name);


    }
}
