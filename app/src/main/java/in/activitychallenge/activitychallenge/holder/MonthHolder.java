package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.MonthClickListner;

/**
 * Created by admin on 12/8/2017.
 */

public class MonthHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView mo_txt;
    public LinearLayout monLL;

    MonthClickListner monthClickListner;

    public MonthHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        mo_txt = (TextView) itemView.findViewById(R.id.mo_txt);
        monLL = (LinearLayout) itemView.findViewById(R.id.monLL);


    }

    @Override
    public void onClick(View view) {
        this.monthClickListner.onItemClick(view, getLayoutPosition());


    }

    public void setItemClickListener(MonthClickListner ic)
    {
        this.monthClickListner=ic;
    }

}
