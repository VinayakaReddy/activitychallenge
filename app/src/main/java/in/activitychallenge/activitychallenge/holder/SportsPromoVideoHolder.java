package in.activitychallenge.activitychallenge.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.itemclicklistners.PromoVideoClickListner;

/**
 * Created by admin on 12/6/2017.
 */

public class SportsPromoVideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView img_view;
    public TextView title;

    PromoVideoClickListner promoVideoClickListner;

    public SportsPromoVideoHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        img_view = (ImageView) itemView.findViewById(R.id.img_view);
        title = (TextView) itemView.findViewById(R.id.title);

    }

    @Override
    public void onClick(View view) {
            this.promoVideoClickListner.onItemClick(view, getLayoutPosition());

    }

    public void setItemClickListener(PromoVideoClickListner ic)
    {
        this.promoVideoClickListner =ic;
    }
}
