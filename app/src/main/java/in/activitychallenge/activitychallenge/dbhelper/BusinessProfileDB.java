package in.activitychallenge.activitychallenge.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BusinessProfileDB extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME = "sponsorbusiness_profile.db";

    private static final String TABLE_DETAILS_BusinessProfile = "sponsorbusiness_profiletable";

    public static final String OWNER_NAME = "owner_naem";
    public static final String BUSINESS_NAME = "business_name";
    public static final String BUSINESS_TYPE = "business_type";
    public static final String ADDRESS = "address";
    public static final String PINCODE = "pincode";
    public static final String QRCODE = "qr_code";


    public BusinessProfileDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS_BusinessProfile + "("
                + OWNER_NAME + " TEXT ,"
                + BUSINESS_NAME + " TEXT ,"
                + BUSINESS_TYPE + " TEXT ,"
                + ADDRESS + " TEXT ,"
                + PINCODE + " TEXT ,"
                + QRCODE + " TEXT " +
                ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS_BusinessProfile);
        onCreate(db);
    }
    public void businessProfileSponsor(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS_BusinessProfile, null, contentValues);
        db.close();
    }


    public List<String> getOwnerName() {
        List<String> owner_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                owner_name.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return owner_name;
    }

    public List<String> getBusniessName() {
        List<String> business_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                business_name.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return business_name;
    }

    public List<String> getBusinessType() {
        List<String> busniess_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                busniess_type.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        return busniess_type;
    }

    public List<String> getAddress() {
        List<String> address = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                address.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return address;
    }

    public List<String> getPincode() {
        List<String> pincode = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                pincode.add(cursor.getString(4));
            } while (cursor.moveToNext());
        }
        return pincode;
    }

    public List<String> getQRCodeImage() {
        List<String> qr_code = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS_BusinessProfile;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                qr_code.add(cursor.getString(5));
            } while (cursor.moveToNext());
        }
        return qr_code;
    }








    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS_BusinessProfile); //delete all rows in a table
        db.close();
    }






    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
