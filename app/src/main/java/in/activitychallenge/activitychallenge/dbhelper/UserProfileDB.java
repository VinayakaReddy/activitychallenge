package in.activitychallenge.activitychallenge.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devolper on 08-Feb-18.
 */

public class UserProfileDB extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "user_profile.db";
    private static final String TABLE_USER_DETAILS = "user_profile";

    public static final String FNAME= "f_name";
    public static final String LNAME = "l_name";

    public static final String PROFILE_IMAGE = "profile_image";

    public static final String RANK = "rank";
    public static final String FOLLOWERS = "followers";
    public static final String FOLLOWINGS = "followings";
    public static final String SPONSORBY = "sponsered_by";

    public static final String WALLET_AMOUNT = "wallet_amount";
    public static final String REFERCODE= "refer_code";
    public static final String ABOUT_ME = "about_me";
    public static final String PAYPALID = "paypal_id";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";
    public static final String COUNTRYCODE = "country_code";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String USER_TYPE = "user_type";
    public static final String USER_STATUS= "user_status";
    public static final String ACCOUNT_CREATED_ON = "acc_created_on";
    public static final String LAST_LOGIN = "last_login";


    public UserProfileDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USER_DETAILS + "("
                + FNAME + " TEXT ,"
                + LNAME + " TEXT ,"
                + PROFILE_IMAGE + " TEXT ,"
                + RANK + " TEXT ,"
                + FOLLOWERS + " TEXT ,"
                + FOLLOWINGS + " TEXT ,"
                + SPONSORBY + " TEXT ,"
                + WALLET_AMOUNT + " TEXT ,"
                + REFERCODE + " TEXT ,"
                + ABOUT_ME + " TEXT ,"
                + PAYPALID + " TEXT ,"
                + DOB + " TEXT ,"
                + GENDER + " TEXT ,"
                + COUNTRYCODE + " TEXT ,"
                + MOBILE + " TEXT ,"
                + EMAIL + " TEXT ,"
                + USER_TYPE + " TEXT ,"
                + USER_STATUS + " TEXT ,"
                + ACCOUNT_CREATED_ON + " TEXT ,"
                + LAST_LOGIN + " TEXT " +
                ")";
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,  int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_DETAILS);
        onCreate(db);
    }

    public void addUserDetail(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_USER_DETAILS, null, contentValues);
        db.close();
    }
    ////Fetching Methods
    public List<String> getUserFName() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return user_type;
    }
    public List<String> getUserLName() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getProfileImage() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getRank() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getFollower() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(4));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getFollowing() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(5));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getSponsorBy() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(6));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getWalletAmt() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(7));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getReferCode() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(8));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getAboutMe() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(9));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getPapalId() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(10));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getDOB() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(11));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getGender() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(12));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getCountryCode() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(13));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getMobile() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(14));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getEmail() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(15));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getUserType() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(16));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getStatus() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(17));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getCreatedOn() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(18));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getLastLogin() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USER_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(19));
            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public void emptyDB()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_USER_DETAILS); //delete all rows in a table
        db.close();
    }



}
