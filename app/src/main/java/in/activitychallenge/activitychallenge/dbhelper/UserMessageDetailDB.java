package in.activitychallenge.activitychallenge.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devolper on 02-Jan-18.
 */

public class UserMessageDetailDB extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION=6;
    private static final String DATABASE_NAME="UserMessageDetail.db";

    public static final String TABLE_USERMSG_DETAILS = "user_message_details";

    public static final String ID = "id";
    public static final String FROM_ID = "from_id";
    public static final String FROM_NAME = "from_name";
    public static final String RECIPIENT = "recipient";
    public static final String MSG = "msg";
    public static final String IS_READ = "is_read";
    public static final String SET_ON_TEXT = "sent_on_txt";
    public static final String SET_ON = "sent_on";

    public UserMessageDetailDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }


    public UserMessageDetailDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USERMSGDETAIL_TABLE = "CREATE TABLE " + TABLE_USERMSG_DETAILS + "("
                + ID + " TEXT  PRIMARY KEY,"
                + FROM_ID + " TEXT ,"
                + FROM_NAME + " TEXT ,"
                + RECIPIENT + " TEXT ,"
                + MSG + " TEXT ,"
                + IS_READ + " INTEGER ,"
                + SET_ON_TEXT + " TEXT ,"
                + SET_ON + " TEXT )";

        db.execSQL(CREATE_USERMSGDETAIL_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERMSG_DETAILS);
        onCreate(db);

    }

    public void addUserChatList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_USERMSG_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getMsgName() {
        List<String> msgContent = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USERMSG_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                msgContent.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return msgContent;
    }

    public List<String> getMessageTimeStamp() {
        List<String> msgtimestamp = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USERMSG_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                msgtimestamp.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return msgtimestamp;
    }

    public List<String> getReceipent() {
        List<String> recepeint = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_USERMSG_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                recepeint.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return recepeint;
    }

    public void emptyMessage()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_USERMSG_DETAILS); //delete all rows in a table
        db.close();
    }


    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
