package in.activitychallenge.activitychallenge.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devolper on 20-Feb-18.
 */

public class GroupChatDB extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION=3;
    private static final String DATABASE_NAME="GroupChatDetail.db";

    public static final String TABLE_GROUPCHAT_DETAILS = "group_chat_details";


    public static final String ID = "id";
    public static final String FROM_ID = "from_id";
    public static final String FROM_NAME = "from_name";
    public static final String RECIPIENT = "recipient";
    public static final String MSG = "msg";
    public static final String IS_READ = "is_read";
    public static final String SET_ON_TEXT = "sent_on_txt";
    public static final String SET_ON = "sent_on";
    public static final String PROFILE_PIC = "profile_pic";

    public GroupChatDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }
    public GroupChatDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USERMSGDETAIL_TABLE = "CREATE TABLE " + TABLE_GROUPCHAT_DETAILS + "("
                + ID + " TEXT  PRIMARY KEY,"
                + FROM_NAME + " TEXT ,"
                + RECIPIENT + " TEXT ,"
                + MSG + " TEXT ,"
                + IS_READ + " INTEGER ,"
                + SET_ON_TEXT + " TEXT ,"
                + PROFILE_PIC + " TEXT ,"
                + SET_ON + " TEXT )";

        db.execSQL(CREATE_USERMSGDETAIL_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPCHAT_DETAILS);
        onCreate(db);

    }

    public void addGroupChatList(ContentValues contentValues)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_GROUPCHAT_DETAILS, null, contentValues);
        db.close();
    }


    public List<String> getMsgName() {
        List<String> msgContent = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_GROUPCHAT_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                msgContent.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return msgContent;
    }

    public List<String> getMessageTimeStamp() {
        List<String> msgtimestamp = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_GROUPCHAT_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                msgtimestamp.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return msgtimestamp;
    }

    public List<String> getReceipent() {
        List<String> recepeint = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_GROUPCHAT_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                recepeint.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return recepeint;
    }

    public List<String> getFromName() {
        List<String> recepeint = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_GROUPCHAT_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                recepeint.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return recepeint;
    }
}
