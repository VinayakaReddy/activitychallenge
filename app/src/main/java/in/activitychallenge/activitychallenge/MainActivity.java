package in.activitychallenge.activitychallenge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gjiazhe.panoramaimageview.GyroscopeObserver;
import com.gjiazhe.panoramaimageview.PanoramaImageView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.activitychallenge.activitychallenge.activities.AllMembersActivity;
import in.activitychallenge.activitychallenge.activities.AllPromotionsActivity;
import in.activitychallenge.activitychallenge.activities.AllSponsorActivity;
import in.activitychallenge.activitychallenge.activities.BusinessProfileActivity;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeRequestActivity;
import in.activitychallenge.activitychallenge.activities.ChatMessgesActivity;
import in.activitychallenge.activitychallenge.activities.DeactivateAccountActivity;
import in.activitychallenge.activitychallenge.activities.EarnActivity;
import in.activitychallenge.activitychallenge.activities.FaqTermsConditionActivity;
import in.activitychallenge.activitychallenge.activities.FeedbackContactUsActivity;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.activities.GroupsActivity;
import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.activities.MyChallengesActivity;
import in.activitychallenge.activitychallenge.activities.MyFollowing;
import in.activitychallenge.activitychallenge.activities.MyProfileActivity;
import in.activitychallenge.activitychallenge.activities.MyPromotionActivity;
import in.activitychallenge.activitychallenge.activities.MySponserActivity;
import in.activitychallenge.activitychallenge.activities.QRCodeActivity;
import in.activitychallenge.activitychallenge.activities.RecentHistoryActivity;
import in.activitychallenge.activitychallenge.activities.ReferFriendActivity;
import in.activitychallenge.activitychallenge.activities.RegistrationActivity;
import in.activitychallenge.activitychallenge.activities.RunningChallengesActivity;
import in.activitychallenge.activitychallenge.activities.SavedCardDetailsActivity;
import in.activitychallenge.activitychallenge.activities.SpecialEvent;
import in.activitychallenge.activitychallenge.activities.SponsorRequestActivity;
import in.activitychallenge.activitychallenge.activities.SponsorStatisticsActivity;
import in.activitychallenge.activitychallenge.activities.SportsPromoVideolist;
import in.activitychallenge.activitychallenge.activities.TakeChalengeActivity;
import in.activitychallenge.activitychallenge.activities.UserNotificationsActivity;
import in.activitychallenge.activitychallenge.activities.WinLossActivity;
import in.activitychallenge.activitychallenge.adapter.ActivityAdapter;
import in.activitychallenge.activitychallenge.adapter.SpecialEventAdapter;
import in.activitychallenge.activitychallenge.adapter.WebBannerAdapter;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;
import in.activitychallenge.activitychallenge.utilities.ActivityDB;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.Config;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityAdapter activityAdapter;
    ArrayList<ActivityModel> activitylist = new ArrayList<ActivityModel>();
    private RecyclerView activities_recyclerview;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    private FlowingDrawer mDrawer;
    private Boolean exit = false;
    private boolean checkInternet;
    SearchView activity_search;
    ActivityDB activityDB;
    SpecialEventAdapter adpte;
    CircleImageView user_profile_image;
     int flag=0;
    ImageView sponsor_iv, challenge_iv, mesage_iv, banner_image, voice_search, image_view;
    TextView sponsor_text, sponsor_qrcode_text,challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text, player_user_name, ranking_text, more;

    LinearLayout linear_sponsor, linear_chllenge, linear_messges, bottom_layout;
    LinearLayout linear_home, linear_cards, linear_profile, linear_recent, linear_win_lost, linear_mychallenges, linear_my_group, linear_allmembers, linear_all_groups, Linear_following, linear_mypromotions, linear_sponsor_qrcode,linear_me_as_sponsor,
            linear_earn_money, linear_all_sponsor_list,linear_sponsors_statics,linear_refer_friend, linear_advertise, linear_report,linearnotifications, linear_help, linear_terms_and_cond, linear_contact_us, linear_deactivate, linear_mysponsorings;
    private GyroscopeObserver gyroscopeObserver;
    UserSessionManager session;
    String user_id, token, device_id, user_type = "", sendUserType = "";
    private final int REQ_CODE_SPEECH_INPUT = 100;
    EditText searchEditText;
    Layout custom_tab;
    int userfollow;
    ////////////////////////////////////////
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    EditText current_location;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Sensor stepSensor;
    SensorManager sManager;
    String special_EventId;
    Dialog dialog;
    TextView follow_Btn_text;
    /////////////////////////
    BannerLayout bannerVertical;
    WebBannerAdapter webBannerAdapter;
    ArrayList<AllPromotionsModel> promoList = new ArrayList<AllPromotionsModel>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        session = new UserSessionManager(MainActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

        // user_type = getIntent().getExtras().getString("USERTYPE");
        Log.d("DEEDE",user_id);
        bannerVertical =  findViewById(R.id.recycler_ver);
      // this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        gps = new GPSTracker(MainActivity.this);

        geocoder = new Geocoder(this, Locale.getDefault());

        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        gyroscopeObserver = new GyroscopeObserver();
        PanoramaImageView panoramaImageView = (PanoramaImageView) findViewById(R.id.panorama_image_view);
        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);
        activity_search = (SearchView) findViewById(R.id.activity_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        activity_search.clearFocus();
        MainActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText = (EditText) activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));



        ImageView voiceIcon = (ImageView) activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        searchEditText.setBackgroundColor(getResources().getColor(R.color.white));

        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        activities_recyclerview = (RecyclerView) findViewById(R.id.activities_recyclerview);
        activities_recyclerview.setHasFixedSize(true);
        activityAdapter = new ActivityAdapter(activitylist, MainActivity.this, R.layout.row_activity);
        gridLayoutManager = new GridLayoutManager(this, 4);
        layoutManager = new LinearLayoutManager(this);
        activities_recyclerview.setLayoutManager(gridLayoutManager);
        activityDB = new ActivityDB(MainActivity.this);
        linear_sponsor = (LinearLayout) findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) findViewById(R.id.linear_messges);
        View view = findViewById(R.id.custom_tab);
        linear_messges.setOnClickListener(this);
        sponsor_iv = (ImageView) findViewById(R.id.sponsor_iv);
        challenge_iv = (ImageView) findViewById(R.id.challenge_iv);
        mesage_iv = (ImageView) findViewById(R.id.mesage_iv);
        banner_image = (ImageView) findViewById(R.id.banner_image);
        voice_search = (ImageView) findViewById(R.id.voice_search);
        sponsor_text = (TextView) findViewById(R.id.sponsor_text);
        challenge_text = (TextView) findViewById(R.id.challenge_text);
        mesages_text = (TextView) findViewById(R.id.mesages_text);
        count_sponsor_text = (TextView) findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) findViewById(R.id.count_mesages_text);
        more = (TextView) findViewById(R.id.more);
        more.setOnClickListener(this);
        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        setupToolbar();
        getAccessControl();
        setupMenu();
        getActivities();
        getAds();
        getCount();

        getValidateUser();
        getUserDetails();
        getToken();
        webBannerAdapter=new WebBannerAdapter(MainActivity.this,promoList);
        webBannerAdapter.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(promoList.get(position).getEntity_id().equals(user_id))
                {

                }
                else {
                    if (promoList.get(position).getEntity_type().equals("USER") ||promoList.get(position).getEntity_type().equals("SPONSOR") ) {
                        Intent intent = new Intent(MainActivity.this, MemberDetailActivity.class);
                        intent.putExtra("MEMBER_ID", promoList.get(position).getEntity_id());
                        intent.putExtra("MEMBER_NAME", promoList.get(position).getEntity_name());
                        intent.putExtra("member_user_type", promoList.get(position).getEntity_type());
                        startActivity(intent);
                    } else if (promoList.get(position).getEntity_type().equals("GROUP")) {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoList.get(position).getEntity_id());
                        intent.putExtra("grp_name", promoList.get(position).getEntity_name());
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoList.get(position).getEntity_type());
                        intent.putExtra("grp_admin_id", promoList.get(position).getAdmin_id());

                        startActivity(intent);
                    } else {

                    }
                }
                //  Toast.makeText(AllPromotionsActivity.this, " " + position, Toast.LENGTH_SHORT).show();
            }
        });



        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                activityAdapter.getFilter().filter(query);
                return false;
            }
        });
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getApplicationContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
// check if GPS enabled
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //  Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        sendUserData(user_id, device_id, lat, lng, city, country, state, place, token);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);
            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            gps.showSettingsAlert();
        }
        if (user_type.equals("USER")) {
            view.setVisibility(View.VISIBLE);
        } else {

            linear_chllenge.setVisibility(View.GONE);
        }
    }

    protected void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
    }

   private void setupMenu() {
       /* FragmentManager fm = getSupportFragmentManager();
        MenuListFragment mMenuFragment = (MenuListFragment) fm.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }*/
        NavigationView vNavigation = (NavigationView) findViewById(R.id.vNavigation);
        View header = vNavigation.getHeaderView(0);
        user_profile_image = (CircleImageView) header.findViewById(R.id.user_profile_image);
        image_view = (ImageView) header.findViewById(R.id.image_view);
       sponsor_qrcode_text=(TextView)header.findViewById(R.id.sponsor_qrcode_text);
        player_user_name = (TextView) header.findViewById(R.id.player_user_name);
        ranking_text = (TextView) header.findViewById(R.id.ranking_text);
        linear_home = (LinearLayout) header.findViewById(R.id.linear_home);
        linear_home.setOnClickListener(this);
        linearnotifications=(LinearLayout)header.findViewById(R.id.linear_notifications);
        linearnotifications.setOnClickListener(this);
        linear_profile = (LinearLayout) header.findViewById(R.id.linear_profile);
        linear_profile.setOnClickListener(this);
        linear_recent = (LinearLayout) header.findViewById(R.id.linear_recent);
        linear_recent.setOnClickListener(this);
        linear_cards = (LinearLayout) header.findViewById(R.id.linear_cards);
        linear_cards.setOnClickListener(this);
        linear_win_lost = (LinearLayout) header.findViewById(R.id.linear_win_lost);
        linear_win_lost.setOnClickListener(this);
        linear_mychallenges = (LinearLayout) header.findViewById(R.id.linear_mychallenges);
        linear_mychallenges.setOnClickListener(this);
        linear_my_group = (LinearLayout) header.findViewById(R.id.linear_my_group);
        linear_my_group.setOnClickListener(this);
        linear_allmembers = (LinearLayout) header.findViewById(R.id.linear_allmembers);
        linear_allmembers.setOnClickListener(this);
        linear_all_groups = (LinearLayout) header.findViewById(R.id.linear_all_groups);
        linear_all_groups.setOnClickListener(this);
        Linear_following = (LinearLayout) header.findViewById(R.id.Linear_following);
        Linear_following.setOnClickListener(this);
        linear_mypromotions = (LinearLayout) header.findViewById(R.id.linear_mypromotions);
        linear_mypromotions.setOnClickListener(this);
        linear_me_as_sponsor = (LinearLayout) header.findViewById(R.id.linear_me_as_sponsor);
        linear_me_as_sponsor.setOnClickListener(this);
        linear_sponsor_qrcode = (LinearLayout) header.findViewById(R.id.linear_sponsor_qrcode);
        linear_sponsor_qrcode.setOnClickListener(this);
        linear_earn_money = (LinearLayout) header.findViewById(R.id.linear_earn_money);
        linear_earn_money.setOnClickListener(this);
        linear_refer_friend = (LinearLayout) header.findViewById(R.id.linear_refer_friend);
        linear_refer_friend.setOnClickListener(this);
        linear_advertise = (LinearLayout) header.findViewById(R.id.linear_advertise);
        linear_advertise.setOnClickListener(this);
        linear_report = (LinearLayout) header.findViewById(R.id.linear_report);
        linear_report.setOnClickListener(this);
        linear_help = (LinearLayout) header.findViewById(R.id.linear_help);
        linear_help.setOnClickListener(this);
        linear_terms_and_cond = (LinearLayout) header.findViewById(R.id.linear_terms_and_cond);
        linear_terms_and_cond.setOnClickListener(this);
        linear_contact_us = (LinearLayout) header.findViewById(R.id.linear_contact_us);
        linear_contact_us.setOnClickListener(this);


        linear_deactivate = (LinearLayout) header.findViewById(R.id.linear_deactivate);
        linear_deactivate.setOnClickListener(this);
        linear_mysponsorings = (LinearLayout) header.findViewById(R.id.linear_mysponsorings);
        linear_mysponsorings.setOnClickListener(this);
       linear_all_sponsor_list = (LinearLayout) header.findViewById(R.id.linear_all_sponsor_list);
       linear_all_sponsor_list.setOnClickListener(this);
       linear_sponsors_statics = (LinearLayout) header.findViewById(R.id.linear_sponsors_statics);
       linear_sponsors_statics.setOnClickListener(this);
        if (user_type.contains("SPONSOR")) {
            sponsor_qrcode_text.setText("Business Profile");
            linear_home.setVisibility(View.VISIBLE);
            linear_profile.setVisibility(View.VISIBLE);
            linear_recent.setVisibility(View.VISIBLE);
            linear_cards.setVisibility(View.GONE);
            linear_win_lost.setVisibility(View.GONE);
            linear_mychallenges.setVisibility(View.GONE);
            linear_my_group.setVisibility(View.VISIBLE);
            linear_allmembers.setVisibility(View.VISIBLE);
            linear_all_groups.setVisibility(View.GONE);
            Linear_following.setVisibility(View.VISIBLE);
            linear_mypromotions.setVisibility(View.VISIBLE);
            linear_me_as_sponsor.setVisibility(View.GONE);
            linear_sponsor_qrcode.setVisibility(View.VISIBLE);
            linear_earn_money.setVisibility(View.GONE);
            linear_refer_friend.setVisibility(View.VISIBLE);
            linear_advertise.setVisibility(View.GONE);
            linear_report.setVisibility(View.VISIBLE);
            linear_help.setVisibility(View.VISIBLE);
            linear_terms_and_cond.setVisibility(View.GONE);
            linear_contact_us.setVisibility(View.GONE);

            linear_deactivate.setVisibility(View.VISIBLE);
            linear_mysponsorings.setVisibility(View.VISIBLE);
            linear_all_sponsor_list.setVisibility(View.VISIBLE);
            linear_sponsors_statics.setVisibility(View.VISIBLE);
        }
        else
            {
            if (sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null && sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && gps.canGetLocation()) {
                linear_home.setVisibility(View.VISIBLE);
                sponsor_qrcode_text.setText("Sponsor QR Code");
                linear_profile.setVisibility(View.VISIBLE);
                linear_recent.setVisibility(View.VISIBLE);
                linear_cards.setVisibility(View.GONE);
                linear_win_lost.setVisibility(View.VISIBLE);
                linear_mychallenges.setVisibility(View.VISIBLE);
                linear_my_group.setVisibility(View.VISIBLE);
                linear_allmembers.setVisibility(View.VISIBLE);
                linear_all_groups.setVisibility(View.GONE);
                Linear_following.setVisibility(View.VISIBLE);
                linear_mypromotions.setVisibility(View.VISIBLE);
                linear_me_as_sponsor.setVisibility(View.GONE);
                linear_sponsor_qrcode.setVisibility(View.VISIBLE);
                linear_earn_money.setVisibility(View.VISIBLE);
                linear_refer_friend.setVisibility(View.VISIBLE);
              //    linear_advertise.setVisibility(View.VISIBLE);
                linear_report.setVisibility(View.VISIBLE);
                linear_help.setVisibility(View.VISIBLE);
                linear_terms_and_cond.setVisibility(View.GONE);
                linear_contact_us.setVisibility(View.GONE);
                linear_deactivate.setVisibility(View.VISIBLE);

                linear_mysponsorings.setVisibility(View.VISIBLE);
                linear_all_sponsor_list.setVisibility(View.VISIBLE);
                linear_sponsors_statics.setVisibility(View.GONE);
            }else {
                linear_home.setVisibility(View.VISIBLE);
                linear_profile.setVisibility(View.VISIBLE);
                linear_recent.setVisibility(View.GONE);
                linear_cards.setVisibility(View.GONE);
                linear_win_lost.setVisibility(View.GONE);
                linear_mychallenges.setVisibility(View.GONE);
                linear_my_group.setVisibility(View.VISIBLE);
                linear_allmembers.setVisibility(View.VISIBLE);
                linear_all_groups.setVisibility(View.GONE);
                Linear_following.setVisibility(View.GONE);
                linear_mypromotions.setVisibility(View.GONE);
                linear_me_as_sponsor.setVisibility(View.GONE);
                linear_sponsor_qrcode.setVisibility(View.GONE);
                linear_earn_money.setVisibility(View.GONE);
                linear_refer_friend.setVisibility(View.VISIBLE);
           //     linear_advertise.setVisibility(View.GONE);
                linear_report.setVisibility(View.VISIBLE);
                linear_help.setVisibility(View.VISIBLE);
                linear_terms_and_cond.setVisibility(View.GONE);
                linear_contact_us.setVisibility(View.GONE);
                linear_deactivate.setVisibility(View.VISIBLE);

                linear_mysponsorings.setVisibility(View.VISIBLE);
                linear_all_sponsor_list.setVisibility(View.VISIBLE);
                linear_sponsors_statics.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }

    private void getActivities() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            activitylist.clear();
            activityDB.emptyDBBucket();
            String url = AppUrls.BASE_URL + AppUrls.ACTIVITY;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    ContentValues values = new ContentValues();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        values.put(ActivityDB.ID, jsonObject1.getString("id"));
                                        values.put(ActivityDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                        values.put(ActivityDB.ACTIVITY_NO, jsonObject1.getString("activity_no"));
                                        values.put(ActivityDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                        values.put(ActivityDB.TOOLS_REQUIRED, jsonObject1.getString("tools_required"));
                                        values.put(ActivityDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        values.put(ActivityDB.MIN_VALUE, jsonObject1.getString("min_value"));
                                        values.put(ActivityDB.MIN_VALUE_UNIT, jsonObject1.getString("min_value_unit"));
                                        values.put(ActivityDB.DAILY_CHALLENGES, jsonObject1.getString("daily_challenges"));
                                        values.put(ActivityDB.WEEKLY_CHALLENGES, jsonObject1.getString("weekly_challenges"));
                                        values.put(ActivityDB.LONGRUN_CHALLENGES, jsonObject1.getString("longrun_challenges"));
                                        activityDB.addFriendsList(values);
                                        /* ActivityModel am = new ActivityModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setActivity_name(jsonObject1.getString("activity_name"));
                                        am.setActivity_no(jsonObject1.getString("activity_no"));
                                        am.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                        am.setTools_required(jsonObject1.getString("tools_required"));
                                        am.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                        activitylist.add(am);
                                        Log.d("fjbvsdfkvsdf",activitylist.toString());*/
                                    }
                                    activityList();
                                    // activities_recyclerview.setAdapter(activityAdapter);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            activityList();
            if(isFirstTime())
                showAlert();
           // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void activityList() {
        activitylist.clear();
        List<String> activityName = activityDB.getActivityName();
        if (activityName.size() > 0) {

            List<String> id = activityDB.getId();
            List<String> activity_name = activityDB.getActivityName();
            List<String> activity_no = activityDB.getActivityNo();
            List<String> evalution_factor = activityDB.getEvaluationFactor();
            List<String> tools_required = activityDB.getToolsRequired();
            List<String> activity_image = activityDB.getActivity_image();
            List<String> min_value = activityDB.getMin_value();
            List<String> min_value_unit = activityDB.getMinimum_valu_unit();
            List<String> daily_challenges = activityDB.getDaily_challenges();
            List<String> weekly_challenges = activityDB.getWeekly_challenges();
            List<String> longrun_challenges = activityDB.getLongRun_challenges();

            for (int i = 0; i < activityName.size(); i++) {
                ActivityModel sm = new ActivityModel();
                sm.setId(id.get(i));
                sm.setActivity_name(activity_name.get(i));
                sm.setActivity_no(activity_no.get(i));
                sm.setEvaluation_factor(evalution_factor.get(i));
                sm.setTools_required(tools_required.get(i));
                sm.setActivity_image(activity_image.get(i));
                sm.setMin_value(min_value.get(i));
                sm.setMin_value_unit(min_value_unit.get(i));
                sm.setDaily_challenges(daily_challenges.get(i));
                sm.setWeekly_challenges(weekly_challenges.get(i));
                sm.setLongrun_challenges(longrun_challenges.get(i));
                activitylist.add(sm);
            }
            activities_recyclerview.setAdapter(activityAdapter);
        } else {

        }
    }

    private void getAds() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet)
        {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("PromotionList", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response)
                        {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("PromotionListRESPONSE", response);
                                int count = jsonObject.getInt("count");
                                if (count > 1) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.VISIBLE);
                                }
                                String status = jsonObject.getString("response_code");

                                if (status.equals("10100"))
                                {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AllPromotionsModel rhm = new AllPromotionsModel();
                                        JSONObject itemArray = jsonArray.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setBanner_path(AppUrls.BASE_IMAGE_URL + itemArray.getString("banner_path"));
                                        rhm.setStatus(itemArray.getString("status"));
                                        rhm.setEntity_id(itemArray.getString("entity_id"));
                                        rhm.setEntity_type(itemArray.getString("entity_type"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setEntity_name(itemArray.getString("entity_name"));
                                        promoList.add(rhm);
                                    }

                                    bannerVertical.setAdapter(webBannerAdapter);

                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray1.getJSONObject(0);
                                    String image = AppUrls.BASE_IMAGE_URL + jobj.getString("banner_path");
                                    final String banner_status = jobj.getString("status");
                                    final String banner_user_id = jobj.getString("entity_id");
                                    final String banner_user_type = jobj.getString("entity_type");
                                    final String banner_user_name = jobj.getString("entity_name");
                                    final String admin_id = jobj.getString("admin_id");
                                   Log.d("banner_user_type",banner_user_type+"//"+banner_status);
                                    if(banner_status.equals("PAID"))
                                    {

                                        if(banner_user_id.equals(user_id))
                                        {
                                            Log.d("UUUUUUUU","UUUUUUUUUU");
                                        }
                                        else
                                        {
                                            banner_image.setOnClickListener(new View.OnClickListener()
                                            {
                                                @Override
                                                public void onClick(View view)
                                                {
                                                    Log.d("11111111",banner_user_type);
                                                    if (banner_user_type.equals("USER"))
                                                    {
                                                        Intent intent = new Intent(MainActivity.this, MemberDetailActivity.class);
                                                        intent.putExtra("MEMBER_ID",banner_user_id);
                                                         intent.putExtra("MEMBER_NAME",banner_user_name);
                                                        intent.putExtra("member_user_type",banner_user_type);
                                                        startActivity(intent);
                                                    }
                                                    else if (banner_user_type.equals("GROUP"))
                                                    {
                                                        Intent intent = new Intent(MainActivity.this, GroupDetailActivity.class);
                                                        intent.putExtra("grp_id",banner_user_id);
                                                        intent.putExtra("grp_name",banner_user_name);
                                                        intent.putExtra("GROUP_CONVERSATION_TYPE",banner_user_type);
                                                        intent.putExtra("grp_admin_id",admin_id);
                                                        //  intent.putExtra("grp_name",group_name);
                                                        startActivity(intent);
                                                        // Toast.makeText(MainActivity.this, "Group", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else if (banner_user_type.equals("SPONSOR"))
                                                        {

                                                            Intent intent=new Intent(MainActivity.this, MemberDetailActivity.class);
                                                         //   Log.d("IDDD:",allsponsorMoldelList.get(position).getId());
                                                            intent.putExtra("MEMBER_ID",banner_user_id);
                                                            intent.putExtra("MEMBER_NAME",banner_user_name);
                                                            intent.putExtra("member_user_type",banner_user_type);
                                                           startActivity(intent);
                                                    }
                                                    else
                                                    {}
                                                }
                                            });
                                        }


                                    }
                                    else
                                    {
                                        more.setVisibility(View.VISIBLE);
                                             //nothing
                                    }


                                    Picasso.with(MainActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(banner_image);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        } else {
           // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

        private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL12", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITYCOUNT", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    int call_weekly_loop_ws = jsonArray.getInt("call_weekly_loop_ws");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    int  call_special_event_ws = jsonArray.getInt("call_special_event_ws");
                                    if(call_special_event_ws==1)
                                        getSpecialEventData();
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);
                                    if(call_weekly_loop_ws==1)
                                    {
                                        getWeekLoopData();
                                    }

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getWeekLoopData()
    {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("LoopDataURL", AppUrls.BASE_URL + AppUrls.WEEK_LOOP_DATA+"?user_id="+user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.WEEK_LOOP_DATA+"?user_id="+user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("LoopDataRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    JSONObject jobData = jsonObject.getJSONObject("data");

                                    final String challenge_id = jobData.getString("_id");
                                    final String activity_id = jobData.getString("activity_id");
                                    String special_event_id = jobData.getString("special_event_id");
                                    final String challenge_goal = jobData.getString("challenge_goal");
                                    final String user_id = jobData.getString("user_id");
                                    final String user_type = jobData.getString("user_type");
                                    final String challenge_type = jobData.getString("challenge_type");
                                    final String evaluation_factor = jobData.getString("evaluation_factor");
                                    final String evaluation_factor_unit = jobData.getString("evaluation_factor_unit");
                                    final String amount = jobData.getString("amount");
                                    final String opponent_id = jobData.getString("opponent_id");
                                    final String opponent_type = jobData.getString("opponent_type");
                                    final String location_name = jobData.getString("location_name");
                                    final String location_lat = jobData.getString("location_lat");
                                    final String location_long = jobData.getString("location_long");
                                    final String day_span = jobData.getString("day_span");
                                    String msg = jobData.getString("msg");


                                    new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i)
                                                {
                                                    getCancelWeekLooop(challenge_id,user_id);
                                                    //dialogInterface.cancel();

                                                }
                                            })
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener()
                                            {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which)
                                                {
                                                    //got Pay Page
                                                    Intent intent = new Intent(MainActivity.this, ChallengePaymentActivity.class);

                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("activity", "challengeto");

                                                    intent.putExtra("paymentAmount", amount);
                                                    intent.putExtra("activity_id", activity_id);
                                                  //  intent.putExtra("challenge_id", challenge_id);
                                                    intent.putExtra("challenge_goal", challenge_goal);
                                                    intent.putExtra("evaluation_factor", evaluation_factor);
                                                    intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                    intent.putExtra("type", challenge_type);
                                                    intent.putExtra("span", day_span);
                                                    intent.putExtra("date", "2018-06-12");
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_long);
                                                    intent.putExtra("opponent_id", opponent_id);
                                                    intent.putExtra("opponent_type", opponent_type);


                                                     startActivity(intent);

                                                }
                                            }).show();



                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void getCancelWeekLooop(final String challenge_id, final String user_id)
    {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet)
        {
            Log.d("CANCELWEEKLOOP", AppUrls.BASE_URL + AppUrls.CANCEL_WEEK_LOOP_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CANCEL_WEEK_LOOP_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CANCELWTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {

                                    Toast.makeText(MainActivity.this, "Cancel Successfully", Toast.LENGTH_SHORT).show();

                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("challenge_id", challenge_id);

                    Log.d("WeekCancel_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        }
        else
        {

            Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSpecialEventData() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_DATA_DIALOG +"?user_id="+ user_id;
            Log.d("SPEDATAURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RSEPSPECEVEDATA", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100"))
                                {

                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                     JSONObject jobSpecail=jarray.getJSONObject(0);

                                     String event_name=jobSpecail.getString("name");
                                     String event_description=jobSpecail.getString("description");
                                     String event_start_date=jobSpecail.getString("start_on");
                                     String end_on=jobSpecail.getString("end_on");
                                     String id=jobSpecail.getString("id");
                                     int no_of_days=jobSpecail.getInt("no_of_days");
                                     int follow_status=jobSpecail.getInt("allow_follow");
                                     int user_follow_status=jobSpecail.getInt("has_user_followed");


                                     String roadmap=AppUrls.BASE_IMAGE_URL+jobSpecail.getString("roadmap");
                                      JSONArray tmp=jobSpecail.getJSONArray("location");


                                      getDialog(id,event_name,event_description,event_start_date,end_on,no_of_days,roadmap,tmp,follow_status,user_follow_status);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getAccessControl() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.GET_ACCESS_CONTROL + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    JSONObject ignore_entities = jsonArray.getJSONObject("ignore_entities");
                                    String scratch_card = ignore_entities.getString("scratch_card");
                                    String lottery = ignore_entities.getString("lottery");
                                    String promotion = ignore_entities.getString("promotion");
                                    String sponsoring = ignore_entities.getString("sponsoring");
                                    String challenge_user_group_to_admin = ignore_entities.getString("challenge_user_group_to_admin");
                                    String challenge_group_to_group = ignore_entities.getString("challenge_group_to_group");
                                    String challenge_user_to_user = ignore_entities.getString("challenge_user_to_user");
                                    if (scratch_card.equals("1") || lottery.equals("1")) {
                                        linear_earn_money.setVisibility(View.GONE);
                                    }
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getValidateUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CHECK_USER + user_id + AppUrls.USER_TYPE + user_type;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String is_mobile_verified = jobj.getString("is_mobile_verified");
                                    String is_email_verified = jobj.getString("is_email_verified");
                                    String is_active = jobj.getString("is_active");
                                    String has_payment_done = jobj.getString("has_payment_done");
                                    String has_completed_trial_period = jobj.getString("has_completed_trial_period");
                                    String trial_period_remaining_days = jobj.getString("trial_period_remaining_days");
                                    if (is_active.equals("1")) {
                                       // Toast.makeText(MainActivity.this, "User is Active", Toast.LENGTH_SHORT).show();
                                    } else {
                                      //  Toast.makeText(MainActivity.this, "User is not Active", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUserDetails() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.USER_BASIC_DETAILS + user_id + AppUrls.USER_TYPE + user_type;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String name = jobj.getString("first_name") + " " + jobj.getString("last_name");
                                    String profile_pic = AppUrls.BASE_IMAGE_URL + jobj.getString("profile_pic");
                                    String overall_rank = jobj.getString("overall_rank");
                                    String email = jobj.getString("email");
                                    Picasso.with(MainActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.dummy_user32)
                                          //  .resize(200, 200)
                                            .into(user_profile_image);
                                    Picasso.with(MainActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.dummy_user32)
                                         //   .resize(200, 200)
                                            .into(image_view);
                                    player_user_name.setText(name);
                                    ranking_text.setText(overall_rank);
                                    Log.d("xfkldfbng", profile_pic);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
          //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == linear_home) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
           // if (checkInternet) {
            Intent profile = new Intent(MainActivity.this, MainActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
           /* } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }*/
        }
        if(view==linearnotifications){
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent profile = new Intent(MainActivity.this, UserNotificationsActivity.class);
                startActivity(profile);
                mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_cards) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent profile = new Intent(MainActivity.this, SavedCardDetailsActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_profile) {
            Intent profile = new Intent(MainActivity.this, MyProfileActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
        }
        if (view == linear_recent) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent profile = new Intent(MainActivity.this, RecentHistoryActivity.class);
            startActivity(profile);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_win_lost) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent winlost = new Intent(MainActivity.this, WinLossActivity.class);
            startActivity(winlost);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_mychallenges) {
            Intent challeng = new Intent(MainActivity.this, MyChallengesActivity.class);
            startActivity(challeng);
            mDrawer.closeMenu();
        }
        if (view == linear_my_group) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent group = new Intent(MainActivity.this, GroupsActivity.class);
            startActivity(group);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_allmembers) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent allmembers = new Intent(MainActivity.this, AllMembersActivity.class);
            startActivity(allmembers);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
       /* if (view == linear_all_groups) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent all_groups = new Intent(MainActivity.this, AllGroupsActivity.class);
            startActivity(all_groups);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }*/

         if (view == linear_all_sponsor_list) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent all_sponsor_list = new Intent(MainActivity.this, AllSponsorActivity.class);
            startActivity(all_sponsor_list);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_sponsors_statics) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent sponsor_statics = new Intent(MainActivity.this, SponsorStatisticsActivity.class);
            startActivity(sponsor_statics);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == Linear_following) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent following = new Intent(MainActivity.this, MyFollowing.class);
            startActivity(following);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_mypromotions) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent mypromotions = new Intent(MainActivity.this, MyPromotionActivity.class);
            startActivity(mypromotions);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if(view==linear_sponsor_qrcode){
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet)
            {
                if (user_type.equals("USER"))
                {
                    Intent qrcode = new Intent(MainActivity.this, QRCodeActivity.class);
                    startActivity(qrcode);
                } else {
                    Intent sponerqrcode = new Intent(MainActivity.this, BusinessProfileActivity.class);
                    startActivity(sponerqrcode);
                }

                mDrawer.closeMenu();
            } else
                {
             //   Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    if (user_type.equals("USER"))
                    {
                        Intent qrcode = new Intent(MainActivity.this, QRCodeActivity.class);
                        startActivity(qrcode);
                    } else {
                        Intent sponerqrcode = new Intent(MainActivity.this, BusinessProfileActivity.class);
                        startActivity(sponerqrcode);
                    }
            }
        }
        if (view == linear_earn_money) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent earn_money = new Intent(MainActivity.this, EarnActivity.class);
            startActivity(earn_money);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_refer_friend) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent refer_friend = new Intent(MainActivity.this, ReferFriendActivity.class);
            startActivity(refer_friend);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_advertise) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent advertise = new Intent(MainActivity.this, SportsPromoVideolist.class);
            startActivity(advertise);
            mDrawer.closeMenu();
            } else {
                //Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == linear_help) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet)
            {
                Intent go=new Intent(MainActivity.this, FaqTermsConditionActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","NORMAL");
                go.putExtras(b);
                startActivity(go);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
       /* if (view == linear_terms_and_cond) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent terms_and_cond = new Intent(MainActivity.this, TermsConditionActivity.class);
            startActivity(terms_and_cond);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }*/
       /* if (view == linear_contact_us) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent contact_us = new Intent(MainActivity.this, ContactUsActivity.class);
            startActivity(contact_us);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }*/
        if (view == linear_report) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent report = new Intent(MainActivity.this, FeedbackContactUsActivity.class);
                startActivity(report);
                mDrawer.closeMenu();
            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == linear_deactivate) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent deactivate = new Intent(MainActivity.this, DeactivateAccountActivity.class);
            startActivity(deactivate);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_mysponsorings) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent deactivate = new Intent(MainActivity.this, MySponserActivity.class);
            startActivity(deactivate);
            mDrawer.closeMenu();
            } else {
              //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        /*if (view == linear_me_as_sponsor) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent me_as_sponsor = new Intent(MainActivity.this, MeAsSponsorActivity.class);
                startActivity(me_as_sponsor);
                mDrawer.closeMenu();
            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }*/
//////////////////////////////////////////////
        if (view == linear_sponsor) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent sponsor = new Intent(MainActivity.this, SponsorRequestActivity.class);
            startActivity(sponsor);
            mDrawer.closeMenu();
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_chllenge) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent chellenge = new Intent(MainActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
            mDrawer.closeMenu();
            } else {
             //   Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_messges) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent msg = new Intent(MainActivity.this, ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == more) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
            Intent more = new Intent(MainActivity.this, AllPromotionsActivity.class);
            startActivity(more);
            } else {
               // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }

        }
        if(view.getId()==R.id.follow_Btn_text){
            if(userfollow==1){
                getUnFollowEvent(special_EventId);

            }else {
                getFollowingStatus(special_EventId);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        MenuItem eventitem=menu.findItem(R.id.special_event);
       // blink(eventitem);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (user_type.equals("USER")) {
                    Intent intent = new Intent(MainActivity.this, RunningChallengesActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.special_event:
                Intent eventIntent=new Intent(MainActivity.this, SpecialEvent.class);
                startActivity(eventIntent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    activityAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }

    public void sendUserData(final String user_id, final String device_id, final String lat, final String lng, final String city, final String country, final String state, final String place, final String token) {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FEEDBACK);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CURRENT_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("lat", lat);
                    params.put("long", lng);
                    params.put("city", city);
                    params.put("country", country);
                    params.put("state", state);
                    params.put("place", place);
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public void getToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                   // Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
         //   Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            sendFCMTokes(user_id, user_type, regId);
        } else {
          //  Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gyroscopeObserver.register(this);
        getUserDetails();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
      //  NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        gyroscopeObserver.unregister();
        super.onPause();
    }

    public void sendFCMTokes(final String user_id, final String user_type, final String token_fcm) {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FCM);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.FCM,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                 //   Toast.makeText(MainActivity.this, "Token Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("fcm_register_id", token_fcm);
                    params.put("device_platform", "ANDROID");
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
           // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    //blink menu item text
   private void blink(final MenuItem event){

        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 200;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(flag==0){
                            flag=1;
                            event.setVisible(false);
                        }else{
                            flag=0;
                            event.setVisible(true);
                        }
                        blink(event);
                    }
                });
            }
        }).start();
    }


    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }

    private void showAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("No Internet Connection")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        //alert.setTitle("AlertDialogExample");
        alert.show();
    }

    public void getDialog(final String id, final String event_name, String  event_description, final String  event_start_date, final String end_on, final int  no_of_days, String  roadmap, final JSONArray temp, final int follow, final int userfollowed)
    {
           userfollow=userfollowed;
           Log.v("Tagfgg",">>>"+userfollow);
           special_EventId=id;
        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.special_home_dialog);

        ImageView close = (ImageView)dialog.findViewById(R.id.close);
        TextView special_event_name = (TextView)dialog.findViewById(R.id.special_event_name);
        TextView special_event_startdate = (TextView)dialog.findViewById(R.id.special_event_startdate);
        TextView special_event_no_days = (TextView)dialog.findViewById(R.id.special_event_no_days);
        TextView special_event_dec = (TextView)dialog.findViewById(R.id.special_event_dec);
        ImageView roadmap_img = (ImageView)dialog.findViewById(R.id.roadmap_img);
         follow_Btn_text = (TextView)dialog.findViewById(R.id.follow_Btn_text);
        TextView take_Btn_text = (TextView)dialog.findViewById(R.id.take_Btn_text);
        TextView sponsorBtn_text = (TextView)dialog.findViewById(R.id.sponsorBtn_text);
        follow_Btn_text.setOnClickListener(this);
        if(user_type.equals("USER"))
        {
            take_Btn_text.setVisibility(View.VISIBLE);
        }

        special_event_name.setText(event_name);
        String event_start=parseDateToddMMyyyy(event_start_date);
        special_event_startdate.setText(event_start);

        special_event_dec.setText(event_description);
        special_event_no_days.setText(""+no_of_days);
        Picasso.with(MainActivity.this)
                .load(roadmap)
                .placeholder(R.drawable.dummy_white_group_profile)
                //   .resize(60,60)
                .into(roadmap_img);

        if (userfollowed == 1) {
            follow_Btn_text.setText("UnFollow");
            follow_Btn_text.setTextColor(Color.parseColor("#ffffff"));
            follow_Btn_text.setBackgroundResource(R.drawable.bg_fill);
        } else {
            follow_Btn_text.setText("Follow");
            follow_Btn_text.setTextColor(Color.parseColor("#2BABE2"));
            follow_Btn_text.setBackgroundResource(R.drawable.background_for_login_and_signup);
        }

    /*    follow_Btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

         getUnFollowEvent(id);


            }
        });*/

        take_Btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(MainActivity.this, TakeChalengeActivity.class);
                Log.d("DDDDADAAA",id+"////"+event_start_date+"///"+end_on+"////"+no_of_days+"/////"+temp.toString());
                it.putExtra("eventid", id);
                it.putExtra("startdate", event_start_date);
                it.putExtra("enddate", end_on);
                it.putExtra("noofdays", no_of_days);
                it.putExtra("location", temp.toString());

                startActivity(it);
            }
        });
        sponsorBtn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog = new Dialog(MainActivity.this);
                //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dialog_special_event);
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
                Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);
                TextView activity_name_text = (TextView) dialog.findViewById(R.id.activity_name_text);
                activity_name_text.setText(Html.fromHtml(event_name));

                //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                sendMoneyButton.setOnClickListener(new View.OnClickListener() {

                    @Override

                    public void onClick(View v) {
                        String amt_value = edt_send_amount.getText().toString();
                        if (amt_value.equals("") || amt_value.equals("0")) {
                            Toast.makeText(MainActivity.this, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(MainActivity.this, ChallengePaymentActivity.class);
                            intent.putExtra("activity", "Sponsor_special_event");
                            intent.putExtra("member_id", special_EventId);
                            intent.putExtra("member_user_type", "SPECIAL_EVENT");
                            intent.putExtra("sponsorAmount", amt_value);
                            //   Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                            startActivity(intent);
                        }
                    }

                });
                dialog.show();



            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                viewStatus(id);


            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();

    }

    public void getUnFollowEvent(final String id) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_UNFOLLOW_STATUS;
            Log.d("UnFOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            follow_Btn_text.setText("Follow");
                            follow_Btn_text.setTextColor(Color.parseColor("#2BABE2"));
                            follow_Btn_text.setBackgroundResource(R.drawable.background_for_login_and_signup);
                            follow_Btn_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getFollowingStatus(special_EventId);
                                }
                            });


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("special_event_id", id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    public void getFollowingStatus(final String id) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_FOLLOW_STATUS;
            Log.d("FOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            follow_Btn_text.setText("UnFollow");
                            follow_Btn_text.setTextColor(Color.parseColor("#ffffff"));
                            follow_Btn_text.setBackgroundResource(R.drawable.bg_fill);
                            follow_Btn_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getUnFollowEvent(special_EventId);
                                }
                            });


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("special_event_id", id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }
    private void viewStatus(final String id){
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("viewstatusURL", AppUrls.BASE_URL + AppUrls.VIEW_SPECIAL_EVENT_DATA_DIALOG);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VIEW_SPECIAL_EVENT_DATA_DIALOG,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("viewstatusRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    //Toast.makeText(MainActivity.this,jsonObject.optString("message"),Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("special_event_id", id);

                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
