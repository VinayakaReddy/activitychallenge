package in.activitychallenge.activitychallenge.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.CongratulationsActivity;
import in.activitychallenge.activitychallenge.activities.UserWinWithAdminScratchActivity;
import in.activitychallenge.activitychallenge.fragments.MyChallengesIndividualFragment;
import in.activitychallenge.activitychallenge.holder.ChallengeIndividRunHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeIndividRunItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class ChallengeIndividCompletAdapter extends RecyclerView.Adapter<ChallengeIndividRunHolder>{

    public ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels;
    MyChallengesIndividualFragment context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
    UserSessionManager session;
    String user_name,challenge_type;

    public ChallengeIndividCompletAdapter(ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels, MyChallengesIndividualFragment context, int resource) {
        this.challngIndRunlistModels = challngIndRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.hermes));

        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
    }

    @Override
    public ChallengeIndividRunHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeIndividRunHolder slh = new ChallengeIndividRunHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeIndividRunHolder holder, final int position)
    {

        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = challngIndRunlistModels.get(position).getOpponent_name();
        final String converted_oppname_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        //String username=user_name;
        String username = challngIndRunlistModels.get(position).getUser_name();
        final String converted_user_name = username.substring(0, 1).toUpperCase() + username.substring(1);

        final String winning_status=challngIndRunlistModels.get(position).getWinning_status();

        final String winning_reward_type=challngIndRunlistModels.get(position).getWinning_reward_type();
        final String winning_reward_value=challngIndRunlistModels.get(position).getWinning_reward_value();
        final String is_scratched=challngIndRunlistModels.get(position).getIs_scratched();


        holder.challenger_names.setText(Html.fromHtml(converted_user_name));
        holder.oponent_names.setText(Html.fromHtml(converted_oppname_string));
        holder.vsText.setTypeface(typeface3);

        holder.cash_prize_amount.setText(("$"+challngIndRunlistModels.get(position).getAmount()));
        String timedate=parseDateToddMMyyyy(challngIndRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));

        challenge_type=challngIndRunlistModels.get(position).getChallenge_type();
        if(challenge_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.daily_bg));
        }
        else if(challenge_type.equals("LONGRUN"))
        {
            holder.challenge_type.setText(challenge_type);

            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.longrun_bg));
        }
        else
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.week_bg));
        }



        final String status=challngIndRunlistModels.get(position).getStatus();
      final String ws_winner=challngIndRunlistModels.get(position).getWinner();
      Log.d("ws_winner",""+ws_winner);

        if(status.equals("COMPLETED"))
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            if(ws_winner.equals("challenger"))
            {
                holder.winner_name.setText("Won : "+ converted_user_name);
                if(winning_status.equals("loss"))
                {
                    holder.won_cash.setText(("Lost : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {

                    if(winning_reward_type.equals("SCRATCH_CARD") )
                    {
                        if(is_scratched.equals("0"))
                        {
                            holder.won_cash.setText(("Won,Scratch Card"));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }
                        else
                        {
                            holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getWinning_amount()));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }

                    }
                    else
                    {
                        holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getWinning_amount()));
                        holder.won_cash.setTextColor(Color.parseColor("#239843"));
                    }




                }

            }
            else if(ws_winner.equals("opponent"))
            {
                holder.winner_name.setText("Won : "+converted_oppname_string);

                if(winning_status.equals("loss"))
                {
                    holder.won_cash.setText(("Lost : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {
                    holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getWinning_amount()));
                    holder.won_cash.setTextColor(Color.parseColor("#239843"));
                }


            }
            else if(ws_winner.equals("no"))
            {

                if(challngIndRunlistModels.get(position).getOpponent_name().equals("ADMIN"))
                {
                    holder.winner_name.setText("You Lost the challenge");
                }
                else
                {
                    holder.winner_name.setText("Both Lost the challenge");
                }
                //holder.winner_name.setText("Both Loss the challenge");
                holder.won_cash.setText(("Lost : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));

            }
            else
            {}
           /* if(winning_status.equals("loss"))
            {

                holder.won_cash.setText(("Loss : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
            }
            else
            {

                holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getWinning_amount()));
                holder.won_cash.setTextColor(Color.parseColor("#239843"));
            }
*/
        }
        else if (status.equals("TIE") )
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("You both have won scratch card");
            holder.won_cash.setVisibility(View.GONE);
        }
        else if (status.equals("AUTO_CANCELLATION"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by ADMIN");
        }
        else if ( status.equals("REJECTED"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Rejected by : "+converted_oppname_string);
        }
        else if ( status.equals("MANUAL_CANCELLATION")||status.equals("CANCELLED"))//CANCELLATION
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by : "+converted_user_name);
        }

        holder.status.setText(Html.fromHtml(status));
        holder.status.setBackgroundResource(R.drawable.gradient_toolbar_color);
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context.getActivity())
                .load(challngIndRunlistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


        holder.setItemClickListener(new ChallengeIndividRunItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                if(status.equals("REJECTED") || status.equals("MANUAL_CANCELLATION")|| status.equals("CANCELLED"))
                {
                    //Nothing
                }
                else
                {
                    if(winning_status.equals("loss"))
                    {

                        Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                        intent.putExtra("challenge_id",challngIndRunlistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_oppname_string);
                        intent.putExtra("win_status","LOSS");
                        intent.putExtra("type","USER");
                        intent.putExtra("amount",challngIndRunlistModels.get(position).getAmount());
                        intent.putExtra("is_group_admin",challngIndRunlistModels.get(position).getIs_group_admin());
                        context.startActivity(intent);
                    }
                    else if(winning_status.equals("win"))
                    {

                        if(winning_reward_type.equals("SCRATCH_CARD")  &&  is_scratched.equals("0"))
                        {

                            Intent intent = new Intent(context.getActivity(), UserWinWithAdminScratchActivity.class);
                            intent.putExtra("challenge_id",challngIndRunlistModels.get(position).getChallenge_id());
                            intent.putExtra("is_group_admin", challngIndRunlistModels.get(position).getIs_group_admin());
                            context.startActivity(intent);
                           // context.startActivityForResult(intent, 1);
                        }
                        else
                        {
                            Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                            intent.putExtra("challenge_id",challngIndRunlistModels.get(position).getChallenge_id());
                            intent.putExtra("winner_name",converted_user_name);
                            intent.putExtra("win_status","WON");
                            intent.putExtra("type","USER");
                            intent.putExtra("amount",challngIndRunlistModels.get(position).getWinning_amount());
                            intent.putExtra("is_group_admin",challngIndRunlistModels.get(position).getIs_group_admin());
                            context.startActivity(intent);
                        }



                    }
                    else    // This condition for TIE challenge
                    {
                        Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                        intent.putExtra("challenge_id",challngIndRunlistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_user_name);
                        intent.putExtra("type","USER");
                        intent.putExtra("win_status","TIE");
                        intent.putExtra("amount",challngIndRunlistModels.get(position).getWinning_amount());
                        intent.putExtra("is_group_admin","1");
                        context.startActivity(intent);
                    }
                }

            }
        });


    }



   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String  amount = data.getStringExtra("AMOUNTSEND");

                draw_scrtch_txt.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + amount + "</b>"));
                draw_scrtch_txt.setClickable(false);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }*/

    @Override
    public int getItemCount()
    {
        return this.challngIndRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
