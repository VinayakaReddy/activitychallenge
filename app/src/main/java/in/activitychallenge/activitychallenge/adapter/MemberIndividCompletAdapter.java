package in.activitychallenge.activitychallenge.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.holder.ChallengeIndividRunHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeIndividRunItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class MemberIndividCompletAdapter extends RecyclerView.Adapter<ChallengeIndividRunHolder>{

    public ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels;
    MemberDetailActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
    UserSessionManager session;
    String user_name,challenge_type;




    public MemberIndividCompletAdapter(ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels, MemberDetailActivity context, int resource) {
        this.challngIndRunlistModels = challngIndRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.hermes));

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);


    }

    @Override
    public ChallengeIndividRunHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeIndividRunHolder slh = new ChallengeIndividRunHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeIndividRunHolder holder, final int position)
    {

        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = challngIndRunlistModels.get(position).getOpponent_name();
        String converted_oppname_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem= challngIndRunlistModels.get(position).getUser_name();;
        String converted_user_name = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        String winning_status=challngIndRunlistModels.get(position).getWinning_status();

        holder.challenger_names.setText(Html.fromHtml(converted_user_name));
        holder.oponent_names.setText(Html.fromHtml(converted_oppname_string));
        holder.vsText.setTypeface(typeface3);

        holder.cash_prize_amount.setText(("$"+challngIndRunlistModels.get(position).getAmount()));


        String timedate=parseDateToddMMyyyy(challngIndRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));

        challenge_type=challngIndRunlistModels.get(position).getChallenge_type();
        if(challenge_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        }
        else if(challenge_type.equals("LONGRUN"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
        }
        else
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }


        String status=challngIndRunlistModels.get(position).getStatus();
        if(status.equals("COMPLETED"))
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            if(winning_status.equals("loss"))
            {
                holder.winner_name.setText("Won : "+converted_oppname_string);
                holder.won_cash.setText(("Lost : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
            }
            else
            {
                holder.winner_name.setText("Won : "+converted_user_name);
                holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#239843"));
            }

        }
        else if (status.equals("TIE") )
        {
            holder.ll_completed.setVisibility(View.GONE);
        }
        else if (status.equals("AUTO_CANCELLATION"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by ADMIN");
        }
        else if ( status.equals("REJECTED"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Rejected by : "+converted_oppname_string);
        }
        else if ( status.equals("MANUAL_CANCELLATION")||status.equals("CANCELLED"))//CANCELLATION
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by : "+converted_user_name);
        }


        holder.status.setText(Html.fromHtml(status));
        holder.status.setBackgroundResource(R.drawable.gradient_toolbar_color);
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context)
                .load(challngIndRunlistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


        holder.setItemClickListener(new ChallengeIndividRunItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {


            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.challngIndRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
