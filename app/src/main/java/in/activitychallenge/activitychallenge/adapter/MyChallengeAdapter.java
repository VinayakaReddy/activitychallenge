package in.activitychallenge.activitychallenge.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.MyChallengeActivity;
import in.activitychallenge.activitychallenge.holder.MyChallengeHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MyChallengesListItemClickListener;
import in.activitychallenge.activitychallenge.models.MyChallengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyChallengeAdapter extends RecyclerView.Adapter<MyChallengeHolder> {

    public ArrayList<MyChallengeModel> myChallengeModels;
    MyChallengeActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String token,device_id,user_type;
    UserSessionManager userSessionManager;

    public MyChallengeAdapter(ArrayList<MyChallengeModel> myChallengeModels, MyChallengeActivity context, int resource) {
        this.myChallengeModels = myChallengeModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
    }

    @Override
    public MyChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyChallengeHolder slh = new MyChallengeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyChallengeHolder holder, final int position) {
        
        if (myChallengeModels.get(position).getUser_pic().equals(AppUrls.BASE_IMAGE_URL) && !myChallengeModels.get(position).getGroup_pic().equals(AppUrls.BASE_IMAGE_URL) ) {
            Picasso.with(context)
                    .load(myChallengeModels.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .into(holder.challenger_image);
        }else if (myChallengeModels.get(position).getGroup_pic().equals(AppUrls.BASE_IMAGE_URL) && !myChallengeModels.get(position).getUser_pic().equals(AppUrls.BASE_IMAGE_URL) ) {
            Picasso.with(context)
                    .load(myChallengeModels.get(position).getUser_pic())
                    .placeholder(R.drawable.dummy_user_profile)
                    .into(holder.challenger_image);
        }

        if (myChallengeModels.get(position).getGroup_rank().equals("") && (!myChallengeModels.get(position).getUser_rank().equals("0") || !myChallengeModels.get(position).getUser_rank().equals(""))) {
            holder.rank_rl.setVisibility(View.VISIBLE);
            holder.challenger_rank.setText(myChallengeModels.get(position).getUser_rank());
        }else if(myChallengeModels.get(position).getUser_rank().equals("") && (!myChallengeModels.get(position).getGroup_rank().equals("0") || !myChallengeModels.get(position).getGroup_rank().equals(""))){
            holder.rank_rl.setVisibility(View.VISIBLE);
            holder.challenger_rank.setText(myChallengeModels.get(position).getGroup_rank());
        }

        holder.opponent_name_txt.setText(myChallengeModels.get(position).getUser_name());
        holder.challenge_name.setText(myChallengeModels.get(position).getActivity_name());
        holder.challenger_req_amount_doller.setText("$ "+myChallengeModels.get(position).getAmount());
        if (user_type.equals("USER")) {
            holder.wallet_amount_doller.setText("$ " + myChallengeModels.get(position).getUser_wallet());
        }else if (user_type.equals("GROUP")){
            holder.wallet_amount_doller.setText("$ " + myChallengeModels.get(position).getGroup_wallet());
        }else {
           // Toast.makeText(context, "Check User Type..!", Toast.LENGTH_SHORT).show();
        }

        String status=myChallengeModels.get(position).getStatus();
        if(status.equals("PENDING"))
        {
            holder.cancel_challenge_img.setVisibility(View.VISIBLE);
        }
        holder.status_txt.setText(status);

        holder.cancel_challenge_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Are You Sure?");
                alertDialog.setMessage("You Want To Reject This Challenge?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        rejectChallenge(myChallengeModels.get(position).getId());
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.setItemClickListener(new MyChallengesListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }


    public void rejectChallenge(String challenge_id)
    {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CANCEL_CHALLENGE+challenge_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                           // progressDialog.dismiss();
                            Log.d("RejectChallenge", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                  //  progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");

                                    Intent intent = new Intent(context, MyChallengeActivity.class);
                                    context.startActivity(intent);
                                    context.finish();

                                }
                                if (response_code.equals("10200")) {
                                   // progressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                              //  progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                         //   progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }else {
           // progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getItemCount() {
        return this.myChallengeModels.size();
    }
}
