package in.activitychallenge.activitychallenge.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.FiveKmsActivity;
import in.activitychallenge.activitychallenge.activities.FortyfiveMinsActivity;
import in.activitychallenge.activitychallenge.activities.GetLocationActivity;
import in.activitychallenge.activitychallenge.activities.PicturePosesActivity;
import in.activitychallenge.activitychallenge.activities.SixtyMinsActivity;
import in.activitychallenge.activitychallenge.activities.StepsChallengeActivity;
import in.activitychallenge.activitychallenge.activities.ThirtyMinsActivity;
import in.activitychallenge.activitychallenge.filters.ActivityFilterList;
import in.activitychallenge.activitychallenge.holder.ActivityHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class ActivityAdapter extends RecyclerView.Adapter<ActivityHolder> implements Filterable {

    public ArrayList<ActivityModel> listModels, filterList;
    MainActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    private static final int PLACE_PICKER_REQUEST = 1;
    Sensor stepSensor;
    SensorManager sManager;
    ActivityFilterList filter;
    UserSessionManager session;
    String user_id, token, device_id, user_type = "",activname,actvity_id;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    String location_name = "", location_lat = "", location_lng = "";
    Dialog dialog;

    public ActivityAdapter(ArrayList<ActivityModel> listModels, MainActivity context, int resource) {
        this.listModels = listModels;
        this.context = context;
        this.resource = resource;
        this.filterList = listModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        gps = new GPSTracker(context);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        checkInternet = NetworkChecking.isConnected(context);

    }

    @Override
    public ActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ActivityHolder slh = new ActivityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ActivityHolder holder, final int position) {
        gps = new GPSTracker(context);
    //    Log.d("STTTXXX",listModels.get(position).getLongrun_challenges());

        activname = listModels.get(position).getActivity_name();
        actvity_id=  listModels.get(position).getId();
        geocoder = new Geocoder(context, Locale.getDefault());
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //   Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);

            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                location_name = address;
                location_lat = lat;
                location_lng = lng;
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }


        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -"+listModels.get(position).getActivity_name());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        Log.d("STTTXXX",listModels.get(position).getLongrun_challenges());
        if (sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null && sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && gps.canGetLocation()) {
            if (!listModels.get(position).getDaily_challenges().equals("0") || !listModels.get(position).getWeekly_challenges().equals("0") ||!listModels.get(position).getLongrun_challenges().equals("0") ) {
                holder.card.setBackgroundResource(R.drawable.act_round_bg);
                if(listModels.get(position).getTools_required().contains("GPS_SENSOR")) {
                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {

                            if (user_type.equals("USER"))
                            {
                                if (listModels.get(position).getMin_value().equals("5000 STEPS")) {
                                    // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                    if (listModels.get(position).getMin_value().equals("Walking")) {
                                        Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                    }
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setCancelable(false)
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {

                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "5000steps");
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            Log.d("sgvsdfnvknf","value"+listModels.get(pos).getLongrun_challenges());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(context, StepsChallengeActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);

                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            Log.d("sgvsdfnvknfff","value"+listModels.get(pos).getLongrun_challenges()+"---"+listModels.get(pos).getWeekly_challenges());
                                                            context.startActivity(intent);




                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setCancelable(false)
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {
                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "30mins");
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);

                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);
                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setCancelable(false)
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {
                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "45mins");
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);

                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);
                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();

                                } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setCancelable(false)
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {
                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "60mins");
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, SixtyMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);

                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);
                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();

                                } else if (listModels.get(position).getMin_value().equals("5 KM")) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setCancelable(false)
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {
                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "5km");
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, FiveKmsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);

                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);
                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                            .setCancelable(false)
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet) {
                                                        if (listModels.get(pos).getTools_required().contains("GPS_SENSOR")) {
                                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("type", "hollow_pictures");
                                                            intent.putExtra("event","main");
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, PicturePosesActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                            intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);
                                                        }
                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                }

                            } else
                                {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>Would you like to Sponsor to this Activity?</font>"))
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet)
                                                    {
                                                        dialog.cancel();
                                                        activname=listModels.get(position).getActivity_name();
                                                        actvity_id=listModels.get(position).getId();

                                                        getSponsorActivityDialog(activname,actvity_id);


                                                    }
                                                    else
                                                        {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                //Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
                } else {
                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {

                            if (user_type.equals("USER")) {
                                if (listModels.get(position).getMin_value().equals("5000 STEPS") || listModels.get(position).getMin_value().equals("25 PUSHUP")) {
                                    // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                    if (listModels.get(position).getMin_value().equals("Walking")) {
                                        Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                    }

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, StepsChallengeActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                        intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }


                                } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                         intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }

                                } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                        intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }


                                } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, SixtyMinsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                        intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }


                                } else if (listModels.get(position).getMin_value().equals("5 KM")) {

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, FiveKmsActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                        intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }

                                } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {

                                                    if (checkInternet) {

                                                            Intent intent = new Intent(context, PicturePosesActivity.class);
                                                            intent.putExtra("id", listModels.get(pos).getId());
                                                            intent.putExtra("user_id", user_id);
                                                            intent.putExtra("user_type", user_type);
                                                            intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                            intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                        intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            context.startActivity(intent);

                                                    } else {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }

                                }

                            } else {

                                if(listModels.get(position).getTools_required().contains("PROXIMITY_SENSOR")){
                                    new AlertDialog.Builder(context)
                                            .setTitle("Hi")
                                            .setMessage(Html.fromHtml("<font color='#026c9b'>Would you like to Sponsor to this Activity?</font>"))
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (checkInternet)
                                                    {
                                                        dialog.cancel();
                                                        activname=listModels.get(position).getActivity_name();
                                                        actvity_id=listModels.get(position).getId();

                                                        getSponsorActivityDialog(activname,actvity_id);


                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).show();
                                }else {
                                    Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();

                                }
                            }

                        }

                    });
                    }
            } else {
                holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

                holder.setItemClickListener(new ActivityItemClickListener() {
                    @Override
                    public void onItemClick(View v, final int pos) {
                        if (checkInternet) {
                        if (user_type.equals("USER")) {
                        new AlertDialog.Builder(context)
                                .setTitle("Sorry...")
                                .setMessage( Html.fromHtml("<font color='#FF7F27'>This activity is offline</font>"))
                                .setCancelable(false)
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton( Html.fromHtml("<font color='#000'>Back</font>"), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                        }else {
                            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                        }
                        }else {
                            Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        } else {

            Toast.makeText(context, "Sensors not Working", Toast.LENGTH_SHORT).show();
            holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    if (user_type.equals("USER")) {
                    new AlertDialog.Builder(context)
                            .setTitle("Unsupportable Device")
                            .setMessage("Your device needs sensors, to perform this action.")
                            .setCancelable(false)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                    }else {
                        Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        holder.name.setText(listModels.get(position).getActivity_name());

        Picasso.with(context)
                .load(listModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);
        Log.d("xfkldfbng", holder.name.getText().toString());


    }

    private void getSponsorActivityDialog(String activtyname, final String activityid)
    {
        dialog = new Dialog(context);
        //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_sponsor_activity);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
        Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);
        TextView activity_name_text = (TextView) dialog.findViewById(R.id.activity_name_text);
        activity_name_text.setText(Html.fromHtml(activtyname));

        //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
        sendMoneyButton.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String amt_value = edt_send_amount.getText().toString();
                if (amt_value.equals("") || amt_value.equals("0")) {
                    Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, ChallengePaymentActivity.class);
                    intent.putExtra("activity", "MeAsSponsorActivity");
                    intent.putExtra("member_id", activityid);
                    intent.putExtra("member_user_type", "ACTIVITY");
                    intent.putExtra("sponsorAmount", amt_value);
                    //   Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                    context.startActivity(intent);
                }
            }

        });
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return this.listModels.size();
    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ActivityFilterList(filterList, this);
        }

        return filter;
    }
}
