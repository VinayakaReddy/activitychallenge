package in.activitychallenge.activitychallenge.adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.SponsorAsActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForSponsorAsActivity;
import in.activitychallenge.activitychallenge.holder.SponsorAsActivytHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.SponsorAsActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.ActivityModel;


public class SponsorAsActivtyAdapter extends RecyclerView.Adapter<SponsorAsActivytHolder> implements Filterable {

    public ArrayList<ActivityModel> acticvityModels, allmembersfilterList;
    SponsorAsActivity context;
    LayoutInflater li;
    int resource;

  Dialog dialog;
    CustomFilterForSponsorAsActivity filter;


    public SponsorAsActivtyAdapter(ArrayList<ActivityModel> acticvityModels, SponsorAsActivity context, int resource) {
        this.acticvityModels = acticvityModels;
        this.context = context;
        this.resource = resource;
        this.allmembersfilterList = acticvityModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public SponsorAsActivytHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SponsorAsActivytHolder slh = new SponsorAsActivytHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SponsorAsActivytHolder holder, final int position)
    {

        String str = acticvityModels.get(position).getActivity_name();
        final String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.sp_act_name.setText(Html.fromHtml(converted_string));



         String  img_profile=acticvityModels.get(position).getActivity_image();

          Log.d("PPPROFILE",img_profile);
         if(img_profile.equals("null") && img_profile.equals(""))
         {
             Picasso.with(context)
                     .load(R.drawable.placeholder_dummy)
                     .placeholder(R.drawable.dummy_user_profile)
                     .resize(60,60)
                     .into(holder.sp_acti_img);
         }
         else
         {
             Picasso.with(context)
                     .load(acticvityModels.get(position).getActivity_image())
                     .placeholder(R.drawable.dummy_user_profile)
                     .into(holder.sp_acti_img);
         }


    holder.sp_acti_sponsor_btn.setOnClickListener(new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            dialog = new Dialog(context);
          //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_sponsor_activity);
            Window window = dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);

            final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
            Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);
            TextView activity_name_text = (TextView) dialog.findViewById(R.id.activity_name_text);
            activity_name_text.setText(Html.fromHtml(converted_string));

          //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
            sendMoneyButton.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {
                    String amt_value = edt_send_amount.getText().toString();
                    if (amt_value.equals("") || amt_value.equals("0")) {
                        Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(context, ChallengePaymentActivity.class);
                        intent.putExtra("activity", "MeAsSponsorActivity");
                        intent.putExtra("member_id", acticvityModels.get(position).getId());
                        intent.putExtra("member_user_type", "ACTIVITY");
                        intent.putExtra("sponsorAmount", amt_value);
                     //   Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                       context.startActivity(intent);
                    }
                }

            });
            dialog.show();

        }
    });


        holder.setItemClickListener(new SponsorAsActivityItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.acticvityModels.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForSponsorAsActivity(allmembersfilterList, this);
        }

        return filter;
    }
}
