package in.activitychallenge.activitychallenge.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AdvertiseScratchCardActivity;
import in.activitychallenge.activitychallenge.activities.CardPageActivity;
import in.activitychallenge.activitychallenge.fragments.EarnMoneyAdvertisement;
import in.activitychallenge.activitychallenge.holder.SportsPromoVideoHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.PromoVideoClickListner;
import in.activitychallenge.activitychallenge.models.SportsPromoVideoModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

/**
 * Created by Devolper on 14-Mar-18.
 */


public class SportsPromoAdVideoAdapter extends RecyclerView.Adapter<SportsPromoVideoHolder> {
    EarnMoneyAdvertisement ctx;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    ArrayList<SportsPromoVideoModel> promoList;
    int resource;
    LayoutInflater lInfla;
    String device_id, access_token, vid_uri, txt_title,userid,usertype;
    DisplayMetrics dm;
    MediaController media_Controller;
    ProgressBar progressBar;
    private InterstitialAd mInterstitialAd;

    public SportsPromoAdVideoAdapter(ArrayList<SportsPromoVideoModel> promoList,EarnMoneyAdvertisement ctx,  int resource) {
        this.ctx = ctx;
        this.resource = resource;
        this.promoList = promoList;
        lInfla = (LayoutInflater) ctx.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        session = new UserSessionManager(ctx.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        usertype = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        pprogressDialog = new ProgressDialog(ctx.getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        MobileAds.initialize(ctx.getActivity(), ctx.getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(ctx.getActivity());
        initializeInterstitialAd();
        loadInterstitialAd();
    }

    @Override
    public SportsPromoVideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inf = lInfla.inflate(resource, parent, false);
        SportsPromoVideoHolder rHol = new SportsPromoVideoHolder(inf);
        return rHol;

    }

    @Override
    public void onBindViewHolder(SportsPromoVideoHolder holder, final int position)
    {
        holder.title.setText(promoList.get(position).getCompany_name());

        final String aaa = promoList.get(position).getVideo_path();
        final String video_id = promoList.get(position).getId();

        holder.setItemClickListener(new PromoVideoClickListner()
        {
            @Override
            public void onItemClick(View v, int pos) {
                progressBar = new ProgressBar(ctx.getActivity(), null, android.R.attr.progressBarStyleSmall);
                progressBar.setVisibility(View.VISIBLE);
                //  vid_uri = AppUrls.BASE_IMAGE_URL + promoList.get(pos).getVideo_path();
                txt_title = promoList.get(pos).getCompany_name();
                Log.d("dsfdfdf", txt_title);
                try {
                    // Start the MediaController

                    String uriPath = AppUrls.BASE_IMAGE_URL + promoList.get(pos).getVideo_path();
                    Log.d("VIDEOPATH", uriPath);
                    if(uriPath.equals(""))
                    {
                        if (mInterstitialAd.isLoaded())
                        {
                            Log.d("wwwwwww", "The interstitial loaded.");
                            mInterstitialAd.show();
                            mInterstitialAd.setAdListener(new AdListener()
                            {
                                // Listen for when user closes ad
                                public void onAdClosed()
                                {
                                    // When user closes ad end this activity (go back to first activity)
                                   getScratchData(video_id);
                                }
                            });

                        }
                    }
                     else
                    {
                        final Dialog dialog = new Dialog(ctx.getActivity());// add here your class name
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.scratch_custom_videoview_dialog);
                        final VideoView adv_vid_view = (VideoView) dialog.findViewById(R.id.scratch_vid_view);
                        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);

                        dialog.show();
                        //progressBar.setVisibility(View.VISIBLE);
                        media_Controller = new MediaController(ctx.getActivity());
                        dm = new DisplayMetrics();
                        ctx.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                        int height = dm.heightPixels;
                        int width = dm.widthPixels;
                        adv_vid_view.setMinimumWidth(width);
                        adv_vid_view.setMinimumHeight(height);

                        try {
                            Uri video = Uri.parse(uriPath);
                            adv_vid_view.setVideoURI(video);
                            adv_vid_view.setMediaController(media_Controller);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                        adv_vid_view.requestFocus();
                        adv_vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            // Close the progress bar and play the video
                            public void onPrepared(MediaPlayer mp) {
                                progressBar.setVisibility(View.GONE);
                                adv_vid_view.start();

                            }
                        });

                        dialog.setCanceledOnTouchOutside(false);

                        adv_vid_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                // txtil.setVisibility(View.GONE);
                                dialog.dismiss();
                                getScratchData(video_id);


                                //call scratch Card after completion video

                            }
                        });

                    }


                       /* try
                        {
                            Uri video = Uri.parse(uriPath);
                            vid_view.setVideoURI(video);
                            vid_view.setMediaController(mediacontroller);
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                        vid_view.requestFocus();
                        vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            // Close the progress bar and play the video
                            public void onPrepared(MediaPlayer mp) {

                                vid_view.start();

                            }
                        });*/
                  //  txtil.setText(txt_title);
                    Log.d("SSCCC", txt_title);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return promoList.size();
    }

    private void getScratchData(String video_id)
    {
        checkInternet = NetworkChecking.isConnected(ctx.getActivity());
        if (checkInternet)
        {
            Log.d("vvVIDEURL", AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO_COMPLETION+"?user_type="+usertype+"&user_id="+userid+"&adv_id="+video_id);

            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO_COMPLETION+"?user_type="+usertype+"&user_id="+userid+"&adv_id="+video_id,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();

                            Log.d("COMPLETRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    pprogressDialog.dismiss();
                                 /*   "data": {
                                    "scratch_card": {
                                        "amount": 0.03,
                                                "id": "5aa767d1839b7e26b41b0008",
                                                "is_scratched": 0,
                                                "scratching_date": "2018-03-12 18:30:00",
                                                "view": 1
                                    }*/
                                    JSONObject jsobjj_data = jsonObject.getJSONObject("data");
                                    JSONObject jsob_scratch_data = jsobjj_data.getJSONObject("scratch_card");
                                    String amount=jsob_scratch_data.getString("amount");
                                    String id=jsob_scratch_data.getString("id");
                                    String is_scratched=jsob_scratch_data.getString("is_scratched");
                                    String view=jsob_scratch_data.getString("view");
                                    if(is_scratched.equals("0"))
                                    {
                                        Intent sc_history = new Intent(ctx.getActivity(), AdvertiseScratchCardActivity.class);

                                        sc_history.putExtra("AMOUNT", amount);
                                        sc_history.putExtra("SCRATCH_ID", id);
                                        ctx.startActivity(sc_history);
                                    }
                                    else
                                    {
                                        //nothing
                                    }



                                }
                               /* if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(SportsPromoVideolist.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("PROMOMMOVID_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ctx.getActivity());
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(ctx.getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void loadInterstitialAd() {
        Log.d("LAAAAa","LAAAAa");


        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }
    private void initializeInterstitialAd() {
        mInterstitialAd = new InterstitialAd(ctx.getActivity());
        mInterstitialAd.setAdUnitId(ctx.getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded()
            {
                Log.d("XXXXAD","XXXXAD");
                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                Log.d("FAILED","FAILED");
            }

            @Override
            public void onAdOpened() {
                Log.d("AdOpened","AdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.d("AdLeftApp","AdLeftApp");
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(ctx.getActivity(), "Interstitial Ad Closed",
                        Toast.LENGTH_SHORT).show();
                //Load the ad and keep so that it can be used
                // next time the ad needs to be displayed
                loadInterstitialAd();

            }
        });
    }

}