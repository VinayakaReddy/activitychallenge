package in.activitychallenge.activitychallenge.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.WinLossActivity;
import in.activitychallenge.activitychallenge.fragments.AllWinLossFragment;
import in.activitychallenge.activitychallenge.holder.WinLossChalngeHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.WinLossChalngItemClickListener;
import in.activitychallenge.activitychallenge.models.WinLossChllengeModel;


public class WinLossChalngeAdapter extends RecyclerView.Adapter<WinLossChalngeHolder>  {

    public ArrayList<WinLossChllengeModel> winlossClistModels;
    AllWinLossFragment context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;

    public WinLossChalngeAdapter(ArrayList<WinLossChllengeModel> winlossClistModels, AllWinLossFragment context, int resource) {
        this.winlossClistModels = winlossClistModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));


    }

    @Override
    public WinLossChalngeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        WinLossChalngeHolder slh = new WinLossChalngeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final WinLossChalngeHolder holder, final int position)
    {


     /*   activity_image=(ImageView)itemView.findViewById(R.id.activity_image);

        activity_name=(TextView) itemView.findViewById(R.id.activity_name);
        cash_won=(TextView) itemView.findViewById(R.id.cash_won);
        opponent_name=(TextView) itemView.findViewById(R.id.opponent_name);
        date_text=(TextView) itemView.findViewById(R.id.date_text);
        status_text=(TextView) itemView.findViewById(R.id.status_text);*/

        holder.activity_name.setText(Html.fromHtml(winlossClistModels.get(position).getActivity_name()));

        holder.opponent_name.setText(Html.fromHtml(winlossClistModels.get(position).getOpponent_name()));
        holder.date_text.setText(Html.fromHtml(winlossClistModels.get(position).getCompleted_on_txt()));

        String status=winlossClistModels.get(position).getWinning_status();
        if(status.equals("won"))
        {
           // holder.status_text.setText(Html.fromHtml(winlossClistModels.get(position).getWinning_status()));
            holder.cash_won_title_text.setText("Cash Won");
            holder.status_text.setText(status);
            holder.cash_won.setText(Html.fromHtml("&#36;"+winlossClistModels.get(position).getWinning_amount()));
            holder.status_text.setBackgroundColor(Color.parseColor("#239843"));
        }
        else if(status.equals("lost"))
        {
         //   holder.status_text.setText(Html.fromHtml(winlossClistModels.get(position).getWinning_status()));
            holder.cash_won_title_text.setText("Cash Loss");
            holder.status_text.setText(status);
            holder.cash_won.setText(Html.fromHtml("&#36;"+winlossClistModels.get(position).getAmount()));
            holder.status_text.setBackgroundColor(Color.parseColor("#DD6B55"));
        }
         else
        {
            holder.cash_won_title_text.setText("Cash ");
            //   holder.status_text.setText(Html.fromHtml(winlossClistModels.get(position).getWinning_status()));
            holder.cash_won.setText(Html.fromHtml("&#36;"+winlossClistModels.get(position).getWinning_amount()));
            holder.status_text.setText("Draw");
            holder.status_text.setBackgroundColor(Color.parseColor("#0987be"));
        }





        Picasso.with(context.getActivity())
                .load(winlossClistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);




        holder.setItemClickListener(new WinLossChalngItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.winlossClistModels.size();
    }



}
