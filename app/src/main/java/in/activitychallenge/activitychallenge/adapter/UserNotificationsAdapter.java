package in.activitychallenge.activitychallenge.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.UserNotificationHolder;
import in.activitychallenge.activitychallenge.models.UserNotification;

public class UserNotificationsAdapter extends RecyclerView.Adapter<UserNotificationHolder> {
    public ArrayList<UserNotification> userNotificationsList;
    Context mContext;
    LayoutInflater li;
    int resource;
    String  msgBody;
    Dialog dialog;

    public UserNotificationsAdapter(ArrayList<UserNotification> notificationArrayList, Context context, int resource) {
        this.userNotificationsList = notificationArrayList;
        this.mContext = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public UserNotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        UserNotificationHolder specialHolder = new UserNotificationHolder(layout);
        return specialHolder;
    }

    @Override
    public void onBindViewHolder(UserNotificationHolder holder, final int position) {
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.mp_regular));

        holder.notification_txt.setText(userNotificationsList.get(position).title);

        holder.notification_txt.setTypeface(typeface);
        String createddate = userNotificationsList.get(position).sentOn;
        String createtime=userNotificationsList.get(position).sentOnText;
       // msgBody=userNotificationsList.get(position).text;

        String[] dateStr = createddate.split(" ");

       holder.created_date_txt.setText(createtime);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog = new Dialog(mContext);
                //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dialog_message_detail);
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);


                Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                TextView message_detail_text = (TextView) dialog.findViewById(R.id.message_detail_text);
               message_detail_text.setText(Html.fromHtml(userNotificationsList.get(position).text));

                //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                ok_btn.setOnClickListener(new View.OnClickListener() {

                    @Override

                    public void onClick(View v)
                    {
                        dialog.dismiss();
                    }

                });
                dialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return userNotificationsList.size();
    }

    public String parseDateToddMMyyyy(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
