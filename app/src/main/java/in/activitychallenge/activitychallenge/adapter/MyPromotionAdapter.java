package in.activitychallenge.activitychallenge.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 12/21/2017.
 */

public class MyPromotionAdapter extends FragmentPagerAdapter
{

    private final List<Fragment> gFragmentList = new ArrayList<>();
    private final List<String> gFragmentTitleList = new ArrayList<>();

    public MyPromotionAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return gFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return gFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        gFragmentList.add(fragment);
        gFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return gFragmentTitleList.get(position);
    }
}
