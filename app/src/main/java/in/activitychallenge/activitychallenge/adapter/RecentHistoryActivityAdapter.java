package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.activities.RecentHistoryActivity;
import in.activitychallenge.activitychallenge.holder.RecentHistoryActivityHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.RecentHistoryActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.RecentHistoryModel;

public class RecentHistoryActivityAdapter extends RecyclerView.Adapter<RecentHistoryActivityHolder> {

    public ArrayList<RecentHistoryModel> recentHistoryModels;
    RecentHistoryActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;

    public RecentHistoryActivityAdapter(ArrayList<RecentHistoryModel> recentHistoryModels, RecentHistoryActivity context, int resource) {
        this.recentHistoryModels = recentHistoryModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public RecentHistoryActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        RecentHistoryActivityHolder slh = new RecentHistoryActivityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final RecentHistoryActivityHolder holder, final int position) {

        String str = recentHistoryModels.get(position).getTitle();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.title_txt.setText(Html.fromHtml(converted_string));
        holder.amount_txt.setText(" "+recentHistoryModels.get(position).getAmount()+" "+"USD");

         String txn_flag=recentHistoryModels.get(position).getTxn_flag();
        if(txn_flag.equals("SCRATCH_CARD_WEEKLY"))
        {
            if (!recentHistoryModels.get(position).getFor_span().equals(""))
            {
                holder.txn_flag_txt.setVisibility(View.VISIBLE);
                holder.txn_flag_txt.setText(recentHistoryModels.get(position).getFor_span());
            }
            else {
                holder.txn_flag_txt.setVisibility(View.GONE);
            }
        }



     //   holder.txn_flag_txt.setText(txn_flag);
        holder.txn_type_txt.setText(recentHistoryModels.get(position).getTxn_type());
        if (recentHistoryModels.get(position).getTxn_type().equals("DEBIT")){
            holder.back_layout.setBackgroundColor(Color.parseColor("#DD6B55"));
        }
        else if (recentHistoryModels.get(position).getTxn_type().equals("CREDIT")){
            holder.back_layout.setBackgroundColor(Color.parseColor("#239843"));
        }


        holder.status.setText(recentHistoryModels.get(position).getTxn_status());
        holder.time_txt.setText(recentHistoryModels.get(position).getCreated_on() /*+ " " +"(" + recentHistoryModels.get(position).getCreated_on_txt() + ")"*/);
        holder.updated_time_txt.setText(recentHistoryModels.get(position).getUpdated_on() /*+" " + "(" + recentHistoryModels.get(position).getUpdated_on_txt() + ")"*/);

        holder.setItemClickListener(new RecentHistoryActivityItemClickListener() {
            @Override
            public void onItemClick(View v, final int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.recentHistoryModels.size();
    }
}
