package in.activitychallenge.activitychallenge.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllMembersActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeAcceptLocationActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeRequestActivity;
import in.activitychallenge.activitychallenge.activities.FullImage;
import in.activitychallenge.activitychallenge.activities.HoloImageZoomActivity;
import in.activitychallenge.activitychallenge.activities.MyProfileActivity;
import in.activitychallenge.activitychallenge.activities.PictureChallengeStatusActivity;
import in.activitychallenge.activitychallenge.holder.ChallengeRequestHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengesListItemClickListener;
import in.activitychallenge.activitychallenge.models.AllMembersModel;
import in.activitychallenge.activitychallenge.models.ChallengeRequestModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;
import in.activitychallenge.activitychallenge.utilities.Utils;

public class ChallengeRequestAdapter extends RecyclerView.Adapter<ChallengeRequestHolder> implements View.OnClickListener {

    public ArrayList<ChallengeRequestModel> challengeRequestModels;
    ChallengeRequestActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;


    /*Challenge Accept and Reject*/
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    String challengegoal = "", activityname = "", locationName = "";
    String challenge_id, challengeAmount, walletAmount, lat, lng, address, place, city, state, country, postalCode, knownName, token = "", device_id = "", user_type = "";

    public ChallengeRequestAdapter(ArrayList<ChallengeRequestModel> challengeRequestModels, ChallengeRequestActivity context, int resource) {
        this.challengeRequestModels = challengeRequestModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gps = new GPSTracker(context);
        geocoder = new Geocoder(context, Locale.getDefault());

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
    }

    @Override
    public ChallengeRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeRequestHolder slh = new ChallengeRequestHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ChallengeRequestHolder holder, final int position) {

        challenge_id = challengeRequestModels.get(position).getId();
        Log.d("CHECKING", challengeAmount + "\n" + walletAmount);

        if (challengeRequestModels.get(position).getUser_pic().equals(AppUrls.BASE_IMAGE_URL) && !challengeRequestModels.get(position).getGroup_pic().equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(context)
                    .load(challengeRequestModels.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .resize(70, 70)
                    .into(holder.challenger_image);
        } else if (challengeRequestModels.get(position).getGroup_pic().equals(AppUrls.BASE_IMAGE_URL) && !challengeRequestModels.get(position).getUser_pic().equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(context)
                    .load(challengeRequestModels.get(position).getUser_pic())
                    .placeholder(R.drawable.dummy_user_profile)
                    .resize(70, 70)
                    .into(holder.challenger_image);
        }

        if (challengeRequestModels.get(position).getGroup_name().equals("")) {
            holder.challenger_req_name.setText(challengeRequestModels.get(position).getUser_name());
        } else {
            holder.challenger_req_name.setText(challengeRequestModels.get(position).getGroup_name());
        }

        if (challengeRequestModels.get(position).getGroup_rank().equals("") && !challengeRequestModels.get(position).getUser_rank().equals("")) {
            holder.rank_rl.setVisibility(View.VISIBLE);
            holder.challenger_rank.setText(challengeRequestModels.get(position).getUser_rank());
        } else if (challengeRequestModels.get(position).getUser_rank().equals("") && !challengeRequestModels.get(position).getGroup_rank().equals("")) {
            holder.rank_rl.setVisibility(View.VISIBLE);
            holder.challenger_rank.setText(challengeRequestModels.get(position).getGroup_rank());
        }
        activityname = challengeRequestModels.get(position).getActivity_name();
        holder.challenge_name.setText(activityname);
        challengegoal = challengeRequestModels.get(position).getChallenge_goal();
        String evaluation_factor = challengeRequestModels.get(position).getEvaluation_factor();
        if (evaluation_factor.equalsIgnoreCase("IMAGE")) {
            holder.challenge_goal_img.setVisibility(View.VISIBLE);
            holder.challenge_goal_img.setOnClickListener(this);
            holder.challenge_goal_img.setTag(position);
            Picasso.with(context)
                    .load(AppUrls.BASE_IMAGE_URL + challengeRequestModels.get(position).getChallenge_goal())
                    .into(holder.challenge_goal_img);

        } else {
            holder.challenge_goal_txt.setVisibility(View.VISIBLE);
            holder.challenge_evaluation_unit.setVisibility(View.VISIBLE);
            holder.challenge_evaluation_unit.setText(" " + challengeRequestModels.get(position).getEvaluation_factor_unit());
            holder.challenge_goal_txt.setText(challengeRequestModels.get(position).getChallenge_goal());
        }

        final String challenge_type = challengeRequestModels.get(position).getChallenge_type();
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        } else if (challenge_type.equals("LONGRUN")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
            //holder.challenge_type.setTextColor(R.color.longrun_bg);
        } else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }


        /*if (user_type.equals("USER")){
            holder.challenger_req_amount_doller.setText("$ "+challengeRequestModels.get(position).getUser_wallet());
        }else if (user_type.equals("GROUP")){
            holder.challenger_req_amount_doller.setText("$ "+challengeRequestModels.get(position).getGroup_wallet());
        }else {
            Toast.makeText(context, "Check User Type..!", Toast.LENGTH_SHORT).show();
        }*/
        challengeAmount = challengeRequestModels.get(position).getAmount();
        holder.challenger_req_amount_doller.setText("$ " + challengeAmount);


        holder.accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("Testsing","OnClick");
                if ((context).checkPermission()) {

                    // Snackbar.make(view, "Permission already granted.", Snackbar.LENGTH_LONG).show();
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        lat = String.valueOf(latitude);
                        double longitude = gps.getLongitude();
                        lng = String.valueOf(longitude);
                        Log.d("LOCATION", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                        try {
                            addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            address = addresses.get(0).getAddressLine(0);
                            place = addresses.get(0).getAddressLine(0);
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            knownName = addresses.get(0).getFeatureName();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //acceptChallenge();

                        /*Intent intent = new Intent(context, ChallengePaymentActivity.class);
                        intent.putExtra("activity","ChallengeRequestAdapter");
                        intent.putExtra("challengeAmount",challengeRequestModels.get(position).getAmount());
                        //intent.putExtra("walletAmount",challengeRequestModels.get(position).getWallet_amt());
                        Log.d("RECHECKING",challengeAmount+"\n"+walletAmount);
                        intent.putExtra("challenge_id",challengeRequestModels.get(position).getId());
                        intent.putExtra("location_name",address);
                        intent.putExtra("location_lat",lat);
                        intent.putExtra("location_long",lng);
                        context.startActivity(intent);*/
                        if (challenge_type.equalsIgnoreCase("SPECIAL_EVENT")) {
                            Log.v("LocationName",locationName+"//"+challengeRequestModels.get(position).getSpecial_eventId());
                            locationName = challengeRequestModels.get(position).getLocationName();
                            getSpecialEventDetails(challengeRequestModels.get(position).getSpecial_eventId());
                        } else {
                            Intent intent = new Intent(context, ChallengeAcceptLocationActivity.class);
                            intent.putExtra("activity", "ChallengeRequestAdapter");
                            intent.putExtra("challenge_id", challengeRequestModels.get(position).getId());
                            intent.putExtra("challengeAmount", challengeRequestModels.get(position).getAmount());
                            intent.putExtra("event", "main");
                            Log.d("RECHECKING", challengeAmount + "\n" + walletAmount);
                            context.startActivity(intent);
                        }


                    } else {
                        gps.showSettingsAlert();
                    }

                } else {

                    Snackbar.make(view, "Please request permission.", Snackbar.LENGTH_LONG).show();

                    (context).requestPermission();

                }
            }
        });

        holder.reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Are You Sure?");
                alertDialog.setMessage("You Want To Reject This Challenge?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        rejectChallenge();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.setItemClickListener(new ChallengesListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    //get special event details
    private void getSpecialEventDetails(final String id) {
        Log.d("SPECIALEVENTACCEPTURL", "hhjhjjh"+id);
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = "";
            if (id != null && id.length() > 0)
                url = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_DETAILS + id;
            Log.d("SPECIALEVENTACCEPTURL", url);
            StringRequest req_members = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("SPECIALEVENTACCEPT", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject jsonObject = jobcode.getJSONObject("data");
                            String startdate = jsonObject.getString("start_on");
                            String enddate = jsonObject.getString("end_on");
                            int days = jsonObject.getInt("days");
                            JSONArray location = jsonObject.getJSONArray("location");
                            Intent it = new Intent(context, ChallengeAcceptLocationActivity.class);
                            it.putExtra("startdate", startdate);
                            it.putExtra("enddate", enddate);
                            it.putExtra("noofdays", days);
                            it.putExtra("location", location.toString());
                            it.putExtra("minvalue", challengegoal);
                            it.putExtra("activity_name", activityname);
                            it.putExtra("specialactivityid", id);
                            it.putExtra("event", "special");
                            it.putExtra("activity", "ChallengeRequestAdapter");
                            it.putExtra("locationName", locationName);
                            it.putExtra("challenge_id", challenge_id);
                            it.putExtra("challengeAmount", challengeAmount);
                            context.startActivity(it);


                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(context, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    public void rejectChallenge() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.REJECT_CHALLENGE + challenge_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // progressDialog.dismiss();
                            Log.d("RejectChallenge", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                    //  progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");

                                    Intent intent = new Intent(context, ChallengeRequestActivity.class);
                                    context.startActivity(intent);

                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                //  progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //   progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void acceptChallenge() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_CHALLENGE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            Log.d("AcceptChallenge", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    // progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    String challenge_id = jsonObject1.getString("challenge_id");

                                    Intent intent = new Intent(context, ChallengeRequestActivity.class);
                                    context.startActivity(intent);

                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                // progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_id", challenge_id);
                    params.put("location_name", address);
                    params.put("location_lat", lat);
                    params.put("location_long", lng);

                    Log.d("ACCEPT:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.challengeRequestModels.size();
    }

    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        Intent top = new Intent(context, FullImage.class);
        top.putExtra("HOLOPATH", AppUrls.BASE_IMAGE_URL + challengeRequestModels.get(pos).getChallenge_goal());
        //  Log.d("HOLOPATH", AppUrls.BASE_IMAGE_URL +user_completed_goal);
        context.startActivity(top);
    }
}
