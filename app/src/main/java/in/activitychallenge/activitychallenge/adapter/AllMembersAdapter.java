package in.activitychallenge.activitychallenge.adapter;


import android.content.Context;

import android.content.Intent;
import android.graphics.Typeface;

import android.support.v7.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllMembersActivity;

import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForAllMembersList;

import in.activitychallenge.activitychallenge.holder.AllMembersHolder;

import in.activitychallenge.activitychallenge.itemclicklistners.AllMembersItemClickListener;

import in.activitychallenge.activitychallenge.models.AllMembersModel;



public class AllMembersAdapter extends RecyclerView.Adapter<AllMembersHolder> implements Filterable {

    public ArrayList<AllMembersModel> allmemberlistModels, allmembersfilterList;
    AllMembersActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;

    CustomFilterForAllMembersList filter;


    public AllMembersAdapter(ArrayList<AllMembersModel> allmemberlistModels, AllMembersActivity context, int resource) {
        this.allmemberlistModels = allmemberlistModels;
        this.context = context;
        this.resource = resource;
        this.allmembersfilterList = allmemberlistModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
    }

    @Override
    public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllMembersHolder slh = new AllMembersHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllMembersHolder holder, final int position)
    {

        String str = allmemberlistModels.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.all_member_user_name.setText(Html.fromHtml(converted_string));

        holder.all_member_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).getOverall_rank()));
        holder.all_member_percentage_text.setText(Html.fromHtml(allmemberlistModels.get(position).getWinning_per()+" %"));
        holder.all_member_won_text.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_win()));
        holder.all_member_loss_text.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_loss()));


         String  img_profile=allmemberlistModels.get(position).getProfile_pic();

          Log.d("PPPROFILE",img_profile);
         if(img_profile.equals("null") && img_profile.equals(""))
         {
             Picasso.with(context)
                     .load(R.drawable.placeholder_dummy)
                     .placeholder(R.drawable.dummy_user_profile)
                     .resize(60,60)
                     .into(holder.all_member_image);
         }
         else
         {
             Picasso.with(context)
                     .load(allmemberlistModels.get(position).getProfile_pic())
                     .placeholder(R.drawable.dummy_user_profile)
                     .into(holder.all_member_image);
         }


        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage1())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_one);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage2())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_two);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage3())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_three);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage4())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_four);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage5())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_five);


        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                Intent intent=new Intent(context, MemberDetailActivity.class);
                Log.d("IDDD:",allmemberlistModels.get(position).getId());
                intent.putExtra("MEMBER_ID",allmemberlistModels.get(position).getId());
                intent.putExtra("MEMBER_NAME",allmemberlistModels.get(position).getName());
                intent.putExtra("member_user_type",allmemberlistModels.get(position).getUser_type());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.allmemberlistModels.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForAllMembersList(allmembersfilterList, this);
        }

        return filter;
    }
}
