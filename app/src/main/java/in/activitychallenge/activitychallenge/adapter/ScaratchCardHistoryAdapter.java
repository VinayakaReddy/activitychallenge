package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ScratchCardHistory;
import in.activitychallenge.activitychallenge.holder.ScratchcardHistoryHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ScratchcardHistoryItemClickListener;
import in.activitychallenge.activitychallenge.models.ScratchcardHistoryModel;


public class ScaratchCardHistoryAdapter extends RecyclerView.Adapter<ScratchcardHistoryHolder> {
    public ArrayList<ScratchcardHistoryModel> historyModels;
    public ScratchCardHistory context;
     LayoutInflater li;
    int resource;
    Typeface typeface;

    public ScaratchCardHistoryAdapter(ArrayList<ScratchcardHistoryModel> historyModels, ScratchCardHistory context, int resource) {
        this.historyModels = historyModels;
        this.context = context;
        this.resource = resource;

      typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }



    @Override
    public ScratchcardHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ScratchcardHistoryHolder slh = new ScratchcardHistoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(ScratchcardHistoryHolder holder, final int position)
    {

        String total_amount=historyModels.get(position).getAmount();
        holder.amountname.setText( Html.fromHtml("&#36;"+"<b>"+total_amount+"</b>"));
        holder.amountname.setTypeface(typeface);



      /*  Picasso.with(context)
                .load(historyModels.get(position).getImg_path())
                .placeholder(R.drawable.flag)
                .into(holder.flag_image);
*/
        holder.setItemClickListener(new ScratchcardHistoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });



    }

    @Override
    public int getItemCount() {
        return this.historyModels.size();
    }
}
