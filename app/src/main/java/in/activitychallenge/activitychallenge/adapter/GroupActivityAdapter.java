package in.activitychallenge.activitychallenge.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.FiveKmsActivity;
import in.activitychallenge.activitychallenge.activities.FortyfiveMinsActivity;
import in.activitychallenge.activitychallenge.activities.GetLocationActivity;
import in.activitychallenge.activitychallenge.activities.GroupMainActivity;
import in.activitychallenge.activitychallenge.activities.PicturePosesActivity;
import in.activitychallenge.activitychallenge.activities.SixtyMinsActivity;
import in.activitychallenge.activitychallenge.activities.StepsChallengeActivity;
import in.activitychallenge.activitychallenge.activities.ThirtyMinsActivity;
import in.activitychallenge.activitychallenge.filters.GroupActivityFilterList;
import in.activitychallenge.activitychallenge.holder.ActivityHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;


public class GroupActivityAdapter extends RecyclerView.Adapter<ActivityHolder> implements Filterable {

    public ArrayList<ActivityModel> listModels, filterList;
    GroupMainActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    private static final int PLACE_PICKER_REQUEST = 1;
    Sensor stepSensor;
    SensorManager sManager;
    GroupActivityFilterList filter;
String user_id,user_type;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    String location_name = "", location_lat = "", location_lng = "";

    public GroupActivityAdapter(ArrayList<ActivityModel> listModels, GroupMainActivity context, int resource,String user_id,String user_type) {
        this.listModels = listModels;
        this.context = context;
        this.resource = resource;
        this.filterList = listModels;
        this.user_id = user_id;
        this.user_type = user_type;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        gps = new GPSTracker(context);

    }

    @Override
    public ActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ActivityHolder slh = new ActivityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ActivityHolder holder, final int position) {
        gps = new GPSTracker(context);
        geocoder = new Geocoder(context, Locale.getDefault());
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //   Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);

            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                location_name = address;
                location_lat = lat;
                location_lng = lng;
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }
        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -"+listModels.get(position).getActivity_name());
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        if (sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null && sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && gps.canGetLocation()) {
            if (!listModels.get(position).getDaily_challenges().equals("0") || !listModels.get(position).getWeekly_challenges().equals("0")|| !listModels.get(position).getLongrun_challenges().equals("0")) {
              //  Toast.makeText(context, "sensor Working", Toast.LENGTH_SHORT).show();
//                holder.sensor_not_support_image.setVisibility(View.GONE);
//                holder.name.setBackgroundResource(R.color.black);
                holder.card.setBackgroundResource(R.drawable.act_round_bg);
                if(listModels.get(position).getTools_required().contains("GPS")) {
                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {
                            if (listModels.get(position).getMin_value().equals("5000 STEPS")) {
                                // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                if (listModels.get(position).getMin_value().equals("Walking")) {
                                    Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                }
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "5000steps");
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, StepsChallengeActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }
                                            }
                                        }).show();

                            } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "30mins");
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }
                                            }
                                        }).show();

                            } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "45mins");
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }
                                            }
                                        }).show();

                            } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage("<font color='#026c9b'>This activity needs to access your location</font>")
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "60mins");
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, SixtyMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }

                                            }
                                        }).show();

                            } else if (listModels.get(position).getMin_value().equals("5 KM")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage("Html.fromHtml(\"<font color='#026c9b'>This activity needs to access your location</font>\")")
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "5km");
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, FiveKmsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }

                                            }
                                        }).show();

                            } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Hi")
                                        .setMessage(Html.fromHtml("<font color='#026c9b'>This activity needs to access your location</font>"))
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (listModels.get(pos).getTools_required().contains("GPS")) {
                                                    Intent intent = new Intent(context, GetLocationActivity.class);
                                                    intent.putExtra("type", "hollow_pictures");
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, PicturePosesActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);
                                                }

                                            }
                                        }).show();

                            }

                        }
                    });
                }else{

                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {
                           // Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                            if (listModels.get(position).getMin_value().equals("5000 STEPS")) {
                                // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                if (listModels.get(position).getMin_value().equals("Walking")) {
                                    Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                }

                                                    Intent intent = new Intent(context, StepsChallengeActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);


                            } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {

                                                    Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                                    intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                     context.startActivity(intent);
                            } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {

                                                    Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);



                            } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {

                                                    Intent intent = new Intent(context, SixtyMinsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);


                            } else if (listModels.get(position).getMin_value().equals("5 KM")) {

                                                    Intent intent = new Intent(context, FiveKmsActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);


                            } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {

                                                    Intent intent = new Intent(context, PicturePosesActivity.class);
                                                    intent.putExtra("id", listModels.get(pos).getId());
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("user_type", user_type);
                                                    intent.putExtra("daily_challenges", listModels.get(pos).getDaily_challenges());
                                                    intent.putExtra("weekly_challenges", listModels.get(pos).getWeekly_challenges());
                                intent.putExtra("long_challenges", listModels.get(pos).getLongrun_challenges());
                                                    intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                                    intent.putExtra("location_name", location_name);
                                                    intent.putExtra("location_lat", location_lat);
                                                    intent.putExtra("location_lng", location_lng);
                                                    context.startActivity(intent);


                            }

                        }
                    });
                }
            } else {

                holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

                holder.setItemClickListener(new ActivityItemClickListener() {
                    @Override
                    public void onItemClick(View v, final int pos) {
                        new AlertDialog.Builder(context)
                                .setTitle("Sorry...")
                                .setMessage( Html.fromHtml("<font color='#FF7F27'>This activity is offline</font>"))
                                .setCancelable(false)
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i)
                                    {
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton( Html.fromHtml("<font color='#000'>Back</font>"), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }
                });
            }
        } else {

            Toast.makeText(context, "sensors not Working", Toast.LENGTH_SHORT).show();

            holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    new AlertDialog.Builder(context)
                            .setTitle("Unsupportable Device")
                            .setMessage("Your Device not support for this operation")
                            .setCancelable(false)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int i)
                                {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }
            });
        }
        holder.name.setText(listModels.get(position).getActivity_name());

        Picasso.with(context)
                .load(listModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);
        Log.d("xfkldfbng", holder.name.getText().toString());


    }

    @Override
    public int getItemCount() {
        return this.listModels.size();
    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new GroupActivityFilterList(filterList, this);
        }

        return filter;
    }
}
