package in.activitychallenge.activitychallenge.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.SavedCardDetailsActivity;
import in.activitychallenge.activitychallenge.holder.SavedCardDetailsHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.SavedCardDetailsItemClickListener;
import in.activitychallenge.activitychallenge.models.SavedCardDetailsModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SavedCardDetailsAdapter extends RecyclerView.Adapter<SavedCardDetailsHolder> {

    public ArrayList<SavedCardDetailsModel> savedCardDetailsModels;
    SavedCardDetailsActivity context;
    LayoutInflater li;
    int resource;
    UserSessionManager session;
    String user_id, token, device_id,card_id;
    private boolean checkInternet;
    ProgressDialog progressDialog;

    public SavedCardDetailsAdapter(ArrayList<SavedCardDetailsModel> savedCardDetailsModels, SavedCardDetailsActivity context, int resource) {
        this.savedCardDetailsModels = savedCardDetailsModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
    }

    @Override
    public SavedCardDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SavedCardDetailsHolder slh = new SavedCardDetailsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SavedCardDetailsHolder holder, final int position) {

        card_id = savedCardDetailsModels.get(position).getId();

        holder.name_on_card.setText(savedCardDetailsModels.get(position).getCc_name_on_card());
        holder.acc_type.setText(savedCardDetailsModels.get(position).getAcc_type());
        holder.card_number.setText(savedCardDetailsModels.get(position).getAcc_no());
        holder.month_year.setText(savedCardDetailsModels.get(position).getCc_month() + "/" + savedCardDetailsModels.get(position).getCc_year());

        String timedate=parseDateToddMMyyyy(savedCardDetailsModels.get(position).getCreated_on());
        holder.created_time.setText(timedate);

        holder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Are You Sure?");
                alertDialog.setMessage("You Want To Delete This Card?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteCard();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.setItemClickListener(new SavedCardDetailsItemClickListener() {
            @Override
            public void onItemClick(View v, final int pos) {

            }
        });
    }

    private void deleteCard(){
        checkInternet = NetworkChecking.isConnected(context);
        Log.d("DELETEURL", AppUrls.BASE_URL + AppUrls.DELETE_CARD+user_id+"/"+card_id);

        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, AppUrls.BASE_URL + AppUrls.DELETE_CARD+user_id+"/"+card_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("DELETECARDS", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");

                                if (response_code.equals("10100")) {

                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Card Details Deleted Successfully..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context,SavedCardDetailsActivity.class);
                                    context.startActivity(intent);
                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEDCARDHEAD",headers.toString());
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }else {
            progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.savedCardDetailsModels.size();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
