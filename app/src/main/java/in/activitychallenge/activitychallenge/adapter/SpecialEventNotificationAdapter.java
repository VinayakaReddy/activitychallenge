package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.SpecialEvent;
import in.activitychallenge.activitychallenge.activities.SpecialEventNotifications;
import in.activitychallenge.activitychallenge.holder.SpecialEventHolder;
import in.activitychallenge.activitychallenge.holder.SpecialEventNotificationHolder;
import in.activitychallenge.activitychallenge.models.SpecialEventModel;
import in.activitychallenge.activitychallenge.models.SpecialEventNotificationModel;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SpecialEventNotificationAdapter extends RecyclerView.Adapter<SpecialEventNotificationHolder> {
    public ArrayList<SpecialEventNotificationModel> specialEventNotificationModels;
    SpecialEventNotifications context;
    LayoutInflater li;
    int resource;
    public SpecialEventNotificationAdapter(ArrayList<SpecialEventNotificationModel> specialEventNotificationModels, SpecialEventNotifications context, int resource) {
        this.specialEventNotificationModels = specialEventNotificationModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public SpecialEventNotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SpecialEventNotificationHolder specialHolder = new SpecialEventNotificationHolder(layout);
        return specialHolder;
    }

    @Override
    public void onBindViewHolder(SpecialEventNotificationHolder holder, int position) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        holder.notification_txt.setText(specialEventNotificationModels.get(position).getNotificationtext());
        holder.notification_txt.setTypeface(typeface);
        String createddate=specialEventNotificationModels.get(position).getCreateddate();

        String[] dateStr =createddate.split("T");
        holder.created_date_txt.setText(parseDateToddMMyyyy(dateStr[0]));
    }

    @Override
    public int getItemCount() {
        return specialEventNotificationModels.size();
    }

    public String parseDateToddMMyyyy(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
