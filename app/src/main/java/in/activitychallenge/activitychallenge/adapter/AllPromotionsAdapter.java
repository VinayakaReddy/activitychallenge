package in.activitychallenge.activitychallenge.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllPromotionsActivity;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.holder.AllPromotionsHolder;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllPromotionsAdapter extends RecyclerView.Adapter<AllPromotionsHolder> {
    AllPromotionsActivity context;
    public ArrayList<AllPromotionsModel> promoModels;
    LayoutInflater li;
    int resource;
    UserSessionManager session;
    String user_id, token, device_id, user_type = "";

    public AllPromotionsAdapter(ArrayList<AllPromotionsModel> promoModels, AllPromotionsActivity context, int resource) {
        this.context = context;
        this.promoModels = promoModels;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
    }


    @Override
    public AllPromotionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllPromotionsHolder slh = new AllPromotionsHolder(layout);
        return slh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(AllPromotionsHolder holder, final int position) {

        holder.promotion_name.setText(String.valueOf(promoModels.get(position).getEntity_name()));
        Picasso.with(context)
                .load(promoModels.get(position).getBanner_path())
                .placeholder(R.drawable.no_image_found)
                .into(holder.promotion_image);
        holder.promotion_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(promoModels.get(position).getEntity_id().equals(user_id))
                {

                }
                else
                    {
                    if (promoModels.get(position).getEntity_type().equals("USER") || promoModels.get(position).getEntity_type().equals("SPONSOR") ) {
                        Intent intent = new Intent(context, MemberDetailActivity.class);
                        intent.putExtra("MEMBER_ID", promoModels.get(position).getEntity_id());
                        intent.putExtra("MEMBER_NAME", promoModels.get(position).getEntity_name());
                        intent.putExtra("member_user_type", promoModels.get(position).getEntity_type());
                        context.startActivity(intent);
                    } else if(promoModels.get(position).getEntity_type().equals("GROUP"))
                    {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoModels.get(position).getEntity_id());
                        intent.putExtra("grp_name", promoModels.get(position).getEntity_name());
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoModels.get(position).getEntity_type());
                        intent.putExtra("grp_admin_id", promoModels.get(position).getAdmin_id());

                        context.startActivity(intent);
                    }
                    else
                    {

                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return promoModels.size();
    }
}
