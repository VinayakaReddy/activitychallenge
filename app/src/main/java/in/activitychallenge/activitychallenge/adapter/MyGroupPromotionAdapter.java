package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.fragments.MyGroupPromotionFragment;
import in.activitychallenge.activitychallenge.holder.GroupPromotionHolder;
import in.activitychallenge.activitychallenge.models.GroupPromotionModel;

/**
 * Created by admin on 12/21/2017.
 */

public class MyGroupPromotionAdapter extends RecyclerView.Adapter<GroupPromotionHolder> {
    MyGroupPromotionFragment context;
    public ArrayList<GroupPromotionModel> promoModels;
    LayoutInflater li;
    int resource;
    String datetime;

    public MyGroupPromotionAdapter(ArrayList<GroupPromotionModel> promoModels, MyGroupPromotionFragment context, int resource) {
        this.context = context;
        this.promoModels = promoModels;
        this.resource =resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public GroupPromotionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        GroupPromotionHolder slh = new GroupPromotionHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(GroupPromotionHolder holder, final int position) {
            holder.amount.setText(String.valueOf(promoModels.get(position).getAmount()+" "+"\nUSD"));


           datetime =parseDateToddMMyyyy(promoModels.get(position).getCreated_on());
           holder.cre_date.setText(datetime);


            holder.name.setText(promoModels.get(position).getGroup_name());

            holder.status.setText(promoModels.get(position).getStatus());
        holder.status.setText(promoModels.get(position).getStatus());
        if (holder.status.getText().equals("APPROVED"))
        {
            holder.pay_txt.setVisibility(View.VISIBLE);
            holder.status.setTextColor(Color.parseColor("#28df5a"));
        }
        else if (holder.status.getText().equals("PAID"))
        {
            holder.status.setTextColor(Color.parseColor("#28df5a"));
        }else {
            holder.status.setTextColor(Color.parseColor("#b3270c"));
        }

        holder.promoCode.setText(promoModels.get(position).getPromotion_code());
        holder.pay_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getActivity(), ChallengePaymentActivity.class);
                intent.putExtra("promotionAmount",String.valueOf(promoModels.get(position).getAmount()));
                intent.putExtra("promotion_id",promoModels.get(position).getPromotion_id());
                intent.putExtra("activity","MyPromotionFragment");
                Log.d("SendingData", String.valueOf(promoModels.get(position).getAmount()));
                context.startActivity(intent);
            }
        });
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return promoModels.size();
    }
}
