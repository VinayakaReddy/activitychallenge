package in.activitychallenge.activitychallenge.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllCompletedChallengesActivity;
import in.activitychallenge.activitychallenge.activities.CongratulationsActivity;
import in.activitychallenge.activitychallenge.activities.UserWinWithAdminScratchActivity;
import in.activitychallenge.activitychallenge.holder.AllChallangesMoreHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.AllChalngMoreItemClickListener;
import in.activitychallenge.activitychallenge.models.AllChallengesStatusModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class AllChallangCompletedAdapter extends RecyclerView.Adapter<AllChallangesMoreHolder>{

    public ArrayList<AllChallengesStatusModel> allChalngMorelistModels;
    AllCompletedChallengesActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
     ProgressDialog progressDialog;
    UserSessionManager session;
    String user_name,from_where,challegne_type;



    public AllChallangCompletedAdapter(ArrayList<AllChallengesStatusModel> allChalngMorelistModels, AllCompletedChallengesActivity context, int resource, String from_where) {
        this.allChalngMorelistModels = allChalngMorelistModels;
        this.context = context;
        this.resource = resource;
        this.from_where = from_where;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.hermes));

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);


    }

    @Override
    public AllChallangesMoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllChallangesMoreHolder slh = new AllChallangesMoreHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final AllChallangesMoreHolder holder, final int position)
    {
        Log.d("FROMWHERERER",from_where);
        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = allChalngMorelistModels.get(position).getOpponent_name();
        final String converted_oppname_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem=allChalngMorelistModels.get(position).getUser_name();;
        final String converted_user_name = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        final String winning_status=allChalngMorelistModels.get(position).getWinning_status();
        Log.d("CDCDUEYEYYE",allChalngMorelistModels.get(position).getWinning_reward_type());
        final String winning_reward_type=allChalngMorelistModels.get(position).getWinning_reward_type();
        final String winning_reward_value=allChalngMorelistModels.get(position).getWinning_reward_value();
        final String is_scratched=allChalngMorelistModels.get(position).getIs_scratched();


        holder.all_challenger_names.setText(Html.fromHtml(converted_user_name));
        holder.oponent_names.setText(Html.fromHtml(converted_oppname_string));
        holder.vsText.setTypeface(typeface3);

        holder.all_cash_prize_amount.setText(("$"+allChalngMorelistModels.get(position).getAmount()));

        String timedate=parseDateToddMMyyyy(allChalngMorelistModels.get(position).getStart_on());
        holder.all_dateandtime.setText(Html.fromHtml(timedate));

        challegne_type=allChalngMorelistModels.get(position).getChallenge_type();
        if(challegne_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        }
        else if(challegne_type.equals("LONGRUN"))
        {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
        }
        else
        {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }


        final String status=allChalngMorelistModels.get(position).getStatus();
        final String ws_winner=allChalngMorelistModels.get(position).getWinner();
        Log.d("ws_winner",""+ws_winner);
        if(status.equals("COMPLETED"))
        {
                holder.ll_completed.setVisibility(View.VISIBLE);
            if(ws_winner.equals("challenger"))
            {

                holder.winner_name.setText("Won : "+ converted_user_name);
                if(winning_status.equals("loss"))
                {

                    holder.won_cash.setText(("Lost : "+"$"+allChalngMorelistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {

                      Log.d("winning_reward_type",winning_reward_type);
                    if(winning_reward_type.equals("SCRATCH_CARD") )
                    {
                        Log.d("312","9090");
                        if(is_scratched.equals("0"))
                        {
                            Log.d("TUU","201");
                            holder.won_cash.setText(("Won,Scratch Card"));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }
                        else
                        {
                            Log.d("PIR","326");
                            holder.won_cash.setText(("Won : "+"$"+allChalngMorelistModels.get(position).getWinning_amount()));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }

                    }
                    else
                    {
                        Log.d("cccc","tg");
                        holder.won_cash.setText(("Won : "+"$"+allChalngMorelistModels.get(position).getWinning_amount()));
                        holder.won_cash.setTextColor(Color.parseColor("#239843"));
                    }

                }

            }
            else if(ws_winner.equals("opponent"))
            {
                holder.winner_name.setText("Won : "+converted_oppname_string);

                if(winning_status.equals("loss"))
                {
                    holder.won_cash.setText(("Lost : "+"$"+allChalngMorelistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {
                    holder.won_cash.setText(("Won : "+"$"+allChalngMorelistModels.get(position).getWinning_amount()));
                    holder.won_cash.setTextColor(Color.parseColor("#239843"));
                }


            }
            else if(ws_winner.equals("no"))
            {

                if(allChalngMorelistModels.get(position).getOpponent_name().equals("ADMIN"))
                {
                    holder.winner_name.setText("You Lost the challenge");
                }
                else
                {
                    holder.winner_name.setText("Both Lost the challenge");
                }
                //holder.winner_name.setText("Both Loss the challenge");
                holder.won_cash.setText(("Lost : "+"$"+allChalngMorelistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));

            }
            else
            {}
           /* if(winning_status.equals("loss"))
            {

                holder.won_cash.setText(("Loss : "+"$"+challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
            }
            else
            {

                holder.won_cash.setText(("Won : "+"$"+challngIndRunlistModels.get(position).getWinning_amount()));
                holder.won_cash.setTextColor(Color.parseColor("#239843"));
            }
*/

        }
        else if (status.equals("TIE") )
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("You both have won scratch card");
            holder.won_cash.setVisibility(View.GONE);

        }
        else if (status.equals("AUTO_CANCELLATION"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by ADMIN");
        }
        else if ( status.equals("REJECTED"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Rejected by : "+converted_oppname_string);
        }
        else if ( status.equals("MANUAL_CANCELLATION")||status.equals("CANCELLED"))//CANCELLATION
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by : "+converted_user_name);
        }

        holder.all_status.setText(Html.fromHtml(status));

        holder.all_status.setBackgroundResource(R.drawable.gradient_toolbar_color);
        holder.all_status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context)
                .load(allChalngMorelistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.all_activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new AllChalngMoreItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                if(status.equals("REJECTED") || status.equals("MANUAL_CANCELLATION")|| status.equals("CANCELLED"))
                {
                   //Nothing
                }
                else
                {
                    if(winning_status.equals("loss"))
                    {

                        Intent intent = new Intent(context, CongratulationsActivity.class);
                        intent.putExtra("challenge_id",allChalngMorelistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_oppname_string);
                        intent.putExtra("win_status","LOSS");
                        intent.putExtra("amount",allChalngMorelistModels.get(position).getAmount());
                        intent.putExtra("is_group_admin",allChalngMorelistModels.get(position).getIs_group_admin());
                        context.startActivity(intent);
                    }
                    else if(winning_status.equals("win"))
                    {
                        if(winning_reward_type.equals("SCRATCH_CARD")  &&  is_scratched.equals("0"))
                        {
                            if(allChalngMorelistModels.get(position).getIs_group_admin().equals("0"))
                            {
                                Toast.makeText(context,"Only Admin can scratch the Card.!!",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Intent intent = new Intent(context, UserWinWithAdminScratchActivity.class);
                                intent.putExtra("challenge_id",allChalngMorelistModels.get(position).getChallenge_id());
                                intent.putExtra("is_group_admin", allChalngMorelistModels.get(position).getIs_group_admin());
                                context.startActivity(intent);
                                // context.startActivityForResult(intent, 1);
                            }

                        }
                        else {
                            Intent intent = new Intent(context, CongratulationsActivity.class);
                            intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                            intent.putExtra("winner_name", converted_user_name);
                            intent.putExtra("win_status", "WON");
                            intent.putExtra("amount", allChalngMorelistModels.get(position).getWinning_amount());
                            intent.putExtra("is_group_admin", allChalngMorelistModels.get(position).getIs_group_admin());
                            context.startActivity(intent);
                        }
                    }
                    else
                    {
                        Intent intent = new Intent(context, CongratulationsActivity.class);
                        intent.putExtra("challenge_id",allChalngMorelistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_user_name);
                        intent.putExtra("win_status","TIE");
                        intent.putExtra("amount",allChalngMorelistModels.get(position).getWinning_amount());
                        intent.putExtra("is_group_admin",allChalngMorelistModels.get(position).getIs_group_admin());
                        intent.putExtra("FROM_WHERE",from_where);
                        context.startActivity(intent);
                    }
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.allChalngMorelistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
