package in.activitychallenge.activitychallenge.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.CongratulationsActivity;
import in.activitychallenge.activitychallenge.activities.UserWinWithAdminScratchActivity;
import in.activitychallenge.activitychallenge.fragments.MyChallengesGroupFragment;
import in.activitychallenge.activitychallenge.holder.ChallengeGroupDetailHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeGroupDetailItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeGroupDetailModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class MyChallengeGroupCompletAdapter extends RecyclerView.Adapter<ChallengeGroupDetailHolder>{

    public ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels;
    MyChallengesGroupFragment context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
    UserSessionManager session;
    String user_name,challenge_type;




    public MyChallengeGroupCompletAdapter(ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels, MyChallengesGroupFragment context, int resource) {
        this.challnggroupDetaillistModels = challnggroupDetaillistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.hermes));
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);


    }

    @Override
    public ChallengeGroupDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeGroupDetailHolder slh = new ChallengeGroupDetailHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeGroupDetailHolder holder, final int position)
    {
        String PAUSE_ACCESS=challnggroupDetaillistModels.get(position).getPause_access();
        if(PAUSE_ACCESS.equals("0"))
        {
            holder.pause_challenge.setVisibility(View.GONE);
        }

        String str_opponentname = challnggroupDetaillistModels.get(position).getOpponent_name();
        final String converted_oppname_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem=challnggroupDetaillistModels.get(position).getUser_name();;
        final String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        final String winning_status=challnggroupDetaillistModels.get(position).getWinning_status();

        final String winning_reward_type=challnggroupDetaillistModels.get(position).getWinning_reward_type();
        final String winning_reward_value=challnggroupDetaillistModels.get(position).getWinning_reward_value();
        final String is_scratched=challnggroupDetaillistModels.get(position).getIs_scratched();

        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.oponent_names.setText(Html.fromHtml(converted_oppname_string));
        holder.vsText.setTypeface(typeface3);

        holder.cash_prize_amount.setText(("$"+challnggroupDetaillistModels.get(position).getAmount()));

        String timedate=parseDateToddMMyyyy(challnggroupDetaillistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));

        challenge_type=challnggroupDetaillistModels.get(position).getChallenge_type();
        if(challenge_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.daily_bg));
        }
        else if(challenge_type.equals("LONGRUN"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.longrun_bg));
        }
        else
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.week_bg));
        }

        final String status=challnggroupDetaillistModels.get(position).getStatus();
        final String ws_winner=challnggroupDetaillistModels.get(position).getWinnner();
        Log.d("ws_winner",""+ws_winner);

        if(status.equals("COMPLETED"))
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            if(ws_winner.equals("challenger"))
            {
                holder.winner_name.setText("Won : "+ converted_user_naem);
                if(winning_status.equals("loss"))
                {
                    holder.won_cash.setText(("Lost : "+"$"+challnggroupDetaillistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {
                    if(winning_reward_type.equals("SCRATCH_CARD") )
                    {
                        if(is_scratched.equals("0"))
                        {
                            holder.won_cash.setText(("Won,Scratch Card"));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }
                        else
                        {
                            holder.won_cash.setText(("Won : "+"$"+challnggroupDetaillistModels.get(position).getWinning_amount()));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }

                    }
                    else
                    {
                        holder.won_cash.setText(("Won : "+"$"+challnggroupDetaillistModels.get(position).getWinning_amount()));
                        holder.won_cash.setTextColor(Color.parseColor("#239843"));
                    }

                }

            }
            else if(ws_winner.equals("opponent"))
            {
                holder.winner_name.setText("Won : "+converted_oppname_string);

                if(winning_status.equals("loss"))
                {
                    holder.won_cash.setText(("Lost : "+"$"+challnggroupDetaillistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                }
                else
                {
                    holder.won_cash.setText(("Won : "+"$"+challnggroupDetaillistModels.get(position).getWinning_amount()));
                    holder.won_cash.setTextColor(Color.parseColor("#239843"));
                }


            }
            else if(ws_winner.equals("no"))
            {
                if(challnggroupDetaillistModels.get(position).getOpponent_name().equals("ADMIN"))
                {
                    holder.winner_name.setText("You Lost the challenge");
                }
                else
                {
                    holder.winner_name.setText("Both Lost the challenge");
                }

                holder.won_cash.setText(("Lost : "+"$"+challnggroupDetaillistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));

            }

            else
            {}
           /* if(winning_status.equals("loss"))
            {
                holder.winner_name.setText("Winner : "+converted_oppname_string);
                holder.won_cash.setText(("Loss : "+"$"+challnggroupDetaillistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
            }
            else
            {
                holder.winner_name.setText("Winner : "+converted_user_naem);
                holder.won_cash.setText(("Won : "+"$"+challnggroupDetaillistModels.get(position).getWinning_amount()));
                holder.won_cash.setTextColor(Color.parseColor("#239843"));
            }*/

        }
        else if (status.equals("TIE") )
        {
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.won_cash.setVisibility(View.GONE);
            holder.winner_name.setText("You both have won scratch card");
        }
        else if (status.equals("AUTO_CANCELLATION"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by ADMIN");
        }
        else if ( status.equals("REJECTED"))
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Rejected by : "+converted_oppname_string);
        }
        else if ( status.equals("MANUAL_CANCELLATION")||status.equals("CANCELLED"))//CANCELLATION
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by : "+converted_user_naem);
        }



        holder.status.setText(Html.fromHtml(status));
        holder.status.setBackgroundResource(R.drawable.gradient_toolbar_color);
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context.getActivity())
                .load(challnggroupDetaillistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new ChallengeGroupDetailItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                if(status.equals("REJECTED") || status.equals("MANUAL_CANCELLATION")|| status.equals("CANCELLED"))
                {
                    //Nothing
                }
                else
                {
                    if(winning_status.equals("loss"))
                    {

                        Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                        intent.putExtra("challenge_id",challnggroupDetaillistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_oppname_string);
                        intent.putExtra("win_status","LOSS");
                        intent.putExtra("type","GROUP");
                        intent.putExtra("amount",challnggroupDetaillistModels.get(position).getAmount());
                        intent.putExtra("is_group_admin",challnggroupDetaillistModels.get(position).getIs_group_admin());
                        context.startActivity(intent);
                    }
                    else if(winning_status.equals("win"))
                    {

                        if(winning_reward_type.equals("SCRATCH_CARD")  &&  is_scratched.equals("0"))
                        {
                            if(challnggroupDetaillistModels.get(position).getIs_group_admin().equals("0"))
                            {
                                Toast.makeText(context.getActivity(),"Only Admin can scratch the Card.!!",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Intent intent = new Intent(context.getActivity(), UserWinWithAdminScratchActivity.class);
                                intent.putExtra("challenge_id",challnggroupDetaillistModels.get(position).getChallenge_id());
                                intent.putExtra("is_group_admin", challnggroupDetaillistModels.get(position).getIs_group_admin());
                                context.startActivity(intent);
                                // context.startActivityForResult(intent, 1);
                            }

                        }
                        else
                            {


                                    Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                                    intent.putExtra("challenge_id", challnggroupDetaillistModels.get(position).getChallenge_id());
                                    intent.putExtra("winner_name", converted_user_naem);
                                    intent.putExtra("win_status", "WON");
                                    intent.putExtra("type", "GROUP");
                                    intent.putExtra("amount", challnggroupDetaillistModels.get(position).getWinning_amount());
                                    intent.putExtra("is_group_admin", challnggroupDetaillistModels.get(position).getIs_group_admin());
                                    context.startActivity(intent);

                        }
                    }
                    else
                    {
                        Intent intent = new Intent(context.getActivity(), CongratulationsActivity.class);
                        intent.putExtra("challenge_id",challnggroupDetaillistModels.get(position).getChallenge_id());
                        intent.putExtra("winner_name",converted_user_naem);
                        intent.putExtra("win_status","TIE");
                        intent.putExtra("type","GROUP");
                        intent.putExtra("amount",challnggroupDetaillistModels.get(position).getWinning_amount());
                        intent.putExtra("is_group_admin",challnggroupDetaillistModels.get(position).getIs_group_admin());
                        context.startActivity(intent);
                    }
                }



            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.challnggroupDetaillistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
