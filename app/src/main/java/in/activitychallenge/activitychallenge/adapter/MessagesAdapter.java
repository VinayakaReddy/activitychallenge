package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.MessageDetailActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForGroupChat;
import in.activitychallenge.activitychallenge.filters.CustomFilterForUserChat;
import in.activitychallenge.activitychallenge.fragments.UserChatFragment;
import in.activitychallenge.activitychallenge.holder.MessagesHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MessagesItemClickListener;
import in.activitychallenge.activitychallenge.models.MessagesModel;


public class MessagesAdapter extends RecyclerView.Adapter<MessagesHolder> implements Filterable {
    public ArrayList<MessagesModel> msgModels;
    public UserChatFragment context;
    String isRead;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    CustomFilterForUserChat filter;
    String from_chat_type;

    public MessagesAdapter(ArrayList<MessagesModel> msgModel, UserChatFragment ctx, int layout,String chat_type) {
        msgModels = msgModel;
        context = ctx;
        resource = layout;

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        from_chat_type=chat_type;
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }


    @Override
    public MessagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MessagesHolder slh = new MessagesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(MessagesHolder holder, final int position) {


        String str = msgModels.get(position).getFrom_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.message_name_text.setText(Html.fromHtml(converted_string));
        holder.message_name_text.setTypeface(typeface);

        String str_desc = msgModels.get(position).getMsg();
        String converted_desc_string = str_desc.substring(0, 1).toUpperCase() + str_desc.substring(1);
        holder.mesage_desc_text.setText(Html.fromHtml(converted_desc_string));
        holder.mesage_desc_text.setTypeface(typeface);

        holder.message_sent_time.setText(Html.fromHtml(msgModels.get(position).getSent_on_txt()));
        holder.message_sent_time.setTypeface(typeface);

        if (msgModels.get(position).getIs_read().equals("0") || msgModels.get(position).getIs_read().equals("")) {

            holder.parent_layout.setBackgroundColor(Color.parseColor("#F1F1F1"));
        } else {
            holder.parent_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        Picasso.with(context.getActivity())
                .load(msgModels.get(position).getProfile_pic())
                .placeholder(R.drawable.dummy_user_profile)
                .into(holder.profile_pic);


        holder.setItemClickListener(new MessagesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if(msgModels.size()>0 && msgModels!=null) {
                    Log.d("IDIDIIDID", msgModels.get(pos).getFrom_id());
                    Intent inten = new Intent(context.getActivity(), MessageDetailActivity.class);
                    inten.putExtra("FROM_ID", msgModels.get(pos).getFrom_id());
                    inten.putExtra("FROM_NAME", msgModels.get(pos).getFrom_name());
                    inten.putExtra("FROM_TYPE", msgModels.get(pos).getFrom_user_type());
                    inten.putExtra("FROM_PROFILEPIC", msgModels.get(pos).getProfile_pic());
                    inten.putExtra("FROM_TYPE", from_chat_type);

                    context.startActivity(inten);
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.msgModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForUserChat(msgModels, this);
        }

        return filter;
    }
}
