package in.activitychallenge.activitychallenge.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeToFriendActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForChallengeToFriend;
import in.activitychallenge.activitychallenge.holder.ChallengeToFriendHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.AllMembersItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeToFriendModel;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;


public class ChallengeToFriendAdapter extends RecyclerView.Adapter<ChallengeToFriendHolder> implements Filterable {

    public ArrayList<ChallengeToFriendModel> allmemberlistModels, allmembersfilterList;
    ChallengeToFriendActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    CustomFilterForChallengeToFriend filter;
    String user_id,amount,activity_id,challenge_id,challenge_goal,evaluation_factor,evaluation_factor_unit,type,span,date,location_name,location_lat,location_lng,main_user_type;

    public ChallengeToFriendAdapter(ArrayList<ChallengeToFriendModel> allmemberlistModels, ChallengeToFriendActivity context, int resource,
                                    String user_id,String amount,String activity_id,String challenge_id,String challenge_goal,
                                    String evaluation_factor,String evaluation_factor_unit,String type,String span,String date,
                                    String location_name,String location_lat,String location_lng,String main_user_type) {
        this.allmemberlistModels = allmemberlistModels;
        this.context = context;
        this.resource = resource;
        this.allmembersfilterList = allmemberlistModels;
        this.user_id = user_id;
        this.amount = amount;
        this.activity_id = activity_id;
        this.challenge_id = challenge_id;
        this.challenge_goal = challenge_goal;
        this.evaluation_factor = evaluation_factor;
        this.evaluation_factor_unit = evaluation_factor_unit;
        this.type = type;
        this.span = span;
        this.date = date;
        this.location_name = location_name;
        this.location_lat = location_lat;
        this.location_lng = location_lng;
        this.main_user_type = main_user_type;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));


    }

    @Override
    public ChallengeToFriendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeToFriendHolder slh = new ChallengeToFriendHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ChallengeToFriendHolder holder, final int position)
    {

        String str = allmemberlistModels.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.all_member_user_name.setText(Html.fromHtml(converted_string));

        holder.all_member_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).getOverall_rank()));
        holder.all_member_percentage_text.setText(Html.fromHtml(allmemberlistModels.get(position).getWinning_per()+" %"));
        holder.all_member_won_text.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_win()));
        holder.all_member_loss_text.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_loss()));
        if (!allmemberlistModels.get(position).getAlready_applied().equals("0")){
            holder.sendReq_butt.setVisibility(View.GONE);
        }else {
            holder.sendReq_butt.setVisibility(View.VISIBLE);
        }

         String  img_profile=allmemberlistModels.get(position).getProfile_pic();

         if(img_profile.equals("null"))
         {
             Picasso.with(context)
                     .load(R.drawable.img_circle_placeholder)
                     .placeholder(R.drawable.dummy_user_profile)
                     .into(holder.all_member_image);
         }
         else
         {
             Picasso.with(context)
                     .load(allmemberlistModels.get(position).getProfile_pic())
                     .placeholder(R.drawable.dummy_user_profile)
                     .into(holder.all_member_image);
         }


        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage1())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_one);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage2())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_two);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage3())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_three);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage4())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_four);

        Picasso.with(context)
                .load(allmemberlistModels.get(position).getImage5())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_five);


        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

              /*  Intent intent=new Intent(context, MemberDetailActivity.class);
                Log.d("IDDD:",allmemberlistModels.get(position).getId());
                intent.putExtra("MEMBER_ID",allmemberlistModels.get(position).getId());
                intent.putExtra("MEMBER_NAME",allmemberlistModels.get(position).getName());
                context.startActivity(intent);*/
            }
        });
        holder.sendReq_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                Intent intent =new Intent(context, ChallengePaymentActivity.class);
                intent.putExtra("user_id",user_id);
                intent.putExtra("user_type",main_user_type);
                intent.putExtra("activity","challengeto");
                intent.putExtra("paymentAmount",amount);
                intent.putExtra("activity_id",activity_id);
                intent.putExtra("challenge_id",challenge_id);
                intent.putExtra("challenge_goal",challenge_goal);
                intent.putExtra("evaluation_factor",evaluation_factor);
                intent.putExtra("evaluation_factor_unit",evaluation_factor_unit);
                intent.putExtra("type",type);
                intent.putExtra("span",span);
                intent.putExtra("date",date);
                intent.putExtra("location_name",location_name);
                intent.putExtra("location_lat",location_lat);
                intent.putExtra("location_lng",location_lng);
                intent.putExtra("opponent_id",allmemberlistModels.get(position).getId());
                intent.putExtra("opponent_type","USER");
                context.startActivity(intent);
                } else {
                    Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.allmemberlistModels.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForChallengeToFriend(allmembersfilterList, this);
        }

        return filter;
    }
}
