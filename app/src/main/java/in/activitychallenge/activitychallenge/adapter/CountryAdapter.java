package in.activitychallenge.activitychallenge.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.activities.CountryListActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForCountryList;
import in.activitychallenge.activitychallenge.holder.CountryHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.CountryItemClickListener;
import in.activitychallenge.activitychallenge.models.CountriesModel;
import in.activitychallenge.activitychallenge.R;


public class CountryAdapter extends RecyclerView.Adapter<CountryHolder>implements Filterable {
    public ArrayList<CountriesModel> countryModels,filterList;
    public CountryListActivity context;
    CustomFilterForCountryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public CountryAdapter(ArrayList<CountriesModel> countryModels, CountryListActivity context, int resource) {
        this.countryModels = countryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = countryModels;
       // typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForCountryList(filterList,this);
        }

        return filter;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CountryHolder slh = new CountryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, final int position)
    {

          final String country_code=countryModels.get(position).getCode();
        holder.country_name.setText(countryModels.get(position).getName()+"  ( "+country_code+" )");



        Picasso.with(context)
                .load(countryModels.get(position).getImg_path())
                .placeholder(R.drawable.no_image_found)
                .into(holder.flag_image);


        holder.setItemClickListener(new CountryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",country_code);
                returnIntent.putExtra("flag",countryModels.get(position).getImg_path());
                context.setResult(Activity.RESULT_OK,returnIntent);
                context.finish();
              //  context.setCountryName(countryModels.get(pos).getName(),countryModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.countryModels.size();
    }
}
