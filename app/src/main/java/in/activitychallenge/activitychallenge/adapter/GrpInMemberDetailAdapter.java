package in.activitychallenge.activitychallenge.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.holder.GrpInMemberDetailHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.GrpInMemberDetailItemClickListener;
import in.activitychallenge.activitychallenge.models.GrpInMemberDetailModel;


public class GrpInMemberDetailAdapter extends RecyclerView.Adapter<GrpInMemberDetailHolder> {

    public ArrayList<GrpInMemberDetailModel> grp_in_memberDetaillistModels;
    MemberDetailActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    ProgressDialog progressDialog;




    public GrpInMemberDetailAdapter(ArrayList<GrpInMemberDetailModel> grp_in_memberDetaillistModels, MemberDetailActivity context, int resource) {
        this.grp_in_memberDetaillistModels = grp_in_memberDetaillistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public GrpInMemberDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        GrpInMemberDetailHolder slh = new GrpInMemberDetailHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GrpInMemberDetailHolder holder, final int position)
    {
        String str = grp_in_memberDetaillistModels.get(position).getGroup_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.grpname_in_memberdetail.setText(Html.fromHtml(converted_string));
        holder.grpname_in_memberdetail.setTypeface(typeface);

        holder.grp_membcnt_in_memberdetail.setText(Html.fromHtml("Members  "+grp_in_memberDetaillistModels.get(position).getGroup_members()));
        holder.grp_rank.setText(Html.fromHtml(grp_in_memberDetaillistModels.get(position).getGroup_rank()));
        holder.grpin_member_won_cnt.setText(Html.fromHtml(grp_in_memberDetaillistModels.get(position).getWin_challenges()));
        holder.grpin_member_lost_cnt.setText(Html.fromHtml(grp_in_memberDetaillistModels.get(position).getLost_challenges()));
        holder.grp_inmember_percent.setText(Html.fromHtml(grp_in_memberDetaillistModels.get(position).getWinning_percentage()+"%"));

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getProfile_pic())
                .placeholder(R.drawable.dummy_group_profile)
                .into(holder.grp_in_mem_imge);

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getImgv1())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.grpin_memdetail_img_1);

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getImgv2())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.grpin_memdetail_img_2);

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getImgv3())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.grpin_memdetail_img_3);

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getImgv4())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.grpin_memdetail_img_4);

        Picasso.with(context)
                .load(grp_in_memberDetaillistModels.get(position).getImgv5())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.grpin_memdetail_img_5);


        holder.setItemClickListener(new GrpInMemberDetailItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent ii = new Intent(context, GroupDetailActivity.class);
                ii.putExtra("grp_id" , grp_in_memberDetaillistModels.get(pos).getGroup_id());
                ii.putExtra("grp_name" , grp_in_memberDetaillistModels.get(pos).getGroup_name());
                ii.putExtra("grp_admin_id" , grp_in_memberDetaillistModels.get(pos).getAdmin_id());
                ii.putExtra("GROUP_CONVERSATION_TYPE" , grp_in_memberDetaillistModels.get(pos).getConversation_type());
                context.startActivity(ii);
            }
        });



    }

    @Override
    public int getItemCount() {
        return this.grp_in_memberDetaillistModels.size();
    }

    static public String firstLetterCaps(String data)
    {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }


}
