package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;


import java.util.ArrayList;
import java.util.List;

import in.activitychallenge.activitychallenge.BannerLayout;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.models.AllPromotionsModel;

/**
 * Created by test on 2017/11/22.
 */


public class WebBannerAdapter extends RecyclerView.Adapter<WebBannerAdapter.MzViewHolder> {

    private Context context;
    //private List<String> urlList;
    private BannerLayout.OnBannerItemClickListener onBannerItemClickListener;
    ArrayList<AllPromotionsModel> promoList = new ArrayList<AllPromotionsModel>();
    public WebBannerAdapter(Context context, ArrayList<AllPromotionsModel> promoList) {
        this.context = context;
        this.promoList = promoList;
    }

    public void setOnBannerItemClickListener(BannerLayout.OnBannerItemClickListener onBannerItemClickListener) {
        this.onBannerItemClickListener = onBannerItemClickListener;
    }

    @Override
    public WebBannerAdapter.MzViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MzViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(WebBannerAdapter.MzViewHolder holder, final int position) {
        if (promoList == null || promoList.isEmpty())
            return;
        final int P = position % promoList.size();
        String url = promoList.get(position).getBanner_path();
       // ImageView img = (ImageView) holder.imageView;
        Glide.with(context).load(url).into(holder.imageView);
        holder.name.setText(promoList.get(position).getEntity_name());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBannerItemClickListener != null) {
                    onBannerItemClickListener.onItemClick(P);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (promoList != null) {
           return promoList.size();
        }
       return 0;
    }


    class MzViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        MzViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }

}
