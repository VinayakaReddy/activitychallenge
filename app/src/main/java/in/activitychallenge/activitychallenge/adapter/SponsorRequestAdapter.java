package in.activitychallenge.activitychallenge.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.SponsorRequestActivity;
import in.activitychallenge.activitychallenge.holder.SponsorRequestHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.SponsorRequestItemClickListener;
import in.activitychallenge.activitychallenge.models.SponsorRequestModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SponsorRequestAdapter extends RecyclerView.Adapter<SponsorRequestHolder> {

    public ArrayList<SponsorRequestModel> sponsorRequestModels;
    SponsorRequestActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;

    /*Sponsor Accept and Reject*/
    String sponsor_id, token = "", device_id = "", user_id = "", transaction_id, amount, sponsor_request_id,user_type,sponsor_type;

    public SponsorRequestAdapter(ArrayList<SponsorRequestModel> sponsorRequestModels, SponsorRequestActivity context, int resource) {
        this.sponsorRequestModels = sponsorRequestModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

    }

    @Override
    public SponsorRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SponsorRequestHolder slh = new SponsorRequestHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SponsorRequestHolder holder, final int position)
    {

        sponsor_id = sponsorRequestModels.get(position).getSponsor_id();
        transaction_id = sponsorRequestModels.get(position).getTransaction_id();
        amount = sponsorRequestModels.get(position).getAmount_paid();
        sponsor_request_id = sponsorRequestModels.get(position).getId();
        sponsor_type = sponsorRequestModels.get(position).getSponsor_type();

        Log.d("FORREJECT",sponsor_id+"\n"+transaction_id+"\n"+amount+"\n"+sponsor_request_id);

        holder.sponsor_name.setText(sponsorRequestModels.get(position).getSponsor_name());
        holder.sponsor_req_amount_doller.setText("$ " + sponsorRequestModels.get(position).getAmount_paid());

        Log.d("PTAHJHJ",sponsorRequestModels.get(position).getSponsor_image());
        Picasso.with(context)
                .load(sponsorRequestModels.get(position).getSponsor_image())
                .placeholder(R.drawable.dummy_user_profile)
                .into(holder.sponsor_image);

        holder.accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Log.d("SPREQ",AppUrls.BASE_URL + AppUrls.ACCEPT_SPONSOR);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_SPONSOR,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //progressDialog.dismiss();
                                    Log.d("AcceptSponsor", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String success = jsonObject.getString("success");
                                        String message = jsonObject.getString("message");
                                        String response_code = jsonObject.getString("response_code");
                                        if (response_code.equals("10100")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(context, "You Accepted the Sponsor Request..!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(context, SponsorRequestActivity.class);
                                            context.startActivity(intent);
                                            context.finish();
                                        }
                                        if (response_code.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        progressDialog.cancel();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.cancel();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");

                            Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                            return headers;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("user_type",user_type);   // params.put("user_type", "SPONSOR");
                              // params.put("user_type", "SPONSOR");
                            params.put("transaction_id", sponsorRequestModels.get(position).getTransaction_id());
                            params.put("amount", sponsorRequestModels.get(position).getAmount_paid());
                            params.put("sponsor_request_id", sponsorRequestModels.get(position).getId());
                            params.put("sponsor_id", sponsorRequestModels.get(position).getSponsor_id());

                            Log.d("ACCEPTSPONSOR", "PARMS" + params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);
                } else {
                    // progressDialog.cancel();
                    Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Are You Sure?");
                alertDialog.setMessage("You Want To Reject This Sponsor?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {

                        rejectSponsor(sponsorRequestModels.get(position).getTransaction_id(),sponsorRequestModels.get(position).getAmount_paid(),sponsorRequestModels.get(position).getId(),sponsorRequestModels.get(position).getSponsor_id());
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.setItemClickListener(new SponsorRequestItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    public void rejectSponsor(final String send_transaction_id, final String send_amount, final String send_sponsor_request_id, final String send_sponsor_id) {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REJECT_SPONSOR,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // progressDialog.dismiss();
                            Log.d("RejectSponsor", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "You Rejected the Sponsor Request..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context, SponsorRequestActivity.class);
                                    context.startActivity(intent);

                                }
                                if (response_code.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("sponsor_type", sponsor_type);  //SPONSOR
                    params.put("transaction_id", send_transaction_id);
                    params.put("amount", send_amount);
                    params.put("sponsor_request_id", send_sponsor_request_id);
                    params.put("sponsor_id", send_sponsor_id);

                    Log.d("REJECTSPONSOR:", params.toString());
                    return params;


                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getItemCount() {
        return this.sponsorRequestModels.size();
    }
}
