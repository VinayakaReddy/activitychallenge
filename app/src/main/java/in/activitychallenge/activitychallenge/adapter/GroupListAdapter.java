package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.GroupChatDetailActivity;
import in.activitychallenge.activitychallenge.filters.CustomFilterForChallengeToFriend;
import in.activitychallenge.activitychallenge.filters.CustomFilterForGroupChat;
import in.activitychallenge.activitychallenge.fragments.GroupChatFragment;
import in.activitychallenge.activitychallenge.holder.GroupListHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.GroupListItemClickListener;
import in.activitychallenge.activitychallenge.models.GroupListModel;


public class GroupListAdapter extends RecyclerView.Adapter<GroupListHolder> implements Filterable{
    public ArrayList<GroupListModel> groupListModels;
    public GroupChatFragment context;
    String isRead,group_type;
    LayoutInflater li;
    int resource;
    String from_chat_type;
    Typeface typeface,typeface2;
    CustomFilterForGroupChat filter;
    public GroupListAdapter(ArrayList<GroupListModel> groupListModels, GroupChatFragment context, int resource,String group_type,String chat_type) {
        this.groupListModels = groupListModels;
        this.context = context;
        this.resource = resource;
        this.group_type = group_type;
        from_chat_type=chat_type;
       typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
       typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }



    @Override
    public GroupListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        GroupListHolder slh = new GroupListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(GroupListHolder holder, final int position)
    {



        String str = groupListModels.get(position).getFrom_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.group_name_text.setText(Html.fromHtml(converted_string));
        holder.group_name_text.setTypeface(typeface);

       holder.mesage_desc_text.setText(Html.fromHtml(groupListModels.get(position).getMsg()));
       holder.message_sent_time.setText(Html.fromHtml(groupListModels.get(position).getSent_on_txt()));

          Picasso.with(context.getActivity())
                .load(groupListModels.get(position).getProfile_pic())
                .placeholder(R.drawable.dummy_group_profile)
                .resize(60,60)
                .into(holder.group_profile_pic);


        holder.setItemClickListener(new GroupListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
               Intent ingrpdetail=new Intent(context.getActivity(), GroupChatDetailActivity.class);

                ingrpdetail.putExtra("group_id",groupListModels.get(position).getId());
                ingrpdetail.putExtra("group_name",groupListModels.get(position).getFrom_name());
                ingrpdetail.putExtra("profile_pic",groupListModels.get(position).getProfile_pic());
                ingrpdetail.putExtra("GROUP_TYPE",group_type);
                ingrpdetail.putExtra("GROUP_CONVERSATION_TYPE",groupListModels.get(position).getConversation_type());
                ingrpdetail.putExtra("FROM_TYPE", from_chat_type);
                Log.d("TETAUA",group_type+"IDD:"+groupListModels.get(position).getId()+"fromname:"+groupListModels.get(position).getFrom_name()+"pic:"+groupListModels.get(position).getProfile_pic()+" ctype:"+groupListModels.get(position).getConversation_type());


               context.startActivity(ingrpdetail);


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.groupListModels.size();
    }
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForGroupChat(groupListModels, this);
        }

        return filter;
    }
}
