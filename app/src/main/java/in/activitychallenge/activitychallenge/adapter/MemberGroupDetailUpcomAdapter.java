package in.activitychallenge.activitychallenge.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.holder.ChallengeGroupDetailHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeGroupDetailItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeGroupDetailModel;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class MemberGroupDetailUpcomAdapter extends RecyclerView.Adapter<ChallengeGroupDetailHolder>{

    public ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels;
    GroupDetailActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
    UserSessionManager session;
    String user_name;
    String Paused_by,challenge_type;




    public MemberGroupDetailUpcomAdapter(ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels, GroupDetailActivity context, int resource) {
        this.challnggroupDetaillistModels = challnggroupDetaillistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.hermes));
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);


    }

    @Override
    public ChallengeGroupDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeGroupDetailHolder slh = new ChallengeGroupDetailHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeGroupDetailHolder holder, final int position)
    {

        holder.pause_challenge.setVisibility(View.GONE);
        String str_opponentname = challnggroupDetaillistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem= challnggroupDetaillistModels.get(position).getUser_name();
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        String paused_by   = challnggroupDetaillistModels.get(position).getPaused_by();
        if (!paused_by.equals("")) {
            Paused_by = paused_by.substring(0, 1).toUpperCase() + paused_by.substring(1);
        }

        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.vsText.setTypeface(typeface3);

        holder.cash_prize_amount.setText(("$"+challnggroupDetaillistModels.get(position).getAmount()));
        String timedate=parseDateToddMMyyyy(challnggroupDetaillistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));

        challenge_type=challnggroupDetaillistModels.get(position).getChallenge_type();
        if(challenge_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        }
        else if(challenge_type.equals("LONGRUN"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
        }
        else
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }


        holder.status.setText(Html.fromHtml(challnggroupDetaillistModels.get(position).getStatus()));
        if (challnggroupDetaillistModels.get(position).getStatus().equals("PENDING"))
        {
            holder.status.setText("PENDING");
        }
        else if (challnggroupDetaillistModels.get(position).getStatus().equals("COMPLETED")){
            holder.status.setText("COMPLETED");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("YETTOSTART")){
            holder.status.setText("UPCOMING");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("PAUSED"))
        {
            holder.status.setText("PAUSED");
            holder.pause_challenge.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Paused by : "+Paused_by);
            holder.won_cash.setVisibility(View.GONE);

        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("RUNNING")){
            holder.status.setText("ONGOING");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("REJECTED")){
            holder.status.setText("REJECTED");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("TIE")){
            holder.status.setText("TIE");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("DRAW")){
            holder.status.setText("DRAW");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("MANUAL_CANCELLATION")){
            holder.status.setText("MANUAL CANCELLATION");
        }else if (challnggroupDetaillistModels.get(position).getStatus().equals("AUTO_CANCELLATION")){
            holder.status.setText("AUTO CANCELLATION");
        }else {
            Toast.makeText(context, "no types found ", Toast.LENGTH_SHORT).show();
        }
        holder.status.setBackgroundColor(Color.parseColor("#DD6B55"));
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context)
                .load(challnggroupDetaillistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


        holder.setItemClickListener(new ChallengeGroupDetailItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {


            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.challnggroupDetaillistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
