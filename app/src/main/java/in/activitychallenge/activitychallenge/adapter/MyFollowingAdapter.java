package in.activitychallenge.activitychallenge.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.MemberDetailActivity;
import in.activitychallenge.activitychallenge.activities.MyFollowing;
import in.activitychallenge.activitychallenge.filters.CustomFilterForAllMembersList;
import in.activitychallenge.activitychallenge.holder.AllMembersHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.AllMembersItemClickListener;
import in.activitychallenge.activitychallenge.models.AllMembersModel;


public class MyFollowingAdapter extends RecyclerView.Adapter<AllMembersHolder> {

    public ArrayList<AllMembersModel> myfollowinglistModels;
    MyFollowing context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;

    CustomFilterForAllMembersList filter;


    public MyFollowingAdapter(ArrayList<AllMembersModel> myfollowinglistModels, MyFollowing context, int resource) {
        this.myfollowinglistModels = myfollowinglistModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_bold));
    }

    @Override
    public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllMembersHolder slh = new AllMembersHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllMembersHolder holder, final int position)
    {


        String str = myfollowinglistModels.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.all_member_user_name.setText(Html.fromHtml(converted_string));

        holder.all_member_ranking_text.setText(Html.fromHtml(myfollowinglistModels.get(position).getOverall_rank()));
        holder.all_member_percentage_text.setText(Html.fromHtml(myfollowinglistModels.get(position).getWinning_per()+" %"));
        holder.all_member_won_text.setText(Html.fromHtml(myfollowinglistModels.get(position).getTotal_win()));
        holder.all_member_loss_text.setText(Html.fromHtml(myfollowinglistModels.get(position).getTotal_loss()));


          String user_type= myfollowinglistModels.get(position).getUser_type();
          if(user_type.equals("USER"))
          {
             holder.user_type.setText("USER");
          }
          else if(user_type.equals("GROUP"))
          {
              holder.user_type.setText("GROUP");
          }
          else{
              holder.user_type.setText("SPONSOR");
          }

         String  img_profile=myfollowinglistModels.get(position).getProfile_pic();

         if(img_profile.equals("null"))
         {
             Picasso.with(context)
                     .load(R.drawable.placeholder_dummy)
                     .placeholder(R.drawable.dummy_user_profile)
                     .resize(60,60)
                     .into(holder.all_member_image);
         }
         else
         {
             Picasso.with(context)
                     .load(myfollowinglistModels.get(position).getProfile_pic())
                     .placeholder(R.drawable.dummy_user_profile)
                     .into(holder.all_member_image);
         }


        Picasso.with(context)
                .load(myfollowinglistModels.get(position).getImage1())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_one);

        Picasso.with(context)
                .load(myfollowinglistModels.get(position).getImage2())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_two);

        Picasso.with(context)
                .load(myfollowinglistModels.get(position).getImage3())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_three);

        Picasso.with(context)
                .load(myfollowinglistModels.get(position).getImage4())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_four);

        Picasso.with(context)
                .load(myfollowinglistModels.get(position).getImage5())
                .placeholder(R.drawable.img_circle_placeholder)
                .into(holder.iv_five);


        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                Intent intent=new Intent(context, MemberDetailActivity.class);
                Log.d("IDDD:",myfollowinglistModels.get(position).getId());
                intent.putExtra("MEMBER_ID",myfollowinglistModels.get(position).getId());
                intent.putExtra("MEMBER_NAME",myfollowinglistModels.get(position).getName());
                intent.putExtra("member_user_type",myfollowinglistModels.get(position).getUser_type());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.myfollowinglistModels.size();
    }


}
