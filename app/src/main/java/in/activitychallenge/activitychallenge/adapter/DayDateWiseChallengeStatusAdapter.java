package in.activitychallenge.activitychallenge.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.holder.DayDateWiseChallengeStatusHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.CategoryWinLossClickListner;
import in.activitychallenge.activitychallenge.models.DayDateWiseStatusModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;


public class DayDateWiseChallengeStatusAdapter extends RecyclerView.Adapter<DayDateWiseChallengeStatusHolder>
{
    public ArrayList<DayDateWiseStatusModel> categditem;
   // public StepChallengeStatusActivity context;
    Activity activity;
    LayoutInflater li;
    int resource;
    String status,evaluation_factor;


    public DayDateWiseChallengeStatusAdapter(ArrayList<DayDateWiseStatusModel> categditem, Activity activity, int resource,String evaluation_factor ) {
        this.categditem = categditem;
        this.activity = activity;
        this.resource = resource;
        this.evaluation_factor = evaluation_factor;

        li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();

    }

    @Override
    public DayDateWiseChallengeStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        DayDateWiseChallengeStatusHolder slh = new DayDateWiseChallengeStatusHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final DayDateWiseChallengeStatusHolder holder, final int position)
    {

        holder.date.setText( parseDateToddMMyyyy(categditem.get(position).getDate()));
        Log.v("Dates",categditem.get(position).getDate());
       // holder.values.setText(categditem.get(position).getValue());

        if(evaluation_factor.equals("DISTANCE"))
        {
            holder.values.setVisibility(View.VISIBLE);
            holder.values.setText(categditem.get(position).getValue()+" Km");
        }
        else if(evaluation_factor.equals("IMAGE"))
        {
            //
            holder.holo_img.setVisibility(View.VISIBLE);
            String value=categditem.get(position).getValue();
            if(value!=null &&value.length()>1){
                holder.date.setTextColor(Color.parseColor("#ffffff"));
                Picasso.with(activity)
                        .load(AppUrls.BASE_IMAGE_URL+categditem.get(position).getValue())
                        .placeholder(R.drawable.no_image_found)
                        .into(holder.holo_img);
            }else{
                holder.date.setTextColor(Color.parseColor("#000000"));
                Picasso.with(activity)
                        .load(AppUrls.BASE_IMAGE_URL+categditem.get(position).getValue())
                       .into(holder.holo_img);
            }

        }
        else if(evaluation_factor.equals("TIME"))
        {
            holder.values.setVisibility(View.VISIBLE);

            holder.values.setText(String.format("%d M, %d s",
                    TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(categditem.get(position).getValue())),
                    TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(categditem.get(position).getValue())) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(categditem.get(position).getValue())))));
        }
        else
        {
            Log.d("dDDDDDd",categditem.get(position).getValue());
            holder.values.setVisibility(View.VISIBLE);
            holder.values.setText(categditem.get(position).getValue()+" Steps");
        }


        status=categditem.get(position).getStatus();
        if(status.equals("1"))
        {
            holder.dot_status.setBackgroundResource(R.color.days_complet);
        }
        else if(status.equals("0"))
        {
            holder.dot_status.setBackgroundResource(R.color.days_incomplet);
        }
        else
        {
            holder.dot_status.setBackgroundResource(R.color.days_pending);
        }


        holder.setItemClickListener(new CategoryWinLossClickListner()
        {
            @Override
            public void onItemClick(View v, int pos)
            {


            }
        });

    }

    @Override
    public int getItemCount() {
        return this.categditem.size();
    }

//date conversion

    public String parseDateToddMMyyyy(String datestr) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}



