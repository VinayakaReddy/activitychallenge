package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllMoreGroupFromGDActivity;
import in.activitychallenge.activitychallenge.holder.GetGroupMembersHolder;
import in.activitychallenge.activitychallenge.models.GetGroupMembersModel;

/**
 * Created by admin on 12/19/2017.
 */

public class AllMoreGroupMembersAdapter extends RecyclerView.Adapter<GetGroupMembersHolder> {
    AllMoreGroupFromGDActivity context;
    ArrayList<GetGroupMembersModel> allMoregrpmemberList;
    int resource;
    LayoutInflater lInfla;

    public AllMoreGroupMembersAdapter(AllMoreGroupFromGDActivity context, ArrayList<GetGroupMembersModel> allMoregrpmemberList, int resource) {
        this.context=context;
        this.allMoregrpmemberList=allMoregrpmemberList;
        this.resource=resource;
        lInfla=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public GetGroupMembersHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layout = lInfla.inflate(resource,parent,false);
        GetGroupMembersHolder slh = new GetGroupMembersHolder(layout);
        return slh;
    }





    @Override
    public void onBindViewHolder(GetGroupMembersHolder holder, int position) {
        holder.grp_lost_cnt.setText(String.valueOf(allMoregrpmemberList.get(position).getLost_challenges_M()));
        holder.grp_rank.setText(String.valueOf(allMoregrpmemberList.get(position).getUser_rank_M()));
        holder.grp_name.setText(allMoregrpmemberList.get(position).getUser_name_M());
        holder.grp_won_cnt.setText(String.valueOf(allMoregrpmemberList.get(position).getWin_challenges_M()));
        holder.grp_percent.setText(String.valueOf(allMoregrpmemberList.get(position).getWinning_percentage_M()+" "+"%"));

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getProfile_pic_M())
                .placeholder(R.drawable.dummy_user_profile)
                .into(holder.grp_img);

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getImg1_M())
                .placeholder(R.drawable.img_circle_placeholder)
                .resize(30,30)
                .into(holder.grp_img_1);

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getImg2_M())
                .placeholder(R.drawable.img_circle_placeholder)
                .resize(30,30)
                .into(holder.grp_img_2);

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getImg3_M())
                .placeholder(R.drawable.img_circle_placeholder)
                .resize(30,30)
                .into(holder.grp_img_3);

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getImg4_M())
                .placeholder(R.drawable.img_circle_placeholder)
                .resize(30,30)
                .into(holder.grp_img_4);

        Picasso.with(context)
                .load(allMoregrpmemberList.get(position).getImg5_M())
                .placeholder(R.drawable.img_circle_placeholder)
                .resize(30,30)
                .into(holder.grp_img_5);



    }

    @Override
    public int getItemCount() {
        return allMoregrpmemberList.size();
    }
}
