package in.activitychallenge.activitychallenge.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gjiazhe.panoramaimageview.GyroscopeObserver;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ActivityPrice;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeToFriendActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeToGroupActivity;
import in.activitychallenge.activitychallenge.filters.ActivityFilterList;
import in.activitychallenge.activitychallenge.holder.ActivityPriceHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.ActivityPriceModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class ActivityPriceAdapter extends RecyclerView.Adapter<ActivityPriceHolder> {

    public ArrayList<ActivityPriceModel> listModels, filterList;
    ActivityPrice context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    private static final int PLACE_PICKER_REQUEST = 1;
    Sensor stepSensor;
    SensorManager sManager;
    GPSTracker gps;
    Dialog dialog;
    ActivityFilterList filter;
    private GyroscopeObserver gyroscopeObserver;
    UserSessionManager session;
    int challege_count_value, activityapplied;
    int eventflag = 0;
    String specialeventId;
    String user_id, token, device_id, user_type = "", main_user_id = "", main_user_type, Challenges_count = "", activityAppliedCount = "";
    String activity_id, challenge_id, challenge_goal, evaluation_factor, evaluation_factor_unit, type, span, date, location_name, location_lat, location_lng;

    public ActivityPriceAdapter(ArrayList<ActivityPriceModel> listModels, ActivityPrice context, int resource, String activity_id, String challenge_id, String challenge_goal, String evaluation_factor, String evaluation_factor_unit, String type, String span, String date, String location_name, String location_lat, String location_lng, String main_user_id, String main_user_type, int flag,String special) {
        this.listModels = listModels;
        this.context = context;
        this.resource = resource;
        this.filterList = listModels;
        this.activity_id = activity_id;
        this.challenge_id = challenge_id;
        this.challenge_goal = challenge_goal;
        eventflag = flag;
        specialeventId=special;
        this.evaluation_factor = evaluation_factor;
        this.evaluation_factor_unit = evaluation_factor_unit;
        this.type = type;
        this.span = span;
        this.date = date;
        this.location_name = location_name;
        this.location_lat = location_lat;
        this.location_lng = location_lng;
        this.main_user_id = main_user_id;
        this.main_user_type = main_user_type;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        gps = new GPSTracker(context);
        gyroscopeObserver = new GyroscopeObserver();
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

    }

    @Override
    public ActivityPriceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ActivityPriceHolder slh = new ActivityPriceHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ActivityPriceHolder holder, final int position) {
        getChallengesCount();
        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Activity Challenge");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Check out this in Playstore!");
                context.startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        int value1 = Integer.parseInt(span);
        int value2 = Integer.parseInt(listModels.get(position).getPrice());
        int total_value = value1 * value2;
        String payable_amount = String.valueOf(total_value);
        holder.name.setText("$ " + payable_amount);


        Log.d("xfkldfbng", holder.name.getText().toString());
        holder.setItemClickListener(new ActivityItemClickListener() {
            @Override
            public void onItemClick(View v, final int pos) {
                checkInternet = NetworkChecking.isConnected(context);
                //   Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                if (checkInternet) {
                    String url = AppUrls.BASE_URL + AppUrls.GET_ACCESS_CONTROL + user_id;
                    Log.d("LISTOFINURL", url);
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("ACTIVITY_RESPONSE:", response);

                                        String status = jsonObject.getString("response_code");
                                        if (status.equals("10100")) {
                                            JSONObject jsonArray = jsonObject.getJSONObject("data");
                                            JSONObject ignore_entities = jsonArray.getJSONObject("ignore_entities");
                                            String scratch_card = ignore_entities.getString("scratch_card");
                                            String lottery = ignore_entities.getString("lottery");
                                            String promotion = ignore_entities.getString("promotion");
                                            String sponsoring = ignore_entities.getString("sponsoring");
                                            String challenge_user_group_to_admin = ignore_entities.getString("challenge_user_group_to_admin");
                                            String challenge_group_to_group = ignore_entities.getString("challenge_group_to_group");
                                            String challenge_user_to_user = ignore_entities.getString("challenge_user_to_user");

                                            dialog = new Dialog(context);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setContentView(R.layout.challenge_to_dialog);

                                            LinearLayout ll_chlng_grp = (LinearLayout) dialog.findViewById(R.id.ll_chlng_grp);
                                            LinearLayout ll_chlng_frnd = (LinearLayout) dialog.findViewById(R.id.ll_chlng_frnd);
                                            LinearLayout ll_chlng_myslf = (LinearLayout) dialog.findViewById(R.id.ll_chlng_myslf);
                                            int value1 = Integer.parseInt(span);
                                            int value2 = Integer.parseInt(listModels.get(position).getPrice());
                                            int total_value = value1 * value2;
                                            final String payable_amount = String.valueOf(total_value);
                                            //holder.name.setText("$ "+payable_amount);

                                            if (challenge_user_group_to_admin.equals("0") && challenge_group_to_group.equals("0") && challenge_user_to_user.equals("0")) {
                                                ll_chlng_myslf.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                        if (activityapplied == 0) {
                                                            Intent intent = new Intent(context, ChallengePaymentActivity.class);
                                                            intent.putExtra("user_id", main_user_id);
                                                            intent.putExtra("user_type", main_user_type);
                                                            intent.putExtra("activity", "challengeto");
                                                            // intent.putExtra("paymentAmount",listModels.get(position).getPrice());
                                                            intent.putExtra("paymentAmount", payable_amount);
                                                            intent.putExtra("activity_id", activity_id);
                                                            intent.putExtra("challenge_id", challenge_id);
                                                            intent.putExtra("challenge_goal", challenge_goal);
                                                            intent.putExtra("evaluation_factor", evaluation_factor);
                                                            intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                            if (eventflag == 1) {
                                                                intent.putExtra("type", "SPECIAL_EVENT");
                                                                intent.putExtra("special", specialeventId);
                                                            }
                                                            else {
                                                                intent.putExtra("type", type);
                                                            }
                                                            intent.putExtra("span", span);
                                                            intent.putExtra("date", date);
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            intent.putExtra("opponent_id", "59def9dce25e4081ac0f1093");
                                                            intent.putExtra("opponent_type", "ADMIN");
                                                            context.startActivity(intent);

                                                        } else {
                                                            Toast.makeText(context, "You cannot create two or more challanges against admin", Toast.LENGTH_SHORT).show();
                                                        }

                                                        dialog.cancel();
                                                    }
                                                });


                                                ll_chlng_frnd.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                        Intent intent = new Intent(context, ChallengeToFriendActivity.class);
                                                        intent.putExtra("user_id", main_user_id);
                                                        intent.putExtra("user_type", main_user_type);
                                                        // intent.putExtra("amount",listModels.get(position).getPrice());
                                                        intent.putExtra("amount", payable_amount);
                                                        intent.putExtra("activity_id", activity_id);
                                                        intent.putExtra("challenge_id", challenge_id);
                                                        intent.putExtra("challenge_goal", challenge_goal);
                                                        intent.putExtra("evaluation_factor", evaluation_factor);
                                                        intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                        if (eventflag == 1) {
                                                            intent.putExtra("type", "SPECIAL_EVENT");

                                                        }
                                                        else {
                                                            intent.putExtra("type", type);
                                                        }
                                                        intent.putExtra("span", span);
                                                        intent.putExtra("date", date);
                                                        intent.putExtra("location_name", location_name);
                                                        intent.putExtra("location_lat", location_lat);
                                                        intent.putExtra("location_lng", location_lng);
                                                        context.startActivity(intent);


                                                        dialog.cancel();
                                                    }
                                                });


                                                ll_chlng_grp.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                        //    Toast.makeText(context, "Challenge to Group", Toast.LENGTH_SHORT).show();
                                                        Intent intent_group = new Intent(context, ChallengeToGroupActivity.class);
                                                        intent_group.putExtra("user_id", main_user_id);
                                                        intent_group.putExtra("user_type", main_user_type);
                                                        // intent_group.putExtra("amount",listModels.get(position).getPrice());
                                                        intent_group.putExtra("amount", payable_amount);
                                                        intent_group.putExtra("activity_id", activity_id);
                                                        intent_group.putExtra("challenge_id", challenge_id);
                                                        intent_group.putExtra("challenge_goal", challenge_goal);
                                                        intent_group.putExtra("evaluation_factor", evaluation_factor);
                                                        intent_group.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                        if (eventflag == 1) {
                                                            intent_group.putExtra("type", "SPECIAL_EVENT");

                                                        }
                                                        else {
                                                            intent_group.putExtra("type", type);
                                                        }
                                                        intent_group.putExtra("span", span);
                                                        intent_group.putExtra("date", date);
                                                        intent_group.putExtra("location_name", location_name);
                                                        intent_group.putExtra("location_lat", location_lat);
                                                        intent_group.putExtra("location_lng", location_lng);
                                                        context.startActivity(intent_group);

                                                        dialog.cancel();

                                                    }
                                                });

                                                dialog.create();
                                                if (challege_count_value >= 10) {
                                                    Toast.makeText(context, "You Reached Maximun number of Challenges", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    dialog.show();
                                                }

                                               /* // add a list
                                                String[] animals = {"Challenge Myself", "Challenge to Friend", "Challenge to Group"};
                                                builder.setItems(animals, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        switch (which) {
                                                            case 0:
                                                                Toast.makeText(context, "Challenge Myself", Toast.LENGTH_SHORT).show();
                                                                sendChallengeToAdmin(user_id,listModels.get(position).getPrice(),activity_id,challenge_id,challenge_goal,evaluation_factor,evaluation_factor_unit,type,span,date,location_name,location_lat,location_lng);
                                                            case 1:
                                                                Toast.makeText(context, "Challenge to Friend", Toast.LENGTH_SHORT).show();
                                                                Intent intent =new Intent(context, ChallengeToFriendActivity.class);
                                                                intent.putExtra("user_id",user_id);
                                                                intent.putExtra("amount",listModels.get(position).getPrice());
                                                                intent.putExtra("activity_id",activity_id);
                                                                intent.putExtra("challenge_id",challenge_id);
                                                                intent.putExtra("challenge_goal",challenge_goal);
                                                                intent.putExtra("evaluation_factor",evaluation_factor);
                                                                intent.putExtra("evaluation_factor_unit",evaluation_factor_unit);
                                                                intent.putExtra("type",type);
                                                                intent.putExtra("span",span);
                                                                intent.putExtra("date",date);
                                                                intent.putExtra("location_name",location_name);
                                                                intent.putExtra("location_lat",location_lat);
                                                                intent.putExtra("location_lng",location_lng);
                                                                context.startActivity(intent);
                                                            case 2:
                                                                Toast.makeText(context, "Challenge to Group", Toast.LENGTH_SHORT).show();
                                                                Intent intent_group =new Intent(context, ChallengeToGroupActivity.class);
                                                                intent_group.putExtra("user_id",user_id);
                                                                intent_group.putExtra("amount",listModels.get(position).getPrice());
                                                                intent_group.putExtra("activity_id",activity_id);
                                                                intent_group.putExtra("challenge_id",challenge_id);
                                                                intent_group.putExtra("challenge_goal",challenge_goal);
                                                                intent_group.putExtra("evaluation_factor",evaluation_factor);
                                                                intent_group.putExtra("evaluation_factor_unit",evaluation_factor_unit);
                                                                intent_group.putExtra("type",type);
                                                                intent_group.putExtra("span",span);
                                                                intent_group.putExtra("date",date);
                                                                intent_group.putExtra("location_name",location_name);
                                                                intent_group.putExtra("location_lat",location_lat);
                                                                intent_group.putExtra("location_lng",location_lng);
                                                                context.startActivity(intent_group);
                                                        }
                                                    }
                                                });

                                                // create and show the alert dialog
                                                AlertDialog dialog = builder.create();
                                                dialog.show();*/
                                            } else if (!challenge_user_group_to_admin.equals("0") && challenge_group_to_group.equals("0") && !challenge_user_to_user.equals("0")) {
                                                ll_chlng_grp.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (activityapplied == 0) {
                                                            Intent intent = new Intent(context, ChallengePaymentActivity.class);
                                                            intent.putExtra("user_id", main_user_id);
                                                            intent.putExtra("user_type", main_user_type);
                                                            intent.putExtra("activity", "challengeto");
                                                            // intent.putExtra("paymentAmount",listModels.get(position).getPrice());
                                                            intent.putExtra("paymentAmount", payable_amount);
                                                            intent.putExtra("activity_id", activity_id);
                                                            intent.putExtra("challenge_id", challenge_id);
                                                            intent.putExtra("challenge_goal", challenge_goal);
                                                            intent.putExtra("evaluation_factor", evaluation_factor);
                                                            intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                            if (eventflag == 1) {
                                                                intent.putExtra("type", "SPECIAL_EVENT");

                                                            }
                                                            else {
                                                                intent.putExtra("type", type);
                                                            }
                                                            intent.putExtra("span", span);
                                                            intent.putExtra("date", date);
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            intent.putExtra("opponent_id", "59def9dce25e4081ac0f1093");
                                                            intent.putExtra("opponent_type", "ADMIN");
                                                            context.startActivity(intent);

                                                        } else {
                                                            Toast.makeText(context, "You cannot create two or more challanges against admin", Toast.LENGTH_SHORT).show();
                                                        }
                                                        dialog.cancel();
                                                    }
                                                });

                                                ll_chlng_frnd.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(context, ChallengeToFriendActivity.class);
                                                        intent.putExtra("user_id", main_user_id);
                                                        intent.putExtra("user_type", main_user_type);
                                                        // intent.putExtra("amount",listModels.get(position).getPrice());
                                                        intent.putExtra("amount", payable_amount);
                                                        intent.putExtra("activity_id", activity_id);
                                                        intent.putExtra("challenge_id", challenge_id);
                                                        intent.putExtra("challenge_goal", challenge_goal);
                                                        intent.putExtra("evaluation_factor", evaluation_factor);
                                                        intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                        if (eventflag == 1) {
                                                            intent.putExtra("type", "SPECIAL_EVENT");

                                                        }
                                                        else {
                                                            intent.putExtra("type", type);
                                                        }
                                                        intent.putExtra("span", span);
                                                        intent.putExtra("date", date);
                                                        intent.putExtra("location_name", location_name);
                                                        intent.putExtra("location_lat", location_lat);
                                                        intent.putExtra("location_lng", location_lng);
                                                        context.startActivity(intent);
                                                        dialog.cancel();
                                                    }
                                                });

                                                dialog.create();
                                                if (challege_count_value >= 10) {
                                                    Toast.makeText(context, "You Reached Maximun number of Challenges", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    dialog.show();
                                                }
                                                //dialog.show();

                                            } else if (!challenge_user_group_to_admin.equals("0") && !challenge_group_to_group.equals("0") && challenge_user_to_user.equals("0")) {
                                                ll_chlng_myslf.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (activityapplied == 0) {
                                                            Intent intent = new Intent(context, ChallengePaymentActivity.class);
                                                            intent.putExtra("user_id", main_user_id);
                                                            intent.putExtra("user_type", main_user_type);
                                                            intent.putExtra("activity", "challengeto");
                                                            // intent.putExtra("paymentAmount",listModels.get(position).getPrice());
                                                            intent.putExtra("paymentAmount", payable_amount);
                                                            intent.putExtra("activity_id", activity_id);
                                                            intent.putExtra("challenge_id", challenge_id);
                                                            intent.putExtra("challenge_goal", challenge_goal);
                                                            intent.putExtra("evaluation_factor", evaluation_factor);
                                                            intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                            if (eventflag == 1) {
                                                                intent.putExtra("type", "SPECIAL_EVENT");

                                                            }
                                                            else {
                                                                intent.putExtra("type", type);
                                                            }
                                                            intent.putExtra("span", span);
                                                            intent.putExtra("date", date);
                                                            intent.putExtra("location_name", location_name);
                                                            intent.putExtra("location_lat", location_lat);
                                                            intent.putExtra("location_lng", location_lng);
                                                            intent.putExtra("opponent_id", "59def9dce25e4081ac0f1093");
                                                            intent.putExtra("opponent_type", "ADMIN");
                                                            context.startActivity(intent);

                                                        } else {
                                                            Toast.makeText(context, "You cannot create two or more challanges against admin", Toast.LENGTH_SHORT).show();
                                                        }

                                                        dialog.cancel();
                                                    }
                                                });


                                                ll_chlng_grp.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //   Toast.makeText(context, "Challenge to Group", Toast.LENGTH_SHORT).show();
                                                        Intent intent_group = new Intent(context, ChallengeToGroupActivity.class);
                                                        intent_group.putExtra("user_id", main_user_id);
                                                        intent_group.putExtra("user_type", main_user_type);
                                                        // intent_group.putExtra("amount",listModels.get(position).getPrice());
                                                        intent_group.putExtra("amount", payable_amount);
                                                        intent_group.putExtra("activity_id", activity_id);
                                                        intent_group.putExtra("challenge_id", challenge_id);
                                                        intent_group.putExtra("challenge_goal", challenge_goal);
                                                        intent_group.putExtra("evaluation_factor", evaluation_factor);
                                                        intent_group.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                        if (eventflag == 1) {
                                                            intent_group.putExtra("type", "SPECIAL_EVENT");
                                                            intent_group.putExtra("special", specialeventId);
                                                        }
                                                        else {
                                                            intent_group.putExtra("type", type);
                                                        }
                                                        intent_group.putExtra("span", span);
                                                        intent_group.putExtra("date", date);
                                                        intent_group.putExtra("location_name", location_name);
                                                        intent_group.putExtra("location_lat", location_lat);
                                                        intent_group.putExtra("location_lng", location_lng);
                                                        context.startActivity(intent_group);
                                                        dialog.cancel();
                                                    }
                                                });
                                                dialog.create();
                                                if (challege_count_value >= 10) {
                                                    Toast.makeText(context, "You Reached Maximun number of Challenges", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    dialog.show();
                                                }
                                                // dialog.show();

                                            } else if (!challenge_user_group_to_admin.equals("1") && challenge_group_to_group.equals("1") && challenge_user_to_user.equals("1")) {
                                                ll_chlng_myslf.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(context, ChallengePaymentActivity.class);
                                                        intent.putExtra("user_id", main_user_id);
                                                        intent.putExtra("user_type", main_user_type);
                                                        intent.putExtra("activity", "challengeto");
                                                        //  intent.putExtra("paymentAmount",listModels.get(position).getPrice());
                                                        intent.putExtra("paymentAmount", payable_amount);
                                                        intent.putExtra("activity_id", activity_id);
                                                        intent.putExtra("challenge_id", challenge_id);
                                                        intent.putExtra("challenge_goal", challenge_goal);
                                                        intent.putExtra("evaluation_factor", evaluation_factor);
                                                        intent.putExtra("evaluation_factor_unit", evaluation_factor_unit);
                                                        if (eventflag == 1) {
                                                            intent.putExtra("type", "SPECIAL_EVENT");

                                                        }
                                                        else {
                                                            intent.putExtra("type", type);
                                                        }
                                                        intent.putExtra("span", span);
                                                        intent.putExtra("date", date);
                                                        intent.putExtra("location_name", location_name);
                                                        intent.putExtra("location_lat", location_lat);
                                                        intent.putExtra("location_lng", location_lng);
                                                        intent.putExtra("opponent_id", "59def9dce25e4081ac0f1093");
                                                        intent.putExtra("opponent_type", "ADMIN");
                                                        context.startActivity(intent);
                                                        dialog.cancel();
                                                    }
                                                });

                                                dialog.create();
                                                if (challege_count_value >= 10) {
                                                    Toast.makeText(context, "You Reached Maximun number of Challenges", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    dialog.show();
                                                }
                                                // dialog.show();

                                            }


                                        } else {
                                            Toast.makeText(context, "No Challenges", Toast.LENGTH_SHORT).show();
                                        }
                                        if (status.equals("10200")) {


                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }


                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {


                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                            return headers;
                        }

                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);

                } else {
                    Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listModels.size();
    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    private void getChallengesCount() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = "";
            String datestr = parseDateToddMMyyyy(date);
            if (type.equals("DAILY"))
                url = AppUrls.BASE_URL + AppUrls.GET_APPLIED_CHALLENGES_COUNT + user_id + AppUrls.CHALLENGE_TYPE + type + AppUrls.CHALLENGE_DATE + datestr;
            else
                url = AppUrls.BASE_URL + AppUrls.GET_APPLIED_CHALLENGES_COUNT + user_id + AppUrls.CHALLENGE_TYPE + type;

            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("CHALLENGECOUNTRESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    Challenges_count = jsonArray.getString("count");
                                    activityAppliedCount = jsonArray.getString("activity_applied");
                                    challege_count_value = Integer.parseInt(Challenges_count);
                                    activityapplied = Integer.parseInt(activityAppliedCount);
                                    Log.d("challenges_count", "" + activityapplied);
                                }
                                if (status.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    public String parseDateToddMMyyyy(String datestr) {
        String inputPattern = "dd-MMM-yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(datestr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
