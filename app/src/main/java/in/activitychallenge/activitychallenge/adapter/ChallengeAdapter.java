package in.activitychallenge.activitychallenge.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.FiveKmsActivity;
import in.activitychallenge.activitychallenge.activities.FortyfiveMinsActivity;
import in.activitychallenge.activitychallenge.activities.GetLocationActivity;
import in.activitychallenge.activitychallenge.activities.PicturePosesActivity;
import in.activitychallenge.activitychallenge.activities.SixtyMinsActivity;
import in.activitychallenge.activitychallenge.activities.StepsChallengeActivity;
import in.activitychallenge.activitychallenge.activities.TakeChalengeActivity;
import in.activitychallenge.activitychallenge.activities.ThirtyMinsActivity;
import in.activitychallenge.activitychallenge.holder.ActivityHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ActivityItemClickListener;
import in.activitychallenge.activitychallenge.models.ActivityModel;
import in.activitychallenge.activitychallenge.utilities.GPSTracker;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class ChallengeAdapter extends RecyclerView.Adapter<ActivityHolder> implements View.OnClickListener{
    public ArrayList<ActivityModel> listModels, filterList;
    TakeChalengeActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String user_id,token,device_id,user_type;
    Sensor stepSensor;
    SensorManager sManager;
    UserSessionManager session;

    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    String location_name = "", location_lat = "", location_lng = "";
    String startDate,endDate;
    int noOfDays;
    JSONArray locationArr;


    public ChallengeAdapter(ArrayList<ActivityModel> listModels, TakeChalengeActivity context, int resource,String startdate,String enddate,int noOfDays,JSONArray locationArr) {
        this.listModels = listModels;
        this.context = context;
        this.resource = resource;
        this.filterList = listModels;
        startDate=startdate;
        endDate=enddate;
        this.noOfDays=noOfDays;
        this.locationArr=locationArr;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        gps = new GPSTracker(context);
         session=new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        checkInternet = NetworkChecking.isConnected(context);

    }

    @Override
    public ActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ActivityHolder slh = new ActivityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(ActivityHolder holder, final int position) {

        gps = new GPSTracker(context);
        //    Log.d("STTTXXX",listModels.get(position).getLongrun_challenges());
        geocoder = new Geocoder(context, Locale.getDefault());
        if (gps.canGetLocation()) {
            final Handler mHandler1 = new Handler();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(10000000);
                            mHandler1.post(new Runnable() {

                                @Override
                                public void run() {
                                    //   Toast.makeText(getApplicationContext(), "refreshing", Toast.LENGTH_LONG).show();
                                    double latitude = gps.getLatitude();
                                    String lat = String.valueOf(latitude);
                                    double longitude = gps.getLongitude();
                                    String lng = String.valueOf(longitude);
                                    Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                        String place = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String country = addresses.get(0).getCountryName();
                                        String postalCode = addresses.get(0).getPostalCode();
                                        String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            }).start();

            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);

            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // current_location.setText(address);
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                location_name = address;
                location_lat = lat;
                location_lng = lng;
                //  sendUserData(user_id,device_id,lat,lng,city,country,state,place,token);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gps.showSettingsAlert();
        }

        if (sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null && sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && gps.canGetLocation()) {
            if (!listModels.get(position).getDaily_challenges().equals("0") || !listModels.get(position).getWeekly_challenges().equals("0") ||!listModels.get(position).getLongrun_challenges().equals("0") ) {
                holder.card.setBackgroundResource(R.drawable.act_round_bg);
                if(listModels.get(position).getTools_required().contains("GPS")) {
                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {

                            if (user_type.equals("USER")) {
                                if (listModels.get(position).getMin_value().equals("5000 STEPS")) {
                                    // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                    if (listModels.get(position).getMin_value().equals("Walking")) {
                                        Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                    }
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "5000steps");
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            Log.d("sgvsdfnvknf","value"+listModels.get(pos).getLongrun_challenges());
                                            context.startActivity(intent);
                                        } else {
                                            Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(context, StepsChallengeActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            Log.d("sgvsdfnvknfff","value"+listModels.get(pos).getLongrun_challenges()+"---"+listModels.get(pos).getWeekly_challenges());
                                            context.startActivity(intent);




                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }
                                } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "30mins");
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            context.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            context.startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }
                                } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "45mins");
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            context.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            context.startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }

                                } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "60mins");
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            context.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(context, SixtyMinsActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            context.startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }

                                } else if (listModels.get(position).getMin_value().equals("5 KM")) {
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "5km");
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            context.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(context, FiveKmsActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());

                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            context.startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }
                                } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {
                                    if (checkInternet) {
                                        if (listModels.get(pos).getTools_required().contains("GPS")) {
                                            Intent intent = new Intent(context, GetLocationActivity.class);
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);
                                            intent.putExtra("type", "hollow_pictures");
                                            intent.putExtra("event","special");
                                            intent.putExtra("startdate",startDate);
                                            intent.putExtra("enddate",endDate);
                                            intent.putExtra("noofdays",noOfDays);
                                            intent.putExtra("location",locationArr.toString());
                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("minvalue",listModels.get(pos).getMin_value());
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            context.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(context, PicturePosesActivity.class);
                                            intent.putExtra("id", listModels.get(pos).getId());
                                            intent.putExtra("user_id", user_id);
                                            intent.putExtra("user_type", user_type);

                                            intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                            intent.putExtra("location_name", location_name);
                                            intent.putExtra("location_lat", location_lat);
                                            intent.putExtra("location_lng", location_lng);
                                            context.startActivity(intent);
                                        }
                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }
                                }

                            } else {
                                Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
                } else {
                    holder.setItemClickListener(new ActivityItemClickListener() {
                        @Override
                        public void onItemClick(View v, final int pos) {

                            if (user_type.equals("USER")) {
                                if (listModels.get(position).getMin_value().equals("5000 STEPS")) {
                                    // Toast.makeText(context, "5000 Steps", Toast.LENGTH_SHORT).show();
                                    if (listModels.get(position).getMin_value().equals("Walking")) {
                                        Toast.makeText(context, "Gps and Motion Sensor activated", Toast.LENGTH_SHORT).show();
                                    }

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, StepsChallengeActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);
                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }


                                } else if (listModels.get(position).getMin_value().equals("30 MINUTE")) {

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, ThirtyMinsActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);
                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }

                                } else if (listModels.get(position).getMin_value().equals("45 MINUTE")) {

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, FortyfiveMinsActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);

                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }


                                } else if (listModels.get(position).getMin_value().equals("60 MINUTE")) {

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, SixtyMinsActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);
                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }


                                } else if (listModels.get(position).getMin_value().equals("5 KM")) {

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, FiveKmsActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);

                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }

                                } else if (listModels.get(position).getMin_value().equals("10 HAMMING_DISTANCE")) {

                                    if (checkInternet) {

                                        Intent intent = new Intent(context, PicturePosesActivity.class);
                                        intent.putExtra("id", listModels.get(pos).getId());
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("user_type", user_type);
                                        intent.putExtra("activity_name", listModels.get(pos).getActivity_name());
                                        intent.putExtra("location_name", location_name);
                                        intent.putExtra("location_lat", location_lat);
                                        intent.putExtra("location_lng", location_lng);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                                    }

                                }

                            } else {
                                Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
                }
            } else {
                holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

                holder.setItemClickListener(new ActivityItemClickListener() {
                    @Override
                    public void onItemClick(View v, final int pos) {
                        if (checkInternet) {
                            if (user_type.equals("USER")) {
                                new AlertDialog.Builder(context)
                                        .setTitle("Sorry...")
                                        .setMessage( Html.fromHtml("<font color='#FF7F27'>This activity is offline</font>"))
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        })
                                        .setPositiveButton( Html.fromHtml("<font color='#000'>Back</font>"), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                            }else {
                                Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        } else {

            Toast.makeText(context, "Sensors not Working", Toast.LENGTH_SHORT).show();
            holder.card.setBackgroundResource(R.drawable.act_round_bg_off);

            holder.setItemClickListener(new ActivityItemClickListener() {
                @Override
                public void onItemClick(View v, final int pos) {
                    if (user_type.equals("USER")) {
                        new AlertDialog.Builder(context)
                                .setTitle("Unsupportable Device")
                                .setMessage("Your device needs sensors, to perform this action.")
                                .setCancelable(false)
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }else {
                        Toast.makeText(context, "Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        //holder.setItemClickListener(this);
        holder.name.setText(listModels.get(position).getActivity_name());
        holder.share_icon.setOnClickListener(this);
        holder.share_icon.setTag(position);

        Picasso.with(context)
                .load(listModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);
    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }



    @Override
    public void onClick(View view) {
        int pos= (int) view.getTag();
        if(view.getId()==R.id.share_icon){
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -"+listModels.get(pos).getActivity_name());
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.activitychallenge.activitychallenge&hl=en");
            context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

    }
}
