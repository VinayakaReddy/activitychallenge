package in.activitychallenge.activitychallenge.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.KmChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.MinuiteChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.PictureChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.PushupChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.StepChallengeStatusActivity;
import in.activitychallenge.activitychallenge.fragments.MyChallengesIndividualFragment;
import in.activitychallenge.activitychallenge.holder.ChallengeIndividRunHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.ChallengeIndividRunItemClickListener;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.MyApplication;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class ChallengeIndividRunAdapter extends RecyclerView.Adapter<ChallengeIndividRunHolder> {

    public ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels;
    MyChallengesIndividualFragment context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2, typeface3;
    UserSessionManager session;
    String user_name, challenge_type;

    public ChallengeIndividRunAdapter(ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels, MyChallengesIndividualFragment context, int resource) {
        this.challngIndRunlistModels = challngIndRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mp_bold));
        typeface3 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.hermes));
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);

    }

    @Override
    public ChallengeIndividRunHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeIndividRunHolder slh = new ChallengeIndividRunHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeIndividRunHolder holder, final int position) {
        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = challngIndRunlistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        //String usernem = user_name;
        String usernem = challngIndRunlistModels.get(position).getUser_name();
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);


        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.vsText.setTypeface(typeface3);

        holder.cash_prize_amount.setText(("$" + challngIndRunlistModels.get(position).getAmount()));

        String timedate = parseDateToddMMyyyy(challngIndRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));

        challenge_type = challngIndRunlistModels.get(position).getChallenge_type();
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.daily_bg));
        } else if (challenge_type.equals("LONGRUN")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.longrun_bg));
            //holder.challenge_type.setTextColor(R.color.longrun_bg);
        } else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.week_bg));
        }


        holder.status.setText(Html.fromHtml(challngIndRunlistModels.get(position).getStatus()));

        if (challngIndRunlistModels.get(position).getStatus().equals("PENDING")) {
            holder.status.setText("PENDING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("COMPLETED")) {
            holder.status.setText("COMPLETED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("YETTOSTART")) {
            holder.status.setText("UPCOMING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("PAUSED")) {
            holder.status.setText("PAUSED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("RUNNING")) {
            holder.status.setText("ONGOING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("REJECTED")) {
            holder.status.setText("REJECTED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("TIE")) {
            holder.status.setText("TIE");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("DRAW")) {
            holder.status.setText("DRAW");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("MANUAL_CANCELLATION")) {
            holder.status.setText("MANUAL CANCELLATION");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("AUTO_CANCELLATION")) {
            holder.status.setText("AUTO CANCELLATION");
        } else {
            Toast.makeText(context.getActivity(), "no types found ", Toast.LENGTH_SHORT).show();
        }
        holder.status.setBackgroundColor(Color.parseColor("#239843"));
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        Picasso.with(context.getActivity())
                .load(challngIndRunlistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });

       holder.setItemClickListener(new ChallengeIndividRunItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (challngIndRunlistModels.get(pos).getEvaluation_factor().equals("STEPS")) {
                    Intent intent = new Intent(context.getActivity(), StepChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", challngIndRunlistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", challngIndRunlistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", challngIndRunlistModels.get(position).getGps());
                    context.startActivity(intent);
                } else if (challngIndRunlistModels.get(pos).getEvaluation_factor().equals("DISTANCE")) {
                    //  Toast.makeText(context.getActivity(), "KM", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context.getActivity(), KmChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", challngIndRunlistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", challngIndRunlistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", challngIndRunlistModels.get(position).getGps());
                    context.startActivity(intent);
                } else if (challngIndRunlistModels.get(pos).getEvaluation_factor().equals("TIME")) {
                    //  Toast.makeText(context.getActivity(), "MINUTE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context.getActivity(), MinuiteChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", challngIndRunlistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", challngIndRunlistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", challngIndRunlistModels.get(position).getGps());
                    context.startActivity(intent);
                } else if (challngIndRunlistModels.get(pos).getEvaluation_factor().equals("IMAGE")) {
                    // Toast.makeText(context.getActivity(), "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context.getActivity(), PictureChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", challngIndRunlistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", challngIndRunlistModels.get(position).getOpponent_type());
                    Log.d("IntentData", challngIndRunlistModels.get(position).getActivity_id() + "\n" + challngIndRunlistModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }else if (challngIndRunlistModels.get(pos).getEvaluation_factor().equals("COUNT")) {
                    // Toast.makeText(context, "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context.getActivity(), PushupChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", challngIndRunlistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", challngIndRunlistModels.get(position).getOpponent_type());
                    Log.d("IntentData", challngIndRunlistModels.get(position).getActivity_id() + "\n" + challngIndRunlistModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }

            }
        });


        if (challngIndRunlistModels.get(position).getGps().equals("1") && challngIndRunlistModels.get(position).getStatus().equals("RUNNING")) {
            if (!MyApplication.isMyServiceRunning(context.getActivity(), GpsService.class))
                context.getActivity().startService(new Intent(context.getActivity(), GpsService.class));
        } else {
            if (MyApplication.isMyServiceRunning(context.getActivity(), GpsService.class))
                context.getActivity().stopService(new Intent(context.getActivity(), GpsService.class));
        }


    }


    @Override
    public int getItemCount() {
        return this.challngIndRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
