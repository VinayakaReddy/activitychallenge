package in.activitychallenge.activitychallenge.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.activities.LotteryCodesActivity;
import in.activitychallenge.activitychallenge.holder.LotteryCodesHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.LotteryCodesItemClickListener;
import in.activitychallenge.activitychallenge.models.LotteryCodesModel;

public class LotteryCodesAdapter extends RecyclerView.Adapter<LotteryCodesHolder> {

    public ArrayList<LotteryCodesModel> lotteryCodesModels;
    LotteryCodesActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;

    public LotteryCodesAdapter(ArrayList<LotteryCodesModel> lotteryCodesModels, LotteryCodesActivity context, int resource) {
        this.lotteryCodesModels = lotteryCodesModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public LotteryCodesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        LotteryCodesHolder slh = new LotteryCodesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final LotteryCodesHolder holder, final int position) {

        holder.lottery_code_txt.setText(lotteryCodesModels.get(position).getLottery_codes());


        holder.setItemClickListener(new LotteryCodesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.lotteryCodesModels.size();
    }
}
