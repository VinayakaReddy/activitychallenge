package in.activitychallenge.activitychallenge.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.fragments.MyPromotionFragment;
import in.activitychallenge.activitychallenge.holder.PromotionHolder;
import in.activitychallenge.activitychallenge.models.PromotionModel;

/**
 * Created by admin on 12/21/2017.
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionHolder> {
    MyPromotionFragment context;
    public ArrayList<PromotionModel> promoModels;
    LayoutInflater li;
    int resource;
    String datetime;

    public PromotionAdapter(ArrayList<PromotionModel> promoModels, MyPromotionFragment context, int resource) {
        this.context = context;
        this.promoModels = promoModels;
        this.resource =resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public PromotionHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layout = li.inflate(resource,parent,false);
        PromotionHolder slh = new PromotionHolder(layout);
        return slh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(PromotionHolder holder, final int position) {

        holder.amount.setText(String.valueOf(promoModels.get(position).getAmount()+" "+"\nUSD"));

        datetime =parseDateToddMMyyyy(promoModels.get(position).getCreated_on());
        holder.cre_date.setText(datetime);

        holder.name.setText(promoModels.get(position).getUser_name());
        holder.promoCode.setText(promoModels.get(position).getPromotion_code());

        holder.status.setText(promoModels.get(position).getStatus());
        if (holder.status.getText().equals("APPROVED"))
        {
            holder.pay_txt.setVisibility(View.VISIBLE);
            holder.status.setTextColor(Color.parseColor("#28df5a"));
        }
        else if (holder.status.getText().equals("PAID"))
        {
            holder.status.setTextColor(Color.parseColor("#28df5a"));
        }else {
            holder.status.setTextColor(Color.parseColor("#b3270c"));
        }

        holder.pay_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getActivity(), ChallengePaymentActivity.class);
                intent.putExtra("promotionAmount",String.valueOf(promoModels.get(position).getAmount()));
                intent.putExtra("promotion_id",promoModels.get(position).getPromotion_id());
                intent.putExtra("activity","MyPromotionFragment");
                Log.d("SendingData", String.valueOf(promoModels.get(position).getAmount()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promoModels.size();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
