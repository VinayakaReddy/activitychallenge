package in.activitychallenge.activitychallenge.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.SpecialEvent;
import in.activitychallenge.activitychallenge.activities.SpecialEventMapActivity;
import in.activitychallenge.activitychallenge.activities.SpecialEventNotifications;
import in.activitychallenge.activitychallenge.activities.TakeChalengeActivity;
import in.activitychallenge.activitychallenge.holder.SpecialEventHolder;
import in.activitychallenge.activitychallenge.models.SpecialEventModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class SpecialEventAdapter extends RecyclerView.Adapter<SpecialEventHolder> implements View.OnClickListener {
    public ArrayList<SpecialEventModel> specialEventModels;
    SpecialEvent context;
    LayoutInflater li;
    int resource;
    Boolean checkInternet;
    String user_id, access_token, device_id, user_type;
    android.support.v7.app.AlertDialog descDialog;
    ProgressDialog progressDialog;
    SpecialEventHolder seh;
    UserSessionManager session;
    Dialog dialog;

    public SpecialEventAdapter(ArrayList<SpecialEventModel> specialEventModels, SpecialEvent context, int resource) {
        this.specialEventModels = specialEventModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
    }

    @Override
    public SpecialEventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        seh = new SpecialEventHolder(layout);
        return seh;
    }

    @Override
    public void onBindViewHolder(SpecialEventHolder holder, final int position) {

        final String converted_string = specialEventModels.get(position).getEventName();

        String desc = specialEventModels.get(position).getEventDescription();
        holder.event_name_txt.setText(specialEventModels.get(position).getEventName());
        holder.event_description.setText(fixedLength(desc));

        holder.event_description.setOnClickListener(this);
        holder.event_description.setTag(position);

        holder.roadmapImg.setOnClickListener(this);
        holder.roadmapImg.setTag(position);

        holder.notification.setOnClickListener(this);
        holder.notification.setTag(position);

        holder.marker.setOnClickListener(this);
        holder.marker.setTag(position);


        int follow = specialEventModels.get(position).getFollowstatus();
        int challenge = specialEventModels.get(position).getChalengestatus();
        int usefolled = specialEventModels.get(position).getUserfollowed();

        Picasso.with(context)
                .load(specialEventModels.get(position).getRoadmap())
                .placeholder(R.drawable.no_image_found)
                .into(holder.roadmapImg);

        if (usefolled == 1) {
            holder.follow_text.setText("UnFollow");
            holder.follow_text.setTextColor(Color.parseColor("#ffffff"));
            holder.follow_text.setBackgroundResource(R.drawable.bg_fill);
        } else {
            holder.follow_text.setText("Follow");
            holder.follow_text.setTextColor(Color.parseColor("#000000"));
            holder.follow_text.setBackgroundResource(R.drawable.background_for_login_and_signup);
        }

        if (user_type.equals("SPONSOR")) {
            holder.challenge_txt.setVisibility(View.GONE);
            holder.sponsor_challenge_txt.setVisibility(View.VISIBLE);
        } else {
            holder.challenge_txt.setVisibility(View.VISIBLE);
            holder.sponsor_challenge_txt.setVisibility(View.VISIBLE);
            if (challenge == 0)
                holder.challenge_txt.setVisibility(View.GONE);
            else
                holder.challenge_txt.setVisibility(View.VISIBLE);
        }

        if (follow == 1)
            holder.follow_text.setVisibility(View.VISIBLE);
        else
            holder.follow_text.setVisibility(View.GONE);


        holder.follow_text.setOnClickListener(this);
        holder.follow_text.setTag(position);
        holder.challenge_txt.setOnClickListener(this);
        holder.challenge_txt.setTag(position);


        //Sponsor Special Event
        holder.sponsor_challenge_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context);
                //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dialog_special_event);
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
                Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);
                TextView activity_name_text = (TextView) dialog.findViewById(R.id.activity_name_text);
                activity_name_text.setText(Html.fromHtml(converted_string));

                //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                sendMoneyButton.setOnClickListener(new View.OnClickListener() {

                    @Override

                    public void onClick(View v) {
                        String amt_value = edt_send_amount.getText().toString();
                        if (amt_value.equals("") || amt_value.equals("0")) {
                            Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(context, ChallengePaymentActivity.class);
                            intent.putExtra("activity", "Sponsor_special_event");
                            intent.putExtra("member_id", specialEventModels.get(position).getId());
                            intent.putExtra("member_user_type", "SPECIAL_EVENT");
                            intent.putExtra("sponsorAmount", amt_value);
                            //   Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                            context.startActivity(intent);
                        }
                    }

                });
                dialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.specialEventModels.size();
    }

    private String fixedLength(String desc) {
        String fixeddesc;
        if (desc.length() >= 30) {
            fixeddesc = desc.substring(0, 30) + "...";
        } else {
            fixeddesc = desc;
        }
        return fixeddesc;
    }

    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        if (view.getId() == R.id.special_event_dec) {
            showFullAlertDescription(pos);
        } else if (view.getId() == R.id.chalenge_txt) {
            Intent it = new Intent(context, TakeChalengeActivity.class);
            it.putExtra("eventid", specialEventModels.get(pos).getId());
            it.putExtra("startdate", specialEventModels.get(pos).getStart_on());
            it.putExtra("enddate", specialEventModels.get(pos).getEnd_on());
            it.putExtra("noofdays", specialEventModels.get(pos).getNo_of_days());
            it.putExtra("location", specialEventModels.get(pos).getLocationArr().toString());
            context.startActivity(it);
        } else if (view.getId() == R.id.close_btn) {
            descDialog.cancel();
        } else if (view.getId() == R.id.follow_txt) {
            if (specialEventModels.get(pos).getUserfollowed() == 1)
                getUnFollowEvent(pos);
            else
                getFollowingStatus(pos);
        } else if (view.getId() == R.id.marker_img) {
            Intent maploc = new Intent(context, SpecialEventMapActivity.class);
            maploc.putExtra("location", specialEventModels.get(pos).getLocationArr().toString());
            context.startActivity(maploc);

        } else if (view.getId() == R.id.challenge_notification_icon) {
            Intent it = new Intent(context, SpecialEventNotifications.class);
            it.putExtra("eventid", specialEventModels.get(pos).getId());
            context.startActivity(it);
        }
    }

    private void showFullAlertDescription(int pos) {
        // custom dialog
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View v = inflater.inflate(R.layout.customalert, null);
        TextView desc = (TextView) v.findViewById(R.id.full_desc_txt);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mp_regular));
        Button ok_btn = (Button) v.findViewById(R.id.close_btn);
        ok_btn.setOnClickListener(this);
        ok_btn.setTag(pos);
        desc.setText(specialEventModels.get(pos).getEventDescription());
        desc.setTypeface(typeface);
        View title = inflater.inflate(R.layout.customtitle_header, null);
        AppCompatTextView suggested_loc = (AppCompatTextView) title.findViewById(R.id.heading_text);
        suggested_loc.setText("Event Description");
        dialogBuilder.setCustomTitle(title);
        dialogBuilder.setView(v);


        descDialog = dialogBuilder.create();
        descDialog.setCanceledOnTouchOutside(false);
        descDialog.show();
    }


    private void getUnFollowEvent(final int pos) {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_UNFOLLOW_STATUS;
            Log.d("UnFOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {

                            specialEventModels.get(pos).setUserfollowed(0);
                            notifyDataSetChanged();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("special_event_id", specialEventModels.get(pos).getId());
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    private void getFollowingStatus(final int pos) {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.SPECIAL_EVENT_FOLLOW_STATUS;
            Log.d("FOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String sucess = jobcode.getString("success");
                        String mesage = jobcode.getString("message");
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {

                            specialEventModels.get(pos).setUserfollowed(1);
                            notifyDataSetChanged();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("special_event_id", specialEventModels.get(pos).getId());
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


}
