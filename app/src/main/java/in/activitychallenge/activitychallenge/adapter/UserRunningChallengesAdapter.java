package in.activitychallenge.activitychallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.KmChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.MinuiteChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.PictureChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.PushupChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.RunningChallengesActivity;
import in.activitychallenge.activitychallenge.activities.StepChallengeStatusActivity;
import in.activitychallenge.activitychallenge.holder.RunningChallengesHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.UserRunningChallngItemClickListener;
import in.activitychallenge.activitychallenge.models.UserRunningChallengesModel;

/**
 * Created by admin on 12/26/2017.
 */

public class UserRunningChallengesAdapter extends RecyclerView.Adapter<RunningChallengesHolder> {

    public ArrayList<UserRunningChallengesModel> userRunListModels;
    public RunningChallengesActivity context;
    public LayoutInflater li;
    public int resource;

    public UserRunningChallengesAdapter(RunningChallengesActivity context, ArrayList<UserRunningChallengesModel> userRunListModels, int resource)
    {
        this.context = context;
        this.resource  =resource;
        this.userRunListModels = userRunListModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public RunningChallengesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        RunningChallengesHolder slh = new RunningChallengesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(RunningChallengesHolder holder, final int position)
    {
        holder.acti_amount.setText(String.valueOf("$"+" "+userRunListModels.get(position).getAmount()+" "+"USD"));
        holder.acti_oponent_type.setText(userRunListModels.get(position).getOpponent_type());
        holder.acti_oponent_name.setText(userRunListModels.get(position).getOpponent_name());
        holder.acti_status.setText(userRunListModels.get(position).getStatus());
        holder.acti_name.setText(userRunListModels.get(position).getActivity_name());
        holder.acti_start.setText(userRunListModels.get(position).getStart_on());
        holder.acti_pause.setText(userRunListModels.get(position).getPaused_on());
        holder.acti_complet.setText(userRunListModels.get(position).getCompleted_on_txt());
        Picasso.with(context)
                .load(userRunListModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.acti_img);


        Log.d("der", userRunListModels.get(position).getActivity_image());


        holder.setItemClickListener(new UserRunningChallngItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                if (userRunListModels.get(pos).getEvaluation_factor().equals("STEPS"))
                {
                    Intent intent = new Intent(context, StepChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", userRunListModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", userRunListModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", userRunListModels.get(position).getOpponent_type());
                    intent.putExtra("gps", userRunListModels.get(position).getGetGps());
                    context.startActivity(intent);
                }
                else if (userRunListModels.get(pos).getEvaluation_factor().equals("DISTANCE")){
                  //  Toast.makeText(context, "KM", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, KmChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", userRunListModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", userRunListModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", userRunListModels.get(position).getOpponent_type());
                    intent.putExtra("gps", userRunListModels.get(position).getGetGps());
                    context.startActivity(intent);
                }
                else if (userRunListModels.get(pos).getEvaluation_factor().equals("TIME")){
                   // Toast.makeText(context, "MINUTE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, MinuiteChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", userRunListModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", userRunListModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", userRunListModels.get(position).getOpponent_type());
                    intent.putExtra("gps", userRunListModels.get(position).getGetGps());
                    context.startActivity(intent);
                }
                else if (userRunListModels.get(pos).getEvaluation_factor().equals("IMAGE")){
                   // Toast.makeText(context, "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PictureChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", userRunListModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", userRunListModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", userRunListModels.get(position).getOpponent_type());
                    Log.d("IntentData",userRunListModels.get(position).getActivity_id()+"\n"+userRunListModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }else if (userRunListModels.get(pos).getEvaluation_factor().equals("COUNT")) {
                    // Toast.makeText(context, "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PushupChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", userRunListModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", userRunListModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", userRunListModels.get(position).getOpponent_type());
                    Log.d("IntentData", userRunListModels.get(position).getActivity_id() + "\n" + userRunListModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return userRunListModels.size();
    }
}
