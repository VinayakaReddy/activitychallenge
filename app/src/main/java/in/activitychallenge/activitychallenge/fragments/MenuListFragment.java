
package in.activitychallenge.activitychallenge.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.HashMap;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AdvertisementActivity;
import in.activitychallenge.activitychallenge.activities.AllGroupsActivity;
import in.activitychallenge.activitychallenge.activities.AllMembersActivity;
import in.activitychallenge.activitychallenge.activities.CongratulationsActivity;
import in.activitychallenge.activitychallenge.activities.ContactUsActivity;
import in.activitychallenge.activitychallenge.activities.DeactivateAccountActivity;
import in.activitychallenge.activitychallenge.activities.EarnActivity;
import in.activitychallenge.activitychallenge.activities.HelpActivity;
import in.activitychallenge.activitychallenge.activities.MeAsSponsorActivity;
import in.activitychallenge.activitychallenge.activities.MyChallengesActivity;
import in.activitychallenge.activitychallenge.activities.MyFollowing;
import in.activitychallenge.activitychallenge.activities.MyGroupActivity;
import in.activitychallenge.activitychallenge.activities.MyProfileActivity;
import in.activitychallenge.activitychallenge.activities.MyPromotionActivity;
import in.activitychallenge.activitychallenge.activities.ReferFriendActivity;
import in.activitychallenge.activitychallenge.activities.ReportUsActivity;
import in.activitychallenge.activitychallenge.activities.TermsConditionActivity;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class MenuListFragment extends Fragment
{
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String profile_pic,plyername;
    ImageView user_profile_image;
    TextView player_user_name;
    View headerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_menu, container,false);

        user_profile_image = (ImageView) view.findViewById(R.id.user_profile_image);
        player_user_name = (TextView) view.findViewById(R.id.player_user_name);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        profile_pic = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        plyername = userDetails.get(UserSessionManager.USER_NAME);

        Log.d("PROFILENAMEPIC",profile_pic+""+plyername);


        Picasso.with(getActivity())
                .load(profile_pic)
                .placeholder(R.drawable.dummy_user_profile)
                .into(user_profile_image);
        player_user_name.setText(plyername);



        NavigationView vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);
        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Toast.makeText(getActivity(),menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                int id = menuItem.getItemId();

                if (id == R.id.home)
                {
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    startActivity(i);
                }

                if (id == R.id.won_lost)
                {
                    Intent i = new Intent(getActivity(), CongratulationsActivity.class);
                    startActivity(i);
                }

                if (id == R.id.help)
                {
                    Intent i = new Intent(getActivity(), HelpActivity.class);
                    startActivity(i);
                }

                if (id == R.id.refer_frend)
                {
                    Intent i = new Intent(getActivity(), ReferFriendActivity.class);
                    startActivity(i);
                }

                if (id == R.id.terms_and_cond)
                {
                    Intent i = new Intent(getActivity(), TermsConditionActivity.class);
                    startActivity(i);
                }

                if (id == R.id.my_profile)
                {
                    Intent i = new Intent(getActivity(), MyProfileActivity.class);
                    startActivity(i);
                }

                if (id == R.id.my_challenges)
                {
                    Intent i = new Intent(getActivity(), MyChallengesActivity.class);
                    startActivity(i);
                }

                if (id == R.id.earn_money)
                {
                    Intent i = new Intent(getActivity(), EarnActivity.class);
                    startActivity(i);
                }

                if (id == R.id.mygroup_group)
                {
                    Intent i = new Intent(getActivity(), MyGroupActivity.class);
                    startActivity(i);
                }
                if (id == R.id.all_members)
                {
                    Intent i = new Intent(getActivity(), AllMembersActivity.class);
                    startActivity(i);
                }
                if (id == R.id.all_groups)
                {
                    Intent i = new Intent(getActivity(), AllGroupsActivity.class);
                    startActivity(i);
                }
                if (id == R.id.my_following)
                {
                    Intent i = new Intent(getActivity(), MyFollowing.class);
                    startActivity(i);
                }

                if (id == R.id.my_promotion)
                {
                    Intent i = new Intent(getActivity(), MyPromotionActivity.class);
                    startActivity(i);
                }
                if (id == R.id.me_sponsor)
                {
                    Intent i = new Intent(getActivity(), MeAsSponsorActivity.class);
                    startActivity(i);
                }

                if (id == R.id.contact_us)
                {
                    Intent i = new Intent(getActivity(), ContactUsActivity.class);
                    startActivity(i);
                }

                if (id == R.id.advertisment)
                {
                    Intent i = new Intent(getActivity(), AdvertisementActivity.class);
                    startActivity(i);
                }
                if (id == R.id.report)
                {
                    Intent i = new Intent(getActivity(), ReportUsActivity.class);
                    startActivity(i);
                }

                if (id == R.id.deactive_account)
                {
                    Intent i = new Intent(getActivity(), DeactivateAccountActivity.class);
                    startActivity(i);

                }
                return false;
            }
        }) ;
        setupHeader();
        return  view ;
    }

    private void setupHeader() {
        int avatarSize = getResources().getDimensionPixelSize(R.dimen.global_menu_avatar_size);
        String profilePhoto = getResources().getString(R.string.user_profile_photo);

    }

}
