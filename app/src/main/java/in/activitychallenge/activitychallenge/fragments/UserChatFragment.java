package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.MessagesAdapter;
import in.activitychallenge.activitychallenge.models.MessagesModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

import static android.app.Activity.RESULT_OK;


public class UserChatFragment extends Fragment{

    View view;
    ImageView default_img,voice_search;
    Typeface typeface,typeface_bold;
    RecyclerView recycler_messages;
    MessagesAdapter mesageAdapter;
    ArrayList<MessagesModel> msgModalList=new ArrayList<MessagesModel>();
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    String user_id,user_type,token,device_id;
    SearchView activity_search;
    EditText searchEditText;
    String chat_type="USER";
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.fragment_user_chat, container, false);


        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_bold));

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        activity_search = (SearchView)view. findViewById(R.id.members_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText = (EditText) activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHint("Search a Member");
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon = (ImageView) activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        voice_search = (ImageView) view.findViewById(R.id.voice_search);
        Log.d("DETAILL",user_id+"//"+user_type+"//"+token+"//"+device_id);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        default_img = view.findViewById(R.id.default_img);

        recycler_messages=(RecyclerView) view.findViewById(R.id.recycler_messages);

        layoutManager = new LinearLayoutManager(getActivity());
        recycler_messages.setNestedScrollingEnabled(false);
        recycler_messages.setLayoutManager(layoutManager);
        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(mesageAdapter!=null)
                mesageAdapter.getFilter().filter(query);
                return false;
            }
        });
        activity_search.clearFocus();
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
       getMessages();

        return view;
    }

    private void getMessages()
    {
        msgModalList.clear();
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            Log.d("MSGURL", AppUrls.BASE_URL+AppUrls.GET_NOTIFICATION_MESSAGE+"?entity_id="+user_id+"&entity_type="+user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.GET_NOTIFICATION_MESSAGE+"?entity_id="+user_id+"&entity_type="+user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {
                                Log.d("MESSAGRESP",response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    pprogressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        MessagesModel msg = new MessagesModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        msg.setFrom_id(jsonObject1.getString("from_id"));
                                        msg.setFrom_name(jsonObject1.getString("from_name"));
                                        msg.setFrom_user_type(jsonObject1.getString("from_user_type"));
                                        msg.setMsg(jsonObject1.getString("msg"));
                                        msg.setIs_read(jsonObject1.getString("is_read"));
                                        msg.setSent_on_txt(jsonObject1.getString("sent_on_txt"));
                                        msg.setSent_on(jsonObject1.getString("sent_on"));
                                        msg.setProfile_pic(AppUrls.BASE_IMAGE_URL+jsonObject1.getString("profile_pic"));

                                        msgModalList.add(msg);
                                    }
                                    mesageAdapter = new MessagesAdapter(msgModalList, UserChatFragment.this, R.layout.row_messages,chat_type);
                                    recycler_messages.setAdapter(mesageAdapter);
                                }

                                if (responceCode.equals("10200")) {
                                    pprogressDialog.cancel();
                                    recycler_messages.setVisibility(View.GONE);
                                    default_img.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {
                                    pprogressDialog.cancel();
                                    recycler_messages.setVisibility(View.GONE);
                                    default_img.setVisibility(View.VISIBLE);
                                    //Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                pprogressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else
            {
           Toast.makeText(getActivity(),"No Internet Connection..!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
       // msgModalList.clear();
       // getMessages();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    mesageAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }
}
