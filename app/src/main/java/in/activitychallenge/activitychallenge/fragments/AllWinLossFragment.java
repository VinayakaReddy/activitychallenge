package in.activitychallenge.activitychallenge.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.FilterChllengesActivity;
import in.activitychallenge.activitychallenge.activities.WinLossActivity;
import in.activitychallenge.activitychallenge.adapter.WinLossChalngeAdapter;
import in.activitychallenge.activitychallenge.models.WinLossChllengeModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class AllWinLossFragment extends Fragment implements View.OnClickListener,Toolbar.OnMenuItemClickListener
{

    View view;
    ImageView  default_img;
    Typeface typeface, typeface_bold;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    RecyclerView recycler_win_los_challng;
    WinLossChalngeAdapter winlossChalangeAdapter;
    ArrayList<WinLossChllengeModel> winlossChalngeMoldelList = new ArrayList<WinLossChllengeModel>();
    RecyclerView.LayoutManager layoutManager;
    String type = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view=inflater.inflate(R.layout.fragment_all_win_loss, container, false);

        Toolbar toolbar= (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_winloss_filter);

        toolbar.setOnMenuItemClickListener(this);

        type = "ALL";
        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        default_img = (ImageView)view.findViewById(R.id.default_img);


        recycler_win_los_challng = (RecyclerView)view.findViewById(R.id.recycler_win_los_challng);
        winlossChalangeAdapter = new WinLossChalngeAdapter(winlossChalngeMoldelList, AllWinLossFragment.this, R.layout.row_recent_activity_detail);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_win_los_challng.setNestedScrollingEnabled(false);
        recycler_win_los_challng.setLayoutManager(layoutManager);

        getWinLossData(type);

        return  view;
    }

    private void getWinLossData(String type) {
        winlossChalngeMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            String url_all_members = AppUrls.BASE_URL + AppUrls.WIN_LOSS_CHALLENGES + "?user_id=" + user_id + "&user_type=" + user_type + AppUrls.TYPE + type;
            Log.d("WINLOSSURL", url_all_members);
            StringRequest req_members =  new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("RESPWINLOSS", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            if (jsonArray.length() == 0) {
                                recycler_win_los_challng.setVisibility(View.GONE);
                                default_img.setVisibility(View.VISIBLE);
                            }else {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jdataobj = jsonArray.getJSONObject(i);
                                    WinLossChllengeModel winlossData = new WinLossChllengeModel();
                                    winlossData.setChallenge_id(jdataobj.getString("challenge_id"));
                                    winlossData.setOpponent_name(jdataobj.getString("opponent_name"));
                                    winlossData.setWinning_status(jdataobj.getString("winning_status"));
                                    winlossData.setStatus(AppUrls.BASE_IMAGE_URL + jdataobj.getString("status"));
                                    winlossData.setAmount(jdataobj.getString("amount"));
                                    winlossData.setWinning_amount(jdataobj.getString("winning_amount"));
                                    winlossData.setActivity_name(jdataobj.getString("activity_name"));
                                    winlossData.setActivity_image(AppUrls.BASE_IMAGE_URL + jdataobj.getString("activity_image"));
                                    winlossData.setCompleted_on_txt(jdataobj.getString("completed_on_txt"));
                                    winlossData.setCompleted_on(jdataobj.getString("completed_on"));

                                    winlossChalngeMoldelList.add(winlossData);
                                }
                            }

                            recycler_win_los_challng.setAdapter(winlossChalangeAdapter);

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            recycler_win_los_challng.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "Invalid input.!", Toast.LENGTH_LONG).show();

                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            recycler_win_los_challng.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            // Toast.makeText(WinLossActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(req_members);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


     @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            winlossChalngeMoldelList.clear();
            String typppp = data.getStringExtra("win_type");
            type = typppp;
            getWinLossData(type);
        }
    }


    @Override
    public void onClick(View view)
    {

    }




    @Override
    public boolean onMenuItemClick(MenuItem item)
    {

        switch (item.getItemId()) {
            case R.id.filter:
                Intent intent = new Intent(getActivity(), FilterChllengesActivity.class);
                startActivityForResult(intent, 2);
                return true;
        }
        return false;
    }
}
