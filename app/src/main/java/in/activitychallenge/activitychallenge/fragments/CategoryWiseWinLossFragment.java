package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.BannerRecyclerView;
import in.activitychallenge.activitychallenge.BannerScaleHelper;
import in.activitychallenge.activitychallenge.CardAdapter;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.models.ActivityDataModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryWiseWinLossFragment extends Fragment implements View.OnClickListener,Toolbar.OnMenuItemClickListener {

    View view;
    ImageView default_img;

    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    SearchView activity_search;
    RecyclerView recylerview;
    LinearLayoutManager lManager;
   // WinLossCategoryWiseAdapter winlossCategoryWiseAdapter;
   // ArrayList<WinLossChllengeModel> winlossChalngeMoldelList = new ArrayList<WinLossChllengeModel>();
    ArrayList<ActivityDataModel> activityDataList = new ArrayList<ActivityDataModel>();
    protected SwipeRefreshLayout mSwipeLayout;
    private BannerRecyclerView mRecyclerView;
    private BannerScaleHelper mBannerScaleHelper = null;
     CardAdapter mAdapter;
    AppCompatSpinner activity_spinner;
    String acivity_drop_name;
    ArrayList<String> activity_drop_list;
    public static String flag = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_category_wise_win_loss, container, false);


       // toolbar.inflateMenu(R.menu.menu_winloss_filter);
      /*  Toolbar toolbar= (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();*/
        //toolbar.setOnMenuItemClickListener(this);

/*

        mRecyclerView = (BannerRecyclerView) view.findViewById(R.id.recyclerView);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new CardAdapter(activityDataList,CategoryWiseWinLossFragment.this);
*/




        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        default_img = (ImageView) view.findViewById(R.id.default_img);
        activity_search = (SearchView) view.findViewById(R.id.activity_search);

        activity_spinner = (AppCompatSpinner) view.findViewById(R.id.activity_spinner);

        activity_drop_list=new ArrayList<>();

        activity_search.setOnClickListener(this);
        activity_search.setIconified(false);
        activity_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity_search.setIconified(false);
            }
        });
        activity_search.clearFocus();
        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });

        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        mAdapter = new CardAdapter(activityDataList, CategoryWiseWinLossFragment.this, R.layout.row_category_wise_winloss_details);
        lManager = new LinearLayoutManager(getActivity());
        recylerview.setNestedScrollingEnabled(false);
        recylerview.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));

        getActivityData();
        activity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(activity_spinner.getSelectedItem().equals("Select Activity"))
                {

                    mAdapter.getFilter().filter("");
                }
                else
                {
                    mAdapter.getFilter().filter(activity_spinner.getItemAtPosition(activity_spinner.getSelectedItemPosition()).toString());

                }


            }

            @Override

            public void onNothingSelected(AdapterView<?> adapterView) {

                // DO Nothing here

            }

        });

        return view;
    }


    public void getActivityData() {
        activityDataList.clear();
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            String url_all_members = AppUrls.BASE_URL + AppUrls.USER_ACTIVITY_DATA  + user_id + "&user_type=" + user_type;
            Log.d("NEWWINLOSSURL", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("NEWRESPWINLOSS", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            if (jsonArray.length() == 0) {
                                default_img.setVisibility(View.GONE);
                            } else {
                                activity_drop_list.add("Select Activity");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jdataobj = jsonArray.getJSONObject(i);
                                    ActivityDataModel winlossData = new ActivityDataModel();
                                    winlossData.setActivity_name(jdataobj.getString("activity_name"));
                                    String acivity_drop_name=jdataobj.getString("activity_name");  //this value for dropdown
                                    winlossData.setTotal_challenges(jdataobj.getString("total_challenges"));
                                    winlossData.setTie_challenges(jdataobj.getString("tie_challenges"));
                                    winlossData.setWin_challenges(jdataobj.getString("win_challenges"));
                                    winlossData.setLoss_challenges(jdataobj.getString("loss_challenges"));
                                    winlossData.setTie_amount(jdataobj.getString("tie_amount"));
                                    winlossData.setWin_amount(jdataobj.getString("win_amount"));
                                    winlossData.setActivity_image(AppUrls.BASE_IMAGE_URL + jdataobj.getString("activity_image"));
                                    winlossData.setLoss_amount(jdataobj.getString("loss_amount"));
                                    winlossData.setTotal_amount(jdataobj.getString("total_amount"));
                                    activity_drop_list.add(acivity_drop_name); //this value for dropdown
                                    activityDataList.add(winlossData);
                                    Log.d("dfgjdfnbgdf",activityDataList.toString());
                                }

                            }
  /*spinner values adapter*/ activity_spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, activity_drop_list));
                            recylerview.setAdapter(mAdapter);   //setting addpeter into recyclerview

                          /*  mBannerScaleHelper = new BannerScaleHelper();
                           // mBannerScaleHelper.setFirstItemPos(1000);
                            mBannerScaleHelper.attachToRecyclerView(mRecyclerView);
                          // recylerview.setAdapter(winlossCategoryWiseAdapter);*/

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();

                            Toast.makeText(getActivity(), "Invalid input.!", Toast.LENGTH_LONG).show();

                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();

                            // Toast.makeText(WinLossActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(req_members);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public boolean onMenuItemClick(MenuItem item)
    {


            return false;




    }



}


