package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.utilities.AppUrls;

public class ContactusFragment extends Fragment {

   View view;
    Typeface typeface, typeface_bold;
    WebView browser;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    public ContactusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_contactus, container, false);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        typeface = Typeface.createFromAsset(this.getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface_bold = Typeface.createFromAsset(this.getActivity().getAssets(), getResources().getString(R.string.mp_bold));

        browser = (WebView)view. findViewById(R.id.webVw);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setWebViewClient(new MyBrowser());
        browser.loadUrl(AppUrls.BASE_IMAGE_URL + AppUrls.CONTACT_US_LINK);


        return view;
    }
     class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
