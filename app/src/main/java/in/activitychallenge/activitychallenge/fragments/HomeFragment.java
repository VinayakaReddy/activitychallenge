package in.activitychallenge.activitychallenge.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.activitychallenge.activitychallenge.R;

public class HomeFragment extends Fragment
{

     View view;
    RecyclerView main_categ_recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view=inflater.inflate(R.layout.fragment_home, container, false);


        return  view;
    }

}
