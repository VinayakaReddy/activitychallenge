package in.activitychallenge.activitychallenge.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllGroupsActivity;
import in.activitychallenge.activitychallenge.activities.AllMembersActivity;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.activities.SponsorAsActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeAsASponsorFragment extends Fragment implements View.OnClickListener {

    View view;
    Button sponsor_user, sponsor_group,sponsor_app,sponsor_activity;
    Dialog dialog;

    public MeAsASponsorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

       view=  inflater.inflate(R.layout.fragment_me_as_asponsor, container, false);

        sponsor_user = (Button) view.findViewById(R.id.sponsor_user);
        sponsor_user.setOnClickListener(MeAsASponsorFragment.this);

        sponsor_group = (Button)view. findViewById(R.id.sponsor_group);
        sponsor_group.setOnClickListener(MeAsASponsorFragment.this);

        sponsor_app = (Button)view. findViewById(R.id.sponsor_app);
        sponsor_app.setOnClickListener(MeAsASponsorFragment.this);

        sponsor_activity = (Button)view. findViewById(R.id.sponsor_activity);
        sponsor_activity.setOnClickListener(MeAsASponsorFragment.this);

       return  view;
    }

    @Override
    public void onClick(View v)
    {
        if (v == sponsor_user) {
            Intent intent = new Intent(getActivity(), AllMembersActivity.class);
            startActivity(intent);
        }

        if (v == sponsor_group) {
            Intent intent = new Intent(getActivity(), AllGroupsActivity.class);
            startActivity(intent);
        }
        if (v == sponsor_activity) {
            Intent intent = new Intent(getActivity(), SponsorAsActivity.class);
            startActivity(intent);
        }

        if (v == sponsor_app)
        {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_sponsor_app);
            final EditText edt_send_amount = (EditText) dialog.findViewById(R.id.edt_send_amount);
            final Button sendMoneyButton = (Button) dialog.findViewById(R.id.sendMoneyButton);

            sendMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();

                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_send_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                        sendWalletAmount(send_amount);

                    }
                }
            });

            dialog.show();
        }

    }
    private void sendWalletAmount(String send_amount)
    {
        Intent intent = new Intent(getActivity(), ChallengePaymentActivity.class);
        intent.putExtra("activity", "MeAsaSponsorAPP");

        intent.putExtra("sponsorAmount", send_amount);
        Log.d("SPONSOR",  send_amount);
        startActivity(intent);
    }
}
