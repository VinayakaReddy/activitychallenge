package in.activitychallenge.activitychallenge.fragments;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividCompletAdapter;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividRunAdapter;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividUpcomAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.IndividualCompletedChallengesDB;
import in.activitychallenge.activitychallenge.utilities.IndividualRunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.IndividualUpcomingChallengesDB;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.SpecialEventCompletedChallengesDB;
import in.activitychallenge.activitychallenge.utilities.SpecialEventRunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.SpecialEventUpComimgChallengesDB;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.

 */
public class SpecialEventFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    View view;
    ImageView no_data_image;
    RecyclerView recycler_specialevent_running,recycler_specialevent_upcoming,recycler_specialevent_completed;
    LinearLayoutManager layoutManager,layoutManager1,layoutManager2;
    GridLayoutManager gridLayoutManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token,user_id,user_type,device_id;
    UserSessionManager userSessionManager;
    Typeface typeface,typeface2;
    //Running Adapter
    ChallengeIndividRunAdapter challengIndvidRunAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidRunList = new ArrayList<ChallengeIndividualRunningModel>();
    //Upcoming Adapter
    ChallengeIndividUpcomAdapter challengIndvidUppAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidUpcomList = new ArrayList<ChallengeIndividualRunningModel>();
    //Complet Adapter
    ChallengeIndividCompletAdapter challengIndvidCompletAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidCompletcomList = new ArrayList<ChallengeIndividualRunningModel>();

    LinearLayout ll_running,ll_upcoming,ll_completed,linear_sponsor, linear_chllenge, linear_messges;
    TextView running_more_text,upcoming_more_text,complet_more_text,sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    SpecialEventRunningChallengesDB specialEventRunningChallengesDB;
    SpecialEventUpComimgChallengesDB specialEventUpComimgChallengesDB;
    SpecialEventCompletedChallengesDB specialEventCompletedChallengesDB;

    Intent myGpsService;
    ContentValues values,values1,values2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_special_event, container, false);
        specialEventRunningChallengesDB = new SpecialEventRunningChallengesDB(getActivity());
        specialEventUpComimgChallengesDB = new SpecialEventUpComimgChallengesDB(getActivity());
        specialEventCompletedChallengesDB = new SpecialEventCompletedChallengesDB(getActivity());
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        initGpsListeners();
        no_data_image=(ImageView) view.findViewById(R.id.no_data_image);

        ll_running=(LinearLayout)view.findViewById(R.id.ll_running);
        ll_upcoming=(LinearLayout)view.findViewById(R.id.ll_upcoming);
        ll_completed=(LinearLayout)view.findViewById(R.id.ll_completed);

        sponsor_text = (TextView) view.findViewById(R.id.sponsor_text);
        challenge_text = (TextView) view.findViewById(R.id.challenge_text);
        mesages_text = (TextView) view.findViewById(R.id.mesages_text);

        count_sponsor_text = (TextView) view.findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) view.findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) view.findViewById(R.id.count_mesages_text);

        linear_sponsor = (LinearLayout) view.findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) view.findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) view.findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);

        running_more_text = (TextView) view.findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        upcoming_more_text = (TextView) view.findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        complet_more_text = (TextView) view.findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);

        recycler_specialevent_running=(RecyclerView)view.findViewById(R.id.recycler_specialevent_running);
        recycler_specialevent_upcoming=(RecyclerView)view.findViewById(R.id.recycler_specialevent_upcoming);
        recycler_specialevent_completed=(RecyclerView)view.findViewById(R.id.recycler_specialevent_completed);
         getCount();
        return view;
    }

    private void initGpsListeners()
    {
        myGpsService = new Intent(getActivity(), GpsService.class);
        getActivity().startService(myGpsService);

    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {


            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {

        }
    }

    @Override
    public void onClick(View view) {

    }
}
