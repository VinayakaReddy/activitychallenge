package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.filters.MyAllGroupFilterList;
import in.activitychallenge.activitychallenge.holder.MyGroupHolder;
import in.activitychallenge.activitychallenge.holder.MyGroupHorizontalHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;
import in.activitychallenge.activitychallenge.models.AllGroupsModel;
import in.activitychallenge.activitychallenge.models.MyGroupModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class AllGroupsFragment extends Fragment implements  View.OnClickListener
{

    View view;
    ImageView  grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5,no_data_image;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id,user_type;
    UserSessionManager session;
    SearchView searchMyGroup;
    RecyclerView my_group_recylerview,my_group_recylerview_horozontal;
    LinearLayoutManager lManager;
    ArrayList<AllGroupsModel> myGlist = new ArrayList<AllGroupsModel>();
    ArrayList<MyGroupModel> myGlisthorizontal = new ArrayList<MyGroupModel>();
    MyGroupAdapter myGroAdap;
    MyGroupAdapterHorizontal myGroupAdapterHorizontal;

    TextView no_group_text;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_all_groups, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
     //   toolbar_title.setText("All Groups");
        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SSDERGNDATA", user_id + "\n" + access_token + "\n" + device_id);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        grp_img_1 = (ImageView)view. findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) view.findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) view.findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) view.findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) view.findViewById(R.id.grp_img_5);

        no_data_image = (ImageView) view.findViewById(R.id.no_data_image);

        no_group_text=(TextView)view.findViewById(R.id.no_groups_text);



        searchMyGroup = (SearchView) view.findViewById(R.id.searchMyGroup);

        searchMyGroup.setIconified(false);
        searchMyGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMyGroup.setIconified(false);
            }
        });
        searchMyGroup.clearFocus();
        searchMyGroup.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                myGroAdap.getFilter().filter(query);
                return false;
            }
        });

        my_group_recylerview = (RecyclerView) view.findViewById(R.id.my_group_recylerview);
        my_group_recylerview.setNestedScrollingEnabled(false);
        myGroAdap = new MyGroupAdapter(AllGroupsFragment.this, myGlist, R.layout.row_all_groups);
        lManager = new LinearLayoutManager(getActivity());
        my_group_recylerview.setLayoutManager(lManager);

        //  MyGroupAdapterHorizontal myGroupAdapterHorizontal;//my_group_recylerview_horozontal//myGlisthorizontal

        my_group_recylerview_horozontal = (RecyclerView) view.findViewById(R.id.my_group_recylerview_horozontal);
        my_group_recylerview_horozontal.setHasFixedSize(true);
        my_group_recylerview_horozontal.setNestedScrollingEnabled(false);
        myGroupAdapterHorizontal = new MyGroupAdapterHorizontal(AllGroupsFragment.this, myGlisthorizontal, R.layout.row_mygroups_horizontal);
        my_group_recylerview_horozontal.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false));



        getAllGroups();
        getMyGroupsHorizontal();

        return view;
    }

    @Override
    public void onClick(View view) {

    }

    private void getAllGroups() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Log.d("GetGroupUrl", AppUrls.BASE_URL + AppUrls.GET_GROUPS);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_GROUPS,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();


                            Log.d("GetGroupUrlResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    //  Toast.makeText(AllGroupsActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();

                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        AllGroupsModel rhm = new AllGroupsModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);

                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setAdminId(itemArray.getString("admin_id"));
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setGroup_pic(itemArray.getString("group_pic"));
                                        rhm.setGroup_members(itemArray.getString("group_members") + " " + "members");
                                        rhm.setTotal_win(itemArray.getString("total_win"));
                                        rhm.setTotal_loss(itemArray.getString("total_loss"));
                                        rhm.setOverall_rank(itemArray.getString("overall_rank"));
                                        rhm.setWinning_per(itemArray.getString("winning_per") + " " + "%");

                                        rhm.setImg1(itemArray.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        rhm.setImg2(itemArray.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        rhm.setImg3(itemArray.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        rhm.setImg4(itemArray.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        rhm.setImg5(itemArray.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));


                                        myGlist.add(rhm);
                                    }
                                    my_group_recylerview.setAdapter(myGroAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Invalid Input", Toast.LENGTH_SHORT).show();

                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    no_data_image.setVisibility(View.VISIBLE);
                                    // Toast.makeText(AllGroupsActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MyGroup_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strRe);

        } else {
            pprogressDialog.cancel();
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void getMyGroupsHorizontal()
    {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + "group/user?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("UUUUU", url);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/user?user_id=" + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GetMYGroupUrlResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    // Toast.makeText(MyGroupActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        MyGroupModel rhm = new MyGroupModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setGroup_pic(itemArray.getString("group_pic"));
                                        rhm.setGroup_members(itemArray.getString("group_members") + " " + "members");
                                        rhm.setTotal_win(itemArray.getString("total_win"));
                                        rhm.setTotal_loss(itemArray.getString("total_loss"));
                                        rhm.setOverall_rank(itemArray.getString("overall_rank"));
                                        rhm.setWinning_per(itemArray.getString("winning_per") + " " + "%");
                                        rhm.setImg1(itemArray.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        rhm.setImg2(itemArray.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        rhm.setImg3(itemArray.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        rhm.setImg4(itemArray.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        rhm.setImg5(itemArray.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        myGlisthorizontal.add(rhm);
                                    }
                                    //  MyGroupAdapterHorizontal myGroupAdapterHorizontal;//my_group_recylerview_horozontal/
                                    my_group_recylerview_horozontal.setAdapter(myGroupAdapterHorizontal);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    // no_data_image.setVisibility(View.VISIBLE);
                                    no_group_text.setVisibility(View.VISIBLE);
                                    // Toast.makeText(AllGroupsActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MyGRPP_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public class MyGroupAdapter extends RecyclerView.Adapter<MyGroupHolder> implements Filterable {
        AllGroupsFragment context;
        LayoutInflater lInfla;
        int resource;
        public ArrayList<AllGroupsModel> myList;
        ArrayList<AllGroupsModel> filterlist;
        MyAllGroupFilterList filter;

        public MyGroupAdapter(AllGroupsFragment context, ArrayList<AllGroupsModel> myList, int resource) {
            this.context = context;
            this.myList = myList;
            this.resource = resource;
            this.filterlist = myList;
            lInfla = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = lInfla.inflate(resource, null);
            MyGroupHolder slh = new MyGroupHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(MyGroupHolder holder, final int position) {
            String str = myList.get(position).getName();
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.grp_name.setText(converted_string);
            holder.grp_memb_cnt.setText(myList.get(position).getGroup_members());
            holder.grp_lost_cnt.setText(myList.get(position).getTotal_loss());
            holder.grp_won_cnt.setText(myList.get(position).getTotal_win());
            holder.grp_percent.setText(myList.get(position).getWinning_per());
            holder.grp_rank.setText(myList.get(position).getOverall_rank());
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .resize(90, 90)
                    .into(holder.grp_img);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg1())
                    .into(holder.grp_img_1);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg2())
                    .into(holder.grp_img_2);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg3())
                    .into(holder.grp_img_3);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg4())
                    .into(holder.grp_img_4);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg5())
                    .into(holder.grp_img_5);


            holder.setItemClickListener(new MyGroupItemClickListener() {
                @Override
                public void onItemClick(View view, int layoutPosition) {
                    Intent ii = new Intent(getActivity(), GroupDetailActivity.class);
                    ii.putExtra("grp_id", myList.get(position).getId());
                    ii.putExtra("grp_name", myList.get(position).getName());
                    ii.putExtra("grp_admin_id", myList.get(position).getAdminId());
                    ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_NOT_USER");

                    startActivity(ii);
                }
            });


        }

        @Override
        public int getItemCount() {
            return myList.size();
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new MyAllGroupFilterList(filterlist, this);
            }

            return filter;
        }


    }

    ////horizontal Adapter

    public class MyGroupAdapterHorizontal extends RecyclerView.Adapter<MyGroupHorizontalHolder>  {
        AllGroupsFragment context;
        LayoutInflater lInfla;
        int resource;
        public ArrayList<MyGroupModel> myList;



        public MyGroupAdapterHorizontal(AllGroupsFragment context, ArrayList<MyGroupModel> myList, int resource)
        {
            this.context = context;
            this.myList = myList;
            this.resource = resource;

            lInfla = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MyGroupHorizontalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = lInfla.inflate(resource, null);
            MyGroupHorizontalHolder slh = new MyGroupHorizontalHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(MyGroupHorizontalHolder holder, final int position) {
            String str = myList.get(position).getName();
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.grp_name.setText(converted_string);

            holder.grp_rank.setText(myList.get(position).getOverall_rank());
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    .resize(90, 90)
                    .into(holder.grp_img);



            holder.setItemClickListener(new MyGroupItemClickListener() {
                @Override
                public void onItemClick(View view, int layoutPosition) {
                    Intent ii = new Intent(getActivity(), GroupDetailActivity.class);
                    ii.putExtra("grp_id", myList.get(position).getId());
                    ii.putExtra("grp_name", myList.get(position).getName());
                    ii.putExtra("grp_admin_id", myList.get(position).getAdmin_id());
                    ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_NOT_USER");

                    startActivity(ii);
                }
            });


        }

        @Override
        public int getItemCount() {
            return myList.size();
        }




    }
}
