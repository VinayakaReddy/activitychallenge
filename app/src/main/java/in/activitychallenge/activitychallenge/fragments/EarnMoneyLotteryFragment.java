package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.ChallengePaymentActivity;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.activities.PaypalActivity;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class EarnMoneyLotteryFragment extends Fragment implements View.OnClickListener {
    View view;
    Spinner spinner_tkt;
    ImageView default_img;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    TextView total_tkt_amount, lottry_amount, minus_butt, tkkt_count, add_butt, send_report_button, text_only_tickets;

//    String[] tkt_count = { "0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50" };
    String user_id,user_type,access_token, device_id, id, for_month, status, created_on_txt, created_on="",lot_amount,ress,ref_txn_id,wallet_amount;

    int lottery_amount,per_tkt_cost,amount_collected,result;
    int qty = 0;
    LinearLayout ll_t_amount,ll_t_ticket,ll_t_buy_tickets;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_earn_money_lottery, container, false);

        session = new UserSessionManager(getContext());
        HashMap<String,String>userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(getContext());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        text_only_tickets = (TextView) view.findViewById(R.id.text_only_tickets);
        send_report_button = (TextView) view.findViewById(R.id.send_report_button);
        send_report_button.setOnClickListener(this);


        default_img = (ImageView) view.findViewById(R.id.default_img);

        ll_t_amount = (LinearLayout) view.findViewById(R.id.ll_t_amount);
        ll_t_ticket = (LinearLayout) view.findViewById(R.id.ll_t_ticket);
        ll_t_buy_tickets = (LinearLayout) view.findViewById(R.id.ll_t_buy_tickets);





        spinner_tkt = (Spinner) view.findViewById(R.id.spinner_tkt);
        total_tkt_amount = (TextView) view.findViewById(R.id.total_tkt_amount);
        lottry_amount = (TextView) view.findViewById(R.id.lottry_amount);

        minus_butt = (TextView) view.findViewById(R.id.minus_butt);
        minus_butt.setOnClickListener(this);
        tkkt_count = (TextView) view.findViewById(R.id.tkkt_count);
        add_butt = (TextView) view.findViewById(R.id.add_butt);
        add_butt.setOnClickListener(this);

//        ArrayAdapter<String> adapter= new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item , tkt_count);
//        spinner_tkt.setAdapter(adapter);
//
//        spinner_tkt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                // Get select item
//                int sid=spinner_tkt.getSelectedItemPosition();
//                int fu = Integer.parseInt(tkt_count[sid]);
//                result = fu * per_tkt_cost;
//                String ress = String.valueOf(result);
//
//                total_tkt_amount.setText("$"+" "+ ress + " " + "USD");
//                try {
//                    Double val = Double.valueOf(result);
//                    Double final_res = val * 0.6;
//                    Double val1 = Double.valueOf(lottery_amount);
//                    Double reso = val1 + final_res;
//
//
//                    String str = String.valueOf(reso);
//
////                    if (str.equals("0")){
////                        total_tkt_amount.setVisibility(View.GONE);
////                    }
////                    else {
////                        total_tkt_amount.setVisibility(View.VISIBLE);
////                    }
//
//                    lottry_amount.setText("$"+" "+ str + " " + "USD");
//                }catch (Exception e){
//
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent)
//            {
//            }
//
//
//        });


        getLottery();


        return view;
    }



    private void getLottery()
    {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet)
        {

                Log.d("LOTTYURL",AppUrls.BASE_URL + AppUrls.LOTTERY);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LOTTERY,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {

                                Log.d("LOTTERRESP", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("response_code");
                                    if (successResponceCode.equals("10100"))
                                    {
                                        pprogressDialog.dismiss();
                                      //  Toast.makeText(getContext(), "Data fetch successfully", Toast.LENGTH_SHORT).show();

                                        JSONArray jArr = jsonObject.getJSONArray("data");

                                            JSONObject itemArray = jArr.getJSONObject(0);
                                            id = itemArray.getString("id");
                                            for_month = itemArray.getString("for_month");

                                            per_tkt_cost = itemArray.getInt("per_ticket_price");
                                            String tkt_cos = String.valueOf(per_tkt_cost);
                                            text_only_tickets.setText("$"+" "+ tkt_cos + " " + "USD");

                                            lottery_amount = itemArray.getInt("lottery_amount");
                                            lot_amount  = String.valueOf(lottery_amount);
                                        //    lottry_amount.setText(lot_amount + "\n" + "USD");


                                            status = itemArray.getString("status");
                                            amount_collected = itemArray.getInt("amount_collected");
                                           lottry_amount.setText(amount_collected + "\n" + "USD");
                                            created_on_txt = itemArray.getString("cre ated_on_txt");
                                            created_on = itemArray.getString("created_on");

                                            Log.d("resssrs",id+""+for_month+""+per_tkt_cost+""+lottery_amount+""+status+""+amount_collected+""+created_on_txt+""+created_on);



                                    }
                                    if (successResponceCode.equals(" 10200"))
                                    {
                                        pprogressDialog.dismiss();
                                        default_img.setVisibility(View.VISIBLE);
                                        ll_t_amount.setVisibility(View.GONE);
                                        ll_t_ticket.setVisibility(View.GONE);
                                        ll_t_buy_tickets.setVisibility(View.GONE);
                                        Toast.makeText(getContext(), "No Lottery Amount For this Month..!", Toast.LENGTH_SHORT).show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pprogressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token",access_token);
                        headers.put("x-device-id",device_id);
                        headers.put("x-device-platform","ANDROID");

                        Log.d("REPADER", "HEADDER "+headers.toString());
                        return headers;
                    }

                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(stringRequest);
        }
        else {
            pprogressDialog.cancel();
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onClick(View view)
    {
        if (view==add_butt)
        {
            String b = tkkt_count.getText().toString();
            int c = Integer.parseInt(b);
            c = c + 1;
            tkkt_count.setText(String.valueOf(c));

            result = c * per_tkt_cost;
            if (result != 0){
                total_tkt_amount.setVisibility(View.VISIBLE);
                send_report_button.setVisibility(View.VISIBLE);
            }else {
                total_tkt_amount.setVisibility(View.GONE);
                send_report_button.setVisibility(View.GONE);
            }
            ress = String.valueOf(result);
            total_tkt_amount.setText("$"+" "+ ress + " " + "USD");

            try {
                Double val = Double.valueOf(result);
                Double final_res = val * 0.6;
                Double val1 = Double.valueOf(amount_collected);
                Double reso = val1 + final_res;

                String str = String.valueOf(reso);

                lottry_amount.setText("$"+" "+ str + " " + "USD");
            }catch (Exception e){
            }

            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_around_center_point);
            lottry_amount.startAnimation(animation);




        }

        if (view==minus_butt){
                String b = tkkt_count.getText().toString();
                int c = Integer.parseInt(b);
            if (!(c <= 0)){
                c = c - 1;
                tkkt_count.setText(String.valueOf(c));

                result = c * per_tkt_cost;
                if (result != 0){
                    total_tkt_amount.setVisibility(View.VISIBLE);
                    send_report_button.setVisibility(View.VISIBLE);
                }else {
                    total_tkt_amount.setVisibility(View.GONE);
                    send_report_button.setVisibility(View.GONE);
                }
                ress = String.valueOf(result);
                total_tkt_amount.setText("$" + " " + ress + " " + "USD");
            } else {
                    tkkt_count.setText("0");
                }

            try {
                Double val = Double.valueOf(result);
                Double final_res = val * 0.6;
                Double val1 = Double.valueOf(amount_collected);
                Double reso = val1 + final_res;


                String str = String.valueOf(reso);

                lottry_amount.setText("$"+" "+ str + " " + "USD");
            }catch (Exception e){

            }

            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_around_center_point_r);
            lottry_amount.startAnimation(animation);

        }

        if(view == send_report_button){

            checkInternet = NetworkChecking.isConnected(getActivity());
            if (checkInternet) {
                pprogressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.EARN_MONEY_PAY,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Log.d("REPORTRESP", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String success = jsonObject.getString("success");
                                    String message = jsonObject.getString("message");
                                    String response_code = jsonObject.getString("response_code");
                                    if (response_code.equals("10100")) {
                                        pprogressDialog.dismiss();
                                        JSONObject jobj = jsonObject.getJSONObject("data");
                                        ref_txn_id = jobj.getString("ref_txn_id");
                                        wallet_amount = jobj.getString("amount");
                                       // Toast.makeText(getActivity(), "Redirecting to Paypal Payment page..!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(),ChallengePaymentActivity.class);
                                        intent.putExtra("activity","EarnMoney");
                                        intent.putExtra("ref_txn_id",ref_txn_id);
                                        intent.putExtra("wallet_amount",wallet_amount);
                                        intent.putExtra("paymentAmount",ress);
                                        startActivity(intent);
                                    }
                                    if (response_code.equals("10200"))
                                    {
                                        pprogressDialog.dismiss();
                                    //    Toast.makeText(getActivity(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pprogressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token",access_token);
                        headers.put("x-device-id",device_id);
                        headers.put("x-device-platform","ANDROID");

                        Log.d("REPORT_HEADER", "HEADDER "+headers.toString());
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put("user_type", user_type);
                        params.put("txn_flag", "LOTTORY_AMT");
                        params.put("amount", ress);
                        params.put("extra_info", id);

                        Log.d("PAY_PARAM:", "PARMS" + params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            }else {
                pprogressDialog.cancel();
                Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }



}



