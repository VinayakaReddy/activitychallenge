package in.activitychallenge.activitychallenge.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clock.scratch.ScratchView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.CardPageActivity;
import in.activitychallenge.activitychallenge.activities.ScratchCardHistory;
import in.activitychallenge.activitychallenge.activities.SportsPromoVideolist;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class EarnMoneyScratchFragment extends Fragment implements View.OnClickListener {

    View view;
    Button promo_butt,scratch_history;
    Typeface typeface,typeface2;
    TextView status_monthly_card_text,status_weekely_card_text,week_availabel_date_text,month_availabel_date_text,is_scratch_view_week_text,is_scratch_view_month_text;
    ScratchView scratch_view_weekly,scratch_view_monthly;
    ImageView monthlycard_image,weeklycard_imag;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    String user_id,user_type,token,device_id,month_amount,week_amount,month_videoUrl,month_video_id,week_videoUrl,week_video_id;
    CardView frame_month,frame_week;
    DisplayMetrics dm;
    MediaController media_Controller;
    // Dialog dialog;
    private InterstitialAd mInterstitialAd;
    String android_id;
    AdRequest adRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=   inflater.inflate(R.layout.fragment_earn_money_scratch, container, false);
        MobileAds.initialize(getContext(), getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(getContext());
     /*   mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();*/


        initializeInterstitialAd();
       loadInterstitialAd();

        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_bold));

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        Log.d("DETAILL",user_id+"//"+user_type+"//"+token+"//"+device_id);


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        promo_butt = (Button) view.findViewById(R.id.promo_butt);
        promo_butt.setOnClickListener(this);

        scratch_history = (Button) view.findViewById(R.id.scratch_history);
        scratch_history.setOnClickListener(this);
//,
        status_monthly_card_text=(TextView)view.findViewById(R.id.status_monthly_card_text);
        status_monthly_card_text.setTypeface(typeface);

        status_weekely_card_text=(TextView)view.findViewById(R.id.status_weekely_card_text);
        status_weekely_card_text.setTypeface(typeface);

        month_availabel_date_text=(TextView)view.findViewById(R.id.month_availabel_date_text);
        month_availabel_date_text.setTypeface(typeface);
        week_availabel_date_text=(TextView)view.findViewById(R.id.week_availabel_date_text);
        week_availabel_date_text.setTypeface(typeface);

        frame_month=(CardView) view.findViewById(R.id.frame_month);
        frame_month.setOnClickListener(this);
        frame_week=(CardView)view.findViewById(R.id.frame_week);
        frame_week.setOnClickListener(this);

        is_scratch_view_week_text=(TextView)view.findViewById(R.id.is_scratch_view_week_text);
        is_scratch_view_week_text.setTypeface(typeface);

        is_scratch_view_month_text=(TextView)view.findViewById(R.id.is_scratch_view_month_text);
        is_scratch_view_month_text.setTypeface(typeface);

        // getScratchCardData();



        return view;
    }

    private void getScratchCardData()
    {
     //http://192.168.1.61:8090/api/v1/payment/scratch_card?user_id=59f03caf1f3c56e80e000029&user_type=USER

     Log.d("SCRATCHCARDURL", AppUrls.BASE_URL+AppUrls.SCRATCH_CARD+"?user_id="+user_id+"&user_type="+user_type);
        checkInternet = NetworkChecking.isConnected(getActivity());

        if (checkInternet)
        {                         //http://192.168.1.61:8090/api/v1/payment/scratch_card?user_id=5a2a92f3bc8d0f1264fbe497&user_type=USER
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET,AppUrls.BASE_URL+AppUrls.SCRATCH_CARD+"?user_id="+user_id+"&user_type="+user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("SCRATC",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if(successResponceCode.equals("10100"))
                                {
                                    progressDialog.dismiss();

                                    JSONObject jsonObjData = jsonObject.getJSONObject("data");
                                    Log.d("DATAOBJ",jsonObjData.toString());

                                    JSONObject jobMonthly=jsonObjData.getJSONObject("monthly_sc");//for monthly card

                                    final  String month_sratchcard_id=jobMonthly.getString("id");
                                     month_amount=jobMonthly.getString("amount");
                                    String month_is_scratched=jobMonthly.getString("is_scratched");
                                    final   String month_scratching_date=jobMonthly.getString("scratching_date");
                                    final String scratching_month_date=parseDateToddMMyyyy(month_scratching_date);
                                    final   String month_view=jobMonthly.getString("view");
                                    month_videoUrl=jobMonthly.getString("video_path");


                                    JSONObject jobWeekly=jsonObjData.getJSONObject("weekly_sc");//for weekly card

                                    final  String week_sratchcard_id=jobWeekly.getString("id");
                                    week_amount=jobWeekly.getString("amount");
                                    String week_is_scratched=jobWeekly.getString("is_scratched");
                                    final   String week_scratching_date=jobWeekly.getString("scratching_date");
                                    final String scratching_week_date=parseDateToddMMyyyy(week_scratching_date);
                                    final  String week_view=jobWeekly.getString("view");
                                    week_videoUrl=jobWeekly.getString("video_path");



                                   if(month_is_scratched.equals("0")) //already scratch done or not condition check  if 0 then not scratch if 1 then already scratch
                                   {
                                       if (month_view.equals("0")) {
                                           month_availabel_date_text.setVisibility(View.VISIBLE);
                                           month_availabel_date_text.setText("Available on \n" + scratching_month_date);
                                           frame_month.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {

                                               }
                                           });

                                       } else {
                                           frame_month.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View view) {
                                                   if (month_videoUrl.equals(""))
                                                   {
                                                       if (mInterstitialAd.isLoaded())
                                                       {
                                                           mInterstitialAd.show();
                                                           mInterstitialAd.setAdListener(new AdListener()
                                                           {
                                                               // Listen for when user closes ad
                                                               public void onAdClosed() {
                                                                   Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                                   sc_history.putExtra("SCRATCH", "MONTH");
                                                                   sc_history.putExtra("AMOUNT", month_amount);
                                                                   sc_history.putExtra("SCRATCH_ID", month_sratchcard_id);
                                                                   startActivity(sc_history);
                                                               }
                                                           });

                                                       } else {
                                                           Log.d("TAG", "The interstitial wasn't loaded yet.");
                                                           Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                           sc_history.putExtra("SCRATCH", "MONTH");
                                                           sc_history.putExtra("AMOUNT", month_amount);
                                                           sc_history.putExtra("SCRATCH_ID", month_sratchcard_id);
                                                           startActivity(sc_history);
                                                       }

                                                   }
                                                   else
                                                   {
                                                   final Dialog dialog = new Dialog(getActivity());// add here your class name
                                                   dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                   dialog.setContentView(R.layout.scratch_custom_videoview_dialog);
                                                final   VideoView scratch_vid_view = (VideoView) dialog.findViewById(R.id.scratch_vid_view);

                                                   dialog.show();

                                                       media_Controller = new MediaController(getActivity());
                                                       dm = new DisplayMetrics();
                                                       getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                                                       int height = dm.heightPixels;
                                                       int width = dm.widthPixels;
                                                       scratch_vid_view.setMinimumWidth(width);
                                                       scratch_vid_view.setMinimumHeight(height);
                                                       String uriPath = AppUrls.BASE_IMAGE_URL + month_videoUrl;
                                                       try {
                                                           Uri video = Uri.parse(uriPath);
                                                           scratch_vid_view.setVideoURI(video);
                                                           scratch_vid_view.setMediaController(media_Controller);
                                                       } catch (Exception e) {
                                                           Log.e("Error", e.getMessage());
                                                           e.printStackTrace();
                                                       }

                                                       scratch_vid_view.requestFocus();
                                                       scratch_vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                           // Close the progress bar and play the video
                                                           public void onPrepared(MediaPlayer mp) {

                                                               scratch_vid_view.start();

                                                           }
                                                       });

                                                   dialog.setCanceledOnTouchOutside(false);

                                                   scratch_vid_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                       @Override
                                                       public void onCompletion(MediaPlayer mediaPlayer) {
                                                           dialog.dismiss();
                                                           Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                           sc_history.putExtra("SCRATCH", "MONTH");
                                                           sc_history.putExtra("AMOUNT", month_amount);
                                                           sc_history.putExtra("SCRATCH_ID", month_sratchcard_id);
                                                           startActivity(sc_history);
                                                       }
                                                   });
                                                  }
                                               }
                                           });

                                       }
                                   }
                                   else
                                   {
                                       is_scratch_view_month_text.setVisibility(View.VISIBLE);
                                       is_scratch_view_month_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + month_amount + "</b>"));

                                       frame_month.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {

                                           }
                                       });
                                   }


                                   if(week_is_scratched.equals("0")) ////already scratch done or not condition check  if 0 then not scratch if 1 then already scratch
                                   {
                                       if (week_view.equals("0"))
                                       {
                                           week_availabel_date_text.setVisibility(View.VISIBLE);
                                           week_availabel_date_text.setText("Available on \n" + scratching_week_date);
                                           frame_week.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {

                                               }
                                           });
                                       } else {
                                           frame_week.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view)
                                               {
                                                   if(week_videoUrl.equals(""))
                                                   {
                                                       if (mInterstitialAd.isLoaded())
                                                       {
                                                           Log.d("wwwwwww", "The interstitial loaded.");
                                                           mInterstitialAd.show();
                                                           mInterstitialAd.setAdListener(new AdListener()
                                                           {
                                                               // Listen for when user closes ad
                                                               public void onAdClosed()
                                                               {
                                                                   // When user closes ad end this activity (go back to first activity)
                                                                   Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                                   sc_history.putExtra("SCRATCH", "WEEK");
                                                                   sc_history.putExtra("AMOUNT", week_amount);
                                                                   sc_history.putExtra("SCRATCH_ID", week_sratchcard_id);
                                                                   startActivity(sc_history);
                                                               }
                                                           });

                                                       }
                                                       else {
                                                           Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                           sc_history.putExtra("SCRATCH", "WEEK");
                                                           sc_history.putExtra("AMOUNT", week_amount);
                                                           sc_history.putExtra("SCRATCH_ID", week_sratchcard_id);
                                                           startActivity(sc_history);
                                                           Log.d("tetetet", "The interstitial wasn't loaded yet.");
                                                       }

                                                   }
                                                   else
                                                       {
                                                       final Dialog dialog = new Dialog(getActivity());// add here your class name
                                                     //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                       dialog.setContentView(R.layout.scratch_custom_videoview_dialog);
                                                     final VideoView scratch_vid_view = (VideoView) dialog.findViewById(R.id.scratch_vid_view);
                                                   //  final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progressbar);
                                                       ;

                                                       dialog.show();
                                                         // progressbar.setVisibility(View.VISIBLE);
                                                           media_Controller = new MediaController(getActivity());

                                                         /*  dm = new DisplayMetrics();
                                                           getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                                                           int height = dm.heightPixels;
                                                           int width = dm.widthPixels;
                                                           scratch_vid_view.setMinimumWidth(width);
                                                           scratch_vid_view.setMinimumHeight(height);*/
                                                       //    scratch_vid_view.setMediaController(media_Controller);
                                                           String uriPath = AppUrls.BASE_IMAGE_URL + week_videoUrl;
                                                           try
                                                           {
                                                               Uri video = Uri.parse(uriPath);
                                                               scratch_vid_view.setVideoURI(video);
                                                               scratch_vid_view.setMediaController(media_Controller);
                                                           } catch (Exception e) {
                                                               Log.e("Error", e.getMessage());
                                                               e.printStackTrace();
                                                           }

                                                           scratch_vid_view.requestFocus();
                                                           scratch_vid_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                               // Close the progress bar and play the video
                                                               public void onPrepared(MediaPlayer mp) {

                                                                   scratch_vid_view.start();


                                                               }
                                                           });
                                                           scratch_vid_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                                                               @Override
                                                               public boolean onError(MediaPlayer mp, int what, int extra) {
                                                                   Log.d("video", "setOnErrorListener ");
                                                                   return true;
                                                               }
                                                           });
                                                           dialog.setCanceledOnTouchOutside(false);

                                                       scratch_vid_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                           @Override
                                                           public void onCompletion(MediaPlayer mediaPlayer) {
                                                               dialog.dismiss();
                                                               Intent sc_history = new Intent(getContext(), CardPageActivity.class);
                                                               sc_history.putExtra("SCRATCH", "WEEK");
                                                               sc_history.putExtra("AMOUNT", week_amount);
                                                               sc_history.putExtra("SCRATCH_ID", week_sratchcard_id);
                                                               startActivity(sc_history);
                                                           }
                                                       });
                                                   }
                                               }
                                           });

                                       }
                                   }
                                   else
                                   {
                                       is_scratch_view_week_text.setVisibility(View.VISIBLE);
                                       is_scratch_view_week_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + week_amount + "</b>"));
                                       frame_week.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {

                                           }
                                       });
                                   }
                              }


                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError)
                            {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
                 {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
        else
        {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }




    @Override
    public void onClick(View view)
    {
        if (view==promo_butt){

            checkInternet = NetworkChecking.isConnected(getActivity());
            if (checkInternet) {
                Intent ii = new Intent(getContext(), SportsPromoVideolist.class);
                startActivity(ii);

            } else {
                Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }



        }
        if (view==scratch_history)
        {
            checkInternet = NetworkChecking.isConnected(getActivity());
            if (checkInternet) {
                Intent sc_history = new Intent(getContext(), ScratchCardHistory.class);
                startActivity(sc_history);

            } else {
                Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }




        }

    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy hh:mm a ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
             e.printStackTrace();
        }
        return str;
    }


    @Override
    public void onResume()
    {
        super.onResume();

        getScratchCardData();
    }
    private void loadInterstitialAd() {
        Log.d("LAAAAa","LAAAAa");


      mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }
    private void initializeInterstitialAd() {
        mInterstitialAd = new InterstitialAd(getActivity());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded()
            {
                 Log.d("XXXXAD","XXXXAD");
                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                Log.d("FAILED","FAILED");
            }

            @Override
            public void onAdOpened() {
                Log.d("AdOpened","AdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.d("AdLeftApp","AdLeftApp");
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getContext(), "Interstitial Ad Closed",
                        Toast.LENGTH_SHORT).show();
                //Load the ad and keep so that it can be used
                // next time the ad needs to be displayed
                loadInterstitialAd();

            }
        });
    }


}
