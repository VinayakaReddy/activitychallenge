package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllCompletedChallengesActivity;
import in.activitychallenge.activitychallenge.activities.AllRunningChallengesActivity;
import in.activitychallenge.activitychallenge.activities.AllUpcomingChallengesActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeRequestActivity;
import in.activitychallenge.activitychallenge.activities.ChatMessgesActivity;
import in.activitychallenge.activitychallenge.activities.SponsorRequestActivity;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividCompletAdapter;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividRunAdapter;
import in.activitychallenge.activitychallenge.adapter.ChallengeIndividUpcomAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeIndividualRunningModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GpsService;
import in.activitychallenge.activitychallenge.utilities.IndividualCompletedChallengesDB;
import in.activitychallenge.activitychallenge.utilities.IndividualRunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.IndividualUpcomingChallengesDB;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyChallengesIndividualFragment extends Fragment implements View.OnClickListener
{

View view;
    ImageView no_data_image;
    RecyclerView recycler_individual_running,recycler_individual_upcoming,recycler_individual_completed;
    LinearLayoutManager layoutManager,layoutManager1,layoutManager2;
    GridLayoutManager gridLayoutManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token,user_id,user_type,device_id;
    UserSessionManager userSessionManager;
    Typeface typeface,typeface2;
   //Running Adapter
    ChallengeIndividRunAdapter challengIndvidRunAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidRunList = new ArrayList<ChallengeIndividualRunningModel>();
    //Upcoming Adapter
    ChallengeIndividUpcomAdapter challengIndvidUppAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidUpcomList = new ArrayList<ChallengeIndividualRunningModel>();
    //Complet Adapter
    ChallengeIndividCompletAdapter challengIndvidCompletAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidCompletcomList = new ArrayList<ChallengeIndividualRunningModel>();

    LinearLayout ll_running,ll_upcoming,ll_completed,linear_sponsor, linear_chllenge, linear_messges;
    TextView running_more_text,upcoming_more_text,complet_more_text,sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    IndividualRunningChallengesDB individualRunningChallengesDB;
    IndividualUpcomingChallengesDB individualUpcomingChallengesDB;
    IndividualCompletedChallengesDB individualCompletedChallengesDB;


    ContentValues values,values1,values2;
  /*  MainGpsReciever gpsReciever;
    String GPS_FILTER = "MyGPSLocation";*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.fragment_my_challenges_individual, container, false);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_bold));

        individualRunningChallengesDB = new IndividualRunningChallengesDB(getActivity());
        individualUpcomingChallengesDB = new IndividualUpcomingChallengesDB(getActivity());
        individualCompletedChallengesDB = new IndividualCompletedChallengesDB(getActivity());
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        no_data_image=(ImageView) view.findViewById(R.id.no_data_image);

        ll_running=(LinearLayout)view.findViewById(R.id.ll_running);
        ll_upcoming=(LinearLayout)view.findViewById(R.id.ll_upcoming);
        ll_completed=(LinearLayout)view.findViewById(R.id.ll_completed);

        sponsor_text = (TextView) view.findViewById(R.id.sponsor_text);
        challenge_text = (TextView) view.findViewById(R.id.challenge_text);
        mesages_text = (TextView) view.findViewById(R.id.mesages_text);

        count_sponsor_text = (TextView) view.findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) view.findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) view.findViewById(R.id.count_mesages_text);

        linear_sponsor = (LinearLayout) view.findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) view.findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) view.findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);

        running_more_text = (TextView) view.findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        upcoming_more_text = (TextView) view.findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        complet_more_text = (TextView) view.findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);

        recycler_individual_running=(RecyclerView)view.findViewById(R.id.recycler_individual_running);
        recycler_individual_upcoming=(RecyclerView)view.findViewById(R.id.recycler_individual_upcoming);
        recycler_individual_completed=(RecyclerView)view.findViewById(R.id.recycler_individual_completed);


        challengIndvidRunAdpter = new ChallengeIndividRunAdapter(challengIndvidRunList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_individual_running.setNestedScrollingEnabled(false);
        recycler_individual_running.setLayoutManager(layoutManager);

        challengIndvidUppAdpter = new ChallengeIndividUpcomAdapter(challengIndvidUpcomList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager1 = new LinearLayoutManager(getActivity());
        recycler_individual_upcoming.setNestedScrollingEnabled(false);
        recycler_individual_upcoming.setLayoutManager(layoutManager1);

        challengIndvidCompletAdpter = new ChallengeIndividCompletAdapter(challengIndvidCompletcomList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager2 = new LinearLayoutManager(getActivity());
        recycler_individual_completed.setNestedScrollingEnabled(false);
        recycler_individual_completed.setLayoutManager(layoutManager2);



        getCount();
        getIndividualChallenges();
        return view;
    }



    public void getIndividualChallenges()
    {
        checkInternet = NetworkChecking.isConnected(getContext());
        if(checkInternet)
        {
            challengIndvidRunList.clear();
            individualRunningChallengesDB.emptyDBBucket();
            individualUpcomingChallengesDB.emptyDBBucket();
            individualCompletedChallengesDB.emptyDBBucket();
            String url= AppUrls.BASE_URL+AppUrls.MY_CHALLENGES+"user_id="+user_id+"&user_type="+user_type+"&type=INDIVIDUAL";
            Log.d("INDVCHALLURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("CHALL",response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100"))
                                {

                                    values = new ContentValues();
                                    values1 = new ContentValues();
                                    values2 = new ContentValues();
                                    JSONObject jObjData = jsonObject.getJSONObject("data");
                                  /*  "running_cnt": 2,
                                        "upcomming_cnt": 3,
                                        "completed_cnt": 0,*/
                                    int running_cnt=jObjData.getInt("running_cnt");
                                    values.put(IndividualRunningChallengesDB.RUNNING_COUNT, jObjData.getInt("running_cnt"));
                                    if (running_cnt>2){
                                        running_more_text.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        running_more_text.setVisibility(View.GONE);
                                    }
                                    int upcomming_cnt=jObjData.getInt("upcomming_cnt");
                                    values1.put(IndividualUpcomingChallengesDB.UPCOMING_COUNT, jObjData.getInt("upcomming_cnt"));
                                    if (upcomming_cnt>2){
                                        upcoming_more_text.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        upcoming_more_text.setVisibility(View.GONE);
                                    }
                                    int completed_cnt=jObjData.getInt("completed_cnt");
                                    values2.put(IndividualCompletedChallengesDB.COMPLETED_COUNT, jObjData.getInt("completed_cnt"));
                                    if (completed_cnt>2){
                                        complet_more_text.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        complet_more_text.setVisibility(View.GONE);
                                    }
                                    if(running_cnt!=0)
                                    {
                                        //ContentValues values = new ContentValues();
                                        JSONArray jsonArrayrunning=jObjData.getJSONArray("running");
                                        for (int i = 0; i < jsonArrayrunning.length(); i++)
                                        {
                                            JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                            ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                                            values.put(IndividualRunningChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values.put(IndividualRunningChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values.put(IndividualRunningChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values.put(IndividualRunningChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values.put(IndividualRunningChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values.put(IndividualRunningChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values.put(IndividualRunningChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values.put(IndividualRunningChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values.put(IndividualRunningChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values.put(IndividualRunningChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values.put(IndividualRunningChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values.put(IndividualRunningChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values.put(IndividualRunningChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values.put(IndividualRunningChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values.put(IndividualRunningChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values.put(IndividualRunningChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values.put(IndividualRunningChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values.put(IndividualRunningChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values.put(IndividualRunningChallengesDB.WINNER, jsonObject1.getString("winner"));
                                            values.put(IndividualRunningChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                            values.put(IndividualRunningChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                            values.put(IndividualRunningChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values.put(IndividualRunningChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values.put(IndividualRunningChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values.put(IndividualRunningChallengesDB.GET_GPS, jsonObject1.getString("gps"));
                                            values.put(IndividualRunningChallengesDB.OPPONENT_TYPE,jsonObject1.getString("opponent_name"));
                                            individualRunningChallengesDB.addRunningChallengesList(values);
                                            Log.d("dsvbjsdbvjksdv",values.toString());
                                           /* am_runn.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_runn.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_runn.setUser_name(jsonObject1.getString("user_name"));
                                            am_runn.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_runn.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_runn.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_runn.setStatus(jsonObject1.getString("status"));
                                            am_runn.setAmount(jsonObject1.getString("amount"));
                                            am_runn.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_runn.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_runn.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_runn.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_runn.setStart_on(jsonObject1.getString("start_on"));
                                            am_runn.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_runn.setResume_on(jsonObject1.getString("resume_on"));
                                            am_runn.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_runn.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_runn.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidRunList.add(am_runn);*/


                                        }

                                    }
                                    else
                                    {// ,,;
                                        ll_running.setVisibility(View.GONE);
                                    }
//upconing
                                    if(upcomming_cnt!=0)
                                    {
                                        //ContentValues values1 = new ContentValues();
                                        JSONArray jsonArrayupcom=jObjData.getJSONArray("upcomming");
                                        for (int j = 0; j < jsonArrayupcom.length(); j++)
                                        {
                                            JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                            values1.put(IndividualUpcomingChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values1.put(IndividualUpcomingChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values1.put(IndividualUpcomingChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values1.put(IndividualUpcomingChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values1.put(IndividualUpcomingChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values1.put(IndividualUpcomingChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values1.put(IndividualUpcomingChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values1.put(IndividualUpcomingChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values1.put(IndividualUpcomingChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values1.put(IndividualUpcomingChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values1.put(IndividualUpcomingChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values1.put(IndividualUpcomingChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values1.put(IndividualUpcomingChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values1.put(IndividualUpcomingChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values1.put(IndividualUpcomingChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values1.put(IndividualUpcomingChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values1.put(IndividualUpcomingChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values1.put(IndividualUpcomingChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values1.put(IndividualUpcomingChallengesDB.WINNER, jsonObject1.getString("winner"));
                                            values1.put(IndividualUpcomingChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                            values1.put(IndividualUpcomingChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                            values1.put(IndividualUpcomingChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values1.put(IndividualUpcomingChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values1.put(IndividualUpcomingChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values1.put(IndividualUpcomingChallengesDB.GET_GPS, jsonObject1.getString("gps"));
                                            individualUpcomingChallengesDB.addUpcomingChallengesList(values1);
                                           /* ChallengeIndividualRunningModel am_up = new ChallengeIndividualRunningModel();
                                            am_up.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_up.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_up.setUser_name(jsonObject1.getString("user_name"));
                                            am_up.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_up.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_up.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_up.setStatus(jsonObject1.getString("status"));
                                            am_up.setAmount(jsonObject1.getString("amount"));
                                            am_up.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_up.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_up.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_up.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_up.setStart_on(jsonObject1.getString("start_on"));
                                            am_up.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_up.setResume_on(jsonObject1.getString("resume_on"));
                                            am_up.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_up.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_up.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidUpcomList.add(am_up);*/

                                        }
                                    }
                                    else
                                    {

                                        ll_upcoming.setVisibility(View.GONE);
                                    }
//complete
                                    if(completed_cnt!=0)
                                    {
                                       // ContentValues values2 = new ContentValues();
                                        JSONArray jsonArraycomplet=jObjData.getJSONArray("completed");
                                        for (int k = 0; k < jsonArraycomplet.length(); k++)
                                        {
                                            JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                            values2.put(IndividualCompletedChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values2.put(IndividualCompletedChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values2.put(IndividualCompletedChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values2.put(IndividualCompletedChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values2.put(IndividualCompletedChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values2.put(IndividualCompletedChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values2.put(IndividualCompletedChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values2.put(IndividualCompletedChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values2.put(IndividualCompletedChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values2.put(IndividualCompletedChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values2.put(IndividualCompletedChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values2.put(IndividualCompletedChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values2.put(IndividualCompletedChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values2.put(IndividualCompletedChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values2.put(IndividualCompletedChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values2.put(IndividualCompletedChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values2.put(IndividualCompletedChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values2.put(IndividualCompletedChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values2.put(IndividualCompletedChallengesDB.WINNER, jsonObject1.getString("winner"));
                                            values2.put(IndividualCompletedChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                            values2.put(IndividualCompletedChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                            values2.put(IndividualCompletedChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values2.put(IndividualCompletedChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values2.put(IndividualCompletedChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values2.put(IndividualCompletedChallengesDB.GET_GPS, jsonObject1.getString("gps"));
                                            String gvvvv=jsonObject1.getString("winner");
                                            Log.d("GGGGGGG",gvvvv +"/////"+values2);
                                            individualCompletedChallengesDB.addCompletedChallengesList(values2);
                                            /*ChallengeIndividualRunningModel am_complet = new ChallengeIndividualRunningModel();
                                            am_complet.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_complet.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_complet.setUser_name(jsonObject1.getString("user_name"));
                                            am_complet.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_complet.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_complet.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_complet.setStatus(jsonObject1.getString("status"));
                                            am_complet.setAmount(jsonObject1.getString("amount"));
                                            am_complet.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_complet.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_complet.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_complet.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_complet.setStart_on(jsonObject1.getString("start_on"));
                                            am_complet.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_complet.setResume_on(jsonObject1.getString("resume_on"));
                                            am_complet.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_complet.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_complet.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengIndvidCompletcomList.add(am_complet);*/

                                        }
                                    }
                                    else
                                    {

                                        ll_completed.setVisibility(View.GONE);
                                    }




                                     // UPCOMING RECYCLER



                                    // Completed RECYCLER
                                    individualRunningChallenges();
                                    individualUpcomingChallenges();
                                    individualCompletedChallenges();
                                  //  recycler_individual_running.setAdapter(challengIndvidRunAdpter);
                                   // recycler_individual_upcoming.setAdapter(challengIndvidUppAdpter);
                                   // recycler_individual_completed.setAdapter(challengIndvidCompletAdpter);

                                }
                                if(status.equals("10200"))
                                {
                                   // no_data_image.setVisibility(View.VISIBLE);
                                 Toast.makeText(getActivity(),"No Challenges Found...!",Toast.LENGTH_LONG).show();
                                }
                                if(status.equals("10300"))
                                {
                                    no_data_image.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(),"No data Found..!",Toast.LENGTH_LONG).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);

        }
        else
        {
            individualRunningChallenges();
            individualUpcomingChallenges();
            individualCompletedChallenges();
           /* if(isFirstTime())
            Toast.makeText(getContext(),"No Internet Connection...!",Toast.LENGTH_LONG).show();*/
        }
    }


    @Override
    public void onClick(View view)
    {
        if(view==running_more_text)
        {
           Intent irunmore=new Intent(getActivity(), AllRunningChallengesActivity.class);
            irunmore.putExtra("condition","INDIVIDUAL");
           startActivity(irunmore);
        }
        if(view==upcoming_more_text)
        {
            Intent irunmore=new Intent(getActivity(), AllUpcomingChallengesActivity.class);
            irunmore.putExtra("condition","INDIVIDUAL");
            startActivity(irunmore);
        }
        if(view==complet_more_text)
        {
            Intent irunmore=new Intent(getActivity(), AllCompletedChallengesActivity.class);
            irunmore.putExtra("condition","INDIVIDUAL");
            startActivity(irunmore);
        }

        if (view == linear_sponsor) {
            Intent sponsor = new Intent(getActivity(), SponsorRequestActivity.class);
            startActivity(sponsor);

        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(getActivity(), ChallengeRequestActivity.class);
            startActivity(chellenge);

        }
        if (view == linear_messges) {
            Intent msg = new Intent(getActivity(), ChatMessgesActivity.class);
            msg.putExtra("condition","NORMAL");    //for switching tab order
            startActivity(msg);

        }

    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {


            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            if(isFirstTime())
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
    private void individualRunningChallenges() {

        challengIndvidRunList.clear();
        List<String> activityName = individualRunningChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = individualRunningChallengesDB.getChallengeID();
            List<String> activity_id = individualRunningChallengesDB.getActivityID();
            List<String> user_name = individualRunningChallengesDB.getUserName();
            List<String> opponent_name = individualRunningChallengesDB.getOpponentName();
            List<String> winning_status = individualRunningChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualRunningChallengesDB.getIsGroupAdmin();
            List<String> status = individualRunningChallengesDB.getStatus();
            List<String> amount = individualRunningChallengesDB.getAmount();
            List<String> winning_amount = individualRunningChallengesDB.getWinningAmount();
            List<String> activity_name = individualRunningChallengesDB.getActivityName();
            List<String> evaluation_factor = individualRunningChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = individualRunningChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = individualRunningChallengesDB.getStartOn();
            List<String> paused_on = individualRunningChallengesDB.getPausedOn();
            List<String> resume_on = individualRunningChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualRunningChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualRunningChallengesDB.getCompletedOn();
            List<String> activity_image = individualRunningChallengesDB.getActivityImage();
            List<String> winner = individualRunningChallengesDB.getWinner();
            List<String> paused_by = individualRunningChallengesDB.getPaused_by();
            List<String> challenge_type = individualRunningChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualRunningChallengesDB.win_RewardType();
            List<String> winning_reward_value = individualRunningChallengesDB.win_RewardValue();
            List<String> is_scratched = individualRunningChallengesDB.isScratched();
            List<String> get_gps = individualRunningChallengesDB.getGPS();
            int running_count = individualRunningChallengesDB.getRunningCount();
            List<String> opponenttypes = individualRunningChallengesDB.getOpponentType();
            Log.d("runningcountSS",""+running_count);
            if(running_count==0)
            {
                ll_running.setVisibility(View.GONE);
            }
            if (running_count>2){
                running_more_text.setVisibility(View.VISIBLE);
            }
            else {
                running_more_text.setVisibility(View.GONE);
            }
            for (int i = 0; i < activityName.size(); i++) {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setOpponent_type(opponenttypes.get(i));
                am_runn.setCount_value(String.valueOf(running_count));
                challengIndvidRunList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengIndvidRunList.toString());

            }
            recycler_individual_running.setAdapter(challengIndvidRunAdpter);
        } else {

        }
    }

    private void individualUpcomingChallenges() {

        challengIndvidUpcomList.clear();
        List<String> activityName = individualUpcomingChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = individualUpcomingChallengesDB.getChallengeID();
            List<String> activity_id = individualUpcomingChallengesDB.getActivityID();
            List<String> user_name = individualUpcomingChallengesDB.getUserName();
            List<String> opponent_name = individualUpcomingChallengesDB.getOpponentName();
            List<String> winning_status = individualUpcomingChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualUpcomingChallengesDB.getIsGroupAdmin();
            List<String> status = individualUpcomingChallengesDB.getStatus();
            List<String> amount = individualUpcomingChallengesDB.getAmount();
            List<String> winning_amount = individualUpcomingChallengesDB.getWinningAmount();
            List<String> activity_name = individualUpcomingChallengesDB.getActivityName();
            List<String> evaluation_factor = individualUpcomingChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = individualUpcomingChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = individualUpcomingChallengesDB.getStartOn();
            List<String> paused_on = individualUpcomingChallengesDB.getPausedOn();
            List<String> resume_on = individualUpcomingChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualUpcomingChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualUpcomingChallengesDB.getCompletedOn();
            List<String> activity_image = individualUpcomingChallengesDB.getActivityImage();
            List<String> winner = individualUpcomingChallengesDB.getWinner();
            List<String> paused_by = individualUpcomingChallengesDB.getPaused_by();
            List<String> challenge_type = individualUpcomingChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualUpcomingChallengesDB.win_RewardType();
            List<String> winning_reward_value = individualUpcomingChallengesDB.win_RewardValue();
            List<String> is_scratched = individualUpcomingChallengesDB.isScratched();
            List<String> get_gps = individualUpcomingChallengesDB.getGPS();

            int upcoming_count = individualUpcomingChallengesDB.getUpcomingingCount();
            Log.d("upcomingcountSS",""+upcoming_count);
            if(upcoming_count==0)
            {
                ll_upcoming.setVisibility(View.GONE);
            }
            if (upcoming_count>2){
                upcoming_more_text.setVisibility(View.VISIBLE);
            }
            else {
                upcoming_more_text.setVisibility(View.GONE);
            }

            for (int i = 0; i < activityName.size(); i++) {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setCount_value(String.valueOf(upcoming_count));
                challengIndvidUpcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengIndvidUpcomList.toString());

            }
            recycler_individual_upcoming.setAdapter(challengIndvidUppAdpter);
        } else {

        }
    }

    private void individualCompletedChallenges() {

        challengIndvidCompletcomList.clear();
        List<String> activityName = individualCompletedChallengesDB.getActivityName();
        if (activityName.size() > 0)
        {
            List<String> challenge_id = individualCompletedChallengesDB.getChallengeID();
            List<String> activity_id = individualCompletedChallengesDB.getActivityID();
            List<String> user_name = individualCompletedChallengesDB.getUserName();
            List<String> opponent_name = individualCompletedChallengesDB.getOpponentName();
            List<String> winning_status = individualCompletedChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualCompletedChallengesDB.getIsGroupAdmin();
            List<String> status = individualCompletedChallengesDB.getStatus();
            List<String> amount = individualCompletedChallengesDB.getAmount();
            List<String> winning_amount = individualCompletedChallengesDB.getWinningAmount();
            List<String> activity_name = individualCompletedChallengesDB.getActivityName();
            List<String> evaluation_factor = individualCompletedChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = individualCompletedChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = individualCompletedChallengesDB.getStartOn();
            List<String> paused_on = individualCompletedChallengesDB.getPausedOn();
            List<String> resume_on = individualCompletedChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualCompletedChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualCompletedChallengesDB.getCompletedOn();
            List<String> activity_image = individualCompletedChallengesDB.getActivityImage();
            List<String> winner = individualCompletedChallengesDB.getWinner();
            List<String> paused_by = individualCompletedChallengesDB.getPaused_by();
            List<String> challenge_type = individualCompletedChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualCompletedChallengesDB.win_RewardType();
            List<String> winning_reward_value = individualCompletedChallengesDB.win_RewardValue();
            List<String> is_scratched = individualCompletedChallengesDB.isScratched();
            List<String> get_gps = individualCompletedChallengesDB.getGPS();


            int completed_count = individualCompletedChallengesDB.getCompletedCount();
            Log.d("completedcountSS",""+completed_count);
            if(completed_count==0)
            {
                ll_completed.setVisibility(View.GONE);
            }
            if (completed_count>2){
                complet_more_text.setVisibility(View.VISIBLE);
            }
            else {
                complet_more_text.setVisibility(View.GONE);
            }
            Log.d("WWWWW",""+winner );
            for (int i = 0; i < activityName.size(); i++)
            {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setCount_value(String.valueOf(completed_count));
                challengIndvidCompletcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengIndvidUpcomList.toString());

            }
            recycler_individual_completed.setAdapter(challengIndvidCompletAdpter);
        } else {

        }
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }
}
