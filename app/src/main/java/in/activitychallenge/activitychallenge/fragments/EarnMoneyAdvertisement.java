package in.activitychallenge.activitychallenge.fragments;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AdvertiseScratchCardActivity;
import in.activitychallenge.activitychallenge.activities.CardPageActivity;
import in.activitychallenge.activitychallenge.activities.SportsPromoVideolist;
import in.activitychallenge.activitychallenge.adapter.SportsPromoAdVideoAdapter;
import in.activitychallenge.activitychallenge.holder.SportsPromoVideoHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.PromoVideoClickListner;
import in.activitychallenge.activitychallenge.models.SportsPromoVideoModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class EarnMoneyAdvertisement extends Fragment {

    View view;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    SportsPromoAdVideoAdapter promoAdap;
    RecyclerView promo_recycler;
    LinearLayoutManager linLay;
    ArrayList<SportsPromoVideoModel> promooList = new ArrayList<SportsPromoVideoModel>();
    VideoView vid_view;
     ImageView no_data_image;
    TextView txtil;
    String device_id, access_token, vid_uri, txt_title,userid,usertype;
    DisplayMetrics dm;
    MediaController media_Controller;
    private InterstitialAd mInterstitialAd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view=   inflater.inflate(R.layout.fragment_earn_money_advertisement, container, false);
        MobileAds.initialize(getContext(), getString(R.string.admob_app_id));



        initializeInterstitialAd();
        loadInterstitialAd();
        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        userid = userDetails.get(UserSessionManager.USER_ID);
        usertype = userDetails.get(UserSessionManager.USER_TYPE);
        Log.d("SESSIgATA", access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
      //  txtil = (TextView) view.findViewById(R.id.txtil);
        vid_view = (VideoView) view.findViewById(R.id.vid_view);

        no_data_image = (ImageView) view.findViewById(R.id.no_data_image);

        promo_recycler = (RecyclerView) view.findViewById(R.id.promo_recycler);
        promo_recycler.setNestedScrollingEnabled(false);
        promoAdap = new SportsPromoAdVideoAdapter(promooList,EarnMoneyAdvertisement.this,  R.layout.row_promovideo);
        linLay = new LinearLayoutManager(getActivity());
        promo_recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

     //   getVideoList();

        return view;
    }
    private void getVideoList() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("PROMOVIDE", AppUrls.BASE_URL + AppUrls.PROMOTIONS_VIDEO);
                            Log.d("PROMOVIDE_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    //  Toast.makeText(SportsPromoVideolist.this, "Success", Toast.LENGTH_SHORT).show();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        SportsPromoVideoModel rhm = new SportsPromoVideoModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setAdv_id(itemArray.getString("adv_id"));
                                        rhm.setCompany_name(itemArray.getString("company_name"));
                                        rhm.setTimespan(itemArray.getString("timespan"));
                                        rhm.setVideo_path(itemArray.getString("video_path"));
                                        promooList.add(rhm);
                                    }
                                    promo_recycler.setAdapter(promoAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    if (mInterstitialAd.isLoaded()) {
                                        mInterstitialAd.show();
                                        mInterstitialAd.setAdListener(new AdListener() {
                                            // Listen for when user closes ad
                                            public void onAdClosed() {

                                            }
                                        });
                                    }
                                    //Toast.makeText(getActivity(), "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("PROMOMMOVID_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getVideoList();
        promooList.clear();
    }
    private void loadInterstitialAd() {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }
    private void initializeInterstitialAd() {
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getContext(), "Interstitial Ad Closed",
                        Toast.LENGTH_SHORT).show();
                //Load the ad and keep so that it can be used
                // next time the ad needs to be displayed
                loadInterstitialAd();

            }
        });
    }
}
