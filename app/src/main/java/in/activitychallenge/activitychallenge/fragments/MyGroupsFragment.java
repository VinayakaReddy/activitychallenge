package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.GroupDetailActivity;
import in.activitychallenge.activitychallenge.activities.GroupMainActivity;
import in.activitychallenge.activitychallenge.filters.MyGroupFilterFilterList;
import in.activitychallenge.activitychallenge.holder.MyGroupHolder;
import in.activitychallenge.activitychallenge.itemclicklistners.MyGroupItemClickListener;
import in.activitychallenge.activitychallenge.models.MyGroupModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;


public class MyGroupsFragment extends Fragment implements View.OnClickListener  {

    View view;
    ImageView close, grp_img_1, grp_img_2, grp_img_3, grp_img_4, grp_img_5,no_data_image;

    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id, user_type;
    UserSessionManager session;
    SearchView searchMyGroup;
    RecyclerView my_group_recylerview;
    LinearLayoutManager lManager;
    ArrayList<MyGroupModel> myGlist = new ArrayList<MyGroupModel>();
    MyGroupAdapter myGroAdap;

    public MyGroupsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_my_groups, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id+"///////"+user_type);
        TextView toolbar_title = (TextView)view. findViewById(R.id.toolbar_title);
        toolbar_title.setText("My Groups");
        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        no_data_image = (ImageView)view. findViewById(R.id.no_data_image);

        grp_img_1 = (ImageView) view.findViewById(R.id.grp_img_1);
        grp_img_2 = (ImageView) view.findViewById(R.id.grp_img_2);
        grp_img_3 = (ImageView) view.findViewById(R.id.grp_img_3);
        grp_img_4 = (ImageView) view.findViewById(R.id.grp_img_4);
        grp_img_5 = (ImageView) view.findViewById(R.id.grp_img_5);
        close = (ImageView) view.findViewById(R.id.close);
        close.setOnClickListener(MyGroupsFragment.this);

        searchMyGroup = (SearchView) view.findViewById(R.id.searchMyGroup);
        searchMyGroup.setIconified(false);
        searchMyGroup.clearFocus();
        searchMyGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMyGroup.setIconified(false);
            }
        });
        searchMyGroup.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
               if(myGroAdap!=null)
                myGroAdap.getFilter().filter(query);
                return false;
            }
        });
        my_group_recylerview = (RecyclerView) view.findViewById(R.id.my_group_recylerview);
        my_group_recylerview.setNestedScrollingEnabled(false);
        myGroAdap = new MyGroupAdapter(MyGroupsFragment.this, myGlist, R.layout.row_mygroup,user_type);
        lManager = new LinearLayoutManager(getActivity());
        my_group_recylerview.setLayoutManager(lManager);

        getMyGroups();


        return  view;
    }

    private void getMyGroups() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + "group/user?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("UUUUU", url);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/user?user_id=" + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("GetMYGroupUrlResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    searchMyGroup.setVisibility(View.VISIBLE);
                                    // Toast.makeText(MyGroupActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        MyGroupModel rhm = new MyGroupModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setName(itemArray.getString("name"));
                                        rhm.setAdmin_id(itemArray.getString("admin_id"));
                                        rhm.setGroup_pic(itemArray.getString("group_pic"));
                                        rhm.setGroup_members(itemArray.getString("group_members") + " " + "members");
                                        rhm.setTotal_win(itemArray.getString("total_win"));
                                        rhm.setTotal_loss(itemArray.getString("total_loss"));
                                        rhm.setOverall_rank(itemArray.getString("overall_rank"));
                                        rhm.setWinning_per(itemArray.getString("winning_per") + " " + "%");
                                        rhm.setImg1(itemArray.getJSONArray("recent_five_activity").getJSONObject(0).getString("img1"));
                                        rhm.setImg2(itemArray.getJSONArray("recent_five_activity").getJSONObject(1).getString("img2"));
                                        rhm.setImg3(itemArray.getJSONArray("recent_five_activity").getJSONObject(2).getString("img3"));
                                        rhm.setImg4(itemArray.getJSONArray("recent_five_activity").getJSONObject(3).getString("img4"));
                                        rhm.setImg5(itemArray.getJSONArray("recent_five_activity").getJSONObject(4).getString("img5"));
                                        myGlist.add(rhm);
                                    }
                                    my_group_recylerview.setAdapter(myGroAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    searchMyGroup.setVisibility(View.GONE);
                                    pprogressDialog.dismiss();
                                    no_data_image.setVisibility(View.VISIBLE);
                                    //Toast.makeText(MyGroupActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MyGRPP_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

  public  class MyGroupAdapter extends RecyclerView.Adapter<MyGroupHolder> implements Filterable {
        MyGroupsFragment context;
        LayoutInflater lInfla;
        int resource;
        public ArrayList<MyGroupModel> myList;
        ArrayList<MyGroupModel> filterlist;
        MyGroupFilterFilterList filter;
       String u_type;

        public MyGroupAdapter(MyGroupsFragment context, ArrayList<MyGroupModel> myList, int resource,String type) {
            this.context = context;
            this.myList = myList;
            this.resource = resource;
            this.filterlist = myList;
            this.u_type = type;
            lInfla = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            HashMap<String, String> userDetails = session.getUserDetails();
            u_type = userDetails.get(UserSessionManager.USER_TYPE);
            Log.d("SESSTTTTT",u_type);

        }

        @Override
        public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = lInfla.inflate(resource, null);
            MyGroupHolder slh = new MyGroupHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(MyGroupHolder holder, final int position) {
            String str = myList.get(position).getName();
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            Log.d("TTTTTTTT",u_type);



            String admin_id = myList.get(position).getAdmin_id();
            Log.d("dettttt", admin_id + "///////" + user_id);
            if (admin_id.equals(user_id)) {
                holder.take_challeng.setVisibility(View.VISIBLE);
                holder.ll_take.setVisibility(View.VISIBLE);
            } else {
                holder.take_challeng.setVisibility(View.GONE);
                holder.ll_take.setVisibility(View.GONE);
            }
            if(u_type.equals("SPONSOR"))
            {
                holder.ll_take.setVisibility(View.GONE);
            }

            holder.grp_name.setText(converted_string);
            holder.grp_memb_cnt.setText(myList.get(position).getGroup_members());
            holder.grp_lost_cnt.setText(myList.get(position).getTotal_loss());
            holder.grp_won_cnt.setText(myList.get(position).getTotal_win());
            holder.grp_percent.setText(myList.get(position).getWinning_per());
            holder.grp_rank.setText(myList.get(position).getOverall_rank());
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getGroup_pic())
                    .placeholder(R.drawable.dummy_group_profile)
                    //.resize(70, 70)
                    .into(holder.grp_img);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg1())
                    .into(holder.grp_img_1);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg2())
                    .into(holder.grp_img_2);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg3())
                    .into(holder.grp_img_3);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg4())
                    .into(holder.grp_img_4);
            Picasso.with(getActivity())
                    .load(AppUrls.BASE_IMAGE_URL + myList.get(position).getImg5())
                    .into(holder.grp_img_5);
            holder.setItemClickListener(new MyGroupItemClickListener() {
                @Override
                public void onItemClick(View view, int layoutPosition) {
                    Intent ii = new Intent(getActivity(), GroupDetailActivity.class);
                    ii.putExtra("grp_id", myList.get(position).getId());
                    ii.putExtra("grp_name", myList.get(position).getName());
                    ii.putExtra("grp_admin_id", myList.get(position).getAdmin_id());
                    ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_INTERNAL");
                    Log.d("sdfdsf", myList.get(position).getId() + "///" + myList.get(position).getName() + "///" + myList.get(position).getAdmin_id());
                    startActivity(ii);
                }
            });
            holder.take_challeng.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), GroupMainActivity.class);
                    intent.putExtra("user_id", myList.get(position).getId());
                    intent.putExtra("user_type", "GROUP");
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return myList.size();
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new MyGroupFilterFilterList(filterlist, this);
            }
            return filter;
        }
    }
    @Override
    public void onClick(View view) {

    }
}
