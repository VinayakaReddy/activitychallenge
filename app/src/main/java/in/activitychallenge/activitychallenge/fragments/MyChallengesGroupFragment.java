package in.activitychallenge.activitychallenge.fragments;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.AllCompletedChallengesActivity;
import in.activitychallenge.activitychallenge.activities.AllRunningChallengesActivity;
import in.activitychallenge.activitychallenge.activities.AllUpcomingChallengesActivity;
import in.activitychallenge.activitychallenge.activities.ChallengeRequestActivity;
import in.activitychallenge.activitychallenge.activities.ChatMessgesActivity;
import in.activitychallenge.activitychallenge.activities.SponsorRequestActivity;
import in.activitychallenge.activitychallenge.adapter.MyChallengeGroupCompletAdapter;
import in.activitychallenge.activitychallenge.adapter.MyChallengeGroupRunAdapter;
import in.activitychallenge.activitychallenge.adapter.MyChallengeGroupUpcomAdapter;
import in.activitychallenge.activitychallenge.models.ChallengeGroupDetailModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.GroupCompletedChallengesDB;
import in.activitychallenge.activitychallenge.utilities.GroupRunningChallengesDB;
import in.activitychallenge.activitychallenge.utilities.GroupUpcomingChallengesDB;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

public class MyChallengesGroupFragment extends Fragment implements View.OnClickListener {
    ImageView no_data_image;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    UserSessionManager userSessionManager;
    Typeface typeface, typeface2;
    LinearLayoutManager layoutManager, layoutManager1, layoutManager2;
    View view;
    TextView running_more_text, upcoming_more_text, complet_more_text, sponsor_text, challenge_text, mesages_text, count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout ll_running, ll_upcoming, ll_completed, linear_sponsor, linear_chllenge, linear_messges;

    RecyclerView grp_running_recycler, grp_upcoming_recycler, grp_completed_recycler;

    //Running Adapter
    MyChallengeGroupRunAdapter challengGroupRunAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupRunList = new ArrayList<ChallengeGroupDetailModel>();
    //Upcoming Adapter
    MyChallengeGroupUpcomAdapter challengGroupUppAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupUpcomList = new ArrayList<ChallengeGroupDetailModel>();
    //Complet Adapter
    MyChallengeGroupCompletAdapter challengGroupCompletAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupCompletcomList = new ArrayList<ChallengeGroupDetailModel>();
    GroupRunningChallengesDB groupRunningChallengesDB;
    GroupUpcomingChallengesDB groupUpcomingChallengesDB;
    GroupCompletedChallengesDB groupCompletedChallengesDB;
    ContentValues values,values1,values2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_challenges_group, container, false);
        challengGroupRunList.clear();
        challengGroupUpcomList.clear();
        challengGroupCompletcomList.clear();
        groupRunningChallengesDB = new GroupRunningChallengesDB(getActivity());
        groupUpcomingChallengesDB = new GroupUpcomingChallengesDB(getActivity());
        groupCompletedChallengesDB = new GroupCompletedChallengesDB(getActivity());
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_regular));
        typeface2 = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mp_bold));

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        no_data_image = (ImageView) view.findViewById(R.id.no_data_image);

        ll_running = (LinearLayout) view.findViewById(R.id.ll_running);
        ll_upcoming = (LinearLayout) view.findViewById(R.id.ll_upcoming);
        ll_completed = (LinearLayout) view.findViewById(R.id.ll_completed);

        sponsor_text = (TextView) view.findViewById(R.id.sponsor_text);
        challenge_text = (TextView) view.findViewById(R.id.challenge_text);
        mesages_text = (TextView) view.findViewById(R.id.mesages_text);

        count_sponsor_text = (TextView) view.findViewById(R.id.count_sponsor_text);
        count_challenge_text = (TextView) view.findViewById(R.id.count_challenge_text);
        count_mesages_text = (TextView) view.findViewById(R.id.count_mesages_text);

        linear_sponsor = (LinearLayout) view.findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = (LinearLayout) view.findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = (LinearLayout) view.findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);


        running_more_text = (TextView) view.findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        upcoming_more_text = (TextView) view.findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        complet_more_text = (TextView) view.findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);

        grp_running_recycler = (RecyclerView) view.findViewById(R.id.grp_running_recycler);
        grp_upcoming_recycler = (RecyclerView) view.findViewById(R.id.grp_upcoming_recycler);
        grp_completed_recycler = (RecyclerView) view.findViewById(R.id.grp_completed_recycler);

        challengGroupRunAdpter = new MyChallengeGroupRunAdapter(challengGroupRunList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager = new LinearLayoutManager(getActivity());
        grp_running_recycler.setNestedScrollingEnabled(false);
        grp_running_recycler.setLayoutManager(layoutManager);

        challengGroupUppAdpter = new MyChallengeGroupUpcomAdapter(challengGroupUpcomList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager1 = new LinearLayoutManager(getActivity());
        grp_upcoming_recycler.setNestedScrollingEnabled(false);
        grp_upcoming_recycler.setLayoutManager(layoutManager1);


        challengGroupCompletAdpter = new MyChallengeGroupCompletAdapter(challengGroupCompletcomList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager2 = new LinearLayoutManager(getActivity());
        grp_completed_recycler.setNestedScrollingEnabled(false);
        grp_completed_recycler.setLayoutManager(layoutManager2);

        getCount();
        getGroupChallenges();
        return view;
    }

    public void getGroupChallenges() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            challengGroupRunList.clear();
            groupRunningChallengesDB.emptyDBBucket();
            groupUpcomingChallengesDB.emptyDBBucket();
            groupCompletedChallengesDB.emptyDBBucket();
            //   challengGroupDetailUpcomList.clear();
            //   challengGroupDetailCompletcomList.clear();
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + user_id + "&user_type=USER&type=" + "GROUP";
            Log.d("GROUPPCHALLURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("GGGRPOCHALLINDVRESP:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100"))
                                {
                                    values = new ContentValues();
                                    values1 = new ContentValues();
                                    values2 = new ContentValues();
                                    JSONObject jObjData = jsonObject.getJSONObject("data");

                                    int running_cnt = jObjData.getInt("running_cnt");
                                    values.put(GroupRunningChallengesDB.RUNNING_COUNT, jObjData.getInt("running_cnt"));
                                    int upcomming_cnt = jObjData.getInt("upcomming_cnt");
                                    values1.put(GroupUpcomingChallengesDB.UPCOMING_COUNT, jObjData.getInt("upcomming_cnt"));
                                    int completed_cnt = jObjData.getInt("completed_cnt");
                                    values2.put(GroupCompletedChallengesDB.COMPLETED_COUNT, jObjData.getInt("completed_cnt"));
                                    if (running_cnt > 2) {
                                        running_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (upcomming_cnt > 2) {
                                        upcoming_more_text.setVisibility(View.VISIBLE);
                                    }
                                    if (completed_cnt > 2) {
                                        complet_more_text.setVisibility(View.VISIBLE);
                                    }

                                    if (running_cnt != 0) {
                                       // ContentValues values = new ContentValues();
                                        JSONArray jsonArrayrunning = jObjData.getJSONArray("running");
                                        Log.d("RUNNNN", jsonArrayrunning.toString());
                                        for (int i = 0; i < jsonArrayrunning.length(); i++) {
                                            JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                            values.put(GroupRunningChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values.put(GroupRunningChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values.put(GroupRunningChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values.put(GroupRunningChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values.put(GroupRunningChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values.put(GroupRunningChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values.put(GroupRunningChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values.put(GroupRunningChallengesDB.USER_TYPE, jsonObject1.getString("user_type"));
                                            values.put(GroupRunningChallengesDB.USER_ID, jsonObject1.getString("user_id"));
                                            values.put(GroupRunningChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values.put(GroupRunningChallengesDB.CHALLENGER_GROUP_ID, jsonObject1.getString("challenger_group_id"));
                                            values.put(GroupRunningChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values.put(GroupRunningChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values.put(GroupRunningChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values.put(GroupRunningChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values.put(GroupRunningChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values.put(GroupRunningChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values.put(GroupRunningChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values.put(GroupRunningChallengesDB.PAUSE_ACCESS, jsonObject1.getString("pause_access"));
                                            values.put(GroupRunningChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values.put(GroupRunningChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values.put(GroupRunningChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values.put(GroupRunningChallengesDB.WINNER,  jsonObject1.getString("winner"));
                                            values.put(GroupRunningChallengesDB.PAUSED_BY,  jsonObject1.getString("paused_by"));
                                            values.put(GroupRunningChallengesDB.CHALLENGE_TYPE,  jsonObject1.getString("challenge_type"));
                                            values.put(GroupRunningChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values.put(GroupRunningChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values.put(GroupRunningChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values.put(GroupRunningChallengesDB.GET_GPS, jsonObject1.getString("gps"));
                                            values.put(GroupRunningChallengesDB.OPPONENT_TYPE, jsonObject1.getString("opponent_name"));
                                            groupRunningChallengesDB.addGroupRunningChallengesList(values);
                                           /* ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                                            am_runn.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_runn.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_runn.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_runn.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_runn.setStatus(jsonObject1.getString("status"));
                                            am_runn.setAmount(jsonObject1.getString("amount"));
                                            am_runn.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_runn.setUser_type(jsonObject1.getString("user_type"));
                                            am_runn.setUser_type(jsonObject1.getString("user_id"));
                                            am_runn.setUser_name(jsonObject1.getString("user_name"));
                                            am_runn.setUser_type(jsonObject1.getString("challenger_group_id"));
                                            am_runn.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_runn.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_runn.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_runn.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_runn.setStart_on(jsonObject1.getString("start_on"));
                                            am_runn.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_runn.setResume_on(jsonObject1.getString("resume_on"));
                                            am_runn.setPause_access(jsonObject1.getString("pause_access"));
                                            am_runn.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_runn.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_runn.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupRunList.add(am_runn);*/


                                        }

                                    } else {
                                        ll_running.setVisibility(View.GONE);
                                    }
                                    //upcoming
                                    if (upcomming_cnt != 0) {
                                      //  ContentValues values1 = new ContentValues();
                                        JSONArray jsonArrayupcom = jObjData.getJSONArray("upcomming");
                                        Log.d("UPPPP", jsonArrayupcom.toString());
                                        for (int j = 0; j < jsonArrayupcom.length(); j++) {
                                            JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                            values1.put(GroupUpcomingChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values1.put(GroupUpcomingChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values1.put(GroupUpcomingChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values1.put(GroupUpcomingChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values1.put(GroupUpcomingChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values1.put(GroupUpcomingChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values1.put(GroupUpcomingChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values1.put(GroupUpcomingChallengesDB.USER_TYPE, jsonObject1.getString("user_type"));
                                            values1.put(GroupUpcomingChallengesDB.USER_ID, jsonObject1.getString("user_id"));
                                            values1.put(GroupUpcomingChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values1.put(GroupUpcomingChallengesDB.CHALLENGER_GROUP_ID, jsonObject1.getString("challenger_group_id"));
                                            values1.put(GroupUpcomingChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values1.put(GroupUpcomingChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values1.put(GroupUpcomingChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values1.put(GroupUpcomingChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values1.put(GroupUpcomingChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values1.put(GroupUpcomingChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values1.put(GroupUpcomingChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values1.put(GroupUpcomingChallengesDB.PAUSE_ACCESS, jsonObject1.getString("pause_access"));
                                            values1.put(GroupUpcomingChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values1.put(GroupUpcomingChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values1.put(GroupUpcomingChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values1.put(GroupUpcomingChallengesDB.WINNER,  jsonObject1.getString("winner"));
                                            values1.put(GroupUpcomingChallengesDB.PAUSED_BY,  jsonObject1.getString("paused_by"));
                                            values1.put(GroupUpcomingChallengesDB.CHALLENGE_TYPE,  jsonObject1.getString("challenge_type"));
                                            values1.put(GroupUpcomingChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values1.put(GroupUpcomingChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values1.put(GroupUpcomingChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values1.put(GroupUpcomingChallengesDB.GET_GPS, jsonObject1.getString("gps"));
                                            groupUpcomingChallengesDB.addGroupUpcomingChallengesList(values1);
                                            /*ChallengeGroupDetailModel am_up = new ChallengeGroupDetailModel();
                                            am_up.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_up.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_up.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_up.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_up.setStatus(jsonObject1.getString("status"));
                                            am_up.setAmount(jsonObject1.getString("amount"));
                                            am_up.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_up.setUser_type(jsonObject1.getString("user_type"));
                                            am_up.setUser_type(jsonObject1.getString("user_id"));
                                            am_up.setUser_name(jsonObject1.getString("user_name"));
                                            am_up.setUser_type(jsonObject1.getString("challenger_group_id"));
                                            am_up.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_up.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_up.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_up.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_up.setStart_on(jsonObject1.getString("start_on"));
                                            am_up.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_up.setResume_on(jsonObject1.getString("resume_on"));
                                            am_up.setPause_access(jsonObject1.getString("pause_access"));
                                            am_up.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_up.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_up.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupUpcomList.add(am_up);*/

                                        }
                                    } else {
                                        ll_upcoming.setVisibility(View.GONE);
                                    }
                                    //complete
                                    if (completed_cnt != 0) {
                                       // ContentValues values2 = new ContentValues();
                                        JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                        Log.d("COMPL", jsonArraycomplet.toString());
                                        for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                            JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                            values2.put(GroupCompletedChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                            values2.put(GroupCompletedChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                            values2.put(GroupCompletedChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                            values2.put(GroupCompletedChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                            values2.put(GroupCompletedChallengesDB.STATUS, jsonObject1.getString("status"));
                                            values2.put(GroupCompletedChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                            values2.put(GroupCompletedChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                            values2.put(GroupCompletedChallengesDB.USER_TYPE, jsonObject1.getString("user_type"));
                                            values2.put(GroupCompletedChallengesDB.USER_ID, jsonObject1.getString("user_id"));
                                            values2.put(GroupCompletedChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                            values2.put(GroupCompletedChallengesDB.CHALLENGER_GROUP_ID, jsonObject1.getString("challenger_group_id"));
                                            values2.put(GroupCompletedChallengesDB.ACTIVITY_NAME, jsonObject1.getString("activity_name"));
                                            values2.put(GroupCompletedChallengesDB.ACTIVITY_ID, jsonObject1.getString("activity_id"));
                                            values2.put(GroupCompletedChallengesDB.EVALUATION_FACTOR, jsonObject1.getString("evaluation_factor"));
                                            values2.put(GroupCompletedChallengesDB.EVALUATION_FACTOR_UNIT, jsonObject1.getString("evaluation_factor_unit"));
                                            values2.put(GroupCompletedChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                            values2.put(GroupCompletedChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                            values2.put(GroupCompletedChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                            values2.put(GroupCompletedChallengesDB.PAUSE_ACCESS, jsonObject1.getString("pause_access"));
                                            values2.put(GroupCompletedChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                            values2.put(GroupCompletedChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                            values2.put(GroupCompletedChallengesDB.ACTIVITY_IMAGE, AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            values2.put(GroupCompletedChallengesDB.WINNER,  jsonObject1.getString("winner"));
                                            values2.put(GroupCompletedChallengesDB.PAUSED_BY,  jsonObject1.getString("paused_by"));
                                            values2.put(GroupCompletedChallengesDB.CHALLENGE_TYPE,  jsonObject1.getString("challenge_type"));
                                            values2.put(GroupCompletedChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                            values2.put(GroupCompletedChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                            values2.put(GroupCompletedChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                            values2.put(GroupCompletedChallengesDB.GET_GPS, jsonObject1.getString("gps"));

                                            groupCompletedChallengesDB.addGroupCompletedChallengesList(values2);
                                            /*ChallengeGroupDetailModel am_complet = new ChallengeGroupDetailModel();
                                            am_complet.setChallenge_id(jsonObject1.getString("challenge_id"));
                                            am_complet.setOpponent_name(jsonObject1.getString("opponent_name"));
                                            am_complet.setWinning_status(jsonObject1.getString("winning_status"));
                                            am_complet.setIs_group_admin(jsonObject1.getString("is_group_admin"));
                                            am_complet.setStatus(jsonObject1.getString("status"));
                                            am_complet.setAmount(jsonObject1.getString("amount"));
                                            am_complet.setWinning_amount(jsonObject1.getString("winning_amount"));
                                            am_complet.setUser_type(jsonObject1.getString("user_type"));
                                            am_complet.setUser_type(jsonObject1.getString("user_id"));
                                            am_complet.setUser_name(jsonObject1.getString("user_name"));
                                            am_complet.setUser_type(jsonObject1.getString("challenger_group_id"));
                                            am_complet.setActivity_name(jsonObject1.getString("activity_name"));
                                            am_complet.setActivity_id(jsonObject1.getString("activity_id"));
                                            am_complet.setEvaluation_factor(jsonObject1.getString("evaluation_factor"));
                                            am_complet.setEvaluation_factor_unit(jsonObject1.getString("evaluation_factor_unit"));
                                            am_complet.setStart_on(jsonObject1.getString("start_on"));
                                            am_complet.setPaused_on(jsonObject1.getString("paused_on"));
                                            am_complet.setResume_on(jsonObject1.getString("resume_on"));
                                            am_complet.setPause_access(jsonObject1.getString("pause_access"));
                                            am_complet.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                            am_complet.setCompleted_on(jsonObject1.getString("completed_on"));
                                            am_complet.setActivity_image(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("activity_image"));
                                            challengGroupCompletcomList.add(am_complet);
*/
                                        }
                                    } else {
                                        ll_completed.setVisibility(View.GONE);
                                    }


                                    // Completed RECYCLER

                                    // UPCOMING RECYCLER

                                    individualRunningChallenges();
                                    individualUpcommingChallenges();
                                    individualCompletedChallenges();
                                   // grp_running_recycler.setAdapter(challengGroupRunAdpter);
                                   // grp_upcoming_recycler.setAdapter(challengGroupUppAdpter);
                                  //  grp_completed_recycler.setAdapter(challengGroupCompletAdpter);

                                }
                                if (status.equals("10200")) {
                                    Toast.makeText(getActivity(), "No Challenges Found...!", Toast.LENGTH_LONG).show();
                                }
                                if (status.equals("10300")) {
                                    no_data_image.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "No data Found..!", Toast.LENGTH_LONG).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GGGGG_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            individualRunningChallenges();
            individualUpcommingChallenges();
            individualCompletedChallenges();
          //  Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == running_more_text) {
            Intent irunmore = new Intent(getActivity(), AllRunningChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }
        if (view == upcoming_more_text) {
            Intent irunmore = new Intent(getActivity(), AllUpcomingChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }
        if (view == complet_more_text) {
            Intent irunmore = new Intent(getActivity(), AllCompletedChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }

        if (view == linear_sponsor) {
            Intent sponsor = new Intent(getActivity(), SponsorRequestActivity.class);
            startActivity(sponsor);

        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(getActivity(), ChallengeRequestActivity.class);
            startActivity(chellenge);

        }
        if (view == linear_messges) {
            Intent msg = new Intent(getActivity(), ChatMessgesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);

        }

    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {


            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
           // Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
    private void individualRunningChallenges() {
        challengGroupRunList.clear();
        List<String> activityName = groupRunningChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = groupRunningChallengesDB.getChallengeID();
            List<String> opponent_name = groupRunningChallengesDB.getOpponentName();
            List<String> winning_status = groupRunningChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupRunningChallengesDB.getIsGroupAdmin();
            List<String> status = groupRunningChallengesDB.getStatus();
            List<String> amount = groupRunningChallengesDB.getAmount();
            List<String> winning_amount = groupRunningChallengesDB.getWinningAmount();
            List<String> user_type = groupRunningChallengesDB.getUserType();
            List<String> user_id = groupRunningChallengesDB.getUserID();
            List<String> user_name = groupRunningChallengesDB.getUserName();
            List<String> challenger_group_id = groupRunningChallengesDB.getChallengerGroupId();
            List<String> activity_name = groupRunningChallengesDB.getActivityName();
            List<String> activity_id = groupRunningChallengesDB.getActivityId();
            List<String> evaluation_factor = groupRunningChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = groupRunningChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = groupRunningChallengesDB.getStartOn();
            List<String> paused_on = groupRunningChallengesDB.getPausedOn();
            List<String> resume_on = groupRunningChallengesDB.getResumeOn();
            List<String> pause_access = groupRunningChallengesDB.getPauseAccess();
            List<String> completed_on_txt = groupRunningChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupRunningChallengesDB.getCompletedOn();
            List<String> activity_image = groupRunningChallengesDB.getActivityImage();
            List<String> winner = groupRunningChallengesDB.getWinner();
            List<String> paused_by = groupRunningChallengesDB.getPaused_by();
            List<String> challenge_by = groupRunningChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupRunningChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupRunningChallengesDB.win_RewardValue();
            List<String> is_scratched = groupRunningChallengesDB.isScratched();
            List<String> get_gps = groupRunningChallengesDB.getGPS();
            List<String> opponentTypes = groupRunningChallengesDB.getOpponentType();
            int running_count = groupRunningChallengesDB.getRunningCount();
            Log.d("grouprunningcountSS",""+running_count);
            if(running_count==0)
            {
                ll_running.setVisibility(View.GONE);
            }
            if (running_count>2){
                running_more_text.setVisibility(View.VISIBLE);
            }
            else {
                running_more_text.setVisibility(View.GONE);
            }
            for (int i = 0; i < activityName.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setUser_type(user_type.get(i));
                am_runn.setUser_id(user_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setChallenger_group_id(challenger_group_id.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setPause_access(pause_access.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinnner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_by.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setCount_values(String.valueOf(running_count));
                am_runn.setOpponent_type(opponentTypes.get(i));
                challengGroupRunList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengGroupRunList.toString());

            }
            grp_running_recycler.setAdapter(challengGroupRunAdpter);
        } else {

        }
    }
    private void individualUpcommingChallenges() {
        challengGroupUpcomList.clear();
        List<String> activityName = groupUpcomingChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = groupUpcomingChallengesDB.getChallengeID();
            List<String> opponent_name = groupUpcomingChallengesDB.getOpponentName();
            List<String> winning_status = groupUpcomingChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupUpcomingChallengesDB.getIsGroupAdmin();
            List<String> status = groupUpcomingChallengesDB.getStatus();
            List<String> amount = groupUpcomingChallengesDB.getAmount();
            List<String> winning_amount = groupUpcomingChallengesDB.getWinningAmount();
            List<String> user_type = groupUpcomingChallengesDB.getUserType();
            List<String> user_id = groupUpcomingChallengesDB.getUserID();
            List<String> user_name = groupUpcomingChallengesDB.getUserName();
            List<String> challenger_group_id = groupUpcomingChallengesDB.getChallengerGroupId();
            List<String> activity_name = groupUpcomingChallengesDB.getActivityName();
            List<String> activity_id = groupUpcomingChallengesDB.getActivityId();
            List<String> evaluation_factor = groupUpcomingChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = groupUpcomingChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = groupUpcomingChallengesDB.getStartOn();
            List<String> paused_on = groupUpcomingChallengesDB.getPausedOn();
            List<String> resume_on = groupUpcomingChallengesDB.getResumeOn();
            List<String> pause_access = groupUpcomingChallengesDB.getPauseAccess();
            List<String> completed_on_txt = groupUpcomingChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupUpcomingChallengesDB.getCompletedOn();
            List<String> activity_image = groupUpcomingChallengesDB.getActivityImage();
            List<String> winner = groupUpcomingChallengesDB.getWinner();
            List<String> paused_by = groupUpcomingChallengesDB.getPaused_by();
            List<String> challenge_type = groupUpcomingChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupUpcomingChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupUpcomingChallengesDB.win_RewardValue();
            List<String> is_scratched = groupUpcomingChallengesDB.isScratched();
            List<String> get_gps = groupUpcomingChallengesDB.getGPS();
            int upcoming_count = groupUpcomingChallengesDB.getUpcomingCount();
            Log.d("groupupcomingcountSS",""+upcoming_count);
            if(upcoming_count==0)
            {
                ll_upcoming.setVisibility(View.GONE);
            }
            if (upcoming_count>2){
                upcoming_more_text.setVisibility(View.VISIBLE);
            }
            else {
                upcoming_more_text.setVisibility(View.GONE);
            }

            for (int i = 0; i < activityName.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setUser_type(user_type.get(i));
                am_runn.setUser_id(user_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setChallenger_group_id(challenger_group_id.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setPause_access(pause_access.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinnner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setCount_values(String.valueOf(upcoming_count));
                challengGroupUpcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengGroupUpcomList.toString());

            }
            grp_upcoming_recycler.setAdapter(challengGroupUppAdpter);
        } else {

        }
    }
    private void individualCompletedChallenges() {
        challengGroupCompletcomList.clear();
        List<String> activityName = groupCompletedChallengesDB.getActivityName();
        if (activityName.size() > 0) {
            List<String> challenge_id = groupCompletedChallengesDB.getChallengeID();
            List<String> opponent_name = groupCompletedChallengesDB.getOpponentName();
            List<String> winning_status = groupCompletedChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupCompletedChallengesDB.getIsGroupAdmin();
            List<String> status = groupCompletedChallengesDB.getStatus();
            List<String> amount = groupCompletedChallengesDB.getAmount();
            List<String> winning_amount = groupCompletedChallengesDB.getWinningAmount();
            List<String> user_type = groupCompletedChallengesDB.getUserType();
            List<String> user_id = groupCompletedChallengesDB.getUserID();
            List<String> user_name = groupCompletedChallengesDB.getUserName();
            List<String> challenger_group_id = groupCompletedChallengesDB.getChallengerGroupId();
            List<String> activity_name = groupCompletedChallengesDB.getActivityName();
            List<String> activity_id = groupCompletedChallengesDB.getActivityId();
            List<String> evaluation_factor = groupCompletedChallengesDB.getEvaluationFactor();
            List<String> evaluation_factor_unit = groupCompletedChallengesDB.getEvaluationFactorUnit();
            List<String> start_on = groupCompletedChallengesDB.getStartOn();
            List<String> paused_on = groupCompletedChallengesDB.getPausedOn();
            List<String> resume_on = groupCompletedChallengesDB.getResumeOn();
            List<String> pause_access = groupCompletedChallengesDB.getPauseAccess();
            List<String> completed_on_txt = groupCompletedChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupCompletedChallengesDB.getCompletedOn();
            List<String> activity_image = groupCompletedChallengesDB.getActivityImage();
            List<String> winner = groupCompletedChallengesDB.getWinner();
            List<String> paused_by = groupCompletedChallengesDB.getPaused_by();
            List<String> challenge_type = groupCompletedChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupCompletedChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupCompletedChallengesDB.win_RewardValue();
            List<String> is_scratched = groupCompletedChallengesDB.isScratched();
            List<String> get_gps = groupCompletedChallengesDB.getGPS();

            int completedCount = groupCompletedChallengesDB.getCompletedCount();
            Log.d("groupcomplertedcountSS",""+completedCount);
            if(completedCount==0)
            {
                ll_completed.setVisibility(View.GONE);
            }
            if (completedCount>2){
                complet_more_text.setVisibility(View.VISIBLE);
            }
            else {
                complet_more_text.setVisibility(View.GONE);
            }

            for (int i = 0; i < activityName.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setUser_type(user_type.get(i));
                am_runn.setUser_id(user_id.get(i));
                am_runn.setUser_name(user_name.get(i));
                am_runn.setChallenger_group_id(challenger_group_id.get(i));
                am_runn.setActivity_name(activity_name.get(i));
                am_runn.setActivity_id(activity_id.get(i));
                am_runn.setEvaluation_factor(evaluation_factor.get(i));
                am_runn.setEvaluation_factor_unit(evaluation_factor_unit.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setPause_access(pause_access.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));
                am_runn.setActivity_image(activity_image.get(i));
                am_runn.setWinnner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setGps(get_gps.get(i));
                am_runn.setCount_values(String.valueOf(completedCount));
                challengGroupCompletcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg",challengGroupUpcomList.toString());

            }
            grp_completed_recycler.setAdapter(challengGroupCompletAdpter);
        } else {

        }
    }
}
