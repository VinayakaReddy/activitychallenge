package in.activitychallenge.activitychallenge.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.adapter.PromotionAdapter;
import in.activitychallenge.activitychallenge.models.PromotionModel;
import in.activitychallenge.activitychallenge.utilities.AppUrls;
import in.activitychallenge.activitychallenge.utilities.NetworkChecking;
import in.activitychallenge.activitychallenge.utilities.UserSessionManager;

import static in.activitychallenge.activitychallenge.utilities.AppUrls.MY_PROMOTIONS;

public class MyPromotionFragment extends Fragment {

    View view;
    ImageView default_img;
    RecyclerView promo_recycler;
    PromotionAdapter promoAdap;
    ArrayList<PromotionModel> promoList = new ArrayList<PromotionModel>();
    LinearLayoutManager lMang;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token,user_id,user_type,device_id;
    UserSessionManager userSessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_promotion, container, false);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        default_img = view.findViewById(R.id.default_img);

        promo_recycler=(RecyclerView)view.findViewById(R.id.promo_recycler);
        promoAdap = new PromotionAdapter(promoList, MyPromotionFragment.this, R.layout.row_frag_promotions);
        lMang = new LinearLayoutManager(getActivity());
        promo_recycler.setNestedScrollingEnabled(false);
        promo_recycler.setLayoutManager(lMang);

        getPromotions();

        return view;
    }

    private void getPromotions() {

            checkInternet = NetworkChecking.isConnected(getContext());
            if (checkInternet)
            {
                String url = AppUrls.BASE_URL+MY_PROMOTIONS+user_id+"&type="+user_type;
                Log.d("MyPromotionURL", url);
                StringRequest strRe = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                pprogressDialog.dismiss();

                                Log.d("Promo_RESPON", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("response_code");
                                    if (successResponceCode.equals("10100")) {
                                        pprogressDialog.dismiss();


                                        JSONArray jArr = jsonObject.getJSONArray("data");
                                        for (int i = 0; i<jArr.length(); i++){
                                            PromotionModel rhm = new PromotionModel();
                                            JSONObject itemArray = jArr.getJSONObject(i);

                                            rhm.setPromotion_id(itemArray.getString("promotion_id"));
                                            rhm.setStatus(itemArray.getString("status"));
                                            rhm.setAmount(itemArray.getString("amount"));
                                            rhm.setPromotion_code(itemArray.getString("promotion_code"));
                                            rhm.setBanner_path(AppUrls.BASE_IMAGE_URL+itemArray.getString("banner_path"));
                                            rhm.setCreated_on(itemArray.getString("created_on"));
                                            rhm.setFrom_on(itemArray.getString("from_on"));
                                            rhm.setTo_on(itemArray.getString("to_on"));
                                            rhm.setUser_name(itemArray.getString("user_name"));

                                            promoList.add(rhm);
                                        }
                                        promo_recycler.setAdapter(promoAdap);
                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        promo_recycler.setVisibility(View.GONE);
                                        default_img.setVisibility(View.VISIBLE);

                                    }
                                  /*  if (successResponceCode.equals("10300")) {
                                        pprogressDialog.dismiss();
                                        promo_recycler.setVisibility(View.GONE);
                                        default_img.setVisibility(View.VISIBLE);
                                        Toast.makeText(getContext(), "No data Found", Toast.LENGTH_SHORT).show();
                                    }*/

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token",token);
                        headers.put("x-device-id",device_id);
                        headers.put("x-device-platform","ANDROID");

                        Log.d("PROMO_HEADER", "HEADER "+headers.toString());
                        return headers;
                    }

                };
                strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(strRe);

            }
            else
            {
                pprogressDialog.cancel();
                Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
    }
}
