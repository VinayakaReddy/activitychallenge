package in.activitychallenge.activitychallenge.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluejamesbond.text.DocumentView;

import in.activitychallenge.activitychallenge.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EulaRulesFragment extends Fragment
{
    DocumentView eula_rules_text;
    TextView eula_rules_text_title;
  View view;
    public EulaRulesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_eula_rules, container, false);

      //  eula_rules_text_title=(TextView)view.findViewById(R.id.eula_rules_text_title);
      /*  eula_rules_text=(DocumentView)view.findViewById(R.id.eula_rules_text);
        Resources res = getResources();

        String text = String.format(res.getString(R.string.eule_rule_text));
        eula_rules_text.setText(text);*/
        return  view;
    }

}
