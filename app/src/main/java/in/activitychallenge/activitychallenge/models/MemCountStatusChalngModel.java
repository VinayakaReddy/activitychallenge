package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/8/2017.
 */

public class MemCountStatusChalngModel {

    public String user_id;
    public String name;
    public String evaluation_factor_value;
    public String evaluation_factor_unit;
    public String evaluation_factor;
    public String completed_on_txt;
    public String completed_on;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEvaluation_factor_value() {
        return evaluation_factor_value;
    }

    public void setEvaluation_factor_value(String evaluation_factor_value) {
        this.evaluation_factor_value = evaluation_factor_value;
    }

    public String getEvaluation_factor_unit() {
        return evaluation_factor_unit;
    }

    public void setEvaluation_factor_unit(String evaluation_factor_unit) {
        this.evaluation_factor_unit = evaluation_factor_unit;
    }

    public String getEvaluation_factor() {
        return evaluation_factor;
    }

    public void setEvaluation_factor(String evaluation_factor) {
        this.evaluation_factor = evaluation_factor;
    }

    public String getCompleted_on_txt() {
        return completed_on_txt;
    }

    public void setCompleted_on_txt(String completed_on_txt) {
        this.completed_on_txt = completed_on_txt;
    }

    public String getCompleted_on() {
        return completed_on;
    }

    public void setCompleted_on(String completed_on) {
        this.completed_on = completed_on;
    }
}
