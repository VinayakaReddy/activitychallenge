package in.activitychallenge.activitychallenge.models;

public class SavedCardDetailsModel {

    public String id;
    public String acc_type;
    public String acc_no;
    public String cc_year;
    public String cc_month;
    public String cc_cvv;

    public String getCc_cvv() {
        return cc_cvv;
    }

    public void setCc_cvv(String cc_cvv) {
        this.cc_cvv = cc_cvv;
    }

    public String cc_name_on_card;
    public String created_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAcc_type() {
        return acc_type;
    }

    public void setAcc_type(String acc_type) {
        this.acc_type = acc_type;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getCc_year() {
        return cc_year;
    }

    public void setCc_year(String cc_year) {
        this.cc_year = cc_year;
    }

    public String getCc_month() {
        return cc_month;
    }

    public void setCc_month(String cc_month) {
        this.cc_month = cc_month;
    }

    public String getCc_name_on_card() {
        return cc_name_on_card;
    }

    public void setCc_name_on_card(String cc_name_on_card) {
        this.cc_name_on_card = cc_name_on_card;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}
