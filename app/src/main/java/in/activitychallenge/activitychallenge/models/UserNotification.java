package in.activitychallenge.activitychallenge.models;

import org.json.JSONObject;

import java.io.Serializable;

public class UserNotification implements Serializable {

    public String id, title, text, sentOnText, sentOn;

    public UserNotification(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getString("id");
            this.title = jsonObject.getString("title");
            this.text = jsonObject.getString("text");
            this.sentOnText = jsonObject.getString("sent_on_txt");
            this.sentOn = jsonObject.getString("sent_on");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
