package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class ActivityModel
{
    public String id;
    public String activity_name;
    public String activity_no;
    public String evaluation_factor;
    public String tools_required;
    public String activity_image;
    public String min_value;
    public String min_value_unit;
    public String daily_challenges;
    public String weekly_challenges;

    public String longrun_challenges;

    public String getLongrun_challenges() {
        return longrun_challenges;
    }

    public void setLongrun_challenges(String longrun_challenges) {
        this.longrun_challenges = longrun_challenges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getActivity_no() {
        return activity_no;
    }

    public void setActivity_no(String activity_no) {
        this.activity_no = activity_no;
    }

    public String getEvaluation_factor() {
        return evaluation_factor;
    }

    public void setEvaluation_factor(String evaluation_factor) {
        this.evaluation_factor = evaluation_factor;
    }

    public String getTools_required() {
        return tools_required;
    }

    public void setTools_required(String tools_required) {
        this.tools_required = tools_required;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getMin_value() {
        return min_value;
    }

    public void setMin_value(String min_value) {
        this.min_value = min_value;
    }

    public String getMin_value_unit() {
        return min_value_unit;
    }

    public void setMin_value_unit(String min_value_unit) {
        this.min_value_unit = min_value_unit;
    }

    public String getDaily_challenges() {
        return daily_challenges;
    }

    public void setDaily_challenges(String daily_challenges) {
        this.daily_challenges = daily_challenges;
    }

    public String getWeekly_challenges() {
        return weekly_challenges;
    }

    public void setWeekly_challenges(String weekly_challenges) {
        this.weekly_challenges = weekly_challenges;
    }

    public String users;
    public String active_users;
    public String sponsors;
    public String tot_activities;
    public String total;

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getActive_users() {
        return active_users;
    }

    public void setActive_users(String active_users) {
        this.active_users = active_users;
    }

    public String getSponsors() {
        return sponsors;
    }

    public void setSponsors(String sponsors) {
        this.sponsors = sponsors;
    }

    public String getTot_activities() {
        return tot_activities;
    }

    public void setTot_activities(String tot_activities) {
        this.tot_activities = tot_activities;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
