package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 27-Mar-18.
 */

public class DayDateWiseStatusModel
{

    String date;
    String status;
    String value;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
