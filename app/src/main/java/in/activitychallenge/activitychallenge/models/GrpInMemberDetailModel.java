package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class GrpInMemberDetailModel
{

    public String group_id;
    public String group_name;
    public String admin_id;
    public String conversation_type;
    public String profile_pic;
    public String group_rank;
    public String group_members;
    public String winning_percentage;
    public String win_challenges;
    public String lost_challenges;
    public String imgv1;
    public String imgv2;
    public String imgv3;
    public String imgv4;
    public String imgv5;


    public String getConversation_type() {
        return conversation_type;
    }

    public void setConversation_type(String conversation_type) {
        this.conversation_type = conversation_type;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getGroup_members() {
        return group_members;
    }

    public void setGroup_members(String group_members) {
        this.group_members = group_members;
    }

    public String getGroup_rank() {
        return group_rank;
    }

    public void setGroup_rank(String group_rank) {
        this.group_rank = group_rank;
    }



    public String getLost_challenges() {
        return lost_challenges;
    }

    public void setLost_challenges(String lost_challenges) {
        this.lost_challenges = lost_challenges;
    }


    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }


    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }



    public String getWin_challenges() {
        return win_challenges;
    }

    public void setWin_challenges(String win_challenges) {
        this.win_challenges = win_challenges;
    }



    public String getWinning_percentage() {
        return winning_percentage;
    }

    public void setWinning_percentage(String winning_percentage) {
        this.winning_percentage = winning_percentage;
    }

    public String getImgv1() {
        return imgv1;
    }

    public void setImgv1(String imgv1) {
        this.imgv1 = imgv1;
    }

    public String getImgv2() {
        return imgv2;
    }

    public void setImgv2(String imgv2) {
        this.imgv2 = imgv2;
    }

    public String getImgv3() {
        return imgv3;
    }

    public void setImgv3(String imgv3) {
        this.imgv3 = imgv3;
    }

    public String getImgv4() {
        return imgv4;
    }

    public void setImgv4(String imgv4) {
        this.imgv4 = imgv4;
    }

    public String getImgv5() {
        return imgv5;
    }

    public void setImgv5(String imgv5) {
        this.imgv5 = imgv5;
    }
}
