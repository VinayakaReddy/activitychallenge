package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class ChallengeIndividualRunningModel
{
    public String challenge_id;
    public String activity_id;

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String user_name;
    public String winner;

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String opponent_name;
    public String winning_status;
    public String status;
    public String amount;
    public String activity_name;
    public String activity_image;
    public String start_on;
    public String paused_on;
    public String evaluation_factor;
    public String evaluation_factor_unit;
    public String resume_on;
    public String pause_access;
    public String winning_amount;
    public String is_group_admin;
    public String paused_by;
    public String challenge_type;

    public String winning_reward_type;
    public String winning_reward_value;
    public String is_scratched;
    public String gps;
    public String count_value;

    public String getOpponent_type() {
        return opponent_type;
    }

    public void setOpponent_type(String opponent_type) {
        this.opponent_type = opponent_type;
    }

    public String opponent_type;

    public String getCount_value() {
        return count_value;
    }

    public void setCount_value(String count_value) {
        this.count_value = count_value;
    }

    public String getGps()
    {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getWinning_reward_type() {
        return winning_reward_type;
    }

    public void setWinning_reward_type(String winning_reward_type) {
        this.winning_reward_type = winning_reward_type;
    }

    public String getWinning_reward_value() {
        return winning_reward_value;
    }

    public void setWinning_reward_value(String winning_reward_value) {
        this.winning_reward_value = winning_reward_value;
    }

    public String getIs_scratched() {
        return is_scratched;
    }

    public void setIs_scratched(String is_scratched) {
        this.is_scratched = is_scratched;
    }

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    public String getPaused_by() {
        return paused_by;
    }

    public void setPaused_by(String paused_by) {
        this.paused_by = paused_by;
    }

    public String getIs_group_admin() {
        return is_group_admin;
    }

    public void setIs_group_admin(String is_group_admin) {
        this.is_group_admin = is_group_admin;
    }

    public String getWinning_amount() {
        return winning_amount;
    }

    public void setWinning_amount(String winning_amount) {
        this.winning_amount = winning_amount;
    }

    public String getPause_access() {
        return pause_access;
    }

    public void setPause_access(String pause_access) {
        this.pause_access = pause_access;
    }

    public String getResume_on() {
        return resume_on;
    }

    public void setResume_on(String resume_on) {
        this.resume_on = resume_on;
    }

    public String getCompleted_on_txt() {
        return completed_on_txt;
    }

    public void setCompleted_on_txt(String completed_on_txt) {
        this.completed_on_txt = completed_on_txt;
    }

    public String getCompleted_on() {
        return completed_on;
    }

    public void setCompleted_on(String completed_on) {
        this.completed_on = completed_on;
    }

    public String completed_on_txt;
    public String completed_on;

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getOpponent_name() {
        return opponent_name;
    }

    public void setOpponent_name(String opponent_name) {
        this.opponent_name = opponent_name;
    }

    public String getWinning_status() {
        return winning_status;
    }

    public void setWinning_status(String winning_status) {
        this.winning_status = winning_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getStart_on() {
        return start_on;
    }

    public void setStart_on(String start_on) {
        this.start_on = start_on;
    }

    public String getPaused_on() {
        return paused_on;
    }

    public void setPaused_on(String paused_on) {
        this.paused_on = paused_on;
    }

    public String getEvaluation_factor() {
        return evaluation_factor;
    }

    public void setEvaluation_factor(String evaluation_factor) {
        this.evaluation_factor = evaluation_factor;
    }

    public String getEvaluation_factor_unit() {
        return evaluation_factor_unit;
    }

    public void setEvaluation_factor_unit(String evaluation_factor_unit) {
        this.evaluation_factor_unit = evaluation_factor_unit;
    }
}
