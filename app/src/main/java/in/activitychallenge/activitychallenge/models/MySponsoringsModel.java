package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/4/2017.
 */

public class MySponsoringsModel {
    String name;
    String profile_pic;
    String status;
    String date;
    String amount;
    String to_sponsor_type;

    public String getTo_sponsor_type() {
        return to_sponsor_type;
    }

    public void setTo_sponsor_type(String to_sponsor_type) {
        this.to_sponsor_type = to_sponsor_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
