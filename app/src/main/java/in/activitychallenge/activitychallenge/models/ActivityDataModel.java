package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class ActivityDataModel
{
    public String activity_name;
    public String activity_image;
    public String total_challenges;
    public String tie_challenges;
    public String win_challenges;
    public String loss_challenges;
    public String tie_amount;
    public String win_amount;
    public String loss_amount;
    public String total_amount;

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getTotal_challenges() {
        return total_challenges;
    }

    public void setTotal_challenges(String total_challenges) {
        this.total_challenges = total_challenges;
    }

    public String getTie_challenges() {
        return tie_challenges;
    }

    public void setTie_challenges(String tie_challenges) {
        this.tie_challenges = tie_challenges;
    }

    public String getWin_challenges() {
        return win_challenges;
    }

    public void setWin_challenges(String win_challenges) {
        this.win_challenges = win_challenges;
    }

    public String getLoss_challenges() {
        return loss_challenges;
    }

    public void setLoss_challenges(String loss_challenges) {
        this.loss_challenges = loss_challenges;
    }

    public String getTie_amount() {
        return tie_amount;
    }

    public void setTie_amount(String tie_amount) {
        this.tie_amount = tie_amount;
    }

    public String getWin_amount() {
        return win_amount;
    }

    public void setWin_amount(String win_amount) {
        this.win_amount = win_amount;
    }

    public String getLoss_amount() {
        return loss_amount;
    }

    public void setLoss_amount(String loss_amount) {
        this.loss_amount = loss_amount;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }
}
