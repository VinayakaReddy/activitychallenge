package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/26/2017.
 */

public class UserRunningChallengesModel {

    public int getTotal_challenges() {
        return total_challenges;
    }

    public void setTotal_challenges(int total_challenges) {
        this.total_challenges = total_challenges;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }



    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getOpponent_type() {
        return opponent_type;
    }

    public void setOpponent_type(String opponent_type) {
        this.opponent_type = opponent_type;
    }

    public String getOpponent_name() {
        return opponent_name;
    }

    public void setOpponent_name(String opponent_name) {
        this.opponent_name = opponent_name;
    }

    public String getWinning_status() {
        return winning_status;
    }

    public void setWinning_status(String winning_status) {
        this.winning_status = winning_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getStart_on() {
        return start_on;
    }

    public void setStart_on(String start_on) {
        this.start_on = start_on;
    }

    public String getPaused_on() {
        return paused_on;
    }

    public void setPaused_on(String paused_on) {
        this.paused_on = paused_on;
    }

    public String getCompleted_on_txt() {
        return completed_on_txt;
    }

    public void setCompleted_on_txt(String completed_on_txt) {
        this.completed_on_txt = completed_on_txt;
    }

    public String getCompleted_on() {
        return completed_on;
    }

    public void setCompleted_on(String completed_on) {
        this.completed_on = completed_on;
    }

    public int total_challenges, amount;
    public String challenge_id;

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String activity_image;
    public String opponent_type;
    public String opponent_name;
    public String winning_status;
    public String status;
    public String activity_name;
    public String start_on;
    public String paused_on;
    public String completed_on_txt;
    public String completed_on;

    public String getGetGps() {
        return getGps;
    }

    public void setGetGps(String getGps) {
        this.getGps = getGps;
    }

    public String evaluation_factor;
    public String evaluation_factor_unit;
    public String activity_id;
    public String getGps;



    public String getEvaluation_factor() {
        return evaluation_factor;
    }

    public void setEvaluation_factor(String evaluation_factor) {
        this.evaluation_factor = evaluation_factor;
    }

    public String getEvaluation_factor_unit() {
        return evaluation_factor_unit;
    }

    public void setEvaluation_factor_unit(String evaluation_factor_unit) {
        this.evaluation_factor_unit = evaluation_factor_unit;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }
}
