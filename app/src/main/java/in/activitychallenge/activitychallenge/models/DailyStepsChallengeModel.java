package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class DailyStepsChallengeModel
{
    public String id;
    public String factor_eval;
    public String factor_goal_val;
    public String factor_eval_unit;
    public String image;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFactor_eval() {
        return factor_eval;
    }

    public void setFactor_eval(String factor_eval) {
        this.factor_eval = factor_eval;
    }

    public String getFactor_goal_val() {
        return factor_goal_val;
    }

    public void setFactor_goal_val(String factor_goal_val) {
        this.factor_goal_val = factor_goal_val;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFactor_eval_unit() {
        return factor_eval_unit;
    }

    public void setFactor_eval_unit(String factor_eval_unit) {
        this.factor_eval_unit = factor_eval_unit;
    }
}
