package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/15/2017.
 */

public class UserTopFiveActivitiesModel {

    public String activity_id;
    public String activity_image;
    public String title;


    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
