package in.activitychallenge.activitychallenge.models;

public class MyChallengeModel {

    public String user_wallet;
    public String group_wallet;

    public String getUser_wallet() {
        return user_wallet;
    }

    public void setUser_wallet(String user_wallet) {
        this.user_wallet = user_wallet;
    }

    public String getGroup_wallet() {
        return group_wallet;
    }

    public void setGroup_wallet(String group_wallet) {
        this.group_wallet = group_wallet;
    }

    public String id;
    public  String status;
    public  String amount;
    public  String user_pic;
    public  String group_pic;
    public  String user_name;
    public  String group_name;
    public  String activity_name;
    public  String user_rank;
    public  String group_rank;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    public String getGroup_pic() {
        return group_pic;
    }

    public void setGroup_pic(String group_pic) {
        this.group_pic = group_pic;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getUser_rank() {
        return user_rank;
    }

    public void setUser_rank(String user_rank) {
        this.user_rank = user_rank;
    }

    public String getGroup_rank() {
        return group_rank;
    }

    public void setGroup_rank(String group_rank) {
        this.group_rank = group_rank;
    }
}
