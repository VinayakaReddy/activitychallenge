package in.activitychallenge.activitychallenge.models;

public class SponsorRequestModel {

    public String id;
    public String amount_paid;
    public  String transaction_id;
    public  String sponsor_type;

    public String getSponsor_type() {
        return sponsor_type;
    }

    public void setSponsor_type(String sponsor_type) {
        this.sponsor_type = sponsor_type;
    }

    public  String status;
    public  String sponsor_id;
    public  String sponsor_image;
    public  String sponsor_name;

    public String getSponsor_name() {
        return sponsor_name;
    }

    public void setSponsor_name(String sponsor_name) {
        this.sponsor_name = sponsor_name;
    }

    public String getSponsor_image() {
        return sponsor_image;
    }

    public void setSponsor_image(String sponsor_image) {
        this.sponsor_image = sponsor_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount_paid() {
        return amount_paid;
    }

    public void setAmount_paid(String amount_paid) {
        this.amount_paid = amount_paid;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSponsor_id() {
        return sponsor_id;
    }

    public void setSponsor_id(String sponsor_id) {
        this.sponsor_id = sponsor_id;
    }
}
