package in.activitychallenge.activitychallenge.models;

public class GroupPromotionModel {

    public String promotion_id;
    public String promotion_code;
    public String banner_path;
    public String status;
    public int amount;
    public String created_on;
    public String from_on;
    public String to_on;
    public String group_name;
    public String group_id;

    public String getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(String promotion_id) {
        this.promotion_id = promotion_id;
    }

    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public String getBanner_path() {
        return banner_path;
    }

    public void setBanner_path(String banner_path) {
        this.banner_path = banner_path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getFrom_on() {
        return from_on;
    }

    public void setFrom_on(String from_on) {
        this.from_on = from_on;
    }

    public String getTo_on() {
        return to_on;
    }

    public void setTo_on(String to_on) {
        this.to_on = to_on;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }
}
