package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/18/2017.
 */

public class AllGroupsModel {
    public String id;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String adminId;
    public String name;
    public String group_pic;
    public String group_members;
    public String total_win;
    public String total_loss;
    public String winning_per;
    public String overall_rank;

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) {
        this.img5 = img5;
    }

    public String img1;
    public String img2;
    public String img3;
    public String img4;
    public String img5;

    public String getOverall_rank() {
        return overall_rank;
    }

    public void setOverall_rank(String overall_rank) {
        this.overall_rank = overall_rank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup_pic() {
        return group_pic;
    }

    public void setGroup_pic(String group_pic) {
        this.group_pic = group_pic;
    }

    public String getGroup_members() {
        return group_members;
    }

    public void setGroup_members(String group_members) {
        this.group_members = group_members;
    }

    public String getTotal_win() {
        return total_win;
    }

    public void setTotal_win(String total_win) {
        this.total_win = total_win;
    }

    public String getTotal_loss() {
        return total_loss;
    }

    public void setTotal_loss(String total_loss) {
        this.total_loss = total_loss;
    }

    public String getWinning_per() {
        return winning_per;
    }

    public void setWinning_per(String winning_per) {
        this.winning_per = winning_per;
    }
}
