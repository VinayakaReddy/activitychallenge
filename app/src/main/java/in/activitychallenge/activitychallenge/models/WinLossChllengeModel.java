package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class WinLossChllengeModel
{
    public String challenge_id;
    public String opponent_name;
    public String winning_status;
    public String amount;
    public String activity_name;
    public String activity_image;
    public String completed_on_txt;
    public String completed_on;
    public String status;
    public String winning_amount;

    public String getWinning_amount() {
        return winning_amount;
    }

    public void setWinning_amount(String winning_amount) {
        this.winning_amount = winning_amount;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getOpponent_name() {
        return opponent_name;
    }

    public void setOpponent_name(String opponent_name) {
        this.opponent_name = opponent_name;
    }

    public String getWinning_status() {
        return winning_status;
    }

    public void setWinning_status(String winning_status) {
        this.winning_status = winning_status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getCompleted_on_txt() {
        return completed_on_txt;
    }

    public void setCompleted_on_txt(String completed_on_txt) {
        this.completed_on_txt = completed_on_txt;
    }

    public String getCompleted_on() {
        return completed_on;
    }

    public void setCompleted_on(String completed_on) {
        this.completed_on = completed_on;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
