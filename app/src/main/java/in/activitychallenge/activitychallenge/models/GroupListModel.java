package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class GroupListModel
{
    public String id;
    public String from_name;
    public String profile_pic;
    public String msg;
    public String conversation_type;
    public String is_read;
    public String sent_on_txt;
    public String sent_on;

    public String getSent_on() {
        return sent_on;
    }

    public void setSent_on(String sent_on) {
        this.sent_on = sent_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getConversation_type() {
        return conversation_type;
    }

    public void setConversation_type(String conversation_type) {
        this.conversation_type = conversation_type;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getSent_on_txt() {
        return sent_on_txt;
    }

    public void setSent_on_txt(String sent_on_txt) {
        this.sent_on_txt = sent_on_txt;
    }
}
