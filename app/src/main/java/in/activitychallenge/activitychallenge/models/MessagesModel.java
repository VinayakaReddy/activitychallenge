package in.activitychallenge.activitychallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class MessagesModel
{
    public String from_id;
    public String from_name;
    public String from_user_type;
    public String profile_pic;
    public String msg;
    public String is_read;
    public String sent_on_txt;
    public String sent_on;

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getFrom_user_type() {
        return from_user_type;
    }

    public void setFrom_user_type(String from_user_type) {
        this.from_user_type = from_user_type;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getSent_on_txt() {
        return sent_on_txt;
    }

    public void setSent_on_txt(String sent_on_txt) {
        this.sent_on_txt = sent_on_txt;
    }

    public String getSent_on() {
        return sent_on;
    }

    public void setSent_on(String sent_on) {
        this.sent_on = sent_on;
    }
}
