package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/21/2017.
 */

public class PromotionModel {

    public String promotion_id;
    public String status;
    public String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String promotion_code;
    public String banner_path;
    public String created_on;
    public String from_on;
    public String to_on;
    public String user_name;

    public String getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(String promotion_id) {
        this.promotion_id = promotion_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public String getBanner_path() {
        return banner_path;
    }

    public void setBanner_path(String banner_path) {
        this.banner_path = banner_path;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getFrom_on() {
        return from_on;
    }

    public void setFrom_on(String from_on) {
        this.from_on = from_on;
    }

    public String getTo_on() {
        return to_on;
    }

    public void setTo_on(String to_on) {
        this.to_on = to_on;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
