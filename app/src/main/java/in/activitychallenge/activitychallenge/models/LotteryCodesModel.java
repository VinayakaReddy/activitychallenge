package in.activitychallenge.activitychallenge.models;

public class LotteryCodesModel
{
    public String lotid;
    public String lottery_codes;
    public String dates;

    public String getLotid() {
        return lotid;
    }

    public void setLotid(String lotid) {
        this.lotid = lotid;
    }



    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getLottery_codes() {
        return lottery_codes;
    }

    public void setLottery_codes(String lottery_codes) {
        this.lottery_codes = lottery_codes;
    }
}
