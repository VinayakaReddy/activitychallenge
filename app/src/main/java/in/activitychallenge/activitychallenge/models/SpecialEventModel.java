package in.activitychallenge.activitychallenge.models;

import org.json.JSONArray;

import java.util.ArrayList;

public class SpecialEventModel {
    public String id;
    public String eventName;
    public String eventDescription;
    public int followstatus;
    public int chalengestatus;
    public String roadmap;

    public int userfollowed;
    public String start_on;



    public String end_on;
    public int no_of_days;

    public JSONArray getLocationArr() {
        return locationArr;
    }

    public void setLocationArr(JSONArray locationArr) {
        this.locationArr = locationArr;
    }

    public JSONArray locationArr;



    public int getNo_of_days() {
        return no_of_days;
    }

    public void setNo_of_days(int no_of_days) {
        this.no_of_days = no_of_days;
    }

    public String getStart_on() {
        return start_on;
    }

    public void setStart_on(String start_on) {
        this.start_on = start_on;
    }

    public String getEnd_on() {
        return end_on;
    }

    public void setEnd_on(String end_on) {
        this.end_on = end_on;
    }

    public int getUserfollowed() {
        return userfollowed;
    }

    public void setUserfollowed(int userfollowed) {
        this.userfollowed = userfollowed;
    }

    public String getRoadmap() {

        return roadmap;
    }

    public void setRoadmap(String roadmap) {
        this.roadmap = roadmap;
    }





    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getFollowstatus() {
        return followstatus;
    }

    public void setFollowstatus(int followstatus) {
        this.followstatus = followstatus;
    }

    public int getChalengestatus() {
        return chalengestatus;
    }

    public void setChalengestatus(int chalengestatus) {
        this.chalengestatus = chalengestatus;
    }

    public String getEventName() {
        return eventName;

    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
