package in.activitychallenge.activitychallenge.models;

public class SpecialEventNotificationModel {
    public String notificationtext;
    public String createddate;

    public String getNotificationtext() {
        return notificationtext;
    }

    public void setNotificationtext(String notificationtext) {
        this.notificationtext = notificationtext;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }


}
