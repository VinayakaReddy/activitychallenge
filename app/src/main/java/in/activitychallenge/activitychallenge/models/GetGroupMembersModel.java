package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/19/2017.
 */

public class GetGroupMembersModel {


    public int user_rank_M, win_challenges_M, lost_challenges_M, winning_percentage_M;
    public String group_user_type_M;
    public String id_M;
    public String user_name_M;
    public String profile_pic_M;
    public String img1_M;
    public String img2_M;
    public String img3_M;
    public String img4_M;
    public String img5_M;


    public int getUser_rank_M() {
        return user_rank_M;
    }

    public void setUser_rank_M(int user_rank_M) {
        this.user_rank_M = user_rank_M;
    }

    public int getWin_challenges_M() {
        return win_challenges_M;
    }

    public void setWin_challenges_M(int win_challenges_M) {
        this.win_challenges_M = win_challenges_M;
    }

    public int getLost_challenges_M() {
        return lost_challenges_M;
    }

    public void setLost_challenges_M(int lost_challenges_M) {
        this.lost_challenges_M = lost_challenges_M;
    }

    public int getWinning_percentage_M() {
        return winning_percentage_M;
    }

    public void setWinning_percentage_M(int winning_percentage_M) {
        this.winning_percentage_M = winning_percentage_M;
    }

    public String getGroup_user_type_M() {
        return group_user_type_M;
    }

    public void setGroup_user_type_M(String group_user_type_M) {
        this.group_user_type_M = group_user_type_M;
    }

    public String getId_M() {
        return id_M;
    }

    public void setId_M(String id_M) {
        this.id_M = id_M;
    }

    public String getUser_name_M() {
        return user_name_M;
    }

    public void setUser_name_M(String user_name_M) {
        this.user_name_M = user_name_M;
    }

    public String getProfile_pic_M() {
        return profile_pic_M;
    }

    public void setProfile_pic_M(String profile_pic_M) {
        this.profile_pic_M = profile_pic_M;
    }

    public String getImg1_M() {
        return img1_M;
    }

    public void setImg1_M(String img1_M) {
        this.img1_M = img1_M;
    }

    public String getImg2_M() {
        return img2_M;
    }

    public void setImg2_M(String img2_M) {
        this.img2_M = img2_M;
    }

    public String getImg3_M() {
        return img3_M;
    }

    public void setImg3_M(String img3_M) {
        this.img3_M = img3_M;
    }

    public String getImg4_M() {
        return img4_M;
    }

    public void setImg4_M(String img4_M) {
        this.img4_M = img4_M;
    }

    public String getImg5_M() {
        return img5_M;
    }

    public void setImg5_M(String img5_M) {
        this.img5_M = img5_M;
    }


}
