package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/18/2017.
 */

public class DeactivateReasonModel {
    public String id;
    public String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
