package in.activitychallenge.activitychallenge.models;

public class RecentHistoryModel
{
    public String id;
    public String user_id;
    public String user_type;
    public String title;
    public String txn_type;
    public String txn_flag;
    public String txn_status;
    public String amount;
    public String created_on_txt;
    public String created_on;
    public String for_span;
   public String updated_on_txt;
    public String updated_on;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(String txn_type) {
        this.txn_type = txn_type;
    }

    public String getTxn_flag() {
        return txn_flag;
    }

    public void setTxn_flag(String txn_flag) {
        this.txn_flag = txn_flag;
    }

    public String getTxn_status() {
        return txn_status;
    }

    public void setTxn_status(String txn_status) {
        this.txn_status = txn_status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreated_on_txt() {
        return created_on_txt;
    }

    public void setCreated_on_txt(String created_on_txt) {
        this.created_on_txt = created_on_txt;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getUpdated_on_txt() {
        return updated_on_txt;
    }

    public void setUpdated_on_txt(String updated_on_txt) {
        this.updated_on_txt = updated_on_txt;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }


    public String getFor_span() {
        return for_span;
    }

    public void setFor_span(String for_span) {
        this.for_span = for_span;
    }


}
