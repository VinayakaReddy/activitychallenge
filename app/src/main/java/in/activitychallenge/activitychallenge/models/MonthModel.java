package in.activitychallenge.activitychallenge.models;

/**
 * Created by admin on 12/8/2017.
 */

public class MonthModel {

    public String id;
    public String for_month;
    public String per_ticket_price;
    public String lottery_amount;
    public String status;
    public String amount_collected;
    public String created_on_txt;
    public String created_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFor_month() {
        return for_month;
    }

    public void setFor_month(String for_month) {
        this.for_month = for_month;
    }

    public String getPer_ticket_price() {
        return per_ticket_price;
    }

    public void setPer_ticket_price(String per_ticket_price) {
        this.per_ticket_price = per_ticket_price;
    }

    public String getLottery_amount() {
        return lottery_amount;
    }

    public void setLottery_amount(String lottery_amount) {
        this.lottery_amount = lottery_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount_collected() {
        return amount_collected;
    }

    public void setAmount_collected(String amount_collected) {
        this.amount_collected = amount_collected;
    }

    public String getCreated_on_txt() {
        return created_on_txt;
    }

    public void setCreated_on_txt(String created_on_txt) {
        this.created_on_txt = created_on_txt;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}
