package in.activitychallenge.activitychallenge.utilities;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.activities.StepChallengeStatusActivity;

public class StepsCountService extends Service implements SensorEventListener{
    SensorManager sensorManager;
    Sensor stepCounter;
    int steps;
    private boolean checkInternet;
    StepChallengeStatusActivity activity;
    UserSessionManager session;
    String token = "", device_id = "",user_id;
    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
    String activityid,challengeid,evaluation_factor,group_id,android_id,evaluation_factor_unit,device_name,device_model;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("Oncreate",">>>>");
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        stepCounter = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        SharedPreferences preferences=getSharedPreferences("StepsCount",MODE_PRIVATE);
        steps=preferences.getInt("steps",0);

        session = new UserSessionManager(StepsCountService.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.v("Oncreate",">>>>"+steps+"...."+device_id);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // activity=StepChallengeStatusActivity.instance;
        SharedPreferences preferences=getSharedPreferences("StepsCount",MODE_PRIVATE);
        activityid=preferences.getString("activityid","");
        challengeid=preferences.getString("challenge_id","");
        evaluation_factor=preferences.getString("evaluation_factor","");
        evaluation_factor_unit=preferences.getString("evaluation_factor_unit","");
        group_id=preferences.getString("groupid","");
        Log.v("FromService",activityid+">>"+challengeid+">>"+evaluation_factor+">>"+evaluation_factor_unit+">>"
        +android_id+">>"+device_name+">>"+device_model);
        sensorManager.registerListener(StepsCountService.this, stepCounter, SensorManager.SENSOR_DELAY_NORMAL);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        float[] values = event.values;
        int value = -1;
        if (values.length > 0) {
            value = (int) values[0];
        }
        if (sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            steps++;
            SharedPreferences preferences=getSharedPreferences("StepsCount",MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();
            editor.putInt("steps",steps);
            editor.apply();
            Intent intent = new Intent("YourAction");
            Bundle bundle = new Bundle();
            bundle.putInt("steps", steps);
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            Log.d("onSensorChanged", "value: " + steps);

            // <-- this value does not increase after app stopped
            Toast.makeText(this," "+steps,Toast.LENGTH_SHORT).show();
            scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                public void run() {
                sendChallenge(steps);
                }
            }, 0, 1, TimeUnit.MINUTES);


        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }



    public void sendChallenge(final int steps) {
        checkInternet = NetworkChecking.isConnected(StepsCountService.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS;
            Log.d("from service", url);

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.CHALLENGE_DETAILS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHAL_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                       /*  if(activity!=null){
                                             activity.getChallengeStatus();
                                         }*/
                                       // Toast.makeText(StepsCountService.this, "From service Challenge Updating", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                   // Toast.makeText(StepsCountService.this, "Challenge Updating Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                  //  Toast.makeText(StepsCountService.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                        params.put("activity_id", activityid);
                        params.put("challenge_id", challengeid);
                        params.put("user_id", user_id);
                        params.put("group_id", "");
                        params.put("evaluation_factor", evaluation_factor);
                        params.put("evaluation_factor_unit", evaluation_factor_unit);
                        params.put("evaluation_factor_value", String.valueOf(steps));
                        params.put("device_id", device_id);
                        params.put("device_name", Build.BRAND);
                        params.put("device_model", Build.MODEL);
                        if(group_id!=null && group_id.length()>0)
                            params.put("group_id",group_id);
                        else
                            params.put("group_id","");
                        Log.d("CHALLENGE_RESPONSE:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(StepsCountService.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(StepsCountService.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

}


