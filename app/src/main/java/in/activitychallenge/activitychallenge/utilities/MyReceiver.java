package in.activitychallenge.activitychallenge.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Devolper on 09-Jan-18.
 */

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent myIntent = new Intent(context, BackServices.class);
        context.startService(myIntent);

    }
}