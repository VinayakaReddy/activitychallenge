package in.activitychallenge.activitychallenge.utilities;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.R;
import in.activitychallenge.activitychallenge.activities.KmChallengeStatusActivity;

public class CalculateDistance extends Service {
    public static final int notify = 60000;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    SharedPreferences sharedPreferences;
    double latitude = 0, longitude = 0;
    StatusKmsChallengeDB statusKmsChallengeDB;
    KmChallengeStatusActivity activity;
    float latestdistance;
    String challengeId;



    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences preferences=getSharedPreferences("Distance",MODE_PRIVATE);
       // latestdistance=preferences.getFloat("latestdistance",0);

        challengeId =   preferences.getString("challengeId","");


        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new GetLocation(), 0, notify);

        statusKmsChallengeDB=new StatusKmsChallengeDB(this);

        latestdistance=statusKmsChallengeDB.latestGoal(challengeId);
        Log.v("TEEEEEEE","((((((((((((((("+latestdistance+"%%%"+challengeId);

    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        new GetLocation().cancel();
        Log.e("Serive is Destoryed","OK");
    }

    class GetLocation extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    calculateDistance();
                }
            });
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
          stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    private void calculateDistance(){
        SharedPreferences sharedPreferences=getSharedPreferences("KM",MODE_PRIVATE);
        if (sharedPreferences.getString("Latitude", "").length() > 0) {
            latitude = Double.parseDouble(sharedPreferences.getString("Latitude", ""));
            longitude = Double.parseDouble(sharedPreferences.getString("Longitude", ""));
        }

        // display toast
        GPSTracker gps = new GPSTracker(this);

        // check if GPS enabled
        if (gps.canGetLocation()) {
            double newLatitude = gps.getLatitude();
            double newLongitude = gps.getLongitude();
                     //   Log.v("old Lat: " + latitude, "" + "old Long: " + longitude);
                     //  Log.v("Lat: " + newLatitude, "" + "Long: " + newLongitude);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Latitude", String.valueOf(newLatitude));
            editor.putString("Longitude", String.valueOf(newLongitude));
            editor.apply();

            if (latitude > 0) {
                Location crntLocation = new Location("crntlocation");
                crntLocation.setLatitude(latitude);
                crntLocation.setLongitude(longitude);

                Location newLocation = new Location("newlocation");
                newLocation.setLatitude(newLatitude);
                newLocation.setLongitude(newLongitude);

                float distance = crntLocation.distanceTo(newLocation);       //   in meters

                if(distance>5){
                    double value=distance * 0.001;
                    float latest=latestdistance+Float.parseFloat(String.valueOf(value));
                    Log.v("distance", " >>> " + value+"ddddd"+latestdistance);
                    Log.v("distance1", " >>> " + latest);
                    statusKmsChallengeDB.updateGoal(String.valueOf(round(latest,4)),challengeId);
                }



            }

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public static float round(float d, int decimalPlace) {
        return BigDecimal.valueOf(d).setScale(decimalPlace,BigDecimal.ROUND_HALF_UP).floatValue();
    }
}
