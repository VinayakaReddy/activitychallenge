package in.activitychallenge.activitychallenge.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ActivityDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 12;
    private static final String DATABASE_NAME = "storeactivities.db";
    private static final String TABLE_DETAILS = "stract";

    public static final String ID = "id";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String ACTIVITY_NO = "activity_no";
    public static final String EVALUATION_FACTOR = "evaluation_factor";
    public static final String TOOLS_REQUIRED = "tools_required";
    public static final String ACTIVITY_IMAGE = "activity_image";
    public static final String MIN_VALUE = "min_value";
    public static final String MIN_VALUE_UNIT = "min_value_unit";
    public static final String DAILY_CHALLENGES = "daily_challenges";
    public static final String WEEKLY_CHALLENGES = "weekly_challenges";
    public static final String LONGRUN_CHALLENGES = "long_run_challenges";


    public ActivityDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ActivityDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + ID + " TEXT ,"
                + ACTIVITY_NAME + " TEXT ,"
                + ACTIVITY_NO + " TEXT ,"
                + EVALUATION_FACTOR + " TEXT ,"
                + TOOLS_REQUIRED + " TEXT ,"
                + ACTIVITY_IMAGE + " TEXT ,"
                + MIN_VALUE + " TEXT ,"
                + MIN_VALUE_UNIT + " TEXT ,"
                + DAILY_CHALLENGES + " TEXT ,"
                + WEEKLY_CHALLENGES + " TEXT ,"
                + LONGRUN_CHALLENGES + " TEXT " +
                 ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);

     //db.execSQL("CREATE INDEX idxModel ON serfrd (NAME);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public void addFriendsList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getId() {
        List<String> id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return id;
    }

    public List<String> getActivityName() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }

    public List<String> getActivityNo() {
        List<String> activity_no = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_no.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return activity_no;
    }

    public List<String> getEvaluationFactor() {
        List<String> evaluation_factor = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                evaluation_factor.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return evaluation_factor;
    }

    public List<String> getToolsRequired() {
        List<String> tools_required = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                tools_required.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return tools_required;
    }

    public List<String> getActivity_image() {
        List<String> activity_image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_image.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return activity_image;
    }public List<String> getMin_value() {
        List<String> min_value = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                min_value.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return min_value;
    }public List<String> getMinimum_valu_unit() {
        List<String> minimum_value_unit = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                minimum_value_unit.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return minimum_value_unit;
    }public List<String> getDaily_challenges() {
        List<String> daily_challenges = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                daily_challenges.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return daily_challenges;
    }
    public List<String> getWeekly_challenges() {
        List<String> weekly_challenges = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                weekly_challenges.add(cursor.getString(9));

            } while (cursor.moveToNext());
        }
        return weekly_challenges;
    }

    public List<String> getLongRun_challenges() {
        List<String> longrun_challenges = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                longrun_challenges.add(cursor.getString(10));

            } while (cursor.moveToNext());
        }
        return longrun_challenges;
    }


    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public Cursor retrieve(String searchTerm)
    {
        String[] columns={ACTIVITY_NAME};
        Cursor c=null;
        SQLiteDatabase db = this.getWritableDatabase();
        if(searchTerm != null && searchTerm.length()>0)
        {
            String sql="SELECT * FROM "+ TABLE_DETAILS+" WHERE "+ ACTIVITY_NAME+" LIKE '%"+searchTerm+"%'";
            c=db.rawQuery(sql,null);
            return c;
        }
        c=db.query(TABLE_DETAILS,columns,null,null,null,null,null);
        return c;
    }

    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
