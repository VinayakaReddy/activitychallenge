package in.activitychallenge.activitychallenge.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class StatusStepChallengeDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 16;
    private static final String DATABASE_NAME = "steps.db";
    public static final String TABLE_DETAILS = "stepsdetails";
    public static final String CHALLENGE_ID="challenge_id";
    public static final String USER_TYPE = "user_type";
    public static final String CHALLENGE_GOAL = "challenge_goal";
    public static final String AMOUNT = "amount";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String USER_COMPLETED_GOAL = "user_completed_goal";
    public static final String USER_NAME = "user_name";
    public static final String USER_RANK = "user_rank";
    public static final String OPPONENT_COMPLETED_GOAL = "opponent_completed_goal";
    public static final String OPPONENT_TYPE = "opponent_type";
    public static final String OPPONENT_ID = "opponent_id";
    public static final String OPPONENT_NAME = "opponent_name";
    public static final String OPPONENT_RANK = "opponent_rank";
    public static final String OPPONENT_IMAGE = "opponent_image";
    public static final String IMAGE = "image";




    public StatusStepChallengeDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public StatusStepChallengeDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("

                + USER_TYPE + " TEXT ,"
                + CHALLENGE_GOAL + " TEXT ,"
                + AMOUNT + " TEXT ,"
                + ACTIVITY_NAME + " TEXT ,"
                + USER_COMPLETED_GOAL + " TEXT ,"
                + USER_NAME + " TEXT ,"
                + USER_RANK + " TEXT ,"
                + OPPONENT_COMPLETED_GOAL + " TEXT ,"
                + OPPONENT_ID + " TEXT ,"
                + OPPONENT_NAME + " TEXT ,"
                + OPPONENT_RANK + " TEXT ,"
                + OPPONENT_TYPE + " TEXT ,"
                + OPPONENT_IMAGE + " TEXT ,"
                + IMAGE + " TEXT ,"
                + CHALLENGE_ID +" VARCHAR "+
                 ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);
    }
    public void updateGoal( String goal,String id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_COMPLETED_GOAL, goal);

        db.update(TABLE_DETAILS, values, CHALLENGE_ID+ " = ?",new String[] { id });

        db.close();

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }
    public void addSteps(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getChallengeID() {
        List<String> challengeId = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challengeId.add(cursor.getString(14));
            } while (cursor.moveToNext());
        }
        return challengeId;
    }

    public List<String> getUserType() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return user_type;
    }
    public List<String> getChallengeGoal() {
        List<String> challenge_goal = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challenge_goal.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return challenge_goal;
    }

    public List<String> getAmount() {
        List<String> amount = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                amount.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return amount;
    }



    public List<String> getActivityName() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }

    public List<String> getUserCompletedGoal() {
        List<String> user_completed_goal = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
              user_completed_goal.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return user_completed_goal;
    }
    public List<String> getUserName() {
        List<String> user_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_name.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return user_name;
    }public List<String> getUserRank() {
        List<String> user_rank = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_rank.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return user_rank;
    }
    public List<String> getImage() {
        List<String> image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                image.add(cursor.getString(13));

            } while (cursor.moveToNext());
        }
        return image;
    }
    public List<String> getopponentType() {
        List<String> opponenttype = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponenttype.add(cursor.getString(11));

            } while (cursor.moveToNext());
        }
        return opponenttype;
    }
    public List<String> getopponentName() {
        List<String> opponentname = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponentname.add(cursor.getString(9));

            } while (cursor.moveToNext());
        }
        return opponentname;
    }

    public List<String> getopponentrank() {
        List<String> opponentrank = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponentrank.add(cursor.getString(10));

            } while (cursor.moveToNext());
        }
        return opponentrank;
    }
    public List<String> getopponentImge() {
        List<String> opponentimage = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponentimage.add(cursor.getString(12));

            } while (cursor.moveToNext());
        }
        return opponentimage;
    }
    public List<String> getopponentgoal() {
        List<String> opponentgoal =new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponentgoal.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return opponentgoal;
    }

    public List<String> getopponentId() {
        List<String> opponentId = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponentId.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return opponentId;
    }
    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public Cursor retrieve(String searchTerm)
    {
        String[] columns={ACTIVITY_NAME};
        Cursor c=null;
        SQLiteDatabase db = this.getWritableDatabase();
        if(searchTerm != null && searchTerm.length()>0)
        {
            String sql="SELECT * FROM "+ TABLE_DETAILS+" WHERE "+ ACTIVITY_NAME+" LIKE '%"+searchTerm+"%'";
            c=db.rawQuery(sql,null);
            return c;
        }
        c=db.query(TABLE_DETAILS,columns,null,null,null,null,null);
        return c;
    }

    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String ChallengeIDstr(String id){
        String challengeid=id;
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlQuery= "SELECT * FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_ID ='"+challengeid +"' ";
        db.rawQuery(sqlQuery,null);
        return  challengeid;
    }
    public int latestGoal(String id){
        int latest=0;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String sqlQuery= "SELECT "+ USER_COMPLETED_GOAL + " FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_ID ='"+id +"' ";

            Cursor cursor= db.rawQuery(sqlQuery,null);
            Log.v("QUERY",">>>"+sqlQuery);
            if(cursor!=null && cursor.getCount()>0){
                if (cursor.moveToFirst()) {
                    do {
                        latest =  cursor.getInt(cursor.getColumnIndex(USER_COMPLETED_GOAL));
                    } while (cursor.moveToNext());

                }
            }
            Log.v("latest",">>>"+latest);


        }catch (Exception e){
              e.printStackTrace();
        }
        return latest;
    }

    public  boolean CheckIsDataAlreadyInDBorNot(String id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query =  "SELECT * FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_ID ='"+id +"' ";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
}
