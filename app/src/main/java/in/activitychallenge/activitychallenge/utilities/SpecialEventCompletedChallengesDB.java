package in.activitychallenge.activitychallenge.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class SpecialEventCompletedChallengesDB  extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 29;
    private static final String DATABASE_NAME = "specialeventcompletedchallenges.db";
    private static final String TABLE_DETAILS = "specialeventcompletedchallengesdetails";
    public static final String CHALLENGE_ID = "challenge_id";
    public static final String ACTIVITY_ID = "activity_id";
    public static final String USER_NAME = "user_name";
    public static final String OPPONENT_NAME = "opponent_name";
    public static final String WINNING_STATUS = "winning_status";
    public static final String IS_GROUP_ADMIN = "is_group_admin";
    public static final String STATUS = "status";
    public static final String AMOUNT = "amount";
    public static final String WINNING_AMOUNT = "winning_amount";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String EVALUATION_FACTOR = "evaluation_factor";
    public static final String EVALUATION_FACTOR_UNIT = "evaluation_factor_unit";
    public static final String START_ON = "start_on";
    public static final String PAUSED_ON = "paused_on";
    public static final String RESUME_ON = "resume_on";
    public static final String COMPLETED_ON_TXT = "completed_on_txt";
    public static final String COMPLETED_ON = "completed_on";
    public static final String ACTIVITY_IMAGE = "activity_image";
    public static final String WINNER = "winner";
    public static final String PAUSED_BY = "paused_by";
    public static final String CHALLENGE_TYPE = "challenge_type";

    public static final String WIN_REWARD_TYPE = "winning_reward_type";
    public static final String WIN_REWARD_VALUE = "winning_reward_value";
    public static final String IS_SCRATCHED = "is_scratched";
    public static final String GET_GPS = "gps";
    public static final String COMPLETED_COUNT = "completed_count";


    public SpecialEventCompletedChallengesDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public SpecialEventCompletedChallengesDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + ACTIVITY_ID + " TEXT ,"
                + USER_NAME + " TEXT ,"
                + OPPONENT_NAME + " TEXT ,"
                + WINNING_STATUS + " TEXT ,"
                + IS_GROUP_ADMIN + " TEXT ,"
                + STATUS + " TEXT ,"
                + AMOUNT + " TEXT ,"
                + WINNING_AMOUNT + " TEXT ,"
                + ACTIVITY_NAME + " TEXT ,"
                + EVALUATION_FACTOR + " TEXT ,"
                + EVALUATION_FACTOR_UNIT + " TEXT ,"
                + START_ON + " TEXT ,"
                + PAUSED_ON + " TEXT ,"
                + RESUME_ON + " TEXT ,"
                + COMPLETED_ON_TXT + " TEXT ,"
                + COMPLETED_ON + " TEXT ,"
                + ACTIVITY_IMAGE + " TEXT ,"
                + WINNER + " TEXT ,"
                + PAUSED_BY + " TEXT ,"
                + CHALLENGE_TYPE + " TEXT ,"
                + WIN_REWARD_TYPE + " TEXT ,"
                + WIN_REWARD_VALUE + " TEXT ,"
                + IS_SCRATCHED + " TEXT ,"
                + GET_GPS + " TEXT ,"
                + COMPLETED_COUNT + " VARCHAR " +
                ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);

        //db.execSQL("CREATE INDEX idxModel ON serfrd (NAME);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public void addCompletedChallengesList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getChallengeID() {
        List<String> challenge_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challenge_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return challenge_id;
    }

    public List<String> getActivityID() {
        List<String> activity_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_id.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return activity_id;
    }

    public List<String> getUserName() {
        List<String> user_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_name.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return user_name;
    }



    public List<String> getOpponentName() {
        List<String> opponent_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponent_name.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return opponent_name;
    }

    public List<String> getWinningStatus() {
        List<String> winning_status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                winning_status.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return winning_status;
    }public List<String> getIsGroupAdmin() {
        List<String> is_group_admin = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                is_group_admin.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return is_group_admin;
    }public List<String> getStatus() {
        List<String> status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                status.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return status;
    }public List<String> getAmount() {
        List<String> amount = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                amount.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return amount;
    }public List<String> getWinningAmount() {
        List<String> winning_amount = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                winning_amount.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return winning_amount;
    }
    public List<String> getActivityName() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(9));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }
    public List<String> getEvaluationFactor() {
        List<String> evaluation_factor = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                evaluation_factor.add(cursor.getString(10));

            } while (cursor.moveToNext());
        }
        return evaluation_factor;
    }
    public List<String> getEvaluationFactorUnit() {
        List<String> evaluation_factor_unit = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                evaluation_factor_unit.add(cursor.getString(11));

            } while (cursor.moveToNext());
        }
        return evaluation_factor_unit;
    }
    public List<String> getStartOn() {
        List<String> start_on = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                start_on.add(cursor.getString(12));

            } while (cursor.moveToNext());
        }
        return start_on;
    }
    public List<String> getPausedOn() {
        List<String> paused_on = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                paused_on.add(cursor.getString(13));

            } while (cursor.moveToNext());
        }
        return paused_on;
    }

    public List<String> getResumeOn() {
        List<String> resume_on = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                resume_on.add(cursor.getString(14));

            } while (cursor.moveToNext());
        }
        return resume_on;
    }

    public List<String> getCompletedOnTxt() {
        List<String> completed_on_txt = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                completed_on_txt.add(cursor.getString(15));

            } while (cursor.moveToNext());
        }
        return completed_on_txt;
    }
    public List<String> getCompletedOn() {
        List<String> completed_on = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                completed_on.add(cursor.getString(16));

            } while (cursor.moveToNext());
        }
        return completed_on;
    }

    public List<String> getActivityImage() {
        List<String> activity_image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_image.add(cursor.getString(17));

            } while (cursor.moveToNext());
        }
        return activity_image;
    }

    public List<String> getWinner() {
        List<String> winner = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                winner.add(cursor.getString(18));

            } while (cursor.moveToNext());
        }
        return winner;
    }

    public List<String> getPaused_by() {
        List<String> paused_by = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                paused_by.add(cursor.getString(19));

            } while (cursor.moveToNext());
        }
        return paused_by;
    }

    public List<String> getChallengeType() {
        List<String> challenge_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challenge_type.add(cursor.getString(20));

            } while (cursor.moveToNext());
        }
        return challenge_type;
    }

    public List<String> win_RewardType() {
        List<String> reward_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                reward_type.add(cursor.getString(21));

            } while (cursor.moveToNext());
        }
        return reward_type;
    }


    public List<String> win_RewardValue() {
        List<String> reward_value = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                reward_value.add(cursor.getString(22));

            } while (cursor.moveToNext());
        }
        return reward_value;
    }


    public List<String> isScratched() {
        List<String> is_scratch = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                is_scratch.add(cursor.getString(23));

            } while (cursor.moveToNext());
        }
        return is_scratch;
    }

    public List<String> getGPS() {
        List<String> gps = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                gps.add(cursor.getString(24));

            } while (cursor.moveToNext());
        }
        return gps;
    }

    public int getCompletedCount() {
        int completed_count=0;

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                completed_count=cursor.getInt(25);

            } while (cursor.moveToNext());
        }
        // Log.d("DBRUNNING",""running_count);
        return completed_count;
    }


    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public Cursor retrieve(String searchTerm)
    {
        String[] columns={ACTIVITY_NAME};
        Cursor c=null;
        SQLiteDatabase db = this.getWritableDatabase();
        if(searchTerm != null && searchTerm.length()>0)
        {
            String sql="SELECT * FROM "+ TABLE_DETAILS+" WHERE "+ ACTIVITY_NAME+" LIKE '%"+searchTerm+"%'";
            c=db.rawQuery(sql,null);
            return c;
        }
        c=db.query(TABLE_DETAILS,columns,null,null,null,null,null);
        return c;
    }



    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
