package in.activitychallenge.activitychallenge.utilities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import in.activitychallenge.activitychallenge.activities.ReferFriendActivity;
import in.activitychallenge.activitychallenge.activities.StepChallengeStatusActivity;
import in.activitychallenge.activitychallenge.activities.StepsChallengeActivity;

/**
 * Created by Devolper on 28-Mar-18.
 */

public class BackgroundService extends Service implements SensorEventListener {

    public BackgroundService() {
    }

    private SensorManager mSensorManager;
    private Sensor mSensor;
    String steps_count;
    //   public String BROADCAST_ACTION = "in.activitychallenge.activitychallenge.StepChallengeStatusActivity";
    public String totalsteps = "";
    private long steps = 0;
    private long timestamp;

    StatusStepChallengeDB statusStepChallengeDB;
    private boolean checkInternet;
    List<String> user_completed_goal;
    //   private Thread detectorTimeStampUpdaterThread;

//    private Handler handler;

    //  private boolean isRunning = true;
    SharedPreferences sharedPreferences;
    int stepsCount;

    @Override
    public void onCreate() {
        super.onCreate();
        checkInternet = NetworkChecking.isConnected(BackgroundService.this);
        statusStepChallengeDB = new StatusStepChallengeDB(BackgroundService.this);
        Log.d("ZZZZZZZ", "RERERERER89");
        // Get sensor manager on starting the service.
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        // receiver=LocalBroadcastManager.getInstance(BackgroundService.this);
        //  setupDetectorTimestampUpdaterThread();
        // Registering...
        //   mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        // Get default sensor type
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        // Get default sensor type
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        //  mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //user_completed_goal = statusStepChallengeDB.getUserCompletedGoal();

        sharedPreferences = getSharedPreferences("StepCount", Context.MODE_PRIVATE);

        stepsCount = Integer.parseInt(sharedPreferences.getString("steps", ""));

//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("steps", t);
//        editor.apply();


        registerForSensorEvents();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("OOOOOOOO", "OOOOOOOO8999");
        // Get sensor manager on starting the service.
/*        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // Registering...
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        // Get default sensor type
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);*/

        return START_STICKY;

    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not Yet Implemented");
    }


    private void registerForSensorEvents() {

        SensorManager sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        // Step Counter
        sManager.registerListener(new SensorEventListener() {

                                      @Override
                                      public void onSensorChanged(SensorEvent event) {

                                          Sensor sensor = event.sensor;
                                          float[] values = event.values;
                                          int value = -1;

                                          if (values.length > 0) {
                                              value = (int) values[0];
                                          }
                                          if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
//                                              steps= Long.parseLong(user_completed_goal.get(0));
                                              Log.d("Usercount", "" + stepsCount);
                                              stepsCount++;

                                              Log.d("ONCHNG", "ONCHNG");
                                              getDistanceRun(stepsCount);
                                          }

                                          /*Log.d("events ", ">>>> " + Arrays.toString(event.values));

                                          long stepsw = (long) event.values[0];


                                          steps = Long.parseLong(user_completed_goal.get(0));

                                          Log.d("LONGGGGGG", "" + steps);

                                          stepsw++;
                                          Log.d("LONGGGGGG >>", ">>>> " + stepsw);
                                          //  total_steps= (int) steps_count;
                                          getDistanceRun((long) stepsw);*/
                                      }

                                      @Override
                                      public void onAccuracyChanged(Sensor sensor, int accuracy) {

                                      }
                                  }, sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),
                SensorManager.SENSOR_DELAY_UI);

        // Step Detector
        sManager.registerListener(new SensorEventListener() {

                                      @Override
                                      public void onSensorChanged(SensorEvent event) {
                                          // Time is in nanoseconds, convert to millis
                                          timestamp = event.timestamp / 1000000;
                                      }

                                      @Override
                                      public void onAccuracyChanged(Sensor sensor, int accuracy) {

                                      }
                                  }, sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),
                SensorManager.SENSOR_DELAY_UI);
    }

  /*  @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {

        Log.d("TTTTTTTTT","TTTTTTTt");
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        Sensor mySensor = sensorEvent.sensor;

       mSensorManager.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);

        if (mySensor.getType() == Sensor.TYPE_STEP_DETECTOR)
        {
              Log.d("KKK","KKK");


                steps = Long.parseLong(user_completed_goal.get(0));

                steps++;
                Log.d("LONGGGGGG",""+steps++);
                //  total_steps= (int) steps_count;
                getDistanceRun(steps);



            // Stop the sensor and service
           // mSensorManager.unregisterListener(this);
           // stopSelf();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
*/


    public float getDistanceRun(long steps) {
        checkInternet = NetworkChecking.isConnected(BackgroundService.this);

        float distance = (float) (steps * 78) / (float) 100000;
        if (checkInternet) {
            steps_count = String.valueOf(steps);
            Log.d("steps_COUNT", steps_count + "//" + steps++);
            steps++;

            // int total = Integer.parseInt(user_completed_goal)+Integer.parseInt(steps_count);
            int total = Integer.parseInt(steps_count);
            String t = String.valueOf(total);
            Log.d("steps_value123", t);
            totalsteps = t;


           // statusStepChallengeDB.getUserCompletedGoal().add(0, t);

            SharedPreferences sharedPreferences = getSharedPreferences("StepCount", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("steps", t);
            editor.apply();

            Log.d("BBBBBBB", "BBBBBBC");


        } else {

        }
        return distance;
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Log.v("Sensor", "test");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public String getStepsCount() {

        String count;
        int total = Integer.parseInt(steps_count);
        String t = String.valueOf(total);
        Log.d("steps_value123", t);
        totalsteps = t;
        count = totalsteps;
        return count;
    }
}
