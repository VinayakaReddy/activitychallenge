package in.activitychallenge.activitychallenge.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class PictureMemberCountDB extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 26;
    private static final String DATABASE_NAME = "picturemember.db";
    private static final String TABLE_DETAILS = "picturememberdetails";
    public static final String NAME = "name";
    public static final String EVALUATION_FACTOR_VALUE = "evaluation_factor_value";

    public PictureMemberCountDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public PictureMemberCountDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + NAME + " TEXT ,"
                + EVALUATION_FACTOR_VALUE +" TEXT "+
                ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }
    public void addPictureMembers(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }
    public List<String> getNames() {
        List<String> names = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                names.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return names;
    }
    public List<String> getEvaluationFactorValues() {
        List<String> evaluationvalues = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                evaluationvalues.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return evaluationvalues;
    }

    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
