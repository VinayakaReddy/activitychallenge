package in.activitychallenge.activitychallenge.utilities;

public class AppUrls {
    ////Local URL////
       /* public static String BASE_URL = "http://192.168.1.61:8090/api/v1/";
      public static String BASE_IMAGE_URL = "http://192.168.1.61:8088/activity_challenge/";*/

    //////Live URL// http://health-env-energy.com:8090//";/
    public static String BASE_URL = "http://www.activity-challenge.com:8090/api/v1/";
    public static String BASE_IMAGE_URL = "http://www.activity-challenge.com/";

    public static String GET_COUNTRIES = "get_countries";
    public static String GET_REGISTER_MOBILE = "register_mobile";
    public static String GET_OTP = "send_otp";
    public static String GET_VERIFIED_OTP = "verify_otp";
    public static String REGISTRATION = "register";
    public static String LOGIN = "verify_login";
    public static String LOGIN_SESION_STATUS = "user/session_data";
    public static String SOCIAL_AUTH = "social_auth";
    public static String FORGET_PASSWORD = "forget_password";
    public static String VERIFY_FORGETPASSWORD_OTP = "verify_fp_otp";
    public static String CHANGE_PASSWORD = "change_pwd";
    public static String ACTIVITY = "activity";
    public static String FEEDBACK = "user/feedback";
    public static String FEEDBACK_HISTORY = "user/feedback?id=";
    public static String LOT_HIS = "user/lottery/history";
    public static String LOTTERY = "user/lottery";
    public static String PROMOTIONS_VIDEO = "promotion/sport_adv";
    public static String PROMOTIONS_VIDEO_COMPLETION = "payment/scratch_card/sport_adv";
    public static String EARN_MONEY_PAY = "payment/do_txn";
    public static String EARN_MONEY_AFTER_PAY = "payment/update_txn";
    public static String HISTORY_LOTTERY_CODES = "user/lottery/my_applied_lottery";
    public static String SEND_CURRENT_LOCATION = "user/set_location";
    public static String SCRATCH_CARD = "payment/scratch_card";
    public static String INITIAL_PAYMENT = "payment/initial_payment";
    public static String WINNER = "user/lottery/lottery_winners/";
    public static String PROMOTION = "promotion";
    public static String COUNTS = "user/counts?user_id=";
    public static String CHALLENGE_TYPE="&challenge_type=";
    public static String CHALLENGE_DATE="&challenge_date=";
    public static String SCRATCH_CARD_HISTORY = "payment/scratch_card_history";
    public static String SCRATCH_CARD_DO = "payment/scratch_card/do_scratch";         //using for both TIE challange and Earn Money Scratch Card
    public static String WEEKLYCHALLENGES = "activity/challenges_mst?activity_id=";
    public static String TYPE = "&type=";
    public static String PRICE = "activity/challenges/prices?activity_id=";
    public static String ID = "&id=";
    // public static String GET_NOTIFICATION_MESSAGE ="notification/messages";
    public static String GET_NOTIFICATION_MESSAGE = "notification/chat/user";
    public static String GET_NOTIFICATION_SEND_MSG = "notification/messages/send";
    public static String NOTIFICATION_CONVERSATION = "notification/messages/conversation";
    public static String MY_CHALLENGE = "user/challenges/my_send_challenges?id=";
    public static String CANCEL_CHALLENGE = "user/challenges/my_challenge_cancellation?id=";
    public static String CHALLENGE_REQUEST = "user/challenges/request?id=";
    public static String SPONSOR_REQUEST = "sponsoring/?id=";
    public static String ACCEPT_CHALLENGE = "user/challenges/accept";
    public static String ACCEPT_SPONSOR = "sponsoring/accept_sponsor_request";
    public static String REJECT_CHALLENGE = "user/challenges/reject/?id=";
    public static String REJECT_SPONSOR = "sponsoring/reject_sponsor_request";
    public static String AFTER_SCRATCH = "payment/update_txn/sport_adv";
    public static String CHECK_USER = "user/check_genuine?user_id=";
    public static String USER_TYPE = "&user_type=";
    public static String MEMBERS_ADD_GROUP = "group/members_mst?user_id=";
    public static String CREATE_GROUP = "group/create";
    public static String UPDATE_PROFILE = "user/update";
    public static String UPDATE_PROFILE_PIC = "user/update_profile_pic";
    public static String SAVE_CARD = "payment/savecards";
    public static String SAVED_CARD = "payment/savecards?user_id=";
    public static String DELETE_CARD = "payment/savecards/";
    public static String TOP_FIVE_ACTIVITIES = "user/user_top_activity/?id=";
    public static String REFERAL_code = "user/refercode?user_id=";

    public static String GET_GROUPS = "group";
    public static String MY_GROUP_LIST = "group/user";
    public static String GROUP_LIST = "notification/chat/group";
    public static String GROUP_SEND_MESSAGES = "notification/chat/send";
    public static String GROUP_MESSAGES_CONVERSATION = "notification/chat/conversation";

    public static String ALL_MEMBERS = "user";
    public static String WIN_LOSS_CHALLENGES = "user/challenges/win_loss";
    public static String USER_BASIC_DETAILS = "user/get_basic_details?user_id=";
    public static String GET_ACCESS_CONTROL = "user/access_control?user_id=";
    public static String WALLET_AMOUNT = "payment/wallet_amount?user_id=";

    public static String FOLLOWING_STATUS = "user/following_status";
    public static String FOLLOW_USER = "user/follow_user";
    public static String UNFOLLOW_USER = "user/unfollow_user";

    public static String GROUP_FOLLOWING_STATUS = "group/following_status";
    public static String GROUP_FOLLOW = "group/follow_group";
    public static String GROUP_UNFOLLOW = "group/unfollow_group";
    public static String GET_GROUP_MEMEBER_STATUS = "group/get_members_status";
    public static String GROUP_UPDATE_PROFILE_PIC = "group/update_group_pic";
    public static String GROUP_UPDATE = "group/update";
    public static String GROUP_DELETE = "group/";
    public static String GROUP_DEL = "group/";
    public static String GROUP_UPDATE_NAME = "group/update_name";

    public static String USER_MEMBER_DETAIL = "user/member_details";
    public static String MY_PROMOTIONS = "promotion/get_my_promotions?id=";
    public static String GROUP_PROMOTION = "promotion/get_my_group_promotions?user_id=";
    public static String PROMOTE_STATUS = "promotion/get_promote_status?entity_type=";
    public static String ADD_PROMOTE = "promotion/add_promote";

    public static String MY_CHALLENGES = "user/challenges/my_challenges?";
    public static String CHALLENGE_TO_USER = "user/challenges/users?user_id=";
    public static String CHALLENGE_TO_GROUP = "user/challenges/groups?user_id=";
    public static String SEND_CHALLENGES = "user/challenges";
    public static String RECENT_HISTORY = "payment/get_recent_payment?user_id=";

    public static String ALLMORE_MYCHALLENGES = "user/challenges";  //this url used for Mychlleanges more option click based on status

    public static String USER_DEACTIVATE_REASON = "user/deactivate_reasons";
    public static String DEACTIVATE_USER_DETAIL = "user/deactivate_details/";
    public static String DEACTIVATE_USER = "user/deactivate_user/";


    public static String CHALLENGE_STATUS = "user/challenges/status_details?challenge_id=";

    public static String PROMOTION_PAY = "promotion/pay_promotion";
    public static String FCM = "activity/user/fcm";
    public static String SPONSOR_PAY = "sponsoring";

    public static String CONTACT_US_LINK = "web/contact-us";
    public static String HELP_LINK = "web/help";
    public static String TERMS_CONDITION_LINK = "web/tnc";

    public static String GROUP_MEMBER_DELETE_STATUS = "group/member/delete-status";

    public static String GROUP_REMOVE_EXIT = "group"; ///member Final Remove from Group
    public static String GROUP_DETIL_MEMBER_MORE = "group/members"; //from group detail more click


    public static String MY_FOLLOWING = "user/followings"; //My followings : to whome i following
    public static String MY_FOLLOWERS = "user/followers"; //My followers : my followers(who is following me)


    public static String UPLOAD_HOLO_PIC = "user/challenges/upload_holo_pictures";
    public static String CHALLENGE_DETAILS = "user/challenges/details";

    public static String SEND_VERIFICATION_EMAIL = "user/send-verification-email";

    public static String PAUSE_CHALLENGE = "user/challenges/pause";
    public static String MY_SPONSORINGS = "sponsoring/my_sponsoring?user_id=";
    public static String GET_APPLIED_CHALLENGES_COUNT = "user/challenges/applied/count?user_id=";

    public static String TIE_SCRATCH_CARD = "payment/scratch_card/tie";
    public static String TIE_SCRATCH_CARD_TIE_STATUS = "payment/scratch_card/tie_status";

    public static String TRANSFER_AMOUNT_TO_PAYPAL_ACC = "payment/paypal/payout";


    public static String WINLOSS_ACTIVITY_CATEGORY = "activity/played";

    public static String USER_WIN_WITH_ADMIN_STATUS = "payment/scratch_card/challenge_win_status";
    public static String USER_ACTIVITY_DATA = "user/challenges/win_loss/statistic?user_id=";
    public static String USER_WALLET_PAY = "payment/money/add";

    public static String GET_USER_ACTIVENESS_URL = "user/activeness?user_id=";

    public static String GET_SPECIAL_EVENTS = "activity/special_event?user_id=";

    public static String SPECIAL_EVENT_FOLLOW_STATUS = "activity/special_event/follow";
    public static String SPECIAL_EVENT_UNFOLLOW_STATUS = "activity/special_event/unfollow";
    public static String TAKE_CHALLENGE_SPECIAL_EVENT = "activity?special_event_id=";

    public static String SPECIAL_EVENT_NOTIFICATIONS = "activity/special_event/notifications?special_event_id=";
    public static String GPS_TRACKING = "user/challenges/latlong";

    public static String GET_NOTIFICATION = "notification/system?user_id=";
    public static String BUSINESS_PROFILE_GET = "sponsoring/business_profile";
    public static String SCAN_QRCODE = "sponsoring/drink";
    public static String SPONSOR_ALL_LIST = "sponsoring/all";
    public static String SPONSOR_STATISTICS = "sponsoring/statistic";
    public static String SPECIAL_EVENT_CHALLENGES = "activity/challenges_mst?activity_id=";
    public static String SPECIAL_EVENT_DETAILS = "activity/special_event/details?special_event_id=";
    public static String SPECIAL_EVENT_DATA_DIALOG = "activity/special_event/latest";
    public static String VIEW_SPECIAL_EVENT_DATA_DIALOG = "activity/special_event/viewed";

    public static String WEEK_LOOP_DATA = "user/challenges/weekly_rotation/challenge_detail";
    public static String CANCEL_WEEK_LOOP_DATA = "user/challenges/weekly_rotation/cancel";

    public static String BLOCK_USER = "user/block_user";
    public static String UNBLOCK_USER = "user/unblock_user";

    public static String ABUSE_TYPE = "user/abuse_type";
    public static String REPORT_ABUSE= "user/report_abuse";

    public static String SHARE_APP_URL= "http://www.activity-challenge.com/share";



}





