package in.activitychallenge.activitychallenge.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DaysWiseDB extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 25;
    private static final String DATABASE_NAME = "days.db";
    private static final String TABLE_DETAILS = "daydatewisestatus";
    public static final String DATE = "date";
    public static final String STATUS = "status";
    public static final String VALUE = "value";
    public static final String CHALLENGE_TYPE = "challenge_type";
    public static final String CHALLENGE_ID = "challenge_id";
    public static final String EVALULATION_FACTOR = "evaluation_factor";

    public DaysWiseDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DaysWiseDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " VARCHAR ,"
                + DATE + " TEXT ,"
                + STATUS + " TEXT ,"
                + VALUE +" TEXT ,"
                + CHALLENGE_TYPE +" TEXT ,"
                + EVALULATION_FACTOR +" TEXT "+
                ")";
        db.execSQL(CREATE_DATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
    }

    public List<String> getDates() {
        List<String> dates = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                dates.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return dates;
    }

    public List<String> getStatuses() {
        List<String> statuss = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                statuss.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        return statuss;
    }
    public List<String> getValues() {
        List<String> values = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                values.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return values;
    }
    public String getChallengeType() {
        String type ="";
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                type=cursor.getString(4);
            } while (cursor.moveToNext());
        }
        return type;
    }
    public String getChallengeId() {
        String id ="";
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                id=cursor.getString(0);
            } while (cursor.moveToNext());
        }
        return id;
    }
    public String getEvalulationFactor() {
        String evaluationfactor ="";
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                evaluationfactor=cursor.getString(5);
            } while (cursor.moveToNext());
        }
        return evaluationfactor;
    }
    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void addDayDates(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }
}
