package in.activitychallenge.activitychallenge.utilities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

public class ChallengeFinishedTime {
    private Context context;
    public ChallengeFinishedTime(Context context) {
            this.context = context;
    }
    public void challengeFinishTime() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            //calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 45);
            calendar.set(Calendar.HOUR, 11);
            calendar.set(Calendar.AM_PM, Calendar.PM);
            Intent intent = new Intent(context, ChallengeFinishReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context.getApplicationContext(), 1, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()
                    , pendingIntent);

        }

}
