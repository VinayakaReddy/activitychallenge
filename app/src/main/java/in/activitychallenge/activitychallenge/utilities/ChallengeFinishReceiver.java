package in.activitychallenge.activitychallenge.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class ChallengeFinishReceiver extends BroadcastReceiver {
    StatusStepChallengeDB statusStepChallengeDB;
    StatusKmsChallengeDB statusKmsChallengeDB;
    StatusMinuiteChallengeDB statusMinuiteChallengeDB;

    @Override
    public void onReceive(Context context, Intent intent) {
        statusStepChallengeDB=new StatusStepChallengeDB(context);
        statusKmsChallengeDB=new StatusKmsChallengeDB(context);
        statusMinuiteChallengeDB=new StatusMinuiteChallengeDB(context);
        statusStepChallengeDB.emptyDBBucket();
        statusKmsChallengeDB.emptyDBBucket();
        statusMinuiteChallengeDB.emptyDBBucket();
        Log.v("Challenges","Clear");

    }
}