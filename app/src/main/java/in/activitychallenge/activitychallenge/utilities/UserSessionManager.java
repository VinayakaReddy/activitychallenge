package in.activitychallenge.activitychallenge.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;


import in.activitychallenge.activitychallenge.MainActivity;
import in.activitychallenge.activitychallenge.activities.VerifyAccountActivity;

public class UserSessionManager
{
    SharedPreferences pref,preferences;
    Editor editor,editor2;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "activitychallenge";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    public static final String KEY_ACCSES = "access_key";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_ACCOUNT_STATUS = "account_status";
    public static final String PROFILE_PIC_URL = "user_profile_pic_url";
    public static final String INTRO_SLIDE = "intorslide";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String USER_REG_MOBILE= "user_reg_mobile";
    public static final String USER_TYPE= "user_type";
    public static final String DEVICE_ID= "device_id";
    public static final String COUNTRY_CODE= "country_code";
    public static final String MOBILE_NO= "mobile_no";
    public static final String PAYMENT_STATUS= "payment_status";
    public static final String SECOND_TIME= "second_time";

    public UserSessionManager(Context context){
        this._context = context;
        preferences = context.getSharedPreferences(INTRO_SLIDE, PRIVATE_MODE);
        editor2 = preferences.edit();

        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserLoginSession(String key, String username, String email, String mobile,String status,String user_type,String userid,String provided_acc_info)
    {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_ACCSES, key);//token value
        editor.putString(USER_NAME, username);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_MOBILE, mobile);
        editor.putString(USER_ACCOUNT_STATUS, status);
        editor.putString(PAYMENT_STATUS, provided_acc_info);
        editor.putString(USER_TYPE, user_type);
        editor.putString(USER_ID,userid);
        //  editor.putString(PROFILE_PIC_URL, profile_pic_url);
        editor.commit();
    }

    public void createUserId(String deviceId,String country_code,String mobile_no)
    {
        editor.putBoolean(IS_USER_LOGIN, true);
       // editor.putString(USER_ID,userId);
        editor.putString(DEVICE_ID,deviceId);
        editor.putString(COUNTRY_CODE,country_code);
        editor.putString(MOBILE_NO,mobile_no);
        editor.commit();
    }


    public void createIsFirstTimeAppLunch()
    {
        editor2.putBoolean(ISFIRTTIMEAPP, true);
        editor2.commit();
    }

    public boolean checkIsFirstTime()
    {

        if(!this.isFirstTimeAppLunch())
        {

            Intent i = new Intent(_context, VerifyAccountActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

            return true;
        }
        return false;
    }





    public HashMap<String, String> getUserDetails()
    {

        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));
        user.put(USER_ACCOUNT_STATUS, pref.getString(USER_ACCOUNT_STATUS, null));
        user.put(USER_REG_MOBILE, pref.getString(USER_REG_MOBILE, null));
        user.put(DEVICE_ID, pref.getString(DEVICE_ID, null));
        user.put(COUNTRY_CODE, pref.getString(COUNTRY_CODE, null));
        user.put(MOBILE_NO, pref.getString(MOBILE_NO, null));
        user.put(PROFILE_PIC_URL, pref.getString(PROFILE_PIC_URL, null));
        user.put(USER_TYPE, pref.getString(USER_TYPE, null));
        user.put(PAYMENT_STATUS, pref.getString(PAYMENT_STATUS, null));
        user.put(SECOND_TIME, pref.getString(SECOND_TIME, null));
        return user;
    }


    public boolean checkLogin(){

        if(!this.isUserLoggedIn()){

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }

    public boolean isUserLoggedIn()
    {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public boolean isFirstTimeAppLunch()
    {
        return preferences.getBoolean(ISFIRTTIMEAPP, false);
    }
}
