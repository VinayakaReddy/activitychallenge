package in.activitychallenge.activitychallenge.utilities;

public class Utils {
    private static final Utils ourInstance = new Utils();

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public static Utils getInstance() {
        return ourInstance;
    }
    String eventtype;
    boolean isStarted;

    public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean started) {
        isStarted = started;
    }

    private Utils() {

    }
}
