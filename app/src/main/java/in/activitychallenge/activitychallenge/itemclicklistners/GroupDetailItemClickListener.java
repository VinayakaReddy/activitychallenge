package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface GroupDetailItemClickListener
{
    void onItemClick(View v, int pos);
}
