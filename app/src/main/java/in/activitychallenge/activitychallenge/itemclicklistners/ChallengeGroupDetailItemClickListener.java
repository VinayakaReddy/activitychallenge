package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface ChallengeGroupDetailItemClickListener
{
    void onItemClick(View v, int pos);
}
