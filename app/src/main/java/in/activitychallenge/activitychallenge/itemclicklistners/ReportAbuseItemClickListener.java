package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;



public interface ReportAbuseItemClickListener {
    void onItemClick(View v, int pos);
}