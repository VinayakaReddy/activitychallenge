package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface ChallengesListItemClickListener
{
    void onItemClick(View v, int pos);
}
