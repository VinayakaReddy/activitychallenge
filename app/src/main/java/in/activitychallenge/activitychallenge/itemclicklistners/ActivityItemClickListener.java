package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface ActivityItemClickListener
{
    void onItemClick(View v, int pos);
}
