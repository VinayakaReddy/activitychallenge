package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

/**
 * Created by admin on 12/18/2017.
 */

public interface MyGroupItemClickListener {
    void onItemClick(View view, int layoutPosition);
}
