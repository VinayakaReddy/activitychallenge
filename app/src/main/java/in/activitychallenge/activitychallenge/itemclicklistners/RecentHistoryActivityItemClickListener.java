package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface RecentHistoryActivityItemClickListener
{
    void onItemClick(View v, int pos);
}
