package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface AllChalngMoreItemClickListener
{
    void onItemClick(View v, int pos);
}
