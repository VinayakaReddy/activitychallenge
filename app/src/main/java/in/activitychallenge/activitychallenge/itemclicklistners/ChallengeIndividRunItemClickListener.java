package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface ChallengeIndividRunItemClickListener
{
    void onItemClick(View v, int pos);
}
