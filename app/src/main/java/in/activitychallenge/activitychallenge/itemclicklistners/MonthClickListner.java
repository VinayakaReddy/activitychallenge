package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

/**
 * Created by admin on 12/8/2017.
 */

public interface MonthClickListner {
    void onItemClick(View view, int layoutPosition);
}
