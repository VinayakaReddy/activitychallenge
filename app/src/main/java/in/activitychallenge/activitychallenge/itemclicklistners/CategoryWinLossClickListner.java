package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface CategoryWinLossClickListner {
    void onItemClick(View v, int pos);
}
