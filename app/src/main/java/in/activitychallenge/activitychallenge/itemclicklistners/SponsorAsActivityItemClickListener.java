package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface SponsorAsActivityItemClickListener
{
    void onItemClick(View v, int pos);
}
