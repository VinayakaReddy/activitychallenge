package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface AllMembersItemClickListener
{
    void onItemClick(View v, int pos);
}
