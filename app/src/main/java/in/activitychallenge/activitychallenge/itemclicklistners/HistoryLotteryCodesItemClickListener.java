package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface HistoryLotteryCodesItemClickListener
{
    void onItemClick(View v, int pos);
}
