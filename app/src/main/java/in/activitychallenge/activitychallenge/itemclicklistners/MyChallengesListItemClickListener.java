package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface MyChallengesListItemClickListener
{
    void onItemClick(View v, int pos);
}
