package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface LotteryCodesItemClickListener
{
    void onItemClick(View v, int pos);
}
