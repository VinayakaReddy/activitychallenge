package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface GrpInMemberDetailItemClickListener
{
    void onItemClick(View v, int pos);
}
