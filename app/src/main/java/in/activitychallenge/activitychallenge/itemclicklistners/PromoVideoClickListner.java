package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

/**
 * Created by admin on 12/6/2017.
 */

public interface PromoVideoClickListner {
    void onItemClick(View view, int layoutPosition);
}
