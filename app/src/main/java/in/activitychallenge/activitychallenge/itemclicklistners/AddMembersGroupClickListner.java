package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

/**
 * Created by admin on 12/13/2017.
 */

public interface AddMembersGroupClickListner {
    void onItemClick(View view, int layoutPosition);
}
