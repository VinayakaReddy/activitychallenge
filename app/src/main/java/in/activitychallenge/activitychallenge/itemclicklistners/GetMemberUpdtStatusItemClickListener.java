package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface GetMemberUpdtStatusItemClickListener
{
    void onItemClick(View v, int pos);
}
