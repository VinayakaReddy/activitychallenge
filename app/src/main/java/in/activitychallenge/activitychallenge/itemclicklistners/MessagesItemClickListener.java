package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface MessagesItemClickListener
{
    void onItemClick(View v, int pos);
}
