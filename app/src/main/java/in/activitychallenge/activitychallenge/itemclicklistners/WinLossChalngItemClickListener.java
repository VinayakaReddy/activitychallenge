package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface WinLossChalngItemClickListener
{
    void onItemClick(View v, int pos);
}
