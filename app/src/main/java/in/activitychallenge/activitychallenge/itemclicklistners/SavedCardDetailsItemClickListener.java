package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface SavedCardDetailsItemClickListener
{
    void onItemClick(View v, int pos);
}
