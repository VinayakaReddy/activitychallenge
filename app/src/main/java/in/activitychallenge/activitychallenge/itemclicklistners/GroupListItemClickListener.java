package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface GroupListItemClickListener
{
    void onItemClick(View v, int pos);
}
