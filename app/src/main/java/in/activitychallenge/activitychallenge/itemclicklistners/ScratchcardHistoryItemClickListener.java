package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface ScratchcardHistoryItemClickListener
{
    void onItemClick(View v, int pos);
}
