package in.activitychallenge.activitychallenge.itemclicklistners;

import android.view.View;

public interface MessagesDetailItemClickListener
{
    void onItemClick(View v, int pos);
}
